﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using FlightPak.Common;
using FlightPak.Common.Constants;
using System.Text.RegularExpressions;

//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text.RegularExpressions;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.BusinessLite;
using System.Web.Services.Description;
#endregion

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewRoster : BaseSecuredPage
    {
        /*
         * Code Modification Details
         * ---------------------------------------------------------------------------
         *  Date        Developer   Task/Bug ID       Comments
         * ---------------------------------------------------------------------------
         *  19-02-2014  Karthikeyan     2659        - Fixed 2659
         * */

        private bool SaveConfirm = false;
        private bool _selectLastModified = false;
        bool isPassportPopup = false;
        #region "Crew Main"
        #region "Main Button Events"
        /// <summary>
        /// Command Event Trigger for Save or Update Crew Roster Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            Save();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Command Event Trigger for Confirm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            SaveConfirm = true;
                            Save();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }



        /// <summary>
        /// Event Trigger for Save or Update Crew Roster Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChangesTop_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Save();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Cancel Crew Roster Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Cancel Crew Roster Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelTop_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #region "Crew Roster Search Grid Events"
        /// <summary>
        /// Bind Crew Roster Data into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            var CrewData = CrewRosterService.GetAllCrewForGrid();
                            var CrewList = CrewData.EntityList.ToList();
                            if (CrewData.ReturnFlag == true)
                            {
                                if (IsPopUp && Session["SelectedCrewRosterID"] != null)
                                {
                                    CrewList = CrewData.EntityList.Where(x => x.CrewID == Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim())).ToList();
                                    dgCrewRoster.DataSource = CrewList;
                                }
                                else
                                {
                                    dgCrewRoster.DataSource = CrewList;
                                }

                            }
                            Session["CrewList"] = (List<GetAllCrewForGrid>)CrewList;
                            if (!IsPopUp)
                                DoSearchfilter(false);

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton EditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(EditButton, divExternalForm, RadAjaxLoadingPanel1);

                            // To find the insert button in the grid
                            LinkButton InsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(InsertButton, divExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Bind Crew Roster Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                pnlFilterForm.Enabled = false;
                                if (Session["SelectedCrewRosterID"] != null)
                                {
                                    btnEdit.Visible = false;
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Crew, Convert.ToInt64(Convert.ToString((Session["SelectedCrewRosterID"]))));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                                            SelectItem();
                                            pnlFilterForm.Enabled = true;
                                            return;
                                        }
                                    }
                                }
                                hdnTemp.Value = "On";
                                SelectItem();
                                DisplayEditForm();
                                DefaultChecklistSelection();
                                DefaultTypeRatingSelection();
                                GridEnable(false, true, false);
                                ddlImg.Enabled = true;
                                if (imgFile.ImageUrl.Trim() != "" || lnkFileName.NavigateUrl.Trim() != "")
                                    btndeleteImage.Enabled = true;
                                tbImgName.Enabled = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbLastName);
                                lbWarningMessage.Visible = false;
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                hdnTemp.Value = "On";
                                btnEdit.Visible = false;
                                DisplayInsertForm();
                                tbCrewCode.Enabled = true;
                                tbCrewCode.Enabled = true;
                                if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportICAOCd.Trim() != string.Empty)
                                {
                                    tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd.ToString().Trim();
                                    if (UserPrincipal != null && UserPrincipal.Identity._airportId != 0)
                                    {
                                        hdnHomeBaseID.Value = Convert.ToString(UserPrincipal.Identity._airportId);
                                    }
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;

                                }
                                pnlFilterForm.Enabled = false;
                                chkActiveCrew.Checked = true;
                                chkFixedWing.Checked = true;
                                DefaultChecklistSelection();
                                DefaultFillChecklist();
                                DefaultTypeRatingSelection();
                                GridEnable(true, false, false);
                                tbImgName.Enabled = true;
                                ddlImg.Enabled = true;
                                btndeleteImage.Enabled = true;
                                dgCrewRoster.SelectedIndexes.Clear();
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbLastName);
                                lbWarningMessage.Visible = false;
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Update Command Method for Crew Roster
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRoster_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            string CrewID = Session["SelectedCrewRosterID"].ToString();
                            string CustomerID = string.Empty, CrewCD = string.Empty;
                            foreach (GridDataItem Item in dgCrewRoster.MasterTableView.Items)
                            {
                                if (Item["CrewID"].Text.Trim() == CrewID.Trim())
                                {
                                    CrewCD = Item["CrewCD"].Text.Trim();
                                    CustomerID = Item["CustomerID"].Text.Trim();
                                    break;
                                }
                            }
                            bool IsValidate = true;
                            if (tbLastName.Text.Trim() == "")
                            {
                                tabCrewRoster.SelectedIndex = 0;
                                MainInfo.Selected = true;
                                rfvLastName.IsValid = false;
                                pnlMaintenance.Items[0].Expanded = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbLastName);
                                IsValidate = false;
                            }
                            if (tbFirstName.Text.Trim() == "")
                            {
                                tabCrewRoster.SelectedIndex = 0;
                                MainInfo.Selected = true;
                                rfvFirstName.IsValid = false;
                                pnlMaintenance.Items[0].Expanded = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbFirstName);
                                IsValidate = false;
                            }
                            if (CheckAircraftTypeCodeExist())
                            {
                                tabCrewRoster.SelectedIndex = 2;
                                CheckList.Selected = true;
                                cvAssociateTypeCode.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbAircraftType);
                                IsValidate = false;
                            }
                            if (CheckHomeBaseExist())
                            {
                                tabCrewRoster.SelectedIndex = 0;
                                MainInfo.Selected = true;
                                cvHomeBase.IsValid = false;
                                pnlMaintenance.Items[0].Expanded = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                                IsValidate = false;
                            }
                            if (CheckClientCodeExist())
                            {
                                tabCrewRoster.SelectedIndex = 0;
                                MainInfo.Selected = true;
                                cvClientCode.IsValid = false;
                                pnlMaintenance.Items[0].Expanded = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                                IsValidate = false;
                            }
                            if (CheckCountryExist(tbCountry, cvCountry))
                            {
                                tabCrewRoster.SelectedIndex = 0;
                                MainInfo.Selected = true;
                                cvCountry.IsValid = false;
                                pnlMaintenance.Items[0].Expanded = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCountry);
                                IsValidate = false;
                            }
                            if (CheckCountryExist(tbCountryBirth, cvCountryBirth))
                            {
                                tabCrewRoster.SelectedIndex = 0;
                                MainInfo.Selected = true;
                                cvCountryBirth.IsValid = false;
                                pnlMaintenance.Items[0].Expanded = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCountryBirth);
                                IsValidate = false;
                            }
                            if (CheckCountryExist(tbCitizenCode, cvCitizenCode))
                            {
                                tabCrewRoster.SelectedIndex = 0;
                                MainInfo.Selected = true;
                                cvCitizenCode.IsValid = false;
                                pnlMaintenance.Items[0].Expanded = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCitizenCode);
                                IsValidate = false;
                            }
                            if (CheckDepartmentCodeExist())
                            {
                                tabCrewRoster.SelectedIndex = 0;
                                MainInfo.Selected = true;
                                cvDepartmentCode.IsValid = false;
                                pnlMaintenance.Items[0].Expanded = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
                                IsValidate = false;
                            }
                            if (IsValidate)
                            {
                                using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                                {

                                    FlightPakMasterService.Crew oCrew = new FlightPakMasterService.Crew();
                                    oCrew = GetCrewMainInfo(oCrew);
                                    oCrew = SaveCrewDefinition(oCrew);
                                    oCrew = SaveCrewVisa(oCrew);
                                    oCrew = SaveCrewPassport(oCrew);
                                    oCrew = SaveTypeRatings(oCrew);
                                    oCrew = SaveCrewChecklist(oCrew);
                                    oCrew = SaveAircraftAssigned(oCrew);

                                    var RetVal = CrewRosterService.UpdateCrew(oCrew);
                                    if (RetVal.ReturnFlag == true)
                                    {
                                        if (dgPassport.Items.Count > 0)
                                        {
                                            UpdateFutureTripsheet(oCrew);
                                        }

                                        #region Image Upload in Edit Mode
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        foreach (var DicItem in dicImg)
                                        {

                                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "Crew";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                                                Service.UWAWebpageName = "CrewRoster.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                objService.AddFWHType(Service);
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in Delete
                                        using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                            foreach (var DicItem in dicImgDelete)
                                            {
                                                objFWHType.UWAFileName = DicItem.Key;
                                                objFWHType.RecordID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                                                objFWHType.RecordType = "Crew";
                                                objFWHType.IsDeleted = true;
                                                objFWHType.IsDeleted = true;
                                                objFWHTypeService.DeleteFWHType(objFWHType);
                                            }
                                        }
                                        #endregion

                                        ShowSuccessMessage();

                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        if (Session["SelectedCrewRosterID"] != null)
                                        {
                                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                            {
                                                var returnValue = CommonService.UnLock(EntitySet.Database.Crew, Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim()));
                                            }
                                        }

                                        // Remove Crew related Cache 
                                        CacheLite cacheObj = new CacheLite();
                                        cacheObj.RemoveCrewDetailFromCache(oCrew.CrewID);
                                        cacheObj.RemoveCrewPassportFromCache(oCrew.CrewID);

                                        GridEnable(true, true, true);
                                        chkDisplayInactiveChecklist.Checked = false;
                                        chkDisplayInactiveChecks.Checked = false;
                                        SelectItem();
                                        ReadOnlyForm();
                                        SelectItem();
                                        // Set as Main Info as Default Tab
                                        tabCrewRoster.SelectedIndex = 0;
                                        RadMultiPage1.SelectedIndex = 0;
                                        pnlFilterForm.Enabled = true;
                                        btnEdit.Visible = true;
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(RetVal.ErrorMessage, ModuleNameConstants.Database.Crew);
                                    }
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser

                            Session.Remove("SelectedCrewRosterID");
                            if (!isPassportPopup)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                            else {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('rdCrewPassport');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.ReloadCrewPassportGrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void CrewRoster_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCrewRoster.ClientSettings.Scrolling.ScrollTop = "0";
                        CrewRosterPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void CrewRoster_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CrewRosterPageNavigated)
                        {
                            SelectItem();
                        }
                        GridEnable(true, true, true);

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewRoster, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Insert Command Method for Crew Roster
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRoster_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    string returnId = "";
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (hdnSaveConfirm.Value == "Yes")
                        {
                            IsValidate = true;
                        }
                        if (CheckCrewCodeAlreadyExists())
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewCode);
                            string msgToDisplay = "Crew Code already exists, please create unique code.";
                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                            IsValidate = false;
                        }
                        if (tbLastName.Text.Trim() == "")
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            rfvLastName.IsValid = false;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbLastName);
                            IsValidate = false;
                        }



                        if (tbFirstName.Text.Trim() == "")
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            rfvFirstName.IsValid = false;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbFirstName);
                            IsValidate = false;
                        }
                        if (CheckAircraftTypeCodeExist())
                        {
                            tabCrewRoster.SelectedIndex = 2;
                            CheckList.Selected = true;
                            cvAssociateTypeCode.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAircraftType);
                            IsValidate = false;
                        }
                        if (CheckHomeBaseExist())
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            cvHomeBase.IsValid = false;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                            IsValidate = false;
                        }
                        if (CheckClientCodeExist())
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            cvClientCode.IsValid = false;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                            IsValidate = false;
                        }
                        if (CheckCountryExist(tbCountry, cvCountry))
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            cvCountry.IsValid = false;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCountry);
                            IsValidate = false;
                        }
                        if (CheckCountryExist(tbCountryBirth, cvCountryBirth))
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            cvCountryBirth.IsValid = false;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCountryBirth);
                            IsValidate = false;
                        }
                        if (CheckCountryExist(tbCitizenCode, cvCitizenCode))
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            cvCitizenCode.IsValid = false;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCitizenCode);
                            IsValidate = false;
                        }
                        if (CheckDepartmentCodeExist())
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            cvDepartmentCode.IsValid = false;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
                            IsValidate = false;
                        }
                        if (CheckDepartmentCodeExist())
                        {
                            tabCrewRoster.SelectedIndex = 0;
                            MainInfo.Selected = true;
                            cvDepartmentCode.IsValid = false;
                            pnlMaintenance.Items[0].Expanded = true;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
                            IsValidate = false;
                        }
                        if (SaveConfirm == false)
                        {
                            string FirstName = tbFirstName.Text.Trim();
                            string MiddleName = tbMiddleName.Text.Trim();
                            string LastName = tbLastName.Text.Trim();
                            int FirstNameLength = 0;
                            int MiddleNameLength = 0;
                            int LastNameLength = 0;

                            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CrewOLastFirst != null)
                            {
                                FirstNameLength = UserPrincipal.Identity._fpSettings._CrewOLastFirst.Value;
                            }
                            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CrewOLastMiddle != null)
                            {
                                MiddleNameLength = UserPrincipal.Identity._fpSettings._CrewOLastMiddle.Value;
                            }
                            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CrewOLastLast != null)
                            {
                                LastNameLength = UserPrincipal.Identity._fpSettings._CrewOLastLast.Value;
                            }
                            hdnFirstOrLast.Value = "";
                            string Mesaage = "";
                            if (FirstName.Length < FirstNameLength)
                            {
                                hdnFirstOrLast.Value = "First";
                                Mesaage = "Your First Name length should not be less than " + FirstNameLength;
                                //IsValidate = false;
                            }
                            if (MiddleName.Length < MiddleNameLength)
                            {
                                hdnFirstOrLast.Value = "Middle";
                                Mesaage += "<br/>Your Middle Name length should not be less than " + MiddleNameLength;
                                //IsValidate = false;
                            }
                            if (LastName.Length < LastNameLength)
                            {

                                hdnFirstOrLast.Value = "Last";
                                Mesaage += "<br/>Your Last Name length should not be less than " + LastNameLength;
                                //IsValidate = false;
                            }
                            if (Mesaage.Trim() != "")
                            {
                                RadWindowManager1.RadConfirm(Mesaage + ", do you want to continue?", "confirmNameLengthFn", 330, 100, null, "Confirmation!");
                                IsValidate = false;
                            }
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Crew oCrew = new FlightPakMasterService.Crew();
                                oCrew = GetCrewMainInfo(oCrew);
                                oCrew = SaveCrewDefinition(oCrew);
                                oCrew = SaveCrewVisa(oCrew);
                                oCrew = SaveCrewPassport(oCrew);

                                oCrew = SaveTypeRatings(oCrew);
                                oCrew = SaveCrewChecklist(oCrew);
                                oCrew = SaveAircraftAssigned(oCrew);
                                var RetVal = CrewRosterService.AddCrew(oCrew);
                                if (RetVal.ReturnFlag == true)
                                {
                                    #region Image Upload in New Mode
                                    var InsertedFleetID = CrewRosterService.GetCrewID("");
                                    if (InsertedFleetID != null)
                                    {
                                        foreach (FlightPakMasterService.GetCrewID FltId in InsertedFleetID.EntityList)
                                        {
                                            var crewData = CrewRosterService.GetCrewbyCrewId(Convert.ToInt64(FltId.CrewID));
                                            hdnCrewID.Value = crewData.EntityList[0].CrewCD;
                                            returnId = hdnCrewID.Value;
                                        }

                                    }

                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    foreach (var DicItem in dicImg)
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                            Service.FileWarehouseID = 0;
                                            Service.RecordType = "Crew";
                                            Service.UWAFileName = DicItem.Key;
                                            Service.RecordID = Convert.ToInt64(hdnCrewID.Value);
                                            Service.UWAWebpageName = "CrewRoster.aspx";
                                            Service.UWAFilePath = DicItem.Value;
                                            Service.IsDeleted = false;
                                            Service.FileWarehouseID = 0;
                                            objService.AddFWHType(Service);
                                        }
                                    }
                                    #endregion

                                    ShowSuccessMessage();
                                    dgCrewRoster.Rebind();
                                    GridEnable(true, true, true);
                                    chkDisplayInactiveChecklist.Checked = false;
                                    chkDisplayInactiveChecks.Checked = false;
                                    DefaultSelection(false);
                                    // Set as Main Info as Default Tab
                                    tabCrewRoster.SelectedIndex = 0;
                                    RadMultiPage1.SelectedIndex = 0;
                                    pnlFilterForm.Enabled = true;
                                    btnEdit.Visible = true;
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(RetVal.ErrorMessage, ModuleNameConstants.Database.Crew);
                                }
                            }

                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.argument = '" + returnId + "'; oWnd.close();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
                finally
                {
                    dgCrewRoster.Rebind();
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRoster_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Crew Crew = new FlightPakMasterService.Crew();
                                string CrewID = Session["SelectedCrewRosterID"].ToString();
                                string CrewCD = string.Empty;
                                GridDataItem item = dgCrewRoster.SelectedItems[0] as GridDataItem;
                                Crew.CrewID = Convert.ToInt64(CrewID);
                                Crew.CrewCD = item["CrewCD"].Text;                    // Doubt
                                Crew.MiddleInitial = tbMiddleName.Text;
                                Crew.IsDeleted = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Crew, Convert.ToInt64(Session["SelectedCrewRosterID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                                        return;
                                    }
                                }
                                CrewRosterService.DeleteCrew(Crew);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = false;
                                DefaultSelection(false);
                                lbWarningMessage.Visible = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
                finally
                {
                    if (Session["SelectedCrewRosterID"] != null)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.Crew, Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim()));
                        }
                    }
                }

            }


        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();
                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
                
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                Session.Remove("AircraftAssignedList");
                                //Session["AircraftAssignedList"] = null;
                                GridDataItem item = dgCrewRoster.SelectedItems[0] as GridDataItem;
                                pnlFilterForm.Enabled = true;
                                if (item["CrewID"].Text.Trim() != "")
                                {
                                    Session["SelectedCrewRosterID"] = item["CrewID"].Text;
                                    Session["SelectedCrewRosterCD"] = item["CrewCD"].Text;
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);

                                    DefaultChecklistSelection();
                                    DefaultTypeRatingSelection();
                                }
                            }

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                    }
                }
            }
        }
        /// <summary>
        /// Bound Row styles, based on Checklist information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            Label lbCheckList = (Label)Item.FindControl("lbCheckList");
                            if (lbCheckList.Text.Trim() == "0")
                            {
                                lbCheckList.Text = System.Web.HttpUtility.HtmlEncode("!");
                                lbCheckList.ForeColor = System.Drawing.Color.Red;
                                Item.ForeColor = System.Drawing.Color.Red;
                            }
                            else
                            {
                                lbCheckList.Text = System.Web.HttpUtility.HtmlEncode("");
                                lbCheckList.ForeColor = System.Drawing.Color.Black;
                                Item.ForeColor = System.Drawing.Color.Black;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #region "Save & Cancel "
        /// <summary>
        /// Method to Raise Save / Update Event based on Hidden Value
        /// </summary>
        private void Save()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    bool IsValidate = true;
                    if (ValidateDate())
                    {

                        string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                            + @" oManager.radalert('Termination date should be greater than Hire date in Main Information Tab', 360, 50, 'Crew Roaster Catalog');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        IsValidate = false;
                    }
                    if (ValidateDateofBirth())
                    {

                        string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                            + @" oManager.radalert('Date of Birth should not be a Future Date in Main Information Tab', 360, 50, 'Crew Roaster Catalog');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        IsValidate = false;
                    }
                    if (!ValidateVisa())
                    {
                        IsValidate = false;
                    }
                    if (!ValidatePassport())
                    {
                        IsValidate = false;
                    }
                    if (!DuplicatePassport() && IsValidate == true)
                    {
                        tabCrewRoster.SelectedIndex = 4;
                        Passports.Selected = true;
                        IsValidate = false;
                    }
                    if (IsValidate)
                    {
                        if (string.IsNullOrEmpty(hdnSave.Value) && lblSaveValue.Text.Length > 0)
                            hdnSave.Value = lblSaveValue.Text;

                        if (hdnSave.Value == "Update")
                        {
                            //if (IsConfiem == false)
                            //{
                            //    SaveConfirm = true;
                            //}
                            //else
                            //{
                            //    SaveConfirm = false;
                            //}
                            (dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                        //Session["SelectedCrewRosterID"] = null;
                        //Session["CrewAddInfo"] = null;
                        //Session["CrewTypeRating"] = null;
                        //Session["CrewCheckList"] = null;
                        //Session["CrewVisa"] = null;
                        //Session["CrewPassport"] = null;
                        //Session["CrewAircraftAssigned"] = null;
                        //Session["AircraftAssignedList"] = null;
                        hdnTemp.Value = "Off";
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            pnlFilterForm.Enabled = false;
            if (Session["SelectedCrewRosterID"] != null)
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    var returnValue = CommonService.Lock(EntitySet.Database.Crew, Convert.ToInt64(Convert.ToString((Session["SelectedCrewRosterID"]))));
                    if (!returnValue.ReturnFlag)
                    {
                        if (IsPopUp)
                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                        else
                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                        SelectItem();
                        pnlFilterForm.Enabled = true;
                        return;
                    }
                }
            }
            hdnTemp.Value = "On";
            SelectItem();
            DisplayEditForm();
            DefaultChecklistSelection();
            DefaultTypeRatingSelection();
            btnEdit.Visible = false;
            GridEnable(false, true, false);
            ddlImg.Enabled = true;
            if (imgFile.ImageUrl.Trim() != "" || lnkFileName.NavigateUrl.Trim() != "")
                btndeleteImage.Enabled = true;
            tbImgName.Enabled = true;

            RadAjaxManager.GetCurrent(Page).FocusControl(tbLastName);
        }

        /// <summary>
        /// Cancel Method
        /// </summary>
        private void Cancel()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    pnlFilterForm.Enabled = true;
                    if (Session["SelectedCrewRosterID"] != null)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim()));
                        }
                    }
                    //Session["SelectedCrewRosterID"] = null;
                    Session.Remove("AircraftAssignedList");
                    Session.Remove("CrewAddInfo");
                    Session.Remove("CrewTypeRating");
                    Session.Remove("CrewCheckList");
                    Session.Remove("CrewVisa");
                    Session.Remove("CrewPassport");
                    Session.Remove("CrewAircraftAssigned");
                    Session.Remove("AircraftAssignedList");
                    GridEnable(true, true, true);
                    btnEdit.Visible = true;
                    lbWarningMessage.Visible = false;
                    DefaultSelection(false);
                    hdnTemp.Value = "Off";
                    CheckTypeRating(false);
                    if (IsPopUp)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        #endregion
        #region "Crew Main TextChanged Events"
        protected void tbCrewCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                 try
                 {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CompanySett.IsAutoCrew != true)
                        {
                            if (CheckCrewCodeAlreadyExists())
                            {                                
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewCode);
                                string msgToDisplay = "Crew Code already exists, please create unique code.";
                                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");                               
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Method to validate country code in the Visa grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountryCD_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TextBox tbCountryCD = new TextBox();
                        int RowIndex = 0;
                        int i = 0;
                        for (i = 0; i <= dgVisa.Items.Count - 1; i += 1)
                        {
                            tbCountryCD.Text = ((TextBox)dgVisa.Items[RowIndex].Cells[0].FindControl("tbCountryCD")).Text;
                            if (tbCountryCD.Text != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var CountryValue = CountryService.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper() == (tbCountryCD.Text.Trim().ToUpper()));
                                    if (CountryValue.Count() <= 0)
                                    {

                                        string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                            + @" oManager.radalert('Invalid Country Code', 360, 50, 'Crew Roaster');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                        ((TextBox)dgVisa.Items[RowIndex].Cells[0].FindControl("tbCountryCD")).Text = string.Empty;
                                        ((TextBox)dgVisa.Items[RowIndex].Cells[0].FindControl("tbCountryCD")).Focus();
                                    }
                                    else
                                    {
                                        List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                        CountryList = (List<FlightPakMasterService.Country>)CountryValue.ToList();
                                        ((HiddenField)dgVisa.Items[RowIndex].Cells[0].FindControl("hdnCountryID")).Value = Convert.ToString(CountryList[0].CountryID);
                                    }
                                }
                            }
                            RowIndex += 1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        /// <summary>
        /// Method to validate country code in the Passport grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Nation_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TextBox tbCountryCD = new TextBox();
                        int RowIndex = 0;
                        int i = 0;
                        for (i = 0; i <= dgPassport.Items.Count - 1; i += 1)
                        {
                            tbCountryCD.Text = ((TextBox)dgPassport.Items[RowIndex].Cells[0].FindControl("tbPassportCountry")).Text;
                            if (tbCountryCD.Text != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var CountryValue = CountryService.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper() == (tbCountryCD.Text.Trim().ToUpper()));
                                    if (CountryValue.Count() <= 0)
                                    {

                                        string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                            + @" oManager.radalert('Invalid Country Code', 360, 50, 'Crew Roaster');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                        ((TextBox)dgPassport.Items[RowIndex].Cells[0].FindControl("tbPassportCountry")).Text = string.Empty;
                                        ((TextBox)dgPassport.Items[RowIndex].Cells[0].FindControl("tbPassportCountry")).Focus();
                                    }
                                    else
                                    {
                                        List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                        CountryList = (List<FlightPakMasterService.Country>)CountryValue.ToList();
                                        ((HiddenField)dgPassport.Items[RowIndex].Cells[0].FindControl("hdnPassportCountry")).Value = Convert.ToString(CountryList[0].CountryID);
                                    }
                                }
                            }
                            RowIndex += 1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #region "Validate Data"
        private bool ValidateDate()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!string.IsNullOrEmpty(tbHireDT.Text) && !string.IsNullOrEmpty(tbTermDT.Text))
                    {
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateStart = new DateTime();
                        DateTime DateEnd = new DateTime();
                        if (!string.IsNullOrEmpty(tbHireDT.Text))
                        {
                            if (DateFormat != null)
                            {
                                DateStart = Convert.ToDateTime(FormatDate(tbHireDT.Text, DateFormat));
                            }
                            else
                            {
                                DateStart = Convert.ToDateTime(tbHireDT.Text);
                            }
                        }
                        if (!string.IsNullOrEmpty(tbTermDT.Text))
                        {
                            if (DateFormat != null)
                            {
                                DateEnd = Convert.ToDateTime(FormatDate(tbTermDT.Text, DateFormat));
                            }
                            else
                            {
                                DateEnd = Convert.ToDateTime(tbTermDT.Text);
                            }
                        }
                        if (DateStart > DateEnd)
                        {
                            ReturnVal = true;
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);

                return ReturnVal;
            }

        }
        private bool ValidateDateofBirth()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!string.IsNullOrEmpty(tbDateOfBirth.Text))
                    {
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateOfBirth = new DateTime();

                        if (!string.IsNullOrEmpty(tbDateOfBirth.Text))
                        {
                            if (DateFormat != null)
                            {
                                DateOfBirth = Convert.ToDateTime(FormatDate(tbDateOfBirth.Text, DateFormat));
                            }
                            else
                            {
                                DateOfBirth = Convert.ToDateTime(tbDateOfBirth.Text);
                            }
                        }
                        if (DateOfBirth > System.DateTime.Now)
                        {
                            ReturnVal = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnVal;
            }
        }
        #endregion
        #region "Main Crew Text Changed Events"
        /// <summary>
        /// Method to check hoembase is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbHomeBase.Text.Trim() != string.Empty)
                        {
                            CheckHomeBaseExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to check clientcode is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbClientCode.Text.Trim() != string.Empty)
                        {
                            CheckClientCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to check department is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Department_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbDepartment.Text.Trim() != string.Empty)
                        {
                            CheckDepartmentCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to check countrycode is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Country_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCountry.Text != string.Empty)
                        {
                            CheckCountryExist(tbCountry, cvCountry);
                        }
                        else
                        {
                            hdnCountryID.Value = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to check country of birth is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountryBirth_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCountryBirth.Text != string.Empty)
                        {

                            CheckCountryExist(tbCountryBirth, cvCountryBirth);
                        }
                        else
                        {
                            hdnResidenceCountryID.Value = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to check citizen code is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CitizenCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCitizenCode.Text != string.Empty)
                        {
                            if (CheckCountryExist(tbCitizenCode, cvCitizenCode))
                            {
                                lbCitizenDesc.Text = System.Web.HttpUtility.HtmlEncode("");
                                cvCitizenCode.IsValid = false;
                            }
                        }
                        else
                        {
                            lbCitizenDesc.Text = System.Web.HttpUtility.HtmlEncode("");
                            hdnCitizenshipID.Value = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        
        #endregion
        #region "Insert,Edit,Read,Load,Enable & Clear"
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    lblSaveValue.Text = hdnSave.Value;
                    ClearForm();
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    lnkFileName.NavigateUrl = "";
                    chkDisplayInactiveChecklist.Checked = true;
                    chkDisplayInactiveChecks.Checked = true;
                    hdnIsPassportChoice.Value = "No";
                    lbColumnValue1.Text = "";
                    lbColumnValue2.Text = "";
                    EnableForm(true);
                    tbCrewCode.Enabled = CompanySett.IsAutoCrew == null ? true : !CompanySett.IsAutoCrew.Value;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    dgAircraftAssigned.Rebind();
                    hdnSave.Value = "Update";
                    lblSaveValue.Text = hdnSave.Value;
                    hdnRedirect.Value = "";
                    hdnIsPassportChanged.Value = string.Empty;
                    hdnIsPassportChoice.Value = "No";
                    chkDisplayInactiveChecklist.Checked = true;
                    chkDisplayInactiveChecks.Checked = true;
                    LoadControlData();
                    EnableForm(true);
                    tbCrewCode.Enabled = false;
                    btnApplyToAdditionalCrew.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    dgAircraftAssigned.Rebind();
                    LoadControlData();
                    DefaultChecklistSelection();
                    DefaultTypeRatingSelection();
                    EnableForm(false);
                    if (dgCrewCheckList.Items.Count > 0)
                    {
                        btnApplyToAdditionalCrew.Enabled = true;
                    }
                    else
                    {
                        btnApplyToAdditionalCrew.Enabled = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear the Form Fields
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Main Info. Fields
                    chkActiveCrew.Checked = true;
                    chkFixedWing.Checked = true;
                    chkRoteryWing.Checked = false;
                    tbCrewCode.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    tbClientCode.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbFirstName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbAddress1.Text = string.Empty;
                    tbAddress2.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbStateProvince.Text = string.Empty;
                    tbStateProvincePostal.Text = string.Empty;
                    tbCountry.Text = string.Empty;
                    rbMale.Checked = true;
                    rbFemale.Checked = false;
                    tbDateOfBirth.Text = string.Empty;
                    chkNoCalenderDisplay.Checked = false;
                    tbSSN.Text = string.Empty;
                    tbCityBirth.Text = string.Empty;
                    tbStateBirth.Text = string.Empty;
                    tbCountryBirth.Text = string.Empty;
                    tbHomePhone.Text = string.Empty;
                    tbMobilePhone.Text = string.Empty;
                    tbPager.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbHireDT.Text = string.Empty;
                    tbTermDT.Text = string.Empty;
                    tbCitizenCode.Text = string.Empty;
                    lbCitizenDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    tbLicNo.Text = string.Empty;
                    tbLicExpDate.Text = string.Empty;
                    tbLicCity.Text = string.Empty;
                    tbLicCountry.Text = string.Empty;
                    tbLicType.Text = string.Empty;
                    tbAddLic.Text = string.Empty;
                    tbAddLicExpDate.Text = string.Empty;
                    tbAddLicCity.Text = string.Empty;
                    tbAddLicCountry.Text = string.Empty;
                    tbAddLicType.Text = string.Empty;
                    tbCrewType.Text = string.Empty;
                    tbDepartment.Text = string.Empty;
                    tbNotes.Text = string.Empty;
                    // <new fields>
                    tbAddress3.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    tbSecondaryMobile.Text = string.Empty;
                    tbBusinessFax.Text = string.Empty;
                    tbBusinessPhone.Text = string.Empty;
                    tbAsOfDt.Text = string.Empty;

                    tbBirthday.Text = string.Empty;
                    tbAnnivesaries.Text = string.Empty;
                    tbHotelPreferences.Text = string.Empty;
                    tbEmergencyContact.Text = string.Empty;
                    tbCreditCardInfo.Text = string.Empty;
                    tbCateringPreferences.Text = string.Empty;
                    chkDisplayInactiveChecks.Checked = false;

                    // <\new fields>
                    # region Clear Section
                    hdnNationalityID.Value = "";
                    hdnClientID.Value = "";
                    hdnHomeBaseID.Value = "";
                    hdnDepartmentID.Value = "";
                    hdnResidenceCountryID.Value = "";
                    hdnLicCountryID.Value = "";
                    hdnAddLicCountryID.Value = "";
                    hdnCitizenshipID.Value = "";
                    hdnCountryID.Value = "";
                    hdnCrewID.Value = "";  // check
                    hdnCrewGroupID.Value = "";
                    #endregion
                    chkActiveCrew.Checked = false;
                    chkFixedWing.Checked = false;
                    chkRoteryWing.Checked = false;
                    chkIsTripEmail.Checked = false;
                    #region "Add Infor Grid"
                    Session.Remove("CrewAddInfo");
                    BindAdditionalInfoGrid(0);
                    #endregion

                    #region "Type Rating Grid"
                    ClearTypeRatingFields();
                    Session.Remove("CrewTypeRating");
                    BindTypeRatingGrid(0);


                    #endregion

                    #region "Crew Checklist grid"
                    Session.Remove("CrewCheckList");
                    ClearCheckListFields();
                    BindCrewCheckListGrid(0);
                    #endregion

                    #region "Aircraft Assign Grid"
                    // Aircraft Assigned Fields 
                    Session.Remove("CrewAircraftAssigned");
                    foreach (GridDataItem Item in dgAircraftAssigned.MasterTableView.Items)
                    {
                        ((CheckBox)Item["AircraftAssigned"].FindControl("chkAircraftAssigned")).Checked = false;
                    }
                    #endregion

                    #region "Passport Grid"
                    // Passports/Visas Fields
                    Session.Remove("CrewPassport");
                    BindCrewPassportGrid(0);
                    #endregion

                    #region "Crew Visa Grid"
                    Session.Remove("CrewVisa");
                    BindCrewVisaGrid(0);
                    #endregion

                    #region Image clear
                    CreateDictionayForImgUpload();
                    fileUL.Enabled = false;
                    ddlImg.Enabled = false;
                    imgFile.ImageUrl = "";
                    ImgPopup.ImageUrl = null;
                    lnkFileName.NavigateUrl = "";
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbNotes.Text = "";
                    tbImgName.Text = "";
                    #endregion

                    #region "Permanent Residence Card"

                    tbCardExpires.Text = string.Empty;
                    tbGreenNationality.Text = string.Empty;
                    tbINSANumber.Text = string.Empty;
                    tbResidentSinceYear.Text = string.Empty;
                    tbCategory.Text = string.Empty;
                    hdnGreenNationality.Value = "";

                    #endregion

                    // Currency Fields
                    lbToday.Text = System.Web.HttpUtility.HtmlEncode(DateTime.Now.ToShortDateString());
                    tabSwitchViews.SelectedIndex = 1;
                    BindCrewCurrency(0);
                    // Addl Notes Field
                    tbAdditionalNotes.Text = string.Empty;
                    //Email Preferences Fields
                    chkSelectAll.Checked = false;
                    EnableEmailPreferences(false);
                    radZULU.Checked = true;
                    radLocal.Checked = false;

                    chkDisplayInactiveChecklist.Checked = false;
                    hdnIsPassportChoice.Value = "No";
                    hdnWarning.Value = "1";
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Enable or Disable form
        /// </summary>
        /// <param name="Enable">Pass Boolean Value</param>
        private void CheckTypeRating(bool Assign)
        {
            chkTRPIC91.Enabled = Assign;
            chkTRSIC91.Enabled = Assign;
            chkTREngineer.Enabled = Assign;
            chkTRAttendant91.Enabled = Assign;
            chkTRAirman.Enabled = Assign;
            chkTRPIC135.Enabled = Assign;
            chkTRSIC135.Enabled = Assign;
            chkTRInstructor.Enabled = Assign;
            chkTRAttendant135.Enabled = Assign;
            chkTRAttendant.Enabled = Assign;
            chkPIC121.Enabled = Assign;
            chkSIC121.Enabled = Assign;
            chkPIC125.Enabled = Assign;
            chkSIC125.Enabled = Assign;
            chkAttendent121.Enabled = Assign;
            chkAttendent125.Enabled = Assign;
        }

        /// <summary>
        /// Method to Check/Uncheck TypeRating
        /// </summary>
        /// <param name="Enable">Pass Boolean Value</param>
        private void CheckUncheckTypeRating(bool Assign)
        {
            chkTRPIC91.Checked = Assign;
            chkTRSIC91.Checked = Assign;
            chkTREngineer.Checked = Assign;
            chkTRAttendant91.Checked = Assign;
            chkTRAirman.Checked = Assign;
            chkTRPIC135.Checked = Assign;
            chkTRSIC135.Checked = Assign;
            chkTRInstructor.Checked = Assign;
            chkTRAttendant135.Checked = Assign;
            chkTRAttendant.Checked = Assign;
            chkPIC121.Checked = Assign;
            chkSIC121.Checked = Assign;
            chkPIC125.Checked = Assign;
            chkSIC125.Checked = Assign;
            chkAttendent121.Checked = Assign;
            chkAttendent125.Checked = Assign;
        }

        /// <summary>
        /// Method to Enable or Disable form
        /// </summary>
        /// <param name="Enable">Pass Boolean Value</param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCrewCode.Enabled = Enable;
                    tbHomeBase.Enabled = Enable;
                    if (UserPrincipal != null && UserPrincipal.Identity._clientId != null && UserPrincipal.Identity._clientId != 0)
                    {
                        tbClientCode.Enabled = false;
                        tbClientCode.Text = UserPrincipal.Identity._clientCd.ToString().Trim();
                        hdnClientID.Value = UserPrincipal.Identity._clientId.ToString().Trim();
                        btnClientCode.Enabled = false;
                    }
                    else
                    {
                        tbClientCode.Enabled = Enable;
                        btnClientCode.Enabled = Enable;
                    }
                    tbLastName.Enabled = Enable;
                    tbFirstName.Enabled = Enable;
                    tbMiddleName.Enabled = Enable;
                    tbAddress1.Enabled = Enable;
                    tbAddress2.Enabled = Enable;
                    tbCity.Enabled = Enable;
                    tbStateProvince.Enabled = Enable;
                    tbStateProvincePostal.Enabled = Enable;
                    tbCountry.Enabled = Enable;
                    rbMale.Enabled = Enable;
                    rbFemale.Enabled = Enable;
                    tbAsOfDt.Enabled = Enable;
                    tbHireDT.Enabled = Enable;
                    tbDateOfBirth.Enabled = Enable;
                    tbTermDT.Enabled = Enable;

                    tbBirthday.Enabled = Enable;
                    tbAnnivesaries.Enabled = Enable;
                    tbHotelPreferences.Enabled = Enable;
                    tbEmergencyContact.Enabled = Enable;
                    tbCreditCardInfo.Enabled = Enable;
                    tbCateringPreferences.Enabled = Enable;

                    btnCountry.Enabled = Enable;

                    tbCardExpires.Enabled = Enable;
                    tbGreenNationality.Enabled = Enable;
                    tbINSANumber.Enabled = Enable;
                    tbResidentSinceYear.Enabled = Enable;
                    tbCategory.Enabled = Enable;

                    if (Enable == true)
                        btnCountry.CssClass = "button";
                    else
                        btnCountry.CssClass = "button-disable";

                    chkNoCalenderDisplay.Enabled = Enable;
                    tbSSN.Enabled = Enable;
                    tbCityBirth.Enabled = Enable;
                    tbStateBirth.Enabled = Enable;
                    tbCountryBirth.Enabled = Enable;
                    tbHomePhone.Enabled = Enable;
                    tbMobilePhone.Enabled = Enable;
                    tbPager.Enabled = Enable;
                    tbFax.Enabled = Enable;
                    tbEmail.Enabled = Enable;
                    tbCitizenCode.Enabled = Enable;
                    lbCitizenDesc.Enabled = Enable;
                    tbLicNo.Enabled = Enable;
                    tbLicExpDate.Enabled = Enable;
                    tbLicCity.Enabled = Enable;
                    tbLicCountry.Enabled = Enable;
                    tbLicType.Enabled = Enable;
                    tbAddLic.Enabled = Enable;
                    tbAddLicExpDate.Enabled = Enable;
                    tbAddLicCity.Enabled = Enable;
                    tbAddLicCountry.Enabled = Enable;
                    tbAddLicType.Enabled = Enable;
                    tbCrewType.Enabled = Enable;
                    tbDepartment.Enabled = Enable;
                    tbNotes.Enabled = Enable;
                    chkActiveCrew.Enabled = Enable;
                    chkFixedWing.Enabled = Enable;
                    chkRoteryWing.Enabled = Enable;
                    chkIsTripEmail.Enabled = Enable;
                    //<new field>
                    tbAddress3.Enabled = Enable;
                    tbOtherEmail.Enabled = Enable;
                    tbPersonalEmail.Enabled = Enable;
                    tbOtherPhone.Enabled = Enable;
                    tbSecondaryMobile.Enabled = Enable;
                    tbBusinessFax.Enabled = Enable;
                    tbBusinessPhone.Enabled = Enable;
                    //<\new field>
                    chkDisplayInactiveChecklist.Enabled = true;
                    //chkDisplayInactiveChecks.Enabled = Enable;
                    ddlImg.Enabled = Enable;
                    tbImgName.Enabled = Enable;
                    btndeleteImage.Enabled = Enable;
                    fileUL.Enabled = Enable;

                    // Addl Info Fields
                    dgCrewAddlInfo.Enabled = Enable;

                    // Type Ratings Fields
                    lbTypeCode.Enabled = Enable;
                    lbDescription.Enabled = Enable;
                    chkTRInactive.Enabled = Enable;
                    if (hdnSave.Value == "Update" || hdnSave.Value == "Save")
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            CheckTypeRating(true);

                        }
                        else
                        {
                            CheckUncheckTypeRating(false);
                            CheckTypeRating(false);
                        }
                    }
                    else
                    {
                        CheckTypeRating(false);
                    }
                    EnableHours(Enable);
                    lbCheckListCode.Enabled = Enable;
                    lbCheckListDescription.Enabled = Enable;
                    EnableCheckListFields(Enable);
                    btnAircraftType.Enabled = Enable;
                    dgAircraftAssigned.Enabled = Enable;
                    dgPassport.Enabled = Enable;
                    foreach (GridDataItem item in dgVisa.MasterTableView.Items)
                    {
                        item.Enabled = Enable;
                    }
                    lbToday.Enabled = Enable;
                    //tabSwitchViews.Enabled = Enable;
                    tbAdditionalNotes.Enabled = Enable;
                    chkSelectAll.Enabled = Enable;
                    chkDepartmentAuthorization.Enabled = Enable;
                    chkRequestorPhone.Enabled = Enable;
                    chkAccountNo.Enabled = Enable;
                    chkCancellationDesc.Enabled = Enable;
                    chkStatus.Enabled = Enable;
                    chkAirport.Enabled = Enable;
                    chkChecklist.Enabled = Enable;
                    chkRunway.Enabled = Enable;
                    chkHomeArrival.Enabled = Enable;
                    chkHomeDeparture.Enabled = Enable;
                    radZULU.Enabled = Enable;
                    radLocal.Enabled = Enable;
                    chkEndDuty.Enabled = Enable;
                    chkOverride.Enabled = Enable;
                    chkCrewRules.Enabled = Enable;
                    chkFAR.Enabled = Enable;
                    chkDutyHours.Enabled = Enable;
                    chkFlightHours.Enabled = Enable;
                    chkRestHours.Enabled = Enable;
                    chkAssociatedCrew.Enabled = Enable;
                    chkNEWSPAPER.Enabled = Enable;
                    chkCOFFEE.Enabled = Enable;
                    chkIsTripEmail.Enabled = Enable;
                    chkJUICE.Enabled = Enable;
                    chkDepartureInformation.Enabled = Enable;
                    chkArrivalInformation.Enabled = Enable;
                    chkCrewTransport.Enabled = Enable;
                    chkCrewArrival.Enabled = Enable;
                    chkHotel.Enabled = Enable;
                    chkPassengerTransport.Enabled = Enable;
                    chkPassengerArrival.Enabled = Enable;
                    chkPassengerDetails.Enabled = Enable;
                    chkPassengerHotel.Enabled = Enable;
                    chkPassengerPaxPhone.Enabled = Enable;
                    chkDepartureCatering.Enabled = Enable;
                    chkArrivalCatering.Enabled = Enable;
                    chkArrivalDepartureTime.Enabled = Enable;
                    chkAircraft.Enabled = Enable;
                    chkArrivalDepartureICAO.Enabled = Enable;
                    chkGeneralCrew.Enabled = Enable;
                    chkGeneralPassenger.Enabled = Enable;
                    chkOutBoundInstructions.Enabled = Enable;
                    chkGeneralChecklist.Enabled = Enable;
                    chkLogisticsFBO.Enabled = Enable;
                    chkLogisticsHotel.Enabled = Enable;
                    chkLogisticsTransportation.Enabled = Enable;
                    chkLogisticsCatering.Enabled = Enable;
                    chkCrewHotelConfirm.Enabled = Enable;
                    chkCrewHotelComments.Enabled = Enable;
                    chkCrewDeptTransConfirm.Enabled = Enable;
                    chkCrewDeptTransComments.Enabled = Enable;
                    chkCrewArrTransConfirm.Enabled = Enable;
                    chkCrewArrTransComments.Enabled = Enable;
                    chkCrewNotes.Enabled = Enable;
                    chkPAXHotelConfirm.Enabled = Enable;
                    chkPAXHotelComments.Enabled = Enable;
                    chkPAXDeptTransConfirm.Enabled = Enable;
                    chkPAXDeptTransComments.Enabled = Enable;
                    chkPAXArrTransConfirm.Enabled = Enable;
                    chkPAXArrTransComments.Enabled = Enable;
                    chkPAXNotes.Enabled = Enable;
                    chkOutboundInstComments.Enabled = Enable;
                    chkDeptAirportNotes.Enabled = Enable;
                    chkArrAirportNotes.Enabled = Enable;
                    chkDeptAirportAlerts.Enabled = Enable;
                    chkArrAirportAlerts.Enabled = Enable;
                    chkArrFBOConfirm.Enabled = Enable;
                    chkArrFBOComments.Enabled = Enable;
                    chkDeptFBOConfirm.Enabled = Enable;
                    chkDeptFBOComments.Enabled = Enable;
                    chkDeptCaterConfirm.Enabled = Enable;
                    chkDeptCaterComments.Enabled = Enable;
                    chkArrCaterConfirm.Enabled = Enable;
                    chkArrCaterComments.Enabled = Enable;
                    chkTripAlerts.Enabled = Enable;
                    chkTripNotes.Enabled = Enable;
                    btnHomeBase.Enabled = Enable;
                    btnCountryPopup.Enabled = Enable;
                    btnCitizenship.Enabled = Enable;
                    //btnLicCountry.Enabled = Enable;
                    //btnAddLicCountry.Enabled = Enable;
                    btnCountryOfBirth.Enabled = Enable;
                    btnAddlInfo.Enabled = Enable;
                    btnDeleteInfo.Enabled = Enable;
                    btnDepartment.Enabled = Enable;
                    btnAddChecklist.Enabled = Enable;
                    btnDeleteChecklist.Enabled = Enable;
                    btnChgBaseDate.Enabled = Enable;
                    btnAddVisa.Enabled = Enable;
                    btnDeleteVisa.Enabled = Enable;
                    btnAddPassport.Enabled = Enable;
                    btnDeletePassport.Enabled = Enable;
                    btndeleteImage.Visible = Enable;
                    btnAddlInfo.Visible = Enable;
                    btnDeleteInfo.Visible = Enable;
                    btnAddPassport.Visible = Enable;
                    btnDeletePassport.Visible = Enable;
                    btnAddVisa.Visible = Enable;
                    btnDeleteVisa.Visible = Enable;
                    btnAddChecklist.Visible = Enable;
                    btnDeleteChecklist.Visible = Enable;
                    btnFlightExp.Visible = Enable;
                    btnUpdateFltExperience.Visible = Enable;
                    btnUpdateAllFltExperience.Visible = Enable;
                    btnAddRating.Visible = Enable;
                    btnDeleteRating.Visible = Enable;
                    btnSaveChanges.Visible = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChangesTop.Visible = Enable;
                    btnCancelTop.Visible = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        if (IsAuthorized(Permission.Database.AddCrewRoster))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                            {
                                lbtnInsertCtl.Enabled = true;
                            }
                            else
                            {
                                lbtnInsertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.DeleteCrewRoster))
                        {
                            lbtnDelCtl.Visible = true;
                            if (Delete)
                            {
                                lbtnDelCtl.Enabled = true;
                                lbtnDelCtl.OnClientClick = "javascript:return ProcessCrewCheckDelete();";
                            }
                            else
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.EditCrewRoster))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                            {
                                lbtnEditCtl.Enabled = true;
                                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnEditCtl.Enabled = false;
                                lbtnEditCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// To assign the data to controls 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //if (dgCrewRoster.SelectedItems.Count == 0)
                    //{
                    //    DefaultSelection(false);
                    //}
                    ClearForm();
                    if (Session["SelectedCrewRosterID"] != null)
                    {
                        // Clear the Existing Session Values
                        Session.Remove("AircraftAssignedList");
                        Session.Remove("CrewAddInfo");
                        Session.Remove("CrewTypeRating");
                        Session.Remove("CrewCheckList");
                        Session.Remove("CrewVisa");
                        Session.Remove("CrewPassport");
                        Session.Remove("CrewAircraftAssigned");
                        string CrewID = Session["SelectedCrewRosterID"].ToString();
                        string LastUpdUID = string.Empty, LastUpdTS = string.Empty, CustomerID = string.Empty;
                        foreach (GridDataItem Item in dgCrewRoster.MasterTableView.Items)
                        {
                            if (Item["CrewID"].Text.Trim() == CrewID.Trim())
                            {
                                LastUpdUID = Item["LastUpdUID"].Text.Trim().Replace("&nbsp;", "");
                                LastUpdTS = Item["LastUpdTS"].Text.Trim().Replace("&nbsp;", "");
                                CustomerID = Item["CustomerID"].Text.Trim().Replace("&nbsp;", "");

                                lbColumnName1.Text = "Crew Code";
                                lbColumnName2.Text = "Crew Name";
                                lbColumnValue1.Text = Item["CrewCD"].Text;
                                lbColumnValue2.Text = Item["CrewName"].Text;
                                Item.Selected = true;
                                break;
                            }
                        }

                        Label lblUser = (Label)dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (!string.IsNullOrEmpty(LastUpdUID))
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + LastUpdUID);
                        }
                        else
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                        if (!string.IsNullOrEmpty(LastUpdTS))
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(LastUpdTS)));
                        }
                        List<FlightPakMasterService.GetCrewByCrewID> objCrew = new List<FlightPakMasterService.GetCrewByCrewID>();
                        using (FlightPakMasterService.MasterCatalogServiceClient objCrewService = new MasterCatalogServiceClient())
                        {
                            var Results = objCrewService.GetCrewbyCrewId(Convert.ToInt64(Session["SelectedCrewRosterID"].ToString())).EntityList;
                            if (Results.Count() > 0 && Results != null)
                            {
                                foreach (GetCrewByCrewID Crew in Results)
                                {
                                    // Main Info Fields
                                    chkActiveCrew.Checked = Convert.ToBoolean(Crew.IsStatus, CultureInfo.CurrentCulture);
                                    chkFixedWing.Checked = Convert.ToBoolean(Crew.IsFixedWing, CultureInfo.CurrentCulture);
                                    chkRoteryWing.Checked = Convert.ToBoolean(Crew.IsRotaryWing, CultureInfo.CurrentCulture);
                                    tbCrewCode.Text = Crew.CrewCD;
                                    tbHomeBase.Text = (!string.IsNullOrEmpty(Crew.HomeBaseCD)) ? Crew.HomeBaseCD : string.Empty;
                                    tbClientCode.Text = (!string.IsNullOrEmpty(Crew.ClientCD)) ? Crew.ClientCD : string.Empty;
                                    tbLastName.Text = (Crew.LastName != null) ? Crew.LastName : string.Empty;
                                    tbFirstName.Text = (Crew.FirstName != null) ? Crew.FirstName : string.Empty;
                                    tbMiddleName.Text = (Crew.MiddleInitial != null) ? Crew.MiddleInitial : string.Empty;
                                    tbAddress1.Text = (Crew.Addr1 != null) ? Crew.Addr1 : string.Empty;
                                    tbAddress2.Text = (Crew.Addr2 != null) ? Crew.Addr2 : string.Empty;
                                    tbCity.Text = (Crew.CityName != null) ? Crew.CityName : string.Empty;
                                    tbStateProvince.Text = (Crew.StateName != null) ? Crew.StateName : string.Empty;
                                    tbStateProvincePostal.Text = (Crew.PostalZipCD != null) ? Crew.PostalZipCD : string.Empty;
                                    tbCountry.Text = (!string.IsNullOrEmpty(Crew.ResidenceCountryCD)) ? Crew.ResidenceCountryCD : string.Empty;
                                    if (Crew.Gender != null)
                                    {
                                        if (Crew.Gender.ToString() == "M")
                                        {
                                            rbMale.Checked = true;
                                            rbFemale.Checked = false;
                                        }
                                        else
                                        {
                                            rbMale.Checked = false;
                                            rbFemale.Checked = true;
                                        }
                                    }

                                    tbDateOfBirth.Text = (Crew.BirthDT != null) ? String.Format("{0:" + DateFormat + "}", Crew.BirthDT) : string.Empty;
                                    chkNoCalenderDisplay.Checked = Convert.ToBoolean(Crew.IsNoCalendarDisplay, CultureInfo.CurrentCulture);
                                    tbSSN.Text = (Crew.SSN != null) ? Crew.SSN : string.Empty;
                                    tbCityBirth.Text = (Crew.CityOfBirth != null) ? Crew.CityOfBirth : string.Empty;
                                    tbStateBirth.Text = (Crew.StateofBirth != null) ? Crew.StateofBirth : string.Empty;
                                    tbCountryBirth.Text = (!string.IsNullOrEmpty(Crew.NationalityCD)) ? Crew.NationalityCD : string.Empty;
                                    tbHomePhone.Text = (Crew.PhoneNum != null) ? Crew.PhoneNum : string.Empty;
                                    tbMobilePhone.Text = (Crew.CellPhoneNum != null) ? Crew.CellPhoneNum : string.Empty;
                                    tbPager.Text = (Crew.PagerNum != null) ? Crew.PagerNum : string.Empty;
                                    tbFax.Text = (Crew.FaxNum != null) ? Crew.FaxNum : string.Empty;
                                    tbEmail.Text = (Crew.EmailAddress != null) ? Crew.EmailAddress : string.Empty;
                                    tbHireDT.Text = (Crew.HireDT != null) ? String.Format("{0:" + DateFormat + "}", Crew.HireDT) : string.Empty;
                                    tbTermDT.Text = (Crew.TerminationDT != null) ? String.Format("{0:" + DateFormat + "}", Crew.TerminationDT) : string.Empty;
                                    tbCitizenCode.Text = (Crew.CitizenshipCD != null) ? Crew.CitizenshipCD : string.Empty;
                                    lbCitizenDesc.Text = System.Web.HttpUtility.HtmlEncode((Crew.CitizenshipName != null) ? Crew.CitizenshipName : string.Empty);
                                    tbLicNo.Text = (Crew.PilotLicense1 != null) ? Crew.PilotLicense1 : string.Empty;
                                    tbLicExpDate.Text = (Crew.License1ExpiryDT != null) ? String.Format("{0:" + DateFormat + "}", Crew.License1ExpiryDT) : string.Empty;
                                    tbLicCity.Text = (Crew.License1CityCountry != null) ? Crew.License1CityCountry : string.Empty;
                                    if (!string.IsNullOrEmpty(Crew.LicenseCountry1) && Crew.LicenseCountry1.Trim() != "")
                                    {
                                        List<Country> c = objCrewService.GetCountryMasterList().EntityList;
                                        Country fc = c.Where(x => x.CountryCD.ToUpper() == Crew.LicenseCountry1.ToUpper()).FirstOrDefault();
                                        hdnLICCountryCode.Value = fc.CountryCD.ToString();
                                        hdntbLicCountryId.Value = fc.CountryID.ToString();
                                        hdntbLicCountryName.Value = Convert.ToString(fc.CountryName);
                                        tbLicCountry.Text = Convert.ToString(fc.CountryName);
                                    }
                                    else
                                    {
                                        tbLicCountry.Text = string.Empty;
                                        hdnLICCountryCode.Value = string.Empty;
                                        hdntbLicCountryId.Value = "0";
                                        hdntbLicCountryName.Value = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(Crew.LicenseCountry2) && Crew.LicenseCountry2.Trim() != "")
                                    {
                                        List<Country> c = objCrewService.GetCountryMasterList().EntityList;
                                        Country fc = c.Where(x => x.CountryCD.ToUpper() == Crew.LicenseCountry2.ToUpper()).FirstOrDefault();
                                        hdnADDLICCountryCode.Value = fc.CountryCD.ToString();
                                        hdntbAddLicCountryId.Value = fc.CountryID.ToString();
                                        hdntbAddLicCountryName.Value = Convert.ToString(fc.CountryName);
                                        tbAddLicCountry.Text = Convert.ToString(fc.CountryName);
                                    }
                                    else
                                    {
                                        tbAddLicCountry.Text = string.Empty;
                                        hdnADDLICCountryCode.Value = string.Empty;
                                        hdntbAddLicCountryId.Value = "0";
                                        hdntbAddLicCountryName.Value = string.Empty;
                                    }
                                    tbLicType.Text = (Crew.LicenseType1 != null) ? Crew.LicenseType1 : string.Empty;
                                    tbAddLic.Text = (Crew.PilotLicense2 != null) ? Crew.PilotLicense2 : string.Empty;
                                    tbAddLicExpDate.Text = (Crew.License2ExpiryDT != null) ? String.Format("{0:" + DateFormat + "}", Crew.License2ExpiryDT) : string.Empty;
                                    tbAddLicCity.Text = (Crew.License2CityCountry != null) ? Crew.License2CityCountry : string.Empty;
                                    tbAddress3.Text = (!string.IsNullOrEmpty(Crew.Addr3)) ? Crew.Addr3 : string.Empty;
                                    tbOtherEmail.Text = (!string.IsNullOrEmpty(Crew.OtherEmail)) ? Crew.OtherEmail : string.Empty;
                                    tbPersonalEmail.Text = (!string.IsNullOrEmpty(Crew.PersonalEmail)) ? Crew.PersonalEmail : string.Empty;
                                    tbOtherPhone.Text = (!string.IsNullOrEmpty(Crew.OtherPhone)) ? Crew.OtherPhone : string.Empty;
                                    tbSecondaryMobile.Text = (!string.IsNullOrEmpty(Crew.CellPhoneNum2)) ? Crew.CellPhoneNum2 : string.Empty;
                                    tbBusinessFax.Text = (!string.IsNullOrEmpty(Crew.BusinessFax)) ? Crew.BusinessFax : string.Empty;
                                    tbBusinessPhone.Text = (!string.IsNullOrEmpty(Crew.BusinessPhone)) ? Crew.BusinessPhone : string.Empty;
                                    tbAddLicType.Text = (Crew.LicenseType2 != null) ? Crew.LicenseType2 : string.Empty;
                                    //Commented to fix Bug# 5927
                                    //tbCrewType.Text = (Crew.CrewTypeCD != null) ? Crew.CrewTypeCD : string.Empty;
                                    tbCrewType.Text = (Crew.CrewTypeDescription != null) ? Crew.CrewTypeDescription : string.Empty;
                                    tbDepartment.Text = (!string.IsNullOrEmpty(Crew.DepartmentCD)) ? Crew.DepartmentCD : string.Empty;
                                    tbNotes.Text = (Crew.Notes != null) ? Crew.Notes : string.Empty;
                                    tbINSANumber.Text = (Crew.INSANumber != null) ? Crew.INSANumber : string.Empty;
                                    tbCategory.Text = (Crew.Category != null) ? Crew.Category : string.Empty;
                                    tbGreenNationality.Text = (Crew.GreenCardCountryCD != null) ? Crew.GreenCardCountryCD : string.Empty;
                                    tbResidentSinceYear.Text = (Crew.ResidentSinceYear != null) ? Crew.ResidentSinceYear.ToString() : string.Empty;
                                    tbCardExpires.Text = (Crew.CardExpires != null) ?  String.Format("{0:" + DateFormat + "}",Crew.CardExpires) : string.Empty;

                                    if (tbAdditionalNotes.Text.Trim() != "")
                                    {
                                        btnAdditionalNotes.CssClass = "ui_nav_ForeColor_Red";
                                    }
                                    else
                                    {
                                        btnAdditionalNotes.CssClass = "ui_nav";
                                    }
                                    // Addl Info Fields
                                    BindAdditionalInfoGrid(Crew.CrewID);
                                    // Type Ratings Fields
                                    ClearTypeRatingFields();
                                    BindTypeRatingGrid(Crew.CrewID);
                                    using (FlightPakMasterService.MasterCatalogServiceClient AircraftAssignedService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        CrewAircraftAssigned CrewAircraft = new CrewAircraftAssigned();
                                        CrewAircraft.CrewID = Convert.ToInt64(CrewID);
                                        var CrewRosterServiceValues = AircraftAssignedService.GetAircraftAssignedList(CrewAircraft);
                                        var AircraftList = CrewRosterServiceValues.EntityList.Where(x => x.IsDeleted == false).ToList();
                                        Session["AircraftAssignedList"] = (List<CrewAircraftAssigned>)AircraftList;
                                        dgAircraftAssigned.Rebind();
                                    }
                                    chkDisplayInactiveChecklist.Checked = true;
                                    // Checklist Fields
                                    ClearCheckListFields();
                                    BindCrewCheckListGrid(Crew.CrewID);
                                    #region Get Image from Database
                                    CreateDictionayForImgUpload();
                                    imgFile.ImageUrl = "";
                                    ImgPopup.ImageUrl = null;
                                    lnkFileName.NavigateUrl = "";
                                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var ObjRetImg = ObjImgService.GetFileWarehouseList("Crew", Convert.ToInt64(Session["SelectedCrewRosterID"].ToString())).EntityList;
                                        ddlImg.Items.Clear();
                                        ddlImg.Text = "";
                                        int i = 0;
                                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                        {
                                            Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                            string encodedFileame = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.UWAFileName);
                                            ddlImg.Items.Insert(i, new ListItem(encodedFileame, encodedFileame));
                                            dicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                                            if (i == 0)
                                            {
                                                byte[] picture = fwh.UWAFilePath;

                                                //Ramesh: Set the URL for image file or link based on the file type
                                                SetURL(fwh.UWAFileName, picture);

                                                ////start of modification for image issue
                                                //if (hdnBrowserName.Value == "Chrome")
                                                //{
                                                //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                                //}
                                                //else
                                                //{
                                                //    Session["Base64"] = picture;
                                                //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                                //}
                                                ////end of modification for image issue
                                            }
                                            i = i + 1;
                                        }
                                    }
                                    if (ddlImg.Items.Count > 0)
                                    {
                                        ddlImg.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                        lnkFileName.NavigateUrl = "";
                                    }
                                    #endregion
                                    BindCrewVisaGrid(Crew.CrewID);
                                    BindCrewPassportGrid(Crew.CrewID);
                                    BindCrewCurrency(Crew.CrewID);
                                    // Addl Notes Field
                                    tbAdditionalNotes.Text = Crew.Notes2;
                                    //Email Preferences Fields
                                    int count = 0;
                                    int countaddinfo = 0;
                                    if (Crew.BlackBerryTM.ToString().Trim() == "Z")
                                    {
                                        radZULU.Checked = true;
                                        radLocal.Checked = false;
                                    }
                                    else
                                    {
                                        radLocal.Checked = true;
                                        radZULU.Checked = false;
                                    }
                                    chkDepartmentAuthorization.Checked = Convert.ToBoolean(Crew.IsDepartmentAuthorization, CultureInfo.CurrentCulture);
                                    chkRequestorPhone.Checked = Convert.ToBoolean(Crew.IsRequestPhoneNum, CultureInfo.CurrentCulture);
                                    chkAccountNo.Checked = Convert.ToBoolean(Crew.IsAccountNum, CultureInfo.CurrentCulture);
                                    chkCancellationDesc.Checked = Convert.ToBoolean(Crew.IsCancelDescription, CultureInfo.CurrentCulture);
                                    chkStatus.Checked = Convert.ToBoolean(Crew.IsStatusT, CultureInfo.CurrentCulture);
                                    chkAirport.Checked = Convert.ToBoolean(Crew.IsAirport, CultureInfo.CurrentCulture);
                                    chkChecklist.Checked = Convert.ToBoolean(Crew.IsCheckList, CultureInfo.CurrentCulture);
                                    chkRunway.Checked = Convert.ToBoolean(Crew.IsRunway, CultureInfo.CurrentCulture);
                                    chkHomeArrival.Checked = Convert.ToBoolean(Crew.IsHomeArrivalTM, CultureInfo.CurrentCulture);
                                    chkHomeDeparture.Checked = Convert.ToBoolean(Crew.IsHomeDEPARTTM, CultureInfo.CurrentCulture);
                                    chkEndDuty.Checked = Convert.ToBoolean(Crew.IsEndDuty, CultureInfo.CurrentCulture);
                                    chkOverride.Checked = Convert.ToBoolean(Crew.IsOverride, CultureInfo.CurrentCulture);
                                    chkCrewRules.Checked = Convert.ToBoolean(Crew.IsCrewRules, CultureInfo.CurrentCulture);
                                    chkFAR.Checked = Convert.ToBoolean(Crew.IsFAR, CultureInfo.CurrentCulture);
                                    chkDutyHours.Checked = Convert.ToBoolean(Crew.IsDuty, CultureInfo.CurrentCulture);
                                    chkFlightHours.Checked = Convert.ToBoolean(Crew.IsFlightHours, CultureInfo.CurrentCulture);
                                    chkRestHours.Checked = Convert.ToBoolean(Crew.IsRestHrs, CultureInfo.CurrentCulture);
                                    chkAssociatedCrew.Checked = Convert.ToBoolean(Crew.IsAssociatedCrew, CultureInfo.CurrentCulture);
                                    chkNEWSPAPER.Checked = Convert.ToBoolean(Crew.Label2, CultureInfo.CurrentCulture);
                                    chkCOFFEE.Checked = Convert.ToBoolean(Crew.Label1, CultureInfo.CurrentCulture);
                                    chkJUICE.Checked = Convert.ToBoolean(Crew.Label3, CultureInfo.CurrentCulture);
                                    chkDepartureInformation.Checked = Convert.ToBoolean(Crew.IsDepartureFBO, CultureInfo.CurrentCulture);
                                    chkArrivalInformation.Checked = Convert.ToBoolean(Crew.IsArrivalFBO, CultureInfo.CurrentCulture);
                                    chkCrewTransport.Checked = Convert.ToBoolean(Crew.IsCrewDepartTRANS, CultureInfo.CurrentCulture);
                                    chkCrewArrival.Checked = Convert.ToBoolean(Crew.IsCrewArrivalTRANS, CultureInfo.CurrentCulture);
                                    chkHotel.Checked = Convert.ToBoolean(Crew.IsHotel, CultureInfo.CurrentCulture);
                                    chkPassengerTransport.Checked = Convert.ToBoolean(Crew.IsPassDepartTRANS, CultureInfo.CurrentCulture);
                                    chkPassengerArrival.Checked = Convert.ToBoolean(Crew.IsPassArrivalHotel, CultureInfo.CurrentCulture);
                                    chkPassengerDetails.Checked = Convert.ToBoolean(Crew.IsPassDetails, CultureInfo.CurrentCulture);
                                    chkPassengerHotel.Checked = Convert.ToBoolean(Crew.IsPassHotel, CultureInfo.CurrentCulture);
                                    chkPassengerPaxPhone.Checked = Convert.ToBoolean(Crew.IsPassPhoneNum, CultureInfo.CurrentCulture);
                                    chkDepartureCatering.Checked = Convert.ToBoolean(Crew.IsDepartCatering, CultureInfo.CurrentCulture);
                                    chkArrivalCatering.Checked = Convert.ToBoolean(Crew.IsArrivalCatering, CultureInfo.CurrentCulture);
                                    chkArrivalDepartureTime.Checked = Convert.ToBoolean(Crew.IsArrivalDepartTime, CultureInfo.CurrentCulture);
                                    chkAircraft.Checked = Convert.ToBoolean(Crew.IsAircraft, CultureInfo.CurrentCulture);
                                    chkArrivalDepartureICAO.Checked = Convert.ToBoolean(Crew.IcaoID, CultureInfo.CurrentCulture);
                                    chkGeneralCrew.Checked = Convert.ToBoolean(Crew.IsCrew, CultureInfo.CurrentCulture);
                                    chkGeneralPassenger.Checked = Convert.ToBoolean(Crew.IsPassenger, CultureInfo.CurrentCulture);
                                    chkOutBoundInstructions.Checked = Convert.ToBoolean(Crew.IsOutboundINST, CultureInfo.CurrentCulture);
                                    chkGeneralChecklist.Checked = Convert.ToBoolean(Crew.IsCheckList1, CultureInfo.CurrentCulture);
                                    chkLogisticsFBO.Checked = Convert.ToBoolean(Crew.IsFBO, CultureInfo.CurrentCulture);
                                    chkLogisticsHotel.Checked = Convert.ToBoolean(Crew.IsHotel, CultureInfo.CurrentCulture);
                                    chkLogisticsTransportation.Checked = Convert.ToBoolean(Crew.IsTransportation, CultureInfo.CurrentCulture);
                                    chkLogisticsCatering.Checked = Convert.ToBoolean(Crew.IsCatering, CultureInfo.CurrentCulture);
                                    chkCrewHotelConfirm.Checked = Convert.ToBoolean(Crew.IsCrewHotelConfirm, CultureInfo.CurrentCulture);
                                    chkCrewHotelComments.Checked = Convert.ToBoolean(Crew.IsCrewHotelComments, CultureInfo.CurrentCulture);
                                    chkCrewDeptTransConfirm.Checked = Convert.ToBoolean(Crew.IsCrewDepartTRANSConfirm, CultureInfo.CurrentCulture);
                                    chkCrewDeptTransComments.Checked = Convert.ToBoolean(Crew.IsCrewDepartTRANSComments, CultureInfo.CurrentCulture);
                                    chkCrewArrTransConfirm.Checked = Convert.ToBoolean(Crew.IsCrewArrivalConfirm, CultureInfo.CurrentCulture);
                                    chkCrewArrTransComments.Checked = Convert.ToBoolean(Crew.IsCrewArrivalComments, CultureInfo.CurrentCulture);
                                    chkCrewNotes.Checked = Convert.ToBoolean(Crew.IsCrewNotes, CultureInfo.CurrentCulture);
                                    chkPAXHotelConfirm.Checked = Convert.ToBoolean(Crew.IsPassHotelConfirm, CultureInfo.CurrentCulture);
                                    chkPAXHotelComments.Checked = Convert.ToBoolean(Crew.IsPassHotelComments, CultureInfo.CurrentCulture);
                                    chkPAXDeptTransConfirm.Checked = Convert.ToBoolean(Crew.IsPassDepartTRANSConfirm, CultureInfo.CurrentCulture);
                                    chkPAXDeptTransComments.Checked = Convert.ToBoolean(Crew.IsPassDepartTRANSComments, CultureInfo.CurrentCulture);
                                    chkPAXArrTransConfirm.Checked = Convert.ToBoolean(Crew.IsPassArrivalConfirm, CultureInfo.CurrentCulture);
                                    chkPAXArrTransComments.Checked = Convert.ToBoolean(Crew.IsPassArrivalComments, CultureInfo.CurrentCulture);
                                    chkPAXNotes.Checked = Convert.ToBoolean(Crew.IsPassNotes, CultureInfo.CurrentCulture);
                                    chkOutboundInstComments.Checked = Convert.ToBoolean(Crew.IsOutboundComments, CultureInfo.CurrentCulture);
                                    chkDeptAirportNotes.Checked = Convert.ToBoolean(Crew.IsDepartAirportNotes, CultureInfo.CurrentCulture);
                                    chkArrAirportNotes.Checked = Convert.ToBoolean(Crew.IsArrivalAirportNoes, CultureInfo.CurrentCulture);
                                    chkDeptAirportAlerts.Checked = Convert.ToBoolean(Crew.IsDepartAirportAlerts, CultureInfo.CurrentCulture);
                                    chkArrAirportAlerts.Checked = Convert.ToBoolean(Crew.IsArrivalAirportAlerts, CultureInfo.CurrentCulture);
                                    chkArrFBOConfirm.Checked = Convert.ToBoolean(Crew.IsFBOArrivalConfirm, CultureInfo.CurrentCulture);
                                    chkArrFBOComments.Checked = Convert.ToBoolean(Crew.IsFBOArrivalComment, CultureInfo.CurrentCulture);
                                    chkDeptFBOConfirm.Checked = Convert.ToBoolean(Crew.IsFBODepartConfirm, CultureInfo.CurrentCulture);
                                    chkDeptFBOComments.Checked = Convert.ToBoolean(Crew.IsFBODepartComment, CultureInfo.CurrentCulture);
                                    chkDeptCaterConfirm.Checked = Convert.ToBoolean(Crew.IsDepartCateringConfirm, CultureInfo.CurrentCulture);
                                    chkDeptCaterComments.Checked = Convert.ToBoolean(Crew.IsDepartCateringComment, CultureInfo.CurrentCulture);
                                    chkArrCaterConfirm.Checked = Convert.ToBoolean(Crew.IsArrivalCateringConfirm, CultureInfo.CurrentCulture);
                                    chkArrCaterComments.Checked = Convert.ToBoolean(Crew.IsArrivalCateringComment, CultureInfo.CurrentCulture);
                                    chkTripAlerts.Checked = Convert.ToBoolean(Crew.IsTripAlerts, CultureInfo.CurrentCulture);
                                    chkTripNotes.Checked = Convert.ToBoolean(Crew.IsTripNotes, CultureInfo.CurrentCulture);
                                    chkIsTripEmail.Checked = Convert.ToBoolean(Crew.IsTripEmail, CultureInfo.CurrentCulture);

                                    tbBirthday.Text = (Crew.Birthday != null) ? String.Format("{0:" + DateFormat + "}", Crew.Birthday) : string.Empty;
                                    tbAnnivesaries.Text = (Crew.Anniversaries != null) ? String.Format("{0:" + DateFormat + "}", Crew.Anniversaries) : string.Empty;

                                    tbHotelPreferences.Text = (Crew.HotelPreferences != null) ? Crew.HotelPreferences : string.Empty;
                                    tbEmergencyContact.Text = (Crew.EmergencyContacts != null) ? Crew.EmergencyContacts : string.Empty;
                                    tbCreditCardInfo.Text = (Crew.CreditcardInfo != null) ? Crew.CreditcardInfo : string.Empty;
                                    tbCateringPreferences.Text = (Crew.CateringPreferences != null) ? Crew.CateringPreferences : string.Empty;

                                    if (chkDepartmentAuthorization.Checked == true)
                                        count = count + 1;
                                    if (chkRequestorPhone.Checked == true)
                                        count = count + 1;
                                    if (chkAccountNo.Checked == true)
                                        count = count + 1;
                                    if (chkCancellationDesc.Checked == true)
                                        count = count + 1;
                                    if (chkStatus.Checked == true)
                                        count = count + 1;
                                    if (chkAirport.Checked == true)
                                        count = count + 1;
                                    if (chkChecklist.Checked == true)
                                        count = count + 1;
                                    if (chkRunway.Checked == true)
                                        count = count + 1;
                                    if (chkHomeArrival.Checked == true)
                                        count = count + 1;
                                    if (chkHomeDeparture.Checked == true)
                                        count = count + 1;
                                    if (chkLogisticsCatering.Checked == true)
                                        count = count + 1;
                                    if (chkLogisticsTransportation.Checked == true)
                                        count = count + 1;
                                    if (chkLogisticsHotel.Checked == true)
                                        count = count + 1;
                                    if (chkLogisticsFBO.Checked == true)
                                        count = count + 1;
                                    if (chkGeneralChecklist.Checked == true)
                                        count = count + 1;
                                    if (chkOutBoundInstructions.Checked == true)
                                        count = count + 1;
                                    if (chkGeneralPassenger.Checked == true)
                                        count = count + 1;
                                    if (chkGeneralCrew.Checked == true)
                                        count = count + 1;
                                    if (chkArrivalDepartureICAO.Checked == true)
                                        count = count + 1;
                                    if (chkAircraft.Checked == true)
                                        count = count + 1;
                                    if (chkArrivalDepartureTime.Checked == true)
                                        count = count + 1;
                                    if (chkArrivalCatering.Checked == true)
                                        count = count + 1;
                                    if (chkDepartureCatering.Checked == true)
                                        count = count + 1;
                                    if (chkPassengerPaxPhone.Checked == true)
                                        count = count + 1;
                                    if (chkPassengerHotel.Checked == true)
                                        count = count + 1;
                                    if (chkPassengerDetails.Checked == true)
                                        count = count + 1;
                                    if (chkEndDuty.Checked == true)
                                        count = count + 1;
                                    if (chkOverride.Checked == true)
                                        count = count + 1;
                                    if (chkCrewRules.Checked == true)
                                        count = count + 1;
                                    if (chkFAR.Checked == true)
                                        count = count + 1;
                                    if (chkDutyHours.Checked == true)
                                        count = count + 1;
                                    if (chkFlightHours.Checked == true)
                                        count = count + 1;
                                    if (chkRestHours.Checked == true)
                                        count = count + 1;
                                    if (chkAssociatedCrew.Checked == true)
                                        count = count + 1;
                                    if (chkNEWSPAPER.Checked == true)
                                        count = count + 1;
                                    if (chkCOFFEE.Checked == true)
                                        count = count + 1;
                                    if (chkJUICE.Checked == true)
                                        count = count + 1;
                                    if (chkDepartureInformation.Checked == true)
                                        count = count + 1;
                                    if (chkArrivalInformation.Checked == true)
                                        count = count + 1;
                                    if (chkCrewTransport.Checked == true)
                                        count = count + 1;
                                    if (chkCrewArrival.Checked == true)
                                        count = count + 1;
                                    if (chkHotel.Checked == true)
                                        count = count + 1;
                                    if (chkPassengerTransport.Checked == true)
                                        count = count + 1;
                                    if (chkPassengerArrival.Checked == true)
                                        count = count + 1;

                                    if (chkTripNotes.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkTripAlerts.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkArrCaterComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkArrCaterConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkDeptCaterComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkDeptCaterConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkDeptFBOComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkDeptFBOConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkArrFBOComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkArrFBOConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkArrAirportAlerts.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkDeptAirportAlerts.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkArrAirportNotes.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkPAXArrTransComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkPAXNotes.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkOutboundInstComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkDeptAirportNotes.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkPAXArrTransConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkPAXDeptTransComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkPAXDeptTransConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkPAXHotelComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkPAXHotelConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkCrewNotes.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkCrewArrTransComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkCrewArrTransConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkCrewHotelConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkCrewHotelComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkCrewDeptTransConfirm.Checked == true)
                                        countaddinfo = countaddinfo + 1;
                                    if (chkCrewDeptTransComments.Checked == true)
                                        countaddinfo = countaddinfo + 1;

                                    if ((countaddinfo + count) == 47)
                                    {
                                        chkSelectAll.Checked = true;
                                    }
                                    else
                                    {
                                        chkSelectAll.Checked = false;
                                    }
                                }
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Check / Uncheck based on the selected value
        /// </summary>
        /// <param name="Enable">Pass Boolean value</param>
        private void EnableEmailPreferences(bool Enable)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //General Section
                    chkDepartmentAuthorization.Checked = Enable;
                    chkRequestorPhone.Checked = Enable;
                    chkAccountNo.Checked = Enable;
                    chkCancellationDesc.Checked = Enable;
                    chkStatus.Checked = Enable;
                    chkAirport.Checked = Enable;
                    chkRunway.Checked = Enable;
                    chkChecklist.Checked = Enable;
                    chkHomeArrival.Checked = Enable;
                    chkHomeDeparture.Checked = Enable;
                    radZULU.Checked = Enable;
                    chkEndDuty.Checked = Enable;
                    chkOverride.Checked = Enable;
                    chkFAR.Checked = Enable;
                    chkCrewRules.Checked = Enable;
                    chkDutyHours.Checked = Enable;
                    chkRestHours.Checked = Enable;
                    chkFlightHours.Checked = Enable;
                    chkAssociatedCrew.Checked = Enable;
                    chkNEWSPAPER.Checked = Enable;
                    chkCOFFEE.Checked = Enable;
                    chkJUICE.Checked = Enable;
                    //FBO Handler
                    chkDepartureInformation.Checked = Enable;
                    chkArrivalInformation.Checked = Enable;
                    //Crew 
                    chkCrewTransport.Checked = Enable;
                    chkCrewArrival.Checked = Enable;
                    chkHotel.Checked = Enable;
                    //Passenger
                    chkPassengerTransport.Checked = Enable;
                    chkPassengerArrival.Checked = Enable;
                    chkPassengerDetails.Checked = Enable;
                    chkPassengerHotel.Checked = Enable;
                    chkPassengerPaxPhone.Checked = Enable;
                    //Catering
                    chkDepartureCatering.Checked = Enable;
                    chkArrivalCatering.Checked = Enable;
                    //General
                    chkArrivalDepartureTime.Checked = Enable;
                    chkAircraft.Checked = Enable;
                    chkArrivalDepartureICAO.Checked = Enable;
                    chkGeneralCrew.Checked = Enable;
                    chkGeneralPassenger.Checked = Enable;
                    chkOutBoundInstructions.Checked = Enable;
                    chkGeneralChecklist.Checked = Enable;
                    //Logistics
                    chkLogisticsFBO.Checked = Enable;
                    chkLogisticsHotel.Checked = Enable;
                    chkLogisticsTransportation.Checked = Enable;
                    chkLogisticsCatering.Checked = Enable;
                    chkCrewHotelConfirm.Checked = Enable;
                    chkCrewHotelComments.Checked = Enable;
                    chkCrewDeptTransConfirm.Checked = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Check Exists Methods"
        private bool CheckCountryExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx, cval))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetCountryMasterList().EntityList.Where(x => Convert.ToString(x.CountryCD.Trim().ToUpper()) == Convert.ToString(txtbx.Text.Trim().ToUpper())).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                CountryList = (List<FlightPakMasterService.Country>)objRetVal.ToList();
                                if (txtbx.ID.Equals("tbCountry"))
                                {
                                    hdnCountryID.Value = Convert.ToString(CountryList[0].CountryID);
                                    tbCountry.Text = Convert.ToString(CountryList[0].CountryCD);
                                }
                                else if (txtbx.ID.Equals("tbCountryBirth"))
                                {
                                    hdnResidenceCountryID.Value = Convert.ToString(CountryList[0].CountryID);
                                    tbCountryBirth.Text = Convert.ToString(CountryList[0].CountryCD);
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbHomePhone);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbHomePhone.ClientID + "');", true);
                                }
                                else if (txtbx.ID.Equals("tbCitizenCode"))
                                {
                                    hdnCitizenshipID.Value = Convert.ToString(CountryList[0].CountryID);
                                    lbCitizenDesc.Text = System.Web.HttpUtility.HtmlEncode(Convert.ToString(CountryList[0].CountryName));
                                    tbCitizenCode.Text = Convert.ToString(CountryList[0].CountryCD);
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbDateOfBirth);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDateOfBirth.ClientID + "');", true);
                                }
                                else if (txtbx.ID.Equals("tbLicCountry"))
                                {
                                    hdnLicCountryID.Value = Convert.ToString(CountryList[0].CountryID);
                                    tbLicCountry.Text = Convert.ToString(CountryList[0].CountryCD);
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbAddLic);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbAddLic.ClientID + "');", true);
                                }
                                else if (txtbx.ID.Equals("tbGreenNationality"))
                                {
                                    hdnGreenNationality.Value = CountryList[0].CountryID.ToString();
                                    tbGreenNationality.Text = CountryList[0].CountryCD;
                                }
                                else if (txtbx.ID.Equals("tbAddLicCountry"))
                                {
                                    hdnAddLicCountryID.Value = Convert.ToString(CountryList[0].CountryID);
                                    tbAddLicCountry.Text = Convert.ToString(CountryList[0].CountryCD);
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbNotes);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbNotes.ClientID + "');", true);
                                }
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(txtbx);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckCrewCodeAlreadyExists() 	
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (CompanySett.IsAutoCrew != true)
                    {
                        if ((tbCrewCode.Text != string.Empty) && (tbCrewCode.Text != null))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var crewValueFilter = ClientService.GetAllCrewWithFilters(0, false, false, false, null, 0, 0, tbCrewCode.Text.Trim()).EntityList;
                                if (crewValueFilter.Count() > 0 && crewValueFilter != null)
                                {
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewCode);
                                    RetVal = true;
                                }
                            }
                        }
                    }                  
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        /// <summary>
        /// To check if valid client code exists or not
        /// </summary>
        /// <returns></returns>
        private bool CheckClientCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClientCode.Text != string.Empty) && (tbClientCode.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValueFilter = ClientService.GetClientCodeList().EntityList.Where(x => (x.ClientCD != null));
                            var ClientValue = ClientValueFilter.Where(x => x.ClientCD.Trim().ToUpper().Equals(Convert.ToString(tbClientCode.Text).ToUpper().Trim()));
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                cvClientCode.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                                RetVal = true;
                            }
                            else
                            {
                                foreach (FlightPakMasterService.Client cm in ClientValue)
                                {
                                    hdnClientID.Value = Convert.ToString(cm.ClientID);
                                    tbClientCode.Text = cm.ClientCD;
                                }
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDepartment.ClientID + "');", true);
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        private bool CheckClientFilterCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClientCodeFilter.Text.Trim() != string.Empty) && (tbClientCodeFilter.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValueFilter = ClientService.GetClientCodeList().EntityList.Where(x => (x.ClientCD != null));

                            var ClientValue = ClientValueFilter.Where(x => x.ClientCD.Trim().ToUpper().Equals(Convert.ToString(tbClientCodeFilter.Text).ToUpper().Trim())).ToList();
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                RetVal = true;
                            }
                            else
                            {
                                tbClientCodeFilter.Text = ((FlightPakMasterService.Client)ClientValue[0]).ClientCD;
                                RetVal = false;
                            }
                        }
                    }
                    if ((hdnClientFilterID.Value != string.Empty) && (hdnClientFilterID.Value != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValueFilter = ClientService.GetClientCodeList().EntityList.Where(x => (x.ClientID != null));

                            var ClientValue = ClientValueFilter.Where(x => x.ClientID == Convert.ToInt64(hdnClientFilterID.Value)).ToList();
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                RetVal = true;
                            }
                            else
                            {
                                tbClientCodeFilter.Text = ((FlightPakMasterService.Client)ClientValue[0]).ClientCD;
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        private bool CheckClientFilterCodeExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((hdnClientID.Value != string.Empty) && (hdnClientID.Value != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValueFilter = ClientService.GetClientCodeList().EntityList.Where(x => (x.ClientID != null));

                            var ClientValue = ClientValueFilter.Where(x => x.ClientID == Convert.ToInt64(hdnClientID.Value)).ToList();
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                RetVal = true;
                            }
                            else
                            {
                                tbClientCodeFilter.Text = ((FlightPakMasterService.Client)ClientValue[0]).ClientCD;
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }


        private bool CheckCrewGroupFilterCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbCrewGroupFilter.Text.Trim() != string.Empty) && (tbCrewGroupFilter.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValue = ClientService.GetCrewGroupInfo().EntityList.Where(x => x.CrewGroupCD.Trim().ToUpper().Equals(Convert.ToString(tbCrewGroupFilter.Text).ToUpper().Trim())).ToList();
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                RetVal = true;
                            }
                            else
                            {
                                hdnCrewGroupID.Value = Convert.ToString(ClientValue[0].CrewGroupID);
                                tbCrewGroupFilter.Text = ClientValue[0].CrewGroupCD;
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        /// To check if valid Department code exists or not
        /// </summary>
        /// <returns></returns>
        private bool CheckDepartmentCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbDepartment.Text != string.Empty) && (tbDepartment.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var DepartmentValue = DepartmentService.GetAllDeptAuthorizationList().EntityList.Where(x => x.DepartmentCD.Trim().ToUpper().Equals(Convert.ToString(tbDepartment.Text).ToUpper().Trim()));
                            if (DepartmentValue.Count() == 0 || DepartmentValue == null)
                            {
                                cvDepartmentCode.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
                                RetVal = true;
                            }
                            else
                            {
                                foreach (FlightPakMasterService.GetAllDeptAuth cm in DepartmentValue)
                                {
                                    hdnDepartmentID.Value = Convert.ToString(cm.DepartmentID);
                                    tbDepartment.Text = cm.DepartmentCD;
                                }
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbFirstName);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbFirstName.ClientID + "');", true);
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

                return RetVal;
            }
        }


        /// <summary>
        /// To check if valid homebase code exists or not
        /// </summary>
        /// <returns></returns>
        private bool CheckHomeBaseExist()
        {
            if ((tbHomeBase.Text != string.Empty) && (tbHomeBase.Text != null))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient HomeBaseService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var HomeBaseValue = HomeBaseService.GetAirportByAirportICaoID(tbHomeBase.Text).EntityList;
                    if (HomeBaseValue.Count() == 0 || HomeBaseValue == null)
                    {
                        cvHomeBase.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                        return true;
                    }
                    else
                    {
                        foreach (FlightPakMasterService.GetAllAirport cm in HomeBaseValue)
                        {
                            hdnHomeBaseID.Value = Convert.ToString(cm.AirportID);
                            tbHomeBase.Text = cm.IcaoID;
                        }
                        return false;
                    }
                }
            }
            return false;
        }
        #endregion
        #region "AJAX Events"
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1) || (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1))
                        {
                            e.Updated = dgCrewRoster;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Define the method of IPostBackEventHandler that raises change events.
        /// </summary>
        /// <param name="sourceControl"></param>
        /// <param name="eventArgument"></param>
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sourceControl, eventArgument))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.RaisePostBackEvent(sourceControl, eventArgument);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        /// <summary>
        /// Rebind the popup values in the corresponding grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                // Additional Info
                                // Declaring Lists
                                List<GetCrewDefinition> FinalList = new List<GetCrewDefinition>();
                                List<GetCrewDefinition> RetainList = new List<GetCrewDefinition>();
                                List<GetCrewDefinition> NewList = new List<GetCrewDefinition>();
                                // Retain Grid Items and bind into List and add into Final List
                                RetainList = RetainCrewDefinitionGrid(dgCrewAddlInfo);
                                FinalList.AddRange(RetainList);
                                if (Session["CrewNewAddlInfo"] != null)
                                {
                                    // Bind Selected New Item and add into Final List
                                    NewList = (List<GetCrewDefinition>)Session["CrewNewAddlInfo"];
                                    FinalList.AddRange(NewList);
                                }
                                // Bind final list into Session
                                Session["CrewAddInfo"] = FinalList;
                                // Bind final list into Grid
                                dgCrewAddlInfo.DataSource = FinalList.Where(x => x.IsDeleted == false).ToList();
                                dgCrewAddlInfo.DataBind();
                                // Empty Newly added Session item
                                Session.Remove("CrewNewAddlInfo");
                                // Type Rating
                                // Declaring Lists
                                List<GetCrewRating> FinalList1 = new List<GetCrewRating>();
                                List<GetCrewRating> RetainList1 = new List<GetCrewRating>();
                                List<GetCrewRating> NewList1 = new List<GetCrewRating>();
                                // Retain Grid Items and bind into List and add into Final List
                                RetainList1 = RetainCrewTypeRatingGrid(dgTypeRating);
                                FinalList1.AddRange(RetainList1);
                                if (Session["CrewNewTypeRating"] != null)
                                {
                                    // Bind Selected New Item and add into Final List
                                    NewList1 = (List<GetCrewRating>)Session["CrewNewTypeRating"];
                                    FinalList1.InsertRange(0, NewList1);
                                }
                                // Bind final list into Session
                                Session["CrewTypeRating"] = FinalList1;
                                // Bind final list into Grid
                                dgTypeRating.DataSource = FinalList1.Where(x => x.IsDeleted == false).ToList();
                                dgTypeRating.DataBind();
                                // Empty Newly added Session item
                                Session.Remove("CrewNewTypeRating");
                                // Crew Check List
                                // Declaring Lists
                                List<GetCrewCheckListDate> FinalList2 = new List<GetCrewCheckListDate>();
                                List<GetCrewCheckListDate> RetainList2 = new List<GetCrewCheckListDate>();
                                List<GetCrewCheckListDate> NewList2 = new List<GetCrewCheckListDate>();
                                // Retain Grid Items and bind into List and add into Final List
                                RetainList2 = RetainCrewCheckListGrid(dgCrewCheckList);
                                FinalList2.AddRange(RetainList2);
                                if (Session["CrewNewCheckList"] != null)
                                {
                                    // Bind Selected New Item and add into Final List
                                    NewList2 = (List<GetCrewCheckListDate>)Session["CrewNewCheckList"];
                                    FinalList2.InsertRange(0, NewList2);
                                }
                                Session["CrewCheckList"] = FinalList2;
                                dgCrewCheckList.DataSource = FinalList2.Where(x => x.IsDeleted == false).ToList();
                                dgCrewCheckList.DataBind();
                                Session.Remove("CrewNewCheckList");
                                DefaultChecklistSelection();
                                DefaultTypeRatingSelection();
                                if (!String.IsNullOrEmpty(lbTypeCode.Text) && (hdnSave.Value.ToUpper() == "UPDATE" || hdnSave.Value.ToUpper() == "SAVE"))
                                {
                                    CheckTypeRating(true);
                                }

                                //Case for Switch Operation
                                if (Session["NewCrewRosterSelectID"] != null)
                                {
                                    dgCrewRoster.Rebind();
                                    ReadOnlyForm();
                                    DisplayEditForm();
                                    lbWarningMessage.Visible = false;
                                    Session.Remove("NewCrewRosterSelectID");
                                }
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }
        #endregion
        #region Filters
        public List<GetAllCrewWithFilters> ReorderCrew(List<GetAllCrewWithFilters> CrewList, long CustomerID, long CrewGroupID)
        {
            List<GetAllCrewWithFilters> GetCrew = new List<GetAllCrewWithFilters>();
            using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var ReturnValue = CrewService.GetcrewselectedList(CustomerID, CrewGroupID);
                if (ReturnValue != null && ReturnValue.EntityList != null && ReturnValue.EntityList.Count > 0)
                {
                    int Count = ReturnValue.EntityList.Count;
                    for (int i = 0; i < Count; i++)
                    {
                        foreach (GetAllCrewWithFilters crew in CrewList)
                        {
                            if (crew.CrewID == ReturnValue.EntityList[i].CrewID)
                            {
                                GetCrew.Add(crew);
                            }
                        }
                    }
                }
            }

            return GetCrew;
        }

        public bool DoSearchfilter(bool IsDatabind)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient Service1 = new FlightPakMasterService.MasterCatalogServiceClient())
            {

                if (((CheckCrewGroupFilterCodeExist() == false) && (CheckClientFilterCodeExist() == false)))
                {
                    List<GetAllCrewWithFilters> FilterCrew = new List<GetAllCrewWithFilters>();
                    bool IsInActive = false;
                    bool IsRotaryWing = false;
                    bool IsFixedWing = false;
                    long CrewID = 0;
                    long ClientID = 0;
                    long CrewGroupID = 0;
                    string CrewCD = string.Empty;
                    string ClientCode = string.Empty;
                    string CrewGroupCD = string.Empty;
                    string ICAO = string.Empty;

                    if (chkActiveOnly.Checked)
                    {
                        IsInActive = true;
                    }
                    if (chkHomeBaseOnly.Checked)
                    {
                        ICAO = UserPrincipal.Identity._airportICAOCd;
                    }
                    if (chkFixedWingOnly.Checked)
                    {
                        IsFixedWing = true;
                    }
                    if (chkRotaryWingOnly.Checked)
                    {
                        IsRotaryWing = true;
                    }

                    if (tbClientCodeFilter.Text.Trim() != "")
                    {
                        ClientCode = tbClientCodeFilter.Text.ToString().ToUpper().Trim();
                        if (hdnClientFilterID.Value != string.Empty)
                            ClientID = Convert.ToInt64(hdnClientFilterID.Value);
                    }

                    if (tbCrewGroupFilter.Text.Trim() != "")
                    {
                        CrewGroupCD = tbCrewGroupFilter.Text.ToString().ToUpper().Trim();
                        if (hdnCrewGroupID.Value != string.Empty)
                            CrewGroupID = Convert.ToInt64(hdnCrewGroupID.Value);
                    }
                    FilterCrew = Service1.GetAllCrewWithFilters(ClientID, IsInActive, IsRotaryWing, IsFixedWing, ICAO, CrewGroupID, CrewID, CrewCD).EntityList.ToList();

                    if (CrewGroupID != 0)
                    {
                        FilterCrew = ReorderCrew(FilterCrew, Convert.ToInt64(UserPrincipal.Identity._customerID), CrewGroupID);
                    }
                    dgCrewRoster.DataSource = FilterCrew;
                    if (IsDatabind)
                    {
                        dgCrewRoster.DataBind();
                    }
                }

            }
            return false;
        }


        protected void ActiveOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchfilter(true);
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void HomeBaseOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchfilter(true);
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void FixedWingOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchfilter(true);
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void RotaryWing_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchfilter(true);
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void CrewGroupFilter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnCrewGroupID.Value = "";
                        if (CheckCrewGroupFilterCodeExist())
                        {
                            lbCrewGroupCodeFilter.Visible = true;
                            lbCrewGroupCodeFilter.ForeColor = System.Drawing.Color.Red;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewGroupFilter);
                        }
                        else
                        {
                            lbCrewGroupCodeFilter.Visible = false;
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if ((tbCrewGroupFilter.Text != string.Empty))
                                {
                                    if (hdnCrewGroupID.Value != string.Empty)
                                    {
                                        DoSearchfilter(true);
                                        DefaultSelection(true);
                                    }
                                }
                                if ((tbCrewGroupFilter.Text == string.Empty))
                                {
                                    DoSearchfilter(true);
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void HireDT_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTermDT);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbTermDT.ClientID + "');", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void TermDT_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbEmergencyContact);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbEmergencyContact.ClientID + "');", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        protected void Birthday_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbCityBirth);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCityBirth.ClientID + "');", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void ClientCodeFilter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //hdnClientFilterID.Value = string.Empty;
                        //tbClientCodeFilter.Text = string.Empty;
                        if (tbClientCodeFilter.Text != string.Empty)
                        {
                            if (CheckClientFilterCodeExist())
                            {
                                lbClientCodeFilter.Visible = true;
                                lbClientCodeFilter.ForeColor = System.Drawing.Color.Red;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCodeFilter);
                            }
                            else
                            {
                                lbClientCodeFilter.Visible = false;
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    if ((tbClientCodeFilter.Text.Trim() != string.Empty))
                                    {
                                        DoSearchfilter(true);
                                        DefaultSelection(true);
                                    }
                                    if ((tbClientCodeFilter.Text.Trim() == string.Empty))
                                    {
                                        DoSearchfilter(true);
                                        DefaultSelection(true);
                                    }
                                }
                            }
                        }
                        else
                        {
                            hdnClientFilterID.Value = "";
                            lbClientCodeFilter.Visible = false;
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {

                                DoSearchfilter(true);
                                DefaultSelection(true);

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void ClientCodeFilters_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckClientFilterCodeExists())
                        {
                            lbClientCodeFilter.Visible = true;
                            lbClientCodeFilter.ForeColor = System.Drawing.Color.Red;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCodeFilter);
                        }
                        else
                        {
                            lbClientCodeFilter.Visible = false;
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if ((tbClientCodeFilter.Text.Trim() != string.Empty))
                                {
                                    DoSearchfilter(true);
                                    DefaultSelection(true);
                                }
                                if ((tbClientCodeFilter.Text.Trim() == string.Empty))
                                {
                                    DoSearchfilter(true);
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        #endregion
        #region "Common"
        private DateTime FormatDateTime(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                string[] TimeFormat = { "hh", "mm", "ss" };
                string[] SplitTime = new string[3];
                if (DateFormat.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateFormat.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateFormat.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                    SplitTime = DateAndTime[1].Split(':');
                }
                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                    SplitTime = DateAndTime[1].Split(':');
                }
                int dd = 0, mm = 0, yyyy = 0, hh = 0, MM = 0, ss = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                for (int Index = 0; Index < TimeFormat.Count(); Index++)
                {
                    if (TimeFormat[Index].ToLower().Contains("hh"))
                    {
                        hh = Convert.ToInt32(SplitTime[Index]);
                    }
                    if (TimeFormat[Index].ToLower().Contains("mm"))
                    {
                        MM = Convert.ToInt32(SplitTime[Index]);
                    }
                    //if (TimeFormat[Index].ToLower().Contains("ss"))
                    //{
                    //    ss = Convert.ToInt32(SplitTime[Index]);
                    //}
                }
                return new DateTime(yyyy, mm, dd, hh, MM, ss);
            }
        }
        private void DefaultTypeRatingSelection()
        {
            if (hdnTemp.Value == "On")
            {
                if (hdnSave.Value == "Update")
                {
                    if (dgTypeRating.Items.Count > 0)
                    {
                        dgTypeRating.SelectedIndexes.Add(0);
                        BindSelectedTypeRatingItem();
                        btnDeleteRating.Enabled = true;
                        EnableHours(true);
                        EnableForm(true);
                    }
                    else
                    {
                        EnableHours(false);
                        btnDeleteRating.Enabled = false;
                        CheckUncheckTypeRating(false);
                        CheckTypeRating(false);

                    }
                }
                else
                {
                    if (dgTypeRating.Items.Count > 0)
                    {
                        dgTypeRating.SelectedIndexes.Add(0);
                        BindSelectedTypeRatingItem();
                        btnDeleteRating.Enabled = true;
                        EnableHours(true);
                    }
                    else
                    {
                        CheckUncheckTypeRating(false);
                        EnableHours(false);
                        btnDeleteRating.Enabled = false;
                    }

                }
            }
            else
            {
                if (dgTypeRating.Items.Count > 0)
                {
                    dgTypeRating.SelectedIndexes.Add(0);
                    BindSelectedTypeRatingItem();
                    btnDeleteRating.Enabled = false;
                    EnableHours(false);
                    EnableForm(false);
                }
                else
                {
                    CheckUncheckTypeRating(false);
                    EnableHours(false);
                    btnDeleteRating.Enabled = false;
                }
            }
        }
        private void DefaultChecklistSelection()
        {

            if (hdnTemp.Value == "On")
            {
                if (hdnSave.Value == "Update" && hdnDelete.Value != "Yes")
                {
                    if (dgCrewCheckList.Items.Count > 0)
                    {
                        //Edited
                        int index = Convert.ToInt32(HttpContext.Current.Session["index"]);
                        dgCrewCheckList.SelectedIndexes.Add(index);

                        BindSelectedCrewChecklistItem();
                        EnableCheckListFields(true);
                        btnDeleteChecklist.Enabled = true;
                    }
                    else
                    {
                        EnableCheckListFields(false);
                        btnDeleteChecklist.Enabled = false;
                    }
                }
                else
                {
                    if (dgCrewCheckList.Items.Count > 0)
                    {
                        dgCrewCheckList.SelectedIndexes.Add(0);
                        BindSelectedCrewChecklistItem();
                        EnableCheckListFields(true);
                        btnDeleteChecklist.Enabled = true;
                    }
                    else
                    {
                        EnableCheckListFields(false);
                        btnDeleteChecklist.Enabled = false;
                    }
                }
            }
            else
            {
                if (dgCrewCheckList.Items.Count > 0)
                {
                    dgCrewCheckList.SelectedIndexes.Add(0);
                    BindSelectedCrewChecklistItem();
                    EnableCheckListFields(false);
                    btnDeleteChecklist.Enabled = true;
                }
                else
                {
                    EnableCheckListFields(false);
                    btnDeleteChecklist.Enabled = false;
                }
            }
            //}

        }
        private void DefaultFillChecklist()
        {
            if (UserPrincipal.Identity._fpSettings._IsChecklistAssign == true)
            {
                if (Session["CheckListSetting"] != null)
                {
                    List<GetCrewCheckListDate> AutoChecklistFill = new List<GetCrewCheckListDate>();
                    List<FlightPakMasterService.CrewCheckList> AutoChecklist = new List<FlightPakMasterService.CrewCheckList>();
                    AutoChecklist = (List<FlightPakMasterService.CrewCheckList>)Session["CheckListSetting"];
                    for (int Index = 0; Index < AutoChecklist.Count; Index++)
                    {
                        GetCrewCheckListDate getcrewcheck = new GetCrewCheckListDate();
                        getcrewcheck.CrewID = Convert.ToInt64("0");
                        getcrewcheck.CheckListID = Convert.ToInt64("0");
                        getcrewcheck.CrewChecklistDescription = AutoChecklist[Index].CrewChecklistDescription;
                        getcrewcheck.CheckListCD = AutoChecklist[Index].CrewCheckCD;
                        getcrewcheck.GraceDays = Convert.ToInt32("0");
                        getcrewcheck.AlertDays = Convert.ToInt32("0");
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays != null)
                        {
                            getcrewcheck.AlertDays = Convert.ToInt32(UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays);
                        }
                        getcrewcheck.FrequencyMonth = Convert.ToInt32("0");
                        getcrewcheck.TotalREQFlightHrs = Convert.ToDecimal("0.0");
                        AutoChecklistFill.Add(getcrewcheck);
                    }
                    dgCrewCheckList.DataSource = AutoChecklistFill;
                    dgCrewCheckList.DataBind();
                    Session["CrewCheckList"] = AutoChecklistFill;
                    dgCurrencyChecklist.DataSource = AutoChecklistFill;
                    dgCurrencyChecklist.DataBind();
                }

            }
        }
        private void DefaultFillChecklistEdit()
        {
            if (Session["CheckListSetting"] == null)
            {
                List<FlightPakMasterService.CrewCheckList> AutoChecklist = new List<FlightPakMasterService.CrewCheckList>();
                using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                {
                    var objDefaultFillCheckonEdit = CrewRosterService.GetCrewChecklistList();
                    if (objDefaultFillCheckonEdit.ReturnFlag == true)
                    {
                        AutoChecklist = objDefaultFillCheckonEdit.EntityList.ToList<FlightPakMasterService.CrewCheckList>();
                    }
                    Session["CheckListSetting"] = AutoChecklist;
                }
            }
        }
        private FlightPakMasterService.Crew UpdateFutureTripsheet(FlightPakMasterService.Crew oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Crew>(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                    {
                        List<GetAllCrewPassport> PassengerPassportList = new List<GetAllCrewPassport>();
                        CrewPassengerPassport PassengerPassport = new CrewPassengerPassport();
                        PassengerPassport.CrewID = oPassenger.CrewID;
                        PassengerPassport.PassengerRequestorID = null;
                        var objPassengerPassport = objService.GetCrewPassportListInfo(PassengerPassport);
                        if (objPassengerPassport.ReturnFlag == true)
                        {
                            PassengerPassportList = objPassengerPassport.EntityList.Where(x => x.CrewID == oPassenger.CrewID && x.IsDeleted == false).ToList();
                        }

                        Int64 PassportID = 0;
                        for (int Index = 0; Index < PassengerPassportList.Count; Index++)
                        {
                            PassportID = PassengerPassportList[0].PassportID;
                            if ((PassengerPassportList[Index].IsDeleted == false) && (PassengerPassportList[Index].Choice == true))
                            {
                                PassportID = PassengerPassportList[Index].PassportID;
                                break;
                            }
                        }
                        if (hdnIsPassportChoice.Value == "Yes")
                        {
                            if (dgCrewRoster.Items.Count != 0)
                            {
                                objService.UpdatePassportForFutureTrip(PassportID, 0, oPassenger.CrewID);
                            }
                            else if (dgCrewRoster.Items.Count == 0)
                            {
                                objService.UpdatePassportForFutureTrip(-1, 0, oPassenger.CrewID);
                            }
                            //Commented by Karthik
                            //Session["CurrentPreFlightTrip"] = null;
                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                PreflightService.PreflightMain Trip = (PreflightService.PreflightMain)Session["CurrentPreFlightTrip"];
                                if (Trip.PreflightLegs != null)
                                {
                                    foreach (PreflightService.PreflightLeg Leg in Trip.PreflightLegs)
                                    {
                                        if (Leg.PreflightCrewLists != null)
                                        {
                                            foreach (PreflightService.PreflightCrewList PrefCrew in Leg.PreflightCrewLists)
                                            {
                                                if (PrefCrew.CrewID == (long)oPassenger.CrewID)
                                                {
                                                    PrefCrew.PassportID = PassportID;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void ZULU_checkchanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string Time = radZULU.Checked ? radZULU.Text : radLocal.Checked ? radLocal.Text : string.Empty;
                        string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                            + @" oManager.radalert('E-mail sent will now be in " + Time + " time. E-mail sent prior to this change may be inconsistent.', 360, 50, 'Crew Roster Catalog');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion

        #region "Retain Grid Events"
        /// <summary>
        /// Method to Retain Additional Info Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Definition List</returns>
        private List<GetCrewDefinition> RetainCrewDefinitionGrid(RadGrid grid)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<GetCrewDefinition> CrewDefinitionList = new List<GetCrewDefinition>();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.GetCrewDefinition>>(() =>
                {
                    List<FlightPakMasterService.GetCrewDefinition> CrewRetainDefinition = new List<FlightPakMasterService.GetCrewDefinition>();
                    CrewRetainDefinition = (List<FlightPakMasterService.GetCrewDefinition>)Session["CrewAddInfo"];
                    List<FlightPakMasterService.GetCrewDefinition> CrewRetainDefinitionList = new List<FlightPakMasterService.GetCrewDefinition>();
                    for (int Index = 0; Index < CrewRetainDefinition.Count; Index++)
                    {
                        foreach (GridDataItem item in grid.MasterTableView.Items)
                        {

                            if (item.GetDataKeyValue("CrewInfoCD").ToString() == CrewRetainDefinition[Index].CrewInfoCD)
                            {
                                CrewRetainDefinition[Index].CrewID = CrewRetainDefinition[Index].CrewID;
                                CrewRetainDefinition[Index].IsReptFilter = ((CheckBox)item["IsReptFilter"].FindControl("chkIsReptFilter")).Checked;
                                CrewRetainDefinition[Index].InformationValue = ((TextBox)item["AddlInfo"].FindControl("tbAddlInfo")).Text;
                                //PassengerAddlInfo[Index].IsDeleted = false;
                                break;
                            }
                        }
                    }
                    return CrewRetainDefinition;

                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }

        /// <summary>
        /// Method to Retain Type Ratings Grid Items into List
        /// </summary>
        /// <param name="Grid"></param>
        /// <returns>Returns Crew Type Ratings List</returns>
        private List<GetCrewRating> RetainCrewTypeRatingGrid(RadGrid Grid)
        {
            List<GetCrewRating> CrewRateList = new List<GetCrewRating>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Grid))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.GetCrewRating>>(() =>
                {

                    List<FlightPakMasterService.GetCrewRating> CrewRetainRating = new List<FlightPakMasterService.GetCrewRating>();
                    CrewRetainRating = (List<FlightPakMasterService.GetCrewRating>)Session["CrewTypeRating"];
                    List<FlightPakMasterService.GetCrewRating> CrewRetainDefinitionList = new List<FlightPakMasterService.GetCrewRating>();
                    for (int Index = 0; Index < CrewRetainRating.Count; Index++)
                    {
                        foreach (GridDataItem Item in Grid.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("AircraftTypeCD").ToString() == CrewRetainRating[Index].AircraftTypeCD)
                            {
                                if (((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")) != null)
                                {
                                    CrewRetainRating[Index].IsPilotinCommand = ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsPilotinCommand = false;
                                }

                                if (((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")) != null)
                                {
                                    CrewRetainRating[Index].IsSecondInCommand = ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsSecondInCommand = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null)
                                {
                                    CrewRetainRating[Index].IsQualInType135PIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsQualInType135PIC = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null)
                                {
                                    CrewRetainRating[Index].IsQualInType135SIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsQualInType135SIC = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null)
                                {
                                    CrewRetainRating[Index].IsEngineer = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsEngineer = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null)
                                {
                                    CrewRetainRating[Index].IsInstructor = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsInstructor = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null)
                                {
                                    CrewRetainRating[Index].IsCheckAttendant = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsCheckAttendant = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null)
                                {
                                    CrewRetainRating[Index].IsAttendantFAR91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsAttendantFAR91 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null)
                                {
                                    CrewRetainRating[Index].IsAttendantFAR135 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsAttendantFAR135 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null)
                                {
                                    CrewRetainRating[Index].IsCheckAirman = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsCheckAirman = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null)
                                {
                                    CrewRetainRating[Index].IsInActive = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsInActive = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null)
                                {
                                    CrewRetainRating[Index].IsPIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsPIC121 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null)
                                {
                                    CrewRetainRating[Index].IsSIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsSIC121 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null)
                                {
                                    CrewRetainRating[Index].IsPIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsPIC125 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null)
                                {
                                    CrewRetainRating[Index].IsSIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsSIC125 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null)
                                {
                                    CrewRetainRating[Index].IsAttendant121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsAttendant121 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null)
                                {
                                    CrewRetainRating[Index].IsAttendant125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsAttendant125 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")) != null)
                                {
                                    CrewRetainRating[Index].IsSIC91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsSIC91 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")) != null)
                                {
                                    CrewRetainRating[Index].IsPIC91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsPIC91 = false;
                                }
                                // Total Hrs
                                //if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                                //{
                                //    CrewRetainRating[Index].TotalUpdateAsOfDT = Convert.ToDateTime(FormatDateTime((((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim()), DateFormat));
                                //}

                                if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                                {
                                    CrewRetainRating[Index].TotalUpdateAsOfDT = Convert.ToDateTime(FormatDateTime((((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim()), DateFormat));
                                }

                                //Grid Data Items Start
                                if (!string.IsNullOrEmpty(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text))
                                {
                                    decimal TotalTimeInTypeHrs = 0;
                                    if (((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Contains(":"))
                                    {
                                        TotalTimeInTypeHrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Trim(), true, "GENERAL")), 1);
                                    }
                                    else
                                    {
                                        TotalTimeInTypeHrs = Convert.ToDecimal(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                    CrewRetainRating[Index].TotalTimeInTypeHrs = TotalTimeInTypeHrs;
                                }
                                else
                                {
                                    CrewRetainRating[Index].TotalTimeInTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text))
                                {
                                    decimal TotalDayHrs = 0;
                                    if (((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Contains(":"))
                                    {
                                        TotalDayHrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Trim(), true, "GENERAL")), 1);
                                        //CrewRetainRating[Index].TotalDayHrs = 0;
                                    }
                                    else
                                    {
                                        TotalDayHrs = Convert.ToDecimal(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                    CrewRetainRating[Index].TotalDayHrs = TotalDayHrs;
                                }
                                else
                                {
                                    CrewRetainRating[Index].TotalDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text))
                                {
                                    decimal TotalNightHrs = 0;
                                    if (((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Contains(":"))
                                    {
                                        TotalNightHrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Trim(), true, "GENERAL")), 1);
                                    }
                                    else
                                    {
                                        TotalNightHrs = Convert.ToDecimal(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                    CrewRetainRating[Index].TotalNightHrs = TotalNightHrs;
                                }
                                else
                                {
                                    CrewRetainRating[Index].TotalNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text))
                                {
                                    decimal TotalINSTHrs = 0;
                                    if (((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text.Contains(":"))
                                    {
                                        TotalINSTHrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text.Trim(), true, "GENERAL")), 1);
                                    }
                                    else
                                    {
                                        TotalINSTHrs = Convert.ToDecimal(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                    CrewRetainRating[Index].TotalINSTHrs = TotalINSTHrs;
                                }
                                else
                                {
                                    CrewRetainRating[Index].TotalINSTHrs = 0;
                                }

                                //Grid Data Items End
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text))
                                {
                                    if (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text == ":")
                                    {
                                        CrewRetainRating[Index].TimeInType = 0;
                                    }
                                    else
                                    {
                                        CrewRetainRating[Index].TimeInType = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                }
                                else
                                {
                                    CrewRetainRating[Index].TimeInType = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                                {
                                    CrewRetainRating[Index].Day1 = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].Day1 = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                                {
                                    CrewRetainRating[Index].Night1 = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].Night1 = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                                {
                                    CrewRetainRating[Index].Instrument = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].Instrument = 0;
                                }


                                // PIC Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                                {
                                    CrewRetainRating[Index].PilotInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].PilotInCommandTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                                {
                                    CrewRetainRating[Index].TPilotinCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TPilotinCommandDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                                {
                                    CrewRetainRating[Index].TPilotInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TPilotInCommandNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                                {
                                    CrewRetainRating[Index].TPilotInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TPilotInCommandINSTHrs = 0;
                                }



                                // SIC Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                                {
                                    CrewRetainRating[Index].SecondInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].SecondInCommandTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                                {
                                    CrewRetainRating[Index].SecondInCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].SecondInCommandDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                                {
                                    CrewRetainRating[Index].SecondInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].SecondInCommandNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                                {
                                    CrewRetainRating[Index].SecondInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].SecondInCommandINSTHrs = 0;
                                }


                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewRetainRating[Index].AsOfDT = Convert.ToDateTime(FormatDate(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text.Trim(), DateFormat));
                                    }
                                    else
                                    {
                                        CrewRetainRating[Index].AsOfDT = Convert.ToDateTime(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text.Trim());
                                    }
                                }
                                else
                                {
                                    CrewRetainRating[Index].AsOfDT = null;
                                }
                                // Others Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                                {

                                    CrewRetainRating[Index].OtherSimHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].OtherSimHrs = 0;
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                                {
                                    CrewRetainRating[Index].OtherFlightEngHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].OtherFlightEngHrs = 0;
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                                {
                                    CrewRetainRating[Index].OtherFlightInstrutorHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].OtherFlightInstrutorHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                                {
                                    CrewRetainRating[Index].OtherFlightAttdHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].OtherFlightAttdHrs = 0;
                                }
                                break;
                            }
                        }
                    }
                    return CrewRetainRating;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Retain Crew Checklist Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Checklist</returns>
        private List<GetCrewCheckListDate> RetainCrewCheckListGrid(RadGrid grid)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<GetCrewCheckListDate> CrewCheckListDateList = new List<GetCrewCheckListDate>();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.GetCrewCheckListDate>>(() =>
                {
                    List<FlightPakMasterService.GetCrewCheckListDate> GetCrewCheckListDateRetain = new List<FlightPakMasterService.GetCrewCheckListDate>();
                    GetCrewCheckListDateRetain = (List<FlightPakMasterService.GetCrewCheckListDate>)Session["CrewCheckList"];
                    List<FlightPakMasterService.GetCrewCheckListDate> CrewCheckListRetainList = new List<FlightPakMasterService.GetCrewCheckListDate>();

                    for (int Index = 0; Index < GetCrewCheckListDateRetain.Count; Index++)
                    {
                        foreach (GridDataItem Item in grid.MasterTableView.Items)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (Item.GetDataKeyValue("CheckListCD").ToString() == GetCrewCheckListDateRetain[Index].CheckListCD)
                            {
                                GetCrewCheckListDateRetain[Index].CrewID = GetCrewCheckListDateRetain[Index].CrewID;
                                GetCrewCheckListDateRetain[Index].CrewChecklistDescription = ((Label)Item["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text;
                                if (!string.IsNullOrEmpty(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].OriginalDT = Convert.ToDateTime(FormatDate(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].OriginalDT = Convert.ToDateTime(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].PreviousCheckDT = Convert.ToDateTime(FormatDate(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].PreviousCheckDT = Convert.ToDateTime(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["DueDT"].FindControl("lbDueDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].DueDT = Convert.ToDateTime(FormatDate(((Label)Item["DueDT"].FindControl("lbDueDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].DueDT = Convert.ToDateTime(((Label)Item["DueDT"].FindControl("lbDueDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].AlertDT = Convert.ToDateTime(FormatDate(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].AlertDT = Convert.ToDateTime(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text);
                                    }
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].GraceDT = Convert.ToDateTime(FormatDate(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].GraceDT = Convert.ToDateTime(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].AlertDays = Convert.ToInt32(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].GraceDays = Convert.ToInt32(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].FrequencyMonth = Convert.ToInt32(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].BaseMonthDT = Convert.ToDateTime(FormatDate(((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].BaseMonthDT = Convert.ToDateTime(((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text);
                                    }
                                }
                                //if (!string.IsNullOrEmpty(((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text))
                                //{
                                //    GetCrewCheckListDateRetain[Index].AircraftTypeCD = ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text;
                                //    MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient();
                                //    var CrewRosterServiceValue = CrewRosterService.GetAircraftList().EntityList.Where(x => x.AircraftCD.ToString().ToUpper().Trim().Equals(GetCrewCheckListDateRetain[Index].AircraftTypeCD.ToUpper().Trim())).ToList();
                                //    if (CrewRosterServiceValue != null && CrewRosterServiceValue.Count > 0)
                                //    {
                                //        GetCrewCheckListDateRetain[Index].AircraftID = Convert.ToInt64(((FlightPak.Web.FlightPakMasterService.GetAllAircraft)CrewRosterServiceValue[0]).AircraftID.ToString());
                                //    }
                                //}

                                // Start
                                if (!string.IsNullOrEmpty(((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].AircraftTypeCD = ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text;
                                }
                                if (!string.IsNullOrEmpty(((HiddenField)Item["AircraftTypeCD"].FindControl("hdnAircraftTypeCD")).Value))
                                {
                                    GetCrewCheckListDateRetain[Index].AircraftID = Convert.ToInt64(((HiddenField)Item["AircraftTypeCD"].FindControl("hdnAircraftTypeCD")).Value);
                                }
                                //End

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].Frequency = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text);
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsPilotInCommandFAR91 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsPilotInCommandFAR135 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsSecondInCommandFAR91 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsSecondInCommandFAR135 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsInActive = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsMonthEnd = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsStopCALC = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsOneTimeEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsCompleted = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsNoConflictEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsNoCrewCheckListREPTt = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsNoChecklistREPT = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsPrintStatus = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsPassedDueAlert = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsNextMonth = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsEndCalendarYear = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text))
                                {
                                    if (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text == "0")
                                    {
                                        GetCrewCheckListDateRetain[Index].TotalREQFlightHrs = 0;
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].TotalREQFlightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                }
                                else
                                {
                                    GetCrewCheckListDateRetain[Index].TotalREQFlightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text))
                                {
                                    if (((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text == "0")
                                    {
                                        GetCrewCheckListDateRetain[Index].FlightLogHours = 0;
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].FlightLogHours = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text, CultureInfo.CurrentCulture);
                                    }
                                }
                                else
                                {
                                    GetCrewCheckListDateRetain[Index].FlightLogHours = 0;
                                }
                                break;
                            }
                        }
                    }

                    return GetCrewCheckListDateRetain;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Retain Crew Passenger Visa Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Pax Visa List</returns>
        //private List<GetAllCrewPaxVisa> RetainCrewVisaGrid(RadGrid grid)
        private List<GetAllCrewPaxVisa> RetainCrewVisaGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<GetAllCrewPaxVisa>>(() =>
                {
                    List<GetAllCrewPaxVisa> CrewPaxVisaList = new List<GetAllCrewPaxVisa>();
                    GetAllCrewPaxVisa CrewPaxVisaListDef = new GetAllCrewPaxVisa();
                    foreach (GridDataItem Item in grid.MasterTableView.Items)
                    {
                        CrewPaxVisaListDef = new GetAllCrewPaxVisa();
                        CrewPaxVisaListDef.VisaID = Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString());
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            CrewPaxVisaListDef.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                        }
                        else
                        {
                            CrewPaxVisaListDef.CrewID = 0;
                        }
                        CrewPaxVisaListDef.PassengerRequestorID = null;
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value != string.Empty)
                        {
                            CrewPaxVisaListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value);
                            CrewPaxVisaListDef.CountryCD = ((TextBox)Item["CountryCD"].FindControl("tbCountryCD")).Text;
                        }
                        CrewPaxVisaListDef.VisaTYPE = string.Empty;
                        if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text != null)
                        {
                            CrewPaxVisaListDef.VisaNum = ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text;
                        }
                        else
                        {
                            CrewPaxVisaListDef.VisaNum = string.Empty;
                        }
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaListDef.ExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxVisaListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.IssuePlace = ((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text;
                        }
                        if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaListDef.IssueDate = Convert.ToDateTime(FormatDate(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxVisaListDef.IssueDate = Convert.ToDateTime(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text);
                            }
                        }

                        if (((DropDownList)Item["TypeOfVisa"].FindControl("ddlTypeOfVisa")).SelectedValue != "Select")
                        {
                            CrewPaxVisaListDef.TypeOfVisa = ((DropDownList)Item["TypeOfVisa"].FindControl("ddlTypeOfVisa")).SelectedValue.ToString();
                        }
                        else
                        {
                            CrewPaxVisaListDef.TypeOfVisa = "";
                        }

                        if (((TextBox)Item["EntriesAllowedInPassport"].FindControl("tbEntriesAllowedInPassport")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.EntriesAllowedInPassport = Convert.ToInt32(((TextBox)Item["EntriesAllowedInPassport"].FindControl("tbEntriesAllowedInPassport")).Text);
                        }

                        if (((TextBox)Item["VisaExpireInDays"].FindControl("tbVisaExpireInDays")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.VisaExpireInDays = Convert.ToInt32(((TextBox)Item["VisaExpireInDays"].FindControl("tbVisaExpireInDays")).Text);
                        }

                        if (((TextBox)Item["Notes"].FindControl("tbNotes")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.Notes = ((TextBox)Item["Notes"].FindControl("tbNotes")).Text;
                        }
                        CrewPaxVisaListDef.IsDeleted = false;
                        CrewPaxVisaList.Add(CrewPaxVisaListDef);
                    }
                    return CrewPaxVisaList;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Retain Crew Passenger Passport Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        private List<GetAllCrewPassport> RetainCrewPassportGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<GetAllCrewPassport> CrewPassportList = new List<GetAllCrewPassport>();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    foreach (GridDataItem Item in grid.MasterTableView.Items)
                    {
                        GetAllCrewPassport CrewPassport = new GetAllCrewPassport();
                        if (!string.IsNullOrEmpty(((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text))
                        {
                            CrewPassport.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                        }
                        CrewPassport.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            CrewPassport.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                        }
                        else
                        {
                            CrewPassport.CrewID = 0;
                        }
                        CrewPassport.Choice = ((CheckBox)Item["Choice"].FindControl("chkChoice")).Checked;
                        if (((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPassport.PassportExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPassport.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                            }
                        }
                        if (!string.IsNullOrEmpty(((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text))
                        {
                            CrewPassport.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                        }
                        if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPassport.IssueDT = Convert.ToDateTime(FormatDate(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPassport.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                            }
                        }
                        if (!string.IsNullOrEmpty(((TextBox)Item["PassportCountry"].FindControl("tbPassportCountry")).Text))
                        {
                            CrewPassport.CountryCD = ((TextBox)Item["PassportCountry"].FindControl("tbPassportCountry")).Text;
                        }
                        if (Item.GetDataKeyValue("CountryID") != null)
                        {
                            CrewPassport.CountryID = Convert.ToInt64(Item.GetDataKeyValue("CountryID").ToString());
                        }
                        if (((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value != string.Empty)
                        {
                            CrewPassport.CountryID = Convert.ToInt64(((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value);
                            CrewPassport.CountryCD = ((TextBox)Item["PassportCountry"].FindControl("tbPassportCountry")).Text;
                        }
                        CrewPassport.IsDeleted = false;
                        CrewPassportList.Add(CrewPassport);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return CrewPassportList;
            }
        }

        #endregion
        #region "Radgrid Item Databounds"
        protected void Visa_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;

                            if (Item.GetDataKeyValue("ExpiryDT") != null)
                            {
                                ((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("ExpiryDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("IssueDate") != null)
                            {
                                ((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text = String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("IssueDate").ToString())));
                            }
                            if (Item.GetDataKeyValue("VisaID") != null)
                            {
                                if (Item.GetDataKeyValue("VisaID").ToString() == "0")
                                {
                                    ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Enabled = true;
                                }
                                else
                                {
                                    ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Enabled = false;
                                }

                            }
                            GridEditableItem item = e.Item as GridEditableItem;
                            DropDownList list = item.FindControl("ddlTypeOfVisa") as DropDownList;
                            list.Items.Add("Select");
                            list.Items.Add("Business");
                            list.Items.Add("Crew");
                            list.Items.Add("Private");
                            list.Items.Add("Tourist");
                            list.Items.Add("Work");

                            string CurrentType;
                            CurrentType = Convert.ToString(Item.GetDataKeyValue("TypeOfVisa"));

                            if (!string.IsNullOrEmpty(CurrentType))
                            {
                                list.SelectedValue = Item.GetDataKeyValue("TypeOfVisa").ToString();
                            }
                            else
                            {
                                list.SelectedValue = "Select";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }
        protected void Passport_ItemDataBound(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Item.GetDataKeyValue("PassportExpiryDT") != null)
                            {
                                ((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("PassportExpiryDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("IssueDT") != null)
                            {
                                ((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text = String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("IssueDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("PassportID") != null)
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("PassportID")) < 1000)
                                {
                                    ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Enabled = true;
                                }
                                else
                                {
                                    ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Enabled = false;
                                }

                            }
                            CheckBox checkColumn = Item["Choice"].FindControl("chkChoice") as CheckBox;
                            checkColumn.Attributes.Add("onclick", "uncheckOther(this);");
                        }
                        if (e.Item is GridHeaderItem)
                        {
                            if (UserPrincipal.Identity._fpSettings._IsAPISSupport == true)
                            {
                                GridHeaderItem Item = (GridHeaderItem)e.Item;
                                GridDataItem DataItem = e.Item as GridDataItem;
                                GridColumn PassportNumColumn = dgPassport.MasterTableView.GetColumn("PassportNum");
                                GridColumn PassportCountryColumn = dgPassport.MasterTableView.GetColumn("PassportCountry");
                                PassportNumColumn.HeaderStyle.CssClass = "important-bold-text";
                                PassportCountryColumn.HeaderStyle.CssClass = "important-bold-text";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        #endregion
        #endregion
        #region "Crew Additional Info"
        #region "Crew Roster Additional Info Grid Events"
        /// <summary>
        /// Edit Row styles, based on Crew Roster Additional Info.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewAddlInfo_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Item.GetDataKeyValue("IsReptFilter") != null)
                            {
                                if (Item.GetDataKeyValue("IsReptFilter").ToString() == "True")
                                {
                                    ((CheckBox)Item["IsReptFilter"].FindControl("chkIsReptFilter")).Checked = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to Bind Additional Information Grid, based on Crew Code
        /// </summary>
        /// <param name="CrewID">Pass Crew Code</param>
        private void BindAdditionalInfoGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // GridDataItem Item = (GridDataItem)Session["SelectedCrewRosterID"];
                    if (Session["CrewAddInfo"] != null)
                    {
                        List<GetCrewDefinition> CrewDefinitionList = (List<GetCrewDefinition>)Session["CrewAddInfo"];
                        dgCrewAddlInfo.DataSource = CrewDefinitionList;
                        dgCrewAddlInfo.DataBind();
                    }
                    else
                    {
                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            List<GetCrewDefinition> CrewAdditionalInfoList = new List<GetCrewDefinition>();
                            GetCrewDefinition crew = new GetCrewDefinition();
                            crew.CrewID = CrewID;
                            var CrewDefinitionValue = CrewRosterService.GetCrewDefinitionList(crew);

                            if (CrewDefinitionValue.ReturnFlag == true)
                            {
                                CrewAdditionalInfoList = CrewDefinitionValue.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.GetCrewDefinition>();
                                dgCrewAddlInfo.DataSource = CrewAdditionalInfoList;
                                dgCrewAddlInfo.DataBind();
                                Session["CrewAddInfo"] = CrewAdditionalInfoList;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Crew Add Info Button Events"
        /// <summary>
        /// Delete Selected Crew Roster Additional Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteInfo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnDelete.Value == "Yes")
                        {
                            if (dgCrewAddlInfo.SelectedItems.Count > 0)
                            {
                                GridDataItem item = (GridDataItem)dgCrewAddlInfo.SelectedItems[0];
                                Int64 CrewInfoXRefID = Convert.ToInt64(item.GetDataKeyValue("CrewInfoXRefID").ToString());
                                string CrewInfoCD = item.GetDataKeyValue("CrewInfoCD").ToString();
                                List<FlightPakMasterService.GetCrewDefinition> CrewDefinitionList = new List<FlightPakMasterService.GetCrewDefinition>();
                                CrewDefinitionList = (List<FlightPakMasterService.GetCrewDefinition>)Session["CrewAddInfo"];
                                for (int Index = 0; Index < CrewDefinitionList.Count; Index++)
                                {
                                    if (CrewDefinitionList[Index].CrewInfoCD == CrewInfoCD)
                                    {
                                        CrewDefinitionList[Index].IsDeleted = true;
                                    }
                                }
                                Session["CrewAddInfo"] = CrewDefinitionList;
                                List<FlightPakMasterService.GetCrewDefinition> CrewDefinitionInfoList = new List<FlightPakMasterService.GetCrewDefinition>();
                                // Retain the Grid Items and bind into List to remove the selected item.
                                CrewDefinitionInfoList = RetainCrewDefinitionGrid(dgCrewAddlInfo);
                                dgCrewAddlInfo.DataSource = CrewDefinitionInfoList.Where(x => x.IsDeleted == false).ToList();
                                dgCrewAddlInfo.DataBind();
                                Session["CrewAddInfo"] = CrewDefinitionList;
                            }
                            else
                            {

                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Please select the record from Additional Information table', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #endregion
        #region "Crew Type Rating"
        #region "Crew Roster Type Ratings Grid Events"

        public bool GetCrewRatingDisplay(Int64 CrewID)
        {
            bool IsDisplay = false;
            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
            {
                var CrewData = CrewRosterService.GetCrewforDisplay(CrewID);
                var CrewList = CrewData.EntityList.ToList();
                if (CrewList != null && CrewList[0].IsCrewDisplay != null)
                {
                    Session["Display"] = CrewList[0].IsCrewDisplay;
                }
                else
                {
                    Session["Display"] = false;
                }
                if (Session["Display"] != null)
                {
                    IsDisplay = Convert.ToBoolean(Session["Display"]);
                }
                else
                {
                    IsDisplay = true;
                }
            }
            return IsDisplay;
        }


        public void UpdateCrewforDisplay(Int64 CrewID, bool IsDisplay)
        {
            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
            {
                var CrewData = CrewRosterService.UpdateCrewDisplay(CrewID, IsDisplay);
                Session["Display"] = IsDisplay;
            }
        }


        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TypeRating_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.SelectedItems.Count > 0)
                        {
                            BindSelectedTypeRatingItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Edit Row styles, based on Crew Rating.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TypeRating_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Convert.ToString(Item.GetDataKeyValue("IsPilotinCommand")) == "True" || Convert.ToString(Item.GetDataKeyValue("IsQualInType135PIC")) == "True")
                            {
                                ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked = true;
                            }
                            if (Convert.ToString(("IsSecondInCommand")) == "True" || Convert.ToString(Item.GetDataKeyValue("IsQualInType135SIC")) == "True")
                            {
                                ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked = true;
                            }
                            string TestDueDT = ((Label)(Item["HiddenCheckboxValues"].FindControl("lbAsOfDt"))).Text.Trim();
                            if (TestDueDT != string.Empty)
                            {
                                ((Label)(Item["HiddenCheckboxValues"].FindControl("lbAsOfDt"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestDueDT)));
                            }

                            //Grid Data Item Starts

                            //TotalTimeInTypeHrs
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text))
                                    ((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode("00:00");
                                else if ((((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Trim() == "0") || (((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Trim() == ""))
                                    ((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode("00:00");
                                else
                                    ((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text).ToString()), 1).ToString()));
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text))
                                    ((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                else if ((((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Trim() == "0") || (((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Trim() == ""))
                                    ((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                else
                                    ((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode(AddPadding((((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text)));
                            }

                            //Total Day Hours
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text))
                                    ((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text = System.Web.HttpUtility.HtmlEncode("00:00");
                                else if ((((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Trim() == "0.0") || (((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Trim() == "0") || (((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Trim() == ""))
                                    ((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text = System.Web.HttpUtility.HtmlEncode("00:00");
                                else
                                    ((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text).ToString()), 1).ToString()));
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text))
                                    ((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                else if ((((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Trim() == "0.0") || (((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Trim() == "0") || (((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Trim() == ""))
                                    ((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                else
                                    ((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text = System.Web.HttpUtility.HtmlEncode(AddPadding((((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text)));
                            }


                            //Total Night Hours
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text))
                                    ((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text = System.Web.HttpUtility.HtmlEncode("00:00");
                                else if ((((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Trim() == "0.0") || (((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Trim() == "0") || (((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Trim() == ""))
                                    ((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text = System.Web.HttpUtility.HtmlEncode("00:00");
                                else
                                    ((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text).ToString()), 1).ToString()));
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text))
                                    ((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                else if ((((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Trim() == "0.0") || (((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Trim() == "0") || (((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Trim() == ""))
                                    ((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                else
                                    ((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text = System.Web.HttpUtility.HtmlEncode(AddPadding((((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text)));
                            }

                            //Total Instr 
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text))
                                    ((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text = System.Web.HttpUtility.HtmlEncode("00:00");
                                else if ((((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text.Trim() == "0.0") || (((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text.Trim() == "0") || (((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text.Trim() == ""))
                                    ((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text = System.Web.HttpUtility.HtmlEncode("00:00");
                                else
                                    ((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text).ToString()), 1).ToString()));
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text))
                                    ((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                else if ((((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text.Trim() == "0.0") || (((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text.Trim() == "0") || (((Label)Item["TotalNightHrs"].FindControl("lbTotalINSTHrs")).Text.Trim() == ""))
                                    ((Label)Item["TotalNightHrs"].FindControl("lbTotalINSTHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                else
                                    ((Label)Item["TotalNightHrs"].FindControl("lbTotalINSTHrs")).Text = System.Web.HttpUtility.HtmlEncode(AddPadding((((Label)Item["TotalNightHrs"].FindControl("lbTotalINSTHrs")).Text)));
                            }
                            if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                            {
                                if (((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim() == "")
                                {
                                    ((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text = System.Web.HttpUtility.HtmlEncode("");
                                }
                                else
                                {
                                    ((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text = System.Web.HttpUtility.HtmlEncode(GetDateWithoutSeconds(Convert.ToDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text)));
                                }
                            }
                            else
                            {
                                ((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text = System.Web.HttpUtility.HtmlEncode("");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Bind Selected Type Raing Grid Item in the Fields
        /// </summary>
        private void BindSelectedTypeRatingItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgTypeRating.SelectedItems.Count > 0)
                    {
                        if (btnFlightExp.Text == "Display Current Flight Experience")
                        {
                            GridDataItem Item = (GridDataItem)dgTypeRating.SelectedItems[0];
                            Item.Selected = true;
                            lbTypeCode.Text = ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text.Trim();
                            lbDescription.Text = ((Label)Item["CrewRatingDescription"].FindControl("lbCrewRatingDescription")).Text;
                            chkTRPIC91.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")).Checked : false;
                            chkTRSIC91.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")).Checked : false;
                            chkTRPIC135.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked : false;
                            chkTRSIC135.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked : false;
                            chkTREngineer.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked : false;
                            chkTRInstructor.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked : false;
                            chkTRAttendant.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked : false;
                            chkTRAttendant91.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked : false;
                            chkTRAttendant135.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked : false;
                            chkTRAirman.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked : false;
                            chkTRInactive.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked : false;
                            chkPIC121.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked : false;
                            chkSIC121.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked : false;
                            chkPIC125.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked : false;
                            chkSIC125.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked : false;
                            chkAttendent121.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked : false;
                            chkAttendent125.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked : false;
                            //1
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text))
                                    tbTotalHrsInType.Text = "00:00";
                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == ""))
                                    tbTotalHrsInType.Text = "00:00";
                                else
                                    tbTotalHrsInType.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text))
                                    tbTotalHrsInType.Text = "0.0";
                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == ""))
                                    tbTotalHrsInType.Text = "0.0";
                                else
                                    tbTotalHrsInType.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text));
                            }
                            //2
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                                    tbTotalHrsDay.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == ""))
                                    tbTotalHrsDay.Text = "00:00";
                                else
                                    tbTotalHrsDay.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                                    tbTotalHrsDay.Text = "0.0";
                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == ""))
                                    tbTotalHrsDay.Text = "0.0";
                                else
                                    tbTotalHrsDay.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text));
                            }
                            //3
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                                    tbTotalHrsNight.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == ""))
                                    tbTotalHrsNight.Text = "00:00";
                                else
                                    tbTotalHrsNight.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text).ToString()), 1).ToString());
                            }
                            else
                            {

                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                                    tbTotalHrsNight.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == ""))
                                    tbTotalHrsNight.Text = "0.0";
                                else
                                    tbTotalHrsNight.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text));
                            }

                            //4
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                                    tbTotalHrsInstrument.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == ""))
                                    tbTotalHrsInstrument.Text = "00:00";
                                else
                                    tbTotalHrsInstrument.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                                    tbTotalHrsInstrument.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == ""))
                                    tbTotalHrsInstrument.Text = "0.0";
                                else
                                    tbTotalHrsInstrument.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text));
                            }

                            //// PIC Hrs
                            //5
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                                    tbPICHrsInType.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == ""))
                                    tbPICHrsInType.Text = "00:00";
                                else
                                    tbPICHrsInType.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                                    tbPICHrsInType.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == ""))
                                    tbPICHrsInType.Text = "0.0";
                                else
                                    tbPICHrsInType.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text));
                            }

                            //6
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                                    tbPICHrsDay.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == ""))
                                    tbPICHrsDay.Text = "00:00";
                                else
                                    tbPICHrsDay.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                                    tbPICHrsDay.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == ""))
                                    tbPICHrsDay.Text = "0.0";
                                else
                                    tbPICHrsDay.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text));
                            }


                            //7
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                                    tbPICHrsNight.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == ""))
                                    tbPICHrsNight.Text = "00:00";
                                else
                                    tbPICHrsNight.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text).ToString()), 1).ToString());
                            }
                            else
                            {

                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                                    tbPICHrsNight.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == ""))
                                    tbPICHrsNight.Text = "0.0";
                                else
                                    tbPICHrsNight.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text));
                            }

                            //8
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                                    tbPICHrsInstrument.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == ""))
                                    tbPICHrsInstrument.Text = "00:00";
                                else
                                    tbPICHrsInstrument.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                                    tbPICHrsInstrument.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == ""))
                                    tbPICHrsInstrument.Text = "0.0";
                                else
                                    tbPICHrsInstrument.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text));
                            }
                            //// SIC Hrs
                            //9
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                                    tbSICInTypeHrs.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == ""))
                                    tbSICInTypeHrs.Text = "00:00";
                                else
                                    tbSICInTypeHrs.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                                    tbSICInTypeHrs.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == ""))
                                    tbSICInTypeHrs.Text = "0.0";
                                else
                                    tbSICInTypeHrs.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text));
                            }


                            //10
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                                    tbSICHrsDay.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == ""))
                                    tbSICHrsDay.Text = "00:00";
                                else
                                    tbSICHrsDay.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                                    tbSICHrsDay.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == ""))
                                    tbSICHrsDay.Text = "0.0";
                                else
                                    tbSICHrsDay.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text));
                            }


                            //11
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                                    tbSICHrsNight.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == ""))
                                    tbSICHrsNight.Text = "00:00";
                                else
                                    tbSICHrsNight.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                                    tbSICHrsNight.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == ""))
                                    tbSICHrsNight.Text = "0.0";
                                else
                                    tbSICHrsNight.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text));
                            }


                            //12
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                                    tbSICHrsInstrument.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == ""))
                                    tbSICHrsInstrument.Text = "00:00";
                                else
                                    tbSICHrsInstrument.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                                    tbSICHrsInstrument.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == ""))
                                    tbSICHrsInstrument.Text = "0.0";
                                else
                                    tbSICHrsInstrument.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text));
                            }

                            //// Other Hrs

                            //13
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                                    tbOtherHrsSimulator.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == ""))
                                    tbOtherHrsSimulator.Text = "00:00";
                                else
                                    tbOtherHrsSimulator.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                                    tbOtherHrsSimulator.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == ""))
                                    tbOtherHrsSimulator.Text = "0.0";
                                else
                                    tbOtherHrsSimulator.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text));
                            }

                            //14
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                                    tbOtherHrsFltEngineer.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == ""))
                                    tbOtherHrsFltEngineer.Text = "00:00";
                                else
                                    tbOtherHrsFltEngineer.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                                    tbOtherHrsFltEngineer.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == ""))
                                    tbOtherHrsFltEngineer.Text = "0.0";
                                else
                                    tbOtherHrsFltEngineer.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text));
                            }

                            //15
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                                    tbOtherHrsFltInstructor.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == ""))
                                    tbOtherHrsFltInstructor.Text = "00:00";
                                else
                                    tbOtherHrsFltInstructor.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                                    tbOtherHrsFltInstructor.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == ""))
                                    tbOtherHrsFltInstructor.Text = "0.0";
                                else
                                    tbOtherHrsFltInstructor.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text));

                            }
                            //16
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                                    tbOtherHrsFltAttendant.Text = "00:00";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == ""))
                                    tbOtherHrsFltAttendant.Text = "00:00";
                                else
                                    tbOtherHrsFltAttendant.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text).ToString()), 1).ToString());
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                                    tbOtherHrsFltAttendant.Text = "0.0";

                                else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == ""))
                                    tbOtherHrsFltAttendant.Text = "0.0";
                                else
                                    tbOtherHrsFltAttendant.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text));
                            }
                            if ((pnlBeginning.Visible == true) && (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text)))
                            {
                                tbAsOfDt.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text, DateFormat)));
                            }
                            else
                            {
                                tbAsOfDt.Text = string.Empty;
                            }
                            tbTotalHrsInType.Focus();
                        }
                        else
                        {
                            GridDataItem Item = (GridDataItem)dgTypeRating.SelectedItems[0];
                            Item.Selected = true;
                            long CrewID = Convert.ToInt64(Item.GetDataKeyValue("CrewID"));
                            long AircraftID = Convert.ToInt64(Item.GetDataKeyValue("AircraftTypeID"));
                            CalulateDisplayCurrentFlightExp(CrewID, AircraftID);
                            lbTypeCode.Text = ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text.Trim();
                            lbDescription.Text = ((Label)Item["CrewRatingDescription"].FindControl("lbCrewRatingDescription")).Text;
                            chkTRPIC91.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")).Checked : false;
                            chkTRSIC91.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")).Checked : false;
                            chkTRPIC135.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked : false;
                            chkTRSIC135.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked : false;
                            chkTREngineer.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked : false;
                            chkTRInstructor.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked : false;
                            chkTRAttendant.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked : false;
                            chkTRAttendant91.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked : false;
                            chkTRAttendant135.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked : false;
                            chkTRAirman.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked : false;
                            chkTRInactive.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked : false;
                            chkPIC121.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked : false;
                            chkSIC121.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked : false;
                            chkPIC125.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked : false;
                            chkSIC125.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked : false;
                            chkAttendent121.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked : false;
                            chkAttendent125.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked : false;

                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Type Rating Grid based on Crew Code
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private void BindTypeRatingGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewTypeRating"] != null)
                    {
                        List<GetCrewRating> GetCrewRatingList = (List<GetCrewRating>)Session["CrewTypeRating"];
                        Session["CrewTypeRatingDisplay"] = GetCrewRatingList;
                        dgTypeRating.DataSource = GetCrewRatingList;
                        dgTypeRating.DataBind();
                    }
                    else
                    {
                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            List<GetCrewRating> CrewRatingList = new List<GetCrewRating>();
                            GetCrewRating crew = new GetCrewRating();
                            crew.CrewID = CrewID;
                            var CrewRatingValue = CrewRosterService.GetCrewRatingList(crew);
                            if (CrewRatingValue.ReturnFlag == true)
                            {
                                CrewRatingList = CrewRatingValue.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.GetCrewRating>();
                                dgTypeRating.DataSource = CrewRatingList;
                                dgTypeRating.DataBind();
                                Session["CrewTypeRating"] = CrewRatingList;
                                Session["CrewTypeRatingDisplay"] = CrewRatingList;
                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        public void CalulateDisplayCurrentFlightExp(Int64 CrewID, Int64 AircraftID)
        {
            bool IsPIC = false;
            bool IsSIC = false;
            bool IsOthers = false;

            decimal TotalFlight = 0;
            decimal TotalNight = 0;
            decimal TotalDay = 0;
            decimal TotalInstr = 0;

            decimal PIC_TotalFlight = 0;
            decimal PIC_TotalNight = 0;
            decimal PIC_TotalDay = 0;
            decimal PIC_TotalInstr = 0;

            decimal SIC_TotalFlight = 0;
            decimal SIC_TotalNight = 0;
            decimal SIC_TotalDay = 0;
            decimal SIC_TotalInstr = 0;

            decimal Others_TotalFlight = 0;
            decimal Others_TotalNight = 0;
            decimal Others_TotalDay = 0;
            decimal Others_TotalInstr = 0;

            if (Session["CrewTypeRatingDisplay"] != null)
            {
                List<GetCrewRating> GetCrewRatingList = (List<GetCrewRating>)Session["CrewTypeRatingDisplay"];


                if (Session["SelectedCrewRosterID"] != null)
                {
                    CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                }
                List<GetOtherCrewDutyLog> OtherCrewDutyList = new List<GetOtherCrewDutyLog>();
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ObjRetval = objService.GetOtherCrewDutyLog(CrewID, AircraftID);
                    if (ObjRetval.ReturnFlag)
                    {
                        OtherCrewDutyList = ObjRetval.EntityList.ToList();
                    }
                }
                int DateDiff = 1;
                decimal Flight = 0;
                decimal Night = 0;
                decimal Day = 0;
                decimal Instr = 0;

                if (OtherCrewDutyList != null && OtherCrewDutyList.Count > 0)
                {
                    foreach (GetOtherCrewDutyLog OtherCrew in OtherCrewDutyList)
                    {
                        if (OtherCrew.DepartureDTTMLocal != null && OtherCrew.ArrivalDTTMLocal != null && OtherCrew.DepartureDTTMLocal.ToString().Trim() != string.Empty && OtherCrew.ArrivalDTTMLocal.ToString().Trim() != string.Empty)
                        {
                            DateTime StartDate = new DateTime();
                            DateTime EndDate = new DateTime();
                            StartDate = Convert.ToDateTime(OtherCrew.DepartureDTTMLocal);
                            EndDate = Convert.ToDateTime(OtherCrew.ArrivalDTTMLocal);
                            //StartDate =  Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
                            //EndDate = Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
                            DateDiff = EndDate.Subtract(StartDate).Days + 1;
                        }
                        if (OtherCrew.FlightHours != null && OtherCrew.FlightHours.ToString().Trim() != string.Empty)
                        {
                            Flight = Convert.ToDecimal(OtherCrew.FlightHours * DateDiff);
                        }
                        if (OtherCrew.TakeOffDay != null && OtherCrew.TakeOffDay.ToString().Trim() != string.Empty)
                        {
                            Day = Convert.ToDecimal(OtherCrew.TakeOffDay * DateDiff);
                        }
                        if (OtherCrew.Night != null && OtherCrew.Night.ToString().Trim() != string.Empty)
                        {
                            Night = Convert.ToDecimal(OtherCrew.Night * DateDiff);
                        }
                        if (OtherCrew.Instrument != null && OtherCrew.Instrument.ToString().Trim() != string.Empty)
                        {
                            Instr = Convert.ToDecimal(OtherCrew.Instrument * DateDiff);
                        }
                        if (OtherCrew.DutyTYPE != null && OtherCrew.DutyTYPE.ToString().Trim() != string.Empty && OtherCrew.DutyTYPE.ToString().Trim().ToUpper().Contains("P"))
                        {
                            IsPIC = true;
                        }
                        if (OtherCrew.DutyTYPE != null && OtherCrew.DutyTYPE.ToString().Trim() != string.Empty && OtherCrew.DutyTYPE.ToString().Trim().ToUpper().Contains("S"))
                        {
                            IsSIC = true;
                        }
                        if (OtherCrew.DutyTYPE != null && OtherCrew.DutyTYPE.ToString().Trim() != string.Empty && OtherCrew.DutyTYPE.ToString().Trim().ToUpper().Contains("E"))
                        {
                            IsOthers = true;
                        }
                        if (OtherCrew.DutyTYPE != null && OtherCrew.DutyTYPE.ToString().Trim() != string.Empty && OtherCrew.DutyTYPE.ToString().Trim().ToUpper().Contains("I"))
                        {
                            IsOthers = true;
                        }
                        if (OtherCrew.DutyTYPE != null && OtherCrew.DutyTYPE.ToString().Trim() != string.Empty && OtherCrew.DutyTYPE.ToString().Trim().ToUpper().Contains("A"))
                        {
                            IsOthers = true;
                        }
                        if (OtherCrew.DutyTYPE != null && OtherCrew.DutyTYPE.ToString().Trim() != string.Empty && OtherCrew.DutyTYPE.ToString().Trim().ToUpper().Contains("O"))
                        {
                            IsOthers = true;
                        }
                        TotalFlight += Flight;
                        TotalInstr += Instr;
                        TotalDay += Day;
                        TotalNight += Night;
                        if (IsPIC)
                        {
                            PIC_TotalFlight += Flight;
                            PIC_TotalInstr += Instr;
                            PIC_TotalDay += Day;
                            PIC_TotalNight += Night;
                        }
                        if (IsSIC)
                        {
                            SIC_TotalFlight += Flight;
                            SIC_TotalInstr += Instr;
                            SIC_TotalDay += Day;
                            SIC_TotalNight += Night;
                        }
                        if (IsOthers)
                        {
                            Others_TotalFlight += Flight;
                            Others_TotalInstr += Instr;
                            Others_TotalDay += Day;
                            Others_TotalNight += Night;
                        }
                        IsPIC = false;
                        IsSIC = false;
                        IsOthers = false;
                        Flight = 0;
                        Night = 0;
                        Day = 0;
                        Instr = 0;
                    }
                    TotalDay = TotalFlight - TotalNight;
                    PIC_TotalDay = PIC_TotalFlight - PIC_TotalNight;
                    SIC_TotalDay = SIC_TotalFlight - SIC_TotalNight;
                    Others_TotalDay = Others_TotalFlight - Others_TotalNight;
                }
                if (GetCrewRatingList != null && GetCrewRatingList.Count > 0)
                {
                    GetCrewRatingList = GetCrewRatingList.Where(x => x.AircraftTypeID == AircraftID).ToList();
                }
                if (GetCrewRatingList != null && GetCrewRatingList.Count > 0)
                {
                    // Total Hrs
                    tbTotalHrsInTypedisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].TimeInType + TotalFlight), 1));
                    tbTotalHrsDayDisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].Day1 + TotalDay), 1));
                    tbTotalHrsNightDisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].Night1 + TotalNight), 1));
                    tbTotalHrsInstrumentDisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].Instrument + TotalInstr), 1));

                    // PIC Hrs
                    tbPICHrsInTypedisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].PilotInCommandTypeHrs + PIC_TotalFlight), 1));
                    tbPICHrsDaydisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].TPilotinCommandDayHrs + PIC_TotalDay), 1));
                    tbPICHrsNightdisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].TPilotInCommandNightHrs + PIC_TotalNight), 1));
                    tbPICHrsInstrumentdiaplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].TPilotInCommandINSTHrs + PIC_TotalInstr), 1));
                    // SIC Hrs
                    tbSICInTypeHrsdisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].SecondInCommandTypeHrs + SIC_TotalFlight), 1));
                    tbSICHrsDaydisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].SecondInCommandDayHrs + SIC_TotalDay), 1));
                    tbSICHrsNightdisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].SecondInCommandNightHrs + SIC_TotalNight), 1));
                    tbSICHrsInstrumentdisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].SecondInCommandINSTHrs + SIC_TotalInstr), 1));
                    // Other Hrs
                    tbOtherHrsSimulatordisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].OtherSimHrs + Others_TotalFlight), 1));
                    tbOtherHrsFltEngineerdisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].OtherFlightEngHrs + Others_TotalDay), 1));
                    tbOtherHrsFltInstructordisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].OtherFlightInstrutorHrs + Others_TotalNight), 1));
                    tbOtherHrsFltAttendantdisplay.Text = Convert.ToString(Math.Round(Convert.ToDecimal(GetCrewRatingList[0].OtherFlightAttdHrs + Others_TotalInstr), 1));
                    TextboxadjustmentsCall();
                }
                else
                {
                    // Total Hrs
                    tbTotalHrsInTypedisplay.Text = string.Empty;
                    tbTotalHrsDayDisplay.Text = string.Empty;
                    tbTotalHrsNightDisplay.Text = string.Empty;
                    tbTotalHrsInstrumentDisplay.Text = string.Empty;

                    // PIC Hrs
                    tbPICHrsInTypedisplay.Text = string.Empty;
                    tbPICHrsDaydisplay.Text = string.Empty;
                    tbPICHrsNightdisplay.Text = string.Empty;
                    tbPICHrsInstrumentdiaplay.Text = string.Empty;

                    // SIC Hrs
                    tbSICInTypeHrsdisplay.Text = string.Empty;
                    tbSICHrsDaydisplay.Text = string.Empty;
                    tbSICHrsNightdisplay.Text = string.Empty;
                    tbSICHrsInstrumentdisplay.Text = string.Empty;

                    // Other Hrs
                    tbOtherHrsSimulatordisplay.Text = string.Empty;
                    tbOtherHrsFltEngineerdisplay.Text = string.Empty;
                    tbOtherHrsFltInstructordisplay.Text = string.Empty;
                    tbOtherHrsFltAttendantdisplay.Text = string.Empty;
                    TextboxadjustmentsCall();
                }
            }
        }

        public void TextboxadjustmentsCall()
        {
            // Total Hrs
            Textboxadjustments(tbTotalHrsInTypedisplay);
            Textboxadjustments(tbTotalHrsDayDisplay);
            Textboxadjustments(tbTotalHrsNightDisplay);
            Textboxadjustments(tbTotalHrsInstrumentDisplay);

            // PIC Hrs
            Textboxadjustments(tbPICHrsInTypedisplay);
            Textboxadjustments(tbPICHrsDaydisplay);
            Textboxadjustments(tbPICHrsNightdisplay);
            Textboxadjustments(tbPICHrsInstrumentdiaplay);
            // SIC Hrs
            Textboxadjustments(tbSICInTypeHrsdisplay);
            Textboxadjustments(tbSICHrsDaydisplay);
            Textboxadjustments(tbSICHrsNightdisplay);
            Textboxadjustments(tbSICHrsInstrumentdisplay);
            // Other Hrs
            Textboxadjustments(tbOtherHrsSimulatordisplay);
            Textboxadjustments(tbOtherHrsFltEngineerdisplay);
            Textboxadjustments(tbOtherHrsFltInstructordisplay);
            Textboxadjustments(tbOtherHrsFltAttendantdisplay);
        }

        public void Textboxadjustments(TextBox TypeRatingText)
        {
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
            {
                if (string.IsNullOrEmpty(Convert.ToString(TypeRatingText.Text)))
                    TypeRatingText.Text = "00:00";
                else if ((TypeRatingText.Text.Trim() == "0.0") || (TypeRatingText.Text.Trim() == "0") || (TypeRatingText.Text.Trim() == ""))
                    TypeRatingText.Text = "00:00";
                else
                    TypeRatingText.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((TypeRatingText.Text).ToString()), 1).ToString());
            }
            else
            {
                if (string.IsNullOrEmpty(TypeRatingText.Text))
                    TypeRatingText.Text = "0.0";
                else if (((TypeRatingText.Text.Trim() == "0.0") || (TypeRatingText.Text.Trim() == "0") || (TypeRatingText.Text.Trim() == "")))
                    TypeRatingText.Text = "0.0";
                else
                    TypeRatingText.Text = AddPadding((TypeRatingText.Text));
            }
        }




        public void UpdateDisplayCurrentFlightExp(Int64 CrewID, Int64 AircraftID)
        {
            ////if (Session["CrewTypeRating"] != null)
            ////{
            ////    List<GetCrewRating> GetCrewRatingList = (List<GetCrewRating>)Session["CrewTypeRating"];


            ////    if (Session["SelectedCrewRosterID"] != null)
            ////    {
            ////        CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
            ////    }
            ////    List<GetOtherCrewDutyLog> OtherCrewDutyList = new List<GetOtherCrewDutyLog>();
            ////    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
            ////    {
            ////        var ObjRetval = objService.GetOtherCrewDutyLog(CrewID, AircraftID);
            ////        if (ObjRetval.ReturnFlag)
            ////        {
            ////            OtherCrewDutyList = ObjRetval.EntityList.ToList();
            ////        }
            ////    }
            ////    int DateDiff = 1;
            ////    decimal Flight = 0;
            ////    decimal TotalFlight = 0;
            ////    decimal Night = 0;
            ////    decimal TotalNight = 0;
            ////    decimal Day = 0;
            ////    decimal TotalDay = 0;
            ////    decimal Instr = 0;
            ////    decimal TotalInstr = 0;
            ////    if (OtherCrewDutyList != null && OtherCrewDutyList.Count > 0)
            ////    {
            ////        foreach (GetOtherCrewDutyLog OtherCrew in OtherCrewDutyList)
            ////        {
            ////            if (OtherCrew.DepartureDTTMLocal != null && OtherCrew.ArrivalDTTMLocal != null && OtherCrew.DepartureDTTMLocal.ToString().Trim() != string.Empty && OtherCrew.ArrivalDTTMLocal.ToString().Trim() != string.Empty)
            ////            {
            ////                DateTime StartDate = new DateTime();
            ////                DateTime EndDate = new DateTime();
            ////                StartDate = Convert.ToDateTime(OtherCrew.DepartureDTTMLocal);
            ////                EndDate = Convert.ToDateTime(OtherCrew.ArrivalDTTMLocal);
            ////                //StartDate =  Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
            ////                //EndDate = Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
            ////                DateDiff = EndDate.Subtract(StartDate).Days + 1;
            ////            }
            ////            if (OtherCrew.FlightHours != null && OtherCrew.FlightHours.ToString().Trim() != string.Empty)
            ////            {
            ////                Flight = Convert.ToDecimal(OtherCrew.FlightHours * DateDiff);
            ////            }
            ////            if (OtherCrew.TakeOffDay != null && OtherCrew.TakeOffDay.ToString().Trim() != string.Empty)
            ////            {
            ////                Day = Convert.ToDecimal(OtherCrew.TakeOffDay * DateDiff);
            ////            }
            ////            if (OtherCrew.Night != null && OtherCrew.Night.ToString().Trim() != string.Empty)
            ////            {
            ////                Night = Convert.ToDecimal(OtherCrew.Night * DateDiff);
            ////            }
            ////            if (OtherCrew.Instrument != null && OtherCrew.Instrument.ToString().Trim() != string.Empty)
            ////            {
            ////                Instr = Convert.ToDecimal(OtherCrew.Instrument * DateDiff);
            ////            }
            ////            TotalFlight += Flight;
            ////            TotalInstr += Instr;
            ////            TotalDay += Day;
            ////            TotalNight += Night;
            ////            Flight = 0;
            ////            Night = 0;
            ////            Day = 0;
            ////            Instr = 0;
            ////        }
            ////    }

            ////    if (GetCrewRatingList != null && GetCrewRatingList.Count > 0)
            ////    {
            ////        // Total Hrs
            ////        tbTotalHrsInTypedisplay.Text = Convert.ToString(GetCrewRatingList[0].TimeInType + TotalFlight);
            ////        tbTotalHrsDayDisplay.Text = Convert.ToString(GetCrewRatingList[0].Day1 + TotalDay);
            ////        tbTotalHrsNightDisplay.Text = Convert.ToString(GetCrewRatingList[0].Night1 + TotalNight);
            ////        tbTotalHrsInstrumentDisplay.Text = Convert.ToString(GetCrewRatingList[0].Instrument + TotalInstr);

            ////        ////// Total Hrs
            ////        ////tbPICHrsInTypedisplay.Text = Convert.ToString(GetCrewRatingList[0].TimeInType + TotalFlight);
            ////        ////tbPICHrsDaydisplay.Text = Convert.ToString(GetCrewRatingList[0].Day1 + TotalDay);
            ////        ////tbPICHrsNightdisplay.Text = Convert.ToString(GetCrewRatingList[0].Night1 + TotalNight);
            ////        ////tbPICHrsInstrumentdiaplay.Text = Convert.ToString(GetCrewRatingList[0].Instrument + TotalInstr);
            ////        // PIC Hrs
            ////        tbPICHrsInTypedisplay.Text = Convert.ToString(GetCrewRatingList[0].PilotInCommandTypeHrs + TotalFlight);
            ////        tbPICHrsDaydisplay.Text = Convert.ToString(GetCrewRatingList[0].TPilotinCommandDayHrs + TotalDay);
            ////        tbPICHrsNightdisplay.Text = Convert.ToString(GetCrewRatingList[0].TPilotInCommandNightHrs + TotalNight);
            ////        tbPICHrsInstrumentdiaplay.Text = Convert.ToString(GetCrewRatingList[0].TPilotInCommandINSTHrs + TotalInstr);
            ////        // SIC Hrs
            ////        tbSICInTypeHrsdisplay.Text = Convert.ToString(GetCrewRatingList[0].SecondInCommandTypeHrs + TotalFlight);
            ////        tbSICHrsDaydisplay.Text = Convert.ToString(GetCrewRatingList[0].SecondInCommandDayHrs + TotalDay);
            ////        tbSICHrsNightdisplay.Text = Convert.ToString(GetCrewRatingList[0].SecondInCommandNightHrs + TotalNight);
            ////        tbSICHrsInstrumentdisplay.Text = Convert.ToString(GetCrewRatingList[0].SecondInCommandINSTHrs + TotalInstr);
            ////        // Other Hrs
            ////        tbOtherHrsSimulatordisplay.Text = Convert.ToString(GetCrewRatingList[0].OtherSimHrs + TotalFlight);
            ////        tbOtherHrsFltEngineerdisplay.Text = Convert.ToString(GetCrewRatingList[0].OtherFlightEngHrs + TotalDay);
            ////        tbOtherHrsFltInstructordisplay.Text = Convert.ToString(GetCrewRatingList[0].OtherFlightInstrutorHrs + TotalNight);
            ////        tbOtherHrsFltAttendantdisplay.Text = Convert.ToString(GetCrewRatingList[0].OtherFlightAttdHrs + TotalInstr);
            ////        TextboxadjustmentsCall();
            ////    }
            ////    else
            ////    {
            ////        // Total Hrs
            ////        tbTotalHrsInTypedisplay.Text = string.Empty;
            ////        tbTotalHrsDayDisplay.Text = string.Empty;
            ////        tbTotalHrsNightDisplay.Text = string.Empty;
            ////        tbTotalHrsInstrumentDisplay.Text = string.Empty;

            ////        // PIC Hrs
            ////        tbPICHrsInTypedisplay.Text = string.Empty;
            ////        tbPICHrsDaydisplay.Text = string.Empty;
            ////        tbPICHrsNightdisplay.Text = string.Empty;
            ////        tbPICHrsInstrumentdiaplay.Text = string.Empty;

            ////        // SIC Hrs
            ////        tbSICInTypeHrsdisplay.Text = string.Empty;
            ////        tbSICHrsDaydisplay.Text = string.Empty;
            ////        tbSICHrsNightdisplay.Text = string.Empty;
            ////        tbSICHrsInstrumentdisplay.Text = string.Empty;

            ////        // Other Hrs
            ////        tbOtherHrsSimulatordisplay.Text = string.Empty;
            ////        tbOtherHrsFltEngineerdisplay.Text = string.Empty;
            ////        tbOtherHrsFltInstructordisplay.Text = string.Empty;
            ////        tbOtherHrsFltAttendantdisplay.Text = string.Empty;
            ////        TextboxadjustmentsCall();
            ////    }
            ////}

            CalulateDisplayCurrentFlightExp(CrewID, AircraftID);
        }

        #endregion
        #region "Type Rating Textchanged Events & Check Changed Events"
        private bool ValidateText(string text)
        {
            bool IsCheck = true;
            if (UserPrincipal.Identity != null)
            {
                Regex Tenths = new Regex("^[0-9]{0,7}(\\.[0-9]{0,1})?$");
                Regex Minutes = new Regex("^[0-9]{0,7}(:([0-5]{0,1}[0-9]{0,1}))?$");
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        if (!Tenths.IsMatch(text))
                        {
                            string msgToDisplay = "You entered: " + text + "." + "<br/>" + "Its a Invalid Format." + "<br/>" + " The Format is NNNNNNN.N";
                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                            IsCheck = false;
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        if (!Minutes.IsMatch(text))
                        {
                            if (text.IndexOf(":") != -1)
                            {
                                string[] timeArray = text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    Int64 result;
                                    bool isNum = Int64.TryParse(timeArray[1], out result);
                                    if (isNum)
                                    {
                                        if (result > 59)
                                        {
                                            string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Minute entry must be between 0 and 59.";
                                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                            IsCheck = false;
                                        }
                                        else
                                        {
                                            string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Minute entry must be between 0 and 59.";
                                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                            IsCheck = false;
                                        }
                                    }
                                    else
                                    {

                                        string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Minute entry must be between 0 and 59.";
                                        RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                        IsCheck = false;
                                    }

                                }
                                else
                                {
                                    string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Invalid Format. HHHHHHH:MM";
                                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                    IsCheck = false;
                                }
                            }
                            else
                            {
                                string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Invalid Format." + "<br/>" + " The Format is HHHHHHH:MM";
                                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                IsCheck = false;
                            }
                        }

                    }
                }
            }
            return IsCheck;
        }
        protected void Typerating_TextChanged(object sender, EventArgs e)
        {
            if (dgTypeRating.SelectedItems.Count > 0)
            {
                GridDataItem Item = dgTypeRating.SelectedItems[0] as GridDataItem;
                string TextboxID = ((TextBox)sender).ID;
                string TextboxValue = ((TextBox)sender).Text.Trim();
                if (TextboxID.ToUpper() == "tbAsOfDt".ToUpper())
                {
                    if (tbAsOfDt.Text.Trim() != "")
                    {
                        if (Convert.ToDateTime(FormatDate(tbAsOfDt.Text.Trim(), DateFormat)) <= System.DateTime.Now)
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text = System.Web.HttpUtility.HtmlEncode(tbAsOfDt.Text);
                        }
                        else
                        {

                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Future dates not allowed. Enter Valid Date', 360, 50, 'Crew Roster Catalog');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            tbAsOfDt.Text = "";
                        }
                    }
                    else
                    {
                        ((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text = System.Web.HttpUtility.HtmlEncode("");
                    }
                    return;
                }

                hdnTempHoursTxtbox.Value = TextboxID;
                if (ValidateText(TextboxValue))
                {
                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null))
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            if ((TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != "."))
                            {
                                if (TextboxValue.IndexOf(":") != -1)
                                {
                                    string[] timeArray = TextboxValue.Split(':');
                                    if (string.IsNullOrEmpty(timeArray[0]))
                                    {
                                        timeArray[0] = "0";
                                        ((TextBox)sender).Text = timeArray[0] + ":" + timeArray[1];
                                    }
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        Int64 result = 0;
                                        string timeHours;
                                        if (timeArray[1].Length != 2)
                                        {
                                            result = (Convert.ToInt32(timeArray[1])) / 10;
                                            if (result < 1)
                                            {
                                                if (timeArray[0] != null)
                                                {
                                                    if (timeArray[0].Trim() == "")
                                                    {
                                                        timeArray[0] = "0";
                                                    }
                                                }
                                                else
                                                {
                                                    timeArray[0] = "0";
                                                }
                                                ((TextBox)sender).Text = timeArray[0] + ":" + "0" + timeArray[1];

                                            }
                                        }
                                    }
                                    else
                                    {
                                        ((TextBox)sender).Text = TextboxValue + "00";
                                    }
                                }
                                else
                                {
                                    ((TextBox)sender).Text = TextboxValue + ":00";
                                }
                            }
                            else
                            {
                                ((TextBox)sender).Text = "00:00";
                            }
                        }
                        else
                        {
                            if ((TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != "."))
                            {
                                if (TextboxValue.IndexOf(".") != -1)
                                {
                                    string[] timeArray = TextboxValue.Split('.');
                                    if (string.IsNullOrEmpty(timeArray[0]))
                                    {
                                        timeArray[0] = "0";
                                        ((TextBox)sender).Text = timeArray[0] + "." + timeArray[1];
                                    }
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        if (timeArray[0] != null)
                                        {
                                            if (timeArray[0].Trim() == "")
                                            {
                                                timeArray[0] = "0";
                                            }
                                        }
                                        else
                                        {
                                            timeArray[0] = "0";
                                        }
                                        ((TextBox)sender).Text = timeArray[0] + "." + timeArray[1];
                                    }
                                    else
                                    {
                                        ((TextBox)sender).Text = TextboxValue + "0";
                                    }
                                }
                                else
                                {
                                    ((TextBox)sender).Text = TextboxValue + ".0";
                                }
                            }
                            else
                            {
                                ((TextBox)sender).Text = "0.0";
                            }

                        }
                    }
                    else
                    {
                        if ((TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != "."))
                        {
                            if (TextboxValue.IndexOf(".") != -1)
                            {
                                string[] timeArray = TextboxValue.Split('.');
                                if (string.IsNullOrEmpty(timeArray[0]))
                                {
                                    timeArray[0] = "0000000";
                                    ((TextBox)sender).Text = timeArray[0] + "." + timeArray[1];
                                }
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    if (timeArray[0] != null)
                                    {
                                        if (timeArray[0].Trim() == "")
                                        {
                                            timeArray[0] = "0000000";
                                        }
                                    }
                                    else
                                    {
                                        timeArray[0] = "0000000";
                                    }
                                    ((TextBox)sender).Text = timeArray[0] + "." + "0";
                                }
                                else
                                {
                                    ((TextBox)sender).Text = TextboxValue + "0";
                                }
                            }
                            else
                            {
                                ((TextBox)sender).Text = TextboxValue + ".0";
                            }
                        }
                        else
                        {
                            ((TextBox)sender).Text = "0.0";
                        }


                    }
                    //1
                    if (TextboxID.ToUpper() == "tbTotalHrsInType".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalHrsInType.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbTotalHrsInType.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalHrsInType.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbTotalHrsInType.Text)) : "0");

                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTotalHrsDay);
                    }
                    //2
                    if (TextboxID.ToUpper() == "tbTotalHrsDay".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalHrsDay.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbTotalHrsDay.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalHrsDay.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbTotalHrsDay.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTotalHrsNight);
                    }

                    //3
                    if (TextboxID.ToUpper() == "tbTotalHrsNight".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalHrsNight.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbTotalHrsNight.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalHrsNight.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbTotalHrsNight.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTotalHrsInstrument);
                    }
                    //4
                    if (TextboxID.ToUpper() == "tbTotalHrsInstrument".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalHrsInstrument.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbTotalHrsInstrument.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalHrsInstrument.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbTotalHrsInstrument.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPICHrsInType);
                    }
                    // PIC Hrs
                    //5
                    if (TextboxID.ToUpper() == "tbPICHrsInType".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text = System.Web.HttpUtility.HtmlEncode((tbPICHrsInType.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPICHrsInType.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text = System.Web.HttpUtility.HtmlEncode((tbPICHrsInType.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbPICHrsInType.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPICHrsDay);
                    }

                    //6
                    if (TextboxID.ToUpper() == "tbPICHrsDay".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text = System.Web.HttpUtility.HtmlEncode((tbPICHrsDay.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPICHrsDay.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text = System.Web.HttpUtility.HtmlEncode((tbPICHrsDay.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbPICHrsDay.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPICHrsNight);
                    }
                    //7
                    if (TextboxID.ToUpper() == "tbPICHrsNight".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text = System.Web.HttpUtility.HtmlEncode((tbPICHrsNight.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPICHrsNight.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text = System.Web.HttpUtility.HtmlEncode((tbPICHrsNight.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbPICHrsNight.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPICHrsInstrument);
                    }

                    //8
                    if (TextboxID.ToUpper() == "tbPICHrsInstrument".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text = System.Web.HttpUtility.HtmlEncode((tbPICHrsInstrument.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPICHrsInstrument.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text = System.Web.HttpUtility.HtmlEncode((tbPICHrsInstrument.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbPICHrsInstrument.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSICInTypeHrs);
                    }
                    // SIC Hrs
                    //9
                    if (TextboxID.ToUpper() == "tbSICInTypeHrs".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbSICInTypeHrs.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbSICInTypeHrs.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbSICInTypeHrs.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbSICInTypeHrs.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSICHrsDay);
                    }
                    //10
                    if (TextboxID.ToUpper() == "tbSICHrsDay".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text = System.Web.HttpUtility.HtmlEncode((tbSICHrsDay.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbSICHrsDay.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text = System.Web.HttpUtility.HtmlEncode((tbSICHrsDay.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbSICHrsDay.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSICHrsNight);
                    }

                    //11
                    if (TextboxID.ToUpper() == "tbSICHrsNight".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text = System.Web.HttpUtility.HtmlEncode((tbSICHrsNight.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbSICHrsNight.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text = System.Web.HttpUtility.HtmlEncode((tbSICHrsNight.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbSICHrsNight.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSICHrsInstrument);
                    }

                    //12
                    if (TextboxID.ToUpper() == "tbSICHrsInstrument".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text = System.Web.HttpUtility.HtmlEncode((tbSICHrsInstrument.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbSICHrsInstrument.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text = System.Web.HttpUtility.HtmlEncode((tbSICHrsInstrument.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbSICHrsInstrument.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbOtherHrsSimulator);
                    }


                    // SIC Hrs
                    //13
                    if (TextboxID.ToUpper() == "tbOtherHrsSimulator".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text = System.Web.HttpUtility.HtmlEncode((tbOtherHrsSimulator.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbOtherHrsSimulator.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text = System.Web.HttpUtility.HtmlEncode((tbOtherHrsSimulator.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbOtherHrsSimulator.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbOtherHrsFltEngineer);
                    }

                    //14
                    if (TextboxID.ToUpper() == "tbOtherHrsFltEngineer".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text = System.Web.HttpUtility.HtmlEncode((tbOtherHrsFltEngineer.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbOtherHrsFltEngineer.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text = System.Web.HttpUtility.HtmlEncode((tbOtherHrsFltEngineer.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbOtherHrsFltEngineer.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbOtherHrsFltInstructor);
                    }
                    //15
                    if (TextboxID.ToUpper() == "tbOtherHrsFltInstructor".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text = System.Web.HttpUtility.HtmlEncode((tbOtherHrsFltInstructor.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbOtherHrsFltInstructor.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text = System.Web.HttpUtility.HtmlEncode((tbOtherHrsFltInstructor.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbOtherHrsFltInstructor.Text)) : "0");
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbOtherHrsFltAttendant);
                    }
                    //16
                    if (TextboxID.ToUpper() == "tbOtherHrsFltAttendant".ToUpper())
                    {
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text = System.Web.HttpUtility.HtmlEncode((tbOtherHrsFltAttendant.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbOtherHrsFltAttendant.Text, true, "GENERAL")), 1)) : "0");
                        }
                        else
                        {
                            ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text = System.Web.HttpUtility.HtmlEncode((tbOtherHrsFltAttendant.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbOtherHrsFltAttendant.Text)) : "0");
                        }
                    }


                }
            }
        }
        protected void TypeRating_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgTypeRating.SelectedItems[0] as GridDataItem;
                            string text = ((CheckBox)sender).ID;

                            if (text.ToUpper() == "chkTRPIC91".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")).Checked = chkTRPIC91.Checked;
                            }
                            if (text.ToUpper() == "chkTRSIC91".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")).Checked = chkTRSIC91.Checked;
                            }
                            if (text.ToUpper() == "chkTREngineer".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked = chkTREngineer.Checked;
                            }
                            if (text.ToUpper() == "chkPIC121".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked = chkPIC121.Checked;
                            }
                            if (text.ToUpper() == "chkSIC121".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked = chkSIC121.Checked;
                            }
                            if (text.ToUpper() == "chkPIC125".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked = chkPIC125.Checked;
                            }
                            if (text.ToUpper() == "chkSIC125".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked = chkSIC125.Checked;
                            }
                            if (text.ToUpper() == "chkAttendent121".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked = chkAttendent121.Checked;
                            }
                            if (text.ToUpper() == "chkAttendent125".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked = chkAttendent125.Checked;
                            }
                            if (text.ToUpper() == "chkTRInstructor".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked = chkTRInstructor.Checked;
                            }
                            if (text.ToUpper() == "chkTRAttendant".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked = chkTRAttendant.Checked;
                            }
                            if (text.ToUpper() == "chkTRAttendant91".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked = chkTRAttendant91.Checked;
                            }
                            if (text.ToUpper() == "chkTRAttendant135".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked = chkTRAttendant135.Checked;
                            }
                            if (text.ToUpper() == "chkTRAirman".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked = chkTRAirman.Checked;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #region "Checkbox Changed Events"
        protected void chkTRInactive_CheckedChanged(object sender, EventArgs e)
        {
            if (dgTypeRating.Items.Count != 0)
            {
                if (dgTypeRating.SelectedItems.Count != 0)
                {
                    GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                    CheckBox chkTRInactive1 = (CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRInactive");
                    if (chkTRInactive.Checked == true)
                    {
                        chkTRInactive1.Checked = true;
                    }
                    else
                    {
                        chkTRInactive1.Checked = false;
                    }
                }
                else
                {

                    string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                        + @" oManager.radalert('Please select a record in the grid.', 360, 50, 'Crew Roster - Type Rating');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                }
            }
            else
            {

                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                    + @" oManager.radalert('Please Add a Record in the Grid and Select it', 360, 50, 'Crew Roster - Type Rating');";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
            }
        }
        /// <summary>
        /// Method to Select All / De-Select All in Email Preferences Section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectAll_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkSelectAll.Checked == true)
                        {
                            EnableEmailPreferences(true);
                            chkOutboundInstComments.Checked = false;
                            chkTripNotes.Checked = false;
                            chkPAXNotes.Checked = false; chkTripAlerts.Checked = false;
                            chkPAXArrTransComments.Checked = false;
                            chkArrCaterComments.Checked = false;
                            chkPAXArrTransConfirm.Checked = false;
                            chkArrCaterConfirm.Checked = false;
                            chkPAXDeptTransComments.Checked = false;
                            chkDeptCaterComments.Checked = false;
                            chkPAXDeptTransConfirm.Checked = false;
                            chkDeptCaterConfirm.Checked = false;
                            chkPAXHotelComments.Checked = false;
                            chkDeptFBOComments.Checked = false;
                            chkPAXHotelConfirm.Checked = false;
                            chkDeptFBOConfirm.Checked = false;
                            chkCrewNotes.Checked = false;
                            chkArrFBOComments.Checked = false;
                            chkCrewArrTransComments.Checked = false;
                            chkArrFBOConfirm.Checked = false;
                            chkCrewArrTransConfirm.Checked = false;
                            chkArrAirportAlerts.Checked = false;
                            chkCrewDeptTransComments.Checked = false;
                            chkDeptAirportAlerts.Checked = false;
                        }
                        else
                        {
                            EnableEmailPreferences(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Check Change Event for Trip Pilot in Command 91 Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TRPIC91_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                                CheckBox chkIsPilotinCommand = (CheckBox)SelectedItem["IsPilotinCommand"].FindControl("chkIsPilotinCommand");
                                if (chkTRPIC91.Checked == true)
                                {
                                    chkIsPilotinCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRPIC91")).Checked = true;
                                }
                                else
                                {
                                    if (chkPIC121.Checked == false && chkPIC125.Checked == false && chkPIC135.Checked == false && chkTRPIC91.Checked == false)
                                        chkIsPilotinCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRPIC91")).Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Check Change Event for Second in Command 91 Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TRSIC91_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                                CheckBox chkIsSecondInCommand = (CheckBox)SelectedItem["IsSecondInCommand"].FindControl("chkIsSecondInCommand");
                                if (chkTRSIC91.Checked == true)
                                {
                                    chkIsSecondInCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRSIC91")).Checked = true;
                                }
                                else
                                {
                                    if (chkSIC121.Checked == false && chkSIC125.Checked == false && chkSIC135.Checked == false && chkTRSIC91.Checked == false)
                                        chkIsSecondInCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRSIC91")).Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Check Change Event for Pilot in Command 135 Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TRPIC135_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                                CheckBox chkIsPilotinCommand = (CheckBox)SelectedItem["IsPilotinCommand"].FindControl("chkIsPilotinCommand");
                                if (chkTRPIC135.Checked == true)
                                {
                                    chkIsPilotinCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked = true;
                                }
                                else
                                {
                                    if (chkPIC121.Checked == false && chkPIC125.Checked == false && chkPIC135.Checked == false && chkTRPIC91.Checked == false)
                                        chkIsPilotinCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        /// <summary>
        /// Check Change Event for Second in Command 135 Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TRSIC135_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];

                                CheckBox chkIsSecondInCommand = (CheckBox)SelectedItem["IsSecondInCommand"].FindControl("chkIsSecondInCommand");
                                if (chkTRSIC135.Checked == true)
                                {
                                    chkIsSecondInCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked = true;
                                }
                                else
                                {
                                    if (chkSIC121.Checked == false && chkSIC125.Checked == false && chkSIC135.Checked == false && chkTRSIC91.Checked == false)
                                        chkIsSecondInCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void TRSIC121_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                                CheckBox chkIsSecondInCommand = (CheckBox)SelectedItem["IsSecondInCommand"].FindControl("chkIsSecondInCommand");
                                if (chkSIC121.Checked == true)
                                {
                                    chkIsSecondInCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked = true;
                                }
                                else
                                {
                                    if (chkSIC121.Checked == false && chkSIC125.Checked == false && chkSIC135.Checked == false && chkTRSIC91.Checked == false)
                                        chkIsSecondInCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void TRPIC121_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                                CheckBox chkIsPilotinCommand = (CheckBox)SelectedItem["IsPilotinCommand"].FindControl("chkIsPilotinCommand");
                                if (chkPIC121.Checked == true)
                                {
                                    chkIsPilotinCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked = true;
                                }
                                else
                                {
                                    if (chkPIC121.Checked == false && chkPIC125.Checked == false && chkPIC135.Checked == false && chkTRPIC91.Checked == false)
                                        chkIsPilotinCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void TRSIC125_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                                CheckBox chkIsSecondInCommand = (CheckBox)SelectedItem["IsSecondInCommand"].FindControl("chkIsSecondInCommand");
                                if (chkSIC125.Checked == true)
                                {
                                    chkIsSecondInCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked = true;
                                }
                                else
                                {
                                    if (chkSIC121.Checked == false && chkSIC125.Checked == false && chkSIC135.Checked == false && chkTRSIC91.Checked == false)
                                        chkIsSecondInCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void TRPIC125_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                                CheckBox chkIsPilotinCommand = (CheckBox)SelectedItem["IsPilotinCommand"].FindControl("chkIsPilotinCommand");
                                if (chkPIC125.Checked == true)
                                {
                                    chkIsPilotinCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked = true;
                                }
                                else
                                {
                                    if (chkPIC121.Checked == false && chkPIC125.Checked == false && chkPIC135.Checked == false && chkTRPIC91.Checked == false)
                                        chkIsPilotinCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #region "Crew Type Rating Button Events"
        /// <summary>
        /// Delete Selected Crew Roster Rating Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteRating_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnDelete.Value == "Yes")
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem item = (GridDataItem)dgTypeRating.SelectedItems[0];
                                Int64 CrewRatingID = Convert.ToInt64(item.GetDataKeyValue("CrewRatingID").ToString());
                                string AircraftTypeCD = item.GetDataKeyValue("AircraftTypeCD").ToString();
                                List<FlightPakMasterService.GetCrewRating> CrewRatingList = new List<FlightPakMasterService.GetCrewRating>();
                                CrewRatingList = (List<FlightPakMasterService.GetCrewRating>)Session["CrewTypeRating"];
                                for (int Index = 0; Index < CrewRatingList.Count; Index++)
                                {
                                    if (CrewRatingList[Index].AircraftTypeCD == AircraftTypeCD)
                                    {
                                        CrewRatingList[Index].IsDeleted = true;
                                    }
                                }
                                Session["CrewTypeRating"] = CrewRatingList;
                                List<GetCrewRating> CrewRatingInfoList = new List<GetCrewRating>();
                                CrewRatingInfoList = RetainCrewTypeRatingGrid(dgTypeRating);
                                for (int Index = 0; Index < CrewRatingInfoList.Count; Index++)
                                {
                                    if (CrewRatingInfoList[Index].AircraftTypeCD == AircraftTypeCD)
                                    {
                                        CrewRatingInfoList[Index].IsDeleted = true;
                                    }
                                }
                                dgTypeRating.DataSource = CrewRatingInfoList.Where(x => x.IsDeleted == false).ToList();
                                dgTypeRating.DataBind();
                                DefaultTypeRatingSelection();
                                Session["CrewTypeRating"] = CrewRatingList;
                                EnableHours(false);
                            }
                            else
                            {

                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Please select the record from Rating table', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void UpdateFltExperience_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgTypeRating.SelectedItems[0] as GridDataItem;
                            Item.Selected = true;
                            List<GetCrewRating> CrewRateList = (List<GetCrewRating>)Session["CrewTypeRating"];
                            if (Item.GetDataKeyValue("AircraftTypeCD") != null)
                            {
                                CrewRatingAircraftCD = Item.GetDataKeyValue("AircraftTypeCD").ToString();
                            }
                            if (Item.GetDataKeyValue("CrewID") != null)
                            {
                                CrewRatingCrewID = Convert.ToInt64(Item.GetDataKeyValue("CrewID").ToString());
                            }
                            CrewRateList[0].TotalUpdateAsOfDT = DateTime.Now;
                            CrewRateList.Where(x => x.AircraftTypeCD == Item.GetDataKeyValue("AircraftTypeCD").ToString()).ToList().ForEach(y => SetTypeRating(y, GetCrewRatingItems(Item)));
                            Session["DisplayCrewTypeRating"] = CrewRateList;
                            dgTypeRating.DataSource = CrewRateList.Where(x => x.IsDeleted == false).ToList();
                            dgTypeRating.DataBind();

                            foreach (GridDataItem TyItem in dgTypeRating.MasterTableView.Items)
                            {
                                if (TyItem.GetDataKeyValue("AircraftTypeCD").ToString() == CrewRatingAircraftCD)
                                {
                                    TyItem.Selected = true;
                                }
                            }
                            //Session
                            UpdateDisplayCurrentFlightExp(CrewRatingCrewID, Convert.ToInt64(Item.GetDataKeyValue("AircraftTypeID").ToString()));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to Update values from Form and bind into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateAllFltExperience_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.MasterTableView.Items.Count > 0)
                        {
                            List<GetCrewRating> CrewRateList = (List<GetCrewRating>)Session["CrewTypeRating"];
                            for (int Check = 0; Check < CrewRateList.Count; Check++)
                            {
                                if (CrewRateList[Check].IsDeleted == true)
                                {
                                    DeletedAircraftType.Add(Convert.ToInt64(CrewRateList[Check].AircraftTypeID));
                                }
                            }
                            foreach (GridDataItem Item in dgTypeRating.MasterTableView.Items)
                            {
                                if (dgTypeRating.SelectedItems.Count > 0)
                                {
                                    GridDataItem SItem = dgTypeRating.SelectedItems[0] as GridDataItem;
                                    Item.Selected = false;
                                }
                                if (Item.GetDataKeyValue("CrewRatingID") != null)
                                {
                                    CrewRatingCrewID = Convert.ToInt64(Item.GetDataKeyValue("CrewRatingID").ToString());
                                }
                                else
                                {
                                    CrewRatingCrewID = 0;
                                }
                                CrewRateList.Where(x => x.AircraftTypeCD.Trim().ToUpper() == Item.GetDataKeyValue("AircraftTypeCD").ToString().Trim().ToUpper()).ToList().ForEach(y => SetTypeRating(y, GetCrewRatingItems(Item)));
                            }
                            if (DeletedAircraftType != null)
                            {
                                for (int Check1 = 0; Check1 < CrewRateList.Count; Check1++)
                                {
                                    if (DeletedAircraftType.Contains(Convert.ToInt64(CrewRateList[Check1].AircraftTypeID)))
                                    {
                                        if (Session["SelectedCrewRosterID"] != null)
                                        {
                                            CrewRateList[Check1].CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"]);
                                        }
                                        else
                                        {
                                            CrewRateList[Check1].CrewID = -1;
                                        }
                                        CrewRateList[Check1].IsDeleted = true;
                                    }
                                }
                            }
                            foreach (GetCrewRating List in CrewRateList)
                            {
                                List.TotalUpdateAsOfDT = DateTime.Now;
                                //UpdateDisplayCurrentFlightExp(CrewRatingCrewID, Convert.ToInt64(Item.GetDataKeyValue("AircraftTypeID").ToString()));
                            }
                            Session["CrewTypeRating"] = CrewRateList;
                            dgTypeRating.DataSource = CrewRateList.Where(x => x.IsDeleted == false).ToList();
                            dgTypeRating.DataBind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #endregion
        #region "Crew Checklist"
        private bool ValidateTotalFltHours(string text)
        {
            bool IsCheck = true;
            if (UserPrincipal.Identity != null)
            {
                Regex Tenths = new Regex("^[0-9]{0,5}(\\.[0-9]{0,1})?$");
                Regex Minutes = new Regex("^[0-9]{0,5}(:([0-5]{0,1}[0-9]{0,1}))?$");
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        if (!Tenths.IsMatch(text))
                        {
                            string msgToDisplay = "You entered: " + text + "." + "<br/>" + "Its a Invalid Format." + "<br/>" + " The Format is NNNNN.N";
                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                            IsCheck = false;
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        if (!Minutes.IsMatch(text))
                        {
                            if (text.IndexOf(":") != -1)
                            {
                                string[] timeArray = text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    Int64 result;
                                    bool isNum = Int64.TryParse(timeArray[1], out result);
                                    if (isNum)
                                    {
                                        if (result > 59)
                                        {
                                            string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Minute entry must be between 0 and 59.";
                                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                            IsCheck = false;
                                        }
                                        else
                                        {
                                            string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Minute entry must be between 0 and 59.";
                                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                            IsCheck = false;
                                        }
                                    }
                                    else
                                    {

                                        string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Minute entry must be between 0 and 59.";
                                        RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                        IsCheck = false;
                                    }

                                }
                                else
                                {
                                    string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Invalid Format. HHHHH:MM";
                                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                    IsCheck = false;
                                }
                            }
                            else
                            {
                                string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Invalid Format." + "<br/>" + " The Format is HHHHH:MM";
                                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Crew Roster", "alertCrewCallBackFn");
                                IsCheck = false;
                            }
                        }

                    }
                }
            }
            return IsCheck;
        }
        #region "Crew Roster Crew CheckList Grid Events"
        /// <summary>
        /// Method to Enable or Disable Crew Checklist Form Fields
        /// </summary>
        /// <param name="Enable">Pass Boolean Value</param>
        protected void EnableCheckListFields(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Enable == true)
                    {
                        if (btnSaveChanges.Visible == true)
                        {
                            Enable = true;
                        }
                        else
                        {
                            Enable = false;
                        }
                    }
                    tbOriginalDate.Enabled = Enable;
                    tbChgBaseDate.Enabled = Enable;
                    tbFreq.Enabled = Enable;
                    radFreq.Enabled = Enable;
                    tbPrevious.Enabled = Enable;
                    tbDueNext.Enabled = Enable;
                    tbAlertDate.Enabled = Enable;
                    tbAlertDays.Enabled = Enable;
                    tbGraceDate.Enabled = Enable;
                    tbGraceDays.Enabled = Enable;
                    tbAircraftType.Enabled = Enable;

                    chkPIC91.Enabled = Enable;
                    chkPIC135.Enabled = Enable;
                    chkSIC91.Enabled = Enable;
                    chkSIC135.Enabled = Enable;
                    chkInactive.Enabled = Enable;
                    chkSetToEndOfMonth.Enabled = Enable;
                    chkSetToNextOfMonth.Enabled = Enable;
                    chkSetToEndOfCalenderYear.Enabled = Enable;
                    ckhDisableDateCalculation.Enabled = Enable;
                    chkOneTimeEvent.Enabled = Enable;
                    chkNonConflictedEvent.Enabled = Enable;
                    chkRemoveFromCrewRosterRept.Enabled = Enable;
                    chkRemoveFromChecklistRept.Enabled = Enable;
                    chkDoNotPrintPassDueAlert.Enabled = Enable;


                    if (Enable == false)
                    {
                        tbTotalReqFlightHrs.Enabled = false;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tbAircraftType.Text))
                        {
                            tbTotalReqFlightHrs.Enabled = true;
                        }
                        else
                        {
                            tbTotalReqFlightHrs.Enabled = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            int index = Convert.ToInt32(HttpContext.Current.Session["index"] == null ? 0 : HttpContext.Current.Session["index"]);
                            GridDataItem OldItem = (GridDataItem)dgCrewCheckList.Items[index];
                            if (OldItem != null) { 
                                setColorCheckListRowItem(OldItem,true);
                            }
                            EnableCheckListFields(true);
                            BindSelectedCrewChecklistItem();
                            GridDataItem InsertItem = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            setColorCheckListRowItem(InsertItem);
                            Session["CrewCD"] = tbCrewCode.Text;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Edit Row styles, based on Crew Rating.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            if (e.Item is GridDataItem)
                            {
                                GridDataItem Item = (GridDataItem)e.Item;
                                ////1
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                {
                                    if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text))
                                        ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode("0:00");
                                    else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text.Trim() == ""))
                                        ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode("0:00");
                                    else
                                        ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text).ToString()), 3).ToString()));

                                    //if (((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text != "0:00")
                                    //{
                                    if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text))
                                        ((Label)Item["FlightLogHours"].FindControl("lbFlightLogHours")).Text = System.Web.HttpUtility.HtmlEncode("0:00");
                                    else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text.Trim() == ""))
                                        ((Label)Item["FlightLogHours"].FindControl("lbFlightLogHours")).Text = System.Web.HttpUtility.HtmlEncode("0:00");
                                    else
                                        ((Label)Item["FlightLogHours"].FindControl("lbFlightLogHours")).Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text).ToString()), 3).ToString()));
                                    //}
                                    //else
                                    //{
                                    //    ((Label)Item["FlightLogHours"].FindControl("lbFlightLogHours")).Text = "0:00";
                                    //}
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text))
                                        ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                    else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text.Trim() == ""))
                                        ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                    else
                                        ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode(AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text)));

                                    //if (((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text != "0.0")
                                    //{
                                    if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text))
                                        ((Label)Item["FlightLogHours"].FindControl("lbFlightLogHours")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                    else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text.Trim() == ""))
                                        ((Label)Item["FlightLogHours"].FindControl("lbFlightLogHours")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                    else
                                        ((Label)Item["FlightLogHours"].FindControl("lbFlightLogHours")).Text = System.Web.HttpUtility.HtmlEncode(AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblFlightLogHours")).Text)));
                                    //}
                                    //else
                                    //{
                                    //    ((Label)Item["FlightLogHours"].FindControl("lbFlightLogHours")).Text = "0.0";
                                    //}
                                }
                                string TestDueDT = ((Label)(Item["DueDT"].FindControl("lbDueDT"))).Text;
                                string TestAlertDT = ((Label)(Item["AlertDT"].FindControl("lbAlertDT"))).Text;
                                string TestGraceDT = ((Label)(Item["GraceDT"].FindControl("lbGraceDT"))).Text;
                                string TestPreviousDT = ((Label)(Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT"))).Text;
                                string TestBaseDT = ((Label)(Item["BaseMonthDT"].FindControl("lbBaseMonthDT"))).Text;
                                bool NonConflictEvent = Convert.ToBoolean(((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked, CultureInfo.CurrentCulture);
                                bool IsScheduleCheckFlag = false;
                                if (Session["CheckListSetting"] != null)
                                {
                                    List<FlightPakMasterService.CrewCheckList> GetCrewCheckList = new List<FlightPakMasterService.CrewCheckList>();
                                    GetCrewCheckList = (List<FlightPakMasterService.CrewCheckList>)Session["CheckListSetting"];
                                    var objScheduleCheck = GetCrewCheckList.Where(x => (x.CrewCheckCD == Item["ChecklistCD"].Text.Trim()) && (x.IsScheduleCheck == true));
                                    if (objScheduleCheck.Count() > 0)
                                    {
                                        Item.CssClass = "Expired-rec";
                                        IsScheduleCheckFlag = true;
                                    }
                                    else
                                    {
                                        Item.CssClass = "Normal-rec";
                                    }
                                }
                                if (TestDueDT != string.Empty)
                                {
                                    ((Label)(Item["DueDT"].FindControl("lbDueDT"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestDueDT)));
                                }

                                if (TestAlertDT != string.Empty)
                                {
                                    ((Label)(Item["AlertDT"].FindControl("lbAlertDT"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestAlertDT)));
                                }

                                if (TestGraceDT != string.Empty)
                                {
                                    ((Label)(Item["GraceDT"].FindControl("lbGraceDT"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestGraceDT)));
                                }

                                if (TestPreviousDT != string.Empty)
                                {
                                    ((Label)(Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestPreviousDT)));
                                }

                                if (TestBaseDT != string.Empty)
                                {
                                    ((Label)(Item["BaseMonthDT"].FindControl("lbBaseMonthDT"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestBaseDT)));
                                }

                                if (IsScheduleCheckFlag == true && (!string.IsNullOrEmpty(TestDueDT)) && (!string.IsNullOrEmpty(TestAlertDT)) && (!string.IsNullOrEmpty(TestGraceDT)))
                                {
                                    DateTime DueNextSource = DateTime.MinValue;
                                    DueNextSource = Convert.ToDateTime(FormatDate(((Label)(Item["DueDT"].FindControl("lbDueDT"))).Text, DateFormat));

                                    DateTime AlertSource1 = DateTime.MinValue;
                                    AlertSource1 = Convert.ToDateTime(FormatDate(((Label)(Item["AlertDT"].FindControl("lbAlertDT"))).Text, DateFormat));

                                    DateTime GraceSource2 = DateTime.MinValue;
                                    GraceSource2 = Convert.ToDateTime(FormatDate(((Label)(Item["GraceDT"].FindControl("lbGraceDT"))).Text, DateFormat));

                                    if ((DueNextSource > System.DateTime.Now) && (AlertSource1 > System.DateTime.Now) && (GraceSource2 > System.DateTime.Now))
                                    {
                                        if (NonConflictEvent != true)
                                        {
                                            Item.CssClass = "Normal-rec";
                                        }
                                    }
                                    if (((DueNextSource < System.DateTime.Now) && (System.DateTime.Now < GraceSource2)) || ((GraceSource2 < System.DateTime.Now) && (System.DateTime.Now < DueNextSource)))
                                    {
                                        if (NonConflictEvent != true)
                                        {
                                            Item.CssClass = "Grace-rec";
                                        }
                                    }
                                    if ((System.DateTime.Now > DueNextSource) && (GraceSource2 < System.DateTime.Now))
                                    {
                                        if (NonConflictEvent != true)
                                        {
                                            Item.CssClass = "Expired-rec";
                                        }
                                    }
                                    if (((DueNextSource < System.DateTime.Now) && (System.DateTime.Now < AlertSource1)) || ((AlertSource1 < System.DateTime.Now) && (System.DateTime.Now < DueNextSource)))
                                    {
                                        if (NonConflictEvent != true)
                                        {
                                            Item.CssClass = "Alert-rec";
                                        }
                                    }
                                }
                                if (Item.GetDataKeyValue("OriginalDT") != null)
                                {
                                    ((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("OriginalDT").ToString()))));
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Bind Selected Crew Checklist Grid Item in the Fields
        /// </summary>
        private void BindSelectedCrewChecklistItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ClearCheckListFields();
                    GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                    
                    // Edited
                    HttpContext.Current.Session["index"] = Item.ItemIndex;
                    //--------------------------------------------

                    Item.Selected = true;
                    if (Item.GetDataKeyValue("CheckListCD") != null)
                    {
                        lbCheckListCode.Text = System.Web.HttpUtility.HtmlEncode(Item.GetDataKeyValue("CheckListCD").ToString());
                        Session["CrewChecklistCD"] = Item.GetDataKeyValue("CheckListCD").ToString();
                    }
                    lbCheckListDescription.Text = ((Label)Item["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text;
                    if (!string.IsNullOrEmpty(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text))
                    {
                        tbPrevious.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbPrevious.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["DueDT"].FindControl("lbDueDT")).Text))
                    {
                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["DueDT"].FindControl("lbDueDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbDueNext.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text))
                    {
                        tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbAlertDate.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text))
                    {
                        tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbGraceDate.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                    {
                        tbAlertDays.Text = ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text;
                    }
                    else
                    {
                        tbAlertDays.Text = "0";
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                    {
                        tbGraceDays.Text = ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text;
                    }
                    else
                    {
                        tbGraceDays.Text = "0";
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                    {
                        tbFreq.Text = ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text;
                    }
                    else
                    {
                        tbFreq.Text = "0";
                    }
                    if (((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text != string.Empty)
                    {
                        tbChgBaseDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbChgBaseDate.Text = string.Empty;
                    }
                    if (((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text != string.Empty)
                    {
                        tbOriginalDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbOriginalDate.Text = string.Empty;
                    }
                    if (((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text != string.Empty)
                    {
                        tbAircraftType.Text = ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text;
                    }
                    else
                    {
                        tbAircraftType.Text = string.Empty;
                    }
                    if (((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text != string.Empty)
                    {
                        tbTotalReqFlightHrs.Text = ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text;
                    }
                    else
                    {
                        tbTotalReqFlightHrs.Text = "";
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                    {
                        if (((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text == "1")
                        {
                            radFreq.Items[0].Selected = true;
                        }
                        else
                        {
                            radFreq.Items[1].Selected = true;
                        }
                    }
                    else
                    {
                        radFreq.Items[0].Selected = true;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))) != null)
                    {
                        chkPIC91.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked;
                    }
                    else
                    {
                        chkPIC91.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))) != null)
                    {
                        chkPIC135.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked;
                    }
                    else
                    {
                        chkPIC135.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))) != null)
                    {
                        chkRemoveFromChecklistRept.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                    }
                    else
                    {
                        chkRemoveFromChecklistRept.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))) != null)
                    {
                        chkRemoveFromCrewRosterRept.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked;
                    }
                    else
                    {
                        chkRemoveFromCrewRosterRept.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))) != null)
                    {
                        chkPrintOneTimeEventCompleted.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked;
                    }
                    else
                    {
                        chkPrintOneTimeEventCompleted.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))) != null)
                    {
                        chkSIC91.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked;
                    }
                    else
                    {
                        chkSIC91.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))) != null)
                    {
                        chkSIC135.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked;
                    }
                    else
                    {
                        chkSIC135.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))) != null)
                    {
                        chkInactive.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                    }
                    else
                    {
                        chkInactive.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))) != null)
                    {
                        chkSetToEndOfMonth.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                    }
                    else
                    {
                        chkSetToEndOfMonth.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))) != null)
                    {
                        ckhDisableDateCalculation.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                    }
                    else
                    {
                        ckhDisableDateCalculation.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))) != null)
                    {
                        chkOneTimeEvent.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                    }
                    else
                    {
                        chkOneTimeEvent.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))) != null)
                    {
                        chkCompleted.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                    }
                    else
                    {
                        chkCompleted.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))) != null)
                    {
                        chkNonConflictedEvent.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                    }
                    else
                    {
                        chkNonConflictedEvent.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))) != null)
                    {
                        chkDoNotPrintPassDueAlert.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                    }
                    else
                    {
                        chkDoNotPrintPassDueAlert.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))) != null)
                    {
                        chkSetToNextOfMonth.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                    }
                    else
                    {
                        chkSetToNextOfMonth.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))) != null)
                    {
                        chkSetToEndOfCalenderYear.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                    }
                    else
                    {
                        chkSetToEndOfCalenderYear.Checked = false;
                    }
                    if (btnSaveChanges.Visible == true)
                    {
                        if (!string.IsNullOrEmpty(tbAircraftType.Text))
                        {
                            tbTotalReqFlightHrs.Enabled = true;
                        }
                        else
                        {
                            tbTotalReqFlightHrs.Enabled = false;
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Crew Checklist Grid based on Crew Code
        /// </summary>
        /// <param name="CrewID">Pass Crew Code</param>
        private void BindCrewCheckListGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Session["CrewCheckList"] != null)
                        {
                            List<GetCrewCheckListDate> CrewCheckList = (List<GetCrewCheckListDate>)Session["CrewCheckList"];
                            if (chkDisplayInactiveChecklist.Checked == true)
                            {
                                dgCrewCheckList.DataSource = CrewCheckList.Where(x => x.IsInActive == false);
                            }
                            else
                            {
                                dgCrewCheckList.DataSource = CrewCheckList;
                            }
                            dgCrewCheckList.DataBind();
                            // Bind Crew Checklist Currency Tab Grid
                            dgCurrencyChecklist.DataSource = CrewCheckList;
                            dgCurrencyChecklist.DataBind();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.GetCrewCheckListDate CrewChecklistDate = new FlightPakMasterService.GetCrewCheckListDate();
                                CrewChecklistDate.CrewID = CrewID;
                                CrewChecklistDate.CheckListCD = "null";
                                CrewChecklistDate.CrewChecklistDescription = "null";
                                var CheckListValue = CrewRosterService.GetCrewCheckListDate(CrewChecklistDate);
                                List<FlightPakMasterService.GetCrewCheckListDate> CrewChecklistDateList = new List<FlightPakMasterService.GetCrewCheckListDate>();
                                //Start
                                var CurrentFltValues = CrewRosterService.GetCurrentFltHoursInfo(CrewID).EntityList.ToList();
                                Session["CurrentFltHrsSetting"] = CurrentFltValues;
                                //End
                                if (CheckListValue.ReturnFlag == true)
                                {
                                    CrewChecklistDateList = CheckListValue.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.GetCrewCheckListDate>();
                                    if (chkDisplayInactiveChecklist.Checked == true)
                                    {
                                        dgCrewCheckList.DataSource = CrewChecklistDateList.Where(x => x.IsInActive == false);
                                    }
                                    else
                                    {
                                        dgCrewCheckList.DataSource = CrewChecklistDateList;
                                    }
                                    dgCrewCheckList.DataBind();
                                    Session["CrewCheckList"] = CrewChecklistDateList;
                                    // Bind Crew Checklist Currency Tab Grid
                                    //if (chkDisplayInactiveChecks.Checked == true)
                                    //{
                                    //    dgCurrencyChecklist.DataSource = CheckListValue.EntityList.Where(x => x.IsDeleted == false && x.IsInActive == true).ToList<FlightPakMasterService.GetCrewCheckListDate>();
                                    //}
                                    //else
                                    //{
                                    dgCurrencyChecklist.DataSource = CheckListValue.EntityList.Where(x => x.IsDeleted == false && x.IsInActive == false).ToList<FlightPakMasterService.GetCrewCheckListDate>();
                                    //}
                                    dgCurrencyChecklist.DataBind();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #region "Crew Checklist Button Events"
        /// <summary>
        /// Delete Selected Crew Checklist Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteChecklist_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnDelete.Value == "Yes")
                        {
                            if (dgCrewCheckList.SelectedItems.Count > 0)
                            {
                                GridDataItem item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                                Int64 ChecklistID = Convert.ToInt64(item.GetDataKeyValue("CheckListID").ToString());
                                string CheckListCD = item.GetDataKeyValue("CheckListCD").ToString();
                                List<FlightPakMasterService.GetCrewCheckListDate> CrewCheckListDateList = new List<FlightPakMasterService.GetCrewCheckListDate>();
                                CrewCheckListDateList = (List<FlightPakMasterService.GetCrewCheckListDate>)Session["CrewCheckList"];
                                for (int Index = 0; Index < CrewCheckListDateList.Count; Index++)
                                {
                                    if (CrewCheckListDateList[Index].CheckListCD == CheckListCD)
                                    {
                                        CrewCheckListDateList[Index].IsDeleted = true;
                                    }
                                }
                                Session["CrewCheckList"] = CrewCheckListDateList;
                                List<FlightPakMasterService.GetCrewCheckListDate> CrewDefinitionInfoList = new List<FlightPakMasterService.GetCrewCheckListDate>();
                                // Retain the Grid Items and bind into List to remove the selected item.
                                CrewDefinitionInfoList = RetainCrewCheckListGrid(dgCrewCheckList);
                                dgCrewCheckList.DataSource = CrewDefinitionInfoList.Where(x => x.IsDeleted == false).ToList();
                                dgCrewCheckList.DataBind();
                                Session["CrewCheckList"] = CrewCheckListDateList;
                                DefaultChecklistSelection();
                            }
                            else
                            {

                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Please select the record from Checklist table', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        /// <summary>
        /// Method to enable / disable Textbox controls for Flight Experience
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FlightExp_Click(object sender, EventArgs e)
        {
            if (dgTypeRating.SelectedItems.Count > 0)
            {
                if (btnFlightExp.Text == "Display Current Flight Experience")
                {
                    UpdateCrewforDisplay(1, true);
                    Session["Display"] = null;

                    List<GetCrewRating> SelectedCrewRateList = new List<GetCrewRating>();
                    GridDataItem Item = dgTypeRating.SelectedItems[0] as GridDataItem;
                    Item.Selected = true;

                    if (Item.GetDataKeyValue("AircraftTypeID") != null)
                    {
                        CrewRatingAircraftCD = Item.GetDataKeyValue("AircraftTypeID").ToString();
                    }
                    if (Item.GetDataKeyValue("CrewID") != null)
                    {
                        CrewRatingCrewID = Convert.ToInt64(Item.GetDataKeyValue("CrewID").ToString());
                    }
                    pnlCurrent.Visible = true;
                    pnlBeginning.Visible = false;
                    CalulateDisplayCurrentFlightExp(CrewRatingCrewID, Convert.ToInt64(CrewRatingAircraftCD));
                    //CrewDisplaySettings();
                    divDisplay.Visible = true;
                    divModify.Visible = false;
                    btnFlightExp.Text = "Modify Beginning Flt. Experience";
                }
                else
                {
                    pnlBeginning.Visible = true;
                    pnlCurrent.Visible = false;
                    UpdateCrewforDisplay(0, false);
                    Session["Display"] = null;
                    //CrewDisplaySettings();
                    divDisplay.Visible = false;
                    divModify.Visible = true;
                    btnFlightExp.Text = "Display Current Flight Experience";
                    BindSelectedTypeRatingItem();
                }
            }
        }



        private void BindSelectedTypeRatingItem(GetCrewRating SelectedCrewRateList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    CrewDisplaySettings();
                    if (dgTypeRating.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgTypeRating.SelectedItems[0];
                        Item.Selected = true;
                        lbTypeCode.Text = ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text.Trim();
                        lbDescription.Text = ((Label)Item["CrewRatingDescription"].FindControl("lbCrewRatingDescription")).Text;
                        chkTRPIC91.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")).Checked : false;
                        chkTRSIC91.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")).Checked : false;
                        chkTRPIC135.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked : false;
                        chkTRSIC135.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked : false;
                        chkTREngineer.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked : false;
                        chkTRInstructor.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked : false;
                        chkTRAttendant.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked : false;
                        chkTRAttendant91.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked : false;
                        chkTRAttendant135.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked : false;
                        chkTRAirman.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked : false;
                        chkTRInactive.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked : false;
                        chkPIC121.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked : false;
                        chkSIC121.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked : false;
                        chkPIC125.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked : false;
                        chkSIC125.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked : false;
                        chkAttendent121.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked : false;
                        chkAttendent125.Checked = (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null) ? ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked : false;
                        //1
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(Convert.ToString(SelectedCrewRateList.TotalTimeInTypeHrs)))
                                tbTotalHrsInType.Text = "00:00";
                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == ""))
                                tbTotalHrsInType.Text = "00:00";
                            else
                                tbTotalHrsInType.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text))
                                tbTotalHrsInType.Text = "0.0";
                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text.Trim() == ""))
                                tbTotalHrsInType.Text = "0.0";
                            else
                                tbTotalHrsInType.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text));
                        }
                        //2
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                                tbTotalHrsDay.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == ""))
                                tbTotalHrsDay.Text = "00:00";
                            else
                                tbTotalHrsDay.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                                tbTotalHrsDay.Text = "0.0";
                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text.Trim() == ""))
                                tbTotalHrsDay.Text = "0.0";
                            else
                                tbTotalHrsDay.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text));
                        }
                        //3
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                                tbTotalHrsNight.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == ""))
                                tbTotalHrsNight.Text = "00:00";
                            else
                                tbTotalHrsNight.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text).ToString()), 1).ToString());
                        }
                        else
                        {

                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                                tbTotalHrsNight.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text.Trim() == ""))
                                tbTotalHrsNight.Text = "0.0";
                            else
                                tbTotalHrsNight.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text));
                        }

                        //4
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                                tbTotalHrsInstrument.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == ""))
                                tbTotalHrsInstrument.Text = "00:00";
                            else
                                tbTotalHrsInstrument.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                                tbTotalHrsInstrument.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text.Trim() == ""))
                                tbTotalHrsInstrument.Text = "0.0";
                            else
                                tbTotalHrsInstrument.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text));
                        }

                        //// PIC Hrs
                        //5
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                                tbPICHrsInType.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == ""))
                                tbPICHrsInType.Text = "00:00";
                            else
                                tbPICHrsInType.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                                tbPICHrsInType.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text.Trim() == ""))
                                tbPICHrsInType.Text = "0.0";
                            else
                                tbPICHrsInType.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text));
                        }

                        //6
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                                tbPICHrsDay.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == ""))
                                tbPICHrsDay.Text = "00:00";
                            else
                                tbPICHrsDay.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                                tbPICHrsDay.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text.Trim() == ""))
                                tbPICHrsDay.Text = "0.0";
                            else
                                tbPICHrsDay.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text));
                        }


                        //7
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                                tbPICHrsNight.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == ""))
                                tbPICHrsNight.Text = "00:00";
                            else
                                tbPICHrsNight.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text).ToString()), 1).ToString());
                        }
                        else
                        {

                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                                tbPICHrsNight.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text.Trim() == ""))
                                tbPICHrsNight.Text = "0.0";
                            else
                                tbPICHrsNight.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text));
                        }

                        //8
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                                tbPICHrsInstrument.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == ""))
                                tbPICHrsInstrument.Text = "00:00";
                            else
                                tbPICHrsInstrument.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                                tbPICHrsInstrument.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text.Trim() == ""))
                                tbPICHrsInstrument.Text = "0.0";
                            else
                                tbPICHrsInstrument.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text));
                        }
                        //// SIC Hrs
                        //9
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                                tbSICInTypeHrs.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == ""))
                                tbSICInTypeHrs.Text = "00:00";
                            else
                                tbSICInTypeHrs.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                                tbSICInTypeHrs.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text.Trim() == ""))
                                tbSICInTypeHrs.Text = "0.0";
                            else
                                tbSICInTypeHrs.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text));
                        }


                        //10
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                                tbSICHrsDay.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == ""))
                                tbSICHrsDay.Text = "00:00";
                            else
                                tbSICHrsDay.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                                tbSICHrsDay.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text.Trim() == ""))
                                tbSICHrsDay.Text = "0.0";
                            else
                                tbSICHrsDay.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text));
                        }


                        //11
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                                tbSICHrsNight.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == ""))
                                tbSICHrsNight.Text = "00:00";
                            else
                                tbSICHrsNight.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                                tbSICHrsNight.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text.Trim() == ""))
                                tbSICHrsNight.Text = "0.0";
                            else
                                tbSICHrsNight.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text));
                        }


                        //12
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                                tbSICHrsInstrument.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == ""))
                                tbSICHrsInstrument.Text = "00:00";
                            else
                                tbSICHrsInstrument.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                                tbSICHrsInstrument.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text.Trim() == ""))
                                tbSICHrsInstrument.Text = "0.0";
                            else
                                tbSICHrsInstrument.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text));
                        }

                        //// Other Hrs

                        //13
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                                tbOtherHrsSimulator.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == ""))
                                tbOtherHrsSimulator.Text = "00:00";
                            else
                                tbOtherHrsSimulator.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                                tbOtherHrsSimulator.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text.Trim() == ""))
                                tbOtherHrsSimulator.Text = "0.0";
                            else
                                tbOtherHrsSimulator.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text));
                        }

                        //14
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                                tbOtherHrsFltEngineer.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == ""))
                                tbOtherHrsFltEngineer.Text = "00:00";
                            else
                                tbOtherHrsFltEngineer.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                                tbOtherHrsFltEngineer.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text.Trim() == ""))
                                tbOtherHrsFltEngineer.Text = "0.0";
                            else
                                tbOtherHrsFltEngineer.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text));
                        }

                        //15
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                                tbOtherHrsFltInstructor.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == ""))
                                tbOtherHrsFltInstructor.Text = "00:00";
                            else
                                tbOtherHrsFltInstructor.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                                tbOtherHrsFltInstructor.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text.Trim() == ""))
                                tbOtherHrsFltInstructor.Text = "0.0";
                            else
                                tbOtherHrsFltInstructor.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text));

                        }
                        //16
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                                tbOtherHrsFltAttendant.Text = "00:00";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == ""))
                                tbOtherHrsFltAttendant.Text = "00:00";
                            else
                                tbOtherHrsFltAttendant.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text).ToString()), 1).ToString());
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                                tbOtherHrsFltAttendant.Text = "0.0";

                            else if ((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == "0.0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == "0") || (((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text.Trim() == ""))
                                tbOtherHrsFltAttendant.Text = "0.0";
                            else
                                tbOtherHrsFltAttendant.Text = AddPadding((((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text));
                        }
                        if ((pnlBeginning.Visible == true) && (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text)))
                        {
                            tbAsOfDt.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text, DateFormat)));
                        }
                        else
                        {
                            tbAsOfDt.Text = string.Empty;
                        }
                        tbTotalHrsInType.Focus();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Crew Checklist TextChanged Events"
        /// <summary>
        /// Method to change the other dates based ChgBaseDate Value and show the value in grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChgBaseDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            if (ckhDisableDateCalculation.Checked == false)
                            {
                                string FirstDayCalculation = "";
                                if (tbPrevious.Text.Trim() != "")
                                {
                                    FirstDayCalculation = tbPrevious.Text.Trim();
                                }
                                if (tbChgBaseDate.Text.Trim() != "")
                                {
                                    FirstDayCalculation = tbChgBaseDate.Text.Trim();
                                }
                                DateTime FirstDay = DateTime.MinValue;
                                if (FirstDayCalculation != "")
                                {
                                    FirstDay = Convert.ToDateTime(FormatDate(FirstDayCalculation, DateFormat));
                                    if (radFreq.SelectedItem.Text == "Months")
                                    {
                                        int Months = 0;
                                        if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                        {
                                            Months = Convert.ToInt32(tbFreq.Text);
                                        }
                                        FirstDay = FirstDay.AddMonths(Months);
                                    }
                                    else
                                    {
                                        int Days = 0;
                                        if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                        {
                                            Days = Convert.ToInt32(tbFreq.Text);
                                        }
                                        FirstDay = FirstDay.AddDays(Days);
                                    }
                                }
                                if (chkSetToEndOfMonth.Checked == true)
                                {
                                    if (FirstDayCalculation.Trim() != "")
                                    {

                                        int Year = FirstDay.Year;
                                        int Month = FirstDay.Month;
                                        int numberOfDays = DateTime.DaysInMonth(Year, Month);
                                        DateTime value = new DateTime(Year, Month, numberOfDays);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }
                                else if (chkSetToNextOfMonth.Checked == true)
                                {
                                    if (FirstDayCalculation.Trim() != "")
                                    {
                                        int Year = FirstDay.Year;
                                        int Month = FirstDay.Month;
                                        DateTime value = new DateTime(Year, Month + 1, 1);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }
                                else if (chkSetToEndOfCalenderYear.Checked == true)
                                {
                                    if (FirstDayCalculation.Trim() != "")
                                    {
                                        int Year = FirstDay.Year;
                                        DateTime value = new DateTime(Year, 12, 31);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }
                                else
                                {
                                    ChgBaseDateDateCalculation();
                                }
                            }
                            ((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text = System.Web.HttpUtility.HtmlEncode(tbChgBaseDate.Text);
                            ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                            ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                            ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        private void ChgBaseDateDateCalculation()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewCheckList.SelectedItems.Count > 0)
                    {
                        if (ckhDisableDateCalculation.Checked == false)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text = System.Web.HttpUtility.HtmlEncode(tbChgBaseDate.Text);
                            tbDueNext.Text = tbChgBaseDate.Text;
                            tbGraceDate.Text = tbChgBaseDate.Text;
                            tbAlertDate.Text = tbChgBaseDate.Text;
                            ((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text = System.Web.HttpUtility.HtmlEncode(tbOriginalDate.Text);
                            ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbChgBaseDate.Text);
                            ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbChgBaseDate.Text);
                            ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbChgBaseDate.Text);
                            ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                            ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                            FreqChangeCalculation();
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }
        protected void ChgBaseDate_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ChgBaseDateDateCalculation();
                        CheckItemInGrid();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        protected void OriginalDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text = System.Web.HttpUtility.HtmlEncode(tbOriginalDate.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to change increase day or months to other dates and show the value in grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Freq_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((chkSetToEndOfCalenderYear.Checked == false) && (chkSetToEndOfMonth.Checked == false) && (chkSetToNextOfMonth.Checked == false))
                        {
                            FreqChangeCalculation();
                        }
                        else
                        {
                            FreqChangeCalculationonChecked();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void FreqChangeCalculationonChecked()
        {
            DateTime DueNext = DateTime.MinValue;
            DateTime Alert = DateTime.MinValue;
            DateTime Grace = DateTime.MinValue;

            DateTime PreviousDate = DateTime.MinValue;
            DateTime ChgBaseDate = DateTime.MinValue;


            if (dgCrewCheckList.SelectedItems.Count > 0)
            {
                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                if (ckhDisableDateCalculation.Checked == false)
                {

                    string DateCalculation = "";
                    if (tbPrevious.Text.Trim() != string.Empty)
                    {
                        DateCalculation = tbPrevious.Text.Trim();
                    }
                    if (tbChgBaseDate.Text.Trim() != string.Empty)
                    {
                        DateCalculation = tbChgBaseDate.Text.Trim();
                    }
                    if (DateCalculation != string.Empty)
                    {
                        if (radFreq.SelectedItem.Text == "Months")
                        {
                            int Alerts = 0;
                            int GraceDay = 0;
                            int Months = 0;
                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                            {
                                Months = Convert.ToInt32(tbFreq.Text);
                            }
                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                            {
                                Alerts = Convert.ToInt32(tbAlertDays.Text);
                            }
                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                            {
                                GraceDay = Convert.ToInt32(tbGraceDays.Text);
                            }
                            DateTime DateSource = DateTime.MinValue;

                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = DueNext.AddMonths(Months);
                            if (chkSetToEndOfMonth.Checked == true)
                            {
                                int Year = DueNext.Year;
                                int Month = DueNext.Month;
                                int numberOfDays = DateTime.DaysInMonth(Year, Month);
                                DateTime value = new DateTime(Year, Month, numberOfDays);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                            if (chkSetToNextOfMonth.Checked == true)
                            {

                                int Year = DueNext.Year;
                                int Month = DueNext.Month;
                                DateTime value = new DateTime(Year, Month + 1, 1);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                            if (chkSetToEndOfCalenderYear.Checked == true)
                            {
                                int Year = DueNext.Year;
                                DateTime value = new DateTime(Year, 12, 31);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                        }
                        else
                        {
                            int Days = 0;
                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                            {
                                Days = Convert.ToInt32(tbFreq.Text);
                            }
                            DateTime DateSource = DateTime.MinValue;

                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = DateSource.AddDays(Days);
                            if (chkSetToEndOfMonth.Checked == true)
                            {
                                int Year = DueNext.Year;
                                int Month = DueNext.Month;
                                int numberOfDays = DateTime.DaysInMonth(Year, Month);
                                DateTime value = new DateTime(Year, Month, numberOfDays);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                            if (chkSetToNextOfMonth.Checked == true)
                            {

                                int Year = DueNext.Year;
                                int Month = DueNext.Month;
                                DateTime value = new DateTime(Year, Month + 1, 1);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                            if (chkSetToEndOfCalenderYear.Checked == true)
                            {
                                int Year = DueNext.Year;
                                DateTime value = new DateTime(Year, 12, 31);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                        }
                    }

                    if (tbChgBaseDate.Text.Trim() != string.Empty && tbPrevious.Text.Trim() != string.Empty && (radFreq.SelectedItem.Text == "Months"))
                    {
                        PreviousDate = Convert.ToDateTime(FormatDate(tbPrevious.Text.Trim(), DateFormat));
                        ChgBaseDate = Convert.ToDateTime(FormatDate(tbChgBaseDate.Text.Trim(), DateFormat));
                        DueNext = new DateTime(PreviousDate.Year + (Convert.ToInt32(tbFreq.Text) / 12), DueNext.Month, DueNext.Day);
                        Alert = new DateTime(PreviousDate.Year + (Convert.ToInt32(tbFreq.Text) / 12), Alert.Month, Alert.Day);
                        Grace = new DateTime(PreviousDate.Year + (Convert.ToInt32(tbFreq.Text) / 12), Grace.Month, Grace.Day);
                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                        DueNextCalculation();
                        //tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                        //tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                    }
                }
                ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
            }

        }







        protected void FreqChangeCalculation()
        {
            DateTime DateSource = DateTime.MinValue;
            DateTime DueNext = DateTime.MinValue;
            DateTime Alert = DateTime.MinValue;
            DateTime Grace = DateTime.MinValue;

            DateTime PreviousDate = DateTime.MinValue;
            DateTime ChgBaseDate = DateTime.MinValue;

            if (dgCrewCheckList.SelectedItems.Count > 0)
            {
                if (ckhDisableDateCalculation.Checked == false)
                {
                    string DateCalculation = "";
                    if (tbPrevious.Text.Trim() != string.Empty)
                    {
                        DateCalculation = tbPrevious.Text.Trim();
                    }
                    if (tbChgBaseDate.Text.Trim() != string.Empty)
                    {
                        DateCalculation = tbChgBaseDate.Text.Trim();
                    }
                    if (DateCalculation != string.Empty)
                    {
                        GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                        if (radFreq.SelectedItem.Text == "Months")
                        {
                            int Alerts = 0;
                            int GraceDay = 0;
                            int Months = 0;
                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                            {
                                Months = Convert.ToInt32(tbFreq.Text);
                            }
                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                            {
                                Alerts = Convert.ToInt32(tbAlertDays.Text);
                            }
                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                            {
                                GraceDay = Convert.ToInt32(tbGraceDays.Text);
                            }
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = DateSource.AddMonths(Months);
                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                            Alert = DateSource.AddMonths(Months);
                            Alert = Alert.AddDays(-Alerts);
                            tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                            Grace = DateSource.AddMonths(Months);
                            Grace = Grace.AddDays(GraceDay);
                            tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                        }
                        else
                        {
                            int Alerts = 0;
                            int GraceDay = 0;
                            int Days = 0;
                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                            {
                                Days = Convert.ToInt32(tbFreq.Text);
                            }
                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                            {
                                Alerts = Convert.ToInt32(tbAlertDays.Text);
                                Alerts = Alerts - Days;
                            }
                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                            {
                                GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                GraceDay = GraceDay + Days;
                            }

                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = DateSource.AddDays(Days);
                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                            {
                                Alert = Alert.AddDays(-Alerts);
                            }
                            else
                            {
                                Alert = Alert.AddDays(Days);
                            }
                            tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                            {
                                Grace = Grace.AddDays(GraceDay);
                            }
                            else
                            {
                                Grace = Grace.AddDays(Days);
                            }
                            tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                        }
                        //Assigning Previous Date Year's Alone -- 2659

                        if (tbChgBaseDate.Text.Trim() != string.Empty && tbPrevious.Text.Trim() != string.Empty && (radFreq.SelectedItem.Text == "Months"))
                        {
                            PreviousDate = Convert.ToDateTime(FormatDate(tbPrevious.Text.Trim(), DateFormat));
                            ChgBaseDate = Convert.ToDateTime(FormatDate(tbChgBaseDate.Text.Trim(), DateFormat));

                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                            //DueNextCalculation();
                            int GraceDay = 0;
                            int Days = 0;
                            int Alerts = 0;
                            if (ckhDisableDateCalculation.Checked == false)
                            {
                                //if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                //{
                                //    Days = Convert.ToInt32(tbFreq.Text);
                                //}
                                if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                                {
                                    GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                    GraceDay = GraceDay + Days;
                                }
                                if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                                {
                                    Alerts = Convert.ToInt32(tbAlertDays.Text);
                                    Alerts = Alerts - Days;
                                }


                                IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                DueNext = Alert = Grace = new DateTime(PreviousDate.Year + (Convert.ToInt32(tbFreq.Text) / 12), DueNext.Month, DueNext.Day);

                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                                Alert = Alert.AddDays(-Alerts);
                                tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                                Grace = Grace.AddDays(GraceDay);
                                tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                            }
                        }

                        ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                        ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                        ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                        ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                        ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                        ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                    }
                }
            }

        }
        /// <summary>
        /// Methods to add days to Alert date and show the value in grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AlertDays_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            if (ckhDisableDateCalculation.Checked == false)
                            {
                                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                                ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                                ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                AlertDaysFunction();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void AlertDaysFunction()
        {
            if (tbAlertDate.Text != string.Empty)
            {
                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                int AlertDays = 0;
                if (tbDueNext.Text.Trim() != "")
                {
                    string StartDate = tbDueNext.Text;
                    DateTime AlertDate = Convert.ToDateTime(FormatDate(StartDate, DateFormat));
                    if (tbAlertDate.Text != string.Empty)
                    {
                        AlertDays = Convert.ToInt32(tbAlertDays.Text);
                    }
                    tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime((AlertDate.AddDays(-AlertDays))));
                    ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                    ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                }
            }
        }
        protected void GraceDaysFunction()
        {
            if (tbGraceDate.Text != string.Empty)
            {
                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                int GraceDays = 0;
                if (tbDueNext.Text.Trim() != "")
                {
                    string StartDate = tbDueNext.Text;
                    DateTime GraceDate = Convert.ToDateTime(FormatDate(StartDate, DateFormat));
                    if (tbGraceDays.Text != string.Empty)
                    {
                        GraceDays = Convert.ToInt32(tbGraceDays.Text);
                    }
                    tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime((GraceDate.AddDays(GraceDays))));
                    ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                    ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                }
            }
        }
        /// <summary>
        /// Method to show the Aircraft Type value in Crew Checklist Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AircraftType_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (tbAircraftType.Text != null && tbAircraftType.Text.Trim() != "")
                        {
                            CheckAircraftTypeCodeExist();
                        }
                        else
                        {
                            tbTotalReqFlightHrs.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        private bool CheckAircraftTypeCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewCheckList.SelectedItems.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(tbAircraftType.Text.Trim()))
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];

                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {
                                var CrewRosterServiceValue = CrewRosterService.GetAircraftList().EntityList.Where(x => x.AircraftCD.ToString().ToUpper().Trim().Equals(tbAircraftType.Text.ToUpper().Trim())).ToList();
                                if (CrewRosterServiceValue != null && CrewRosterServiceValue.Count > 0)
                                {
                                    ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text = System.Web.HttpUtility.HtmlEncode(tbAircraftType.Text);
                                    ((HiddenField)Item["AircraftTypeCD"].FindControl("hdnAircraftTypeCD")).Value = ((FlightPak.Web.FlightPakMasterService.GetAllAircraft)CrewRosterServiceValue[0]).AircraftID.ToString();
                                    tbAircraftType.Text = ((FlightPak.Web.FlightPakMasterService.GetAllAircraft)CrewRosterServiceValue[0]).AircraftCD;
                                    cvAssociateTypeCode.IsValid = true;
                                    RetVal = false;
                                    tbTotalReqFlightHrs.Enabled = true;
                                }
                                else
                                {
                                    cvAssociateTypeCode.IsValid = false;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbAircraftType);
                                    RetVal = true;
                                }
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TotalReqFlightHrs_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            string TextboxID = ((TextBox)sender).ID;
                            string TextboxValue = ((TextBox)sender).Text.Trim();
                            hdnTempHoursTxtbox.Value = TextboxID;
                            if (ValidateTotalFltHours(TextboxValue))
                            {
                                if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null))
                                {
                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                                    {
                                        if ((TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != "."))
                                        {
                                            if (TextboxValue.IndexOf(":") != -1)
                                            {
                                                string[] timeArray = TextboxValue.Split(':');
                                                if (string.IsNullOrEmpty(timeArray[0]))
                                                {
                                                    timeArray[0] = "0";
                                                    ((TextBox)sender).Text = timeArray[0] + ":" + timeArray[1];
                                                }
                                                if (!string.IsNullOrEmpty(timeArray[1]))
                                                {
                                                    Int64 result = 0;
                                                    string timeHours;
                                                    if (timeArray[1].Length != 2)
                                                    {
                                                        result = (Convert.ToInt32(timeArray[1])) / 10;
                                                        if (result < 1)
                                                        {
                                                            if (timeArray[0] != null)
                                                            {
                                                                if (timeArray[0].Trim() == "")
                                                                {
                                                                    timeArray[0] = "0";
                                                                }
                                                            }
                                                            else
                                                            {
                                                                timeArray[0] = "0";
                                                            }
                                                            ((TextBox)sender).Text = timeArray[0] + ":" + "0" + timeArray[1];

                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    ((TextBox)sender).Text = TextboxValue + "00";
                                                }
                                            }
                                            else
                                            {
                                                ((TextBox)sender).Text = TextboxValue + ":00";
                                            }
                                        }
                                        else
                                        {
                                            ((TextBox)sender).Text = "0:00";
                                        }
                                    }
                                    else
                                    {
                                        if ((TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != "."))
                                        {
                                            if (TextboxValue.IndexOf(".") != -1)
                                            {
                                                string[] timeArray = TextboxValue.Split('.');
                                                if (string.IsNullOrEmpty(timeArray[0]))
                                                {
                                                    timeArray[0] = "0";
                                                    ((TextBox)sender).Text = timeArray[0] + "." + timeArray[1];
                                                }
                                                if (!string.IsNullOrEmpty(timeArray[1]))
                                                {
                                                    if (timeArray[0] != null)
                                                    {
                                                        if (timeArray[0].Trim() == "")
                                                        {
                                                            timeArray[0] = "0";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        timeArray[0] = "0";
                                                    }
                                                    ((TextBox)sender).Text = timeArray[0] + "." + timeArray[1];
                                                }
                                                else
                                                {
                                                    ((TextBox)sender).Text = TextboxValue + "0";
                                                }
                                            }
                                            else
                                            {
                                                ((TextBox)sender).Text = TextboxValue + ".0";
                                            }
                                        }
                                        else
                                        {
                                            ((TextBox)sender).Text = "0.0";
                                        }

                                    }
                                }
                                else
                                {
                                    if ((TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != "."))
                                    {
                                        if (TextboxValue.IndexOf(".") != -1)
                                        {
                                            string[] timeArray = TextboxValue.Split('.');
                                            if (string.IsNullOrEmpty(timeArray[0]))
                                            {
                                                timeArray[0] = "0";
                                                ((TextBox)sender).Text = timeArray[0] + "." + timeArray[1];
                                            }
                                            if (!string.IsNullOrEmpty(timeArray[1]))
                                            {
                                                if (timeArray[0] != null)
                                                {
                                                    if (timeArray[0].Trim() == "")
                                                    {
                                                        timeArray[0] = "0";
                                                    }
                                                }
                                                else
                                                {
                                                    timeArray[0] = "0";
                                                }
                                                ((TextBox)sender).Text = timeArray[0] + "." + "0";
                                            }
                                            else
                                            {
                                                ((TextBox)sender).Text = TextboxValue + "0";
                                            }
                                        }
                                        else
                                        {
                                            ((TextBox)sender).Text = TextboxValue + ".0";
                                        }
                                    }
                                    else
                                    {
                                        ((TextBox)sender).Text = "0.0";
                                    }
                                }
                                if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
                                {
                                    ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalReqFlightHrs.Text.Trim() != "") ? Convert.ToString(System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbTotalReqFlightHrs.Text, true, "GENERAL")), 3)) : "0");
                                }
                                else
                                {
                                    ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode((tbTotalReqFlightHrs.Text.Trim() != "") ? Convert.ToString(Convert.ToDecimal(tbTotalReqFlightHrs.Text)) : "0");
                                }
                                if (!string.IsNullOrEmpty(tbTotalReqFlightHrs.Text))
                                {
                                    ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode(tbTotalReqFlightHrs.Text);
                                }
                                else
                                {
                                    ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode("");
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        /// <summary>
        /// Method to show the Changed Alert Date in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AlertDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {

                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];

                            if (tbDueNext.Text.Trim() != string.Empty || ckhDisableDateCalculation.Checked == true)
                            {
                                if (tbDueNext.Text.Trim() != string.Empty && tbAlertDate.Text != string.Empty)
                                {
                                    ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                    DateTime DueNext = DateTime.MinValue;
                                    DateTime Alert = DateTime.MinValue;

                                    DueNext = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                                    Alert = Convert.ToDateTime(FormatDate(tbAlertDate.Text, DateFormat));

                                    TimeSpan t = DueNext - Alert;
                                    int NrOfDays = t.Days;

                                    tbAlertDays.Text = NrOfDays.ToString();
                                    ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                                }
                            }
                            else
                            {

                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Enter a Due Next Date or check Disable Date Calculation', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                tbAlertDate.Text = "";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to show the Changed Grace Date in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GraceDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];


                            if (tbDueNext.Text.Trim() != string.Empty || ckhDisableDateCalculation.Checked == true)
                            {
                                if (tbDueNext.Text.Trim() != string.Empty && tbGraceDate.Text != string.Empty)
                                {
                                    ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                                    DateTime DueNext = DateTime.MinValue;
                                    DateTime GraceDate = DateTime.MinValue;
                                    DueNext = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                                    GraceDate = Convert.ToDateTime(FormatDate(tbGraceDate.Text, DateFormat));
                                    TimeSpan t = GraceDate - DueNext;
                                    int NrOfDays = t.Days;
                                    tbGraceDays.Text = NrOfDays.ToString();
                                    ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                                }
                            }
                            else
                            {
                                tbGraceDate.Text = "";

                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Enter a Due Next Date or check Disable Date Calculation', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to show the Changed Previous Date in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Previous_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DateTime DueNext = DateTime.MinValue;
                        DateTime Alert = DateTime.MinValue;
                        DateTime Grace = DateTime.MinValue;

                        DateTime PreviousDate = DateTime.MinValue;
                        DateTime ChgBaseDate = DateTime.MinValue;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            if (chkSetToEndOfMonth.Checked == true)
                            {
                                if (tbChgBaseDate.Text == string.Empty)
                                {
                                    if (ckhDisableDateCalculation.Checked == false)
                                    {
                                        if (tbPrevious.Text.Trim() != "")
                                        {
                                            DateTime FirstDay = Convert.ToDateTime(FormatDate(tbPrevious.Text, DateFormat));

                                            if (tbPrevious.Text.Trim() != "")
                                            {
                                                FirstDay = Convert.ToDateTime(FormatDate(tbPrevious.Text.Trim(), DateFormat));
                                                if (radFreq.SelectedItem.Text == "Months")
                                                {
                                                    int Months = 0;
                                                    if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                                    {
                                                        Months = Convert.ToInt32(tbFreq.Text);
                                                    }
                                                    FirstDay = FirstDay.AddMonths(Months);
                                                }
                                                else
                                                {
                                                    int Days = 0;
                                                    if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                                    {
                                                        Days = Convert.ToInt32(tbFreq.Text);
                                                    }
                                                    FirstDay = FirstDay.AddDays(Days);
                                                }
                                            }
                                            int Year = FirstDay.Year;
                                            int Month = FirstDay.Month;
                                            int numberOfDays = DateTime.DaysInMonth(Year, Month);
                                            DateTime value = new DateTime(Year, Month, numberOfDays);
                                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                            DueNextCalculation();
                                        }
                                    }
                                }
                            }
                            else if (chkSetToNextOfMonth.Checked == true)
                            {
                                if (tbChgBaseDate.Text == string.Empty)
                                {
                                    if (ckhDisableDateCalculation.Checked == false)
                                    {
                                        if (tbPrevious.Text.Trim() != "")
                                        {
                                            DateTime FirstDay = Convert.ToDateTime(FormatDate(tbPrevious.Text, DateFormat));
                                            if (tbPrevious.Text.Trim() != "")
                                            {
                                                FirstDay = Convert.ToDateTime(FormatDate(tbPrevious.Text.Trim(), DateFormat));
                                                if (radFreq.SelectedItem.Text == "Months")
                                                {
                                                    int Months = 0;
                                                    if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                                    {
                                                        Months = Convert.ToInt32(tbFreq.Text);
                                                    }
                                                    FirstDay = FirstDay.AddMonths(Months);
                                                }
                                                else
                                                {
                                                    int Days = 0;
                                                    if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                                    {
                                                        Days = Convert.ToInt32(tbFreq.Text);
                                                    }
                                                    FirstDay = FirstDay.AddDays(Days);
                                                }
                                            }
                                            int Year = FirstDay.Year;
                                            int Month = FirstDay.Month;
                                            DateTime value = new DateTime(Year, Month + 1, 1);
                                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                            DueNextCalculation();
                                        }
                                    }
                                }
                            }
                            else if (chkSetToEndOfCalenderYear.Checked == true)
                            {
                                if (tbChgBaseDate.Text == string.Empty)
                                {
                                    if (ckhDisableDateCalculation.Checked == false)
                                    {
                                        if (tbPrevious.Text.Trim() != "")
                                        {
                                            DateTime FirstDay = Convert.ToDateTime(FormatDate(tbPrevious.Text, DateFormat));
                                            if (tbPrevious.Text.Trim() != "")
                                            {
                                                FirstDay = Convert.ToDateTime(FormatDate(tbPrevious.Text.Trim(), DateFormat));
                                                if (radFreq.SelectedItem.Text == "Months")
                                                {
                                                    int Months = 0;
                                                    if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                                    {
                                                        Months = Convert.ToInt32(tbFreq.Text);
                                                    }
                                                    FirstDay = FirstDay.AddMonths(Months);
                                                }
                                                else
                                                {
                                                    int Days = 0;
                                                    if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                                    {
                                                        Days = Convert.ToInt32(tbFreq.Text);
                                                    }
                                                    FirstDay = FirstDay.AddDays(Days);
                                                }
                                            }
                                            int Year = FirstDay.Year;
                                            DateTime value = new DateTime(Year, 12, 31);
                                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                            DueNextCalculation();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (tbChgBaseDate.Text == string.Empty)
                                {
                                    if (ckhDisableDateCalculation.Checked == false)
                                    {
                                        string DateCalculation = tbPrevious.Text.Trim();
                                        if (DateCalculation.Trim() != "")
                                        {
                                            Label lbPreviousDT = new Label();
                                            ((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                                            if (radFreq.SelectedItem.Text == "Months")
                                            {
                                                int Alerts = 0;
                                                int GraceDay = 0;
                                                int Months = 0;
                                                if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                                {
                                                    Months = Convert.ToInt32(tbFreq.Text);
                                                }
                                                if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                                                {
                                                    Alerts = Convert.ToInt32(tbAlertDays.Text);
                                                }
                                                if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                                                {
                                                    GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                                }
                                                DateTime DateSource = DateTime.MinValue;
                                                IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                                DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                                DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                                Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                                Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                                DueNext = DateSource.AddMonths(Months);
                                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                                                Alert = DateSource.AddMonths(Months);
                                                Alert = Alert.AddDays(-Alerts);
                                                tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                                                Grace = DateSource.AddMonths(Months);
                                                Grace = Grace.AddDays(GraceDay);
                                                tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                                            }
                                            else
                                            {
                                                int Alerts = 0;
                                                int GraceDay = 0;
                                                int Days = 0;
                                                if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                                {
                                                    Days = Convert.ToInt32(tbFreq.Text);
                                                }
                                                if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                                                {
                                                    Alerts = Convert.ToInt32(tbAlertDays.Text);
                                                    Alerts = Alerts - Days;
                                                }
                                                if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                                                {
                                                    GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                                    GraceDay = GraceDay + Days;
                                                }
                                                DateTime DateSource = DateTime.MinValue;
                                                IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                                DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                                DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                                Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                                Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                                DueNext = DateSource.AddDays(Days);
                                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                                                if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                                                {
                                                    Alert = Alert.AddDays(-Alerts);
                                                }
                                                else
                                                {
                                                    Alert = Alert.AddDays(Days);
                                                }
                                                tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                                                if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                                                {
                                                    Grace = Grace.AddDays(GraceDay);
                                                }
                                                else
                                                {
                                                    Grace = Grace.AddDays(Days);
                                                }
                                                tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                                            }
                                            ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                                            ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                            ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                                            if (tbGraceDays.Text.Trim() != "")
                                            {
                                                ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                                            }
                                            if (tbAlertDays.Text.Trim() != "")
                                            {
                                                ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                                            }
                                            ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                                        }
                                    }
                                }
                            }
                            //2659
                            if (tbChgBaseDate.Text.Trim() != string.Empty && tbPrevious.Text.Trim() != string.Empty && (radFreq.SelectedItem.Text == "Months") && tbFreq.Text != "0")
                            {
                                PreviousDate = Convert.ToDateTime(FormatDate(tbPrevious.Text.Trim(), DateFormat));
                                ChgBaseDate = Convert.ToDateTime(FormatDate(tbChgBaseDate.Text.Trim(), DateFormat));

                                DueNext = Convert.ToDateTime(FormatDate(tbChgBaseDate.Text.Trim(), DateFormat));


                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                                //DueNextCalculation();
                                int GraceDay = 0;
                                int Days = 0;
                                int Alerts = 0;
                                if (ckhDisableDateCalculation.Checked == false)
                                {
                                    //if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                    //{
                                    //    Days = Convert.ToInt32(tbFreq.Text);
                                    //}
                                    if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                                    {
                                        GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                        GraceDay = GraceDay + Days;
                                    }
                                    if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                                    {
                                        Alerts = Convert.ToInt32(tbAlertDays.Text);
                                        Alerts = Alerts - Days;
                                    }


                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    DueNext = Alert = Grace = new DateTime(PreviousDate.Year + (Convert.ToInt32(tbFreq.Text) / 12), DueNext.Month, DueNext.Day);

                                    tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                                    Alert = Alert.AddDays(-Alerts);
                                    tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                                    Grace = Grace.AddDays(GraceDay);
                                    tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                                }
                            }

                            ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                            ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                            ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                            ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                            ((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to show the Changed Previous Date in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DueNext_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            if (ckhDisableDateCalculation.Checked == false)
                            {
                                DateTime FirstDay = DateTime.MinValue;
                                string DateCalculation = "";
                                if (tbPrevious.Text.Trim() != "")
                                {
                                    DateCalculation = tbPrevious.Text.Trim();
                                }
                                if (tbChgBaseDate.Text.Trim() != "")
                                {
                                    DateCalculation = tbChgBaseDate.Text.Trim();
                                }
                                if (DateCalculation.Trim() != "")
                                {
                                    FirstDay = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                    if (radFreq.SelectedItem.Text == "Months")
                                    {
                                        int Months = 0;
                                        if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                        {
                                            Months = Convert.ToInt32(tbFreq.Text);
                                        }
                                        FirstDay = FirstDay.AddMonths(Months);
                                    }
                                    else
                                    {
                                        int Days = 0;
                                        if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                        {
                                            Days = Convert.ToInt32(tbFreq.Text);
                                        }
                                        FirstDay = FirstDay.AddDays(Days);
                                    }
                                }
                                if (chkSetToEndOfMonth.Checked == true)
                                {
                                    if (DateCalculation != "")
                                    {
                                        int Year = FirstDay.Year;
                                        int Month = FirstDay.Month;
                                        int numberOfDays = DateTime.DaysInMonth(Year, Month);
                                        DateTime value = new DateTime(Year, Month, numberOfDays);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }

                                }
                                else if (chkSetToNextOfMonth.Checked == true)
                                {
                                    if (DateCalculation != "")
                                    {

                                        int Year = FirstDay.Year;
                                        int Month = FirstDay.Month;
                                        DateTime value = new DateTime(Year, Month + 1, 1);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }
                                else if (chkSetToEndOfCalenderYear.Checked == true)
                                {
                                    if (DateCalculation != "")
                                    {
                                        int Year = FirstDay.Year;
                                        DateTime value = new DateTime(Year, 12, 31);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }
                                else
                                {
                                    if (tbChgBaseDate.Text.Trim() == string.Empty && tbPrevious.Text.Trim() == string.Empty)
                                    {
                                        DueNextCalculation();
                                        ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                                    }
                                    else
                                    {
                                        tbDueNext.Text = ((Label)Item["DueDT"].FindControl("lbDueDT")).Text;
                                    }
                                }


                                ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                                ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                                ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                                ((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                            }
                            else
                            {
                                ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        private void DueNextCalculation()
        {
            if (tbDueNext.Text.Trim() != "")
            {
                int GraceDay = 0;
                int Days = 0;
                int Alerts = 0;
                if (ckhDisableDateCalculation.Checked == false)
                {
                    if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                    {
                        Days = Convert.ToInt32(tbFreq.Text);
                    }
                    if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                    {
                        GraceDay = Convert.ToInt32(tbGraceDays.Text);
                        GraceDay = GraceDay + Days;
                    }
                    if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                    {
                        Alerts = Convert.ToInt32(tbAlertDays.Text);
                        Alerts = Alerts - Days;
                    }
                    DateTime DateSource = DateTime.MinValue;
                    DateTime DueNext = DateTime.MinValue;
                    DateTime Alert = DateTime.MinValue;
                    DateTime Grace = DateTime.MinValue;
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    DueNext = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                    Alert = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                    Grace = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                    tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                    Alert = Alert.AddDays(-Alerts);
                    tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                    Grace = Grace.AddDays(GraceDay);
                    tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                }
            }
            else
            {
                tbAlertDate.Text = "";
                tbGraceDate.Text = "";
            }

        }
        /// <summary>
        /// Method to show the Changed Grace Days in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GraceDays_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            if (ckhDisableDateCalculation.Checked == false && dgCrewCheckList.SelectedItems.Count > 0)
                            {
                                GridDataItem Item = dgCrewCheckList.SelectedItems[0] as GridDataItem;
                                GraceDaysFunction();
                                ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                                ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        /// <summary>
        /// Method to Update values in the selected list item and bind into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        /// <summary>
        /// Get Crew Rating Data by passing Grid Item
        /// </summary>
        /// <param name="Item"></param>
        /// <returns>Returns CrewRating Object</returns>
        private GetCrewRating GetCrewRatingItems(GridDataItem Item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Item))
            {
                GetCrewRating CrewRate = new GetCrewRating();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    CrewRate.CrewID = Convert.ToInt64(Item.GetDataKeyValue("CrewID").ToString());
                    if (Item.GetDataKeyValue("ClientID") != null)
                    {
                        CrewRate.ClientID = Convert.ToInt64(Item.GetDataKeyValue("ClientID").ToString());
                    }
                    if (Item.GetDataKeyValue("AircraftTypeID") != null)
                    {
                        CrewRate.AircraftTypeID = Convert.ToInt64(Item.GetDataKeyValue("AircraftTypeID").ToString());
                        Int64 CheckAircraftTypeID = Convert.ToInt64(Item.GetDataKeyValue("AircraftTypeID").ToString());
                    }
                    if (Item.GetDataKeyValue("CrewRatingDescription") != null)
                    {
                        CrewRate.CrewRatingDescription = Item.GetDataKeyValue("CrewRatingDescription").ToString();
                    }
                    if (((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")) != null)
                    {
                        CrewRate.IsPilotinCommand = ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked;
                    }
                    else
                    {
                        CrewRate.IsPilotinCommand = false;
                    }
                    if (((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")) != null)
                    {
                        CrewRate.IsSecondInCommand = ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked;
                    }
                    else
                    {
                        CrewRate.IsSecondInCommand = false;
                    }
                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null)
                    {
                        CrewRate.IsQualInType135PIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked;
                    }
                    else
                    {
                        CrewRate.IsQualInType135PIC = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null)
                    {
                        CrewRate.IsQualInType135SIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked;
                    }
                    else
                    {
                        CrewRate.IsQualInType135SIC = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null)
                    {
                        CrewRate.IsEngineer = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked;
                    }
                    else
                    {
                        CrewRate.IsEngineer = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null)
                    {
                        CrewRate.IsInstructor = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked;
                    }
                    else
                    {
                        CrewRate.IsInstructor = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null)
                    {
                        CrewRate.IsCheckAttendant = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked;
                    }
                    else
                    {
                        CrewRate.IsCheckAttendant = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null)
                    {
                        CrewRate.IsAttendantFAR91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked;
                    }
                    else
                    {
                        CrewRate.IsAttendantFAR91 = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null)
                    {
                        CrewRate.IsAttendantFAR135 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked;
                    }
                    else
                    {
                        CrewRate.IsAttendantFAR135 = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null)
                    {
                        CrewRate.IsCheckAirman = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked;
                    }
                    else
                    {
                        CrewRate.IsCheckAirman = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null)
                    {
                        CrewRate.IsInActive = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked;
                    }
                    else
                    {
                        CrewRate.IsInActive = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null)
                    {
                        CrewRate.IsPIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked;
                    }
                    else
                    {
                        CrewRate.IsPIC121 = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null)
                    {
                        CrewRate.IsSIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked;
                    }
                    else
                    {
                        CrewRate.IsSIC121 = false;
                    }
                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null)
                    {
                        CrewRate.IsPIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked;
                    }
                    else
                    {
                        CrewRate.IsPIC125 = false;
                    }
                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")) != null)
                    {
                        CrewRate.IsSIC91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")).Checked;
                    }
                    else
                    {
                        CrewRate.IsSIC91 = false;
                    }
                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")) != null)
                    {
                        CrewRate.IsPIC91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")).Checked;
                    }
                    else
                    {
                        CrewRate.IsPIC91 = false;
                    }
                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null)
                    {
                        CrewRate.IsSIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked;
                    }
                    else
                    {
                        CrewRate.IsSIC125 = false;
                    }

                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null)
                    {
                        CrewRate.IsAttendant121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked;
                    }
                    else
                    {
                        CrewRate.IsAttendant121 = false;
                    }
                    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null)
                    {
                        CrewRate.IsAttendant125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked;
                    }
                    else
                    {
                        CrewRate.IsAttendant125 = false;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text))
                    {
                        if (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text == "0")
                        {
                            CrewRate.TimeInType = 0;
                        }
                        else
                        {
                            CrewRate.TimeInType = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                        }
                    }
                    else
                    {
                        CrewRate.TimeInType = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                    {
                        CrewRate.Day1 = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.Day1 = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                    {
                        CrewRate.Night1 = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.Night1 = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                    {
                        CrewRate.Instrument = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.Instrument = 0;
                    }
                    // PIC Hrs
                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                    {
                        CrewRate.PilotInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.PilotInCommandTypeHrs = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                    {
                        CrewRate.TPilotinCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.TPilotinCommandDayHrs = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                    {
                        CrewRate.TPilotInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.TPilotInCommandNightHrs = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                    {
                        CrewRate.TPilotInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.TPilotInCommandINSTHrs = 0;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                    {
                        CrewRate.SecondInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.SecondInCommandTypeHrs = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                    {
                        CrewRate.SecondInCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.SecondInCommandDayHrs = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                    {
                        CrewRate.SecondInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.SecondInCommandNightHrs = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                    {
                        CrewRate.SecondInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.SecondInCommandINSTHrs = 0;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                    {
                        CrewRate.OtherSimHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.OtherSimHrs = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                    {
                        CrewRate.OtherFlightEngHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.OtherFlightEngHrs = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                    {
                        CrewRate.OtherFlightInstrutorHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.OtherFlightInstrutorHrs = 0;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                    {
                        CrewRate.OtherFlightAttdHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        CrewRate.OtherFlightAttdHrs = 0;
                    }




                    //// Total Hrs
                    //CrewRate.TotalTimeInTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text = false ? "0.0" : ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                    //CrewRate.TotalDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text = false ? "0.0" : ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                    //CrewRate.TotalNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text = false ? "0.0" : ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                    //CrewRate.TotalINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text = false ? "0.0" : ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                    //// PIC Hrs
                    //CrewRate.PilotInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text = false ? "0.0" : tbPICHrsInType.Text, CultureInfo.CurrentCulture);
                    //CrewRate.TPilotinCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text = false ? "0.0" : tbPICHrsDay.Text, CultureInfo.CurrentCulture);
                    //CrewRate.TPilotInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text = false ? "0.0" : tbPICHrsNight.Text, CultureInfo.CurrentCulture);
                    //CrewRate.TPilotInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text = false ? "0.0" : tbPICHrsInstrument.Text, CultureInfo.CurrentCulture);
                    //// SIC Hrs
                    //CrewRate.SecondInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text = false ? "0.0" : tbSICInTypeHrs.Text, CultureInfo.CurrentCulture);
                    //CrewRate.SecondInCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text = false ? "0.0" : tbSICHrsDay.Text, CultureInfo.CurrentCulture);
                    //CrewRate.SecondInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text = false ? "0.0" : tbSICHrsNight.Text, CultureInfo.CurrentCulture);
                    //CrewRate.SecondInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text = false ? "0.0" : tbSICHrsInstrument.Text, CultureInfo.CurrentCulture);
                    //// Other Hrs
                    //CrewRate.OtherSimHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text = false ? "0.0" : tbOtherHrsSimulator.Text, CultureInfo.CurrentCulture);
                    //CrewRate.OtherFlightEngHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text = false ? "0.0" : tbOtherHrsFltEngineer.Text, CultureInfo.CurrentCulture);
                    //CrewRate.OtherFlightInstrutorHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text = false ? "0.0" : tbOtherHrsFltInstructor.Text, CultureInfo.CurrentCulture);
                    //CrewRate.OtherFlightAttdHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text = false ? "0.0" : tbOtherHrsFltAttendant.Text, CultureInfo.CurrentCulture);

                    CrewRate.TotalUpdateAsOfDT = DateTime.Now;
                    CrewRate.AsOfDT = DateTime.Now;
                    CrewRate.IsDeleted = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return CrewRate;
            }

        }
        /// <summary>
        /// Method to Assign Updated Crew Checklist class items into Crew Rating Class Objects
        /// </summary>
        /// <param name="CrewRate"></param>
        /// <param name="UpdateCrewRate"></param>
        /// 
        private void SetTypeRating(GetCrewRating CrewRate, GetCrewRating UpdateCrewRate)
        {

            Int64 CrewID = 0;
            if (Session["SelectedCrewRosterID"] != null)
            {
                CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
            }
            List<GetOtherCrewDutyLog> OtherCrewDutyList = new List<GetOtherCrewDutyLog>();
            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var ObjRetval = objService.GetOtherCrewDutyLog(CrewID, Convert.ToInt64(CrewRate.AircraftTypeID));
                if (ObjRetval.ReturnFlag)
                {
                    OtherCrewDutyList = ObjRetval.EntityList.ToList();
                }
            }
            int DateDiff = 1;
            decimal Flight = 0;
            decimal TotalFlight = 0;
            decimal Night = 0;
            decimal TotalNight = 0;
            decimal Day = 0;
            decimal TotalDay = 0;
            decimal Instr = 0;
            decimal TotalInstr = 0;
            if (OtherCrewDutyList != null && OtherCrewDutyList.Count > 0)
            {
                foreach (GetOtherCrewDutyLog OtherCrew in OtherCrewDutyList)
                {
                    if (OtherCrew.DepartureDTTMLocal != null && OtherCrew.ArrivalDTTMLocal != null && OtherCrew.DepartureDTTMLocal.ToString().Trim() != string.Empty && OtherCrew.ArrivalDTTMLocal.ToString().Trim() != string.Empty)
                    {
                        DateTime StartDate = new DateTime();
                        DateTime EndDate = new DateTime();
                        StartDate = Convert.ToDateTime(OtherCrew.DepartureDTTMLocal);
                        EndDate = Convert.ToDateTime(OtherCrew.ArrivalDTTMLocal);
                        //StartDate =  Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
                        //EndDate = Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
                        DateDiff = EndDate.Subtract(StartDate).Days + 1;
                    }
                    if (OtherCrew.FlightHours != null && OtherCrew.FlightHours.ToString().Trim() != string.Empty)
                    {
                        Flight = Convert.ToDecimal(OtherCrew.FlightHours * DateDiff);
                    }
                    if (OtherCrew.TakeOffDay != null && OtherCrew.TakeOffDay.ToString().Trim() != string.Empty)
                    {
                        Day = Convert.ToDecimal(OtherCrew.TakeOffDay * DateDiff);
                    }
                    if (OtherCrew.Night != null && OtherCrew.Night.ToString().Trim() != string.Empty)
                    {
                        Night = Convert.ToDecimal(OtherCrew.Night * DateDiff);
                    }
                    if (OtherCrew.Instrument != null && OtherCrew.Instrument.ToString().Trim() != string.Empty)
                    {
                        Instr = Convert.ToDecimal(OtherCrew.Instrument * DateDiff);
                    }
                    TotalFlight += Flight;
                    TotalInstr += Instr;
                    TotalDay += Day;
                    TotalNight += Night;
                    Flight = 0;
                    Night = 0;
                    Day = 0;
                    Instr = 0;
                }
            }
            TotalDay = TotalFlight - TotalNight;
            Int64 TotalTimeInTypeHrs = 0;
            Int64 TotalDayHrs = 0;
            Int64 TotalNightHrs = 0;
            Int64 TotalINSTHrs = 0;
            Int64 PilotInCommandTypeHrs = 0;
            Int64 TPilotinCommandDayHrs = 0;
            Int64 TPilotInCommandNightHrs = 0;
            Int64 TPilotInCommandINSTHrs = 0;
            Int64 SecondInCommandTypeHrs = 0;
            Int64 SecondInCommandDayHrs = 0;
            Int64 SecondInCommandNightHrs = 0;
            Int64 SecondInCommandINSTHrs = 0;
            Int64 OtherSimHrs = 0;
            Int64 OtherFlightEngHrs = 0;
            Int64 OtherFlightInstrutorHrs = 0;
            Int64 OtherFlightAttdHrs = 0;
            //if (CrewRatingAircraftCD != null && CrewRatingCrewID > 0)
            //{
            //    using (MasterCatalogServiceClient CrewRatingUpdateService = new MasterCatalogServiceClient())
            //    {
            //        var CrewRatingUpdateList = CrewRatingUpdateService.GetCalculateCrewRating(Convert.ToInt64(UpdateCrewRate.AircraftTypeID), CrewID).EntityList.ToList();
            //        GetFlightLogDataForCrewMember CrewRatingUpdate = new GetFlightLogDataForCrewMember();
            //        if (CrewRatingUpdateList != null && CrewRatingUpdateList.Count > 0)
            //        {
            //            TotalTimeInTypeHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TOTTIT);
            //            TotalDayHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TOTDAY);
            //            TotalNightHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TOTNIGHT);
            //            TotalINSTHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TOTINSTR);

            //            PilotInCommandTypeHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TPIC);
            //            TPilotinCommandDayHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TPDAY);
            //            TPilotInCommandNightHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TPNIGHT);
            //            TPilotInCommandINSTHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TPINSTR);

            //            SecondInCommandTypeHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSIC);
            //            SecondInCommandDayHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSDAY);
            //            SecondInCommandNightHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSNIGHT);
            //            SecondInCommandINSTHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSINSTR);

            //            OtherSimHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSIMULATOR);
            //            OtherFlightEngHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TFLTENGR);
            //            OtherFlightInstrutorHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TFLTINSTR);
            //            OtherFlightAttdHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TFLTATTEND);
            //        }
            //    }
            //}
            CrewRate.CrewID = UpdateCrewRate.CrewID;
            CrewRate.ClientID = UpdateCrewRate.ClientID;
            CrewRate.AircraftTypeID = UpdateCrewRate.AircraftTypeID;
            CrewRate.CrewRatingDescription = UpdateCrewRate.CrewRatingDescription;
            // Qualified In Type As
            CrewRate.IsPIC91 = UpdateCrewRate.IsPIC91;
            CrewRate.IsQualInType135PIC = UpdateCrewRate.IsQualInType135PIC;
            CrewRate.IsSIC91 = UpdateCrewRate.IsSIC91;
            CrewRate.IsQualInType135SIC = UpdateCrewRate.IsQualInType135SIC;
            CrewRate.IsEngineer = UpdateCrewRate.IsEngineer;
            CrewRate.IsInstructor = UpdateCrewRate.IsInstructor;
            CrewRate.IsCheckAttendant = UpdateCrewRate.IsCheckAttendant;
            CrewRate.IsAttendantFAR91 = UpdateCrewRate.IsAttendantFAR91;
            CrewRate.IsAttendantFAR135 = UpdateCrewRate.IsAttendantFAR135;
            CrewRate.IsCheckAirman = UpdateCrewRate.IsCheckAirman;
            CrewRate.IsInActive = UpdateCrewRate.IsInActive;
            //<new fields>
            CrewRate.IsPIC121 = UpdateCrewRate.IsPIC121;
            CrewRate.IsSIC121 = UpdateCrewRate.IsSIC121;
            CrewRate.IsPIC125 = UpdateCrewRate.IsPIC125;
            CrewRate.IsSIC125 = UpdateCrewRate.IsSIC125;
            CrewRate.IsAttendant121 = UpdateCrewRate.IsAttendant121;
            CrewRate.IsAttendant125 = UpdateCrewRate.IsAttendant125;
            CrewRate.AsOfDT = UpdateCrewRate.AsOfDT;



            // Total Hrs
            CrewRate.TimeInType = UpdateCrewRate.TimeInType;
            CrewRate.Day1 = UpdateCrewRate.Day1;
            CrewRate.Night1 = UpdateCrewRate.Night1;
            CrewRate.Instrument = UpdateCrewRate.Instrument;

            // Total Hrs
            CrewRate.TotalTimeInTypeHrs = UpdateCrewRate.TimeInType + TotalFlight;
            CrewRate.TotalDayHrs = UpdateCrewRate.Day1 + TotalDay;
            CrewRate.TotalNightHrs = UpdateCrewRate.Night1 + TotalNight;
            CrewRate.TotalINSTHrs = UpdateCrewRate.Instrument + TotalInstr;
            // PIC Hrs
            CrewRate.PilotInCommandTypeHrs = UpdateCrewRate.PilotInCommandTypeHrs;
            CrewRate.TPilotinCommandDayHrs = UpdateCrewRate.TPilotinCommandDayHrs;
            CrewRate.TPilotInCommandNightHrs = UpdateCrewRate.TPilotInCommandNightHrs;
            CrewRate.TPilotInCommandINSTHrs = UpdateCrewRate.TPilotInCommandINSTHrs;
            // SIC Hrs
            CrewRate.SecondInCommandTypeHrs = UpdateCrewRate.SecondInCommandTypeHrs;
            CrewRate.SecondInCommandDayHrs = UpdateCrewRate.SecondInCommandDayHrs;
            CrewRate.SecondInCommandNightHrs = UpdateCrewRate.SecondInCommandNightHrs;
            CrewRate.SecondInCommandINSTHrs = UpdateCrewRate.SecondInCommandINSTHrs;
            // Other Hrs
            CrewRate.OtherSimHrs = UpdateCrewRate.OtherSimHrs;
            CrewRate.OtherFlightEngHrs = UpdateCrewRate.OtherFlightEngHrs;
            CrewRate.OtherFlightInstrutorHrs = UpdateCrewRate.OtherFlightInstrutorHrs;
            CrewRate.OtherFlightAttdHrs = UpdateCrewRate.OtherFlightAttdHrs;
            CrewRate.TotalUpdateAsOfDT = DateTime.Now;

        }
        /// <summary>
        /// Enable / Disable Hours in Type Rating Section
        /// </summary>
        /// <param name="Enable">Pass Boolean Value</param>
        private void EnableHours(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Enable == true)
                    {
                        if (btnSaveChanges.Visible == true)
                        {
                            Enable = true;
                        }
                        else
                        {
                            Enable = false;
                        }
                    }
                    tbTotalHrsInType.Enabled = Enable;
                    tbTotalHrsDay.Enabled = Enable;
                    tbTotalHrsNight.Enabled = Enable;
                    tbTotalHrsInstrument.Enabled = Enable;
                    tbPICHrsInType.Enabled = Enable;
                    tbPICHrsDay.Enabled = Enable;
                    tbPICHrsNight.Enabled = Enable;
                    tbPICHrsInstrument.Enabled = Enable;
                    tbSICInTypeHrs.Enabled = Enable;
                    tbSICHrsDay.Enabled = Enable;
                    tbSICHrsNight.Enabled = Enable;
                    tbSICHrsInstrument.Enabled = Enable;
                    tbOtherHrsSimulator.Enabled = Enable;
                    tbOtherHrsFltEngineer.Enabled = Enable;
                    tbOtherHrsFltInstructor.Enabled = Enable;
                    tbOtherHrsFltAttendant.Enabled = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Clear Type Rating Fields
        /// </summary>
        private void ClearTypeRatingFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (UserPrincipal.Identity != null)
                    {
                        // Display Time Fields based on Tenths / Minutes - Company Profile
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            tbTotalHrsInType.Text = "0.0";
                            tbTotalHrsDay.Text = "0.0";
                            tbTotalHrsNight.Text = "0.0";
                            tbTotalHrsInstrument.Text = "0.0";
                            tbPICHrsInType.Text = "0.0";
                            tbPICHrsDay.Text = "0.0";
                            tbPICHrsNight.Text = "0.0";
                            tbPICHrsInstrument.Text = "0.0";
                            tbSICInTypeHrs.Text = "0.0";
                            tbSICHrsDay.Text = "0.0";
                            tbSICHrsNight.Text = "0.0";
                            tbSICHrsInstrument.Text = "0.0";
                            tbOtherHrsSimulator.Text = "0.0";
                            tbOtherHrsFltEngineer.Text = "0.0";
                            tbOtherHrsFltInstructor.Text = "0.0";
                            tbOtherHrsFltAttendant.Text = "0.0";
                        }
                        else
                        {

                            tbTotalHrsInType.Text = "00:00";
                            tbTotalHrsDay.Text = "00:00";
                            tbTotalHrsNight.Text = "00:00";
                            tbTotalHrsInstrument.Text = "00:00";
                            tbPICHrsInType.Text = "00:00";
                            tbPICHrsDay.Text = "00:00";
                            tbPICHrsNight.Text = "00:00";
                            tbPICHrsInstrument.Text = "00:00";
                            tbSICInTypeHrs.Text = "00:00";
                            tbSICHrsDay.Text = "00:00";
                            tbSICHrsNight.Text = "00:00";
                            tbSICHrsInstrument.Text = "00:00";
                            tbOtherHrsSimulator.Text = "00:00";
                            tbOtherHrsFltEngineer.Text = "00:00";
                            tbOtherHrsFltInstructor.Text = "00:00";
                            tbOtherHrsFltAttendant.Text = "00:00";
                        }
                    }
                    else
                    {
                        // SetMask("00:00", "<00..99>:<00..59>");
                        tbTotalHrsInType.Text = "0.0";
                        tbTotalHrsDay.Text = "0.0";
                        tbTotalHrsNight.Text = "0.0";
                        tbTotalHrsInstrument.Text = "0.0";
                        tbPICHrsInType.Text = "0.0";
                        tbPICHrsDay.Text = "0.0";
                        tbPICHrsNight.Text = "0.0";
                        tbPICHrsInstrument.Text = "0.0";
                        tbSICInTypeHrs.Text = "0.0";
                        tbSICHrsDay.Text = "0.0";
                        tbSICHrsNight.Text = "0.0";
                        tbSICHrsInstrument.Text = "0.0";
                        tbOtherHrsSimulator.Text = "0.0";
                        tbOtherHrsFltEngineer.Text = "0.0";
                        tbOtherHrsFltInstructor.Text = "0.0";
                        tbOtherHrsFltAttendant.Text = "0.0";
                    }

                    lbTypeCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    lbDescription.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    chkTRInactive.Checked = false;

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Clear Check List Fields
        /// </summary>
        private void ClearCheckListFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    lbCheckListCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    lbCheckListDescription.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    tbFreq.Text = "0";
                    radFreq.Items[0].Selected = false;
                    radFreq.Items[1].Selected = false;
                    tbPrevious.Text = string.Empty;
                    tbDueNext.Text = string.Empty;
                    tbAlertDate.Text = string.Empty;
                    tbAlertDays.Text = string.Empty;
                    tbGraceDate.Text = string.Empty;
                    tbGraceDays.Text = string.Empty;
                    tbAircraftType.Text = string.Empty;
                    tbOriginalDate.Text = string.Empty;
                    tbChgBaseDate.Text = string.Empty;
                    tbTotalReqFlightHrs.Text = string.Empty;
                    chkPIC91.Checked = false;
                    chkPIC135.Checked = false;
                    chkSIC91.Checked = false;
                    chkSIC135.Checked = false;
                    chkInactive.Checked = false;
                    chkSetToEndOfMonth.Checked = false;
                    chkSetToNextOfMonth.Checked = false;
                    chkSetToEndOfCalenderYear.Checked = false;
                    ckhDisableDateCalculation.Checked = false;
                    chkOneTimeEvent.Checked = false;
                    chkCompleted.Checked = false;
                    chkNonConflictedEvent.Checked = false;
                    chkRemoveFromCrewRosterRept.Checked = false;
                    chkRemoveFromChecklistRept.Checked = false;
                    chkDoNotPrintPassDueAlert.Checked = false;
                    chkPrintOneTimeEventCompleted.Checked = false;

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #endregion
        #region Checklist Check Changed Events
        protected void radFreq_SelectedIndexChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgCrewCheckList.SelectedItems[0] as GridDataItem;
                            if (radFreq.SelectedItem.Value == "1")
                            {
                                if ((chkSetToEndOfCalenderYear.Checked == false) && (chkSetToEndOfMonth.Checked == false) && (chkSetToNextOfMonth.Checked == false))
                                {
                                    FreqChangeCalculation();
                                }
                                else
                                {
                                    FreqChangeCalculationonChecked();
                                }
                                ((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text = System.Web.HttpUtility.HtmlEncode("1");
                            }
                            else
                            {
                                if ((chkSetToEndOfCalenderYear.Checked == false) && (chkSetToEndOfMonth.Checked == false) && (chkSetToNextOfMonth.Checked == false))
                                {
                                    FreqChangeCalculation();
                                }
                                else
                                {
                                    FreqChangeCalculationonChecked();
                                }
                                ((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text = System.Web.HttpUtility.HtmlEncode("2");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ZULU_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (radZULU.Checked == true)
                        {

                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('E-mail sent will now be in UTC time. E-mail sent prior to this change may be inconsistent.', 360, 50, 'Crew Roster Catalog');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        }
                        else
                        {

                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('E-mail sent will now be in Local time. E-mail sent prior to this change may be inconsistent.', 360, 50, 'Crew Roster Catalog');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void chkDisplayInactiveChecks_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetCrewCheckListDate> FinalList = new List<GetCrewCheckListDate>();
                        List<GetCrewCheckListDate> RetainList = new List<GetCrewCheckListDate>();
                        RetainList = RetainCrewCheckListGrid(dgCrewCheckList);
                        Session["CrewTypeChecklist"] = RetainList;
                        if (chkDisplayInactiveChecks.Checked == true)
                        {
                            dgCurrencyChecklist.DataSource = RetainList.Where(x => x.IsDeleted == false && x.IsInActive == true).ToList<FlightPakMasterService.GetCrewCheckListDate>();
                        }
                        else
                        {
                            dgCurrencyChecklist.DataSource = RetainList.Where(x => x.IsDeleted == false && x.IsInActive == false).ToList<FlightPakMasterService.GetCrewCheckListDate>();
                        }
                        dgCurrencyChecklist.DataBind();
                        Session.Remove("CrewTypeChecklist");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        protected void ChecklistchkPIC91_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked = chkPIC91.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkPIC135_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked = chkPIC135.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkSIC91_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked = chkSIC91.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkSIC135_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked = chkSIC135.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkInactive_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked = chkInactive.Checked;

                            if (Session["CrewCheckList"] != null && Item["CheckListCD"].Text != null)
                            {
                                List<GetCrewCheckListDate> CrewCheckList = (List<GetCrewCheckListDate>)Session["CrewCheckList"];
                                foreach (GetCrewCheckListDate CheckItem in CrewCheckList)
                                {
                                    if (CheckItem.IsDeleted == false && CheckItem.CheckListCD.ToString().Trim() == Item["CheckListCD"].Text.Trim())
                                    {
                                        if (chkInactive.Checked == true)
                                        {
                                            CheckItem.IsInActive = true;
                                        }
                                        else
                                        {
                                            CheckItem.IsInActive = false;
                                        }
                                    }
                                }
                                Session["CrewCheckList"] = CrewCheckList;
                            }
                            BindCrewCheckListGrid(Convert.ToInt64(Item.GetDataKeyValue("CrewID").ToString()));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Check Change Event for Set to end of Month Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetToEndOfMonth_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSetToNextOfMonth.Checked = false;
                        chkSetToEndOfCalenderYear.Checked = false;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;
                            if (chkSetToEndOfMonth.Checked == true)
                            {
                                DueNext_TextChanged(sender, e);
                            }
                            else
                            {
                                FreqChangeCalculation();
                            }
                            ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                            ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                            ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                            ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                            ((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Check Change Event for Set to Next Month Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetToNextOfMonth_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSetToEndOfMonth.Checked = false;
                        chkSetToEndOfCalenderYear.Checked = false;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;
                            if (chkSetToNextOfMonth.Checked == true)
                            {
                                DueNext_TextChanged(sender, e);
                            }
                            else
                            {
                                FreqChangeCalculation();
                            }
                            ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                            ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                            ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                            ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                            ((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Check Change Event for Set to end of Calender Year Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetToEndOfCalenderYear_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSetToEndOfMonth.Checked = false;
                        chkSetToNextOfMonth.Checked = false;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;
                            if (chkSetToEndOfCalenderYear.Checked == true)
                            {
                                DueNext_TextChanged(sender, e);
                            }
                            else
                            {
                                FreqChangeCalculation();
                            }
                            ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                            ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                            ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                            ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                            ((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ckhDisableDateCalculation_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked = ckhDisableDateCalculation.Checked;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Check Change Event for One Time Event Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OneTimeEvent_CheckChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkOneTimeEvent.Checked == true)
                        {
                            btnChgBaseDate.Enabled = false;
                            chkCompleted.Enabled = true;
                            chkPrintOneTimeEventCompleted.Enabled = true;
                            tbChgBaseDate.Enabled = false;
                            radFreq.Enabled = false;
                            tbFreq.Enabled = false;
                            tbDueNext.Enabled = false;
                            tbAlertDate.Enabled = false;
                            tbAlertDays.Enabled = false;
                            tbGraceDate.Enabled = false;
                            tbGraceDays.Enabled = false;
                            tbTotalReqFlightHrs.Enabled = false;
                        }
                        else if (chkOneTimeEvent.Checked == false)
                        {
                            btnChgBaseDate.Enabled = true;
                            chkCompleted.Enabled = false;
                            chkPrintOneTimeEventCompleted.Enabled = false;
                            tbChgBaseDate.Enabled = true;
                            radFreq.Enabled = true;
                            tbFreq.Enabled = true;
                            tbDueNext.Enabled = true;
                            tbAlertDate.Enabled = true;
                            tbAlertDays.Enabled = true;
                            tbGraceDate.Enabled = true;
                            tbGraceDays.Enabled = true;
                            tbTotalReqFlightHrs.Enabled = true;
                        }

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked = chkOneTimeEvent.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkCompleted_CheckChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked = chkCompleted.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkNonConflictedEvent_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked = chkNonConflictedEvent.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkRemoveFromCrewRosterRept_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked = chkRemoveFromCrewRosterRept.Checked;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkRemoveFromChecklistRept_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked = chkRemoveFromChecklistRept.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkDoNotPrintPassDueAlert_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked = chkDoNotPrintPassDueAlert.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkPrintOneTimeEventCompleted_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked = chkPrintOneTimeEventCompleted.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void CheckItemInGrid()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewCheckList.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked = chkPIC91.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked = chkPIC135.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked = chkSIC91.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked = chkSIC135.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked = chkInactive.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked = ckhDisableDateCalculation.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked = chkOneTimeEvent.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked = chkCompleted.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked = chkNonConflictedEvent.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked = chkRemoveFromCrewRosterRept.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked = chkRemoveFromChecklistRept.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked = chkPrintOneTimeEventCompleted.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked = chkDoNotPrintPassDueAlert.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;

                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #endregion
        #region "Crew Aircraft Assigned"
        #region "Crew Roster Aircraft Assigned Grid Events"
        protected void AircraftAssigned_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CrewAircraftAssigned"] != null)
                        {
                            List<FlightPakMasterService.GetAllCrewAircraftAssign> FleetList = (List<FlightPakMasterService.GetAllCrewAircraftAssign>)Session["CrewAircraftAssigned"];
                            dgAircraftAssigned.DataSource = FleetList;
                        }
                        else
                        {
                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {
                                GetAllCrewAircraftAssign oGetSelectedAircraft = new GetAllCrewAircraftAssign();
                                if (Session["SelectedCrewRosterID"] != null)
                                {
                                    oGetSelectedAircraft.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                                }
                                else
                                {
                                    oGetSelectedAircraft.CrewID = 0;
                                }
                                var CrewRosterServiceValue = CrewRosterService.GetSelectedAircraftAssignedList(oGetSelectedAircraft);
                                if (CrewRosterServiceValue.ReturnFlag == true)
                                {
                                    dgAircraftAssigned.DataSource = CrewRosterServiceValue.EntityList;
                                    Session["CrewAircraftAssigned"] = (List<FlightPakMasterService.GetAllCrewAircraftAssign>)CrewRosterServiceValue.EntityList.ToList();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void AircraftAssigned_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (MasterCatalogServiceClient AircraftAssignedService = new MasterCatalogServiceClient())
            {
                List<CrewAircraftAssigned> AircraftAssignedList = new List<CrewAircraftAssigned>();
                CrewAircraftAssigned CrewAircraftAssigned = new CrewAircraftAssigned();
                AircraftAssignedList = (List<CrewAircraftAssigned>)Session["AircraftAssignedList"];
                if (AircraftAssignedList != null)
                {
                    var Results = (from code in AircraftAssignedList
                                   select code);
                    foreach (var V in Results)
                    {
                        foreach (GridDataItem Item in dgAircraftAssigned.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("FleetID") != null)
                            {
                                Int64 TailNum = Convert.ToInt64(Item.GetDataKeyValue("FleetID").ToString().Trim());
                                if (V.FleetID == TailNum)
                                {
                                    ((CheckBox)(Item["AircraftAssigned"].FindControl("chkAircraftAssigned"))).Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion
        #endregion
        #region "Crew Passport"
        #region "Crew Roster Passport Grid Events"
        /// <summary>
        /// Bind Crew Passport into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Passport_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //  BindCrewPassportGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Bind Passport Info into Grid based on Crew Code
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private void BindCrewPassportGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewPassport"] != null)
                    {
                        List<GetAllCrewPassport> CrewPassportList = (List<GetAllCrewPassport>)Session["CrewPassport"];
                        dgPassport.DataSource = CrewPassportList;
                        dgPassport.DataBind();
                    }
                    else
                    {
                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            CrewPassengerPassport CrewPassport = new CrewPassengerPassport();
                            CrewPassport.CrewID = CrewID;
                            CrewPassport.PassengerRequestorID = null;
                            var CrewPaxPassportValue = CrewRosterService.GetCrewPassportListInfo(CrewPassport);

                            if (CrewPaxPassportValue.ReturnFlag == true)
                            {
                                List<GetAllCrewPassport> CrewPassportList = new List<GetAllCrewPassport>();
                                CrewPassportList = CrewPaxPassportValue.EntityList;
                                dgPassport.DataSource = CrewPassportList;
                                dgPassport.DataBind();
                                Session["CrewPassport"] = CrewPassportList;
                            }
                        }
                    }
                    foreach (GridDataItem Item in dgPassport.MasterTableView.Items)
                    {
                        if (!string.IsNullOrEmpty(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text))
                        {
                            ((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", ((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                        }
                        if (!string.IsNullOrEmpty(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text))
                        {
                            ((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text = String.Format("{0:" + DateFormat + "}", ((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Crew Passport Button Events"
        /// <summary>
        /// Method for Add New Passport Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddPassport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ValidatePassport())
                        {
                            List<GetAllCrewPassport> FinalList = new List<GetAllCrewPassport>();
                            List<GetAllCrewPassport> RetainList = new List<GetAllCrewPassport>();
                            List<GetAllCrewPassport> NewList = new List<GetAllCrewPassport>();
                            RetainList = RetainCrewPassportGrid(dgPassport);
                            FinalList.AddRange(RetainList);
                            GetAllCrewPassport CrewPaxVisa = new GetAllCrewPassport();
                            CrewPaxVisa.PassportID = FinalList.Count + 1;
                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                CrewPaxVisa.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                            }
                            else
                            {
                                CrewPaxVisa.CrewID = 0;
                            }
                            CrewPaxVisa.PassengerRequestorID = null;
                            NewList.Add(CrewPaxVisa);
                            FinalList.AddRange(NewList);
                            dgPassport.DataSource = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.Rebind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected bool ValidatePassport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    string PassportNum = string.Empty;
                    string IssueDate = string.Empty;
                    string ExpiryDT = string.Empty;
                    string CountryName = string.Empty;
                    bool check;
                    foreach (GridDataItem item in dgPassport.Items)
                    {
                        PassportNum = ((TextBox)item.FindControl("tbPassportNum")).Text;
                        IssueDate = ((TextBox)item.FindControl("tbIssueDT")).Text;
                        ExpiryDT = ((TextBox)item.FindControl("tbPassportExpiryDT")).Text;
                        CountryName = ((TextBox)item.FindControl("tbPassportCountry")).Text;
                        check = ((CheckBox)item.FindControl("chkChoice")).Checked;

                        if (String.IsNullOrEmpty(PassportNum))
                        {

                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Passport Number should not be Empty in Passport/Visa Tab', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(ExpiryDT))
                        {

                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Passport Expiry Date should not be Empty in Passport/Visa Tab', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (!String.IsNullOrEmpty(CountryName))
                        {
                            //if (CountryName.Trim().ToUpper() == "US")
                            //{
                            //    Int64 Validity;
                            //    bool IsValidUSPassportNumber = Int64.TryParse(PassportNum, out Validity);
                            //    bool IsValidUSPassportLength = true;
                            //    if (PassportNum.Length != 9)
                            //    {
                            //        IsValidUSPassportLength = false;
                            //    }
                            //    if (IsValidUSPassportNumber == false || IsValidUSPassportLength == false)
                            //    {
                            //        string alertMsg = "radalert('Passport Number is not valid US Passport Number in Passport/Visa Tab.', 360, 50, 'Crew');";
                            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            //        return false;
                            //    }
                            //}
                        }
                        else
                        {

                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Passport Country should not be Empty in Passport/Visa Tab', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateStart = new DateTime();
                        DateTime DateEnd = new DateTime();
                        if (!(string.IsNullOrEmpty(IssueDate)))
                        {
                            if (DateFormat != null)
                            {
                                DateStart = Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
                            }
                            else
                            {
                                DateStart = Convert.ToDateTime(IssueDate);
                            }
                        }
                        if (!(string.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateFormat != null)
                            {
                                DateEnd = Convert.ToDateTime(FormatDate(ExpiryDT, DateFormat));
                            }
                            else
                            {
                                DateEnd = Convert.ToDateTime(ExpiryDT);
                            }
                        }
                        if (!String.IsNullOrEmpty(IssueDate))
                        {
                            if (DateStart >= DateTime.Today)
                            {

                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Passport Issue Date should not be Future Date in Passport/Visa Tab.', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                        if (!(String.IsNullOrEmpty(IssueDate)) && !(String.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateStart > DateEnd)
                            {

                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Passport Issue Date should be before Expiry Date in Passport/Visa Tab', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                    }



                    return true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected bool DuplicatePassport()
        {

            List<FlightPakMasterService.CrewPassengerPassport> PassportGridInfo = new List<FlightPakMasterService.CrewPassengerPassport>();
            List<FlightPakMasterService.CrewPassengerPassport> DupePassportGridInfo = new List<FlightPakMasterService.CrewPassengerPassport>();




            foreach (GridDataItem Item in dgPassport.MasterTableView.Items)
            {
                CrewPassengerPassport CrewPaxPassportInfoListDef = new CrewPassengerPassport();
                //if (Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString()) != 0)
                //{
                //    CrewPaxPassportInfoListDef.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                //}
                //else
                //{
                //    Indentity = Indentity - 1;
                //    CrewPaxPassportInfoListDef.PassportID = Indentity;
                //}

                CrewPaxPassportInfoListDef.PassportID = 1;
                CrewPaxPassportInfoListDef.PassengerRequestorID = null;
                CrewPaxPassportInfoListDef.CrewID = 1;
                CrewPaxPassportInfoListDef.CustomerID = 1;
                if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text != string.Empty)
                {
                    CrewPaxPassportInfoListDef.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                }
                CrewPaxPassportInfoListDef.Choice = ((CheckBox)Item["Choice"].FindControl("chkChoice")).Checked;
                if (((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    if (DateFormat != null)
                    {
                        CrewPaxPassportInfoListDef.PassportExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat));
                    }
                    else
                    {
                        CrewPaxPassportInfoListDef.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                    }
                }

                if (tbLicNo.Text.Trim() != "")
                {
                    CrewPaxPassportInfoListDef.PilotLicenseNum = tbLicNo.Text;
                }
                if (((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text != string.Empty)
                {
                    CrewPaxPassportInfoListDef.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                }
                if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    if (DateFormat != null)
                    {
                        CrewPaxPassportInfoListDef.IssueDT = Convert.ToDateTime(FormatDate(((TextBox)Item["PassportExpiryDT"].FindControl("tbIssueDT")).Text, DateFormat));
                    }
                    else
                    {
                        CrewPaxPassportInfoListDef.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                    }
                }
                if (((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value != string.Empty)
                {
                    CrewPaxPassportInfoListDef.CountryID = Convert.ToInt64(((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value);
                }
                CrewPaxPassportInfoListDef.IsDeleted = false;
                PassportGridInfo.Add(CrewPaxPassportInfoListDef);
            }


            foreach (FlightPakMasterService.CrewPassengerPassport Item in PassportGridInfo)
            {
                DupePassportGridInfo = PassportGridInfo.Where(x => x.PassportNum == Item.PassportNum && x.CountryID == Item.CountryID && x.PassportExpiryDT >= DateTime.Now).ToList();
                if (DupePassportGridInfo.Count >= 2)
                {

                    string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                        + @" oManager.radalert('Passport Number " + Item.PassportNum + " Must Be Unique in Passport/Visa Tab.', 360, 50, 'Crew');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                    return false;
                }

            }
            return true;

        }

        protected void DeletePassport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if (hdnDelete.Value == "Yes")
                        //{
                        if (dgPassport.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgPassport.SelectedItems[0];
                            string PassportNumber = null;
                            if (((TextBox)(Item.FindControl("tbPassportNum"))).Text.Trim() != "")
                            {
                                PassportNumber = ((TextBox)(Item.FindControl("tbPassportNum"))).Text.Trim();
                            }

                            if (Item.GetDataKeyValue("PassportID") != null)
                            {
                                PassportNumber = Item.GetDataKeyValue("PassportID").ToString().Trim();
                            }

                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.GetAllCrewPassport> CrewPassportList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            CrewPassportList = (List<FlightPakMasterService.GetAllCrewPassport>)Session["CrewPassport"];
                            for (int Index = 0; Index < CrewPassportList.Count; Index++)
                            {
                                if (CrewPassportList[Index].PassportID == Convert.ToInt64(PassportNumber))
                                {
                                    CrewPassportList[Index].IsDeleted = true;
                                }
                            }
                            Session["CrewPassport"] = CrewPassportList;
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.GetAllCrewPassport> PassengerPassportList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            PassengerPassportList = RetainCrewPassportGrid(dgPassport);
                            for (int Index = 0; Index < PassengerPassportList.Count; Index++)
                            {
                                if (PassengerPassportList[Index].PassportID == Convert.ToInt64(PassportNumber))
                                {
                                    PassengerPassportList[Index].IsDeleted = true;
                                }
                            }
                            //PassengerPassportList.RemoveAll(x => x.PassportNum.ToString().Trim().ToUpper() == PassportNumber.Trim().ToUpper());
                            dgPassport.DataSource = PassengerPassportList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.DataBind();
                            Session["CrewPassport"] = CrewPassportList;
                        }
                        else
                        {

                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Please select the record from Passport table', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #endregion
        #region "Crew Visa"
        #region "Crew Roster Visa Grid Events"
        /// <summary>
        /// Bind Crew Additional Information into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Visa_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // BindCrewVisaGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Bind Crew Visa based on Crew Code
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private void BindCrewVisaGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewVisa"] != null)
                    {
                        List<GetAllCrewPaxVisa> CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["CrewVisa"];
                        dgVisa.DataSource = CrewPaxVisaList;
                        dgVisa.DataBind();
                    }
                    else
                    {
                        using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                        {
                            CrewPassengerVisa CrewVisa = new CrewPassengerVisa();
                            CrewVisa.CrewID = CrewID;  // Convert.ToInt64(Item.GetDataKeyValue("CrewID"));
                            CrewVisa.PassengerRequestorID = null;
                            var CrewPaxVisaList = Service.GetCrewVisaListInfo(CrewVisa);

                            if (CrewPaxVisaList.ReturnFlag == true)
                            {
                                List<GetAllCrewPaxVisa> CrewPassengerVisaList = new List<GetAllCrewPaxVisa>();
                                CrewPassengerVisaList = CrewPaxVisaList.EntityList;
                                dgVisa.DataSource = CrewPassengerVisaList;
                                dgVisa.DataBind();
                                Session["CrewVisa"] = CrewPassengerVisaList;
                            }
                        }
                    }
                    foreach (GridDataItem DateItem in dgVisa.MasterTableView.Items)
                    {
                        if (!string.IsNullOrEmpty(((TextBox)DateItem["ExpiryDT"].FindControl("tbExpiryDT")).Text))
                        {
                            ((TextBox)DateItem["ExpiryDT"].FindControl("tbExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", ((TextBox)DateItem["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                        }
                        if (!string.IsNullOrEmpty(((TextBox)DateItem["IssueDate"].FindControl("tbIssueDate")).Text))
                        {
                            ((TextBox)DateItem["IssueDate"].FindControl("tbIssueDate")).Text = String.Format("{0:" + DateFormat + "}", ((TextBox)DateItem["IssueDate"].FindControl("tbIssueDate")).Text);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);


            }

        }
        #endregion
        #region "Crew Visa Button Events"
        /// <summary>
        /// Method to Add New Visa Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddVisa_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (ValidateVisa())
                        {
                            // Declaring Lists
                            List<GetAllCrewPaxVisa> FinalList = new List<GetAllCrewPaxVisa>();
                            List<GetAllCrewPaxVisa> RetainList = new List<GetAllCrewPaxVisa>();
                            List<GetAllCrewPaxVisa> NewList = new List<GetAllCrewPaxVisa>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainCrewVisaGrid(dgVisa);
                            FinalList.AddRange(RetainList);
                            // Bind Selected New Item and add into Final List

                            GetAllCrewPaxVisa CrewPaxVisa = new GetAllCrewPaxVisa();
                            CrewPaxVisa.VisaID = 0;

                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                CrewPaxVisa.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                            }
                            else { CrewPaxVisa.CrewID = 0; }
                            CrewPaxVisa.PassengerRequestorID = null;
                            NewList.Add(CrewPaxVisa);
                            FinalList.AddRange(NewList);
                            // Bind final list into Session
                            //Session["CrewVisa"] = FinalList;      //Commented for testing Delete & add scenario
                            dgVisa.DataSource = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgVisa.Rebind();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Method to validate Issue Date and Expiry Date in the Visa grid
        /// </summary>
        protected bool ValidateVisa()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    string VisaNum = string.Empty;
                    string CountryCode = string.Empty;
                    string IssueDate = string.Empty;
                    string ExpiryDT = string.Empty;
                    foreach (GridDataItem item in dgVisa.Items)
                    {
                        VisaNum = ((TextBox)item.FindControl("tbVisaNum")).Text;
                        CountryCode = ((TextBox)item.FindControl("tbCountryCD")).Text;
                        IssueDate = ((TextBox)item.FindControl("tbIssueDate")).Text;
                        ExpiryDT = ((TextBox)item.FindControl("tbExpiryDT")).Text;
                        if (String.IsNullOrEmpty(CountryCode))
                        {

                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Visa Country should not be empty in Passport/Visa Tab', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(VisaNum))
                        {

                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Visa Number should not be empty in Passport/Visa Tab', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(ExpiryDT))
                        {

                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Visa Expiry Date should not be empty in Passport/Visa Tab', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(IssueDate))
                        {

                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Visa Issue Date should not be empty in Passport/Visa Tab', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (!(String.IsNullOrEmpty(IssueDate)) && !(String.IsNullOrEmpty(ExpiryDT)))
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateTime DateStart = new DateTime();
                            DateTime DateEnd = new DateTime();

                            if (!string.IsNullOrEmpty(IssueDate))
                            {
                                if (DateFormat != null)
                                {
                                    DateStart = Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
                                }
                                else
                                {
                                    DateStart = Convert.ToDateTime(IssueDate);
                                }
                            }
                            if (!string.IsNullOrEmpty(ExpiryDT))
                            {
                                if (DateFormat != null)
                                {
                                    DateEnd = Convert.ToDateTime(FormatDate(ExpiryDT, DateFormat));
                                }
                                else
                                {
                                    DateEnd = Convert.ToDateTime(ExpiryDT);
                                }
                            }

                            if (!String.IsNullOrEmpty(IssueDate))
                            {
                                if (DateStart > DateTime.Today)
                                {

                                    string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                        + @" oManager.radalert('Visa Issue Date should not be future date in Passport/Visa Tab.', 360, 50, 'Crew');";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                    return false;
                                }
                            }
                            if (DateStart > DateEnd)
                            {

                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Visa Issue Date should be before Expiry Date in Passport/Visa Tab', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                    }
                    return true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Delete Visa Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteVisa_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnDelete.Value == "Yes")
                        {
                            if (dgVisa.SelectedItems.Count > 0)
                            {
                                GridDataItem Item = (GridDataItem)dgVisa.SelectedItems[0];
                                string VisaNumber = "";
                                if (((TextBox)(Item["VisaNum"].FindControl("tbVisaNum"))).Text.Trim() != "")
                                {
                                    VisaNumber = ((TextBox)(Item["VisaNum"].FindControl("tbVisaNum"))).Text.Trim();
                                }
                                List<GetAllCrewPaxVisa> CrewPaxVisaList = new List<GetAllCrewPaxVisa>();
                                CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["CrewVisa"];
                                for (int Index = 0; Index < CrewPaxVisaList.Count; Index++)
                                {
                                    if (CrewPaxVisaList[Index].VisaNum == VisaNumber)
                                    {
                                        CrewPaxVisaList[Index].IsDeleted = true;
                                    }
                                }
                                Session["CrewVisa"] = CrewPaxVisaList;
                                // Retain the Grid Items and bind into List to remove the selected item.
                                List<GetAllCrewPaxVisa> CrewPaxList = new List<GetAllCrewPaxVisa>();
                                CrewPaxList = RetainCrewVisaGrid(dgVisa);
                                for (int Index = 0; Index < CrewPaxList.Count; Index++)
                                {
                                    if (CrewPaxList[Index].VisaNum == VisaNumber)
                                    {
                                        CrewPaxList[Index].IsDeleted = true;
                                    }
                                }
                                //CrewPaxList.RemoveAll(x => x.VisaNum.ToString().Trim().ToUpper() == VisaNumber.Trim().ToUpper());
                                dgVisa.DataSource = CrewPaxList.Where(x => x.IsDeleted != true).ToList();
                                dgVisa.DataBind();
                                Session["CrewVisa"] = CrewPaxVisaList;
                            }
                            else
                            {

                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Please select the record from Visa table', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        #endregion
        #endregion
        #region "Crew Currency and Checklist Tab"
        #region"Crew Roaster Currency Checklist Grid Events"
        /// <summary>
        /// Bind Currency Checklist based on Crew Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CurrencyChecklist_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // BindCrewCheckListGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void CurrencyChecklist_ItemDataBound(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (!string.IsNullOrEmpty(((Label)Item["DueDT"].FindControl("lbDueDT")).Text))
                            {
                                ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(((Label)Item["DueDT"].FindControl("lbDueDT")).Text))));
                            }
                            if (!string.IsNullOrEmpty(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text))
                            {
                                ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text))));
                            }
                            if (!string.IsNullOrEmpty(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text))
                            {
                                ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text))));
                            }
                            foreach (GridDataItem CheckItem in dgCrewCheckList.Items)
                            {
                                if (((Label)CheckItem["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text == ((Label)Item["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text)
                                {
                                    e.Item.BackColor = CheckItem.BackColor;
                                    if (e.Item.BackColor.Name != "0" && e.Item.BackColor.Name != "White")
                                    {
                                        //Start
                                        if (Session["CurrentFltHrsSetting"] != null)
                                        {
                                            List<FlightPakMasterService.GetAllCurrentFlightHours> GetCurrentFlightHoursCrewCheckList = new List<FlightPakMasterService.GetAllCurrentFlightHours>();
                                            GetCurrentFlightHoursCrewCheckList = (List<FlightPakMasterService.GetAllCurrentFlightHours>)Session["CurrentFltHrsSetting"];
                                            var CurrentFltValues = GetCurrentFlightHoursCrewCheckList.Where(x => x.CheckListCD.ToString().ToUpper().Trim().Equals(CheckItem.GetDataKeyValue("CheckListCD").ToString().ToUpper().Trim())).ToList();
                                            if (CurrentFltValues != null && CurrentFltValues.Count > 0)
                                            {
                                                if (((FlightPakMasterService.GetAllCurrentFlightHours)CurrentFltValues[0]).FLIGHTHOURS != null)
                                                {
                                                    ((Label)Item["CurrentFltHrs"].FindControl("lbCurrentFltHrs")).Text = System.Web.HttpUtility.HtmlEncode(((FlightPakMasterService.GetAllCurrentFlightHours)CurrentFltValues[0]).FLIGHTHOURS.ToString());
                                                }
                                                else
                                                {
                                                    ((Label)Item["CurrentFltHrs"].FindControl("lbCurrentFltHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                                }
                                            }
                                            else
                                            {
                                                ((Label)Item["CurrentFltHrs"].FindControl("lbCurrentFltHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                            }
                                        }
                                        else
                                        {
                                            ((Label)Item["CurrentFltHrs"].FindControl("lbCurrentFltHrs")).Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        }
                                        //End
                                    }
                                    else
                                    {
                                        Item.Display = false;
                                    }

                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        #endregion
        #region"Crew Roaster Currency Grid Events"

        private void BindCrewCurrency(Int64 CrewID)
        {
            using (MasterCatalogServiceClient CrewrCurrencyService = new MasterCatalogServiceClient())
            {
                GetAllCompanyMaster Company = new GetAllCompanyMaster();
                if (CrewID > 0)
                {
                    if (Session["HomebaseSettings"] != null)
                    {
                        var CompanyCurrencyList = ((List<GetAllCompanyMaster>)Session["HomebaseSettings"]).Where(x => x.HomebaseID.Equals(UserPrincipal.Identity._homeBaseId)).ToList();
                        if (CompanyCurrencyList.Count() != 0 && CompanyCurrencyList != null)
                        {
                            MasterCatalogServiceClient CrewRoasterCurrencyService = new MasterCatalogServiceClient();
                            var CurrencyList = CrewRoasterCurrencyService.GetAllCrewCurrency(CrewID);
                            if (CurrencyList.ReturnFlag == true)
                            {
                                dgCurrency.Columns[0].HeaderText = "Aircraft Type";
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayLanding != null)
                                {
                                    dgCurrency.Columns[1].HeaderText = "L/D" + ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayLanding.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[1].HeaderText = "L/D 0";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).NightLanding != null)
                                {
                                    dgCurrency.Columns[2].HeaderText = "L/N" + ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).NightLanding.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[2].HeaderText = "L/N 0";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).TakeoffDay != null)
                                {
                                    dgCurrency.Columns[3].HeaderText = "T/D" + ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).TakeoffDay.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[3].HeaderText = "T/D 0";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).TakeoffNight != null)
                                {
                                    dgCurrency.Columns[4].HeaderText = "T/N" + ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).TakeoffNight.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[4].HeaderText = "T/N 0";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Approach != null)
                                {
                                    dgCurrency.Columns[5].HeaderText = "Appr" + ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Approach.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[5].HeaderText = "Appr 0";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Instrument != null)
                                {
                                    dgCurrency.Columns[6].HeaderText = "Instr" + ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Instrument.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[6].HeaderText = "Instr 0";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Day7 != null)
                                {
                                    dgCurrency.Columns[7].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Day7.ToString() + "Days";
                                }
                                else
                                {
                                    dgCurrency.Columns[7].HeaderText = "0 Days";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).RestDays != null)
                                {
                                    dgCurrency.Columns[8].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).RestDays.ToString() + "Day Rest";
                                }
                                else
                                {
                                    dgCurrency.Columns[8].HeaderText = "0 Day Rest";
                                }
                                dgCurrency.Columns[9].HeaderText = "Cal Mon.";
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Day90 != null)
                                {
                                    dgCurrency.Columns[10].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Day90.ToString() + "Days";
                                }
                                else
                                {
                                    dgCurrency.Columns[10].HeaderText = "0 Days";
                                }
                                dgCurrency.Columns[11].HeaderText = "Cal Qtr.";
                                dgCurrency.Columns[12].HeaderText = "Cal Qtr. Rest";
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Month6 != null)
                                {
                                    dgCurrency.Columns[13].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Month6.ToString() + "Months";
                                }
                                else
                                {
                                    dgCurrency.Columns[13].HeaderText = "0 Months";
                                }

                                dgCurrency.Columns[14].HeaderText = "Prev 2 Qtrs";

                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Month12 != null)
                                {
                                    dgCurrency.Columns[15].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Month12.ToString() + "Months";
                                }
                                else
                                {
                                    dgCurrency.Columns[15].HeaderText = "0 Months";
                                }
                                dgCurrency.Columns[16].HeaderText = "Cal. Yr";
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Day365 != null)
                                {
                                    dgCurrency.Columns[17].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Day365.ToString() + "Days";
                                }
                                else
                                {
                                    dgCurrency.Columns[17].HeaderText = "0 Days";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Day30 != null)
                                {
                                    dgCurrency.Columns[18].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Day30.ToString() + "Days";
                                }
                                else
                                {
                                    dgCurrency.Columns[18].HeaderText = "0 Days";
                                }
                                dgCurrency.DataSource = CurrencyList.EntityList;
                                dgCurrency.DataBind();
                            }
                            else
                            {
                                dgCurrency.Columns[0].HeaderText = "Aircraft Type";
                                dgCurrency.Columns[1].HeaderText = "L/D 0";
                                dgCurrency.Columns[2].HeaderText = "L/N 0";
                                dgCurrency.Columns[3].HeaderText = "T/D 0";
                                dgCurrency.Columns[4].HeaderText = "T/N 0";
                                dgCurrency.Columns[5].HeaderText = "Appr 0";
                                dgCurrency.Columns[6].HeaderText = "Instr 0";
                                dgCurrency.Columns[7].HeaderText = "0 Days";
                                dgCurrency.Columns[8].HeaderText = "0 Day Rest";
                                dgCurrency.Columns[9].HeaderText = "Cal Mon.";
                                dgCurrency.Columns[10].HeaderText = "0 Days";
                                dgCurrency.Columns[11].HeaderText = "Cal Qtr.";
                                dgCurrency.Columns[12].HeaderText = "Cal Qtr. Rest";
                                dgCurrency.Columns[13].HeaderText = "0 Months";
                                dgCurrency.Columns[14].HeaderText = "Prev 2 Qtrs";
                                dgCurrency.Columns[15].HeaderText = "0 Months";
                                dgCurrency.Columns[16].HeaderText = "Cal. Yr";
                                dgCurrency.Columns[17].HeaderText = "0 Days";
                                dgCurrency.Columns[18].HeaderText = "0 Days";
                                dgCurrency.DataSource = CurrencyList.EntityList;
                                dgCurrency.DataBind();
                            }
                        }
                    }
                    else
                    {
                        dgCurrency.Columns[0].HeaderText = "Aircraft Type";
                        dgCurrency.Columns[1].HeaderText = "L/D 0";
                        dgCurrency.Columns[2].HeaderText = "L/N 0";
                        dgCurrency.Columns[3].HeaderText = "T/D 0";
                        dgCurrency.Columns[4].HeaderText = "T/N 0";
                        dgCurrency.Columns[5].HeaderText = "Appr 0";
                        dgCurrency.Columns[6].HeaderText = "Instr 0";
                        dgCurrency.Columns[7].HeaderText = "0 Days";
                        dgCurrency.Columns[8].HeaderText = "0 Day Rest";
                        dgCurrency.Columns[9].HeaderText = "Cal Mon.";
                        dgCurrency.Columns[10].HeaderText = "0 Days";
                        dgCurrency.Columns[11].HeaderText = "Cal Qtr.";
                        dgCurrency.Columns[12].HeaderText = "Cal Qtr. Rest";
                        dgCurrency.Columns[13].HeaderText = "0 Months";
                        dgCurrency.Columns[14].HeaderText = "Prev 2 Qtrs";
                        dgCurrency.Columns[15].HeaderText = "0 Months";
                        dgCurrency.Columns[16].HeaderText = "Cal. Yr";
                        dgCurrency.Columns[17].HeaderText = "0 Days";
                        dgCurrency.Columns[18].HeaderText = "0 Days";
                        MasterCatalogServiceClient CrewRoasterCurrencyService = new MasterCatalogServiceClient();
                        var CurrencyList = CrewRoasterCurrencyService.GetAllCrewCurrency(CrewID);
                        dgCurrency.DataSource = CurrencyList.EntityList;
                        dgCurrency.DataBind();
                    }
                }
                else
                {
                    dgCurrency.Columns[0].HeaderText = "Aircraft Type";
                    dgCurrency.Columns[1].HeaderText = "L/D 0";
                    dgCurrency.Columns[2].HeaderText = "L/N 0";
                    dgCurrency.Columns[3].HeaderText = "T/D 0";
                    dgCurrency.Columns[4].HeaderText = "T/N 0";
                    dgCurrency.Columns[5].HeaderText = "Appr 0";
                    dgCurrency.Columns[6].HeaderText = "Instr 0";
                    dgCurrency.Columns[7].HeaderText = "0 Days";
                    dgCurrency.Columns[8].HeaderText = "0 Day Rest";
                    dgCurrency.Columns[9].HeaderText = "Cal Mon.";
                    dgCurrency.Columns[10].HeaderText = "0 Days";
                    dgCurrency.Columns[11].HeaderText = "Cal Qtr.";
                    dgCurrency.Columns[12].HeaderText = "Cal Qtr. Rest";
                    dgCurrency.Columns[13].HeaderText = "0 Months";
                    dgCurrency.Columns[14].HeaderText = "Prev 2 Qtrs";
                    dgCurrency.Columns[15].HeaderText = "0 Months";
                    dgCurrency.Columns[16].HeaderText = "Cal. Yr";
                    dgCurrency.Columns[17].HeaderText = "0 Days";
                    dgCurrency.Columns[18].HeaderText = "0 Days";
                    MasterCatalogServiceClient CrewRoasterCurrencyService = new MasterCatalogServiceClient();
                    var CurrencyList = CrewRoasterCurrencyService.GetAllCrewCurrency(CrewID);
                    dgCurrency.DataSource = CurrencyList.EntityList;
                    dgCurrency.DataBind();
                }
            }
        }
        protected void Currency_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            MasterCatalogServiceClient CrewrCurrencyService = new MasterCatalogServiceClient();
                            GetAllCompanyMaster Company = new GetAllCompanyMaster();
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Session["HomebaseSettings"] != null)
                            {
                                var CompanyCurrencyList = ((List<GetAllCompanyMaster>)Session["HomebaseSettings"]).Where(x => x.HomebaseID.Equals(UserPrincipal.Identity._homeBaseId)).ToList();
                                if (CompanyCurrencyList.Count() != 0 && CompanyCurrencyList != null)
                                {
                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayLandingMinimum != null)
                                    {
                                        if (Convert.ToDecimal(Item["LandingDay"].Text) < ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayLandingMinimum)
                                        {
                                            Item["LandingDay"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["LandingDay"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).NightllandingMinimum != null)
                                    {
                                        if (Convert.ToDecimal(Item["L_N90"].Text) < ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).NightllandingMinimum)
                                        {
                                            Item["L_N90"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["L_N90"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).TakeoffDayMinimum != null)
                                    {
                                        if (Convert.ToDecimal(Item["T_D90"].Text) < ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).TakeoffDayMinimum)
                                        {
                                            Item["T_D90"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["T_D90"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).TakeofNightMinimum != null)
                                    {
                                        if (Convert.ToDecimal(Item["T_N90"].Text) < ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).TakeofNightMinimum)
                                        {
                                            Item["T_N90"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["T_N90"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).ApproachMinimum != null)
                                    {
                                        if (Convert.ToDecimal(Item["Appr90"].Text) < ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).ApproachMinimum)
                                        {
                                            Item["Appr90"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["Appr90"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).InstrumentMinimum != null)
                                    {
                                        if (Convert.ToDecimal(Item["Instr90"].Text) < ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).InstrumentMinimum)
                                        {
                                            Item["Instr90"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["Instr90"].ForeColor = System.Drawing.Color.Red;
                                    }
                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["Instr90"].Text)))
                                    {
                                        Item["Instr90"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["Instr90"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["Instr90"].Text).Contains(":")))
                                        {
                                            Item["Instr90"].Text = Item["Instr90"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["Instr90"].Text = (System.Math.Round(Convert.ToDecimal((Item["Instr90"].Text).ToString()), 1).ToString());
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayMaximum7 != null)
                                    {
                                        if (Convert.ToDecimal(Item["days7"].Text) > ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayMaximum7)
                                        {
                                            Item["days7"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["days7"].ForeColor = System.Drawing.Color.Red;
                                    }
                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["days7"].Text)))
                                    {
                                        Item["days7"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["days7"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["days7"].Text).Contains(":")))
                                        {
                                            Item["days7"].Text = Item["days7"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["days7"].Text = (System.Math.Round(Convert.ToDecimal((Item["days7"].Text).ToString()), 1).ToString());
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).RestDaysMinimum != null)
                                    {
                                        if (Convert.ToDecimal(Item["dayrest7"].Text) < ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).RestDaysMinimum)
                                        {
                                            Item["dayrest7"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["dayrest7"].ForeColor = System.Drawing.Color.Red;
                                    }


                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["dayrest7"].Text)))
                                    {
                                        Item["dayrest7"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["dayrest7"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["dayrest7"].Text).Contains(":")))
                                        {
                                            Item["dayrest7"].Text = Item["dayrest7"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["dayrest7"].Text = (System.Math.Round(Convert.ToDecimal((Item["dayrest7"].Text).ToString()), 1).ToString());
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).CalendarMonthMaximum != null)
                                    {
                                        if (Convert.ToDecimal(Item["CalMon"].Text) > ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).CalendarMonthMaximum)
                                        {
                                            Item["CalMon"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["CalMon"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["CalMon"].Text)))
                                    {
                                        Item["CalMon"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["CalMon"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["CalMon"].Text).Contains(":")))
                                        {
                                            Item["CalMon"].Text = Item["CalMon"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["CalMon"].Text = (System.Math.Round(Convert.ToDecimal((Item["CalMon"].Text).ToString()), 1).ToString());
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayMaximum90 != null)
                                    {
                                        if (Convert.ToDecimal(Item["days90"].Text) > ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayMaximum90)
                                        {
                                            Item["days90"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["days90"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["days90"].Text)))
                                    {
                                        Item["days90"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["days90"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["days90"].Text).Contains(":")))
                                        {
                                            Item["days90"].Text = Item["days90"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["days90"].Text = (System.Math.Round(Convert.ToDecimal((Item["days90"].Text).ToString()), 1).ToString());
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).CalendarQTRMaximum != null)
                                    {
                                        if (Convert.ToDecimal(Item["CalQtr"].Text) > ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).CalendarQTRMaximum)
                                        {
                                            Item["CalQtr"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["CalQtr"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["CalQtr"].Text)))
                                    {
                                        Item["CalQtr"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["CalQtr"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["CalQtr"].Text).Contains(":")))
                                        {
                                            Item["CalQtr"].Text = Item["CalQtr"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["CalQtr"].Text = (System.Math.Round(Convert.ToDecimal((Item["CalQtr"].Text).ToString()), 1).ToString());
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).RestCalendarQTRMinimum != null)
                                    {
                                        if (Convert.ToDecimal(Item["CalQtrRest"].Text) < ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).RestCalendarQTRMinimum)
                                        {
                                            Item["CalQtrRest"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["CalQtrRest"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["CalQtrRest"].Text)))
                                    {
                                        Item["CalQtrRest"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["CalQtrRest"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["CalQtrRest"].Text).Contains(":")))
                                        {
                                            Item["CalQtrRest"].Text = Item["CalQtrRest"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["CalQtrRest"].Text = (System.Math.Round(Convert.ToDecimal((Item["CalQtrRest"].Text).ToString()), 1).ToString());
                                    }


                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).MonthMaximum6 != null)
                                    {
                                        if (Convert.ToDecimal(Item["Mon6"].Text) > ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).MonthMaximum6)
                                        {
                                            Item["Mon6"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["Mon6"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["Mon6"].Text)))
                                    {
                                        Item["Mon6"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["Mon6"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["Mon6"].Text).Contains(":")))
                                        {
                                            Item["Mon6"].Text = Item["Mon6"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["Mon6"].Text = (System.Math.Round(Convert.ToDecimal((Item["Mon6"].Text).ToString()), 1).ToString());
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Previous2QTRMaximum != null)
                                    {
                                        if (Convert.ToDecimal(Item["Prev2Qtrs"].Text) > ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).Previous2QTRMaximum)
                                        {
                                            Item["Prev2Qtrs"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["Prev2Qtrs"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["Prev2Qtrs"].Text)))
                                    {
                                        Item["Prev2Qtrs"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["Prev2Qtrs"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["Prev2Qtrs"].Text).Contains(":")))
                                        {
                                            Item["Prev2Qtrs"].Text = Item["Prev2Qtrs"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["Prev2Qtrs"].Text = (System.Math.Round(Convert.ToDecimal((Item["Prev2Qtrs"].Text).ToString()), 1).ToString());
                                    }


                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).MonthMaximum12 != null)
                                    {
                                        if (Convert.ToDecimal(Item["Mon12"].Text) > ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).MonthMaximum12)
                                        {
                                            Item["Mon12"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["Mon12"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["Mon12"].Text)))
                                    {
                                        Item["Mon12"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["Mon12"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["Mon12"].Text).Contains(":")))
                                        {
                                            Item["Mon12"].Text = Item["Mon12"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["Mon12"].Text = (System.Math.Round(Convert.ToDecimal((Item["Mon12"].Text).ToString()), 1).ToString());
                                    }


                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).CalendarYearMaximum != null)
                                    {
                                        if (Convert.ToDecimal(Item["Calyr"].Text) > ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).CalendarYearMaximum)
                                        {
                                            Item["Calyr"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["Calyr"].ForeColor = System.Drawing.Color.Red;
                                    }

                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["Calyr"].Text)))
                                    {
                                        Item["Calyr"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["Calyr"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["Calyr"].Text).Contains(":")))
                                        {
                                            Item["Calyr"].Text = Item["Calyr"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["Calyr"].Text = (System.Math.Round(Convert.ToDecimal((Item["Calyr"].Text).ToString()), 1).ToString());
                                    }

                                    if (((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayMaximum30 != null)
                                    {
                                        if (Convert.ToDecimal(Item["days30"].Text) > ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)CompanyCurrencyList[0]).DayMaximum30)
                                        {
                                            Item["days30"].ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        Item["days30"].ForeColor = System.Drawing.Color.Red;
                                    }
                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["Days365"].Text)))
                                    {
                                        Item["Days365"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["Days365"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["Days365"].Text).Contains(":")))
                                        {
                                            Item["Days365"].Text = Item["Days365"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["Days365"].Text = (System.Math.Round(Convert.ToDecimal((Item["Days365"].Text).ToString()), 1).ToString());
                                    }
                                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(Item["days30"].Text)))
                                    {
                                        Item["days30"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((Item["days30"].Text).ToString()), 3).ToString());
                                        if (!(Convert.ToString(Item["days30"].Text).Contains(":")))
                                        {
                                            Item["days30"].Text = Item["days30"].Text + ":00";
                                        }
                                    }
                                    else
                                    {
                                        Item["days30"].Text = (System.Math.Round(Convert.ToDecimal((Item["days30"].Text).ToString()), 1).ToString());
                                    }

                                }

                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }

        #endregion
        #region "Crew Currency Button Events"

        protected void chkDisplayInactiveChecklist_CheckedChanged(object sender, EventArgs e)
        {
            if (Session["SelectedCrewRosterID"] != null)
            {
                if (Session["CrewCheckList"] != null)
                {
                    List<GetCrewCheckListDate> CrewCheckList = (List<GetCrewCheckListDate>)Session["CrewCheckList"];
                    if (chkDisplayInactiveChecklist.Checked == true)
                    {
                        dgCrewCheckList.DataSource = CrewCheckList.Where(x => x.IsInActive == false);
                    }
                    else
                    {
                        dgCrewCheckList.DataSource = CrewCheckList;
                    }
                    dgCrewCheckList.DataBind();
                    // Bind Crew Checklist Currency Tab Grid
                    dgCurrencyChecklist.DataSource = CrewCheckList;
                    dgCurrencyChecklist.DataBind();
                }
            }
        }

        /// <summary>
        /// Refresh the Grid based on condition
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RefreshView_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tabSwitchViews.SelectedIndex == 1)
                        {
                            List<GetCrewCheckListDate> FinalList = new List<GetCrewCheckListDate>();
                            List<GetCrewCheckListDate> RetainList = new List<GetCrewCheckListDate>();
                            // Retain Grid Items and bind into List and add into Final List
                            if (btnSaveChanges.Visible == true)
                            {
                                RetainList = RetainCrewCheckListGrid(dgCrewCheckList);
                                // Bind final list into Session
                                Session["CrewTypeChecklist"] = RetainList;
                                // Bind final list into Grid
                                //dgCurrencyChecklist.DataSource = RetainList;

                                if (chkDisplayInactiveChecks.Checked == true)
                                {
                                    dgCurrencyChecklist.DataSource = RetainList.Where(x => x.IsDeleted == false && x.IsInActive == true).ToList<FlightPakMasterService.GetCrewCheckListDate>();
                                }
                                else
                                {
                                    dgCurrencyChecklist.DataSource = RetainList.Where(x => x.IsDeleted == false && x.IsInActive == false).ToList<FlightPakMasterService.GetCrewCheckListDate>();
                                }

                                dgCurrencyChecklist.DataBind();
                                // Empty Newly added Session item
                                Session.Remove("CrewTypeChecklist");
                            }
                            //
                            //GridDataItem Item = (GridDataItem)Session["SelectedCrewRosterID"];
                            Int64 TempID = 0;
                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                TempID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                            }
                            BindCrewCurrency(TempID);
                        }
                        else
                        {
                            List<GetCrewCheckListDate> FinalList = new List<GetCrewCheckListDate>();
                            List<GetCrewCheckListDate> RetainList = new List<GetCrewCheckListDate>();
                            // Retain Grid Items and bind into List and add into Final List
                            if (btnSaveChanges.Visible == true)
                            {
                                RetainList = RetainCrewCheckListGrid(dgCrewCheckList);
                                // Bind final list into Session
                                Session["CrewTypeChecklist"] = RetainList;
                                // Bind final list into Grid
                                dgCurrencyChecklist.DataSource = RetainList;
                                dgCurrencyChecklist.DataBind();
                                // Empty Newly added Session item
                                Session.Remove("CrewTypeChecklist");
                            }
                            //
                            //GridDataItem Item = (GridDataItem)Session["SelectedCrewRosterID"];
                            Int64 TempID = 0;
                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                TempID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                            }
                            BindCrewCurrency(TempID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #endregion
        #region "Common"
        #region "Predefined"
        Int64 CountryIDvalue;
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        // Declare Default Date Format
        string DateFormat = string.Empty;
        string CrewRatingAircraftCD = "";
        Int64 CrewRatingCrewID = 0;
        private bool CrewRosterPageNavigated = false;
        List<Int64> DeletedAircraftType = new List<Int64>();
        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }

        protected FlightPakMasterService.Company CompanySett = new FlightPakMasterService.Company();

        #endregion
        #region "PageLoad"
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrewRoster, dgCrewRoster, RadAjaxLoadingPanel1);
                        //store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCrewRoster.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                        // Set Logged-in User Name
                        lbToday.Text = System.Web.HttpUtility.HtmlEncode(GetDateWithoutSeconds(Convert.ToDateTime(DateTime.UtcNow)));//.ToShortDateString();

                        //lbToday.Text = Convert.ToString(Convert.ToDateTime(DateTime.Now).ToShortDateString());
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewCrewRosterReports);
                        //lbtnReports.Visible = lbtnSaveReports.Visible = true;
                        Session.Remove("HomebaseSettings");
                        isPassportPopup = Convert.ToBoolean(Request.QueryString["IsPassport"]) == null ? false : Convert.ToBoolean(Request.QueryString["IsPassport"]);

                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                            RadDatePicker1.DateInput.DateFormat = DateFormat;
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                            RadDatePicker1.DateInput.DateFormat = DateFormat;
                        }
                        DefaultFillChecklistEdit();
                        if (UserPrincipal != null && UserPrincipal.Identity._clientId != null && UserPrincipal.Identity._clientId != 0)
                        {
                            tbClientCodeFilter.Enabled = false;
                            tbClientCodeFilter.Text = UserPrincipal.Identity._clientCd.ToString().Trim();
                            btnClientCodeFilter.Enabled = false;
                        }
                        else
                        {
                            tbClientCodeFilter.Enabled = true;
                            btnClientCodeFilter.Enabled = true;
                        }
                        if (UserPrincipal.Identity._fpSettings._IsAPISSupport != null)
                        {
                            if (UserPrincipal.Identity._fpSettings._IsAPISSupport == true)
                            {
                                lbFirstName.CssClass = "important-bold-text";
                                lbMiddleName.CssClass = "important-text";
                                lbLastName.CssClass = "important-bold-text";
                                lbAddress1.CssClass = "important-text";
                                lbCity.CssClass = "important-text";
                                lbState.CssClass = "important-text";
                                lbCountry.CssClass = "important-text";
                                lbLicNo.CssClass = "important-text";
                                lbPostal.CssClass = "important-text";
                                lbDateOfBirth.CssClass = "important-text";
                                lbCityOfBirth.CssClass = "important-text";
                                lbLicCountry.CssClass = "important-text";
                                lbStateOfBirth.CssClass = "important-text";
                                lbCountryOfBirth.CssClass = "important-text";
                                lbCitizenship.CssClass = "important-text";
                            }
                            else
                            {
                                lbFirstName.CssClass = "mnd_text";
                                lbLastName.CssClass = "mnd_text";
                                lbMiddleName.ForeColor = System.Drawing.Color.Black;
                                lbAddress1.ForeColor = System.Drawing.Color.Black;
                                lbCity.ForeColor = System.Drawing.Color.Black;
                                lbState.ForeColor = System.Drawing.Color.Black;
                                lbCountry.ForeColor = System.Drawing.Color.Black;
                                lbLicNo.ForeColor = System.Drawing.Color.Black;
                                lbPostal.ForeColor = System.Drawing.Color.Black;
                                lbDateOfBirth.ForeColor = System.Drawing.Color.Black;
                                lbCityOfBirth.ForeColor = System.Drawing.Color.Black;
                                lbLicCountry.ForeColor = System.Drawing.Color.Black;
                                lbStateOfBirth.ForeColor = System.Drawing.Color.Black;
                                lbCountryOfBirth.ForeColor = System.Drawing.Color.Black;
                                lbCitizenship.ForeColor = System.Drawing.Color.Black;
                            }
                        }
                        else
                        {
                            lbFirstName.CssClass = "mnd_text";
                            lbLastName.CssClass = "mnd_text";
                            lbMiddleName.ForeColor = System.Drawing.Color.Black;
                            lbAddress1.ForeColor = System.Drawing.Color.Black;
                            lbCity.ForeColor = System.Drawing.Color.Black;
                            lbState.ForeColor = System.Drawing.Color.Black;
                            lbCountry.ForeColor = System.Drawing.Color.Black;
                            lbLicNo.ForeColor = System.Drawing.Color.Black;
                            lbPostal.ForeColor = System.Drawing.Color.Black;
                            lbDateOfBirth.ForeColor = System.Drawing.Color.Black;
                            lbCityOfBirth.ForeColor = System.Drawing.Color.Black;
                            lbLicCountry.ForeColor = System.Drawing.Color.Black;
                            lbStateOfBirth.ForeColor = System.Drawing.Color.Black;
                            lbCountryOfBirth.ForeColor = System.Drawing.Color.Black;
                            lbCitizenship.ForeColor = System.Drawing.Color.Black;
                        }
                        setMaxlenthofHours();

                        if (!IsPostBack)
                        {
                            if (Session["SelectedCrewRosterID"] != null)
 	                        {
                                if(!IsPopUp)
 	                                Session.Remove("SelectedCrewRosterID");
 	                        }
 	
                            if (Session["SelectedCrewRosterCD"] != null)
 	                        {
                                Session.Remove("SelectedCrewRosterCD");
 	                        }
                            CrewDisplaySettings();
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgCrewRoster.Rebind();
                                ReadOnlyForm();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                                {
                                    chkHomeBaseOnly.Checked = true;
                                }
                                chkActiveOnly.Checked = true;

                                Session["StandardFlightpakConversion"] = LoadStandardFPConversion();
                                // Default Control Appearence
                                pnlBeginning.Visible = false;
                                pnlCurrent.Visible = true;
                                CheckAutorization(Permission.Database.ViewCrewRoster);
                                tabSwitchViews.SelectedIndex = 1;
                                RadPageView1.Selected = true;
                                BindCrewCurrency(0);
                                // Call the Default Selection Items
                                DefaultSelection(true);
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                                CreateDictionayForImgUpload();
                            }


                            using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == UserPrincipal.Identity._customerID && x.HomebaseID == UserPrincipal.Identity._homeBaseId).ToList();
                                Session["HomebaseSettings"] = objCompany;
                            }

                        }
                        else
                        {
                            if (Session["HomebaseSettings"] == null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == UserPrincipal.Identity._customerID && x.HomebaseID == UserPrincipal.Identity._homeBaseId).ToList();
                                    if (objCompany.Count > 0)
                                    {
                                        CompanySett.CrewOFullName = objCompany[0].CrewOFullName; //1;
                                        CompanySett.CrewOMiddle = objCompany[0].CrewOMiddle;//2;
                                        CompanySett.CrewOLast = objCompany[0].CrewOLast;//3;
                                        CompanySett.CrewOLastFirst = objCompany[0].CrewOLastFirst; //1;
                                        CompanySett.CrewOLastMiddle = objCompany[0].CrewOLastMiddle;//2;
                                        CompanySett.CrewOLastLast = objCompany[0].CrewOLastLast;//3;
                                        CompanySett.IsEnableTSAPX = objCompany[0].IsEnableTSAPX;
                                        CompanySett.IsAutoCrew = objCompany[0].IsAutoCrew;
                                    }
                                    Session["HomebaseSettings"] = objCompany;
                                }
                            }
                            else
                            {
                                var objCompany = ((List<GetAllCompanyMaster>)Session["HomebaseSettings"]).Where(x => x.CustomerID == UserPrincipal.Identity._customerID && x.HomebaseID == UserPrincipal.Identity._homeBaseId).ToList();
                                if (objCompany.Count > 0)
                                {
                                    CompanySett.CrewOFullName = objCompany[0].CrewOFullName; //1;
                                    CompanySett.CrewOMiddle = objCompany[0].CrewOMiddle;//2;
                                    CompanySett.CrewOLast = objCompany[0].CrewOLast;//3;
                                    CompanySett.CrewOLastFirst = objCompany[0].CrewOLastFirst; //1;
                                    CompanySett.CrewOLastMiddle = objCompany[0].CrewOLastMiddle;//2;
                                    CompanySett.CrewOLastLast = objCompany[0].CrewOLastLast;//3;
                                    CompanySett.IsEnableTSAPX = objCompany[0].IsEnableTSAPX;
                                }
                            }
                        }



                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }



        }


        public void CrewDisplaySettings()
        {
            if (GetCrewRatingDisplay(0))
            {
                divDisplay.Visible = true;
                divModify.Visible = false;
                btnFlightExp.Text = "Modify Beginning Flt. Experience";

            }
            else
            {
                divDisplay.Visible = false;
                divModify.Visible = true;
                btnFlightExp.Text = "Display Current Flight Experience";
            }
        }

        private void setMaxlenthofHours()
        {
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
            {
                tbTotalHrsInType.MaxLength = 10;
                tbTotalHrsDay.MaxLength = 10;
                tbTotalHrsNight.MaxLength = 10;
                tbTotalHrsInstrument.MaxLength = 10;
                tbPICHrsInType.MaxLength = 10;
                tbPICHrsDay.MaxLength = 10;
                tbPICHrsNight.MaxLength = 10;
                tbPICHrsInstrument.MaxLength = 10;
                tbSICInTypeHrs.MaxLength = 10;
                tbSICHrsDay.MaxLength = 10;
                tbSICHrsNight.MaxLength = 10;
                tbSICHrsInstrument.MaxLength = 10;
                tbOtherHrsSimulator.MaxLength = 10;
                tbOtherHrsFltEngineer.MaxLength = 10;
                tbOtherHrsFltInstructor.MaxLength = 10;
                tbOtherHrsFltAttendant.MaxLength = 10;
            }
            else
            {
                tbTotalHrsInType.MaxLength = 9;
                tbTotalHrsDay.MaxLength = 9;
                tbTotalHrsNight.MaxLength = 9;
                tbTotalHrsInstrument.MaxLength = 9;
                tbPICHrsInType.MaxLength = 9;
                tbPICHrsDay.MaxLength = 9;
                tbPICHrsNight.MaxLength = 9;
                tbPICHrsInstrument.MaxLength = 9;
                tbSICInTypeHrs.MaxLength = 9;
                tbSICHrsDay.MaxLength = 9;
                tbSICHrsNight.MaxLength = 9;
                tbSICHrsInstrument.MaxLength = 9;
                tbOtherHrsSimulator.MaxLength = 9;
                tbOtherHrsFltEngineer.MaxLength = 9;
                tbOtherHrsFltInstructor.MaxLength = 9;
                tbOtherHrsFltAttendant.MaxLength = 9;
            }

        }
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgCrewRoster.Rebind();
                    }
                    if (dgCrewRoster.MasterTableView.Items.Count > 0)
                    {
                            if (dgCrewRoster.Items[0].GetDataKeyValue("CrewID") != null)
                            {
                                if (!IsPopUp)
                                    Session["SelectedCrewRosterID"] = dgCrewRoster.Items[0].GetDataKeyValue("CrewID").ToString();
                            }

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        GridEnable(true, true, true);
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    target.Focus();
                    IsEmptyCheck = false;
                }
            }
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewRosterID"] != null)
                    {
                        string CrewID = Session["SelectedCrewRosterID"].ToString();
                        foreach (GridDataItem item in dgCrewRoster.MasterTableView.Items)
                        {
                            if (item["CrewID"].Text.Trim() == CrewID.Trim())
                            {
                                item.Selected = true;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Image Saving"
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = false;
                                table2.Visible = false;
                                dgCrewRoster.Visible = false;
                                
                                if (IsAdd)
                                {
                                    (dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }

                                if (isPassportPopup)
                                {
                                    //select index 4 for passporttab
                                    tabCrewRoster.SelectedIndex = 4;
                                    RadMultiPage1.SelectedIndex = 4;
                                }
                            }
                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                            else if (eventArgument == "DeleteImage_Click")
                            {
                                DeleteImage_Click(sender, e);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCrewRoster.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgCrewRoster.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

                if (Request.QueryString["CrewID"] != null)
                {
                    Session["SelectedCrewRosterID"] = Request.QueryString["CrewID"].Trim();
                }
            }
        }
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];

                        int count = DicImage.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            DicImage.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = DicImage;

                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                            {
                                dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            }
                            Session["DicImgDelete"] = dicImgDelete;

                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in DicImage)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }

                            if (ddlImg.Items.Count > 0)
                            {
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                if (Count > 0)
                                {
                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                            }
                            else
                            {
                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                lnkFileName.NavigateUrl = "";
                            }
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                System.IO.Stream myStream;
                Int32 fileLen;
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        string FileName = fileUL.FileName;
                        fileLen = fileUL.PostedFile.ContentLength;
                        Byte[] Input = new Byte[fileLen];

                        myStream = fileUL.FileContent;
                        myStream.Read(Input, 0, fileLen);

                        //Ramesh: Set the URL for image file or link based on the file type
                        SetURL(FileName, Input);

                        ////start of modification for image issue
                        //if (hdnBrowserName.Value == "Chrome")
                        //{
                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                        //}
                        //else
                        //{
                        //    Session["Base64"] = Input;
                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                        //}

                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                        int count = dicImg.Count(D => D.Key.Equals(FileName));
                        if (count > 0)
                        {
                            dicImg[FileName] = Input;
                        }
                        else
                        {
                            dicImg.Add(FileName, Input);
                            string encodedFileame = Microsoft.Security.Application.Encoder.HtmlEncode(FileName);
                            ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(encodedFileame, encodedFileame));

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);
                        }
                        tbImgName.Text = "";
                        if (ddlImg.Items.Count != 0)
                        {
                            //ddlImg.SelectedIndex = ddlImg.Items.Count - 1;
                            ddlImg.SelectedValue = FileName;
                        }
                        btndeleteImage.Enabled = true;
                    }
                }
            }
        }
        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // if(Session["SelectedCrewRosterID"] != null)
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool imgFound = false;
                            Session["Base64"] = null;   //added for image issue
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {

                                var ObjRetImg = ObjImgService.GetFileWarehouseList("Crew", Convert.ToInt64(Convert.ToString(Session["SelectedCrewRosterID"]))).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());


                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                {
                                    imgFound = true;
                                    byte[] picture = fwh.UWAFilePath;

                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(fwh.UWAFileName, picture);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = picture;
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                                if (!imgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (count > 0)
                                    {
                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }

                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                Session["DicImg"] = dicImg;

                Dictionary<string, string> dicImgDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = dicImgDelete;
            }
        }
        #endregion
        #region "Crew Code"Generation"

        /// <summary>
        /// Generate the Crew code 
        /// </summary>
        private string GetCrewCode(FlightPakMasterService.Company Company)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Company))
            {
                string CrewCD = "";
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (CompanySett.IsAutoCrew != true)
                    {
                        CrewCD = tbCrewCode.Text.Trim();
                    }
                    else
                    {
                        int Count = 1;
                        while (Count <= 3)
                        {
                            if (Company.CrewOFullName == Count)
                            {
                                if (tbFirstName.Text.Length > 0)
                                {
                                    if ((Company.CrewOLastFirst != null) && (Company.CrewOLastFirst.Value > 0))
                                    {
                                        if (tbFirstName.Text.Length >= Company.CrewOLastFirst.Value)
                                        {
                                            CrewCD = CrewCD + MiscUtils.GetRegexExtractAlphaNumeric(tbFirstName.Text, Convert.ToInt32(Company.CrewOLastFirst));
                                        }
                                        else
                                        {
                                            CrewCD = CrewCD + MiscUtils.GetRegexExtractAlphaNumeric(tbFirstName.Text, 1);
                                        }
                                    }
                                    else if ((Company.CrewOLastFirst.Value == 0) && (Company.CrewOLastMiddle.Value == 0) && (Company.CrewOLastLast.Value == 0))
                                    {
                                        CrewCD = CrewCD + MiscUtils.GetRegexExtractAlphaNumeric(tbFirstName.Text, 1);
                                    }
                                }
                            }
                            if (Company.CrewOMiddle == Count)
                            {
                                if (tbMiddleName.Text.Length > 0)
                                {
                                    if ((Company.CrewOLastMiddle != null) && (Company.CrewOLastMiddle.Value > 0))
                                    {
                                        if (tbMiddleName.Text.Length > Company.CrewOLastMiddle.Value)
                                        {
                                            CrewCD = CrewCD + MiscUtils.GetRegexExtractAlphaNumeric(tbMiddleName.Text, Convert.ToInt32(Company.CrewOLastMiddle));
                                        }
                                        else
                                        {
                                            CrewCD = CrewCD + MiscUtils.GetRegexExtractAlphaNumeric(tbMiddleName.Text, 1);
                                        }
                                    }
                                    else if ((Company.CrewOLastFirst.Value == 0) && (Company.CrewOLastMiddle.Value == 0) && (Company.CrewOLastLast.Value == 0))
                                    {
                                        CrewCD = CrewCD + MiscUtils.GetRegexExtractAlphaNumeric(tbMiddleName.Text, 1);
                                    }
                                }
                            }
                            if (Company.CrewOLast == Count)
                            {
                                if (tbLastName.Text.Length > 0)
                                {
                                    if ((Company.CrewOLastLast != null) && (Company.CrewOLastLast.Value > 0))
                                    {
                                        if (tbLastName.Text.Length >= Company.CrewOLastLast.Value)
                                        {
                                            CrewCD = CrewCD + MiscUtils.GetRegexExtractAlphaNumeric(tbLastName.Text, Convert.ToInt32(Company.CrewOLastLast));
                                        }
                                        else
                                        {
                                            CrewCD = CrewCD + MiscUtils.GetRegexExtractAlphaNumeric(tbLastName.Text, 1);
                                        }
                                    }
                                    else if ((Company.CrewOLastFirst.Value == 0) && (Company.CrewOLastMiddle.Value == 0) && (Company.CrewOLastLast.Value == 0))
                                    {
                                        CrewCD = CrewCD + MiscUtils.GetRegexExtractAlphaNumeric(tbLastName.Text, 1);
                                    }
                                }
                            }
                            Count++;
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
                return CrewCD;
            }
        }

        #endregion
        #region "Type Rating Time Conversion"
        protected string ConvertMinToTenths(String time, Boolean isTenths, String conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time, isTenths, conversionType))
            {
                string result = "0.0";

                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    if (timeArray[0].Trim() == "")
                    {
                        timeArray[0] = "0";
                    }
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    if (timeArray[1] == "  ")
                    {
                        timeArray[1] = "00";
                    }
                    decimal minute = Convert.ToDecimal(timeArray[1]);

                    if (isTenths && (conversionType == "1" || conversionType == "2")) // Standard and Flightpak Conversion
                    {
                        if (Session["StandardFlightpakConversion"] != null)
                        {
                            List<StandardFlightpakConversion> StandardFlightpakConversionList = (List<StandardFlightpakConversion>)Session["StandardFlightpakConversion"];
                            var standardList = StandardFlightpakConversionList.ToList().Where(x => minute >= x.StartMinutes && minute <= x.EndMinutes && x.ConversionType.ToString() == conversionType).ToList();

                            if (standardList.Count > 0)
                            {
                                result = Convert.ToString(hour + standardList[0].Tenths);
                            }
                        }
                    }
                    else
                    {
                        decimal decimalOfMin = 0;
                        if (minute > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = minute / 60;

                        result = Convert.ToString(hour + decimalOfMin);
                    }
                }


                return result;
            }
        }
        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "00:00";
                int val;
                if (time.IndexOf(".") != -1)
                {
                    string[] timeArray = time.Split('.');
                    string hour = timeArray[0];
                    string minute = timeArray[1].ToString();//Convert.ToDecimal(timeArray[1]);
                    decimal decimal_min = Convert.ToDecimal(String.Concat("0.", minute));
                    decimal decimalOfMin = 0;
                    if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                        decimalOfMin = (decimal_min * 60 / 100);
                    if (hour.Trim() == "")
                    {
                        hour = "0";
                    }
                    decimal finalval = Math.Round(decimalOfMin, 2);
                    string AlternativeValue = Convert.ToString(finalval);

                    if (AlternativeValue.IndexOf(".") != -1)
                    {
                        string[] timetempArray = AlternativeValue.Split('.');
                        AlternativeValue = timetempArray[1];
                    }
                    if ((AlternativeValue.Trim() == "") || (AlternativeValue.Trim() == "0"))
                    {
                        AlternativeValue = "00";
                    }
                    result = hour + ":" + AlternativeValue;
                }
                else if (int.TryParse(time, out val))
                {
                    result = time + ":00";
                }
                return result;
            }
        }
        private string AddPadding(string Value)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string result = "0.0";
                string[] strArr = new string[2];
                string ReturnValue = string.Empty;
                if (Value.Trim() != "")
                {
                    if (Value.Contains('.'))
                    {
                        strArr = Value.Split('.');
                        if (strArr[0].ToString().Length < 7 && strArr[0].ToString().Length > 0)
                        {
                            //if (strArr[0].ToString().Length == 0)
                            //{
                            //    strArr[0] = ("0000000" + strArr[0]);
                            //}
                            //if (strArr[0].ToString().Length == 1)
                            //{
                            //    strArr[0] = ("000000" + strArr[0]);
                            //}
                            //else if (strArr[0].ToString().Length == 2)
                            //{
                            //    strArr[0] = ("00000" + strArr[0]);
                            //}
                            //else if (strArr[0].ToString().Length == 3)
                            //{
                            //    strArr[0] = ("0000" + strArr[0]);
                            //}
                            //else if (strArr[0].ToString().Length == 4)
                            //{
                            //    strArr[0] = ("000" + strArr[0]);
                            //}
                            //else if (strArr[0].ToString().Length == 5)
                            //{
                            //    strArr[0] = ("00" + strArr[0]);
                            //}
                            //else if (strArr[0].ToString().Length == 6)
                            //{
                            //    strArr[0] = ("0" + strArr[0]);
                            //}
                        }
                        if (strArr[1].Length == 0)
                        {
                            strArr[1] = "0";
                        }
                        decimal check = Convert.ToInt64(strArr[0]);
                        result = Convert.ToString(check) + "." + strArr[1];
                        result = Math.Round(Convert.ToDecimal(result), 1).ToString();
                    }
                    else
                    {
                        if (Value.ToString().Length < 7 && Value.ToString().Length > 0)
                        {
                            //if (Value.ToString().Length == 0)
                            //{
                            //    Value = ("0000000" + Value);
                            //}
                            //if (Value.ToString().Length == 1)
                            //{
                            //    Value = ("000000" + Value);
                            //}
                            //else if (Value.ToString().Length == 2)
                            //{
                            //    Value = ("00000" + Value);
                            //}
                            //else if (Value.ToString().Length == 3)
                            //{
                            //    Value = ("0000" + Value);
                            //}
                            //else if (Value.ToString().Length == 4)
                            //{
                            //    Value = ("000" + Value);
                            //}
                            //else if (Value.ToString().Length == 5)
                            //{
                            //    Value = ("00" + Value);
                            //}
                            //else if (Value.ToString().Length == 6)
                            //{
                            //    Value = ("0" + Value);
                            //}
                        }
                        decimal check = Convert.ToInt64(Value);
                        result = Convert.ToString(check) + "." + "0";
                    }
                }
                return result;
            }
        }
        /// <summary>
        /// Method to Set Default Standard and Flightpak Conversion into List
        /// </summary>
        public List<StandardFlightpakConversion> LoadStandardFPConversion()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<StandardFlightpakConversion> conversionList = new List<StandardFlightpakConversion>();
                // Set Standard Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 5, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 6, EndMinutes = 11, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 12, EndMinutes = 17, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 18, EndMinutes = 23, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 24, EndMinutes = 29, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 30, EndMinutes = 35, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 36, EndMinutes = 41, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 42, EndMinutes = 47, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 48, EndMinutes = 53, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 54, EndMinutes = 59, ConversionType = 1 });

                // Set Flightpak Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 2, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 3, EndMinutes = 8, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 9, EndMinutes = 14, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 15, EndMinutes = 21, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 22, EndMinutes = 27, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 28, EndMinutes = 32, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 33, EndMinutes = 39, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 40, EndMinutes = 44, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 45, EndMinutes = 50, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 51, EndMinutes = 57, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 1.0M, StartMinutes = 58, EndMinutes = 59, ConversionType = 2 });

                return conversionList;
            }
        }
        #endregion
        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string parameter =
                            String.Format(
                                "CrewCD={0};CrewRosterReport={1};Crewchecklist={2};Inactivechecklist={3};CrewTypeRating={4};" +
                                "CrewCurrency={5};UserCD={6};UserCustomerID={7};UserHomebaseID={8};TenToMin={9};ReportHeaderID=10002175006;TempSettings=FUELDTLS::1||FUELVEND::1",
                                tbCrewCode.Text, hdnCrewRosterRpt.Value, hdnCrewChecklistRpt.Value, hdnInactiveChecklistRpt.Value, hdnCrewTypeRatingsRpt.Value, 
                                hdnCrewCurrencyRpt.Value, UserPrincipal.Identity._name, UserPrincipal.Identity._customerID, 
                                UserPrincipal.Identity._homeBaseId, UserPrincipal.Identity._fpSettings._TimeDisplayTenMin);
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = parameter;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion
        #region "Save Grids Data in Database"

        protected void LastName_TextChanged(object sender, EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient Service1 = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                List<GetAllCrewForGrid> FilterCrew = new List<GetAllCrewForGrid>();
                List<GetCrewGroupbasedCrew> FilterGroupCrew = new List<GetCrewGroupbasedCrew>();
                FilterCrew = (List<GetAllCrewForGrid>)Session["CrewList"];

                if (tbLastName.Text.Trim() != string.Empty)
                {
                    FilterCrew = FilterCrew.Where(x => (x.LastName != null)).ToList();
                    FilterCrew = FilterCrew.Where(x => (x.LastName.Trim().ToUpper().Equals(tbLastName.Text.ToString().ToUpper().Trim()))).ToList();

                    if (FilterCrew != null && FilterCrew.Count > 0)
                    {
                        hdnWarning.Value = "1";
                        lbWarningMessage.Visible = true;
                    }
                    else
                    {
                        hdnWarning.Value = "0";
                        lbWarningMessage.Visible = false;
                    }
                }
                else
                {
                    hdnWarning.Value = "1";
                    lbWarningMessage.Visible = false;
                }
            }
        }

        /// <summary>
        /// Method to fetch the Crew Main Info Items
        /// </summary>
        /// <returns>Returns Crew Object</returns>
        private FlightPakMasterService.Crew GetCrewMainInfo(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Main Info Fields
                    if (hdnSave.Value == "Save")
                    {
                        oCrew.CrewCD = GetCrewCode(CompanySett);
                    }
                    //Crew.CrewCD = tbCrewCode.Text;
                    if (hdnSave.Value == "Update")
                    {
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            oCrew.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                        }
                        oCrew.CrewCD = ((GridDataItem)dgCrewRoster.SelectedItems[0]).GetDataKeyValue("CrewCD").ToString().Trim();
                        oCrew.CustomerID = Convert.ToInt64(((GridDataItem)dgCrewRoster.SelectedItems[0]).GetDataKeyValue("CustomerID").ToString());
                    }
                    if (!string.IsNullOrEmpty(hdnHomeBaseID.Value.ToString()))
                    {
                        oCrew.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                    }

                    if (!string.IsNullOrEmpty(hdnClientID.Value.ToString()))
                    {
                        oCrew.ClientID = Convert.ToInt64(hdnClientID.Value);
                    }
                    oCrew.LastName = tbLastName.Text;
                    oCrew.FirstName = tbFirstName.Text;
                    oCrew.MiddleInitial = tbMiddleName.Text;
                    oCrew.Addr1 = tbAddress1.Text;
                    oCrew.Addr2 = tbAddress2.Text;
                    oCrew.CityName = tbCity.Text;
                    oCrew.StateName = tbStateProvince.Text;
                    oCrew.PostalZipCD = tbStateProvincePostal.Text;
                    if (!string.IsNullOrEmpty(tbCountry.Text.Trim()))
                    {
                        oCrew.CountryID = Convert.ToInt64(hdnCountryID.Value);
                    }
                    else
                    {
                        oCrew.CountryID = null;
                    }

                    oCrew.CheckList = "1";
                    foreach (GridDataItem Item in dgCrewCheckList.Items)
                    {
                        if (Item.BackColor == System.Drawing.Color.Red)
                        {
                            oCrew.CheckList = "0";
                        }
                    }
                    if (rbMale.Checked == true)
                    {
                        oCrew.Gender = "M";
                    }
                    else// if (rbFemale.Checked == true)
                    {
                        oCrew.Gender = "F";
                    }
                    oCrew.IsStatus = chkActiveCrew.Checked;
                    oCrew.IsFixedWing = chkFixedWing.Checked;
                    oCrew.IsRotaryWing = chkRoteryWing.Checked;

                    // TextBox tbDateOfBirth = (TextBox)ucDateOfBirth.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbDateOfBirth.Text.Trim().ToString()))
                    {
                        oCrew.BirthDT = Convert.ToDateTime(FormatDate(tbDateOfBirth.Text, DateFormat));
                    }
                    oCrew.IsNoCalendarDisplay = chkNoCalenderDisplay.Checked;
                    oCrew.SSN = tbSSN.Text.Trim();
                    oCrew.CityOfBirth = tbCityBirth.Text;
                    oCrew.StateofBirth = tbStateBirth.Text;

                    if (!string.IsNullOrEmpty(tbCountryBirth.Text.Trim()))
                    {
                        oCrew.CountryOfBirth = Convert.ToInt64(hdnResidenceCountryID.Value);
                    }
                    else
                    {
                        oCrew.CountryOfBirth = null;
                    }
                    oCrew.PhoneNum = tbHomePhone.Text;
                    oCrew.CellPhoneNum = tbMobilePhone.Text;
                    oCrew.PagerNum = tbPager.Text;
                    oCrew.FaxNum = tbFax.Text;
                    oCrew.EmailAddress = tbEmail.Text;
                    // TextBox tbHireDT = (TextBox)ucHireDT.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbHireDT.Text.Trim().ToString()))
                    {
                        //oCrew.HireDT = DateTime.ParseExact(tbHireDT.Text, DateFormat, CultureInfo.InvariantCulture);

                        oCrew.HireDT = Convert.ToDateTime(FormatDate(tbHireDT.Text, DateFormat));
                    }
                    // TextBox tbTermDT = (TextBox)ucTermDT.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbTermDT.Text.Trim().ToString()))
                    {
                        //oCrew.TerminationDT = DateTime.ParseExact(tbTermDT.Text, DateFormat, CultureInfo.InvariantCulture);

                        oCrew.TerminationDT = Convert.ToDateTime(FormatDate(tbTermDT.Text, DateFormat));
                    }

                    if (!string.IsNullOrEmpty(tbCitizenCode.Text.Trim()))
                    {
                        oCrew.Citizenship = Convert.ToInt64(hdnCitizenshipID.Value);
                    }
                    else
                    {
                        oCrew.Citizenship = null;
                    }
                    oCrew.PilotLicense1 = tbLicNo.Text;
                    if (!string.IsNullOrEmpty(tbLicExpDate.Text.Trim().ToString()))
                    {
                        oCrew.License1ExpiryDT = Convert.ToDateTime(FormatDate(tbLicExpDate.Text, DateFormat));
                    }

                    oCrew.License1CityCountry = tbLicCity.Text;
                    oCrew.LicenseCountry1 = hdnLICCountryCode.Value;
                    oCrew.LicenseType1 = tbLicType.Text;
                    oCrew.PilotLicense2 = tbAddLic.Text;
                    if (!string.IsNullOrEmpty(tbAddLicExpDate.Text.Trim().ToString()))
                    {
                        oCrew.License2ExpiryDT = Convert.ToDateTime(FormatDate(tbAddLicExpDate.Text, DateFormat));
                    }
                    oCrew.License2CityCountry = tbAddLicCity.Text;
                    oCrew.LicenseCountry2 = hdnADDLICCountryCode.Value;
                    oCrew.LicenseType2 = tbAddLicType.Text;
                    //Commented to fix Bug# 5927
                    //oCrew.CrewTypeCD = tbCrewType.Text;
                    oCrew.CrewTypeDescription = tbCrewType.Text;

                    if (!string.IsNullOrEmpty(hdnDepartmentID.Value.ToString()))
                    {
                        oCrew.DepartmentID = Convert.ToInt64(hdnDepartmentID.Value);   // hidden value ll come here
                    }
                    oCrew.Notes = tbNotes.Text;
                    oCrew.Notes2 = tbAdditionalNotes.Text;
                    oCrew.INSANumber = tbINSANumber.Text;
                    oCrew.Category = tbCategory.Text;

                    if (!string.IsNullOrEmpty(hdnGreenNationality.Value.ToString()))
                    {
                        oCrew.GreenCardCountryID = Convert.ToInt64(hdnGreenNationality.Value);
                    }
                    if (!string.IsNullOrEmpty(tbResidentSinceYear.Text.ToString()))
                    {
                        oCrew.ResidentSinceYear = Convert.ToInt32(tbResidentSinceYear.Text);
                    }
                    if (!string.IsNullOrEmpty(tbCardExpires.Text.ToString()))
                    {
                        oCrew.CardExpires = Convert.ToDateTime(FormatDate(tbCardExpires.Text, DateFormat));
                    }

                    //Email Preferences Fields
                    oCrew.IsDepartmentAuthorization = chkDepartmentAuthorization.Checked;
                    oCrew.IsRequestPhoneNum = chkRequestorPhone.Checked;
                    oCrew.IsAccountNum = chkAccountNo.Checked;
                    oCrew.IsCancelDescription = chkCancellationDesc.Checked;
                    oCrew.IsStatusT = chkStatus.Checked;
                    oCrew.IsAirport = chkAirport.Checked;
                    oCrew.IsCheckList = chkChecklist.Checked;
                    oCrew.IsRunway = chkRunway.Checked;
                    oCrew.IsHomeArrivalTM = chkHomeArrival.Checked;
                    oCrew.IsHomeDEPARTTM = chkHomeDeparture.Checked;
                    if (radLocal.Checked == true)
                    {
                        oCrew.BlackBerryTM = "L";
                    }
                    else
                    {
                        oCrew.BlackBerryTM = "Z";
                    }
                    oCrew.IsEndDuty = chkEndDuty.Checked;
                    oCrew.IsOverride = chkOverride.Checked;
                    oCrew.IsCrewRules = chkCrewRules.Checked;
                    oCrew.IsFAR = chkFAR.Checked;
                    oCrew.IsDuty = chkDutyHours.Checked;
                    oCrew.IsFlightHours = chkFlightHours.Checked;
                    oCrew.IsRestHrs = chkRestHours.Checked;
                    oCrew.IsAssociatedCrew = chkAssociatedCrew.Checked;
                    oCrew.Label1 = chkCOFFEE.Checked;
                    oCrew.Label2 = chkNEWSPAPER.Checked;
                    oCrew.Label3 = chkJUICE.Checked;
                    oCrew.IsDepartureFBO = chkDepartureInformation.Checked;
                    oCrew.IsArrivalFBO = chkArrivalInformation.Checked;
                    oCrew.IsCrewDepartTRANS = chkCrewTransport.Checked;
                    oCrew.IsCrewArrivalTRANS = chkCrewArrival.Checked;
                    oCrew.IsHotel = chkHotel.Checked;
                    oCrew.IsPassDepartTRANS = chkPassengerTransport.Checked;
                    oCrew.IsPassArrivalHotel = chkPassengerArrival.Checked;
                    oCrew.IsPassDetails = chkPassengerDetails.Checked;
                    oCrew.IsPassHotel = chkPassengerHotel.Checked;
                    oCrew.IsPassPhoneNum = chkPassengerPaxPhone.Checked;
                    oCrew.IsDepartCatering = chkDepartureCatering.Checked;
                    oCrew.IsArrivalCatering = chkArrivalCatering.Checked;
                    oCrew.IsArrivalDepartTime = chkArrivalDepartureTime.Checked;
                    oCrew.IsAircraft = chkAircraft.Checked;
                    oCrew.IcaoID = chkArrivalDepartureICAO.Checked;
                    oCrew.IsCrew = chkGeneralCrew.Checked;
                    oCrew.IsPassenger = chkGeneralPassenger.Checked;
                    oCrew.IsOutboundINST = chkOutBoundInstructions.Checked;
                    oCrew.IsCheckList1 = chkGeneralChecklist.Checked;
                    oCrew.IsFBO = chkLogisticsFBO.Checked;
                    oCrew.IsHotel = chkLogisticsHotel.Checked;
                    oCrew.IsTransportation = chkLogisticsTransportation.Checked;
                    oCrew.IsCatering = chkLogisticsCatering.Checked;

                    oCrew.IsCrewHotelConfirm = chkCrewHotelConfirm.Checked;
                    oCrew.IsCrewHotelComments = chkCrewHotelComments.Checked;
                    oCrew.IsCrewDepartTRANSConfirm = chkCrewDeptTransConfirm.Checked;
                    oCrew.IsCrewDepartTRANSComments = chkCrewDeptTransComments.Checked;
                    oCrew.IsCrewArrivalConfirm = chkCrewArrTransConfirm.Checked;
                    oCrew.IsCrewArrivalComments = chkCrewArrTransComments.Checked;
                    oCrew.IsCrewNotes = chkCrewNotes.Checked;
                    oCrew.IsPassHotelConfirm = chkPAXHotelConfirm.Checked;
                    oCrew.IsPassHotelComments = chkPAXHotelComments.Checked;
                    oCrew.IsPassDepartTRANSConfirm = chkPAXDeptTransConfirm.Checked;
                    oCrew.IsPassDepartTRANSComments = chkPAXDeptTransComments.Checked;
                    oCrew.IsPassArrivalConfirm = chkPAXArrTransConfirm.Checked;
                    oCrew.IsPassArrivalComments = chkPAXArrTransComments.Checked;
                    oCrew.IsPassNotes = chkPAXNotes.Checked;
                    oCrew.IsOutboundComments = chkOutboundInstComments.Checked;
                    oCrew.IsDepartAirportNotes = chkDeptAirportNotes.Checked;
                    oCrew.IsArrivalAirportNoes = chkArrAirportNotes.Checked;
                    oCrew.IsDepartAirportAlerts = chkDeptAirportAlerts.Checked;
                    oCrew.IsArrivalAirportAlerts = chkArrAirportAlerts.Checked;
                    oCrew.IsFBOArrivalConfirm = chkArrFBOConfirm.Checked;
                    oCrew.IsFBOArrivalComment = chkArrFBOComments.Checked;
                    oCrew.IsFBODepartConfirm = chkDeptFBOConfirm.Checked;
                    oCrew.IsFBODepartComment = chkDeptFBOComments.Checked;
                    oCrew.IsDepartCateringConfirm = chkDeptCaterConfirm.Checked;
                    oCrew.IsDepartCateringComment = chkDeptCaterComments.Checked;
                    oCrew.IsArrivalCateringConfirm = chkArrCaterConfirm.Checked;
                    oCrew.IsArrivalCateringComment = chkArrCaterComments.Checked;
                    oCrew.IsTripAlerts = chkTripAlerts.Checked;
                    oCrew.IsTripNotes = chkTripNotes.Checked;
                    oCrew.IsDeleted = false;
                    // oCrew.LastUpdUID = "UC";
                    oCrew.LastUpdTS = System.DateTime.UtcNow;


                    if (!string.IsNullOrEmpty(tbAddress3.Text))
                    {
                        oCrew.Addr3 = tbAddress3.Text;
                    }
                    if (!string.IsNullOrEmpty(tbOtherEmail.Text))
                    {
                        oCrew.OtherEmail = tbOtherEmail.Text;
                    }
                    if (!string.IsNullOrEmpty(tbPersonalEmail.Text))
                    {
                        oCrew.PersonalEmail = tbPersonalEmail.Text;
                    }
                    if (!string.IsNullOrEmpty(tbOtherPhone.Text))
                    {
                        oCrew.OtherPhone = tbOtherPhone.Text;
                    }
                    if (!string.IsNullOrEmpty(tbSecondaryMobile.Text))
                    {
                        oCrew.CellPhoneNum2 = tbSecondaryMobile.Text;  // Secondary Mobile / Cell
                    }
                    if (!string.IsNullOrEmpty(tbBusinessFax.Text))
                    {
                        oCrew.BusinessFax = tbBusinessFax.Text;
                    }
                    if (!string.IsNullOrEmpty(tbBusinessPhone.Text))
                    {
                        oCrew.BusinessPhone = tbBusinessPhone.Text;
                    }

                    if (!string.IsNullOrEmpty(tbBirthday.Text.Trim().ToString()))
                    {
                        oCrew.Birthday = Convert.ToDateTime(FormatDate(tbBirthday.Text, DateFormat));
                    }
                    if (!string.IsNullOrEmpty(tbAnnivesaries.Text.Trim().ToString()))
                    {
                        oCrew.Anniversaries = Convert.ToDateTime(FormatDate(tbAnnivesaries.Text, DateFormat));
                    }
                    if (!string.IsNullOrEmpty(tbEmergencyContact.Text))
                    {
                        oCrew.EmergencyContacts = tbEmergencyContact.Text;
                    }
                    if (!string.IsNullOrEmpty(tbCreditCardInfo.Text))
                    {
                        oCrew.CreditcardInfo = tbCreditCardInfo.Text;
                    }
                    if (!string.IsNullOrEmpty(tbCateringPreferences.Text))
                    {
                        oCrew.CateringPreferences = tbCateringPreferences.Text;
                    }
                    if (!string.IsNullOrEmpty(tbHotelPreferences.Text))
                    {
                        oCrew.HotelPreferences = tbHotelPreferences.Text;
                    }

                    if (!string.IsNullOrEmpty(tbBirthday.Text.Trim().ToString()))
                    {
                        oCrew.Birthday = Convert.ToDateTime(FormatDate(tbBirthday.Text, DateFormat));
                    }
                    if (!string.IsNullOrEmpty(tbAnnivesaries.Text.Trim().ToString()))
                    {
                        oCrew.Anniversaries = Convert.ToDateTime(FormatDate(tbAnnivesaries.Text, DateFormat));
                    }
                    if (!string.IsNullOrEmpty(tbEmergencyContact.Text))
                    {
                        oCrew.EmergencyContacts = tbEmergencyContact.Text;
                    }
                    if (!string.IsNullOrEmpty(tbCreditCardInfo.Text))
                    {
                        oCrew.CreditcardInfo = tbCreditCardInfo.Text;
                    }
                    if (!string.IsNullOrEmpty(tbCateringPreferences.Text))
                    {
                        oCrew.CateringPreferences = tbCateringPreferences.Text;
                    }
                    if (!string.IsNullOrEmpty(tbHotelPreferences.Text))
                    {
                        oCrew.HotelPreferences = tbHotelPreferences.Text;
                    }
                    //<new fields>

                    //New Field Added
                    oCrew.IsTripEmail = chkIsTripEmail.Checked;

                }, FlightPak.Common.Constants.Policy.UILayer);
                return oCrew;
            }
        }
        /// <summary>
        /// Method to Save Crew Definiton Informations
        /// </summary>
        /// <param name="CrewID">Pass Crew Code</param>
        private FlightPakMasterService.Crew SaveCrewDefinition(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    oCrew.CrewDefinition = new List<CrewDefinition>();
                    Int64 Indentity = 0;
                    List<FlightPakMasterService.GetCrewDefinition> CrewDefinitionInfoList = new List<FlightPakMasterService.GetCrewDefinition>();
                    FlightPakMasterService.CrewDefinition CrewDefinition = new FlightPakMasterService.CrewDefinition();
                    CrewDefinitionInfoList = (List<FlightPakMasterService.GetCrewDefinition>)Session["CrewAddInfo"];
                    bool IsExist = false;
                    for (int Index = 0; Index < CrewDefinitionInfoList.Count; Index++)
                    {
                        IsExist = false;
                        CrewDefinition = new CrewDefinition();
                        foreach (GridDataItem Item in dgCrewAddlInfo.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("CrewInfoID").ToString() == CrewDefinitionInfoList[Index].CrewInfoID.ToString())
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("CrewInfoXRefID").ToString()) != 0)
                                {
                                    CrewDefinition.CrewInfoXRefID = Convert.ToInt64(Item.GetDataKeyValue("CrewInfoXRefID").ToString());
                                }
                                else
                                {
                                    Indentity = Indentity - 1;
                                    CrewDefinition.CrewInfoXRefID = Indentity;
                                }
                                CrewDefinition.CrewID = oCrew.CrewID;// CrewDefinitionInfoList[Index].CrewID;
                                CrewDefinition.CrewInfoID = CrewDefinitionInfoList[Index].CrewInfoID;
                                CrewDefinition.CustomerID = oCrew.CustomerID;
                                CrewDefinition.InformationDESC = CrewDefinitionInfoList[Index].InformationDESC;
                                if (((TextBox)Item["AddlInfo"].FindControl("tbAddlInfo")) != null && (!string.IsNullOrEmpty(((TextBox)Item["AddlInfo"].FindControl("tbAddlInfo")).Text)))
                                {
                                    CrewDefinition.InformationValue = ((TextBox)Item["AddlInfo"].FindControl("tbAddlInfo")).Text;//CrewDefinitionInfoList[Index].InformationValue;
                                }
                                else
                                {
                                    CrewDefinition.InformationValue = null;
                                }
                                if (CrewDefinitionInfoList[Index].IsDeleted.HasValue)
                                {
                                    CrewDefinition.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted.Value;
                                }
                                if (((CheckBox)Item["IsReptFilter"].FindControl("chkIsReptFilter")) != null)
                                {
                                    CrewDefinition.IsReptFilter = ((CheckBox)Item["IsReptFilter"].FindControl("chkIsReptFilter")).Checked;
                                }
                                else
                                {
                                    CrewDefinition.IsReptFilter = false;
                                }
                                CrewDefinition.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                                CrewDefinition.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                                CrewDefinition.IsDeleted = false;
                                IsExist = true;
                                break;
                            }
                        }
                        //IsExist
                        if (IsExist == false)
                        {
                            if (CrewDefinitionInfoList[Index].CrewInfoXRefID != 0)
                            {
                                CrewDefinition.CrewInfoXRefID = CrewDefinitionInfoList[Index].CrewInfoXRefID;
                            }
                            else
                            {
                                Indentity = Indentity - 1;
                                CrewDefinition.CrewInfoID = Indentity;
                            }
                            CrewDefinition.CrewID = CrewDefinitionInfoList[Index].CrewID;
                            CrewDefinition.CrewInfoID = CrewDefinitionInfoList[Index].CrewInfoID;
                            CrewDefinition.CrewInfoXRefID = CrewDefinitionInfoList[Index].CrewInfoXRefID;
                            CrewDefinition.CustomerID = CrewDefinitionInfoList[Index].CustomerID;
                            CrewDefinition.InformationDESC = CrewDefinitionInfoList[Index].InformationDESC;
                            CrewDefinition.InformationValue = CrewDefinitionInfoList[Index].InformationValue;
                            if (CrewDefinitionInfoList[Index].IsDeleted.HasValue)
                            {
                                CrewDefinition.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted.Value;
                            }
                            CrewDefinition.IsReptFilter = CrewDefinitionInfoList[Index].IsReptFilter;
                            CrewDefinition.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                            CrewDefinition.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                        }
                        if (CrewDefinition.CrewInfoXRefID == 0)
                        {
                            Indentity = Indentity - 1;
                            CrewDefinition.CrewInfoXRefID = Indentity;
                        }
                        oCrew.CrewDefinition.Add(CrewDefinition);
                    }
                    //}
                    return oCrew;

                }, FlightPak.Common.Constants.Policy.UILayer);
                return oCrew;
            }
        }

        /// <summary>
        /// Method to Save Crew Visa Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>

        //private FlightPakMasterService.Crew SaveCrewVisa(FlightPakMasterService.Crew oCrew)
        private FlightPakMasterService.Crew SaveCrewVisa(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Crew>(() =>
                {
                    // added information items from the Grid to List
                    oCrew.CrewPassengerVisa = new List<FlightPakMasterService.CrewPassengerVisa>();
                    FlightPakMasterService.CrewPassengerVisa CrewPaxVisaInfoListDef = new FlightPakMasterService.CrewPassengerVisa();
                    List<FlightPakMasterService.GetAllCrewPaxVisa> CrewPaxVisaInfoList = new List<FlightPakMasterService.GetAllCrewPaxVisa>();
                    CrewPaxVisaInfoList = (List<GetAllCrewPaxVisa>)Session["CrewVisa"];
                    List<FlightPakMasterService.CrewPassengerVisa> CrewPaxVisaGridInfo = new List<FlightPakMasterService.CrewPassengerVisa>();
                    Int64 Indentity = 0;
                    bool IsExist = false;
                    foreach (GridDataItem Item in dgVisa.MasterTableView.Items)
                    {
                        CrewPaxVisaInfoListDef = new CrewPassengerVisa();
                        if (Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString()) != 0)
                        {
                            CrewPaxVisaInfoListDef.VisaID = Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString());
                        }
                        else
                        {
                            Indentity = Indentity - 1;
                            CrewPaxVisaInfoListDef.VisaID = Indentity;
                        }
                        CrewPaxVisaInfoListDef.CustomerID = oCrew.CustomerID;
                        CrewPaxVisaInfoListDef.CrewID = oCrew.CrewID;// Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                        CrewPaxVisaInfoListDef.PassengerRequestorID = null;
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value);
                        }
                        CrewPaxVisaInfoListDef.VisaTYPE = string.Empty;
                        if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text != null)
                        {
                            CrewPaxVisaInfoListDef.VisaNum = ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text;
                        }
                        else
                        {
                            CrewPaxVisaInfoListDef.VisaNum = string.Empty;
                        }
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaInfoListDef.ExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxVisaInfoListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.IssuePlace = ((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text;
                        }
                        if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaInfoListDef.IssueDate = Convert.ToDateTime(FormatDate(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxVisaInfoListDef.IssueDate = Convert.ToDateTime(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text);
                            }
                        }
                        if (((TextBox)Item["EntriesAllowedInPassport"].FindControl("tbEntriesAllowedInPassport")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.EntriesAllowedInPassport = Convert.ToInt32(((TextBox)Item["EntriesAllowedInPassport"].FindControl("tbEntriesAllowedInPassport")).Text);
                        }
                        if (((TextBox)Item["VisaExpireInDays"].FindControl("tbVisaExpireInDays")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.VisaExpireInDays = Convert.ToInt32(((TextBox)Item["VisaExpireInDays"].FindControl("tbVisaExpireInDays")).Text);
                        }
                        if (((DropDownList)Item["TypeOfVisa"].FindControl("ddlTypeOfVisa")).SelectedValue != "Select")
                        {
                            CrewPaxVisaInfoListDef.TypeOfVisa = ((DropDownList)Item["TypeOfVisa"].FindControl("ddlTypeOfVisa")).SelectedValue.ToString();
                        }
                        else
                        {
                            CrewPaxVisaInfoListDef.TypeOfVisa = "";
                        }
                        if (((TextBox)Item["Notes"].FindControl("tbNotes")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.Notes = ((TextBox)Item["Notes"].FindControl("tbNotes")).Text;
                        }
                        CrewPaxVisaInfoListDef.IsDeleted = false;
                        CrewPaxVisaGridInfo.Add(CrewPaxVisaInfoListDef);
                    }

                    if (CrewPaxVisaInfoList != null)    //added for testing Delete & add scenario
                    {
                        for (int sIndex = 0; sIndex < CrewPaxVisaInfoList.Count; sIndex++)
                        {
                            IsExist = false;
                            for (int gIndex = 0; gIndex < CrewPaxVisaGridInfo.Count; gIndex++)
                            {
                                if ((CrewPaxVisaInfoList[sIndex].VisaNum == CrewPaxVisaGridInfo[gIndex].VisaNum) && (CrewPaxVisaInfoList[sIndex].VisaID == CrewPaxVisaGridInfo[gIndex].VisaID))
                                {
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false)
                            {
                                if (CrewPaxVisaInfoList[sIndex].VisaNum != null)
                                {
                                    if (CrewPaxVisaInfoList[sIndex].VisaID != 0)
                                    {
                                        CrewPaxVisaInfoListDef = new CrewPassengerVisa();
                                        CrewPaxVisaInfoListDef.CountryID = CrewPaxVisaInfoList[sIndex].CountryID;
                                        CrewPaxVisaInfoListDef.CrewID = CrewPaxVisaInfoList[sIndex].CrewID;
                                        CrewPaxVisaInfoListDef.CustomerID = CrewPaxVisaInfoList[sIndex].CustomerID;
                                        CrewPaxVisaInfoListDef.ExpiryDT = CrewPaxVisaInfoList[sIndex].ExpiryDT;
                                        CrewPaxVisaInfoListDef.IsDeleted = true;// CrewPaxVisaInfoList[sIndex].IsDeleted;
                                        CrewPaxVisaInfoListDef.IssueDate = CrewPaxVisaInfoList[sIndex].IssueDate;
                                        CrewPaxVisaInfoListDef.IssuePlace = CrewPaxVisaInfoList[sIndex].IssuePlace;
                                        CrewPaxVisaInfoListDef.LastUpdTS = CrewPaxVisaInfoList[sIndex].LastUpdTS;
                                        CrewPaxVisaInfoListDef.LastUpdUID = CrewPaxVisaInfoList[sIndex].LastUpdUID;
                                        CrewPaxVisaInfoListDef.Notes = CrewPaxVisaInfoList[sIndex].Notes;
                                        CrewPaxVisaInfoListDef.PassengerRequestorID = CrewPaxVisaInfoList[sIndex].PassengerRequestorID;
                                        if (CrewPaxVisaInfoList[sIndex].VisaID != 0)
                                        {
                                            CrewPaxVisaInfoListDef.VisaID = CrewPaxVisaInfoList[sIndex].VisaID;
                                        }
                                        else
                                        {
                                            Indentity = Indentity - 1;
                                            CrewPaxVisaInfoListDef.VisaID = Indentity;
                                        }
                                        CrewPaxVisaInfoListDef.VisaNum = CrewPaxVisaInfoList[sIndex].VisaNum;
                                        CrewPaxVisaInfoListDef.VisaTYPE = CrewPaxVisaInfoList[sIndex].VisaTYPE;
                                        CrewPaxVisaInfoListDef.EntriesAllowedInPassport = CrewPaxVisaInfoList[sIndex].EntriesAllowedInPassport;
                                        CrewPaxVisaInfoListDef.TypeOfVisa = CrewPaxVisaInfoList[sIndex].TypeOfVisa;
                                        CrewPaxVisaInfoListDef.VisaExpireInDays = CrewPaxVisaInfoList[sIndex].VisaExpireInDays;
                                        CrewPaxVisaGridInfo.Add(CrewPaxVisaInfoListDef);
                                    }
                                }
                            }
                        }
                    }
                    oCrew.CrewPassengerVisa = CrewPaxVisaGridInfo;
                    return oCrew;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Save Crew Passport Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>

        private FlightPakMasterService.Crew SaveCrewPassport(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Crew>(() =>
                {
                    // added information items from the Grid to List
                    oCrew.CrewPassengerPassport = new List<FlightPakMasterService.CrewPassengerPassport>();
                    FlightPakMasterService.CrewPassengerPassport CrewPaxPassportInfoListDef = new FlightPakMasterService.CrewPassengerPassport();
                    List<FlightPakMasterService.GetAllCrewPassport> CrewPaxPassportInfoList = new List<FlightPakMasterService.GetAllCrewPassport>();
                    CrewPaxPassportInfoList = (List<GetAllCrewPassport>)Session["CrewPassport"];
                    List<FlightPakMasterService.CrewPassengerPassport> CrewPaxPassportGridInfo = new List<FlightPakMasterService.CrewPassengerPassport>();
                    Int64 Indentity = 0;
                    bool IsExist = false;
                    foreach (GridDataItem Item in dgPassport.MasterTableView.Items)
                    {
                        CrewPaxPassportInfoListDef = new CrewPassengerPassport();
                        if (Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString()) >= 1000)
                        {
                            CrewPaxPassportInfoListDef.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                        }
                        else
                        {
                            Indentity = Indentity - 1;
                            CrewPaxPassportInfoListDef.PassportID = Indentity;
                        }
                        CrewPaxPassportInfoListDef.PassengerRequestorID = null;
                        CrewPaxPassportInfoListDef.CrewID = oCrew.CrewID;
                        CrewPaxPassportInfoListDef.CustomerID = oCrew.CustomerID;
                        if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                        }
                        if (dgPassport.MasterTableView.Items.Count == 1 && hdnIsPassportChoice.Value == "Yes")
                        {
                            CrewPaxPassportInfoListDef.Choice = true;
                        }
                        else
                        {
                            CrewPaxPassportInfoListDef.Choice = ((CheckBox)Item["Choice"].FindControl("chkChoice")).Checked;
                        }

                        if (((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxPassportInfoListDef.PassportExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxPassportInfoListDef.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                            }
                        }

                        if (tbLicNo.Text.Trim() != "")
                        {
                            CrewPaxPassportInfoListDef.PilotLicenseNum = tbLicNo.Text;
                        }
                        if (((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                        }
                        if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxPassportInfoListDef.IssueDT = Convert.ToDateTime(FormatDate(((TextBox)Item["PassportExpiryDT"].FindControl("tbIssueDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxPassportInfoListDef.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                            }
                        }
                        if (((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.CountryID = Convert.ToInt64(((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value);
                        }
                        CrewPaxPassportInfoListDef.IsDeleted = false;
                        CrewPaxPassportGridInfo.Add(CrewPaxPassportInfoListDef);
                    }
                    if (CrewPaxPassportInfoList != null)    //added for testing Delete & add scenario
                    {
                        for (int sIndex = 0; sIndex < CrewPaxPassportInfoList.Count; sIndex++)
                        {
                            IsExist = false;
                            for (int gIndex = 0; gIndex < CrewPaxPassportGridInfo.Count; gIndex++)
                            {
                                if ((CrewPaxPassportInfoList[sIndex].PassportID == CrewPaxPassportGridInfo[gIndex].PassportID) && (CrewPaxPassportInfoList[sIndex].PassportID == CrewPaxPassportGridInfo[gIndex].PassportID))
                                {
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false)
                            {
                                if (CrewPaxPassportInfoList[sIndex].PassportID != null)
                                {
                                    if (CrewPaxPassportInfoList[sIndex].PassportID >= 0)
                                    {
                                        CrewPaxPassportInfoListDef = new CrewPassengerPassport();
                                        CrewPaxPassportInfoListDef.CountryID = CrewPaxPassportInfoList[sIndex].CountryID;
                                        CrewPaxPassportInfoListDef.Choice = CrewPaxPassportInfoList[sIndex].Choice;
                                        CrewPaxPassportInfoListDef.CountryID = CrewPaxPassportInfoList[sIndex].CountryID;
                                        CrewPaxPassportInfoListDef.CrewID = CrewPaxPassportInfoList[sIndex].CrewID;
                                        CrewPaxPassportInfoListDef.CustomerID = CrewPaxPassportInfoList[sIndex].CustomerID;
                                        CrewPaxPassportInfoListDef.IsDefaultPassport = CrewPaxPassportInfoList[sIndex].IsDefaultPassport;
                                        if (CrewPaxPassportInfoList[sIndex].IsDeleted.HasValue)
                                        {
                                            CrewPaxPassportInfoListDef.IsDeleted = CrewPaxPassportInfoList[sIndex].IsDeleted.Value;
                                        }
                                        CrewPaxPassportInfoListDef.IssueCity = CrewPaxPassportInfoList[sIndex].IssueCity;
                                        CrewPaxPassportInfoListDef.IssueDT = CrewPaxPassportInfoList[sIndex].IssueDT;
                                        CrewPaxPassportInfoListDef.LastUpdTS = CrewPaxPassportInfoList[sIndex].LastUpdTS;
                                        CrewPaxPassportInfoListDef.LastUpdUID = CrewPaxPassportInfoList[sIndex].LastUpdUID;
                                        CrewPaxPassportInfoListDef.PassengerRequestorID = CrewPaxPassportInfoList[sIndex].PassengerRequestorID;
                                        CrewPaxPassportInfoListDef.PassportExpiryDT = CrewPaxPassportInfoList[sIndex].PassportExpiryDT;
                                        CrewPaxPassportInfoListDef.PassportNum = CrewPaxPassportInfoList[sIndex].PassportNum;
                                        CrewPaxPassportInfoListDef.PilotLicenseNum = CrewPaxPassportInfoList[sIndex].PilotLicenseNum;
                                        if (CrewPaxPassportInfoList[sIndex].PassportID >= 0)
                                        {
                                            CrewPaxPassportInfoListDef.PassportID = CrewPaxPassportInfoList[sIndex].PassportID;
                                        }
                                        else
                                        {
                                            Indentity = Indentity - 1;
                                            CrewPaxPassportInfoListDef.PassportID = Indentity;
                                        }
                                        CrewPaxPassportGridInfo.Add(CrewPaxPassportInfoListDef);
                                    }
                                }
                            }
                        }
                    }
                    oCrew.CrewPassengerPassport = CrewPaxPassportGridInfo;
                    return oCrew;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Save Type Rating Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>


        private FlightPakMasterService.Crew SaveTypeRatings(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    oCrew.CrewRating = new List<CrewRating>();
                    Int64 Indentity = 0;
                    List<FlightPakMasterService.GetCrewRating> CrewTypeRatingInfoList = new List<FlightPakMasterService.GetCrewRating>();
                    FlightPakMasterService.CrewRating CrewRating = new FlightPakMasterService.CrewRating();
                    CrewTypeRatingInfoList = (List<FlightPakMasterService.GetCrewRating>)Session["CrewTypeRating"];
                    bool IsExist = false;
                    for (int Index = 0; Index < CrewTypeRatingInfoList.Count; Index++)
                    {
                        IsExist = false;
                        CrewRating = new CrewRating();
                        foreach (GridDataItem Item in dgTypeRating.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("AircraftTypeID").ToString() == CrewTypeRatingInfoList[Index].AircraftTypeID.ToString())
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("CrewRatingID").ToString()) != 0)
                                {
                                    CrewRating.CrewRatingID = Convert.ToInt64(Item.GetDataKeyValue("CrewRatingID").ToString());
                                }
                                else
                                {
                                    Indentity = Indentity - 1;
                                    CrewRating.CrewRatingID = Indentity;  // I think CrewRating shud cum here karthik
                                }
                                if (Session["SelectedCrewRosterID"] != null)
                                {
                                    CrewRating.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"]);
                                }
                                else
                                {
                                    CrewRating.CrewID = -1;
                                }
                                if (hdnClientID.Value != string.Empty && hdnClientID.Value != null)
                                {
                                    CrewRating.ClientID = Convert.ToInt64(hdnClientID.Value);
                                }
                                CrewRating.AircraftTypeID = CrewTypeRatingInfoList[Index].AircraftTypeID;
                                CrewRating.CustomerID = oCrew.CustomerID;
                                CrewRating.CrewRatingDescription = Item.GetDataKeyValue("CrewRatingDescription").ToString();
                                if (((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")) != null)
                                {
                                    CrewRating.IsPilotinCommand = ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsPilotinCommand = false;
                                }
                                if (((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")) != null)
                                {
                                    CrewRating.IsSecondInCommand = ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsSecondInCommand = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null)
                                {
                                    CrewRating.IsQualInType135PIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsQualInType135PIC = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null)
                                {
                                    CrewRating.IsQualInType135SIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsQualInType135SIC = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null)
                                {
                                    CrewRating.IsEngineer = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsEngineer = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null)
                                {
                                    CrewRating.IsInstructor = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsInstructor = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null)
                                {
                                    CrewRating.IsCheckAttendant = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsCheckAttendant = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null)
                                {
                                    CrewRating.IsAttendantFAR91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsAttendantFAR91 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null)
                                {
                                    CrewRating.IsAttendantFAR135 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsAttendantFAR135 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null)
                                {
                                    CrewRating.IsCheckAirman = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsCheckAirman = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null)
                                {
                                    CrewRating.IsInActive = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsInActive = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null)
                                {
                                    CrewRating.IsPIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsPIC121 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null)
                                {
                                    CrewRating.IsSIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsSIC121 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null)
                                {
                                    CrewRating.IsPIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsPIC125 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null)
                                {
                                    CrewRating.IsSIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsSIC125 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null)
                                {
                                    CrewRating.IsAttendant121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsAttendant121 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null)
                                {
                                    CrewRating.IsAttendant125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsAttendant125 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null)
                                {
                                    CrewRating.IsAttendant121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsAttendant121 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")) != null)
                                {
                                    CrewRating.IsSIC91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC91")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsSIC91 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")) != null)
                                {
                                    CrewRating.IsPIC91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC91")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsPIC91 = false;
                                }
                                //Grid Data Items

                                ////TotalTimeInTypeHrs
                                //if (!string.IsNullOrEmpty(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text))
                                //{
                                //    if (((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text == "0")
                                //    {
                                //        CrewRating.TotalTimeInTypeHrs = 0;
                                //    }
                                //    else
                                //    {
                                //        CrewRating.TotalTimeInTypeHrs = Convert.ToDecimal(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                //    }
                                //}
                                //else
                                //{
                                //    CrewRating.TotalTimeInTypeHrs = 0;
                                //}

                                ////TotalDayHrs
                                //if (!string.IsNullOrEmpty(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text))
                                //{
                                //    if (((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text == "0")
                                //    {
                                //        CrewRating.TotalDayHrs = 0;
                                //    }
                                //    else
                                //    {
                                //        CrewRating.TotalDayHrs = Convert.ToDecimal(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                                //    }
                                //}
                                //else
                                //{
                                //    CrewRating.TotalDayHrs = 0;
                                //}

                                ////TotalNightHrs
                                //if (!string.IsNullOrEmpty(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text))
                                //{
                                //    if (((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text == "0")
                                //    {
                                //        CrewRating.TotalNightHrs = 0;
                                //    }
                                //    else
                                //    {
                                //        CrewRating.TotalNightHrs = Convert.ToDecimal(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                                //    }
                                //}
                                //else
                                //{
                                //    CrewRating.TotalNightHrs = 0;
                                //}

                                ////TotalInstr
                                //if (!string.IsNullOrEmpty(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text))
                                //{
                                //    if (((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text == "0")
                                //    {
                                //        CrewRating.TotalNightHrs = 0;
                                //    }
                                //    else
                                //    {
                                //        CrewRating.TotalNightHrs = Convert.ToDecimal(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                                //    }
                                //}
                                //else
                                //{
                                //    CrewRating.TotalNightHrs = 0;
                                //}

                                if (!string.IsNullOrEmpty(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text))
                                {
                                    decimal TotalTimeInTypeHrs = 0;
                                    if (((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Contains(":"))
                                    {
                                        TotalTimeInTypeHrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text.Trim(), true, "GENERAL")), 1);
                                    }
                                    else
                                    {
                                        TotalTimeInTypeHrs = Convert.ToDecimal(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                    CrewRating.TotalTimeInTypeHrs = TotalTimeInTypeHrs;
                                }
                                else
                                {
                                    CrewRating.TotalTimeInTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text))
                                {
                                    decimal TotalDayHrs = 0;
                                    if (((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Contains(":"))
                                    {
                                        TotalDayHrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text.Trim(), true, "GENERAL")), 1);
                                        //CrewRetainRating[Index].TotalDayHrs = 0;
                                    }
                                    else
                                    {
                                        TotalDayHrs = Convert.ToDecimal(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                    CrewRating.TotalDayHrs = TotalDayHrs;
                                }
                                else
                                {
                                    CrewRating.TotalDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text))
                                {
                                    decimal TotalNightHrs = 0;
                                    if (((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Contains(":"))
                                    {
                                        TotalNightHrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text.Trim(), true, "GENERAL")), 1);
                                    }
                                    else
                                    {
                                        TotalNightHrs = Convert.ToDecimal(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                    CrewRating.TotalNightHrs = TotalNightHrs;
                                }
                                else
                                {
                                    CrewRating.TotalNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text))
                                {
                                    decimal TotalINSTHrs = 0;
                                    if (((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text.Contains(":"))
                                    {
                                        TotalINSTHrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text.Trim(), true, "GENERAL")), 1);
                                    }
                                    else
                                    {
                                        TotalINSTHrs = Convert.ToDecimal(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                    CrewRating.TotalINSTHrs = TotalINSTHrs;
                                }
                                else
                                {
                                    CrewRating.TotalINSTHrs = 0;
                                }

                                //Grid Data Items Ends
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text))
                                {
                                    if (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text == "0")
                                    {
                                        CrewRating.TimeInType = 0;
                                    }
                                    else
                                    {
                                        CrewRating.TimeInType = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                }
                                else
                                {
                                    CrewRating.TimeInType = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                                {
                                    CrewRating.Day1 = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.Day1 = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                                {
                                    CrewRating.Night1 = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.Night1 = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                                {
                                    CrewRating.Instrument = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.Instrument = 0;
                                }
                                // PIC Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                                {
                                    CrewRating.PilotInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.PilotInCommandTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                                {
                                    CrewRating.TPilotinCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TPilotinCommandDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                                {
                                    CrewRating.TPilotInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TPilotInCommandNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                                {
                                    CrewRating.TPilotInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TPilotInCommandINSTHrs = 0;
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                                {
                                    CrewRating.SecondInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.SecondInCommandTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                                {
                                    CrewRating.SecondInCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.SecondInCommandDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                                {
                                    CrewRating.SecondInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.SecondInCommandNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                                {
                                    CrewRating.SecondInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.SecondInCommandINSTHrs = 0;
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                                {
                                    CrewRating.OtherSimHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.OtherSimHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                                {
                                    CrewRating.OtherFlightEngHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.OtherFlightEngHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                                {
                                    CrewRating.OtherFlightInstrutorHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.OtherFlightInstrutorHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                                {
                                    CrewRating.OtherFlightAttdHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.OtherFlightAttdHrs = 0;
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                                {
                                    CrewRating.TotalUpdateAsOfDT = Convert.ToDateTime(FormatDateTime((((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim()), DateFormat));
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewRating.AsOfDT = Convert.ToDateTime(FormatDate(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text.Trim(), DateFormat));
                                    }
                                    else
                                    {
                                        CrewRating.AsOfDT = Convert.ToDateTime(((Label)Item["HiddenCheckboxValues"].FindControl("lbAsOfDt")).Text.Trim());
                                    }
                                }
                                else
                                {
                                    CrewRating.AsOfDT = null;
                                }
                                CrewRating.LastUpdTS = CrewTypeRatingInfoList[Index].LastUpdTS;
                                CrewRating.LastUpdUID = CrewTypeRatingInfoList[Index].LastUpdUID;
                                CrewRating.IsDeleted = false; // I think CrewRating shud cum here 
                                IsExist = true;
                                break;
                            }
                        }
                        if (IsExist == false)
                        {
                            if (CrewTypeRatingInfoList[Index].CrewRatingID != 0)
                            {
                                CrewRating.CrewRatingID = CrewTypeRatingInfoList[Index].CrewRatingID;
                            }
                            else
                            {
                                Indentity = Indentity - 1;
                                CrewRating.CrewRatingID = Indentity;
                            }
                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                CrewRating.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"]);
                            }
                            else
                            {
                                CrewRating.CrewID = -1;
                            }

                            CrewRating.AircraftTypeID = CrewTypeRatingInfoList[Index].AircraftTypeID;
                            CrewRating.CrewRatingID = CrewTypeRatingInfoList[Index].CrewRatingID;
                            CrewRating.CustomerID = CrewTypeRatingInfoList[Index].CustomerID;
                            if (CrewTypeRatingInfoList[Index].IsDeleted)
                            {
                                CrewRating.IsDeleted = CrewTypeRatingInfoList[Index].IsDeleted;
                            }
                            CrewRating.CrewRatingDescription = CrewTypeRatingInfoList[Index].CrewRatingDescription;
                            CrewRating.AsOfDT = CrewTypeRatingInfoList[Index].AsOfDT;
                            CrewRating.TotalUpdateAsOfDT = CrewTypeRatingInfoList[Index].TotalUpdateAsOfDT;
                            CrewRating.IsPilotinCommand = CrewTypeRatingInfoList[Index].IsPilotinCommand;
                            CrewRating.IsSecondInCommand = CrewTypeRatingInfoList[Index].IsSecondInCommand;
                            CrewRating.IsQualInType135PIC = CrewTypeRatingInfoList[Index].IsQualInType135PIC;
                            CrewRating.IsQualInType135SIC = CrewTypeRatingInfoList[Index].IsQualInType135PIC;
                            CrewRating.IsEngineer = CrewTypeRatingInfoList[Index].IsEngineer;
                            CrewRating.IsInstructor = CrewTypeRatingInfoList[Index].IsInstructor;
                            CrewRating.IsCheckAttendant = CrewTypeRatingInfoList[Index].IsCheckAttendant;
                            CrewRating.IsAttendantFAR91 = CrewTypeRatingInfoList[Index].IsAttendantFAR91;
                            CrewRating.IsAttendantFAR135 = CrewTypeRatingInfoList[Index].IsAttendantFAR135;
                            CrewRating.IsCheckAirman = CrewTypeRatingInfoList[Index].IsCheckAirman;
                            CrewRating.IsInActive = CrewTypeRatingInfoList[Index].IsInActive;
                            CrewRating.IsPIC91 = CrewTypeRatingInfoList[Index].IsPIC91;
                            CrewRating.IsSIC91 = CrewTypeRatingInfoList[Index].IsSIC91;
                            CrewRating.IsPIC121 = CrewTypeRatingInfoList[Index].IsPIC121;
                            CrewRating.IsSIC121 = CrewTypeRatingInfoList[Index].IsSIC121;
                            CrewRating.IsPIC125 = CrewTypeRatingInfoList[Index].IsPIC125;
                            CrewRating.IsSIC125 = CrewTypeRatingInfoList[Index].IsSIC125;
                            CrewRating.IsAttendant121 = CrewTypeRatingInfoList[Index].IsAttendant121;
                            CrewRating.IsAttendant125 = CrewTypeRatingInfoList[Index].IsAttendant125;
                            CrewRating.TimeInType = CrewTypeRatingInfoList[Index].TimeInType;
                            CrewRating.Day1 = CrewTypeRatingInfoList[Index].Day1;
                            CrewRating.Night1 = CrewTypeRatingInfoList[Index].Night1;
                            CrewRating.Instrument = CrewTypeRatingInfoList[Index].Instrument;
                            // PIC Hrs
                            CrewRating.PilotInCommandTypeHrs = CrewTypeRatingInfoList[Index].PilotInCommandTypeHrs;
                            CrewRating.TPilotinCommandDayHrs = CrewTypeRatingInfoList[Index].TPilotinCommandDayHrs;
                            CrewRating.TPilotInCommandNightHrs = CrewTypeRatingInfoList[Index].TPilotInCommandNightHrs;
                            CrewRating.TPilotInCommandINSTHrs = CrewTypeRatingInfoList[Index].TPilotInCommandINSTHrs;
                            // SIC Hrs
                            CrewRating.SecondInCommandTypeHrs = CrewTypeRatingInfoList[Index].SecondInCommandTypeHrs;
                            CrewRating.SecondInCommandDayHrs = CrewTypeRatingInfoList[Index].SecondInCommandDayHrs;
                            CrewRating.SecondInCommandNightHrs = CrewTypeRatingInfoList[Index].SecondInCommandNightHrs;
                            CrewRating.SecondInCommandINSTHrs = CrewTypeRatingInfoList[Index].SecondInCommandINSTHrs;
                            // Other Hrs
                            CrewRating.OtherSimHrs = CrewTypeRatingInfoList[Index].OtherSimHrs;
                            CrewRating.OtherFlightEngHrs = CrewTypeRatingInfoList[Index].OtherFlightEngHrs;
                            CrewRating.OtherFlightInstrutorHrs = CrewTypeRatingInfoList[Index].OtherFlightInstrutorHrs;
                            CrewRating.OtherFlightAttdHrs = CrewTypeRatingInfoList[Index].OtherFlightAttdHrs;
                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                CrewRating.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"]);
                            }
                            else
                            {
                                CrewRating.CrewID = -1;
                            }

                        }
                        if (CrewRating.CrewRatingID == 0)
                        {
                            Indentity = Indentity - 1;
                            CrewRating.CrewRatingID = Indentity;
                        }
                        oCrew.CrewRating.Add(CrewRating);
                    }
                    return oCrew;
                }, FlightPak.Common.Constants.Policy.UILayer);

                return oCrew;
            }
        }

        /// <summary>
        /// Method to Save Crew Checklist Informations
        /// </summary>
        /// <param name="CrewCode"></param>

        private FlightPakMasterService.Crew SaveCrewChecklist(FlightPakMasterService.Crew oCrew)
        {
            //Soft Delete the previous Crew Checklist Items 
            //belongs to the specified Crew Code.

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    oCrew.CrewCheckListDetail = new List<CrewCheckListDetail>();
                    Int64 Indentity = 0;
                    List<FlightPakMasterService.GetCrewCheckListDate> CrewDefinitionInfoList = new List<FlightPakMasterService.GetCrewCheckListDate>();
                    FlightPakMasterService.CrewCheckListDetail CrewCheckListDetail = new FlightPakMasterService.CrewCheckListDetail();
                    CrewDefinitionInfoList = (List<FlightPakMasterService.GetCrewCheckListDate>)Session["CrewCheckList"];
                    bool IsExist = false;
                    for (int Index = 0; Index < CrewDefinitionInfoList.Count; Index++)
                    {
                        IsExist = false;
                        CrewCheckListDetail = new CrewCheckListDetail();
                        foreach (GridDataItem Item in dgCrewCheckList.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("CheckListCD").ToString() == CrewDefinitionInfoList[Index].CheckListCD.ToString())
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("CheckListID").ToString()) != 0)
                                {
                                    CrewCheckListDetail.CheckListID = Convert.ToInt64(Item.GetDataKeyValue("CheckListID").ToString());
                                }
                                else
                                {
                                    Indentity = Indentity - 1;
                                    CrewCheckListDetail.CheckListID = Indentity; // CrewINFO ID
                                }
                                CrewCheckListDetail.CrewID = oCrew.CrewID;
                                if (CrewDefinitionInfoList[Index].IsDeleted)
                                {
                                    CrewCheckListDetail.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted;
                                }
                                if (Item.GetDataKeyValue("CheckListCD") != null)
                                {
                                    CrewCheckListDetail.CheckListCD = (Item.GetDataKeyValue("CheckListCD").ToString());
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.OriginalDT = Convert.ToDateTime(FormatDate(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.OriginalDT = Convert.ToDateTime(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.PreviousCheckDT = Convert.ToDateTime(FormatDate(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.PreviousCheckDT = Convert.ToDateTime(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text);
                                    }
                                }

                                //if (!string.IsNullOrEmpty(((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text))
                                //{
                                //    CrewCheckListDetail.TotalREQFlightHrs = Convert.ToDecimal(((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text);
                                //}

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text))
                                {
                                    if (((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text == "0")
                                    {
                                        CrewCheckListDetail.TotalREQFlightHrs = 0;
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.TotalREQFlightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalREQFlightHrs")).Text, CultureInfo.CurrentCulture);
                                    }
                                }
                                else
                                {
                                    CrewCheckListDetail.TotalREQFlightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["DueDT"].FindControl("lbDueDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.DueDT = Convert.ToDateTime(FormatDate(((Label)Item["DueDT"].FindControl("lbDueDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.DueDT = Convert.ToDateTime(((Label)Item["DueDT"].FindControl("lbDueDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.AlertDT = Convert.ToDateTime(FormatDate(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.AlertDT = Convert.ToDateTime(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                                {
                                    CrewCheckListDetail.Frequency = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text);
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.GraceDT = Convert.ToDateTime(FormatDate(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.GraceDT = Convert.ToDateTime(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.BaseMonthDT = Convert.ToDateTime(FormatDate(((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.BaseMonthDT = Convert.ToDateTime(((Label)Item["BaseMonthDT"].FindControl("lbBaseMonthDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                                {
                                    CrewCheckListDetail.GraceDays = Convert.ToInt32(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                                {
                                    CrewCheckListDetail.AlertDays = Convert.ToInt32(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                                {
                                    CrewCheckListDetail.FrequencyMonth = Convert.ToInt32(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text);
                                }
                                // Start
                                if ((!string.IsNullOrEmpty(((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text)) && (!string.IsNullOrEmpty(((HiddenField)Item["AircraftTypeCD"].FindControl("hdnAircraftTypeCD")).Value)))
                                {
                                    CrewCheckListDetail.AircraftID = Convert.ToInt64(((HiddenField)Item["AircraftTypeCD"].FindControl("hdnAircraftTypeCD")).Value);
                                }
                                //End
                                CrewCheckListDetail.IsPilotInCommandFAR91 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked;
                                CrewCheckListDetail.IsPilotInCommandFAR135 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked;
                                CrewCheckListDetail.IsSecondInCommandFAR91 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked;
                                CrewCheckListDetail.IsSecondInCommandFAR135 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked;
                                CrewCheckListDetail.IsInActive = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                                CrewCheckListDetail.IsMonthEnd = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                                CrewCheckListDetail.IsStopCALC = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                                CrewCheckListDetail.IsOneTimeEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                                CrewCheckListDetail.IsCompleted = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                                CrewCheckListDetail.IsNoConflictEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                                CrewCheckListDetail.IsNoCrewCheckListREPTt = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked;
                                CrewCheckListDetail.IsNoChecklistREPT = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                                CrewCheckListDetail.IsPassedDueAlert = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                                CrewCheckListDetail.IsPrintStatus = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked;
                                CrewCheckListDetail.IsNextMonth = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                                CrewCheckListDetail.IsEndCalendarYear = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                                CrewCheckListDetail.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                                CrewCheckListDetail.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                                CrewCheckListDetail.IsDeleted = false;
                                IsExist = true;
                                break;
                            }
                        }
                        //IsExist
                        if (IsExist == false)
                        {
                            if (CrewDefinitionInfoList[Index].CheckListID != 0)
                            {
                                CrewCheckListDetail.CheckListID = CrewDefinitionInfoList[Index].CheckListID;
                            }
                            else
                            {
                                Indentity = Indentity - 1;
                                CrewCheckListDetail.CheckListID = Indentity; //CrewInfoID
                            }
                            CrewCheckListDetail.CrewID = CrewDefinitionInfoList[Index].CrewID;
                            CrewCheckListDetail.CustomerID = CrewDefinitionInfoList[Index].CustomerID;

                            if (CrewDefinitionInfoList[Index].IsDeleted)
                            {
                                CrewCheckListDetail.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted;
                            }
                            CrewCheckListDetail.CheckListCD = CrewDefinitionInfoList[Index].CheckListCD;
                            CrewCheckListDetail.PreviousCheckDT = CrewDefinitionInfoList[Index].PreviousCheckDT;
                            CrewCheckListDetail.Frequency = CrewDefinitionInfoList[Index].Frequency;
                            CrewCheckListDetail.DueDT = CrewDefinitionInfoList[Index].DueDT;
                            CrewCheckListDetail.AlertDT = CrewDefinitionInfoList[Index].AlertDT;
                            CrewCheckListDetail.GraceDT = CrewDefinitionInfoList[Index].GraceDT;
                            CrewCheckListDetail.GraceDays = CrewDefinitionInfoList[Index].GraceDays;
                            CrewCheckListDetail.AlertDays = CrewDefinitionInfoList[Index].AlertDays;
                            CrewCheckListDetail.FrequencyMonth = CrewDefinitionInfoList[Index].FrequencyMonth;
                            CrewCheckListDetail.AircraftID = CrewDefinitionInfoList[Index].AircraftID;
                            CrewCheckListDetail.TotalREQFlightHrs = CrewDefinitionInfoList[Index].TotalREQFlightHrs;

                            CrewCheckListDetail.BaseMonthDT = CrewDefinitionInfoList[Index].BaseMonthDT;
                            CrewCheckListDetail.OriginalDT = CrewDefinitionInfoList[Index].OriginalDT;
                            CrewCheckListDetail.IsPilotInCommandFAR91 = CrewDefinitionInfoList[Index].IsPilotInCommandFAR91;
                            CrewCheckListDetail.IsPilotInCommandFAR135 = CrewDefinitionInfoList[Index].IsPilotInCommandFAR135;
                            CrewCheckListDetail.IsSecondInCommandFAR91 = CrewDefinitionInfoList[Index].IsSecondInCommandFAR91;
                            CrewCheckListDetail.IsSecondInCommandFAR135 = CrewDefinitionInfoList[Index].IsSecondInCommandFAR135;
                            CrewCheckListDetail.IsInActive = CrewDefinitionInfoList[Index].IsInActive;
                            CrewCheckListDetail.IsMonthEnd = CrewDefinitionInfoList[Index].IsMonthEnd;
                            CrewCheckListDetail.IsStopCALC = CrewDefinitionInfoList[Index].IsStopCALC;
                            CrewCheckListDetail.IsOneTimeEvent = CrewDefinitionInfoList[Index].IsOneTimeEvent;
                            CrewCheckListDetail.IsCompleted = CrewDefinitionInfoList[Index].IsCompleted;
                            CrewCheckListDetail.IsNoConflictEvent = CrewDefinitionInfoList[Index].IsNoConflictEvent;
                            CrewCheckListDetail.IsNoCrewCheckListREPTt = CrewDefinitionInfoList[Index].IsNoCrewCheckListREPTt;
                            CrewCheckListDetail.IsNoChecklistREPT = CrewDefinitionInfoList[Index].IsNoChecklistREPT;
                            CrewCheckListDetail.IsPassedDueAlert = CrewDefinitionInfoList[Index].IsPassedDueAlert;
                            CrewCheckListDetail.IsPrintStatus = CrewDefinitionInfoList[Index].IsPrintStatus;
                            CrewCheckListDetail.IsNextMonth = CrewDefinitionInfoList[Index].IsNextMonth;
                            CrewCheckListDetail.IsEndCalendarYear = CrewDefinitionInfoList[Index].IsEndCalendarYear;
                            CrewCheckListDetail.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                            CrewCheckListDetail.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                        }
                        if (CrewCheckListDetail.CheckListID == 0)
                        {
                            Indentity = Indentity - 1;
                            CrewCheckListDetail.CheckListID = Indentity; //crewinfoid
                        }
                        oCrew.CrewCheckListDetail.Add(CrewCheckListDetail);
                    }
                    return oCrew;

                }, FlightPak.Common.Constants.Policy.UILayer);
                return oCrew;
            }
        }

        /// <summary>
        /// Method to Save Aircraft Assigned Informations
        /// </summary>
        /// <param name="CrewCode"></param>
        private FlightPakMasterService.Crew SaveAircraftAssigned(FlightPakMasterService.Crew oCrew)
        {

            // Soft Delete the previous Crew Definition Items 
            // belongs to the specified Crew Code.
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (hdnSave.Value == "Update")
                    {
                        MasterCatalogServiceClient AircraftAssignedService = new MasterCatalogServiceClient();
                        List<CrewAircraftAssigned> AircraftAssignedList = new List<CrewAircraftAssigned>();
                        CrewAircraftAssigned CrewAircraftAssigned = new CrewAircraftAssigned();
                        CrewAircraftAssigned.CrewID = oCrew.CrewID;
                        CrewAircraftAssigned.CustomerID = oCrew.CustomerID;
                        var CrewRosterServiceValues = AircraftAssignedService.GetAircraftAssignedList(CrewAircraftAssigned).EntityList.ToList();
                        if (CrewRosterServiceValues.Count > 0)
                        {
                            CrewAircraftAssigned.CrewID = oCrew.CrewID;
                            CrewAircraftAssigned.CustomerID = oCrew.CustomerID;
                            CrewAircraftAssigned.IsDeleted = true;
                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {
                                CrewRosterService.DeleteCrewAircraftAssigned(CrewAircraftAssigned);
                            }
                        }


                    }

                    // Save the added information items from the Grid

                    Int64 Indentity = 0;
                    oCrew.CrewAircraftAssigned = new List<CrewAircraftAssigned>();
                    CrewAircraftAssigned CrewAircraftAssigned1;
                    foreach (GridDataItem Item in dgAircraftAssigned.MasterTableView.Items)
                    {
                        CheckBox chkAircraftAssigned = (CheckBox)Item["AircraftAssigned"].FindControl("chkAircraftAssigned");
                        if (chkAircraftAssigned.Checked == true)
                        {
                            CrewAircraftAssigned1 = new CrewAircraftAssigned();
                            if (Item.GetDataKeyValue("CrewAircraftAssignedID") != null)
                            {
                                CrewAircraftAssigned1.CrewAircraftAssignedID = Convert.ToInt64(Item.GetDataKeyValue("CrewAircraftAssignedID").ToString());
                            }
                            else
                            {
                                Indentity = Indentity - 1;
                                CrewAircraftAssigned1.CrewAircraftAssignedID = Indentity;
                            }
                            CrewAircraftAssigned1.CustomerID = oCrew.CustomerID;
                            CrewAircraftAssigned1.CrewID = oCrew.CrewID;  // hidden value ll come here
                            CrewAircraftAssigned1.FleetID = Convert.ToInt64(Convert.ToString(Item.GetDataKeyValue("FleetID")));  // Item.GetDataKeyValue("FleetID").ToString();
                            CrewAircraftAssigned1.IsDeleted = false;
                            oCrew.CrewAircraftAssigned.Add(CrewAircraftAssigned1);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

                return oCrew;
            }
        }

        #endregion
        #endregion

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    
                    dgCrewRoster.SelectedIndexes.Clear();
                    chkHomeBaseOnly.Checked = false;
                    chkActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }

        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var CrewValue = FPKMstService.GetAllCrewForGrid();
            List<FlightPakMasterService.GetAllCrewForGrid> CrewGridData = new List<FlightPakMasterService.GetAllCrewForGrid>();
            if (CrewValue.ReturnFlag)
            {
                CrewGridData = CrewValue.EntityList;
            }

            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CrewGridData);

            if (IsPopUp)
            {
                foreach (var Item in CrewGridData)
                {
                    if (PrimaryKeyValue == Item.CrewID)
                    {
                        FullItemIndex = iIndex;
                        break;
                    }

                    iIndex++;
                }
            }
            else
            {
                List<GetAllCrewWithFilters> filteredList = GetFilteredList(FPKMstService);

                foreach (var Item in filteredList)
                {
                    if (PrimaryKeyValue == Item.CrewID)
                    {
                        FullItemIndex = iIndex;
                        break;
                    }

                    iIndex++;
                }
            }

            int PageSize = dgCrewRoster.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCrewRoster.CurrentPageIndex = PageNumber;
            dgCrewRoster.SelectedIndexes.Add(ItemIndex);
        }

        private Int64 CheckOriginForPreSelection(bool fromSearch, List<FlightPakMasterService.GetAllCrewForGrid> CrewGridData)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedCrewRosterID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CrewGridData.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().CrewID;
                Session["SelectedCrewRosterID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }

        private List<FlightPakMasterService.GetAllCrewWithFilters> GetFilteredList(FlightPakMasterService.MasterCatalogServiceClient FPKMstService)
        {
            List<FlightPakMasterService.GetAllCrewWithFilters> filteredList = new List<FlightPakMasterService.GetAllCrewWithFilters>();

            if (((CheckCrewGroupFilterCodeExist() == false) && (CheckClientFilterCodeExist() == false)))
            {
                bool IsInActive = false;
                bool IsRotaryWing = false;
                bool IsFixedWing = false;
                long CrewID = 0;
                long ClientID = 0;
                long CrewGroupID = 0;
                string CrewCD = string.Empty;
                string ClientCode = string.Empty;
                string CrewGroupCD = string.Empty;
                string ICAO = string.Empty;

                if (chkActiveOnly.Checked)
                {
                    IsInActive = true;
                }
                if (chkHomeBaseOnly.Checked)
                {
                    ICAO = UserPrincipal.Identity._airportICAOCd;
                }
                if (chkFixedWingOnly.Checked)
                {
                    IsFixedWing = true;
                }
                if (chkRotaryWingOnly.Checked)
                {
                    IsRotaryWing = true;
                }

                if (tbClientCodeFilter.Text.Trim() != "")
                {
                    ClientCode = tbClientCodeFilter.Text.ToString().ToUpper().Trim();
                    if (hdnClientFilterID.Value != string.Empty)
                        ClientID = Convert.ToInt64(hdnClientFilterID.Value);
                }

                if (tbCrewGroupFilter.Text.Trim() != "")
                {
                    CrewGroupCD = tbCrewGroupFilter.Text.ToString().ToUpper().Trim();
                    if (hdnCrewGroupID.Value != string.Empty)
                        CrewGroupID = Convert.ToInt64(hdnCrewGroupID.Value);
                }
                filteredList = FPKMstService.GetAllCrewWithFilters(ClientID, IsInActive, IsRotaryWing, IsFixedWing, ICAO, CrewGroupID, CrewID, CrewCD).EntityList.ToList();

                if (CrewGroupID != 0)
                {
                    filteredList = ReorderCrew(filteredList, Convert.ToInt64(UserPrincipal.Identity._customerID), CrewGroupID);
                }
            }

            return filteredList;
        }

        public List<GetCrewRating> TypeRatingforParticularValue(Int64 CrewID, Int64 AircraftID)
        {
            List<GetCrewRating> CrewRatingList = new List<GetCrewRating>();
            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
            {
                GetCrewRating crew = new GetCrewRating();
                crew.CrewID = CrewID;
                var CrewRatingValue = CrewRosterService.GetCrewRatingList(crew);
                if (CrewRatingValue.ReturnFlag == true)
                {
                    CrewRatingList = CrewRatingValue.EntityList.Where(x => x.IsDeleted == false && x.AircraftTypeID == AircraftID).ToList<FlightPakMasterService.GetCrewRating>();
                }
            }
            return CrewRatingList;
        }

        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            //Ramesh: Code to allow any file type for upload control.
            int iIndex = filename.Trim().IndexOf('.');
            //string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            if (regMatch.Success)
            {
                if (Request.UserAgent.Contains("Chrome"))
                {
                    hdnBrowserName.Value = "Chrome";
                }
                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                else
                {
                    Session["Base64"] = filedata;
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid();
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgFile.Visible = false;
                lnkFileName.Visible = true;
                Session["Base64"] = filedata;
                lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();
            }
        }

        protected void tbGreenNationality_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        CheckCountryExist(tbGreenNationality, cvGreenNationality);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        private void setColorCheckListRowItem(GridDataItem Item,bool IsOldRow=false)
        {
            string cssClassNormal = null;
            string cssClassAlert = null;
            string cssClassGrace=null;
            string cssClassExpired=null;
            if (IsOldRow)
            {
                cssClassNormal = "Normal-rec";
                cssClassAlert = "Alert-rec";
                cssClassGrace= "Grace-rec";
                cssClassExpired = "Expired-rec";
            }
            else
            {
                cssClassNormal = "Normal-rec-select";
                cssClassAlert = "Alert-rec-select";
                cssClassGrace = "Grace-rec-select";
                cssClassExpired = "Expired-rec-select";
            }
            if (Item != null)
            {
                bool IsScheduleCheckFlag = false;
                if (Session["CheckListSetting"] != null)
                {
                    List<FlightPakMasterService.CrewCheckList> GetCrewCheckList = new List<FlightPakMasterService.CrewCheckList>();
                    GetCrewCheckList = (List<FlightPakMasterService.CrewCheckList>)Session["CheckListSetting"];
                    var objScheduleCheck = GetCrewCheckList.Where(x => (x.CrewCheckCD == Item["ChecklistCD"].Text.Trim()) && (x.IsScheduleCheck == true));
                    if (objScheduleCheck.Count() > 0)
                    {
                        Item.CssClass = cssClassExpired;
                        IsScheduleCheckFlag = true;
                    }
                    else
                    {
                        Item.CssClass = cssClassNormal;
                    }
                }
                string TestDueDT = ((Label)(Item["DueDT"].FindControl("lbDueDT"))).Text;
                string TestAlertDT = ((Label)(Item["AlertDT"].FindControl("lbAlertDT"))).Text;
                string TestGraceDT = ((Label)(Item["GraceDT"].FindControl("lbGraceDT"))).Text;

                if (IsScheduleCheckFlag == true && (!string.IsNullOrEmpty(TestDueDT)) && (!string.IsNullOrEmpty(TestAlertDT)) && (!string.IsNullOrEmpty(TestGraceDT)))
                {
                    DateTime DueNextSource = DateTime.MinValue;
                    DueNextSource = Convert.ToDateTime(FormatDate(((Label)(Item["DueDT"].FindControl("lbDueDT"))).Text, DateFormat));

                    DateTime AlertSource1 = DateTime.MinValue;
                    AlertSource1 = Convert.ToDateTime(FormatDate(((Label)(Item["AlertDT"].FindControl("lbAlertDT"))).Text, DateFormat));

                    DateTime GraceSource2 = DateTime.MinValue;
                    GraceSource2 = Convert.ToDateTime(FormatDate(((Label)(Item["GraceDT"].FindControl("lbGraceDT"))).Text, DateFormat));

                    if ((DueNextSource > System.DateTime.Now) && (AlertSource1 > System.DateTime.Now) && (GraceSource2 > System.DateTime.Now))
                    {
                        Item.CssClass = cssClassNormal;
                    }
                    if (((DueNextSource < System.DateTime.Now) && (System.DateTime.Now < GraceSource2)) || ((GraceSource2 < System.DateTime.Now) && (System.DateTime.Now < DueNextSource)))
                    {
                        Item.CssClass = cssClassGrace;
                    }
                    if ((System.DateTime.Now > DueNextSource) && (GraceSource2 < System.DateTime.Now))
                    {
                        Item.CssClass = cssClassExpired;
                    }
                    if (((DueNextSource < System.DateTime.Now) && (System.DateTime.Now < AlertSource1)) || ((AlertSource1 < System.DateTime.Now) && (System.DateTime.Now < DueNextSource)))
                    {
                        Item.CssClass = cssClassAlert;
                    }
                }
            }
        }
    }
}
