﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Data;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class EmergencyContactCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private bool EmergencyContactPageNavigated = false;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgEmergencyContact, dgEmergencyContact, this.Page.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                        //store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgEmergencyContact.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (UserPrincipal.Identity._fpSettings._IsAPISSupport != null)
                        {
                            if (UserPrincipal.Identity._fpSettings._IsAPISSupport == true)
                            {
                                lbFirstName.CssClass = "important-bold-text";
                                lbMiddleName.CssClass = "important-text";
                                lbLastName.CssClass = "important-bold-text";
                                lbBusinessPhone.CssClass = "important-bold-text";
                                lbBusinessEmail.CssClass = "important-bold-text";
                            }
                            else
                            {
                                lbFirstName.CssClass = "mnd_text";
                                lbMiddleName.ForeColor = System.Drawing.Color.Black;
                                lbLastName.CssClass = "mnd_text";
                                lbBusinessPhone.CssClass = "mnd_text";
                                lbBusinessEmail.CssClass = "mnd_text";
                            }

                        }
                        else
                        {
                            lbFirstName.CssClass = "mnd_text";
                            lbMiddleName.ForeColor = System.Drawing.Color.Black;
                            lbLastName.CssClass = "mnd_text";
                            lbBusinessPhone.CssClass = "mnd_text";
                            lbBusinessEmail.CssClass = "mnd_text";
                        }
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewEmergencyContactCatalog);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgEmergencyContact.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                        if (IsPopUp)
                        {
                            dgEmergencyContact.AllowPaging = false;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgEmergencyContact.Rebind();
                    }
                    if (dgEmergencyContact.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["EmergencyContactID"] = null;
                        //}
                        if (Session["EmergencyContactID"] == null)
                        {
                            dgEmergencyContact.SelectedIndexes.Add(0);
                            Session["EmergencyContactID"] = dgEmergencyContact.Items[0].GetDataKeyValue("EmergencyContactID").ToString();
                        }

                        if (Request.QueryString["EmergencyContactID"] != null)
                        {
                            Session["EmergencyContactID"] = Request.QueryString["EmergencyContactID"];
                        }
                        else
                        {
                            if (dgEmergencyContact.SelectedIndexes.Count == 0)
                                dgEmergencyContact.SelectedIndexes.Add(0);
                        }

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["EmergencyContactID"] != null)
                    {
                        string ID = Session["EmergencyContactID"].ToString();
                        foreach (GridDataItem Item in dgEmergencyContact.MasterTableView.Items)
                        {
                            if (Item["EmergencyContactID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, delCtl, editCtl;
                    insertCtl = (LinkButton)dgEmergencyContact.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    delCtl = (LinkButton)dgEmergencyContact.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    editCtl = (LinkButton)dgEmergencyContact.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddEmergencyContactCatalog))
                    {
                        insertCtl.Visible = true;
                        if (add)
                        {
                            insertCtl.Enabled = true;
                        }
                        else
                        {
                            insertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteEmergencyContactCatalog))
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditEmergencyContactCatalog))
                    {
                        editCtl.Visible = true;
                        if (edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    LoadControlData();
                    EnableForm(true);
                    tbCode.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbFirstName.Enabled = enable;
                    tbCountry.Enabled = enable;
                    tbAddress1.Enabled = enable;
                    tbAddress2.Enabled = enable;
                    tbCity.Enabled = enable;
                    tbEmail.Enabled = enable;
                    tbFax.Enabled = enable;
                    tbLastName.Enabled = enable;
                    tbMiddleName.Enabled = enable;
                    tbMobile.Enabled = enable;
                    tbPhone.Enabled = enable;
                    tbPostal.Enabled = enable;
                    tbState.Enabled = enable;
                    tbBusinessFax.Enabled = enable;
                    tbBusinessPhone.Enabled = enable;
                    tbOtherEmail.Enabled = enable;
                    tbOtherPhone.Enabled = enable;
                    tbSecondaryMobile.Enabled = enable;
                    tbPersonalEmail.Enabled = enable;
                    tbAddress3.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;
                    btnCountry.Enabled = enable;
                    chkInactive.Enabled = enable;      
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbFirstName.Text = string.Empty;
                    tbCountry.Text = string.Empty;
                    hdnCountry.Value = string.Empty;
                    tbAddress1.Text = string.Empty;
                    tbAddress2.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbMobile.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbPostal.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbBusinessFax.Text = string.Empty;
                    tbBusinessPhone.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    tbSecondaryMobile.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbAddress3.Text = string.Empty;
                    chkInactive.Checked = false;

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["EmergencyContactID"] != null)
                    {
                        FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient();
                        var emergencyContactValue = FPKMstService.GetEmergencyListInfo(); //getting Emergency contact all data

                        foreach (var item in emergencyContactValue.EntityList)
                        {
                            if (item.EmergencyContactID.ToString() == Session["EmergencyContactID"].ToString().Trim())
                            {
                                tbCode.Text = item.EmergencyContactCD.ToString();
                                hdnECID.Value = item.EmergencyContactID.ToString();
                                if (item.FirstName != null)
                                {
                                    tbFirstName.Text = item.FirstName.ToString();
                                }
                                else
                                {
                                    tbFirstName.Text = string.Empty;
                                }
                                if (item.CountryCD != null)
                                {
                                    tbCountry.Text = item.CountryCD.ToString();
                                    hdnCountry.Value = item.CountryID.ToString();
                                }
                                else
                                {
                                    tbCountry.Text = string.Empty;
                                    hdnCountry.Value = string.Empty;
                                }
                                if (item.LastName != null)
                                {
                                    tbLastName.Text = item.LastName.ToString();
                                }
                                else
                                {
                                    tbLastName.Text = string.Empty;
                                }
                                if (item.FirstName != null)
                                {
                                    tbFirstName.Text = item.FirstName.ToString();
                                }
                                else
                                {
                                    tbFirstName.Text = string.Empty;
                                }
                                if (item.MiddleName != null)
                                {
                                    tbMiddleName.Text = item.MiddleName.ToString();
                                }
                                else
                                {
                                    tbMiddleName.Text = string.Empty;
                                }
                                if (item.Addr1 != null)
                                {
                                    tbAddress1.Text = item.Addr1.ToString();
                                }
                                else
                                {
                                    tbAddress1.Text = string.Empty;
                                }
                                if (item.Addr2 != null)
                                {
                                    tbAddress2.Text = item.Addr2.ToString();
                                }
                                else
                                {
                                    tbAddress2.Text = string.Empty;
                                }
                                if (item.CityName != null)
                                {
                                    tbCity.Text = item.CityName.ToString();
                                }
                                else
                                {
                                    tbCity.Text = string.Empty;
                                }
                                if (item.StateName != null)
                                {
                                    tbState.Text = item.StateName.ToString();
                                }
                                else
                                {
                                    tbState.Text = string.Empty;
                                }
                                if (item.PostalZipCD != null)
                                {
                                    tbPostal.Text = item.PostalZipCD.ToString();
                                }
                                else
                                {
                                    tbPostal.Text = string.Empty;
                                }
                                if (item.PostalZipCD != null)
                                {
                                    tbPostal.Text = item.PostalZipCD.ToString();
                                }
                                else
                                {
                                    tbPostal.Text = string.Empty;
                                }
                                if (item.PhoneNum != null)
                                {
                                    tbPhone.Text = item.PhoneNum.ToString();
                                }
                                else
                                {
                                    tbPhone.Text = string.Empty;
                                }
                                if (item.FaxNum != null)
                                {
                                    tbFax.Text = item.FaxNum.ToString();
                                }
                                else
                                {
                                    tbFax.Text = string.Empty;
                                }
                                if (item.CellPhoneNum != null)
                                {
                                    tbMobile.Text = item.CellPhoneNum.ToString();
                                }
                                else
                                {
                                    tbMobile.Text = string.Empty;
                                }
                                if (item.CellPhoneNum != null)
                                {
                                    tbMobile.Text = item.CellPhoneNum.ToString();
                                }
                                else
                                {
                                    tbMobile.Text = string.Empty;
                                }
                                if (item.EmailAddress != null)
                                {
                                    tbEmail.Text = item.EmailAddress.ToString();
                                }
                                else
                                {
                                    tbEmail.Text = string.Empty;
                                }
                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgEmergencyContact.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (item.LastUpdUID != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + item.LastUpdUID.ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (item.LastUpdTS != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.LastUpdTS.ToString().Trim())));//item.GetDataKeyValue("LastUpdTS").ToString();
                                }
                                if (item.BusinessPhone != null)
                                {
                                    tbBusinessPhone.Text = item.BusinessPhone.ToString().Trim();

                                }
                                else
                                {
                                    tbBusinessPhone.Text = string.Empty;
                                }
                                if (item.BusinessFax != null)
                                {
                                    tbBusinessFax.Text = item.BusinessFax.ToString().Trim();

                                }
                                else
                                {
                                    tbBusinessFax.Text = string.Empty;
                                }
                                if (item.CellPhoneNum2 != null)
                                {
                                    tbSecondaryMobile.Text = item.CellPhoneNum2.ToString().Trim();

                                }
                                else
                                {
                                    tbSecondaryMobile.Text = string.Empty;
                                }
                                if (item.OtherPhone != null)
                                {
                                    tbOtherPhone.Text = item.OtherPhone.ToString().Trim();

                                }
                                else
                                {
                                    tbOtherPhone.Text = string.Empty;
                                }
                                if (item.PersonalEmail != null)
                                {
                                    tbPersonalEmail.Text = item.PersonalEmail.ToString().Trim();

                                }
                                else
                                {
                                    tbPersonalEmail.Text = string.Empty;
                                }
                                if (item.OtherEmail != null)
                                {
                                    tbOtherEmail.Text = item.OtherEmail.ToString().Trim();

                                }
                                else
                                {
                                    tbOtherEmail.Text = string.Empty;
                                }
                                if (item.Addr3 != null)
                                {
                                    tbAddress3.Text = item.Addr3.ToString().Trim();

                                }
                                else
                                {
                                    tbAddress3.Text = string.Empty;
                                }
                                if (item.IsInActive != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(item.IsInActive.ToString(), CultureInfo.CurrentCulture);
                                }
                                else 
                                { 
                                    chkInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Emergency Contact Code";
                                lbColumnName2.Text = "Name";
                                lbColumnValue1.Text = item.EmergencyContactCD.ToString();
                                lbColumnValue2.Text = item.FirstName.ToString();
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private FlightPakMasterService.EmergencyContact GetItems(FlightPakMasterService.EmergencyContact objemergencyType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (hdnSave.Value == "Update")
                    {
                        objemergencyType.EmergencyContactID = Convert.ToInt64(hdnECID.Value);
                    }
                    objemergencyType.EmergencyContactCD = tbCode.Text;
                    objemergencyType.LastName = tbLastName.Text;
                    objemergencyType.FirstName = tbFirstName.Text;
                    objemergencyType.MiddleName = tbMiddleName.Text;
                    objemergencyType.Addr1 = tbAddress1.Text;
                    objemergencyType.Addr2 = tbAddress2.Text;
                    objemergencyType.CityName = tbCity.Text;
                    objemergencyType.StateName = tbState.Text;
                    objemergencyType.PostalZipCD = tbPostal.Text;
                    if (!String.IsNullOrEmpty(hdnCountry.Value))
                    {
                        objemergencyType.CountryID = Convert.ToInt64(hdnCountry.Value);
                    }
                    objemergencyType.PhoneNum = tbPhone.Text;
                    objemergencyType.FaxNum = tbFax.Text;
                    objemergencyType.CellPhoneNum = tbMobile.Text;
                    objemergencyType.EmailAddress = tbEmail.Text;
                    objemergencyType.IsDeleted = false;
                    objemergencyType.BusinessPhone = tbBusinessPhone.Text;
                    objemergencyType.BusinessFax = tbBusinessFax.Text;
                    objemergencyType.CellPhoneNum2 = tbSecondaryMobile.Text;
                    objemergencyType.OtherPhone = tbOtherPhone.Text;
                    objemergencyType.PersonalEmail = tbPersonalEmail.Text;
                    objemergencyType.OtherEmail = tbOtherEmail.Text;
                    objemergencyType.Addr3 = tbAddress3.Text;
                    objemergencyType.IsInActive = chkInactive.Checked;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return objemergencyType;
            }
        }
        #region "Grid events"
        protected void dgEmergencyContact_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        protected void dgEmergencyContact_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetEmergencyListInfo();
                            List<FlightPakMasterService.GetEmergencyContact> EmergencyContactList = new List<FlightPakMasterService.GetEmergencyContact>();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                EmergencyContactList = ObjRetVal.EntityList;
                            }
                            dgEmergencyContact.DataSource = EmergencyContactList;
                            Session["EmergencyCodes"] = EmergencyContactList;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        protected void dgEmergencyContact_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                // Lock the record
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.EmergencyContact, Convert.ToInt64(Session["EmergencyContactID"]));
                                    Session["IsEditLockEmergencyCatalog"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.EmergencyContact);
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    DisableLinks();
                                    RadAjaxManager1.FocusControl(tbLastName.ClientID); //tbLastName.Focus();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgEmergencyContact.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                GridEnable(true, false, false);
                                DisableLinks();
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        protected void dgEmergencyContact_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if ((Session["EmergencyContactID"] != null))
                        {
                            if (CheckCountryExist() == false)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient ObjEmergencytypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.EmergencyContact objemergencyType = new FlightPakMasterService.EmergencyContact();
                                    objemergencyType = GetItems(objemergencyType);
                                    var objRetVal = ObjEmergencytypeService.UpdateEmergencyType(objemergencyType);
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.EmergencyContact, Convert.ToInt64(Session["EmergencyContactID"]));
                                        }
                                        Session["IsEditLockEmergencyCatalog"] = "False";
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        dgEmergencyContact.Rebind();
                                        SelectItem();
                                        ReadOnlyForm();

                                        ShowSuccessMessage();
                                        EnableLinks();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.EmergencyContact);
                                    }
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        protected void dgEmergencyContact_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            bool ValidatePage = false;
                            string emergencyContactCD=string.Empty;
                            if (CheckEmergencyContactExist() == false && CheckCountryExist() == false)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.EmergencyContact objemergencyType = new FlightPakMasterService.EmergencyContact();
                                    objemergencyType = GetItems(objemergencyType);
                                    var objRetVal = objService.AddEmergencyType(objemergencyType);
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        GridEnable(true, true, true);
                                        DefaultSelection(true);
                                        emergencyContactCD=objemergencyType.EmergencyContactCD;
                                        ShowSuccessMessage();
                                        EnableLinks();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.EmergencyContact);
                                    }
                                }
                                if (IsPopUp)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); var dialog1 = oWnd.get_windowManager().getWindowByName('rdEmergencyContact');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.reloadEmergencyPopup('" + emergencyContactCD + "');oWnd.close();", true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.EmergencyContact, Convert.ToInt64(Session["EmergencyContactID"]));
                    }
                }
            }
        }
        protected void dgEmergencyContact_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            if ((Session["EmergencyContactID"] != null))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objEmergencyTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.EmergencyContact objemergencyType = new FlightPakMasterService.EmergencyContact();
                                    GridDataItem item = dgEmergencyContact.SelectedItems[0] as GridDataItem;
                                    objemergencyType.EmergencyContactID = Convert.ToInt64((item).GetDataKeyValue("EmergencyContactID").ToString());
                                    objemergencyType.EmergencyContactCD = (item).GetDataKeyValue("EmergencyContactCD").ToString();
                                    objemergencyType.FirstName = (item).GetDataKeyValue("FirstName").ToString();
                                    objemergencyType.IsDeleted = true;
                                    //Lock the record
                                    var returnValue = CommonService.Lock(EntitySet.Database.EmergencyContact, Convert.ToInt64(Session["EmergencyContactID"]));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.EmergencyContact);
                                        return;
                                    }
                                    objEmergencyTypeService.DeleteEmergencyType(objemergencyType);

                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.EmergencyContact, Convert.ToInt64(Session["EmergencyContactID"]));
                    }
                }
            }
        }
        protected void dgEmergencyContact_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            GridDataItem item = dgEmergencyContact.SelectedItems[0] as GridDataItem;
                            Session["EmergencyContactID"] = item["EmergencyContactID"].Text;
                            ReadOnlyForm();
                            GridEnable(true, true, true);
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                    }
                }
            }
        }
        protected void dgEmergencyContact_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgEmergencyContact.ClientSettings.Scrolling.ScrollTop = "0";
            EmergencyContactPageNavigated = true;
        }
        protected void dgEmergencyContact_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (EmergencyContactPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgEmergencyContact, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        #endregion
        /// <summary>
        /// Datagrid Item Created for Flight Purpose Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReloadGrid()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    dgEmergencyContact.Rebind();
                    dgEmergencyContact.SelectedIndexes.Add(0);
                    if (dgEmergencyContact.MasterTableView.Items.Count > 0)
                    {
                        Session["EmergencyContactID"] = dgEmergencyContact.Items[0].GetDataKeyValue("EmergencyContactID").ToString();
                        ReadOnlyForm();
                        GridEnable(true, true, true);
                        GridDataItem Item = (GridDataItem)dgEmergencyContact.SelectedItems[0];
                        Label lblUser;
                        lblUser = (Label)dgEmergencyContact.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lblUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                        }
                    }
                    else
                    {
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((e.Initiator.ID.IndexOf("btnSaveChanges") > -1))
                        {
                            e.Updated = dgEmergencyContact;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgEmergencyContact.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgEmergencyContact.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            //dgEmergencyContact.Rebind();
                            GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        /// <summary>
        /// Cancel Emergency Contact Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.EmergencyContact, Convert.ToInt64(Session["EmergencyContactID"]));
                            //Session.Remove("EmergencyContactID");
                            DefaultSelection(false);
                            EnableLinks();
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        protected void lnkBtnInsert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgEmergencyContact.SelectedIndexes.Clear();
                        DisplayInsertForm();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void tbCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((tbCountry.Text != null) && (tbCountry.Text != string.Empty))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                //To check for unique metro code
                                var CountryValue = CountryService.GetCountryMasterList().EntityList.Where(x => x.CountryCD.ToString().ToUpper().Trim().Equals(tbCountry.Text.ToUpper().Trim()));
                                if (CountryValue.Count() == 0 || CountryValue == null)
                                {
                                    cvCountry.IsValid = false;
                                    RadAjaxManager1.FocusControl(tbCountry.ClientID); //tbCountry.Focus();
                                }
                                else
                                {
                                    List<FlightPakMasterService.Country> CompanyList = new List<FlightPakMasterService.Country>();
                                    CompanyList = (List<FlightPakMasterService.Country>)CountryValue.ToList();
                                    hdnCountry.Value = CompanyList[0].CountryID.ToString();
                                    tbCountry.Text = CompanyList[0].CountryCD;
                                }
                            }
                        }
                        else
                        {
                            hdnCountry.Value = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }

        private bool CheckCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                if ((tbCountry.Text != null) && (tbCountry.Text != string.Empty))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        //To check for unique metro code
                        var CountryValue = CountryService.GetCountryMasterList().EntityList.Where(x => x.CountryCD.ToString().ToUpper().Trim().Equals(tbCountry.Text.ToUpper().Trim()));
                        if (CountryValue.Count() == 0 || CountryValue == null)
                        {
                            cvCountry.IsValid = false;
                            ReturnValue = true;
                            RadAjaxManager1.FocusControl(tbCountry.ClientID); //tbCountry.Focus();
                        }
                        else
                        {
                            List<FlightPakMasterService.Country> CompanyList = new List<FlightPakMasterService.Country>();
                            CompanyList = (List<FlightPakMasterService.Country>)CountryValue.ToList();
                            hdnCountry.Value = CompanyList[0].CountryID.ToString();
                            tbCountry.Text = CompanyList[0].CountryCD;
                            ReturnValue = false;
                        }
                    }
                }
                else
                {
                    hdnCountry.Value = string.Empty;
                }
                return ReturnValue;
            }
        }
        /// <summary>
        /// To check unique Delay Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objEmergencysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (Session["EmergencyCodes"] != null)
                                {
                                    List<GetEmergencyContact> EmergencyContactList = new List<GetEmergencyContact>();
                                    EmergencyContactList = ((List<GetEmergencyContact>)Session["EmergencyCodes"]).Where(x => x.EmergencyContactCD.ToString().ToUpper().Trim() == tbCode.Text.ToString().ToUpper().Trim()).ToList<GetEmergencyContact>();
                                    if (EmergencyContactList.Count != 0)
                                    {
                                        cvEmergencyContactCode.IsValid = false;
                                        RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                    }
                                    else
                                    {
                                        RadAjaxManager1.FocusControl(tbFirstName.ClientID); //tbFirstName.Focus();
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        private bool CheckEmergencyContactExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                if (tbCode.Text != null)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objEmergencysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        if (Session["EmergencyCodes"] != null)
                        {
                            List<GetEmergencyContact> EmergencyContactList = new List<GetEmergencyContact>();
                            //EmergencyContactList = ((List<GetEmergencyContact>)Session["EmergencyCodes"]).Where(x => x.EmergencyContactCD.ToString().ToUpper().Trim().Contains(tbCode.Text.ToString().ToUpper().Trim())).ToList<GetEmergencyContact>();
                            EmergencyContactList = ((List<GetEmergencyContact>)Session["EmergencyCodes"]).Where(x => x.EmergencyContactCD.ToString().ToUpper().Trim() == tbCode.Text.ToString().ToUpper().Trim()).ToList<GetEmergencyContact>();
                            if (EmergencyContactList.Count != 0)
                            {
                                cvEmergencyContactCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                ReturnValue = true;
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbFirstName.ClientID); //tbFirstName.Focus();
                                ReturnValue = false;
                            }
                        }
                    }
                }
                return ReturnValue;
            }
        }

        protected void dgEmergencyContact_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            Item["FirstName"].Text = Item.GetDataKeyValue("LastName").ToString() + ", " + Item.GetDataKeyValue("FirstName").ToString() + " " + Item.GetDataKeyValue("MiddleName").ToString();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetEmergencyContact> lstEmergencyContact = new List<FlightPakMasterService.GetEmergencyContact>();
                if (Session["EmergencyCodes"] != null)
                {
                    lstEmergencyContact = (List<FlightPakMasterService.GetEmergencyContact>)Session["EmergencyCodes"];
                }
                if (lstEmergencyContact.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstEmergencyContact = lstEmergencyContact.Where(x => x.IsInActive == false).ToList<GetEmergencyContact>(); }
                    dgEmergencyContact.DataSource = lstEmergencyContact;
                    if (IsDataBind)
                    {
                        dgEmergencyContact.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgEmergencyContact.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var EmergencyValue = FPKMstService.GetEmergencyListInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, EmergencyValue);
            List<FlightPakMasterService.GetEmergencyContact> filteredList = GetFilteredList(EmergencyValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.EmergencyContactID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgEmergencyContact.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgEmergencyContact.CurrentPageIndex = PageNumber;
            dgEmergencyContact.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetEmergencyContact EmergencyValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["EmergencyContactID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = EmergencyValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().EmergencyContactID;
                Session["EmergencyContactID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetEmergencyContact> GetFilteredList(ReturnValueOfGetEmergencyContact EmergencyValue)
        {
            List<FlightPakMasterService.GetEmergencyContact> filteredList = new List<FlightPakMasterService.GetEmergencyContact>();

            if (EmergencyValue.ReturnFlag)
            {
                filteredList = EmergencyValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetEmergencyContact>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            if (!IsPostBack && IsPopUp)
            {
                //Hide Controls
                dgEmergencyContact.Visible = false;
                chkSearchActiveOnly.Visible = false;
                if (IsAdd)
                {
                    (dgEmergencyContact.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                }
                else
                {
                    (dgEmergencyContact.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                }
            }
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgEmergencyContact.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgEmergencyContact.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
    }
}
