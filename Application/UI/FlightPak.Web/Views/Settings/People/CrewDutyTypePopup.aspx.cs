﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data.SqlClient;
using System.Globalization;
using System.Data;
using System.Drawing;
using System.Configuration;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewDutyTypePopup : BaseSecuredPage
    {
        private string DutyTypeID;
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["DutyTypeID"] != null)
            {
                if (Request.QueryString["DutyTypeID"].ToString().Trim() == "1")
                {
                    dgCrewDutyType.AllowMultiRowSelection = true;
                }
                else
                {
                    dgCrewDutyType.AllowMultiRowSelection = false;
                }
            }
            else
            {
                dgCrewDutyType.AllowMultiRowSelection = false;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Added for Reassign the Selected Value and highlight the specified row in the Grid            
                        if (Request.QueryString["DutyTypeID"] != null)
                        {
                            DutyTypeID = Request.QueryString["DutyTypeID"].Trim();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }

        }
        /// <summary>
        /// Bind Metro Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>

        protected void dgCrewDutyType_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewDutyTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CrewDutyTypeVal = CrewDutyTypeService.GetCrewDutyTypeList();
                            if (CrewDutyTypeVal.ReturnFlag == true)
                            {
                                dgCrewDutyType.DataSource = CrewDutyTypeVal.EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }

        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCrewDutyType;
                        if (!IsPostBack)
                            SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }

        }
        protected void dgCrewDutyType_ItemDataBound(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                            dataItem.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ForeGrndCustomColor"), CultureInfo.CurrentCulture));
                            dataItem.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackgroundCustomColor"), CultureInfo.CurrentCulture));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }

        }
        /// <summary>
        /// To highlight the selected item which they have selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!string.IsNullOrEmpty(DutyTypeID))
                    {
                        foreach (GridDataItem item in dgCrewDutyType.MasterTableView.Items)
                        {
                            if (item["DutyTypeID"].Text.Trim() == DutyTypeID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (dgCrewDutyType.MasterTableView.Items.Count > 0)
                        {
                            dgCrewDutyType.SelectedIndexes.Add(0);

                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }

        protected void dgCrewDutyType_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    string resolvedurl = string.Empty;
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                GridDataItem item = dgCrewDutyType.SelectedItems[0] as GridDataItem;
                                Session["SelectedDutyTypeID"] = item["DutyTypeID"].Text;
                                TryResolveUrl("/Views/Settings/People/CrewDutyTypes.aspx?IsPopup=", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "ProcessPopupAdd('AccountsCatalog.aspx?IsPopup=', 'Add Accounts', '1000','500', '50', '50');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radCrewDutyTypeCRUDPopup');", true);
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/People/CrewDutyTypes.aspx?IsPopup=Add", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radCrewDutyTypeCRUDPopup');", true);
                                break;
                            case RadGrid.FilterCommandName:
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
		                        Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }

        protected void dgCrewDutyType_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewDutyType, Page.Session);
        }

        protected void dgCrewDutyType_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string dutyCode = string.Empty;
                string dutyId = string.Empty;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = dgCrewDutyType.SelectedItems[0] as GridDataItem;
                        dutyCode = item.GetDataKeyValue("DutyTypeCD").ToString().ToUpper().Trim();
                        dutyId = item.GetDataKeyValue("DutyTypeID").ToString().ToUpper().Trim();
                        if (dutyCode == "F" || dutyCode == "G" || dutyCode == "R" || dutyCode == "SM")
                        {
                            ShowLockAlertPopup("R, F, G and SM Codes Are Reserved For Internal Use by FlightPak.", "Crew Duty Type");
                        }
                        else
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CrewDutyTypeTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.CrewDutyType CrewDutyType = new FlightPakMasterService.CrewDutyType();

                                CrewDutyType.DutyTypeID = Convert.ToInt64(item.GetDataKeyValue("DutyTypeID").ToString());
                                CrewDutyType.DutyTypeCD = item.GetDataKeyValue("DutyTypeCD").ToString();
                                CrewDutyType.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.CrewDutyType, Convert.ToInt64(Session["SelectedDutyTypeID"]));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        dgCrewDutyType.Rebind();
                                        ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.CrewDutyType);
                                        return;
                                    }
                                }
                                CrewDutyTypeTypeService.DeleteCrewDutyType(CrewDutyType);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                            }
                        }



                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.CrewDutyType, Convert.ToInt64(Session["SelectedDutyTypeID"]));
                    }
                }
            }

        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyType_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["DutyTypeID"] != null)
                {
                    if (Request.QueryString["DutyTypeID"].ToString().Trim() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }
    }
}