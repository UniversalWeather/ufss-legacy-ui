﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class TravelCoordinatorPopup : BaseSecuredPage
    {
        private string TravelCoordCD;
        private ExceptionManager exManager;
        bool CorporateRequest = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Added for Reassign the Selected Value and highlight the specified row in the Grid           
                        if (!string.IsNullOrEmpty(Request.QueryString["TravelCoordCD"]))
                        {
                            TravelCoordCD = Request.QueryString["TravelCoordCD"].ToUpper().Trim();
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["FromPage"]))
                        {
                            CorporateRequest = true;
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }

        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgTravelCoordinator_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient TravelCoordinatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ReturnValue = TravelCoordinatorService.GetTravelCoordinatorList();
                            List<FlightPakMasterService.GetTravelCoordinator> TravelCoordinatorList = new List<FlightPakMasterService.GetTravelCoordinator>();
                            if (ReturnValue.ReturnFlag == true)
                            {
                                if (!CorporateRequest)
                                {
                                    TravelCoordinatorList = ReturnValue.EntityList;
                                }
                                else
                                {
                                    if (UserPrincipal.Identity._isSysAdmin == false && UserPrincipal.Identity._travelCoordID != null)
                                    {
                                        TravelCoordinatorList = ReturnValue.EntityList.Where( x => x.TravelCoordinatorID == Convert.ToInt64(UserPrincipal.Identity._travelCoordID)).ToList();
                                    }
                                    else
                                    {
                                        TravelCoordinatorList = ReturnValue.EntityList;
                                    }
                                }

                            }
                            dgTravelCoordinator.DataSource = TravelCoordinatorList;
                            Session["TravelCoordinatorListPopUp"] = TravelCoordinatorList;
                            DoSearchFilter(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }

        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgTravelCoordinator;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }

        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(TravelCoordCD))
                {
                    foreach (GridDataItem item in dgTravelCoordinator.MasterTableView.Items)
                    {
                        if (item["TravelCoordCD"].Text.Trim().ToUpper() == TravelCoordCD)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgTravelCoordinator.MasterTableView.Items.Count > 0)
                    {
                        dgTravelCoordinator.SelectedIndexes.Add(0);

                    }
                }

            }

        }

        protected void chkHomeBase_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }

        protected bool DoSearchFilter(bool IsDataBind)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objTravelCoordinatorCatalogService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                List<FlightPakMasterService.GetTravelCoordinator> TravelCoordinatorList = new List<FlightPakMasterService.GetTravelCoordinator>();
                if (Session["TravelCoordinatorListPopUp"] != null)
                {
                    TravelCoordinatorList = (List<FlightPakMasterService.GetTravelCoordinator>)Session["TravelCoordinatorListPopUp"];
                }
                if (TravelCoordinatorList.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { TravelCoordinatorList = TravelCoordinatorList.Where(x => x.IsInactive == false).ToList<GetTravelCoordinator>(); }
                    if (chkHomeBase.Checked == true) { TravelCoordinatorList = TravelCoordinatorList.Where(x => x.HomebaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetTravelCoordinator>(); }
                    dgTravelCoordinator.DataSource = TravelCoordinatorList.OrderBy(x => x.TravelCoordCD);
                }
                if (IsDataBind)
                {
                    dgTravelCoordinator.DataBind();
                }
                return false;
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            DoSearchFilter(true);
        }

        protected void dgTravelCoordinator_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgTravelCoordinator_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgTravelCoordinator, Page.Session);
        }
    }
}
