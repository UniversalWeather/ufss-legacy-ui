﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head runat="server">
    <title>Aircraft Emergency Contact</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>


    <script type="text/javascript">
        var selectedEmergencyCD = "";
        var jqgridTableId = "#dgEmergencyList";
        var isopenlookup = false;
        $(document).ready(function () {
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            selectedEmergencyCD = decodeURI(getQuerystring("EmergencyContactCD", ""));
            if (selectedEmergencyCD != "") {
                isopenlookup = true;
            }

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if (selr != null) {
                    var selectedRow = $(jqgridTableId).getRowData(selr);
                    returnToParent(selectedRow);
                }
                else {
                    showMessageBox('Please select a contact name.', popupTitle);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/Views/Settings/People/EmergencyContactCatalog.aspx?IsPopup=Add", popupTitle, 1100, 798, jqgridTableId);
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                emergencyContactID = rowData['EmergencyContactID'];
                emergencyContactCD = rowData['EmergencyContactCD'];
                var widthDoc = $(document).width();
                if (emergencyContactID != undefined && emergencyContactID != null && emergencyContactID != '' && emergencyContactID != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a record.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        dataType: 'json',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=EmergencyContact&emergencyContactID=' + emergencyContactID,
                        contentType: 'application/json',
                        success: function (data) {
                            verifyReturnedResultForJqgrid(data);
                            $(jqgridTableId).trigger('reloadGrid');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (!IsNullOrEmptyOrUndefined(content)) {
                                if (content.indexOf('404'))
                                    showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                            }
                        }
                    });

                }
            }

            $("#recordEdit").click(function () {

                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                emergencyContactID = rowData['EmergencyContactID'];
                var widthDoc = $(document).width();
                if (emergencyContactID != undefined && emergencyContactID != null && emergencyContactID != '' && emergencyContactID != 0) {
                    popupwindow("/Views/Settings/People/EmergencyContactCatalog.aspx?IsPopup=&EmergencyContactID=" + emergencyContactID, popupTitle, 1100, 798, jqgridTableId);
                }
                else {
                    showMessageBox('Please select a Emergency Contact code.', popupTitle);
                }
                return false;
            });

            var fetchActive = $("#chkSearchActiveOnly").is(":checked");
            jQuery(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {

                        if (postData.filters === undefined) postData.filters = null;

                    }

                    if (postData.filters != null) {
                        $j = jQuery.parseJSON(postData.filters);
                        if ($j.rules.length > 0) {
                            if ($j.rules[0].field == "FirstName") {
                                $j.rules.push({ data: $j.rules[0].data, field: "LastName", op: "bw" });
                                $j.rules.push({ data: $j.rules[0].data, field: "MiddleName", op: "bw" });
                            }
                        }
                    }
                    postData.sendCustomerId = false;
                    postData.apiType = 'fss';
                    postData.method = 'EmergencyContactList';
                    postData.emergencyContactId = 0;
                    postData.emergencyContactCd = selectedEmergencyCD;
                    postData.fetchActiveOnly = fetchActive;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 300,
                width: 550,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: false,
                pager: "#pg_gridPager",
                colNames: ['', 'Code', 'Name', '', ''],
                colModel: [
                    { name: 'EmergencyContactID', index: 'EmergencyContactID', hidden: true },
                    { name: 'EmergencyContactCD', index: 'EmergencyContactCD', width: 80, align: "left", key: true },
                     {
                         name: 'FirstName', index: 'FirstName', width: 320, align: "left", formatter: function (cellvalue, options, rowObject) {
                             return rowObject.LastName + ", " + rowObject.FirstName + " " + rowObject.MiddleName;;
                         }
                     },
                    { name: 'LastName', index: 'LastName', hidden: true },
                    { name: 'MiddleName', index: 'MiddleName', hidden: true }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    selectedEmergencyCD = rowData.EmergencyContactCD;
                    returnToParent(rowData);
                    return;
                },
                onSelectRow: function (rowid, status, e) {

                    var rowData = $(this).jqGrid("getRowData", rowid);
                    var lastSel = rowData['EmergencyContactID'];//replace name with any column
                    selectedEmergencyCD = rowData.EmergencyContactCD;
                    if (rowid !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = rowid;
                    }

                },
                afterInsertRow: function (rowid, rowObject) {

                    if ($.trim(rowObject.EmergencyContactCD.toLowerCase()) == $.trim(selectedEmergencyCD.toLowerCase())) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        jQuery(jqgridTableId).setSelection(rowid, true);
                    }
                },
                loadComplete: function () {
                   
                }
            });
            $("#pagesizebox").insertBefore('.ui-paging-info');


            $("#dgEmergencyList").jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true, ignoreCase: true });

            $('#chkSearchActiveOnly').change(function () {
                reloadEmergencyPopup();
                return false;
            });

        });

        function reloadEmergencyPopup(emergencyContactCD) {
            if (emergencyContactCD != undefined && !IsNullOrEmpty(emergencyContactCD)) {
                selectedEmergencyCD = emergencyContactCD;
                isopenlookup = true;
            }
            $(jqgridTableId).jqGrid().trigger('reloadGrid');
        }
        function returnToParent(selectedRow) {
            var oArg = new Object();
                        var cell1 = selectedRow.EmergencyContactCD;
                        var cell2 = selectedRow.FirstName;
                        var cell3 = selectedRow.EmergencyContactCD;
                    
                        var oArg = new Object();
                        if (selectedRow != null) {
                            oArg.EmergencyContactCD = cell1;
                            oArg.FirstName = cell2;
                            oArg.EmergencyContactID = cell3;
                        }
                        else {
                            oArg.EmergencyContactCD = "";
                            oArg.FirstName = "";
                            oArg.EmergencyContactID = "";
                        }
                        var oWnd = GetRadWindow();
                        if (oArg) {
                            oWnd.close(oArg);
                        }
        }

        function Close() {
                        GetRadWindow().Close();
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="jqgrid emergency_popup">
            <table class="box1">
                <tr>
                    <td>
                        <div class="header_inn_wrapper">
                            <div class="header_wrapper_left">
                                <ul>
                                    <li>
                                        <input type="checkbox" title="Active Only" value="Active Only" id="chkSearchActiveOnly"/>
                                        <label class="left_label" for="chkSearchActiveOnly">Active Only</label>
                                    </li>
                                    <li>
                                        <div class="search_btn" style="display:none">
                                            <input type="submit" class="button" id="btnSearch" value="Search" name="btnSearch" onclick="reloadEmergencyPopup(); return false;">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                         </div>
                </tr>
                <tr>
                    <td>
                        <table id="dgEmergencyList" class="table table-striped table-hover table-bordered"></table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="grid_icon">
                            <div role="group" id="pg_gridPager" class="footer_type2"></div>
                            <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                            <div id="pagesizebox">
                                <span>Page Size:</span>
                                <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                            </div>
                        </div>
                        <div style="padding: 5px 5px; text-align: right;">
                            <input id="btnSubmit" data-dismiss="modal" class="button okButton" value="OK" type="button" />
                        </div>
                    </td>
                </tr>

            </table>

        </div>
    </form>


</body>
</html>
