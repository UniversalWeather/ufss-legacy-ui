﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using FlightPak.Common;
using FlightPak.Common.Constants;

//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewRosterReportsPopup : BaseSecuredPage
    {
        private string CrewCD;
        List<string> Crewcode = new List<string>();
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["CrewCD"] != null)
                        {
                            CrewCD = Request.QueryString["CrewCD"];
                        }
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }

        protected void Cancel_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //lbScript.Visible = true;
                        //lbScript.Text = "<script type='text/javascript'>Close()</" + "script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "Close();", true);
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
    }
}