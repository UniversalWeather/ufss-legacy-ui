﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <title>Crew Duty Rules</title>
    <link href="/Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="/Scripts/Common.js"></script>
<link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
   <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <style>
        .CrewDutyPop .ui-jqgrid-htable{width: 528px!important;}
        .CrewDutyPop #gridCrewDutyRules{width:528px!important;}
    </style>

    <script type="text/javascript">
        var selectedRowData = null;
        var jqgridTableId = '#gridCrewDutyRules';
        var crewdutyruleId = null;
        var crewdutyrulecd = null;
        var ismultiSelect = false;
        var selectedRowMultipleCrew = "";
        var crewDuty = [];
        var isopenlookup = false;
        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            var selectedRowData = decodeURI(getQuerystring("CrewDutyRuleCD", ""));
            selectedRowMultipleCrew = $.trim(decodeURI(getQuerystring("CrewDutyRuleCD", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultipleCrew);
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            if (ismultiSelect) {
                if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                    selectedRowData = jQuery("#hdnselectedfccd").value;
                }
                crewDuty = selectedRowData.split(',');
            }

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultipleCrew.length > 0)) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    var Crewdutyrulecd = rowData['Crewdutyrulecd'];
                    returnToParent(rowData,0);
                }
                else {
                    showMessageBox('Please select a crew duty code.', popupTitle);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/Views/Settings/People/CrewDutyRulesCatalog.aspx?IsPopup=Add", popupTitle, 1100,798,jqgridTableId);
                return false;
            });



            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                crewdutyruleId = rowData['Crewdutyrulesid'];
                crewdutyrulecd = rowData['Crewdutyrulecd'];
                var widthDoc = $(document).width();

                if (crewdutyruleId != undefined && crewdutyruleId != null && crewdutyruleId != '' && crewdutyruleId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a record', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=CrewDutyRules&crewdutyruleId=' + crewdutyruleId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (content.indexOf('404'))
                                showMessageBox('This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.', popupTitle);
                        }
                    });

                }
            }
            

            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                crewdutyruleId = rowData['Crewdutyrulesid'];
                var widthDoc = $(document).width();
                if (crewdutyruleId != undefined && crewdutyruleId != null && crewdutyruleId != '' && crewdutyruleId != 0) {
                    popupwindow("/Views/Settings/People/CrewDutyRulesCatalog.aspx?IsPopup=&CrewDutyRuleId=" + crewdutyruleId, popupTitle, 1100, 798,jqgridTableId);
                }
                else {
                    showMessageBox('Please select a record', popupTitle);
                }
                return false;
            });


            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'crewdutyrules';
                    postData.showInactive = $('#chkSearchActiveOnly').is(':checked') ? false : true;
                    if (self.parent.GetLastInsertedID.Id != undefined && !IsNullOrEmpty(self.parent.GetLastInsertedID.Id)) {
                        selectedRowData = self.parent.GetLastInsertedID.Id;
                        isopenlookup = true;
                        selectedRowMultipleCrew = selectedRowData;
                        self.parent.GetLastInsertedID.Id = "";
                    }
                    postData.crewDutyRuleCD = function () { if (ismultiSelect && selectedRowData.length > 0) { return crewDuty[0]; } else { return selectedRowData; } };
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 300,
                width: 528,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                colNames: ['CrewDutyRulesID', 'Code', 'Description', 'FAR Rule'],
                colModel: [
                    { name: 'Crewdutyrulesid', index: 'Crewdutyrulesid', key: true, hidden: true },
                    { name: 'Crewdutyrulecd', index: 'Crewdutyrulecd', width: 50 },
                    { name: 'Crewdutyrulesdescription', index: 'Crewdutyrulesdescription', width: 150 },
                    { name: 'Fedaviatregnum', index: 'Fedaviatregnum', width: 50 }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData,1);

                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    selectedRowMultipleCrew = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultipleCrew, rowData.Crewdutyrulecd);

                }, onSelectAll: function (id, status) {
                    selectedRowMultipleCrew = JqgridSelectAll(selectedRowMultipleCrew, jqgridTableId, id, status, 'Crewdutyrulecd');
                    jQuery("#hdnselectedfccd").val(selectedRowMultipleCrew);
                },
                afterInsertRow: function (rowid, rowObject) {
                    JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultipleCrew, rowid, rowObject.Crewdutyrulecd, jqgridTableId);
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                }
            });

            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

            $("#pagesizebox").insertBefore('.ui-paging-info');
        });


        function reloadCrewDutyRules() {
            $(jqgridTableId).jqGrid().trigger('reloadGrid');
        }

        function returnToParent(rowData, one) {

            selectedRowMultipleCrew = jQuery.trim(selectedRowMultipleCrew);
            if (selectedRowMultipleCrew.lastIndexOf(",") == selectedRowMultipleCrew.length - 1)
                selectedRowMultipleCrew = selectedRowMultipleCrew.substr(0, selectedRowMultipleCrew.length - 1);

            var oArg = new Object();

            if (one == 0) {
                oArg.CrewDutyRuleCD = selectedRowMultipleCrew;
            }
            else {
                oArg.CrewDutyRuleCD = rowData["Crewdutyrulecd"];
            }
            oArg.CrewDutyRulesDescription = rowData["Crewdutyrulesdescription"];
            
            oArg.CrewDutyRulesID = rowData["Crewdutyrulesid"];
            oArg.FedAviatRegNum = rowData["Fedaviatregnum"];

            oArg.Arg1 = oArg.CrewDutyRuleCD;
            oArg.CallingButton = "Crewdutyrulecd";

            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }

        function rebindgrid() {
            $(jqgridTableId).jqGrid().trigger('reloadGrid');
        }
    </script>

</head>
<body>
    <form id="form1">      
        <div class="jqgrid CrewDutyPop">
            <div>
                <table class="box1">

               <tr>
                <td><input type="hidden" id="hdnselectedfccd" value=""/>
                    <table>
                        <tbody><tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <input type="checkbox" name="chkSearchActiveOnly" id="chkSearchActiveOnly"/><label for="chkSearchActiveOnly">Active Only</label></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <input type="submit"  class="button" id="btnSearch" value="Search" name="btnSearch" onclick="reloadCrewDutyRules(); return false";/>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>

                    <tr>
                        <td>
                            <table id="gridCrewDutyRules" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                               <div id="pagesizebox">
                               <span>Size:</span>
                                <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div> 
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </form>
</body>
</html>
