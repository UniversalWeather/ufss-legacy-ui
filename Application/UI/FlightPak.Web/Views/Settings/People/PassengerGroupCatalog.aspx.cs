﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Collections;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class PassengerGroupCatalog : BaseSecuredPage    //System.Web.UI.Page
    {
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private bool PassengerGroupPageNavigated = false;
        private Int64 CustomerID = 0;
        private bool _selectLastModified = false;
        Int64 PassengerGroupID;
        long? CustomerId;
          
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPaxGroup, dgPaxGroup, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgPaxGroup.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewPassengerGroupCatalog);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgPaxGroup.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgPaxGroup.Rebind();
                    }
                    if (dgPaxGroup.MasterTableView.Items.Count > 0)
                    {
                        
                        //if (dgPaxGroup.SelectedItems.Count > 0)
                        //{
                        //    GridDataItem Item = dgPaxGroup.SelectedItems[0] as GridDataItem;
                        //    Item.Selected = false;
                        //}
                        //if (!IsPostBack)
                        //{
                        //    Session["PassengerGroupID"] = null;
                        //}
                        if (Session["PassengerGroupID"] == null)
                        {
                            dgPaxGroup.SelectedIndexes.Add(0);
                            Session["PassengerGroupID"] = dgPaxGroup.Items[0].GetDataKeyValue("PassengerGroupID").ToString();
                        }
                        if (dgPaxGroup.SelectedIndexes.Count == 0)
                            dgPaxGroup.SelectedIndexes.Add(0);
                       
                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["PassengerGroupID"] != null)
                    {
                        string ID = Session["PassengerGroupID"].ToString();
                        foreach (GridDataItem Item in dgPaxGroup.MasterTableView.Items)
                        {
                            if (Item["PassengerGroupID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl = (LinkButton)dgPaxGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton delCtl = (LinkButton)dgPaxGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton editCtl = (LinkButton)dgPaxGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddPassengerGroupCatalog))
                    {
                        insertCtl.Visible = true;
                        if (add)
                        {
                            insertCtl.Enabled = true;
                        }
                        else
                        {
                            insertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeletePassengerGroupCatalog))
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditPassengerGroupCatalog))
                    {
                        editCtl.Visible = true;
                        if (edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                    if (hdnSave.Value == "Save")
                        ShowHideControls(true);
                    else
                        ShowHideControls(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Edit Button
        /// </summary>
        ///  /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    LoadControlData();
                    EnableForm(true);
                    tbCode.Enabled = false;
                    ShowHideControls(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Function to Enable / Diable the form fields
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = Enable;
                    tbDescription.Enabled = Enable;
                    if (hdnSave.Value == "Save")
                    {
                        if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._homeBaseId != 0)
                        {
                            tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd.ToString().Trim();
                            hdnHomeBase.Value = UserPrincipal.Identity._homeBaseId.ToString().Trim();
                            hdnHomeBaseID.Value = UserPrincipal.Identity._homeBaseId.ToString().Trim();
                        }
                    }
                    tbHomeBase.Enabled = Enable;
                    lstAvailable.Enabled = Enable;
                    tbAvailableFilter.Enabled = Enable;
                    lstSelected.Enabled = Enable;
                    btnHomeBase.Enabled = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChanges.Visible = Enable;
                    //btnNext.Enabled = Enable;
                    //btnPrevious.Enabled = Enable;
                    //btnFirst.Enabled = Enable;
                    //btnLast.Enabled = Enable;
                    chkPaxInactive.Enabled = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear the Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    hdnPassengerGroupID.Value = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    hdnHomeBase.Value = string.Empty;
                    hdnHomeBaseID.Value = string.Empty;
                    lstAvailable.Items.Clear();
                    tbAvailableFilter.Text = string.Empty;
                    lstSelected.Items.Clear();
                    chkPaxInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>        
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Load Selected Item Data into the Form Fields
        /// </summary>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //if (Session["PassengerGroupID"] != null)
                    //{
                    //    foreach (GridDataItem Item in dgPaxGroup.MasterTableView.Items)
                    //    {
                    if (dgPaxGroup.SelectedItems.Count != 0 && Session["PassengerGroupID"] !=null)
                    {
                        //  GridDataItem Item = dgPaxGroup.SelectedItems[0] as GridDataItem;
                    
                        foreach (GridDataItem Item in dgPaxGroup.MasterTableView.Items)
                        {
                            if (Item["PassengerGroupID"].Text.Trim() == Session["PassengerGroupID"].ToString().Trim())
                            {
                                tbCode.Text = Item.GetDataKeyValue("PassengerGroupCD").ToString().Trim();
                                hdnPassengerGroupID.Value = Item.GetDataKeyValue("PassengerGroupID").ToString().Trim();
                                if (Item.GetDataKeyValue("PassengerGroupName") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("PassengerGroupName").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HomeBaseCD") != null)
                                {
                                    tbHomeBase.Text = Item.GetDataKeyValue("HomeBaseCD").ToString().Trim();
                                    hdnHomeBaseID.Value = Item.GetDataKeyValue("HomebaseID").ToString().Trim();
                                    hdnHomeBase.Value = Item.GetDataKeyValue("HomebaseID").ToString().Trim();
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;
                                    hdnHomeBaseID.Value = string.Empty;
                                    hdnHomeBase.Value = string.Empty;
                                }
                                Label lbLastUpdatedUser = (Label)dgPaxGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));// Item.GetDataKeyValue("LastUpdTS").ToString();
                                }
                                //if (dgPaxGroup.SelectedItems.Count > 0)
                                //{
                                //    Item.Selected = false;
                                //}
                                //Item.Selected = true;
                                //break;
                                if (Item.GetDataKeyValue("IsInActive") != null && Item.GetDataKeyValue("IsInActive").ToString().ToUpper().Trim() == "TRUE")
                                {
                                    chkPaxInactive.Checked = true;
                                }
                                else
                                {
                                    chkPaxInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Passenger Group Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["PassengerGroupCD"].Text;
                                lbColumnValue2.Text = Item["PassengerGroupName"].Text;
                            }
                        }
                    }
                    //    }
                    //}
                    LoadSelectedList();
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Load Selected List Items in the Listbox
        /// </summary>
        protected void LoadSelectedList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgPaxGroup.SelectedItems.Count != 0)
                    {
                        GridDataItem Item = dgPaxGroup.SelectedItems[0] as GridDataItem;
                        Int64 PassengerGroupID = Convert.ToInt64(Item.GetDataKeyValue("PassengerGroupID"));//Convert.ToInt64(((GridDataItem)dgPaxGroup.SelectedItems[0]).GetDataKeyValue("PassengerGroupID"));
                        List<FlightPakMasterService.GetPaxAvailableList> PaxAvailableList = new List<FlightPakMasterService.GetPaxAvailableList>();
                        List<FlightPakMasterService.GetPaxSelectedList> PaxSelectedList = new List<FlightPakMasterService.GetPaxSelectedList>();
                        lstAvailable.Items.Clear();
                        lstSelected.Items.Clear();
                        PaxAvailableList = GetAvailableListByCode(PassengerGroupID);
                        
                        
                        //PaxAvailableList = PaxAvailableList.OrderBy(x => x.PassengerName).ThenBy(n => n.PassengerRequestorCD).ToList();

                        PaxSelectedList = GetSelectedListByCode(PassengerGroupID);
                        //PaxSelectedList = PaxSelectedList.OrderBy(x => x.PassengerName).ThenBy(n => n.PassengerRequestorCD).ToList();
                        /*ListItem lstItem;
                        foreach (FlightPakMasterService.GetPaxAvailableList PAvlList in PaxAvailableList)
                        {
                            lstItem = new ListItem(PAvlList.PassengerName.ToString().Trim() + " - " + "(" + PAvlList.PassengerRequestorCD.ToString().Trim() + ")", PAvlList.PassengerRequestorID.ToString());
                            lstAvailable.Items.Add(lstItem);
                            //PaxGroupArray.Add(PAvlList.PassengerRequestorCD);
                            //TempAvlList.Add(PAvlList.PassengerName.ToString().Trim() + " - " + "(" + PAvlList.PassengerRequestorCD.ToString().Trim() + ")");
                        }
                        foreach (FlightPakMasterService.GetPaxSelectedList PSelList in PaxSelectedList)
                        {
                            lstItem = new ListItem(PSelList.PassengerName.ToString().Trim() + " - " + "(" + PSelList.PassengerRequestorCD.ToString().Trim() + ")", PSelList.PassengerRequestorID.ToString());
                            lstSelected.Items.Add(lstItem);
                            //TempSelList.Add(PSelList.PassengerName.ToString().Trim() + " - " + "(" + PSelList.PassengerRequestorCD.ToString().Trim() + ")");
                        }*/
                        lstAvailable.DataSource = PaxAvailableList;
                        lstAvailable.DataTextField = "PassengerNameforList";
                        lstAvailable.DataValueField = "PassengerRequestorID";
                        lstAvailable.DataBind();
                        lstSelected.DataSource = PaxSelectedList;
                        lstSelected.DataTextField = "PassengerNameforList";
                        lstSelected.DataValueField = "PassengerRequestorID";
                        lstSelected.DataBind();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void BindAvailableList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.GetPaxAvailableList> PaxAvailableList = new List<FlightPakMasterService.GetPaxAvailableList>();
                    PaxAvailableList = GetAvailableListByCode(PassengerGroupID);
                        
                    lstAvailable.DataSource = PaxAvailableList;
                    lstAvailable.DataTextField = "PassengerNameforList";
                    lstAvailable.DataValueField = "PassengerRequestorID";
                    lstAvailable.DataBind();
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Function to Get Form Itesm
        /// </summary>
        /// <returns></returns>
        private PassengerGroup GetItems(FlightPakMasterService.PassengerGroup oPassengerGroup)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassengerGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (hdnSave.Value == "Update")
                    {
                        oPassengerGroup.PassengerGroupID = Convert.ToInt64(hdnPassengerGroupID.Value);
                    }
                    oPassengerGroup.PassengerGroupCD = tbCode.Text;
                    oPassengerGroup.PassengerGroupName = tbDescription.Text;
                    if (!string.IsNullOrEmpty(hdnHomeBaseID.Value))
                    {
                        oPassengerGroup.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value); //tbHomeBase.Text;
                    }
                    oPassengerGroup.IsDeleted = false;
                    oPassengerGroup.IsInActive = chkPaxInactive.Checked;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return oPassengerGroup;
            }
        }
        /// <summary>
        /// Delete from [PassengerGroupOrder] Table, based on PaxGroup Code
        /// </summary>
        /// <param name="PassengerRequestorID">Pass Passenger Code</param>
        /// <param name="PassengerGroupID">Pass Passenger Group Code</param>
        protected void DeleteSeletedItem(Int64 PassengerRequestorID, Int64 PassengerGroupID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerGroupID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient PaxGroupService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        FlightPakMasterService.PassengerGroupOrder PaxGroupOrder = new FlightPakMasterService.PassengerGroupOrder();
                        PaxGroupOrder.PassengerRequestorID = PassengerRequestorID;
                        PaxGroupOrder.PassengerGroupID = PassengerGroupID;
                        PaxGroupService.DeletePaxGroupOrder(PaxGroupOrder);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <param name="PaxCode"></param>
        /// <returns></returns>
        public List<FlightPakMasterService.GetPaxAvailableList> GetAvailableListByCode(Int64 PaxCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PaxCode))
            {
                List<FlightPakMasterService.GetPaxAvailableList> PassengerList = new List<GetPaxAvailableList>();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ReturnValue = PaxService.GetPaxAvailableList(CustomerID, PaxCode);
                        PassengerList = ReturnValue.EntityList.ToList();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return PassengerList;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <param name="PaxCode"></param>
        /// <returns></returns>
        public List<FlightPakMasterService.GetPaxSelectedList> GetSelectedListByCode(Int64 PaxCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PaxCode))
            {
                List<FlightPakMasterService.GetPaxSelectedList> PassengerList = new List<GetPaxSelectedList>();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ReturnValue = PaxService.GetPaxSelectedList(CustomerID, PaxCode);
                        PassengerList = ReturnValue.EntityList.ToList();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return PassengerList;
            }
        }
        /// <summary>
        /// Save Selected Passenger Orders into Table
        /// </summary>
        /// <param name="PassengerGroupID">Pass Passenger Group Code</param>
        protected FlightPakMasterService.PassengerGroup SavePaxOrders(FlightPakMasterService.PassengerGroup oPassengerGroup, long? CustomerId,long PassengerGroupID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassengerGroup))
            {
                FlightPakMasterService.PassengerGroupOrder PaxGroupOrder = new FlightPakMasterService.PassengerGroupOrder();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        // Delete the previous Pax Group Entries in [PassengerGroupOrder] Table
                        DeleteSeletedItem(0, oPassengerGroup.PassengerGroupID);
                        // Insert the Selected Pax Group Entries in [PassengerGroupOrder] Table
                        oPassengerGroup.PassengerGroupOrder = new List<PassengerGroupOrder>();
                        if (hdnSave.Value == "Save")
                        {
                            oPassengerGroup.CustomerID = CustomerId;
                            oPassengerGroup.PassengerGroupID = PassengerGroupID;
                        }
                        Int64 Identity = 0;
                        for (int ListItem = 0; ListItem < lstSelected.Items.Count; ListItem++)
                        {
                                Identity = Identity - 1;
                                PaxGroupOrder = new FlightPakMasterService.PassengerGroupOrder();
                                PaxGroupOrder.PassengerRequestorID = Convert.ToInt64(lstSelected.Items[ListItem].Value);
                                PaxGroupOrder.CustomerID = oPassengerGroup.CustomerID;
                                PaxGroupOrder.PassengerGroupOrderID = Identity;
                                PaxGroupOrder.PassengerGroupID = oPassengerGroup.PassengerGroupID;
                                PaxGroupOrder.IsDeleted = false;
                                PaxGroupOrder.OrderNum = ListItem;
                                oPassengerGroup.PassengerGroupOrder.Add(PaxGroupOrder);
                                //PaxService.AddPaxGroupOrder(PaxGroupOrder);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return oPassengerGroup;
            }
        }
        #region "Grid events"
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPaxGroup_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgPaxGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient MasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var PaxGroupListInfo = MasterService.GetPaxGroupListInfo();
                            List<FlightPakMasterService.GetPassengerGroupList> PassengerGroupList = new List<GetPassengerGroupList>();
                            if (PaxGroupListInfo.ReturnFlag == true)
                            {
                                PassengerGroupList = PaxGroupListInfo.EntityList.ToList();
                            }
                            dgPaxGroup.DataSource = PassengerGroupList;
                            Session["GroupCodes"] = PassengerGroupList;
                            if (chkSearchActiveOnly.Checked == true)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// Item Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPaxGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                // Lock the record
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.PassengerGroup, Convert.ToInt64(Session["PassengerGroupID"]));
                                    Session["IsEditLockPassengerGroup"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PassengerGroup);
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    DisableLinks();
                                    RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgPaxGroup.SelectedIndexes.Clear();
                                //tbCode.ReadOnly = false;
                                //tbCode.BackColor = System.Drawing.Color.White;
                                DisplayInsertForm();
                                BindAvailableList();
                                GridEnable(true, false, false);
                                lstSelected.Items.Clear();
                                DisableLinks();
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                break;
                            case "Filterss":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// Update Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPaxGroup_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["PassengerGroupID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.PassengerGroup oPassengerGroup = new FlightPakMasterService.PassengerGroup();
                                oPassengerGroup = GetItems(oPassengerGroup);
                                oPassengerGroup = SavePaxOrders(oPassengerGroup,null,PassengerGroupID);
                                var objRetVal = PaxService.UpdatePaxGroup(oPassengerGroup);
                                if (objRetVal.ReturnFlag == true)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.PassengerGroup, Convert.ToInt64(Session["PassengerGroupID"]));
                                        Session["IsEditLockPassengerGroup"] = "False";
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        dgPaxGroup.Rebind();
                                        SelectItem();
                                        ReadOnlyForm();
                                    }

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    ShowHideControls(true);
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.PassengerGroup);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// Insert Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPaxGroup_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            
                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Unique Code is Required', 360, 50, 'Passenger Group');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        }
                        else
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.PassengerGroup oPassengerGroup = new FlightPakMasterService.PassengerGroup();
                                oPassengerGroup = GetItems(oPassengerGroup);
                                var objRetVal = PaxService.AddPaxGroup(oPassengerGroup);                                
                                if (objRetVal.ReturnFlag == true)
                                {
                                    FlightPakMasterService.MasterCatalogServiceClient oPassengerGroup1 = new FlightPakMasterService.MasterCatalogServiceClient();
                                    var PaxGroupListInfo = oPassengerGroup1.GetPaxGroupListInfo().EntityList.Where(x => x.PassengerGroupCD.Trim().ToUpper().ToString() == tbCode.Text.Trim().ToUpper().ToString());
                                    foreach (var item in PaxGroupListInfo)
                                    {
                                        CustomerId = item.CustomerID;
                                         PassengerGroupID = item.PassengerGroupID;
                                    }
                                    oPassengerGroup = SavePaxOrders(oPassengerGroup, CustomerId, PassengerGroupID);
                                    var objRetVal1 = PaxService.UpdatePaxGroup(oPassengerGroup);
                                    
                                    GridEnable(true, true, true);
                                    DefaultSelection(true);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    if(hdnSave.Value!="Save")
                                    ShowHideControls(true);
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.PassengerGroup);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
                finally
                {
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPaxGroup_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            if (Session["PassengerGroupID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient MasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.PassengerGroup PaxGroup = new FlightPakMasterService.PassengerGroup();
                                    GridDataItem Item = dgPaxGroup.SelectedItems[0] as GridDataItem;
                                    PaxGroup.PassengerGroupCD = Item.GetDataKeyValue("PassengerGroupCD").ToString();
                                    PaxGroup.PassengerGroupID = Convert.ToInt64(Item.GetDataKeyValue("PassengerGroupID").ToString());
                                    PaxGroup.IsDeleted = true;
                                    //Lock the record
                                    var returnValue = CommonService.Lock(EntitySet.Database.PassengerGroup, Convert.ToInt64(Session["PassengerGroupID"]));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PassengerGroup);
                                        return;
                                    }
                                    MasterService.DeletePaxGroup(PaxGroup);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = false;
                                    DefaultSelection(false);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.PassengerGroup, Convert.ToInt64(Session["PassengerGroupID"]));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPaxGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgPaxGroup.SelectedItems[0] as GridDataItem;
                                Session["PassengerGroupID"] = item["PassengerGroupID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                                ShowHideControls(true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                    }
                }
            }
        }
        protected void dgPaxGroup_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgPaxGroup.ClientSettings.Scrolling.ScrollTop = "0";
            PassengerGroupPageNavigated = true;
        }
        protected void dgPaxGroup_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (PassengerGroupPageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPaxGroup, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        #endregion
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgPaxGroup;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Passenger Group Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgPaxGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgPaxGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                        // dgPaxGroup.Rebind();
                        //  GridEnable(true, true, true);
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// Cancel Passenger Group Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.PassengerGroup, Convert.ToInt64(Session["PassengerGroupID"]));
                            //Session.Remove("PassengerGroupID");
                            DefaultSelection(false);
                            EnableLinks();
                            ShowHideControls(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNext_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lstAvailable.SelectedItem != null)
                        {
                            string SelectedList = string.Empty;
                            ArrayList SelectedArrayList = new ArrayList();
                            for (int ListItem = lstAvailable.Items.Count - 1; ListItem >= 0; ListItem--)
                            {
                                if (lstAvailable.Items[ListItem].Selected)
                                {
                                    SelectedList = lstAvailable.Items[ListItem].ToString();
                                    ArrayList tempSelectedList = new ArrayList(SelectedList.Split(new Char[] { '-' }));
                                    SelectedArrayList.Add(tempSelectedList[1].ToString().Trim().Replace("(", "").Replace(")", ""));
                                    lstSelected.Items.Add(lstAvailable.Items[ListItem]);
                                    lstAvailable.Items.Remove(lstAvailable.Items[ListItem]);
                                }
                            }
                            lstSelected.SelectedIndex = -1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrevious_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lstSelected.SelectedItem != null)
                        {
                            string SelectedList = string.Empty;
                            ArrayList SelectedArrayList = new ArrayList();
                            for (int ListItem = lstSelected.Items.Count - 1; ListItem >= 0; ListItem--)
                            {
                                if (lstSelected.Items[ListItem].Selected)
                                {
                                    SelectedList = lstSelected.Items[ListItem].ToString();
                                    ArrayList tempSelectedList = new ArrayList(SelectedList.Split(new Char[] { '-' }));
                                    SelectedArrayList.Add(tempSelectedList[1].ToString().Trim().Replace("(", "").Replace(")", ""));
                                    lstAvailable.Items.Add(lstSelected.Items[ListItem]);
                                    lstSelected.Items.Remove(lstAvailable.Items[ListItem]);
                                }
                            }
                            lstAvailable.SelectedIndex = -1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLast_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ArrayList SelectedArrayList = new ArrayList();
                        for (int ListItem = lstAvailable.Items.Count - 1; ListItem >= 0; ListItem--)
                        {
                            string SelectedList = lstAvailable.Items[ListItem].ToString();
                            ArrayList TempSelectedArrayList = new ArrayList(SelectedList.Split(new Char[] { '-' }));
                            SelectedArrayList.Add(TempSelectedArrayList[1].ToString().Trim().Replace("(", "").Replace(")", ""));
                            lstSelected.Items.Add(lstAvailable.Items[ListItem]);
                            lstAvailable.Items.Remove(lstAvailable.Items[ListItem]);
                        }
                        lstSelected.SelectedIndex = -1;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFirst_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ArrayList SelectedArrayList = new ArrayList();
                        for (int i = lstSelected.Items.Count - 1; i >= 0; i--)
                        {
                            string SelectedList = lstSelected.Items[i].ToString();
                            ArrayList TempSelectedList = new ArrayList(SelectedList.Split(new Char[] { '-' }));
                            SelectedArrayList.Add(TempSelectedList[1].ToString().Trim().Replace("(", "").Replace(")", ""));
                            lstAvailable.Items.Add(lstSelected.Items[i]);
                            lstSelected.Items.Remove(lstSelected.Items[i]);
                        }
                        lstAvailable.SelectedIndex = -1;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text.Trim() == string.Empty)
                        {
                            rfvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                        }
                        else if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
                        {
                            if (CheckAllReadyExist())
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        /// <summary>
        /// Function to Check if Passenger Group Code Alerady Exists
        /// </summary>
        /// <returns></returns>
        private Boolean CheckAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.GetPassengerGroupList> PassengerGroupList = new List<GetPassengerGroupList>();
                    if (Session["GroupCodes"] != null)
                    {
                        PassengerGroupList = ((List<FlightPakMasterService.GetPassengerGroupList>)Session["GroupCodes"]).Where(x => x.PassengerGroupCD.Trim().ToUpper().ToString() == tbCode.Text.Trim().ToUpper().ToString()).ToList();
                        if (PassengerGroupList.Count != 0)
                        {
                            ReturnValue = true;
                        }
                        else
                        {
                            ReturnValue = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// To check Valid Home Base
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbHomeBase.Text != null && tbHomeBase.Text.Trim() != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient MasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ReturnValue = MasterService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.Trim().ToUpper() == (tbHomeBase.Text.Trim().ToUpper())).ToList();
                                if (ReturnValue.Count() <= 0)
                                {
                                    cvHomeBase.IsValid = false;
                                    RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                                }
                                else
                                {
                                    hdnHomeBase.Value = ReturnValue[0].HomebaseID.ToString();
                                    tbHomeBase.Text = ReturnValue[0].HomebaseCD;
                                    hdnHomeBaseID.Value = ReturnValue[0].HomebaseID.ToString();
                                }
                            }
                        }
                        else
                        {
                            hdnHomeBase.Value = string.Empty;
                            hdnHomeBaseID.Value = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerGroup);
                }
            }
        }
        private void ShowHideControls(bool Visibility)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Visibility))
            {
                lstAvailable.Visible = Visibility;
                tbAvailableFilter.Visible = Visibility;
                lstSelected.Visible = Visibility;
                //btnNext.Visible = Visibility;
                //btnPrevious.Visible = Visibility;
                //btnLast.Visible = Visibility;
                //btnFirst.Visible = Visibility;
                lbOrderDetails.Visible = Visibility;
                lbPaxAvailable.Visible = Visibility;
                lbPaxSelected.Visible = Visibility;
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                        GridEnable(true, true, true);
                        dgPaxGroup.Rebind();
                        SelectItem();
                        ReadOnlyForm();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetPassengerGroupList> lstPaxGroup = new List<FlightPakMasterService.GetPassengerGroupList>();
                if (Session["GroupCodes"] != null)
                {
                    lstPaxGroup = (List<FlightPakMasterService.GetPassengerGroupList>)Session["GroupCodes"];
                }
                if (lstPaxGroup.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstPaxGroup = lstPaxGroup.Where(x => x.IsInactive == false).ToList<GetPassengerGroupList>(); }
                    else
                    {
                        lstPaxGroup = lstPaxGroup.ToList<GetPassengerGroupList>();
                    }

                    dgPaxGroup.DataSource = lstPaxGroup;
                    if (IsDataBind)
                    {
                        dgPaxGroup.DataBind();
                    }
                }
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgPaxGroup.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var PaxGroupValue = FPKMstService.GetPaxGroupListInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, PaxGroupValue);
            List<FlightPakMasterService.GetPassengerGroupList> filteredList = GetFilteredList(PaxGroupValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.PassengerGroupID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgPaxGroup.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgPaxGroup.CurrentPageIndex = PageNumber;
            dgPaxGroup.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetPassengerGroupList PaxGroupValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["PassengerGroupID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = PaxGroupValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().PassengerGroupID;
                Session["PassengerGroupID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetPassengerGroupList> GetFilteredList(ReturnValueOfGetPassengerGroupList PaxGroupValue)
        {
            List<FlightPakMasterService.GetPassengerGroupList> filteredList = new List<FlightPakMasterService.GetPassengerGroupList>();

            if (PaxGroupValue.ReturnFlag)
            {
                filteredList = PaxGroupValue.EntityList.Where(x => x.IsDeleted == false).ToList();
            }

            if (chkSearchActiveOnly.Checked)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInactive == false).ToList<GetPassengerGroupList>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgPaxGroup.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgPaxGroup.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}