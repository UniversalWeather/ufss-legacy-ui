﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewChecklistGroupPopup.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.CrewChecklistGroupPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Checklist Group</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
     <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgCrewChecklistGroup.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCrewChecklistGroup.ClientID %>");
               
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "CheckGroupCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "CheckGroupDescription");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "CheckGroupID");

                }
                if (selectedRows.length > 0) {
                    oArg.CheckGroupCD = cell1.innerHTML;
                    oArg.CheckGroupDescription = cell2.innerHTML;
                    oArg.CheckGroupID = cell3.innerHTML;
                }
                else {
                    oArg.CheckGroupCD = "";
                    oArg.CheckGroupDescription = "";
                    oArg.CheckGroupID = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgCrewChecklistGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewChecklistGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgCrewChecklistGroup" runat="server" AllowMultiRowSelection="true"
            AllowSorting="true" OnNeedDataSource="CrewChecklistGroup_BindData" AutoGenerateColumns="false"
            Height="330px" AllowPaging="false" Width="500px" OnItemCommand="dgCrewChecklistGroup_ItemCommand" 
            OnPreRender="dgCrewChecklistGroup_PreRender">
            <MasterTableView DataKeyNames="CheckGroupID,CheckGroupCD,CheckGroupDescription" CommandItemDisplay="None" AllowPaging="false">
                <Columns>
                    <telerik:GridBoundColumn DataField="CheckGroupCD" HeaderText="Checklist Group Code"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="100px" FilterControlWidth="80px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CheckGroupDescription" HeaderText="Description"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="400px" FilterControlWidth="380px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                     <telerik:GridBoundColumn DataField="CheckGroupID" HeaderText="Checklist Group Code" AutoPostBackOnFilter="false"
                     Display="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo" FilterDelay="500">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                        <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick = "returnToParent" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
             <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
