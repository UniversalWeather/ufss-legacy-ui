﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="CustomerAddressCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.CustomerAddressCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function OnClientClick(strPanelToExpand) {

                var PanelBar1 = $find("<%= pnlNotes.ClientID %>");
                var PanelBar2 = $find("<%= pnlMaintenance.ClientID %>");
                PanelBar1.get_items().getItem(0).set_expanded(false);
                PanelBar2.get_items().getItem(0).set_expanded(false);
                if (strPanelToExpand == "Notes") {
                    PanelBar1.get_items().getItem(0).set_expanded(true);
                }
                return false;
            }

            // To Store Image
            function File_onchange() {
                if (validate())
                    __doPostBack('__Page', 'LoadImage');
            }

            function validate() {
                var uploadcontrol = document.getElementById('<%=fileUL.ClientID%>').value;
                //var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.[Bb][Mm][Pp])$/;
                //var reg = /([^\s]+(?=.(bmp)).\2)/gm;
                if (uploadcontrol.length > 0) {
                    //if (reg.test(uploadcontrol)) {
                        return true;
                    }
                    else {
                        //alert("Only .bmp files are allowed!");
                        return false;
                    }
                //}
            }

            function CheckName() {
                var nam = document.getElementById('<%=tbImgName.ClientID%>').value;

                if (nam != "") {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = false;
                }
                else {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = true;
                }
            }

            // this function is related to Image 
            function OpenRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
                oWnd.show();
            }

            // this function is related to Image 
            function CloseRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = null;
                oWnd.close();
            }


            function openHomeBase() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=tbHomeBase.ClientID%>").value, "radCompanyMasterPopup");

            }
            function openClosestIcao() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/AirportMasterPopup.aspx?IcaoID=" + document.getElementById("<%=tbClosestIcao.ClientID%>").value, "radAirportPopup");

            }
            function openCountry() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CountryMasterPopup.aspx?CountryCD=" + document.getElementById("<%=tbCountry.ClientID%>").value, "RadCountryMasterPopup");

            }
            function openMetro() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/MetroCityPopup.aspx?MetroCD=" + document.getElementById("<%=tbMetro.ClientID%>").value, "RadMetroMasterPopup");

            }

            function ConfirmClose(WinName) {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); // GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }
            function OnClientCloseCompanyMasterPopup(oWnd, args) {
                var combo = $find("<%= tbHomeBase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.HomebaseID;
                        document.getElementById("<%=cvHomeBase.ClientID%>").value = "";
                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                        document.getElementById("<%=cvHomeBase.ClientID%>").value = "";
                    }
                }
            }
            function OnClientCloseAirportMasterPopup1(oWnd, args) {
                var combo = $find("<%= tbClosestIcao.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnAirportID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=cvClosestIcao.ClientID%>").value = "";
                    }
                    else {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = "";
                        document.getElementById("<%=hdnAirportID.ClientID%>").value = "";
                        document.getElementById("<%=cvClosestIcao.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }

            }
            function OnClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbCountry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=cvCountry.ClientID%>").value = "";
                    }
                    else {
                        document.getElementById("<%=tbCountry.ClientID%>").value = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = "";
                        document.getElementById("<%=cvCountry.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbMetro.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%=tbMetro.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=cvMetro.ClientID%>").value = "";
                    }
                    else {
                        document.getElementById("<%=tbMetro.ClientID%>").value = "";
                        document.getElementById("<%=cvMetro.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                //alert(new String("Width:" + bounds.width + " " + "Height: " + bounds.height));
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function validateEmptyLatitudeTextbox() {
            }
            function validateEmptyLandingBiasTextbox() {
            }
            function printImage() {
                var image = document.getElementById('<%=ImgPopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlImg = document.getElementById('<%=ddlImg.ClientID%>');
                if (ddlImg.length > 0) {
                    var ImageName = ddlImg.options[ddlImg.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });

                    var oManager = $find("<%= RadWindowManager1.ClientID %>"); 
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Custom Address"); 
                    args.set_cancel(true);
                }
                return ReturnValue;
            }
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");

                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
        </script>
        <script type="text/javascript">
            function browserName() {
                var agt = navigator.userAgent.toLowerCase();
                if (agt.indexOf("msie") != -1) return 'Internet Explorer';
                if (agt.indexOf("chrome") != -1) return 'Chrome';
                if (agt.indexOf("opera") != -1) return 'Opera';
                if (agt.indexOf("firefox") != -1) return 'Firefox';
                if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
                if (agt.indexOf("netscape") != -1) return 'Netscape';
                if (agt.indexOf("safari") != -1) return 'Safari';
                if (agt.indexOf("staroffice") != -1) return 'Star Office';
                if (agt.indexOf("webtv") != -1) return 'WebTV';
                if (agt.indexOf("beonex") != -1) return 'Beonex';
                if (agt.indexOf("chimera") != -1) return 'Chimera';
                if (agt.indexOf("netpositive") != -1) return 'NetPositive';
                if (agt.indexOf("phoenix") != -1) return 'Phoenix';
                if (agt.indexOf("skipstone") != -1) return 'SkipStone';
                if (agt.indexOf('\/') != -1) {
                    if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                        return navigator.userAgent.substr(0, agt.indexOf('\/'));
                    }
                    else return 'Netscape';
                } else if (agt.indexOf(' ') != -1)
                    return navigator.userAgent.substr(0, agt.indexOf(' '));
                else return navigator.userAgent;
            }
            function GetBrowserName() {
                document.getElementById('<%=hdnBrowserName.ClientID%>').value = browserName();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Company Profile" OnClientClose="OnClientCloseCompanyMasterPopup" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Company Profile" OnClientClose="OnClientCloseCompanyMasterPopup" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Airport" OnClientClose="OnClientCloseAirportMasterPopup1" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings//Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Country" OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadMetroMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Metro" OnClientClose="OnClientCloseMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printImage();return false;" /></div>
                                    <asp:Image ID="ImgPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Custom Address</span> <span class="tab-nav-icons">
                        <!--<a href="#"
                        class="search-icon"></a><a href="#" class="save-icon"></a><a href="#" class="print-icon">
                        </a>-->
                        <a href="../../Help/ViewHelp.aspx?Screen=CustomAddressHelp" class="help-icon" target="_blank"
                            title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0" class="head-sub-menu">
            <tr>
                <td>
                    <div class="status-list">
                        <span>
                            <asp:CheckBox ID="chkSearchHomebaseOnly" runat="server" Text="Home Base Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                AutoPostBack="true" /></span> <span>
                                    <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                        AutoPostBack="true" /></span>
                    </div>
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="dgCustomerAddress" runat="server" AllowSorting="true" OnItemCreated="dgCustomerAddress_ItemCreated"
            Visible="true" OnNeedDataSource="dgCustomerAddress_BindData" OnItemCommand="dgCustomerAddress_ItemCommand"
            OnPageIndexChanged="CustomerAddress_PageIndexChanged" OnUpdateCommand="dgCustomerAddress_UpdateCommand"
            OnInsertCommand="dgCustomerAddress_InsertCommand" OnPreRender="CustomerAddress_PreRender"
            OnDeleteCommand="dgCustomerAddress_DeleteCommand" AutoGenerateColumns="false"
            Height="341px" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgCustomerAddress_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
            <MasterTableView DataKeyNames="CustomerID,CustomAddressCD,CountryID,CustomAddressID,UserName,Name,Address1,Address2,Address3,CustomCity,CustomState,PostalZipCD,NationalityCD,MetropolitanArea,Contact,PhoneNum,FaxNum,ContactEmailID,Website,Symbol,LatitudeDegree,LatitudeMinutes,LatitudeNorthSouth,LongitudeDegrees,LongitudeMinutes,LongitudeEastWest,AirportID,HomebaseID,ClosestICAO,HomeBaseCD,Remarks,LastUpdUID,LastUpdTS,IsDeleted
        ,BusinessPhone,CellPhoneNum,HomeFax,CellPhoneNum2,OtherPhone,BusinessEmail,PersonalEmail,OtherEmail,IsInActive"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="CustomAddressCD" HeaderText="Custom Address Code"
                        FilterControlWidth="120px" HeaderStyle-Width="140px" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Name" HeaderText="Description" CurrentFilterFunction="StartsWith"
                        FilterControlWidth="220px" HeaderStyle-Width="240px" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="BusinessPhone" HeaderText="Business Phone" CurrentFilterFunction="StartsWith"
                        FilterControlWidth="180px" HeaderStyle-Width="200px" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" HeaderStyle-Width="100px"
                        FilterControlWidth="80px" CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CustomAddressID" HeaderText="CustomAddressID"
                        Display="false" CurrentFilterFunction="EqualTo" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                        ShowFilterIcon="false" AutoPostBackOnFilter="true"
                        AllowFiltering="false">
                    </telerik:GridCheckBoxColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            Visible='<%# IsAuthorized(Permission.Database.AddCustomerAddressCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            Visible='<%# IsAuthorized(Permission.Database.EditCustomerAddressCatalog)%>'
                            ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                            Visible='<%# IsAuthorized(Permission.Database.DeleteCustomerAddressCatalog)%>'
                            runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                    <div>
                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea-nw">
                            <tr>
                                <td>
                                    <asp:Button ID="btnNotes" runat="server" CssClass="ui_nav" Text="Notes" OnClientClick="javascript:OnClientClick('Notes');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveChangesTop" runat="server" CssClass="button" Text="Save" ValidationGroup="save"
                                        OnClick="SaveChanges_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                        OnClick="Cancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlMaintenance" Width="100%" ExpandAnimation-Type="none"
                CollapseAnimation-Type="none" runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" class="box1">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel80">
                                                            <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <span class="mnd_text">Custom Address Code</span>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCode" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumericAndChar(this, event,'-')"
                                                                            onBlur="return RemoveSpecialChars(this);" AutoPostBack="true" OnTextChanged="Code_TextChanged"
                                                                            CssClass="text40" ValidationGroup="save"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Custom Address Code is Required"
                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="Custom Address Code is Required"
                                                                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbCode" CssClass="alert-text"
                                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbHomeBase" runat="server" Text="Home Base"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="tdLabel80">
                                                                        <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" CssClass="text40" OnTextChanged="HomeBase_TextChanged"
                                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true" ValidationGroup="save"
                                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                        <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openHomeBase();return false;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                            ErrorMessage="Invalid Home Base" Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbName" runat="server" Text="Name"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <asp:TextBox ID="tbName" runat="server" MaxLength="40" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbClosestIcao" runat="server" Text="Closest ICAO"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbClosestIcao" runat="server" MaxLength="4" onBlur="return RemoveSpecialChars(this)"
                                                                            AutoPostBack="true" ValidationGroup="save" CssClass="text40" OnTextChanged="ClosestIcao_TextChanged"
                                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                        <asp:Button ID="btnClosestIcao" runat="server" OnClientClick="javascript:openClosestIcao();return false;"
                                                                            CssClass="browse-button"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvClosestIcao" runat="server" ControlToValidate="tbClosestIcao"
                                                                            ErrorMessage="Invalid Closest ICAO" Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbAddress1" runat="server" Text="Address 1"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <asp:TextBox ID="tbAddr1" runat="server" MaxLength="100" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbAddr2" runat="server" Text="Address 2"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbAddr2" runat="server" MaxLength="100" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbAddr3" runat="server" Text="Address 3"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <asp:TextBox ID="tbAddr3" runat="server" MaxLength="100" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            Metro
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbMetro" runat="server" onBlur="return RemoveSpecialChars(this)"
                                                                            AutoPostBack="true" ValidationGroup="save" CssClass="text40" OnTextChanged="Metro_TextChanged"
                                                                            MaxLength="3"></asp:TextBox>
                                                                        <asp:Button ID="btnMetro" runat="server" OnClientClick="javascript:openMetro();return false;"
                                                                            CssClass="browse-button"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvMetro" runat="server" ControlToValidate="tbMetro" ErrorMessage="Invalid Metro"
                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbCity" runat="server" Text="City"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <asp:TextBox ID="tbCity" runat="server" MaxLength="30" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbState" runat="server" Text="State/Province"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbState" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbCountry" runat="server" Text="Country"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCountry" runat="server" CssClass="text40" OnTextChanged="Country_TextChanged"
                                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true" ValidationGroup="save"
                                                                            MaxLength="3"></asp:TextBox>
                                                                        <asp:Button ID="btnCountry" runat="server" OnClientClick="javascript:openCountry();return false;"
                                                                            CssClass="browse-button"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbCountry"
                                                                            ErrorMessage="Invalid Country" Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbPostal" runat="server" Text="Postal"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbPostal" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbContact" runat="server" Text="Contact"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <asp:TextBox ID="tbContact" runat="server" MaxLength="25" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbWebsite" runat="server" Text="Company Web site"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbWebsite" runat="server" MaxLength="200" CssClass="text200">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%--<asp:RegularExpressionValidator ID="revwebsite" runat="server" Display="Dynamic"
                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbWebsite" CssClass="alert-text"
                                                                            ValidationGroup="save" ValidationExpression="([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"></asp:RegularExpressionValidator>--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbBusinessPhone" runat="server" Text="Business Phone"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <asp:TextBox ID="tbBusinessPhone" runat="server" MaxLength="25" CssClass="text200"
                                                                onKeyPress="return fnAllowPhoneFormat(this,event)" ValidationGroup="save">
                                                            </asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbPhone" runat="server" Text="Home Phone"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbOtherPhone" runat="server" Text="Other Phone"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbOtherPhone" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbCellPhoneNum" runat="server" Text="Primary Mobile/Cell"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCellPhoneNum" runat="server" MaxLength="25" CssClass="text200"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbCellPhoneNum2" runat="server" Text="Secondary Mobile/Cell"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCellPhoneNum2" runat="server" MaxLength="25" CssClass="text200"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbBusinessEmail" runat="server" Text="Business E-mail"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbBusinessEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                            ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="regBusinessEmail" runat="server" Display="Dynamic"
                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbBusinessEmail" CssClass="alert-text"
                                                                            ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                                        </asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbPersonalEmail" runat="server" Text="Personal E-mail"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPersonalEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                            ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="regPersonalEmail" runat="server" Display="Dynamic"
                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbPersonalEmail" CssClass="alert-text"
                                                                            ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                                        </asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbEmailAddress" runat="server" Text="E-mail Address"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbEmail" runat="server" MaxLength="100" CssClass="text200" ValidationGroup="save">

                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                                            ControlToValidate="tbEmail" CssClass="alert-text" ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbOtherEmail" runat="server" Text="Other E-mail"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbOtherEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                            ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="regOtherEmail" runat="server" Display="Dynamic"
                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbOtherEmail" CssClass="alert-text"
                                                                            ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                                        </asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbFax" runat="server" Text="Business Fax"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbFax" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbHomeFax" runat="server" Text="Home Fax"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbHomeFax" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save"
                                                                onKeyPress="return fnAllowPhoneFormat(this,event)">


                                                            </asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbCustom" runat="server" Text="Custom-Symbol"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <asp:TextBox ID="tbCustom" runat="server" MaxLength="12" CssClass="text100" ReadOnly="true"></asp:TextBox>
                                                            <asp:Button ID="btnCustomSymbol" runat="server" CssClass="browse-button" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            Latitude
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left" class="tdLabel45">
                                                                        <asp:TextBox ID="tbLatitudeDeg" runat="server" CssClass="text35" MaxLength="2" onBlur="return validateEmptyLatitudeTextbox(this, event)"
                                                                            onKeyPress="return fnAllowNumeric(this, event)" ValidationGroup="save"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" class="tdLabel45">
                                                                        <asp:TextBox ID="tbLatitudeMin" runat="server" CssClass="text35" MaxLength="4" onBlur="return validateEmptyLandingBiasTextbox(this, event)"
                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" ValidationGroup="save"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" class="tdLabel45">
                                                                        <asp:TextBox ID="tbLatitudeNS" runat="server" CssClass="text20" MaxLength="1" onBlur="return RemoveSpecialChars(this)"
                                                                            onKeyPress="return fnAllowAlpha(this, event)" ValidationGroup="save"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="ltd_int" valign="top">
                                                                        Deg.
                                                                    </td>
                                                                    <td class="ltd_int" valign="top">
                                                                        Min.
                                                                    </td>
                                                                    <td class="ltd_int" valign="top">
                                                                        NS
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel50" valign="top">
                                                                        <asp:RangeValidator ID="rvLatitudeDeg" runat="server" ControlToValidate="tbLatitudeDeg"
                                                                            Type="Integer" MinimumValue="0" MaximumValue="90" ValidationGroup="save" CssClass="alert-text"
                                                                            ErrorMessage="0-90" Display="Dynamic"></asp:RangeValidator>
                                                                    </td>
                                                                    <td class="tdLabel40" valign="top">
                                                                        <asp:RangeValidator ID="rvLatitudeMin" runat="server" Type="Double" ControlToValidate="tbLatitudeMin"
                                                                            MinimumValue="0" MaximumValue="60" ErrorMessage="0-60" ValidationGroup="save"
                                                                            CssClass="alert-text" Display="Dynamic" SetFocusOnError="true"></asp:RangeValidator>
                                                                        <asp:RegularExpressionValidator ID="revLatitudeMin" runat="server" ControlToValidate="tbLatitudeMin"
                                                                            ValidationGroup="save" ErrorMessage="00.0" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:RegularExpressionValidator ID="revLatitudeNS" runat="server" Display="Dynamic"
                                                                            ControlToValidate="tbLatitudeNS" ErrorMessage="N or S" ValidationExpression="[nN]|[sS]"
                                                                            CssClass="alert-text" SetFocusOnError="true" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            Longitude
                                                        </td>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel45">
                                                                        <asp:TextBox ID="tbLongitudeDeg" runat="server" MaxLength="3" CssClass="text35" onKeyPress="return fnAllowNumeric(this, event)"
                                                                            onBlur="return validateEmptyLatitudeTextbox(this, event)"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel45">
                                                                        <asp:TextBox ID="tbLongitudeMin" runat="server" MaxLength="4" CssClass="text35" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                            onBlur="return validateEmptyLandingBiasTextbox(this, event)"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel45">
                                                                        <asp:TextBox ID="tbLongitudeEW" runat="server" MaxLength="1" CssClass="text20" onKeyPress="return fnAllowAlpha(this, event)"
                                                                            onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="ltd_int" valign="top">
                                                                        Deg.
                                                                    </td>
                                                                    <td valign="top" class="ltd_int">
                                                                        Min.
                                                                    </td>
                                                                    <td valign="top" class="ltd_int">
                                                                        EW
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel40" valign="top">
                                                                        <asp:RangeValidator ID="rvLongitudeDeg" runat="server" ControlToValidate="tbLongitudeDeg"
                                                                            Type="double" MinimumValue="0" MaximumValue="180" ValidationGroup="save" CssClass="alert-text"
                                                                            ErrorMessage="0-180" Display="Dynamic"></asp:RangeValidator>
                                                                    </td>
                                                                    <td class="tdLabel40" valign="top">
                                                                        <asp:RangeValidator ID="rvLongitudeMin" runat="server" ControlToValidate="tbLongitudeMin"
                                                                            Type="double" MinimumValue="0" MaximumValue="60" ValidationGroup="save" CssClass="alert-text"
                                                                            ErrorMessage="0-60" Display="Dynamic"></asp:RangeValidator>
                                                                        <asp:RegularExpressionValidator ID="revLongitudeMin" runat="server" ControlToValidate="tbLongitudeMin"
                                                                            ValidationGroup="save" ErrorMessage="00.0" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:RegularExpressionValidator ID="revLongitudeEW" runat="server" ControlToValidate="tbLongitudeEW"
                                                                            ValidationGroup="save" ErrorMessage="E or W" ValidationExpression="[eE]|[wW]"
                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Attachments" CssClass="PanelHeaderStyle">
                        <ContentTemplate>
                            <table width="100%" class="box1">                                
                                <tr style="display: none;">
                                    <td class="tdLabel100">
                                        Document Name
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text210" OnBlur="CheckName();"
                                            ClientIDMode="AutoID"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="tdLabel270">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:FileUpload ID="fileUL" runat="server" Enabled="false" ClientIDMode="AutoID" onchange="javascript:return File_onchange();" />
                                                    <asp:Label ID="lblError" CssClass="alert-text" runat="server" style="float: left;">All file types are allowed.</asp:Label>
                                                    <asp:HiddenField ID="hdnUrl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- <asp:RegularExpressionValidator ID="rexpfileUL" runat="server" ControlToValidate="fileUL"
                                                        ValidationGroup="save" CssClass="alert-text" ErrorMessage="Select Only .bmp file."
                                                        ValidationExpression="(.*\.([Bb][Mm][Pp])$)" Display="Static"></asp:RegularExpressionValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>                                
                                    <td valign="top" class="tdLabel110">
                                        <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                            OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text200"
                                            ClientIDMode="AutoID">
                                        </asp:DropDownList>
                                    </td>
                                    <td valign="top" class="tdLabel80">
                                        <asp:Image ID="imgFile" Width="50px" Height="50px" runat="server" OnClick="OpenRadWindow();"
                                            ClientIDMode="AutoID" />
                                        <asp:HyperLink ID="lnkFileName" runat="server" ClientIDMode="Static">Download File</asp:HyperLink>
                                    </td>
                                    <td valign="top" align="right" class="custom_radbutton">
                                        <telerik:RadButton ID="btndeleteImage" runat="server" Text="Delete" Enabled="false"
                                            OnClientClicking="ImageDeleteConfirm" OnClick="DeleteImage_Click" ClientIDMode="AutoID" />
                                    </td>
                                </tr>                                    
                            </table>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlNotes" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Notes" Expanded="false">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0" class="note-box">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea-db ">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="save"
                            OnClick="SaveChanges_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                        <asp:HiddenField ID="hdnAirportID" runat="server" />
                        <asp:HiddenField ID="hdnCountryID" runat="server" />
                        <asp:HiddenField ID="hdnMetroID" runat="server" />
                        <asp:HiddenField ID="hdnCustomAddressID" runat="server" />
                        <asp:HiddenField ID="hdnBrowserName" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" CausesValidation="false"
                            OnClick="Cancel_Click" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
