﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Crew Roster</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>    
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>    
     <style type="text/css">
       #gbox_gridCrew #gview_gridCrew .ui-jqgrid-bdiv table#gridCrew tr td:nth-last-child(-n+4) { text-align:center !important;}
    </style>

    <script type="text/javascript">
        var selectedRowMultipleCrew = "";
        var selectedRowData = "";
        var jqgridTableId = '#gridCrew';
        var crewId = null;
        var crewcd = null;
        var isopenlookup = false;
        var ismultiSelect = false;
        var CrewLists = [];
        var selectedrows = null;
        var msg = " Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
        var crewcdList = "";

        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
            
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });
            
            var selectedRowData = decodeURI(getQuerystring("CrewCD", ""));
            selectedRowMultipleCrew = $.trim(decodeURI(getQuerystring("CrewCD", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultipleCrew);
            if (selectedRowData != "") {
                isopenlookup = true;
            }
            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultipleCrew.length > 0)) {
                    returnToParent(rowData);
                }
                else {
                    showMessageBox('Please select a crew.', popupTitle);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/Views/Settings/People/CrewRoster.aspx?IsPopup=Add", popupTitle, 1100, 798, jqgridTableId);
                return false;
            });


            $("#btnClientCodeFilter").click(function () {
                var widthDoc = $(document).width();
                popupwindowLevel2("/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("tbClientCodeFilter").value, popupTitle, 545, 495, jqgridTableId, "tbClientCodeFilter", "ClientCD", "hdnClientId", "ClientID");
                return false;
            });

            $("#tbClientCodeFilter").change(function () {
                $("#hdnClientId").val("");
                return false;
            });

            $("#btnCrewGroupFilter").click(function () {
                var widthDoc = $(document).width();
                popupwindowLevel2("/Views/Settings/People/CrewGroupPopup.aspx?CrewGroupCD=" + document.getElementById("tbCrewGroupFilter").value, popupTitle, 574, 488, jqgridTableId, "tbCrewGroupFilter", "CrewGroupCD", "hdnCrewGroupId", "CrewGroupID");
                return false;
            });

            $("#tbCrewGroupFilter").change(function () {
                $("#hdnCrewGroupId").val("");
                return false;
            });

            $("#recordDelete").click(function () {
                if (ismultiSelect) {
                    selectedrows = $(jqgridTableId).jqGrid('getGridParam', 'selarrrow');
                }
                else {
                    selectedrows = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                }
                if (selectedrows!=null && selectedrows.length > 0) {
                    if (ismultiSelect) {
                        for (var i = 0; i < selectedrows.length; i++) {
                            var elem = new Object();
                            var rowsDataValues = $(jqgridTableId).jqGrid('getRowData', selectedrows[i]);
                            elem.CrewID = $.trim(rowsDataValues['CrewID']);
                            elem.CrewCD = $.trim(rowsDataValues['CrewCD']);
                            elem.Value = 0;
                            CrewLists.push(elem);
                        }
                    }
                    else {
                        var elem = new Object();
                        var rowsDataValues = $(jqgridTableId).jqGrid('getRowData', selectedrows);
                        elem.CrewID = $.trim(rowsDataValues['CrewID']);
                        elem.CrewCD = $.trim(rowsDataValues['CrewCD']);
                        elem.Value = 0;
                        CrewLists.push(elem);
                    }

                    if (CrewLists.length == 1) {

                        showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                    }
                    else {
                        showConfirmPopup('Are you sure you want to delete these records?', confirmCallBackFn, 'Confirmation');
                    }

                }
                else {
                    showMessageBox('Please select a crew.', popupTitle);
                }
                return false;

            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    DeleteCrewList(arg, jqgridTableId, CrewLists);
                }
            }


            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                crewId = rowData['CrewID'];
                var widthDoc = $(document).width();
                if (crewId != undefined && crewId != null && crewId != '' && crewId != 0) {
                    popupwindow("/Views/Settings/People/CrewRoster.aspx?IsPopup=&CrewID=" + crewId, popupTitle, 1100, 798, jqgridTableId);
                }
                else {
                    showMessageBox('Please select a crew.', popupTitle);
                }
                return false;
            });


            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                   
                    postData.apiType = 'fss';
                    postData.method = 'crewlist';
                    postData.showInactive = $('#chkActiveOnly').is(':checked') ? true : false;
                    postData.sendIcaoId = $("#chkHomebaseOnly").is(':checked') ? true : false;
                    postData.IsFixedRotary = $("#chkRotaryWingOnly").is(':checked') ? true : false;
                    postData.IsFixedWing = $("#chkFixedWingOnly").is(':checked') ? true : false;
                    postData.CrewGroupID = $("#hdnCrewGroupId").val();
                    postData.ClientID = $("#hdnClientId").val();
                    postData.CrewCD = selectedRowData;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 300,
                width: 850,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselectWidth: 100,
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                colNames: ['CrewID', 'Code', 'Crew Name', 'Phone', 'Home Base', 'Active', 'Checklist',''],
                colModel: [
                 { name: 'CrewID', index: 'CrewID', key: true, hidden: true },
                 { name: 'CrewCD', index: 'CrewCD', width: 90, fixed: true },
                 { name: 'CrewName', index: 'CrewName', width: 330, formatter: fullNameFormatter, searchoptions: { sopt: ['cn'] }, fixed: true },
                 { name: 'PhoneNum', index: 'PhoneNum', width: 150, fixed: true },
                 { name: 'IcaoID', index: 'IcaoID', width: 75, fixed: true },
                 { name: 'IsStatus', index: 'IsStatus', width: 50, formatter: 'checkbox', sortable: false, search: false, align: 'center', fixed: true },
                 { name: 'CheckList', index: 'CheckList', width: 60, formatter: 'checkbox', sortable: false, search: false, align: 'center', fixed: true },
                 { name: 'HomebaseCD', index: 'HomebaseCD', width: 50, hidden: true }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);                    
                    returnToParent(rowData);
                },
                onSelectRow: function (id,status) {
                    var rowData = $(this).getRowData(id);
                    
                    if (ismultiSelect) {
                        var tempcds = selectedRowMultipleCrew.split(",");
                        if (status) {
                            if (selectedRowMultipleCrew.lastIndexOf(",") != selectedRowMultipleCrew.length - 1) {
                                selectedRowMultipleCrew += ",";
                            }
                            if (jQuery.inArray(jQuery.trim(rowData.CrewCD), tempcds) == -1) {
                                selectedRowMultipleCrew += jQuery.trim(rowData.CrewCD);
                            }
                            
                            selectedRowMultipleCrew.substring(0, selectedRowMultipleCrew.length - 1);
                        } else {
                            if (jQuery.inArray(jQuery.trim(rowData.CrewCD), tempcds) != -1) {
                                tempcds[jQuery.inArray(jQuery.trim(rowData.CrewCD), tempcds)] = null;
                            }
                            tempcds = tempcds.filter(function (e) { return e });
                            selectedRowMultipleCrew = tempcds.join(",");
                            selectedRowMultipleCrew = selectedRowMultipleCrew.replace(/^,+/, "");
                            selectedRowMultipleCrew = selectedRowMultipleCrew.replace(/,+$/, "");
                        }
                        
                    } else {
                        
                        var lastSel = rowData['CrewCD']; //replace name with any column
                        selectedRowData = $.trim(rowData['CrewCD']);
                        
                    }
                },
                onSelectAll: function (id, status) {
                    var tempcd = selectedRowMultipleCrew.split(",");
                    if (selectedRowMultipleCrew.lastIndexOf(",") != selectedRowMultipleCrew.length - 1) {
                        selectedRowMultipleCrew += ",";
                    }
                    for (var i = 0; i < id.length; i++) {

                        var rowData = $(jqgridTableId).jqGrid("getRowData", id[i]);

                        if (status) {
                            if (jQuery.inArray(jQuery.trim(rowData.CrewCD), tempcd) == -1) {
                                selectedRowMultipleCrew += jQuery.trim(rowData.CrewCD) + ",";
                            }
                        } else {
                            if (jQuery.inArray(jQuery.trim(rowData.CrewCD), tempcd) != -1) {
                                tempcd[jQuery.inArray(jQuery.trim(rowData.CrewCD), tempcd)] = null;
                            }
                            tempcd = tempcd.filter(function (e) { return e });
                            selectedRowMultipleCrew = tempcd.join(",");
                            selectedRowMultipleCrew = selectedRowMultipleCrew.replace(/^,+/, "");
                            selectedRowMultipleCrew = selectedRowMultipleCrew.replace(/,+$/, "");
                        }

                    }

                    selectedRowMultipleCrew = jQuery.trim(selectedRowMultipleCrew);
                    if (selectedRowMultipleCrew.lastIndexOf(",") == selectedRowMultipleCrew.length - 1)
                        selectedRowMultipleCrew = selectedRowMultipleCrew.substr(0, selectedRowMultipleCrew.length - 1);
                    $("#hdnselectedfccd").val(selectedRowMultipleCrew);
                },
                afterInsertRow: function (rowid, rowObject) {
                    if (ismultiSelect) {
                        var tempcds = selectedRowMultipleCrew.split(",");
                        if (jQuery.inArray(jQuery.trim(rowObject.CrewCD), tempcds) != -1) {
                            jQuery(jqgridTableId).setSelection(rowid, true);
                        }
                    } else {

                        if ($.trim(rowObject.CrewCD.toLowerCase()) == $.trim(selectedRowData.toLowerCase()) && jQuery.trim(selectedRowData) != "") {
                            $(this).find(".selected").removeClass('selected');
                            $(this).find('.ui-state-highlight').addClass('selected');
                            $(jqgridTableId).setSelection(rowid, true);
                        }
                    }
                },
                loadComplete: function (rowData) {

                    if (ismultiSelect) {
                        var gridid = jqgridTableId.substring(1, jqgridTableId.length);
                        $("#jqgh_" + gridid + "_cb").find("b").remove();

                        $("#jqgh_" + gridid + "_cb").append("<b> Select All</b>");
                        $("#jqgh_" + gridid + "_cb").css("height", "25px");
                        $(jqgridTableId + "_cb").css("width", "100px");
                        $(jqgridTableId + " tbody tr").children().first("td").css("width", "100px");

                    }
                }
            });
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

            $("#pagesizebox").insertBefore('.ui-paging-info');
        });

        function reloadCrew() {
            var crewgroupCd = $.trim($("#tbCrewGroupFilter").val());
            var clientCd = $.trim($("#tbClientCodeFilter").val());
            if (!IsNullOrEmptyOrUndefined(crewgroupCd)) {
                if (IsNullOrEmptyOrUndefined(clientCd)) {
                    $("#hdnClientId").val(0);
                }
                $("#hdnSearchResult").val("1");
                if (!IsNullOrEmptyOrUndefined($("#hdnCrewGroupId").val()) && IsNullOrEmptyOrUndefined(clientCd)) {
                    $("#gridCrew").jqGrid().trigger('reloadGrid');
                    $("#hdnSearchResult").val("");
                }
                else if (!IsNullOrEmptyOrUndefined($("#hdnCrewGroupId").val()) && !IsNullOrEmptyOrUndefined($("#hdnClientId").val())) {
                    $("#gridCrew").jqGrid().trigger('reloadGrid');
                    $("#hdnSearchResult").val("");
                }
            }
            else if (IsNullOrEmptyOrUndefined(clientCd)) {
                $("#hdnCrewGroupId").val(0);
                $(jqgridTableId).jqGrid().trigger('reloadGrid');
            }
            else if (!IsNullOrEmptyOrUndefined(clientCd)) {
                if (IsNullOrEmptyOrUndefined(crewgroupCd)) {
                    $("#hdnCrewGroupId").val(0);
                }
                $("#hdnSearchResult").val("1");
                if (!IsNullOrEmptyOrUndefined($("#hdnClientId").val()) && IsNullOrEmptyOrUndefined(crewgroupCd)) {
                    $("#gridCrew").jqGrid().trigger('reloadGrid');
                    $("#hdnSearchResult").val("");
                }
            }
            else if (IsNullOrEmptyOrUndefined(crewgroupCd)) {
                $("#hdnClientId").val(0);
                $("#gridCrew").jqGrid().trigger('reloadGrid');
            }
            selectedRowMultipleCrew = jQuery("#hdnselectedfccd").val();
        }

        function returnToParent(rowData) {
            
            selectedRowMultipleCrew = jQuery.trim(selectedRowMultipleCrew);
            if (selectedRowMultipleCrew.lastIndexOf(",") == selectedRowMultipleCrew.length - 1)
                selectedRowMultipleCrew = selectedRowMultipleCrew.substr(0, selectedRowMultipleCrew.length - 1);

            var oArg = new Object();
            oArg.CrewCD = jQuery.trim(rowData["CrewCD"]);
            oArg.HomeBaseCD = rowData["HomeBaseCD"];
            oArg.CrewName = rowData["FirstName"];
            oArg.CrewID = rowData["CrewID"];

            if (ismultiSelect) {
                oArg.Arg1 = selectedRowMultipleCrew;
            } else {
                oArg.Arg1 = oArg.CrewCD;
            }
            var paramValue = getQuerystring("ControlName", "CrewCD");
            oArg.CallingButton = paramValue;
            
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }

        $(document).ready(function () {
            $("#tbClientCodeFilter").blur(function () {
                var clientCd = $.trim($("#tbClientCodeFilter").val());
                if (!IsNullOrEmptyOrUndefined(clientCd)) {
                    CheckClientCodeExistance(clientCd, hdnClientId, errorMsg, "#gridCrew");
                }
                else {
                    $("#hdnClientId").val(0);
                    $("#errorMsg").removeClass().addClass('hideDiv');
                }
            });
        });

        $(document).ready(function () {
            $("#tbCrewGroupFilter").bind('blur keydown', function (e) {
                debugger;
                if (e.type === 'keydown' && e.keyCode !== 10 && e.keyCode !== 13)
                    return;
                var crewgroupCd = $.trim($("#tbCrewGroupFilter").val());
                if (crewgroupCd != undefined && crewgroupCd != null && crewgroupCd != '') {
                    CheckCrewGroupExistance(crewgroupCd, hdnCrewGroupId, errorMsg1, "#gridCrew");
                }
                else {
                    $("#hdnCrewGroupId").val(0);
                    $(jqgridTableId).jqGrid().trigger('reloadGrid');
                    $("#errorMsg1").removeClass().addClass('hideDiv');
                }
            });
        });

    </script>
     <style type="text/css">
        .showDiv {
            text-align: right;
            margin-right: 30px;
            color: red;
            visibility: visible !important;
        }

        .hideDiv {
            visibility: hidden;
        }
    </style>

</head>
<body>

        <div class="jqgrid">
            <input type="hidden" id="hdnSearchResult" value="" />
            <input type="hidden" id="hdnselectedfccd" value=""/>
            <div>
                <table class="box1">

                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="tdLabel100">
                                            <input type="checkbox" checked="checked" id="chkActiveOnly" /><label for="chkActiveOnly">Active Only</label>
                                        </td>
                                        <td valign="top" class="tdLabel120">
                                            <input type="checkbox" name="Homebase" id="chkHomebaseOnly" /><label for="chkHomeBaseOnly">Home Base Only</label>
                                        </td>
                                        <td valign="top" class="tdLabel90">
                                            <input type="checkbox" id="chkFixedWingOnly" /><label for="chkFixedWingOnly">Fixed Wing</label>
                                        </td>
                                        <td valign="top" class="tdLabel100">
                                            <input type="checkbox" id="chkRotaryWingOnly" /><label for="chkRotaryWingOnly">Rotary Wing</label>
                                        </td>
                                        <td valign="middle">
                                            <table cellspacing="0" cellpadding="0" id="Table1">
                                                <tbody>
                                                    <tr>
                                                        <td class="tdLabel120 single-line">
                                                            <span style="color: #444444; font-family: Arial; font-size: 12px;" id="lbClientCode">Client Code :</span>
                                                        </td>
                                                        <td class="tdLabel120">
                                                            <input type="text" style="font-family: Arial; font-size: 12px;" class="text50" id="tbClientCodeFilter" maxlength="5" name="tbClientCodeFilter" />
                                                            <input type="button" class="browse-button" id="btnClientCodeFilter" value="" name="btnClientCodeFilter" />
                                                            <input id="hdnClientId" type="hidden" value="" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                             <div id="errorMsg" class="hideDiv">Invalid client code.!</div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td valign="middle">
                                            <table cellspacing="0" cellpadding="0" id="Exception">
                                                <tbody>
                                                    <tr>
                                                        <td class="tdLabel120 single-line">
                                                            <span style="color: #444444; font-family: Arial; font-size: 12px;" id="Label2">Crew Group :</span>
                                                        </td>
                                                        <td class="tdLabel120">
                                                            <input type="text" style="font-family: Arial; font-size: 12px;" class="text50" maxlength="5" id="tbCrewGroupFilter" name="tbCrewGroupFilter" />
                                                            <input type="button" class="browse-button" id="btnCrewGroupFilter" value="" name="btnCrewGroupFilter" />
                                                            <input id="hdnCrewGroupId" type="hidden" value="" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                             <div id="errorMsg1" class="hideDiv">Invalid Crew Group.!</div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td valign="top" class="tdLabel100">
                                            <input type="button" class="button" id="btnSearch" onclick="reloadCrew(); return false;" value="Search" name="btnSearch" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table id="gridCrew" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>


</body>
</html>

