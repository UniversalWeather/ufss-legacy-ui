﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewCheckListPopUpRPT.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.CrewCheckListPopUpRPT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Crew CheckList</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            var oArg = new Object();
            var grid = $find("<%= dgCrewCheckList.ClientID %>");

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            //this function is used to get the selected code
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCrewCheckList.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "CrewCheckCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "CrewChecklistDescription")
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "CrewCheckID")
                        
                        if (i == 0) {
                            oArg.CrewCheckCD = cell1.innerHTML + ",";
                            oArg.CrewChecklistDescription = cell2.innerHTML;
                            oArg.CrewCheckID = cell3.innerHTML;
                        }
                        else {

                            oArg.CrewCheckCD += cell1.innerHTML + ",";
                            oArg.CrewChecklistDescription += cell2.innerHTML + ",";
                            oArg.CrewCheckID += cell3.innerHTML + ",";
                        }
                    }
                }
                else {
                    oArg.CrewCheckCD = "";
                    oArg.CrewChecklistDescription = "";
                    
                    oArg.CrewCheckID = "";
                }
                if (oArg.CrewCheckCD != "")
                    oArg.CrewCheckCD = oArg.CrewCheckCD.substring(0, oArg.CrewCheckCD.length - 1)
                else
                    oArg.CrewCheckCD = "";

                oArg.Arg1 = oArg.CrewCheckCD;
                oArg.CallingButton = "ChecklistCode"; // ( the name TypeCode should be same as popup tag)
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }

            }
            //this function is used to close this window
            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
         
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgCrewCheckList" runat="server" AllowMultiRowSelection="true" OnItemCreated="dgCrewCheckList_ItemCreated"
            AllowSorting="true" OnNeedDataSource="CrewCheckList_BindData" AutoGenerateColumns="false"
            Height="330px" PageSize="10" AllowPaging="true" Width="500px" PagerStyle-AlwaysVisible="true"
            OnPreRender="dgCrewCheckList_PreRender" OnItemCommand="dgCrewCheckList_ItemCommand">
            <MasterTableView DataKeyNames="CrewCheckID,CrewCheckCD,CrewChecklistDescription"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="CrewCheckCD" HeaderText="CrewCheck Code" CurrentFilterFunction="StartsWith"
                        FilterDelay="500" ShowFilterIcon="false" HeaderStyle-Width="100px" FilterControlWidth="80px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CrewChecklistDescription" HeaderText="Description"
                        CurrentFilterFunction="StartsWith" FilterDelay="500" ShowFilterIcon="false"
                        HeaderStyle-Width="380px" FilterControlWidth="360px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CrewCheckID" HeaderText="Type Code" CurrentFilterFunction="EqualTo"
                        Display="false" FilterDelay="500" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div class="grd_ok">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                    </div>
                    <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                        visible="false">
                        Use CTRL key to multi select</div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick="returnToParent" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
