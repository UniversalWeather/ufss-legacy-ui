﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Text.RegularExpressions;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CharterQuoteCustomerCatalog : BaseSecuredPage
    {
        #region "Declare Constants,Variables,Objects"
        private ExceptionManager exManager;
        private bool _selectLastModified = false;
        #endregion

        #region "RadAjax Events"
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCQCustomer;
                        }
                        else if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCQCustomer;
                        }
                        if (IsPostBack)
                        {
                            if (btnSaveChanges.Visible == true && tbCode.Enabled == true)
                            {
                                GridEnable(true, false, false);
                            }
                            else if (btnSaveChanges.Visible == true && tbCode.Enabled == false)
                            {
                                GridEnable(false, true, false);
                            }
                            else
                            {
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCQCustomer, dgCQCustomer, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCQCustomer.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                        
                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewCQCustomerCatalog);
                            base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgCQCustomer.Rebind();
                                LoadControlData();
                                LoadControlContactData();
                                DisableLinks();
                                EnableForm(false);
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                SetAsDefault(false);
                                DefaultSelection(true);
                            }

                            if (IsPopUp)
                            {
                                //Hide Controls
                                dgCQCustomer.Visible = false;
                                chkSearchHomebaseOnly.Visible = false;
                                chkSearchActiveOnly.Visible = false;
                                lnkCQCustomerContacts.Visible = false;
                                // Show Insert Form
                                if (IsAdd)
                                {
                                    (dgCQCustomer.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgCQCustomer.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set CQCustomerID value from request URL to session
                if (Request.QueryString["CQCustomerID"] != null)
                    Session["CQCustomerID"] = Request.QueryString["CQCustomerID"];
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCQCustomer.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            CQCustomer oCQCustomer = new CQCustomer();
            var CQCustomerValue = FPKMstService.GetCQCustomer(oCQCustomer);
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CQCustomerValue);
            List<FlightPakMasterService.GetCQCustomer> filteredList = GetFilteredList(CQCustomerValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CQCustomerID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCQCustomer.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCQCustomer.CurrentPageIndex = PageNumber;
            dgCQCustomer.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetCQCustomer CQCustomerValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["CQCustomerID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CQCustomerValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().CQCustomerID;
                Session["CQCustomerID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetCQCustomer> GetFilteredList(ReturnValueOfGetCQCustomer CQCustomerValue)
        {
            List<FlightPakMasterService.GetCQCustomer> filteredList = new List<FlightPakMasterService.GetCQCustomer>();

            if (CQCustomerValue.ReturnFlag)
            {
                filteredList = CQCustomerValue.EntityList;
            }

            if (!IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetCQCustomer>(); }
                    if (chkSearchHomebaseOnly.Checked) { filteredList = filteredList.Where(x => x.HomeBaseIcaoID == UserPrincipal.Identity._airportICAOCd).ToList<GetCQCustomer>(); }
                }
            }

            return filteredList;
        }

        /// <summary>
        /// Method for the setting default for the page.
        /// </summary>
        /// <param name="Enable"></param>    
        private void SetAsDefault(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((UserPrincipal.Identity != null) && (UserPrincipal.Identity._fpSettings != null))
                {
                    if (!IsAuthorized(Permission.Database.AddCQCustomerCatalog) && !IsAuthorized(Permission.Database.EditCQCustomerCatalog) && !IsAuthorized(Permission.Database.DeleteCQCustomerCatalog))
                    {
                        lnkCQCustomerContacts.Enabled = false;
                    }
                    if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                    {
                        chkSearchHomebaseOnly.Checked = true;
                    }
                }
                else
                {

                }
                chkSearchActiveOnly.Checked = true;
            }
        }

        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                if (IsPopUp)
                    dgCQCustomer.AllowPaging = false;
                if (BindDataSwitch)
                {
                    dgCQCustomer.Rebind();
                }
                if (dgCQCustomer.MasterTableView.Items.Count > 0)
                {
                    //if (!IsPostBack)
                    //{
                    //    Session["CQCustomerID"] = null;
                    //}
                    if (Session["CQCustomerID"] == null)
                    {
                        dgCQCustomer.SelectedIndexes.Add(0);

                        if (!IsPopUp)
                        {
                            Session["CQCustomerID"] = dgCQCustomer.Items[0].GetDataKeyValue("CQCustomerID").ToString();
                        }
                    }

                    if (dgCQCustomer.SelectedIndexes.Count == 0)
                        dgCQCustomer.SelectedIndexes.Add(0);

                    LoadControlData();
                    EnableForm(false);
                    GridEnable(true, true, true);
                }
                else
                {
                    ClearForm();
                    EnableForm(false);
                    GridEnable(true, true, true);
                }

                EnableForm(false);
                GridEnable(true, true, true);
            }
        }


        protected void Charterrates_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CQCustomerID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Session["CQCustomerID"].ToString()))
                            {

                                Session["IsFleet"] = "CQ";
                                string FleetID = Session["CQCustomerID"].ToString().Trim();
                                Session["DisplayDescription"] = tbName.Text;
                                Session["SelectedFleetNewCharterID"] = null;
                                RedirectToPage("../Fleet/FleetNewCharterRate.aspx?CQcustomerID=" + Microsoft.Security.Application.Encoder.UrlEncode(FleetID) + "&CQCD=" + Microsoft.Security.Application.Encoder.UrlEncode(tbCode.Text) + "&Screen=CharterQuote");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }


        private void DefaultSelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CQCustomerID"] != null)
                {
                    string ID = Session["CQCustomerID"].ToString();
                    foreach (GridDataItem Item in dgCQCustomer.MasterTableView.Items)
                    {
                        if (Item["CQCustomerID"].Text.Trim() == ID)
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                    if (dgCQCustomer.SelectedItems.Count == 0)
                    {
                        DefaultSelection(false);
                    }
                }
                else
                {
                    DefaultSelection(false);
                }
            }
        }

        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton lbtnInsertCtl = (LinkButton)dgCQCustomer.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                LinkButton lbtnDelCtl = (LinkButton)dgCQCustomer.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                LinkButton lbtnEditCtl = (LinkButton)dgCQCustomer.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                if (IsAuthorized(Permission.Database.AddCQCustomerCatalog) || true)
                {
                    lbtnInsertCtl.Visible = true;
                    if (Add)
                    {
                        lbtnInsertCtl.Enabled = true;
                    }
                    else
                    {
                        lbtnInsertCtl.Enabled = false;
                    }
                }
                else
                {
                    lbtnInsertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteCQCustomerCatalog) || true)
                {
                    lbtnDelCtl.Visible = true;
                    if (Delete)
                    {
                        lbtnDelCtl.Enabled = true;
                        lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        lbtnDelCtl.Enabled = false;
                        lbtnDelCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnDelCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditCQCustomerCatalog) || true)
                {
                    lbtnEditCtl.Visible = true;
                    if (Edit)
                    {
                        lbtnEditCtl.Enabled = true;
                        lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        lbtnEditCtl.Enabled = false;
                        lbtnEditCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnEditCtl.Visible = false;
                }
            }
        }

        /// <summary>
        /// /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                pnlFilterForm.Enabled = !(Enable);
                chkSearchActiveOnly.Enabled = !(Enable);
                chkSearchHomebaseOnly.Enabled = !(Enable);
                lnkCQCustomerContacts.Enabled = !(Enable);

                chkIsActive.Enabled = Enable;
                tbCode.Enabled = Enable;
                if (hdnSave.Value == "Update")
                {
                    tbCode.Enabled = false;
                }
                tbName.Enabled = Enable;
                tbDiscount.Enabled = Enable;
                tbCreditLimit.Enabled = Enable;
                //tbMarginPercentage.Enabled = Enable; //karthik- Hide MarginPercentage Field
                tbHomeBase.Enabled = Enable;
                btnHomeBase.Enabled = Enable;
                tbClosestIcao.Enabled = Enable;
                btnClosestIcao.Enabled = Enable;
                chkApplication.Enabled = Enable;
                chkApproval.Enabled = Enable;
                ddlCustomerType.Enabled = Enable;

                tbBillingName.Enabled = Enable;
                tbBillingAddress1.Enabled = Enable;
                tbBillingAddress2.Enabled = Enable;
                tbBillingAddress3.Enabled = Enable;
                tbBillingCity.Enabled = Enable;
                tbBillingState.Enabled = Enable;
                tbBillingCountry.Enabled = Enable;
                btnBillingCountry.Enabled = Enable;
                tbBillingPostal.Enabled = Enable;
                tbBillingPhone.Enabled = Enable;
                tbBillingTollFreePhone.Enabled = Enable;
                tbBillingEmailAddress.Enabled = Enable;
                tbBillingWebAddress.Enabled = Enable;
                tbBillingFax.Enabled = Enable;
                btndeleteImage.Enabled = Enable;
                btndeleteImage.Visible = Enable;

                ddlImg.Enabled = Enable;
                tbImgName.Enabled = Enable;
                btndeleteImage.Enabled = Enable;
                fileUL.Enabled = Enable;

                tbNotes.Enabled = Enable;

                bool CustomerContactEnable = false;
                tbFirstName.Enabled = CustomerContactEnable;
                tbMiddleName.Enabled = CustomerContactEnable;
                tbLastName.Enabled = CustomerContactEnable;
                tbTitle.Enabled = CustomerContactEnable;
                tbAddr1.Enabled = CustomerContactEnable;
                tbAddr2.Enabled = CustomerContactEnable;
                tbAddr3.Enabled = CustomerContactEnable;
                tbCity.Enabled = CustomerContactEnable;
                tbState.Enabled = CustomerContactEnable;
                tbCountry.Enabled = CustomerContactEnable;
                tbPostal.Enabled = CustomerContactEnable;
                tbPhone.Enabled = CustomerContactEnable;
                tbBusinessPhone.Enabled = CustomerContactEnable;
                tbOtherPhone.Enabled = CustomerContactEnable;
                tbCellPhoneNum.Enabled = CustomerContactEnable;
                tbCellPhoneNum2.Enabled = CustomerContactEnable;
                tbEmail.Enabled = CustomerContactEnable;
                tbBusinessEmail.Enabled = CustomerContactEnable;
                tbPersonalEmail.Enabled = CustomerContactEnable;
                tbOtherEmail.Enabled = CustomerContactEnable;
                tbHomeFax.Enabled = CustomerContactEnable;
                tbFax.Enabled = CustomerContactEnable;
                //tbWebsite.Enabled = CustomerContactEnable;
                //tbMCBusinessEmail.Enabled = CustomerContactEnable;
                tbAdditionalContact.Enabled = CustomerContactEnable;
                tbCreditName1.Enabled = CustomerContactEnable;
                tbCreditName2.Enabled = CustomerContactEnable;
                tbCreditNum1.Enabled = CustomerContactEnable;
                tbCreditNum2.Enabled = CustomerContactEnable;

                btnSaveChanges.Visible = Enable;
                btnSaveChangesTop.Visible = Enable;
                btnCancel.Visible = Enable;
                btnCancelTop.Visible = Enable;
            }
        }

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string EmptyString = string.Empty;
                hdnSave.Value = EmptyString;

                chkIsActive.Checked = false;
                tbCode.Text = EmptyString;
                hdnCQCustomerID.Value = EmptyString;
                tbName.Text = EmptyString;
                tbDiscount.Text = EmptyString;
                tbCreditLimit.Text = "00.00";
                //tbMarginPercentage.Text = EmptyString; //karthik- Hide MarginPercentage Field
                tbHomeBase.Text = EmptyString;
                hdnHomeBaseID.Value = EmptyString;
                tbClosestIcao.Text = EmptyString;
                hdnClosestAirportID.Value = EmptyString;
                chkApplication.Checked = false;
                chkApproval.Checked = false;
                ddlCustomerType.SelectedIndex = 0;

                tbBillingName.Text = EmptyString;
                tbBillingAddress1.Text = EmptyString;
                tbBillingAddress2.Text = EmptyString;
                tbBillingAddress3.Text = EmptyString;
                tbBillingCity.Text = EmptyString;
                tbBillingState.Text = EmptyString;
                tbBillingCountry.Text = EmptyString;
                hdnBillingCountryID.Value = EmptyString;
                tbBillingPostal.Text = EmptyString;
                tbBillingPhone.Text = EmptyString;
                tbBillingTollFreePhone.Text = EmptyString;
                tbBillingEmailAddress.Text = EmptyString;
                tbBillingWebAddress.Text = EmptyString;
                tbBillingFax.Text = EmptyString;

                hdnContactName.Value = EmptyString;
                tbFirstName.Text = EmptyString;
                tbMiddleName.Text = EmptyString;
                tbLastName.Text = EmptyString;
                tbTitle.Text = EmptyString;
                tbAddr1.Text = EmptyString;
                tbAddr2.Text = EmptyString;
                tbAddr3.Text = EmptyString;
                tbCity.Text = EmptyString;
                tbState.Text = EmptyString;
                tbCountry.Text = EmptyString;
                tbPostal.Text = EmptyString;
                tbPhone.Text = EmptyString;
                tbBusinessPhone.Text = EmptyString;
                tbOtherPhone.Text = EmptyString;
                tbCellPhoneNum.Text = EmptyString;
                tbCellPhoneNum2.Text = EmptyString;
                tbEmail.Text = EmptyString;
                //tbBusinessEmail.Text = EmptyString;
                tbPersonalEmail.Text = EmptyString;
                tbOtherEmail.Text = EmptyString;
                tbHomeFax.Text = EmptyString;
                tbFax.Text = EmptyString;
                //tbWebsite.Text = EmptyString;
                //tbMCBusinessEmail.Text = EmptyString;
                tbAdditionalContact.Text = EmptyString;
                tbCreditName1.Text = EmptyString;
                tbCreditName2.Text = EmptyString;
                tbCreditNum1.Text = EmptyString;
                tbCreditNum2.Text = EmptyString;

                tbNotes.Text = EmptyString;

                ddlImg.Enabled = false;
                imgFile.ImageUrl = "";
                ImgPopup.ImageUrl = null;
                lnkFileName.NavigateUrl = "";
                ddlImg.Items.Clear();
                ddlImg.Text = "";
                tbImgName.Text = "";

                //chkSearchHomebaseOnly.Checked = false;
                //chkSearchActiveOnly.Checked = false;
            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();
                CQCustomer oCQCustomer = new CQCustomer();
                if (Session["CQCustomerID"] != null)
                {
                    oCQCustomer.CQCustomerID = Convert.ToInt64(Session["CQCustomerID"].ToString());
                }
                List<GetCQCustomerByCQCustomerID> CQCustomerList = new List<GetCQCustomerByCQCustomerID>();
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    CQCustomerList = objService.GetCQCustomerByCQCustomerID(oCQCustomer).EntityList;
                    if (CQCustomerList.Count != 0)
                    {
                        if (CQCustomerList[0].CQCustomerID != null)
                        {
                            hdnCQCustomerID.Value = CQCustomerList[0].CQCustomerID.ToString().Trim();
                        }
                        if (CQCustomerList[0].CQCustomerCD != null)
                        {
                            tbCode.Text = CQCustomerList[0].CQCustomerCD.ToString().Trim();
                        }
                        if (CQCustomerList[0].CQCustomerName != null)
                        {
                            tbName.Text = CQCustomerList[0].CQCustomerName.ToString().Trim();
                        }
                        if (CQCustomerList[0].IsInActive != null)
                        {
                            if (CQCustomerList[0].IsInActive.Value == true)
                            {
                                chkIsActive.Checked = false;
                            }
                            else
                            {
                                chkIsActive.Checked = true;
                            }
                        }
                        else
                        {
                            chkIsActive.Checked = false;
                        }
                        if (CQCustomerList[0].IsApplicationFiled != null)
                        {
                            chkApplication.Checked = CQCustomerList[0].IsApplicationFiled.Value;
                        }
                        if (CQCustomerList[0].IsApproved != null)
                        {
                            chkApproval.Checked = CQCustomerList[0].IsApproved.Value;
                        }
                        if (CQCustomerList[0].BillingName != null)
                        {
                            tbBillingName.Text = CQCustomerList[0].BillingName.ToString().Trim();
                        }
                        if (CQCustomerList[0].BillingAddr1 != null)
                        {
                            tbBillingAddress1.Text = CQCustomerList[0].BillingAddr1.ToString().Trim();
                        }
                        if (CQCustomerList[0].BillingAddr2 != null)
                        {
                            tbBillingAddress2.Text = CQCustomerList[0].BillingAddr2.ToString().Trim();
                        }
                        if (CQCustomerList[0].BillingAddr3 != null)
                        {
                            tbBillingAddress3.Text = CQCustomerList[0].BillingAddr3.ToString().Trim();
                        }
                        if (CQCustomerList[0].BillingCity != null)
                        {
                            tbBillingCity.Text = CQCustomerList[0].BillingCity.ToString().Trim();
                        }
                        if (CQCustomerList[0].BillingState != null)
                        {
                            tbBillingState.Text = CQCustomerList[0].BillingState.ToString().Trim();
                        }
                        if (CQCustomerList[0].BillingZip != null)
                        {
                            tbBillingPostal.Text = CQCustomerList[0].BillingZip.ToString().Trim();
                        }
                        if (CQCustomerList[0].CountryID != null)
                        {
                            hdnBillingCountryID.Value = CQCustomerList[0].CountryID.ToString().Trim();
                        }
                        if (CQCustomerList[0].CountryCD != null)
                        {
                            tbBillingCountry.Text = CQCustomerList[0].CountryCD.ToString().Trim();
                        }
                        if (CQCustomerList[0].CountryName != null)
                        {
                            //CQCustomerIDList[0].CountryName.ToString().Trim();
                        }
                        if (CQCustomerList[0].BillingPhoneNum != null)
                        {
                            tbBillingPhone.Text = CQCustomerList[0].BillingPhoneNum.ToString().Trim();
                        }
                        if (CQCustomerList[0].BillingFaxNum != null)
                        {
                            tbBillingFax.Text = CQCustomerList[0].BillingFaxNum.ToString().Trim();
                        }
                        if (CQCustomerList[0].Notes != null)
                        {
                            tbNotes.Text = CQCustomerList[0].Notes.ToString().Trim();
                        }
                        if (CQCustomerList[0].Credit != null)
                        {
                            tbCreditLimit.Text = CQCustomerList[0].Credit.ToString().Trim();
                        }
                        //if (CQCustomerList[0].MarginalPercentage != null) //karthik- Hide MarginPercentage Field
                        //{
                        //    tbMarginPercentage.Text = CQCustomerList[0].MarginalPercentage.ToString().Trim();
                        //}
                        if (CQCustomerList[0].DiscountPercentage != null)
                        {
                            tbDiscount.Text = CQCustomerList[0].DiscountPercentage.ToString().Trim();
                        }
                        if (CQCustomerList[0].HomebaseID != null)
                        {
                            hdnHomeBaseID.Value = CQCustomerList[0].HomebaseID.ToString().Trim();
                        }
                        if (CQCustomerList[0].HomeBaseIcaoID != null)
                        {
                            tbHomeBase.Text = CQCustomerList[0].HomeBaseIcaoID.ToString().Trim();
                            //CQCustomerIDList[0].BaseDescription
                        }
                        if (CQCustomerList[0].AirportID != null)
                        {
                            hdnClosestAirportID.Value = CQCustomerList[0].AirportID.ToString().Trim();
                        }
                        if (CQCustomerList[0].AirportIcaoID != null)
                        {
                            tbClosestIcao.Text = CQCustomerList[0].AirportIcaoID.ToString().Trim();
                        }
                        if (CQCustomerList[0].DateAddedDT != null)
                        {
                            // CQCustomerIDList[0].DateAddedDT.ToString().Trim();
                        }
                        if (CQCustomerList[0].WebAddress != null)
                        {
                            tbBillingWebAddress.Text = CQCustomerList[0].WebAddress.ToString().Trim();
                        }
                        if (CQCustomerList[0].EmailID != null)
                        {
                            tbBillingEmailAddress.Text = CQCustomerList[0].EmailID.ToString().Trim();
                        }
                        if (CQCustomerList[0].TollFreePhone != null)
                        {
                            tbBillingTollFreePhone.Text = CQCustomerList[0].TollFreePhone.ToString().Trim();
                        }
                        if (CQCustomerList[0].CustomerType != null)
                        {
                            ddlCustomerType.SelectedValue = CQCustomerList[0].CustomerType.ToString().Trim();
                        }


                        Label lbLastUpdatedUser;
                        lbLastUpdatedUser = (Label)dgCQCustomer.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (CQCustomerList[0].LastUpdUID != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + CQCustomerList[0].LastUpdUID.ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (CQCustomerList[0].LastUpdTS != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(CQCustomerList[0].LastUpdTS.ToString())));
                        }
                        //CQCustomerIDList[0].IsDeleted
                        lbColumnName1.Text = System.Web.HttpUtility.HtmlEncode("Code");
                        lbColumnName2.Text = System.Web.HttpUtility.HtmlEncode("Customer Name");
                        lbColumnValue1.Text = System.Web.HttpUtility.HtmlEncode(CQCustomerList[0].CQCustomerCD.ToString());
                        lbColumnValue2.Text = System.Web.HttpUtility.HtmlEncode(CQCustomerList[0].CQCustomerName.ToString());

                        #region Get Image from Database
                        CreateDictionayForImgUpload();
                        imgFile.ImageUrl = "";
                        ImgPopup.ImageUrl = null;
                        lnkFileName.NavigateUrl = "";
                        using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ReturnImage = ImgService.GetFileWarehouseList("CharterQuoteCustomer", Convert.ToInt64(Session["CQCustomerID"].ToString())).EntityList;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            int i = 0;
                            Session["Base64"] = null;   //added for image issue
                            foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImage)
                            {
                                Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                                ddlImg.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(FWH.UWAFileName), FWH.UWAFileName));
                                DicImage.Add(FWH.UWAFileName, FWH.UWAFilePath);
                                if (i == 0)
                                {
                                    byte[] picture = FWH.UWAFilePath;

                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(FWH.UWAFileName, picture);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = picture;
                                    //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?cd=" + Guid.NewGuid());
                                    //}
                                    ////end of modification for image issue
                                }
                                i = i + 1;
                            }
                            if (ddlImg.Items.Count > 0)
                            {
                                ddlImg.SelectedIndex = 0;
                            }
                            else
                            {
                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                lnkFileName.NavigateUrl = "";
                            }
                        }
                        #endregion
                    }
                    LoadControlContactData();
                }
            }
        }

        private void LoadControlContactData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                CQCustomerContact oCQCustomerContact = new CQCustomerContact();
                if (Session["CQCustomerID"] != null)
                {
                    oCQCustomerContact.CQCustomerID = Convert.ToInt64(Session["CQCustomerID"].ToString());
                }
                List<GetCQCustomerContactByCQCustomerID> CustomerContactList = new List<GetCQCustomerContactByCQCustomerID>();
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    CustomerContactList = objService.GetCQCustomerContactByCQCustomerID(oCQCustomerContact).EntityList.Where(x => x.IsChoice == true).ToList();
                    if (CustomerContactList.Count != 0)
                    {
                        if (CustomerContactList[0].ContactName != null)
                        {
                            hdnContactName.Value = CustomerContactList[0].CQCustomerID.ToString().Trim();
                        }
                        if (CustomerContactList[0].FirstName != null)
                        {
                            tbFirstName.Text = CustomerContactList[0].FirstName.ToString().Trim();
                        }
                        if (CustomerContactList[0].MiddleName != null)
                        {
                            tbMiddleName.Text = CustomerContactList[0].MiddleName.ToString().Trim();
                        }
                        if (CustomerContactList[0].LastName != null)
                        {
                            tbLastName.Text = CustomerContactList[0].LastName.ToString().Trim();
                        }

                        if (CustomerContactList[0].Title != null)
                        {
                            tbTitle.Text = CustomerContactList[0].Title.ToString().Trim();
                        }
                        if (CustomerContactList[0].Addr1 != null)
                        {
                            tbAddr1.Text = CustomerContactList[0].Addr1.ToString().Trim();
                        }
                        if (CustomerContactList[0].Addr2 != null)
                        {
                            tbAddr2.Text = CustomerContactList[0].Addr2.ToString().Trim();
                        }
                        if (CustomerContactList[0].Addr3 != null)
                        {
                            tbAddr3.Text = CustomerContactList[0].Addr3.ToString().Trim();
                        }
                        if (CustomerContactList[0].CityName != null)
                        {
                            tbCity.Text = CustomerContactList[0].CityName.ToString().Trim();
                        }
                        if (CustomerContactList[0].StateName != null)
                        {
                            tbState.Text = CustomerContactList[0].StateName.ToString().Trim();
                        }
                        if (CustomerContactList[0].CountryCD != null)
                        {
                            tbCountry.Text = CustomerContactList[0].CountryCD.ToString().Trim();
                        }
                        if (CustomerContactList[0].PostalZipCD != null)
                        {
                            tbPostal.Text = CustomerContactList[0].PostalZipCD.ToString().Trim();
                        }
                        if (CustomerContactList[0].PhoneNum != null)
                        {
                            tbPhone.Text = CustomerContactList[0].PhoneNum.ToString().Trim();
                        }
                        if (CustomerContactList[0].BusinessPhoneNum != null)
                        {
                            tbBusinessPhone.Text = CustomerContactList[0].BusinessPhoneNum.ToString().Trim();
                        }
                        if (CustomerContactList[0].OtherPhoneNum != null)
                        {
                            tbOtherPhone.Text = CustomerContactList[0].OtherPhoneNum.ToString().Trim();
                        }
                        if (CustomerContactList[0].PrimaryMobileNum != null)
                        {
                            tbCellPhoneNum.Text = CustomerContactList[0].PrimaryMobileNum.ToString().Trim();
                        }
                        if (CustomerContactList[0].SecondaryMobileNum != null)
                        {
                            tbCellPhoneNum2.Text = CustomerContactList[0].SecondaryMobileNum.ToString().Trim();
                        }
                        if (CustomerContactList[0].EmailID != null)
                        {
                            tbEmail.Text = CustomerContactList[0].EmailID.ToString().Trim();
                        }
                        if (CustomerContactList[0].PersonalEmailID != null)
                        {
                            tbPersonalEmail.Text = CustomerContactList[0].PersonalEmailID.ToString().Trim();
                        }
                        if (CustomerContactList[0].OtherEmailID != null)
                        {
                            tbOtherEmail.Text = CustomerContactList[0].OtherEmailID.ToString().Trim();
                        }
                        if (CustomerContactList[0].FaxNum != null)
                        {
                            tbHomeFax.Text = CustomerContactList[0].FaxNum.ToString().Trim();
                        }
                        if (CustomerContactList[0].BusinessFaxNum != null)
                        {
                            tbFax.Text = CustomerContactList[0].BusinessFaxNum.ToString().Trim();
                        }
                        if (CustomerContactList[0].BusinessEmailID != null)
                        {
                            tbBusinessEmail.Text = CustomerContactList[0].BusinessEmailID.ToString().Trim();
                        }
                        // need to modify on this
                        if (CustomerContactList[0].Notes != null)
                        {
                            tbNotes.Text = CustomerContactList[0].Notes.ToString().Trim();
                        }
                        if (CustomerContactList[0].CreditName1 != null)
                        {
                            tbCreditName1.Text = CustomerContactList[0].CreditName1.ToString().Trim();
                        }
                        if (CustomerContactList[0].CreditName2 != null)
                        {
                            tbCreditName2.Text = CustomerContactList[0].CreditName2.ToString().Trim();
                        }
                        if (CustomerContactList[0].CreditNum1 != null)
                        {
                            tbCreditNum1.Text = CustomerContactList[0].CreditNum1.ToString().Trim();
                        }
                        if (CustomerContactList[0].CreditNum2 != null)
                        {
                            tbCreditNum2.Text = CustomerContactList[0].CreditNum2.ToString().Trim();
                        }
                        if (CustomerContactList[0].MoreInfo != null)
                        {
                            tbAdditionalContact.Text = CustomerContactList[0].MoreInfo.ToString().Trim();
                        }
                    }
                }
            }
        }

        private CQCustomer GetItems(CQCustomer oCQCustomer)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQCustomer))
            {
                if (hdnSave.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnCQCustomerID.Value))
                    {
                        oCQCustomer.CQCustomerID = Convert.ToInt64(hdnCQCustomerID.Value);
                    }
                }
                if (!string.IsNullOrEmpty(tbCode.Text.Trim()))
                {
                    oCQCustomer.CQCustomerCD = tbCode.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbName.Text.Trim()))
                {
                    oCQCustomer.CQCustomerName = tbName.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbDiscount.Text.Trim()))
                {
                    oCQCustomer.DiscountPercentage = Convert.ToDecimal(tbDiscount.Text.Trim());
                }
                if (!string.IsNullOrEmpty(tbCreditLimit.Text.Trim()))
                {
                    oCQCustomer.Credit = Convert.ToDecimal(tbCreditLimit.Text.Trim());
                }
                //if (!string.IsNullOrEmpty(tbMarginPercentage.Text.Trim())) //karthik- Hide MarginPercentage Field
                //{
                //    oCQCustomer.MarginalPercentage = Convert.ToDecimal(tbMarginPercentage.Text.Trim());
                //}
                if (!string.IsNullOrEmpty(hdnHomeBaseID.Value))
                {
                    oCQCustomer.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                }
                if (!string.IsNullOrEmpty(hdnClosestAirportID.Value))
                {
                    oCQCustomer.AirportID = Convert.ToInt64(hdnClosestAirportID.Value);
                }
                if (chkIsActive.Checked == true)
                {
                    oCQCustomer.IsInActive = false;
                }
                else
                {
                    oCQCustomer.IsInActive = true;
                }
                oCQCustomer.IsApplicationFiled = chkApplication.Checked;
                oCQCustomer.IsApproved = chkApproval.Checked;
                oCQCustomer.IsDeleted = false;

                if (!string.IsNullOrEmpty(tbBillingName.Text.Trim()))
                {
                    oCQCustomer.BillingName = tbBillingName.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBillingAddress1.Text.Trim()))
                {
                    oCQCustomer.BillingAddr1 = tbBillingAddress1.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBillingAddress2.Text.Trim()))
                {
                    oCQCustomer.BillingAddr2 = tbBillingAddress2.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBillingAddress3.Text.Trim()))
                {
                    oCQCustomer.BillingAddr3 = tbBillingAddress3.Text.Trim();
                }

                if (!string.IsNullOrEmpty(tbBillingCity.Text.Trim()))
                {
                    oCQCustomer.BillingCity = tbBillingCity.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBillingState.Text.Trim()))
                {
                    oCQCustomer.BillingState = tbBillingState.Text.Trim();
                }
                if (!string.IsNullOrEmpty(hdnBillingCountryID.Value))
                {
                    oCQCustomer.CountryID = Convert.ToInt64(hdnBillingCountryID.Value);
                }
                if (!string.IsNullOrEmpty(tbBillingPostal.Text.Trim()))
                {
                    oCQCustomer.BillingZip = tbBillingPostal.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBillingPhone.Text.Trim()))
                {
                    oCQCustomer.BillingPhoneNum = tbBillingPhone.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBillingTollFreePhone.Text.Trim()))
                {
                    oCQCustomer.TollFreePhone = tbBillingTollFreePhone.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBillingEmailAddress.Text.Trim()))
                {
                    oCQCustomer.EmailID = tbBillingEmailAddress.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBillingWebAddress.Text.Trim()))
                {
                    oCQCustomer.WebAddress = tbBillingWebAddress.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBillingFax.Text.Trim()))
                {
                    oCQCustomer.BillingFaxNum = tbBillingFax.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbNotes.Text.Trim()))
                {
                    oCQCustomer.Notes = tbNotes.Text.Trim();
                }
                if (ddlCustomerType.SelectedIndex != 0)
                {
                    oCQCustomer.CustomerType = ddlCustomerType.SelectedValue.ToString();
                }
                return oCQCustomer;
            }
        }

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                lnkCQCustomerContacts.Enabled = false;
                lnkCQCustomerContacts.CssClass = "fleet_link_disable";
                chkSearchActiveOnly.Enabled = false;
                chkSearchHomebaseOnly.Enabled = false;
            }
        }

        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                lnkCQCustomerContacts.Enabled = true;
                lnkCQCustomerContacts.CssClass = "fleet_link";
                chkSearchActiveOnly.Enabled = true;
                chkSearchHomebaseOnly.Enabled = true;
            }
        }

        #region "Grid Events"

        protected void dgCQCustomer_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        protected void dgCQCustomer_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCQCustomerCatalogService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            CQCustomer oCQCustomer = new CQCustomer();
                            List<GetCQCustomer> CQCustomerlist = new List<GetCQCustomer>();
                            var objCQCustomer = objCQCustomerCatalogService.GetCQCustomer(oCQCustomer);
                            if (objCQCustomer.EntityList != null && objCQCustomer.EntityList.Count > 0)
                            {
                                CQCustomerlist = objCQCustomer.EntityList;
                            }
                            dgCQCustomer.DataSource = CQCustomerlist;
                            Session["CQCustomer"] = CQCustomerlist;
                            if (!IsPopUp)
                                SearchFilter(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        protected void dgCQCustomer_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgCQCustomer.SelectedIndexes.Clear();
                                pnlFilterForm.Enabled = false;
                                hdnSave.Value = "Save";
                                ClearForm();
                                EnableForm(true);
                                if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportICAOCd != string.Empty)
                                {
                                    tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd;
                                    if (UserPrincipal != null && UserPrincipal.Identity._airportId != 0)
                                    {
                                        hdnHomeBaseID.Value = Convert.ToString(UserPrincipal.Identity._airportId);
                                    }
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;
                                }
                                chkIsActive.Checked = true;
                                GridEnable(true, false, false);
                                tbCode.Focus();
                                DisableLinks();
                                tbImgName.Enabled = true;
                                ddlImg.Enabled = true;
                                btndeleteImage.Enabled = true;
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["CQCustomerID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CQCustomer, Convert.ToInt64(Session["CQCustomerID"].ToString().Trim()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.CQCustomer);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CQCustomer);
                                            //DefaultSelectItem();
                                            return;
                                        }
                                        pnlFilterForm.Enabled = false;
                                        LoadControlData();
                                        hdnSave.Value = "Update";
                                        hdnRedirect.Value = "";
                                        EnableForm(true);
                                        GridEnable(false, true, false);
                                        ddlImg.Enabled = true;
                                        if (imgFile.ImageUrl.Trim() != "" || lnkFileName.NavigateUrl.Trim() != "")
                                        {
                                            btndeleteImage.Enabled = true;
                                        }
                                        tbImgName.Enabled = true;
                                        tbName.Focus();
                                        DefaultSelectItem();
                                        DisableLinks();
                                    }
                                }
                                break;

                            case "Filter":
                                foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        protected void dgCQCustomer_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        IsValidate = ValidateAll();
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                CQCustomer oCQCustomer = new CQCustomer();
                                oCQCustomer = GetItems(oCQCustomer);
                                var objResult = objService.AddCQCustomer(oCQCustomer);
                                if (objResult.ReturnFlag == true)
                                {
                                    #region Image Upload in Edit Mode
                                    oCQCustomer.CQCustomerID = objResult.EntityInfo.CQCustomerID;
                                    if (Session["DicImg"] != null)
                                    {
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        using (FlightPakMasterService.MasterCatalogServiceClient ImgobjService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            foreach (var DicItem in dicImg)
                                            {
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "CharterQuoteCustomer";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(oCQCustomer.CQCustomerID);
                                                Service.UWAWebpageName = "CharterQuoteCustomerCatalog.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                objService.AddFWHType(Service);
                                            }
                                        }
                                    }
                                    #endregion
                                    dgCQCustomer.Rebind();
                                    DefaultSelection(false);
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objResult.ErrorMessage, ModuleNameConstants.Database.CQCustomer);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            if (!String.IsNullOrEmpty(Request.QueryString["CQCustomerID"]) || Request.QueryString["CQCustomerID"] == "-1")
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCQCPopups');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        protected void dgCQCustomer_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        IsValidate = ValidateAll();
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                CQCustomer oCQCustomer = new CQCustomer();
                                oCQCustomer = GetItems(oCQCustomer);
                                var objResult = objService.UpdateCQCustomer(oCQCustomer);
                                if (objResult.ReturnFlag == true)
                                {
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    #region Image Upload in Edit Mode
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    using (FlightPakMasterService.MasterCatalogServiceClient ImgobjService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        foreach (var DicItem in dicImg)
                                        {
                                            //objService = new FlightPakMasterService.MasterCatalogServiceClient();
                                            FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                            Service.FileWarehouseID = 0;
                                            Service.RecordType = "CharterQuoteCustomer";
                                            Service.UWAFileName = DicItem.Key;
                                            Service.RecordID = Convert.ToInt64(oCQCustomer.CQCustomerID);
                                            Service.UWAWebpageName = "CharterQuoteCustomerCatalog.aspx";
                                            Service.UWAFilePath = DicItem.Value;
                                            Service.IsDeleted = false;
                                            Service.FileWarehouseID = 0;
                                            objService.AddFWHType(Service);
                                        }
                                    }
                                    #endregion
                                    #region Image Upload in Delete
                                    using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                        Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                        foreach (var DicItem in dicImgDelete)
                                        {
                                            objFWHType.UWAFileName = DicItem.Key;
                                            objFWHType.RecordID = Convert.ToInt64(oCQCustomer.CQCustomerID);
                                            objFWHType.RecordType = "CharterQuoteCustomerCatalog.aspx";
                                            objFWHType.IsDeleted = true;
                                            objFWHTypeService.DeleteFWHType(objFWHType);
                                        }
                                    }
                                    #endregion
                                    ///////Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        if (Session["CQCustomerID"] != null)
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.CQCustomer, Convert.ToInt64(Session["CQCustomerID"].ToString().Trim()));
                                        }
                                    }
                                    GridEnable(true, true, true);
                                    DefaultSelectItem();
                                    LoadControlData();
                                    EnableForm(false);
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objResult.ErrorMessage, ModuleNameConstants.Database.CQCustomer);
                                }
                            }

                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                Session.Remove("CQCustomerID");
                                if (!String.IsNullOrEmpty(Request.QueryString["CQCustomerID"]) || Request.QueryString["CQCustomerID"] == "-1")
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                else
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCQCPopups');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        protected void dgCQCustomer_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CQCustomerID"] != null)
                        {
                            using (MasterCatalogServiceClient CQCustomerService = new MasterCatalogServiceClient())
                            {
                                CQCustomer oCQCustomer = new CQCustomer();
                                oCQCustomer.CQCustomerID = Convert.ToInt64(Session["CQCustomerID"].ToString());
                                oCQCustomer.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.CQCustomer, oCQCustomer.CQCustomerID);
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.CQCustomer);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CQCustomer);
                                        return;
                                    }
                                }
                                CQCustomerService.DeleteCQCustomer(oCQCustomer);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                dgCQCustomer.Rebind();
                                DefaultSelection(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.CQCustomer, Convert.ToInt64(Session["CQCustomerID"].ToString()));
                    }
                }
            }
        }

        protected void dgCQCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                DefaultSelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if ((btnSaveChanges.Visible == false) && (btnSaveChangesTop.Visible == false))
                            {
                                GridDataItem Item = dgCQCustomer.SelectedItems[0] as GridDataItem;
                                pnlFilterForm.Enabled = true;
                                Session["CQCustomerID"] = Item["CQCustomerID"].Text;
                                LoadControlData();
                                EnableForm(false);
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                    }
                }
            }
        }

        protected void dgCQCustomer_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCQCustomer, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        protected void dgCQCustomer_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCQCustomer.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        #endregion

        #region "Validations"

        private bool ValidateAll()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnCustomValidateReturn = true;
                if (hdnSave.Value == "Save")
                {
                    if (CheckCodeExist() == false)
                    {
                        return ReturnCustomValidateReturn = false;
                    }
                }
                if (CheckHomeBaseExist() == false)
                {
                    return ReturnCustomValidateReturn = false;
                }
                if (CheckClosestIcaoExist() == false)
                {
                    return ReturnCustomValidateReturn = false;
                }
                if (CheckBillingCountryExist() == false)
                {
                    return ReturnCustomValidateReturn = false;
                }
                return ReturnCustomValidateReturn;
            }
        }

        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        return CheckCodeExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }
        /// <summary>
        /// To check unique code already exists
        /// </summary>
        /// <returns></returns>
        private bool CheckCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(tbCode.Text))
                {
                    CQCustomer objCQCustomer = new CQCustomer();
                    objCQCustomer.CQCustomerCD = tbCode.Text.Trim();
                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objservice.GetCQCustomerByCQCustomerCD(objCQCustomer).EntityList;
                        if (objRetVal.Count() != 0)
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            ReturnValue = false;
                        }
                        else
                        {
                            ReturnValue = true;
                            RadAjaxManager1.FocusControl(tbName.ClientID);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbName.ClientID + "');", true);
                        }
                    }
                }
                else
                {
                    rfvCode.IsValid = false;
                    RadAjaxManager1.FocusControl(tbCode.ClientID);
                    ReturnValue = false;
                }
                return ReturnValue;
            }
        }

        /// <summary>
        /// To check unique Home Base  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        return CheckHomeBaseExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }
        private bool CheckHomeBaseExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(tbHomeBase.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<GetAllCompanyMaster> AirportList = new List<GetAllCompanyMaster>();
                        var objRetVal = objservice.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.Trim().ToUpper() == (tbHomeBase.Text.Trim().ToUpper())).ToList();
                        if (objRetVal.Count() != 0)
                        {
                            AirportList = (List<GetAllCompanyMaster>)objRetVal;
                            hdnHomeBaseID.Value = AirportList[0].HomebaseID.ToString();
                            tbHomeBase.Text = AirportList[0].HomebaseCD;
                            RadAjaxManager1.FocusControl(tbClosestIcao.ClientID);
                            ReturnValue = true;
                        }
                        else
                        {
                            cvHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbHomeBase.ClientID);
                            ReturnValue = false;
                        }
                    }
                }
                else {
                    hdnHomeBaseID.Value = "";
                }
                return ReturnValue;
            }
        }

        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbClosestIcao_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        return CheckClosestIcaoExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }
        private bool CheckClosestIcaoExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(tbClosestIcao.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<GetAllAirport> AirportList = new List<GetAllAirport>();
                        var objRetVal = objservice.GetAirportByAirportICaoID(tbClosestIcao.Text).EntityList;
                        if (objRetVal.Count() != 0)
                        {
                            AirportList = (List<GetAllAirport>)objRetVal;
                            hdnClosestAirportID.Value = AirportList[0].AirportID.ToString();
                            tbClosestIcao.Text = AirportList[0].IcaoID;
                            RadAjaxManager1.FocusControl(chkApplication.ClientID);
                            ReturnValue = true;
                        }
                        else
                        {
                            cvClosestIcao.IsValid = false;
                            RadAjaxManager1.FocusControl(tbClosestIcao.ClientID);
                            ReturnValue = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }

        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbBillingCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        return CheckBillingCountryExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }
        private bool CheckBillingCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(tbBillingCountry.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<Country> CountryList = new List<Country>();
                        var objRetVal = objservice.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper().Equals(tbBillingCountry.Text.ToString().ToUpper().Trim())).ToList();
                        if (objRetVal.Count() != 0)
                        {
                            CountryList = (List<Country>)objRetVal;
                            hdnBillingCountryID.Value = CountryList[0].CountryID.ToString();
                            tbBillingCountry.Text = CountryList[0].CountryCD;
                            RadAjaxManager1.FocusControl(tbBillingPostal.ClientID);
                            ReturnValue = true;
                        }
                        else
                        {
                            cvBillingCountry.IsValid = false;
                            RadAjaxManager1.FocusControl(tbBillingCountry.ClientID);
                            ReturnValue = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }

        #endregion

        /// <summary>
        /// Command Event Trigger for Save or Update CQCustomer Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgCQCustomer.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {

                            bool isCustomerCodeExist = false;

                            //Checking customer code is exist or not.
                            isCustomerCodeExist = CheckCodeExist();

                            //If true then add customer with uniq code
                            if (isCustomerCodeExist)
                            {
                                (dgCQCustomer.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        /// <summary>
        /// Cancel CQCustomer Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["CQCustomerID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.CQCustomer, Convert.ToInt64(Session["CQCustomerID"].ToString().Trim()));
                                    DefaultSelection(false);
                                }
                                if (IsPopUp)
                                {
                                    if (!String.IsNullOrEmpty(Request.QueryString["CQCustomerID"]) || Request.QueryString["CQCustomerID"] == "-1")
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                    else
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCQCPopups');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
            DefaultSelection(false);
            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        protected void lnkCQCustomerContacts_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CQCustomerID"] != null)
                        {
                            //GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                            if (dgCQCustomer.Items.Count > 0)
                            {
                                string CQCustomerID = Session["CQCustomerID"].ToString();
                                Session["CQCustomerContactID"] = null;
                                RedirectToPage("CharterQuoteCustomerContactCatalog.aspx?Screen=CharterQuote&CQCustomerID=" + Microsoft.Security.Application.Encoder.UrlEncode(CQCustomerID) + "&CQCustomerCode=" + Microsoft.Security.Application.Encoder.UrlEncode(tbCode.Text) + "&CQCustomerName=" + Microsoft.Security.Application.Encoder.UrlEncode(tbName.Text));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        #region "Search And Filter"

        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }

        protected void SearchFilter(bool IsDataBind)
        {
            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
            {
                CQCustomer objCQCustomer = new CQCustomer();
                //var objGetCQCustomer = objService.GetCQCustomer(objCQCustomer);
                List<FlightPakMasterService.GetCQCustomer> CQCustomerList = new List<FlightPakMasterService.GetCQCustomer>();
                if (Session["CQCustomer"] != null)
                {
                    CQCustomerList = (List<FlightPakMasterService.GetCQCustomer>)Session["CQCustomer"];
                }
                if (CQCustomerList.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { CQCustomerList = CQCustomerList.Where(x => x.IsInActive == false).ToList<GetCQCustomer>(); }
                    if (chkSearchHomebaseOnly.Checked == true) { CQCustomerList = CQCustomerList.Where(x => x.HomeBaseIcaoID == UserPrincipal.Identity._airportICAOCd).ToList<GetCQCustomer>(); }
                    dgCQCustomer.DataSource = CQCustomerList.OrderBy(x => x.CQCustomerCD);
                }
                if (IsDataBind)
                {
                    //dgCQCustomer.DataBind();
                    DefaultSelection(true);
                }

            }
        }

        #endregion

        #region "Image Upload"
        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                            //code commented by viswa for image issue
                            //else if (eventArgument == "DeleteImage_Click")
                            //{
                            //    DeleteImage_Click(sender, e);
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCQCustomer.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgCQCustomer.Rebind();
                    DefaultSelectItem();
                    DefaultSelection(false);
                }
            }
        }
        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> DicImgage = new Dictionary<string, byte[]>();
                Session["DicImg"] = DicImgage;
                Dictionary<string, string> DicImageDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = DicImageDelete;
            }
        }
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                        int count = DicImage.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            DicImage.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = DicImage;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                            {
                                dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            }
                            Session["DicImgDelete"] = dicImgDelete;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in DicImage)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            if (ddlImg.Items.Count > 0)
                            {
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                Session["Base64"] = null;   //added for image issue
                                if (Count > 0)
                                {
                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                            }
                            else
                            {
                                imgFile.ImageUrl = "../../../App_Themes/Default/images/noimage.jpg";
                                lnkFileName.NavigateUrl = "";
                            }
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }
        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream MyStream;
                Int32 FileLen;
                Session["Base64"] = null;   //added for image issue
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        string FileName = fileUL.FileName;
                        FileLen = fileUL.PostedFile.ContentLength;
                        Byte[] Input = new Byte[FileLen];
                        MyStream = fileUL.FileContent;
                        MyStream.Read(Input, 0, FileLen);

                        //Ramesh: Set the URL for image file or link based on the file type
                        SetURL(FileName, Input);

                        ////start of modification for image issue
                        //if (hdnBrowserName.Value == "Chrome")
                        //{
                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                        //}
                        //else
                        //{
                        //    Session["Base64"] = Input;
                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                        //}
                        ////end of modification for image issue
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                        //Commented for removing document name
                        //if (tbImgName.Text.Trim() != "")
                        //{
                        //    FileName = tbImgName.Text.Trim();
                        //}
                        int Count = DicImage.Count(D => D.Key.Equals(FileName));
                        if (Count > 0)
                        {
                            DicImage[FileName] = Input;
                        }
                        else
                        {
                            DicImage.Add(FileName, Input);
                            ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(System.Web.HttpUtility.HtmlEncode(FileName), FileName));

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);
                        }
                        tbImgName.Text = "";
                        if (ddlImg.Items.Count != 0)
                        {
                            //ddlImg.SelectedIndex = ddlImg.Items.Count - 1;
                            ddlImg.SelectedValue = FileName;
                        }
                        btndeleteImage.Enabled = true;
                    }
                }
            }
        }
        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool ImgFound = false;
                            Session["Base64"] = null;   //added for image issue
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (Session["CQCustomerID"] != null)
                                {
                                    //removed tolower conversion for image issue
                                    var ReturnImg = ImgService.GetFileWarehouseList("CharterQuoteCustomer", Convert.ToInt64(Session["CQCustomerID"].ToString())).EntityList.Where(x => x.UWAFileName == ddlImg.Text.Trim());
                                    foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImg)
                                    {
                                        ImgFound = true;
                                        byte[] Picture = FWH.UWAFilePath;

                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(FWH.UWAFileName, Picture);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Picture);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = Picture;
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }
                                }
                                //tbImgName.Text = ddlImg.Text.Trim();   //Commented for removing document name
                                if (!ImgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (Count > 0)
                                    {
                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomer);
                }
            }
        }
        #endregion

        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            //Ramesh: Code to allow any file type for upload control.
            int iIndex = filename.Trim().IndexOf('.');
            //string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);  
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            if (regMatch.Success)
            {
                if (Request.UserAgent.Contains("Chrome"))
                {
                    hdnBrowserName.Value = "Chrome";
                }
                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                else
                {
                    Session["Base64"] = filedata;
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid();
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgFile.Visible = false;
                lnkFileName.Visible = true;
                Session["Base64"] = filedata;
                lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();
            }
        }
    }
}
