﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewDutyRules_Catalog : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private bool IsEmptyCheck = true;
        private bool CrewDutyRulesPageNavigated = false;
        private string strCrewDutyRulesCD = "";
        private string strCrewDutyRulesID = "";
        private bool _selectLastModified = false;
        private string hdnCrewDutyRulesCD = string.Empty;
        private List<string> lstCrewDutyRules = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrewDutyRulesCatalog, dgCrewDutyRulesCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCrewDutyRulesCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewCrewDutyRulesCatalog);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgCrewDutyRulesCatalog.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                        if (IsPopUp)
                        {
                            dgCrewDutyRulesCatalog.AllowPaging = false;
                            chkSearchActiveOnly.Visible = false;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack && IsPopUp)
            {
                //Hide Controls
                dgCrewDutyRulesCatalog.Visible = false;
                if (IsAdd)
                {
                    (dgCrewDutyRulesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                }
                else
                {
                    (dgCrewDutyRulesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                }
            }
            
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCrewDutyRulesCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgCrewDutyRulesCatalog.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");


                if (Request.QueryString["CrewDutyRuleId"] != null)
                {
                    Session["SelectedCrewDutyRulesID"] = Request.QueryString["CrewDutyRuleId"].Trim();
                }       

            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgCrewDutyRulesCatalog.Rebind();
                    }
                    if (dgCrewDutyRulesCatalog.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedCrewDutyRulesID"] = null;
                        //}
                        if (Session["SelectedCrewDutyRulesID"] == null)
                        {
                            dgCrewDutyRulesCatalog.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                                Session["SelectedCrewDutyRulesID"] = dgCrewDutyRulesCatalog.Items[0].GetDataKeyValue("CrewDutyRulesID").ToString();
                        }

                        if (dgCrewDutyRulesCatalog.SelectedIndexes.Count == 0)
                            dgCrewDutyRulesCatalog.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To set the grid row to be selected
        /// </summary>
        private void SelectItem()
        {
            var item = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewDutyRulesID"] != null)
                    {
                        string ID = Session["SelectedCrewDutyRulesID"].ToString();
                        foreach (GridDataItem Item in dgCrewDutyRulesCatalog.MasterTableView.Items)
                        {
                            if (Item["CrewDutyRulesID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                item = true;
                                break;
                            }
                        }
                        if(!item)
                            Session["SelectedCrewDutyRulesID"] = dgCrewDutyRulesCatalog.Items[0].GetDataKeyValue("CrewDutyRulesID").ToString();
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    target.Focus();
                    IsEmptyCheck = false;
                }
            }
        }


        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            if ((e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1))
            {
                e.Updated = dgCrewDutyRulesCatalog;
            }
        }

        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyRulesCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }

        /// <summary>
        /// Bind Crew Check List Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyRulesCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            List<FlightPakMasterService.CrewDutyRules> CrewDutyRulesList = new List<CrewDutyRules>();
                            var CrewDutyRuleResult = Service.GetCrewDutyRuleList();
                            if (CrewDutyRuleResult.ReturnFlag == true)
                            {
                                CrewDutyRulesList = CrewDutyRuleResult.EntityList;
                            }
                            dgCrewDutyRulesCatalog.DataSource = CrewDutyRulesList;
                            Session["CrewDutyRulesList"] = CrewDutyRulesList;
                            Session.Remove("CrewDutyCode");
                            lstCrewDutyRules = new List<string>();
                            foreach (CrewDutyRules CrewDutyRuleEntity in Service.GetCrewDutyRuleList().EntityList)
                            {
                                lstCrewDutyRules.Add(CrewDutyRuleEntity.CrewDutyRuleCD.ToLower().Trim());
                            }
                            Session["CrewDutyCode"] = lstCrewDutyRules;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                    }
                }
            }
        }

        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSaveFlag.Value = "Save";
                        ClearForm();
                        EnableForm(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }

        /// <summary>
        /// Item Command for Crew Check List Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyRulesCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                if (Session["SelectedCrewDutyRulesID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CrewDutyRules, Convert.ToInt64(Session["SelectedCrewDutyRulesID"].ToString().Trim()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.CrewDutyRules);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewDutyRules);
                                            SelectItem();                                            
                                            return;
                                        }
                                        SelectItem();
                                        DisplayEditForm();
                                        DisableLinks();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;

                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                dgCrewDutyRulesCatalog.SelectedIndexes.Clear();
                                tbCode.Focus();
                                DisableLinks();
                                break;

                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }

        /// <summary>
        /// Update Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyRulesCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Session["SelectedCrewDutyRulesID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient TripChecklistService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = TripChecklistService.UpdateCrewDutyRule(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.CrewDutyRules, Convert.ToInt64(Session["SelectedCrewDutyRulesID"].ToString().Trim()));
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        SelectItem();
                                        ReadOnlyForm();
                                    }

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.Account);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            // Session["SelectedCrewDutyRulesID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "self.parent.GetLastInsertedID('" + hdnCrewDutyRulesCD + "');var oWnd = GetRadWindow(); oWnd.close();", true);
                            hdnCrewDutyRulesCD = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }

        /// <summary>
        /// To check whether the crew code exists already
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyExist()
        {
            //bool ReturnValue = false;
            //try
            //{
            //    FlightPakMasterService.MasterCatalogServiceClient CrewDutyRuleService = new FlightPakMasterService.MasterCatalogServiceClient();
            //    var CrewDutyRuleList = CrewDutyRuleService.GetCrewDutyRuleList().EntityList.Where(x => x.CrewDutyRuleCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
            //    if (CrewDutyRuleList.Count() > 0 && CrewDutyRuleList != null)
            //    {
            //        foreach (FlightPakMasterService.CrewDutyRules Item in CrewDutyRuleList)
            //        {
            //            cvCode.IsValid = false;
            //            tbCode.Focus();
            //            ReturnValue = true;
            //            break;
            //        }
            //    }
            //}
            //catch (System.Exception exc)
            //{
            //    throw exc;
            //}
            //return ReturnValue;


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        using (FlightPakMasterService.MasterCatalogServiceClient CrewDutyRuleService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = CrewDutyRuleService.GetCrewDutyRuleList().EntityList.Where(x => x.CrewDutyRuleCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                            if (objRetVal.Count() > 0 && objRetVal != null)
                            {
                                cvCode.IsValid = false;
                                tbCode.Focus();
                                returnVal = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
                return returnVal;
            }

        }

        /// <summary>
        /// Insert Command for Crew Duty Rule Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyRulesCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objService.AddCrewDutyRule(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    dgCrewDutyRulesCatalog.Rebind();
                                    GridEnable(true, true, true);
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.Account);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    if (IsPopUp)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "self.parent.GetLastInsertedID('" + hdnCrewDutyRulesCD + "');var oWnd = GetRadWindow(); oWnd.close();", true);
                        hdnCrewDutyRulesCD = string.Empty;
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyRulesCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            //try
            //{
            //    e.Canceled = true;
            //    if (Session["SelectedItem"] != null)
            //    {
            //        FlightPakMasterService.MasterCatalogServiceClient CrewDutyRuleService = new FlightPakMasterService.MasterCatalogServiceClient();
            //        FlightPakMasterService.CrewDutyRules CrewDutyRule = new FlightPakMasterService.CrewDutyRules();
            //        GridDataItem Item = (GridDataItem)Session["SelectedItem"];
            //        CrewDutyRule.CrewDutyRulesID = Convert.ToInt64(Item.GetDataKeyValue("CrewDutyRulesID"));
            //        CrewDutyRule.CrewDutyRuleCD = Item.GetDataKeyValue("CrewDutyRuleCD").ToString();
            //        CrewDutyRule.CrewDutyRulesDescription = Item.GetDataKeyValue("CrewDutyRulesDescription").ToString();
            //        CrewDutyRule.LastUptTM = System.DateTime.Now;
            //        CrewDutyRule.IsDeleted = true;
            //        CrewDutyRule.LastUpdUID = "UC";
            //        CrewDutyRuleService.DeleteCrewDutyRule(CrewDutyRule);
            //        e.Item.OwnerTableView.Rebind();
            //        e.Item.Selected = true;
            //        DefaultSelection();
            //    }
            //}
            //catch (System.Exception exc)
            //{
            //    throw exc;
            //}



            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["SelectedCrewDutyRulesID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CrewDutyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.CrewDutyRules CrewDuty = new FlightPakMasterService.CrewDutyRules();
                                    string Code = Session["SelectedCrewDutyRulesID"].ToString();
                                    strCrewDutyRulesCD = "";
                                    strCrewDutyRulesID = "";
                                    foreach (GridDataItem Item in dgCrewDutyRulesCatalog.MasterTableView.Items)
                                    {
                                        if (Item["CrewDutyRulesID"].Text.Trim() == Code.Trim())
                                        {
                                            strCrewDutyRulesCD = Item["CrewDutyRuleCD"].Text.Trim();
                                            strCrewDutyRulesID = Item["CrewDutyRulesID"].Text.Trim();
                                            CrewDuty.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                            CrewDuty.CrewDutyRulesDescription = Item.GetDataKeyValue("CrewDutyRulesDescription").ToString();
                                            break;
                                        }

                                    }
                                    CrewDuty.CrewDutyRuleCD = strCrewDutyRulesCD;
                                    CrewDuty.CrewDutyRulesID = Convert.ToInt64(strCrewDutyRulesID);
                                    //CrewDuty.LastUptTM = System.DateTime.Now;
                                    CrewDuty.IsDeleted = true;
                                    //CrewDuty.LastUpdUID = "UC";
                                    var returnValue = CommonService.Lock(EntitySet.Database.CrewDutyRules, Convert.ToInt64(strCrewDutyRulesID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.CrewDutyRules);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewDutyRules);
                                        return;
                                    }
                                    CrewDutyService.DeleteCrewDutyRule(CrewDuty);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgCrewDutyRulesCatalog.Rebind();
                                    DefaultSelection(false);
                                    _selectLastModified = true;
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                    }
                    finally
                    {
                        // For Positive and negative cases need to unlock the record
                        // The same thing should be done for Edit
                        if (Session["SelectedCrewDutyRulesID"] != null)
                        {
                            var returnValue1 = CommonService.UnLock(EntitySet.Database.CrewDutyRules, Convert.ToInt64(Session["SelectedCrewDutyRulesID"]));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyRulesCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSaveFlag.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgCrewDutyRulesCatalog.SelectedItems[0] as GridDataItem;
                                if (item.GetDataKeyValue("CrewDutyRulesID") != null)
                                {
                                    Session["SelectedCrewDutyRulesID"] = item.GetDataKeyValue("CrewDutyRulesID").ToString();
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception.
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                    }
                }
            }
        }

        protected void CrewDutyRulesCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgCrewDutyRulesCatalog.ClientSettings.Scrolling.ScrollTop = "0";
            CrewDutyRulesPageNavigated = true;
        }

        protected void CrewDutyRulesCatalog_PreRender(object sender, EventArgs e)
        {
            GridEnable(true, true, true);
            if (hdnSaveFlag.Value != "Save")
            {
                SelectItem();
            }
            else if (CrewDutyRulesPageNavigated)
            {
                SelectItem();
            }

            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewDutyRulesCatalog, Page.Session);
        }

        /// <summary>
        /// Command Event Trigger for Save or Update CrewDutyRules Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSaveFlag.Value == "Update")
                            {
                                (dgCrewDutyRulesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                SelectItem();
                                ReadOnlyForm();
                            }
                            else
                            {
                                (dgCrewDutyRulesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            //GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSaveFlag.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }

        /// <summary>
        /// Cancel CrewDutyRules Catalog form information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            if (Session["SelectedCrewDutyRulesID"] != null)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.CrewDutyRules, Convert.ToInt64(Session["SelectedCrewDutyRulesID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        EnableLinks();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCrewDutyRulesPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }

            hdnSaveFlag.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        /// <summary>
        /// To Get the Items in the CrewDutyRules catalog
        /// </summary>
        /// <param name="CrewChecklistcode"></param>
        /// <returns></returns>
        private CrewDutyRules GetItems()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.CrewDutyRules CrewDutyRuleService = null;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewDutyRuleService = new FlightPakMasterService.CrewDutyRules();
                        if (hdnSaveFlag.Value == "Update")
                        {
                            //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                            if (Session["SelectedCrewDutyRulesID"] != null)
                            {
                                CrewDutyRuleService.CrewDutyRulesID = Convert.ToInt64(Session["SelectedCrewDutyRulesID"].ToString().Trim());
                            }
                        }
                        else
                        {
                            CrewDutyRuleService.CrewDutyRulesID = 0;
                        }
                        CrewDutyRuleService.CrewDutyRuleCD = tbCode.Text.Trim();
                        CrewDutyRuleService.CrewDutyRulesDescription = tbDescription.Text.Trim();
                        decimal DutyDayStart = 0;
                        decimal DutyDayEnd = 0;
                        decimal MinFixedRest = 0;
                        decimal MaxDutyHrsAllowed = 0;
                        decimal MaxFlightHrsAllowed = 0;
                        decimal RestMultiple = 0;
                        decimal RestMultipleHrs = 0;
                        hdnCrewDutyRulesCD = tbCode.Text.Trim();
                        if (tbDutyDayStart.Text.Length > 0 && Decimal.TryParse(tbDutyDayStart.Text.ToString(), out DutyDayStart))
                        {
                            CrewDutyRuleService.DutyDayBeingTM = DutyDayStart;
                        }

                        if (tbDutyDayEnd.Text.Length > 0 && Decimal.TryParse(tbDutyDayEnd.Text.ToString(), out DutyDayEnd))
                        {
                            CrewDutyRuleService.DutyDayEndTM = DutyDayEnd;
                        }

                        if (tbMinFixedRest.Text.Length > 0 && Decimal.TryParse(tbMinFixedRest.Text.ToString(), out MinFixedRest))
                        {
                            CrewDutyRuleService.MinimumRestHrs = MinFixedRest;
                        }

                        if (tbMaxDutyHrsAllowed.Text.Length > 0 && Decimal.TryParse(tbMaxDutyHrsAllowed.Text.ToString(), out MaxDutyHrsAllowed))
                        {
                            CrewDutyRuleService.MaximumDutyHrs = MaxDutyHrsAllowed;
                        }

                        if (tbMaxFlightHrsAllowed.Text.Length > 0 && Decimal.TryParse(tbMaxFlightHrsAllowed.Text.ToString(), out MaxFlightHrsAllowed))
                        {
                            CrewDutyRuleService.MaximumFlightHrs = MaxFlightHrsAllowed;
                        }

                        if (tbMultiplied.Text.Length > 0 && Decimal.TryParse(tbMultiplied.Text.ToString(), out RestMultiple))
                        {
                            CrewDutyRuleService.RestMultiple = RestMultiple;
                        }

                        if (tbPlus.Text.Length > 0 && Decimal.TryParse(tbPlus.Text.ToString(), out RestMultipleHrs))
                        {
                            CrewDutyRuleService.RestMultipleHrs = RestMultipleHrs;
                        }

                        string FarRules = radlstUSFARRules.SelectedItem.Value.ToString();

                        CrewDutyRuleService.FedAviatRegNum = FarRules;
                        CrewDutyRuleService.IsInActive = chkInactive.Checked;
                        CrewDutyRuleService.IsDeleted = false;
                        // CrewDutyRuleService.LastUpdUID = "UC";
                        // CrewDutyRuleService.LastUptTM = System.DateTime.Now;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
                return CrewDutyRuleService;
            }
        }

        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LoadControlData();
                        hdnSaveFlag.Value = "Update";
                        hdnRedirect.Value = "";
                        EnableForm(true);
                        tbCode.Enabled = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.CrewDutyRules, Convert.ToInt64(strCrewDutyRulesID));
                    }
                }
            }


        }

        /// <summary>
        /// To enable or disable the icons in the grid for insert, edit or delete
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgCrewDutyRulesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgCrewDutyRulesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgCrewDutyRulesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        if (IsAuthorized(Permission.Database.AddCrewDutyRulesCatalog))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                            {
                                lbtnInsertCtl.Enabled = true;
                            }
                            else
                            {
                                lbtnInsertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.DeleteCrewDutyRulesCatalog))
                        {
                            lbtnDelCtl.Visible = true;
                            if (Delete)
                            {
                                lbtnDelCtl.Enabled = true;
                                lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                            }
                            else
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.EditCrewDutyRulesCatalog))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                            {
                                lbtnEditCtl.Enabled = true;
                                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnEditCtl.Enabled = false;
                                lbtnEditCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }

        /// <summary>
        /// To check unique Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbCode.Text))
                        {
                            if (CheckAllReadyExist())
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }

        /// <summary>
        /// To display the controls in Read Only mode
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewDutyRulesCatalog.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgCrewDutyRulesCatalog.SelectedItems[0] as GridDataItem;
                            Label lbLastUpdatedUser = (Label)dgCrewDutyRulesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");

                            if (Item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                            }
                            else
                            {
                                lbLastUpdatedUser.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("LastUptTM") != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUptTM").ToString().Trim())));
                            }
                            LoadControlData();
                            EnableForm(false);
                        }
                        else
                        {
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyRules);
                }
            }
        }

        /// <summary>
        /// To display the controls in Read Only mode
        /// </summary>
        private void LoadControlData()
        {
            ClearForm();
            if (Session["SelectedCrewDutyRulesID"] != null)
            {
                Decimal DataFormat = 0; //Used to convert the string returned from database to decimal format like 0.0
                strCrewDutyRulesCD = "";
                strCrewDutyRulesID = "";
                foreach (GridDataItem Item in dgCrewDutyRulesCatalog.MasterTableView.Items)
                {
                    if (Item["CrewDutyRulesID"].Text.Trim() == Session["SelectedCrewDutyRulesID"].ToString().Trim())
                    {
                        strCrewDutyRulesCD = Item["CrewDutyRuleCD"].Text.Trim();
                        strCrewDutyRulesID = Item["CrewDutyRulesID"].Text.Trim();
                        tbCode.Text = Item.GetDataKeyValue("CrewDutyRuleCD").ToString().Trim();
                        if (Item.GetDataKeyValue("CrewDutyRulesDescription") != null)
                        {
                            tbDescription.Text = Item.GetDataKeyValue("CrewDutyRulesDescription").ToString().Trim();
                        }
                        else
                        {
                            tbDescription.Text = string.Empty;
                        }

                        if (Item.GetDataKeyValue("DutyDayBeingTM") != null)
                        {
                            DataFormat = Convert.ToDecimal(Item.GetDataKeyValue("DutyDayBeingTM"));
                            tbDutyDayStart.Text = DataFormat.ToString("0.0");
                        }
                        else
                        {
                            tbDutyDayStart.Text = string.Empty;
                        }

                        if (Item.GetDataKeyValue("DutyDayEndTM") != null)
                        {
                            DataFormat = Convert.ToDecimal(Item.GetDataKeyValue("DutyDayEndTM"));
                            tbDutyDayEnd.Text = DataFormat.ToString("0.0");
                        }
                        else
                        {
                            tbDutyDayEnd.Text = string.Empty;
                        }

                        if (Item.GetDataKeyValue("MinimumRestHrs") != null)
                        {
                            DataFormat = Convert.ToDecimal(Item.GetDataKeyValue("MinimumRestHrs"));
                            tbMinFixedRest.Text = DataFormat.ToString("0.0");
                        }
                        else
                        {
                            tbMinFixedRest.Text = string.Empty;
                        }

                        if (Item.GetDataKeyValue("MaximumDutyHrs") != null)
                        {
                            DataFormat = Convert.ToDecimal(Item.GetDataKeyValue("MaximumDutyHrs"));
                            tbMaxDutyHrsAllowed.Text = DataFormat.ToString("0.0");
                        }
                        else
                        {
                            tbMaxDutyHrsAllowed.Text = string.Empty;
                        }

                        if (Item.GetDataKeyValue("MaximumFlightHrs") != null)
                        {
                            DataFormat = Convert.ToDecimal(Item.GetDataKeyValue("MaximumFlightHrs"));
                            tbMaxFlightHrsAllowed.Text = DataFormat.ToString("0.0");
                        }
                        else
                        {
                            tbMaxFlightHrsAllowed.Text = string.Empty;
                        }

                        if (Item.GetDataKeyValue("RestMultiple") != null)
                        {
                            DataFormat = Convert.ToDecimal(Item.GetDataKeyValue("RestMultiple"));
                            tbMultiplied.Text = DataFormat.ToString("0.0");
                        }
                        else
                        {
                            tbMultiplied.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("RestMultipleHrs") != null)
                        {
                            DataFormat = Convert.ToDecimal(Item.GetDataKeyValue("RestMultipleHrs"));
                            tbPlus.Text = DataFormat.ToString("0.0");
                        }
                        else
                        {
                            tbPlus.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("FedAviatRegNum") != null)
                        {
                            if ((Item.GetDataKeyValue("FedAviatRegNum").ToString().Trim() == "91") || (Item.GetDataKeyValue("FedAviatRegNum").ToString().Trim() == "121") || (Item.GetDataKeyValue("FedAviatRegNum").ToString().Trim() == "125") || (Item.GetDataKeyValue("FedAviatRegNum").ToString().Trim() == "135"))
                            {
                                radlstUSFARRules.SelectedValue = Item.GetDataKeyValue("FedAviatRegNum").ToString().Trim();
                            }
                            else
                            {
                                radlstUSFARRules.SelectedValue = "91";
                            }
                        }
                        else
                        {
                            radlstUSFARRules.SelectedValue = "91";
                        }
                        if (Item.GetDataKeyValue("IsInActive") != null)
                        {
                            chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            chkInactive.Checked = false;
                        }
                        lbColumnName1.Text = "Duty Rule Code";
                        lbColumnName2.Text = "Description";
                        lbColumnValue1.Text = Item["CrewDutyRuleCD"].Text;
                        lbColumnValue2.Text = Item["CrewDutyRulesDescription"].Text;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// ClearForm after Adding, Editing & Deleting a record 
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbDutyDayStart.Text = "00.0";
                    tbDutyDayEnd.Text = "00.0";
                    tbMaxDutyHrsAllowed.Text = "00.0";
                    tbMaxFlightHrsAllowed.Text = "00.0";
                    tbMinFixedRest.Text = "00.0";
                    tbMultiplied.Text = "0.0";
                    tbPlus.Text = "00.0";
                    radlstUSFARRules.SelectedValue = "91";
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Enable form for Editing
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = Enable;
                    tbDescription.Enabled = Enable;
                    tbDutyDayStart.Enabled = Enable;
                    tbDutyDayEnd.Enabled = Enable;
                    tbMaxDutyHrsAllowed.Enabled = Enable;
                    tbMaxFlightHrsAllowed.Enabled = Enable;
                    tbMinFixedRest.Enabled = Enable;
                    tbMultiplied.Enabled = Enable;
                    tbPlus.Enabled = Enable;
                    radlstUSFARRules.Enabled = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChanges.Visible = Enable;
                    chkInactive.Enabled = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                        _selectLastModified = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.CrewDutyRules> lstCrewDutyRules = new List<FlightPakMasterService.CrewDutyRules>();
                if (Session["CrewDutyRulesList"] != null)
                {
                    lstCrewDutyRules = (List<FlightPakMasterService.CrewDutyRules>)Session["CrewDutyRulesList"];
                }
                if (lstCrewDutyRules.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstCrewDutyRules = lstCrewDutyRules.Where(x => x.IsInActive == false).ToList<CrewDutyRules>(); }
                    dgCrewDutyRulesCatalog.DataSource = lstCrewDutyRules;
                    if (IsDataBind)
                    {
                        dgCrewDutyRulesCatalog.DataBind();
                    }
                }
                //LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCrewDutyRulesCatalog.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var CrewDutyRulesValue = FPKMstService.GetCrewDutyRuleList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CrewDutyRulesValue);
            List<FlightPakMasterService.CrewDutyRules> filteredList = GetFilteredList(CrewDutyRulesValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CrewDutyRulesID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCrewDutyRulesCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCrewDutyRulesCatalog.CurrentPageIndex = PageNumber;
            dgCrewDutyRulesCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfCrewDutyRules CrewDutyRulesValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedCrewDutyRulesID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CrewDutyRulesValue.EntityList.OrderByDescending(x => x.LastUptTM).FirstOrDefault().CrewDutyRulesID;
                Session["SelectedCrewDutyRulesID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.CrewDutyRules> GetFilteredList(ReturnValueOfCrewDutyRules CrewDutyRulesValue)
        {
            List<FlightPakMasterService.CrewDutyRules> filteredList = new List<FlightPakMasterService.CrewDutyRules>();

            if (CrewDutyRulesValue.ReturnFlag)
            {
                filteredList = CrewDutyRulesValue.EntityList.Where(x => x.IsDeleted == false).ToList();
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<CrewDutyRules>(); }
                }
            }

            return filteredList;
        }
    }
}