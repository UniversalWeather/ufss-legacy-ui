﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="CrewDutyRulesCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.CrewDutyRules_Catalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function ValidateEmptyTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "00.0";
            }
        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <script type="text/javascript">
        function CheckTxtBox(control) {


            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);

                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);

                } return false;
            }
        }
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSaveFlag.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCrewDutyRulesCatalog">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewDutyRulesCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgCrewDutyRulesCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgCrewDutyRulesCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCrewDutyRulesCatalog">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewDutyRulesCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="cvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Duty Rules</span> <span class="tab-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=CrewDutyRulesHelp"
                        target="_blank" title="Help" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgCrewDutyRulesCatalog" runat="server" AllowSorting="true" OnItemCreated="dgCrewDutyRulesCatalog_ItemCreated"
                OnNeedDataSource="dgCrewDutyRulesCatalog_BindData" OnItemCommand="dgCrewDutyRulesCatalog_ItemCommand"
                OnPreRender="CrewDutyRulesCatalog_PreRender" OnUpdateCommand="dgCrewDutyRulesCatalog_UpdateCommand"
                OnInsertCommand="dgCrewDutyRulesCatalog_InsertCommand" Height="341px" OnDeleteCommand="dgCrewDutyRulesCatalog_DeleteCommand"
                AutoGenerateColumns="false" OnPageIndexChanged="CrewDutyRulesCatalog_PageIndexChanged"
                PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgCrewDutyRulesCatalog_SelectedIndexChanged"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
                <MasterTableView DataKeyNames="CustomerID,CrewDutyRulesID,CrewDutyRuleCD,CrewDutyRulesDescription,FedAviatRegNum,DutyDayBeingTM,DutyDayEndTM,MaximumDutyHrs,MaximumFlightHrs,MinimumRestHrs,RestMultiple,RestMultipleHrs,IsDeleted,LastUpdUID,LastUptTM,IsInActive"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="CrewDutyRuleCD" HeaderText="Crew Duty Rule Code"
                            HeaderStyle-Width="120px" FilterControlWidth="100px" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="StartsWith" ShowFilterIcon="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CrewDutyRulesDescription" HeaderText="Description"
                            HeaderStyle-Width="500px" FilterControlWidth="480px" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="StartsWith" ShowFilterIcon="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FedAviatRegNum" HeaderText="FAR Rule" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="80px" FilterControlWidth="60px" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CrewDutyRulesID" UniqueName="CrewDutyRulesID"
                            Display="false" AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            HeaderStyle-Width="80px" ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddCrewDutyRulesCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                Visible='<%# IsAuthorized("Permission.Database.EditCrewDutyRulesCatalog")%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                                Visible='<%# IsAuthorized("Permission.Database.DeleteCrewDutyRulesCatalog")%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    <span class="mnd_text">Duty Rule Code</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel100" valign="top">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCode" runat="server" CssClass="text50" MaxLength="4" OnTextChanged="Code_TextChanged"
                                                    AutoPostBack="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Duty Rule Code is Required. "
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="tbCode"
                                                    ValidationGroup="Save" Display="Dynamic" SetFocusOnError="true" CssClass="alert-text">Duty Rule Code is Required.</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" runat="server" CssClass="text250" MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="tbDescription"
                                                    ValidationGroup="Save" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Description is Required.</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130">
                                    U.S. FAR Rules
                                </td>
                                <td valign="top">
                                    <asp:RadioButtonList ID="radlstUSFARRules" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="FAR 91" Value="91" Selected="true"></asp:ListItem>
                                        <asp:ListItem Text="FAR 121" Value="121"></asp:ListItem>
                                        <asp:ListItem Text="FAR 125" Value="125"></asp:ListItem>
                                        <asp:ListItem Text="FAR 135" Value="135"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    Duty Day Start
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="tdLabel120" valign="top">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="tbDutyDayStart" runat="server" MaxLength="4" CssClass="text50" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"
                                                                onBlur="return ValidateEmptyTextbox(this, event)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RegularExpressionValidator ID="revDutyDayStart" runat="server" ErrorMessage="Invalid Duty Day Start"
                                                                Display="Dynamic" ControlToValidate="tbDutyDayStart" CssClass="alert-text" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                ValidationGroup="Save" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel130" valign="top">
                                    Max. Duty Hrs. Allowed
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" class="tdLabel120">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="tbMaxDutyHrsAllowed" runat="server" CssClass="text50" MaxLength="4"
                                                                onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RegularExpressionValidator ID="revMaxDutyHrsAllowed" runat="server" ErrorMessage="Invalid Max Duty Hrs"
                                                                Display="Dynamic" ControlToValidate="tbMaxDutyHrsAllowed" CssClass="alert-text"
                                                                ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$" ValidationGroup="Save" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    Duty Day End
                                </td>
                                <td class="tdLabel120">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDutyDayEnd" runat="server" CssClass="text50" MaxLength="4" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revDutyDayEnd" runat="server" ErrorMessage="Invalid Duty Day End"
                                                    Display="Dynamic" ControlToValidate="tbDutyDayEnd" CssClass="alert-text" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                    ValidationGroup="Save" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel130" valign="top">
                                    Max. Flight Hrs. Allowed
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbMaxFlightHrsAllowed" runat="server" CssClass="text50" MaxLength="4"
                                                    onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revMaxFlightHrsAllowed" runat="server" ErrorMessage="Invalid Max Flight Hrs"
                                                    Display="Dynamic" ControlToValidate="tbMaxFlightHrsAllowed" CssClass="alert-text"
                                                    ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$" ValidationGroup="Save" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top" class="tdLabel130">
                                    Min. Fixed Rest
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbMinFixedRest" runat="server" CssClass="text50" MaxLength="4" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revMinFixedrest" runat="server" ErrorMessage="Invalid Min.Fixed Rest"
                                                    Display="Dynamic" ControlToValidate="tbMinFixedRest" CssClass="alert-text" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                    ValidationGroup="Save" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel250" valign="top">
                                    Minimum Rest Period (Last Duty Day)
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top" class="tdLabel130">
                                    Multiplier
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="tdLabel120">
                                                <asp:TextBox ID="tbMultiplied" runat="server" CssClass="text50" MaxLength="3" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revMultiplied" runat="server" ErrorMessage="Invalid Multiplied"
                                                    Display="Dynamic" ControlToValidate="tbMultiplied" CssClass="alert-text" ValidationExpression="^[0-9]{0,1}(\.[0-9]{1,1})?$"
                                                    ValidationGroup="Save" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" class="tdLabel130">
                                    Plus
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbPlus" runat="server" CssClass="text50" MaxLength="4" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revPlus" runat="server" ErrorMessage="Invalid Plus"
                                                    Display="Dynamic" ControlToValidate="tbPlus" CssClass="alert-text" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                    ValidationGroup="Save" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table class="pad-top-btm" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                            <tr align="right">
                                <td>
                                    <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="Save"
                                        OnClick="SaveChanges_Click" CssClass="button" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click" CausesValidation="false"
                                        CssClass="button" />
                                    <asp:HiddenField ID="hdnSaveFlag" runat="server" />
                                    <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
