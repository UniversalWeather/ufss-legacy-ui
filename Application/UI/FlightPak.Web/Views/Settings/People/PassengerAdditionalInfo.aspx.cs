﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using System.Linq;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.People
{
    public partial class PassengerAdditionalInfo : BaseSecuredPage  //System.Web.UI.Page
    {
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private bool PassengerAdditionalInfoPageNavigated = false;
        private bool _selectLastModified = false;

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPassengerAdditionalInfo, dgPassengerAdditionalInfo, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgPassengerAdditionalInfo.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewPassengerInfoReport);

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewPassengerAdditionalInfo);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgPassengerAdditionalInfo.Rebind();
                                ReadOnlyForm();                                
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgPassengerAdditionalInfo.Rebind();
                    }
                    if (dgPassengerAdditionalInfo.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["PassengerInformationID"] = null;
                        //}
                        if (Session["PassengerInformationID"] == null)
                        {
                            dgPassengerAdditionalInfo.SelectedIndexes.Add(0);
                            Session["PassengerInformationID"] = dgPassengerAdditionalInfo.Items[0].GetDataKeyValue("PassengerInformationID").ToString();
                        }

                        if (dgPassengerAdditionalInfo.SelectedIndexes.Count == 0)
                            dgPassengerAdditionalInfo.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["PassengerInformationID"] != null)
                    {
                        string ID = Session["PassengerInformationID"].ToString();
                        foreach (GridDataItem Item in dgPassengerAdditionalInfo.MasterTableView.Items)
                        {
                            if (Item["PassengerInformationID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Grid enable event
        /// </summary>
        /// <param name="add"></param>
        /// <param name="edit"></param>
        /// <param name="delete"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, delCtl, editCtl;
                    insertCtl = (LinkButton)dgPassengerAdditionalInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    delCtl = (LinkButton)dgPassengerAdditionalInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    editCtl = (LinkButton)dgPassengerAdditionalInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddPassengerAdditionalInfo))
                    {
                        insertCtl.Visible = true;
                        if (add)
                        {
                            insertCtl.Enabled = true;
                        }
                        else
                        {
                            insertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeletePassengerAdditionalInfo))
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete('This will delete the code from all associated PAX members. Continue Delete?');";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditPassengerAdditionalInfo))
                    {
                        editCtl.Visible = true;
                        if (edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to insert the form Values
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.ReadOnly = false;
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Passenger Additional Info Edit form
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
               
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    LoadControlData();
                    EnableForm(true);
                    tbCode.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Enable form
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbDescription.Enabled = enable;
                    chkShowinTripSheetReport.Enabled = enable;
                    chkIsCheckList.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Clear form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    hdnPassengerAdditionalInfoId.Value = string.Empty;
                    tbDescription.Text = string.Empty;
                    chkShowinTripSheetReport.Checked = false;
                    chkIsCheckList.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Read only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To assign the data to controls 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["PassengerInformationID"] != null)
                    {
                        foreach (GridDataItem item in dgPassengerAdditionalInfo.MasterTableView.Items)
                        {
                            if (item["PassengerInformationID"].Text.Trim() == Session["PassengerInformationID"].ToString().Trim())
                            {
                                tbCode.Text = item.GetDataKeyValue("PassengerInfoCD").ToString();
                                hdnPassengerAdditionalInfoId.Value = item.GetDataKeyValue("PassengerInformationID").ToString();
                                if (item.GetDataKeyValue("PassengerDescription") != null)
                                {
                                    tbDescription.Text = item.GetDataKeyValue("PassengerDescription").ToString();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (item.GetDataKeyValue("IsShowOnTrip") != null)
                                {
                                    chkShowinTripSheetReport.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsShowOnTrip").ToString());
                                }
                                else
                                {
                                    chkShowinTripSheetReport.Checked = false;
                                }
                                if (item.GetDataKeyValue("IsCheckList") != null)
                                {
                                    chkIsCheckList.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsCheckList").ToString());
                                }
                                else
                                {
                                    chkIsCheckList.Checked = false;
                                }
                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgPassengerAdditionalInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.GetDataKeyValue("LastUpdTS").ToString())));//item.GetDataKeyValue("LastUpdTS").ToString();
                                }

                                lbColumnName1.Text = "Additional Info Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = item["PassengerInfoCD"].Text;
                                lbColumnValue2.Text = item["PassengerDescription"].Text;

                                item.Selected = true;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Passenger Additional Info Get items method
        /// </summary>
        /// <param name="PassengerAdditionalInfoCode"></param>
        /// <returns></returns>
        private PassengerInformation GetItems(FlightPakMasterService.PassengerInformation objPassengerInformation)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objPassengerInformation))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (hdnSave.Value == "Update")
                    {
                        objPassengerInformation.PassengerInformationID = Convert.ToInt64(hdnPassengerAdditionalInfoId.Value);
                    }
                    objPassengerInformation.PassengerInfoCD = tbCode.Text;
                    objPassengerInformation.PassengerDescription = tbDescription.Text;
                    objPassengerInformation.IsShowOnTrip = chkShowinTripSheetReport.Checked;
                    objPassengerInformation.IsCheckList = chkIsCheckList.Checked;
                    objPassengerInformation.IsDeleted = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return objPassengerInformation;
            }
        }
        #region "Grid events"
        /// <summary>
        /// Passenger Additional Info Item created Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPassengerAdditionalInfo_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        /// <summary>
        /// Passenger Additional Info Data Bind event
        /// </summary>r
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPassengerAdditionalInfo_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objPassengerAdditionalInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objPassengerInfo = objPassengerAdditionalInfoService.GetPassengerAdditionalInfoList();
                            List<FlightPakMasterService.PassengerInformation> PassengerInfoList = new List<PassengerInformation>();
                            if (objPassengerInfo.ReturnFlag == true)
                            {
                                PassengerInfoList = objPassengerInfo.EntityList.Where(x => x.IsDeleted == false).ToList(); ;
                            }
                            dgPassengerAdditionalInfo.DataSource = PassengerInfoList;
                            Session["PassengerAdditionalInfoCodes"] = PassengerInfoList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        /// <summary>
        /// Passenger Additional Info Item command Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPassengerAdditionalInfo_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                // Lock the record
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.PassengerInformation, Convert.ToInt64(Session["PassengerInformationID"]));
                                    Session["IsEditLockPassengerInformation"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PassengerInformation);
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgPassengerAdditionalInfo.SelectedIndexes.Clear();                                
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        /// <summary>
        /// Passenger Additional Info Update Event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPassengerAdditionalInfo_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if ((Session["PassengerInformationID"] != null))
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                if (hdnValue.Value == "No")
                                {
                                    FlightPakMasterService.PassengerInformation objPassengerInformation = new FlightPakMasterService.PassengerInformation();
                                    GetItems(objPassengerInformation);
                                    var objRetVal = objService.UpdatePassengerAdditionalInfo(objPassengerInformation);
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.PassengerInformation, Convert.ToInt64(Session["PassengerInformationID"]));
                                            Session["IsEditLockPassengerInformation"] = "False";
                                            e.Item.OwnerTableView.Rebind();
                                            e.Item.Selected = true;
                                            GridEnable(true, true, true);
                                            dgPassengerAdditionalInfo.Rebind();
                                            SelectItem();
                                            ReadOnlyForm();

                                            ShowSuccessMessage();

                                            //dgPassengerAdditionalInfo.Rebind();
                                            GridEnable(true, true, true);
                                            _selectLastModified = true;
                                        }
                                    }
                                }
                                else
                                {
                                    FlightPakMasterService.PassengerInformation objPassengerInformation = new FlightPakMasterService.PassengerInformation();
                                    GetItems(objPassengerInformation);
                                    var objRetVal = objService.UpdateforAllPaxMembers(objPassengerInformation);
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.PassengerInformation, Convert.ToInt64(Session["PassengerInformationID"]));
                                            Session["IsEditLockPassengerInformation"] = "False";
                                            e.Item.OwnerTableView.Rebind();
                                            e.Item.Selected = true;
                                            GridEnable(true, true, true);
                                            dgPassengerAdditionalInfo.Rebind();
                                            SelectItem();
                                            ReadOnlyForm();

                                            ShowSuccessMessage();

                                            //dgPassengerAdditionalInfo.Rebind();
                                            GridEnable(true, true, true);
                                            _selectLastModified = true;
                                        }
                                    }
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        /// <summary>
        /// Passenger Additional Info Insert Command
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPassengerAdditionalInfo_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (checkAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                        }
                        else
                        {
                            e.Canceled = true;
                            //using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            //{
                            //    FlightPakMasterService.PassengerInformation objPassengerInformation = new FlightPakMasterService.PassengerInformation();
                            //    GetItems(objPassengerInformation);
                            //    var objRetVal = objService.AddPassengerAdditionalInfo(objPassengerInformation);
                            //    if (objRetVal.ReturnFlag == true)
                            //    {
                            //        dgPassengerAdditionalInfo.Rebind();
                            //        GridEnable(true, true, true);
                            //        DefaultSelection(false);

                            //        ShowSuccessMessage();

                            //        dgPassengerAdditionalInfo.Rebind();
                            //        GridEnable(true, true, true);
                            //    }
                            //    else
                            //    {
                            //        //For Data Anotation
                            //        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.PassengerInformation);
                            //    }
                            //}

                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                if (hdnValue.Value == "No")
                                {
                                    FlightPakMasterService.PassengerInformation objPassengerInformation = new FlightPakMasterService.PassengerInformation();
                                    GetItems(objPassengerInformation);
                                    var objRetVal = objService.AddPassengerAdditionalInfo(objPassengerInformation);
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        DefaultSelection(false);
                                        dgPassengerAdditionalInfo.Rebind();

                                        ShowSuccessMessage();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.PassengerInformation);
                                    }
                                }
                                else
                                {
                                    FlightPakMasterService.PassengerInformation objPassengerInformation = new FlightPakMasterService.PassengerInformation();
                                    GetItems(objPassengerInformation);
                                    var objRetVal = objService.AddforAllPaxMembers(GetItems(objPassengerInformation));
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        DefaultSelection(false);
                                        dgPassengerAdditionalInfo.Rebind();

                                        ShowSuccessMessage();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.PassengerInformation);
                                    }
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        /// <summary>
        /// Passenger Additional Info delete command
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPassengerAdditionalInfo_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["PassengerInformationID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objPassengerAdditionalInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.PassengerInformation objPassengerAdditionalInfo = new FlightPakMasterService.PassengerInformation();
                                    GridDataItem item = dgPassengerAdditionalInfo.SelectedItems[0] as GridDataItem;
                                    objPassengerAdditionalInfo.PassengerInformationID = Convert.ToInt64(item.GetDataKeyValue("PassengerInformationID"));
                                    objPassengerAdditionalInfo.PassengerInfoCD = item.GetDataKeyValue("PassengerInfoCD").ToString();
                                    objPassengerAdditionalInfo.PassengerDescription = item.GetDataKeyValue("PassengerDescription").ToString();
                                    objPassengerAdditionalInfo.IsDeleted = true;
                                    //Lock the record
                                    var returnValue = CommonService.Lock(EntitySet.Database.PassengerInformation, Convert.ToInt64(Session["PassengerInformationID"]));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PassengerInformation);
                                        return;
                                    }
                                    objPassengerAdditionalInfoService.DeletePassengerAdditionalInfo(objPassengerAdditionalInfo);
                                    DefaultSelection(false);
                                    e.Item.OwnerTableView.Rebind();
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.PassengerInformation, Convert.ToInt64(Session["PassengerInformationID"]));
                    }
                }
            }
        }
        /// <summary>
        /// Passenger Additional Info Item selected changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPassengerAdditionalInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgPassengerAdditionalInfo.SelectedItems[0] as GridDataItem;
                                Session["PassengerInformationID"] = item["PassengerInformationID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                    }
                }
            }
        }
        
        protected void dgPassengerAdditionalInfo_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPassengerAdditionalInfo.ClientSettings.Scrolling.ScrollTop = "0";
                        PassengerAdditionalInfoPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        protected void dgPassengerAdditionalInfo_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (PassengerAdditionalInfoPageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPassengerAdditionalInfo, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        #endregion
        /// <summary>
        /// Rad Ajax creating event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgPassengerAdditionalInfo;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        /// <summary>
        /// Passenger Additional Info Save changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgPassengerAdditionalInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgPassengerAdditionalInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                           
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        /// <summary>
        /// Passenger Additional Info Cancel event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.PassengerInformation, Convert.ToInt64(Session["PassengerInformationID"]));
                            //Session.Remove("PassengerInformationID");
                            DefaultSelection(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PassengerAddInfoCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            if(checkAllReadyExist())
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        /// <summary>
        /// To check whether the Already the Passenger exists
        /// </summary>
        /// <returns></returns>
        private bool checkAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objPassengerAdditionalInfosvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPakMasterService.PassengerInformation> PassengerInfoList = new List<PassengerInformation>();
                        PassengerInfoList = ((List<FlightPakMasterService.PassengerInformation>)Session["PassengerAdditionalInfoCodes"]).Where(x => x.PassengerInfoCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<FlightPakMasterService.PassengerInformation>();
                        if (PassengerInfoList.Count != 0)
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            returnVal = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }
        /// <summary>
        /// Clear filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnclrFilters_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        pnlExternalForm.Visible = false;
                        foreach (GridColumn column in dgPassengerAdditionalInfo.MasterTableView.Columns)
                        {
                            //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                            //column.CurrentFilterValue = string.Empty;
                        }
                        dgPassengerAdditionalInfo.MasterTableView.FilterExpression = string.Empty;
                        dgPassengerAdditionalInfo.MasterTableView.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }

        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerAdditionalInfo);
                }
            }
        }
        #endregion

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgPassengerAdditionalInfo.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var PassengerAdditionalInfoValue = FPKMstService.GetPassengerAdditionalInfoList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, PassengerAdditionalInfoValue);
            List<FlightPakMasterService.PassengerInformation> filteredList = GetFilteredList(PassengerAdditionalInfoValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.PassengerInformationID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgPassengerAdditionalInfo.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgPassengerAdditionalInfo.CurrentPageIndex = PageNumber;
            dgPassengerAdditionalInfo.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfPassengerInformation PassengerAdditionalInfoValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["PassengerInformationID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = PassengerAdditionalInfoValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().PassengerInformationID;
                Session["PassengerInformationID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.PassengerInformation> GetFilteredList(ReturnValueOfPassengerInformation PassengerAdditionalInfoValue)
        {
            List<FlightPakMasterService.PassengerInformation> filteredList = new List<FlightPakMasterService.PassengerInformation>();

            if (PassengerAdditionalInfoValue.ReturnFlag)
            {
                filteredList = PassengerAdditionalInfoValue.EntityList.Where(x => x.IsDeleted == false).ToList();
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgPassengerAdditionalInfo.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgPassengerAdditionalInfo.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}
