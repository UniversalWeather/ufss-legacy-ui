﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="TravelCoordinator.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.TravelCoordinator"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function openWin() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=tbHomeBase.ClientID%>").value, "radCompanyMasterPopup");
            }
            function openWin1() {
                GetRequestorListValue();
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../People/AssignRequestorsPopUp.aspx?HomeBase=" + document.getElementById("<%=tbHomeBase.ClientID%>").value + ",&PassengerID=" + document.getElementById("<%=lstRequestorAvailablehidden.ClientID%>").value, "RadWindow2");
            }
            function openWin2() {
                GetAircraftListValue();
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../People/AssignAircraftPopUp.aspx?HomeBase=" + document.getElementById("<%=tbHomeBase.ClientID%>").value + ",&FleetID=" + document.getElementById("<%=lstAircraftAvailableHidden.ClientID%>").value, "RadWindow3");
            }

            function GetAircraftListValue() {
                var list = $find("<%= lstAircraftAvailable.ClientID %>");
                var items = list.get_items();
                document.getElementById("<%=lstAircraftAvailableHidden.ClientID%>").value = '';
                for (var i = 0; i < items.get_count(); i++) {
                    //if (list.getItem(i).get_selected() == true) {
                    document.getElementById("<%=lstAircraftAvailableHidden.ClientID%>").value += list.getItem(i).get_value() + ",";
                    //}
                }
            }
            function GetRequestorListValue() {
                var list = $find("<%= lstRequestorAvailable.ClientID %>");
                var items = list.get_items();
                document.getElementById("<%=lstRequestorAvailablehidden.ClientID%>").value = '';
                for (var i = 0; i < items.get_count(); i++) {
                    //if (list.getItem(i).get_selected() == true) {
                    document.getElementById("<%=lstRequestorAvailablehidden.ClientID%>").value += list.getItem(i).get_value() + ",";
                    //}
                }
            }

            function OnClientClick(strPanelToExpand) {
                var PanelBar1 = $find("<%= pnlbarnotes.ClientID %>");
                var PanelBar2 = $find("<%= Maintenance.ClientID %>");
                PanelBar1.get_items().getItem(0).set_expanded(true);
                PanelBar2.get_items().getItem(0).set_expanded(false);

                return false;
            }

            // this function is used to the refresh the currency listbox
            function refreshGrid(arg) {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest('Rebind');
            }

            function refereshlistaircraft() {
            }
            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }

            function OnClientCloseHomeBase(oWnd, args) {
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.HomebaseID;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                    }
                }
            }


            function OnClientClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {

                        var strReturn = arg.TailNumber;
                        var IsExist = false;
                        document.getElementById("<%=lstAircraftAvailableHidden.ClientID%>").value = arg.TailNumber;
                        var listselect = document.getElementById("<%=lstAircraftAvailable.ClientID%>");
                        var fields = strReturn.split(/,/);
                        for (i = 0; i < fields.length; i++) {
                            IsExist = false;
                            for (var j = 0; j < listselect.options.length; j++) {
                                if (listselect.options[j].value == fields[i]) {
                                    alert("Unique Requestor Code is Required.");
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false) {
                                var newOption = document.createElement("option");
                                newOption.value = fields[i];
                                newOption.text = fields[i];
                                listselect.add(newOption, null);
                            }
                        }

                    }
                }
            }

            function OnClientClose1(oWnd, args) {
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        var strReturn = arg.TailNumber;
                        var IsExist = false;
                        document.getElementById("<%=lstRequestorAvailablehidden.ClientID%>").value = arg.TailNumber;
                        var listselect = document.getElementById("<%=lstRequestorAvailable.ClientID%>");
                        var fields = strReturn.split(/,/);
                        for (i = 0; i < fields.length; i++) {
                            IsExist = false;
                            for (var j = 0; j < listselect.options.length; j++) {
                                if (listselect.options[j].value == fields[i]) {
                                    alert("Unique Requestor Code is Required.");
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false) {
                                var newOption = document.createElement("option");
                                newOption.value = fields[i];
                                newOption.text = fields[i];
                                listselect.add(newOption, null);
                            }
                        }

                    }
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <telerik:RadAjaxManager ID="RadAjaxManager1" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
            runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgTravelCoordinator">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgTravelCoordinator" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%-- <telerik:AjaxSetting AjaxControlID="divExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseHomeBase" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseHomeBase" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClose1" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/AssignRequestorsPopUp.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/AssignAircraftPopUp.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        
        <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                        <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
        </telerik:RadWindowManager>
        <br />
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <div class="tab-nav-top">
                        <span class="head-title">Travel Coordinator</span> <span class="tab-nav-icons">
                            <!--<a
                            href="#" class="print-icon"></a>-->
                            <a href="../../Help/ViewHelp.aspx?Screen=TravelCoordinatorHelp" target="_blank" class="help-icon"
                                title="Help"></a></span>
                    </div>
                </td>
            </tr>
        </table>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="True">
                <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                    <tr>
                        <td>
                            <div class="status-list">
                                <span>
                                    <asp:CheckBox ID="chkHomeBase" runat="server" Text="Home Base Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                        AutoPostBack="true" />
                                </span><span>
                                    <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                        AutoPostBack="true" /></span>
                            </div>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" style="display: none">
                    <tr>
                        <td class="tdLabel240" valign="top">
                            <telerik:RadListBox runat="server" ID="RadListBox1" Height="200px" Width="200px"
                                Enabled="false">
                            </telerik:RadListBox>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="dgTravelCoordinator" runat="server" AllowSorting="true" OnItemCreated="dgTravelCoordinator_ItemCreated" 
                    Visible="true" OnNeedDataSource="dgTravelCoordinator_BindData" OnItemCommand="dgTravelCoordinator_ItemCommand"
                    OnUpdateCommand="dgTravelCoordinator_UpdateCommand" OnInsertCommand="dgTravelCoordinator_InsertCommand"
                    OnDeleteCommand="dgTravelCoordinator_DeleteCommand" AutoGenerateColumns="false"
                    OnPageIndexChanged="dgTravelCoordinator_OnPageIndexChanged" OnPreRender="dgTravelCoordinator_PreRender"
                    OnItemDataBound="dgTravelCoordinator_ItemDataBound" PageSize="10" AllowPaging="true"
                    OnSelectedIndexChanged="dgTravelCoordinator_SelectedIndexChanged" AllowFilteringByColumn="true"
                    PagerStyle-AlwaysVisible="true" EnableLinqExpressions="False" Height="341px">
                    <MasterTableView DataKeyNames="TravelCoordinatorID,CustomerID,TravelCoordCD,FirstName,MiddleName,LastName,PhoneNum,FaxNum,PagerNum,CellPhoneNum,EmailID
                        ,Notes,HomebaseID,LastUpdUID,LastUpdTS,IsDeleted,HomebaseCD,BaseDescription,DisplayName
                        ,BusinessPhone,HomeFax,CellPhoneNum2,OtherPhone,BusinessEmail,PersonalEmail,OtherEmail,Addr3,IsInActive,PassengerRequestorCD,PassengerName"
                        CommandItemDisplay="Bottom">
                        <Columns>
                            <telerik:GridBoundColumn DataField="TravelCoordCD" HeaderText="Travel Coordinator Code"
                                AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                HeaderStyle-Width="80px" FilterControlWidth="60px" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Name" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="200px"
                                FilterControlWidth="180px" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="BusinessPhone" HeaderText="Business Phone" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="80px"
                                FilterControlWidth="60px" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FaxNum" HeaderText="Business Fax" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="80px"
                                FilterControlWidth="60px" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="BusinessEmail" HeaderText="Business<br/>E-mail" FilterDelay="500"
                                AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                HeaderStyle-Width="170px" FilterControlWidth="150px">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="Requestor Code"
                                ShowFilterIcon="false" CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false"
                                HeaderStyle-Width="80px" FilterControlWidth="60px" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PassengerName" HeaderText="Requestor Name" ShowFilterIcon="false"
                                CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false" HeaderStyle-Width="200px"
                                FilterControlWidth="180px" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                                ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" HeaderStyle-Width="50px">
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="70px" FilterControlWidth="50px"
                                FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="TravelCoordinatorID" HeaderText="TravelCoordinatorID"
                                AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo" FilterDelay="500"
                                Display="false">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px; float: left; clear: both;">
                                <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                    Visible='<%# IsAuthorized(Permission.Database.AddTravelCoordinator)%>'>
                            <img style="border: 0px; vertical-align: middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                    ToolTip="Edit" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditTravelCoordinator)%>'>
                            <img style="border: 0px; vertical-align: middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                                    runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeleteTravelCoordinator)%>'>
                            <img style="border: 0px; vertical-align: middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                            </div>
                            <div>
                                <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                            </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings CaseSensitive="false" />
                </telerik:RadGrid>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" class="tblButtonArea-nw">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnNotes" runat="server" CssClass="ui_nav" Text="Notes" OnClientClick="javascript:OnClientClick('Notes');return false;" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSaveChanges1" Visible="false" CssClass="button" Text="Save" runat="server"
                                            OnClick="SaveChanges_Click" ValidationGroup="save" />
                                        <%--<asp:Button ID="btndgSave" runat="server" CssClass="button" Text="Save" OnClick="btnSaveChanges_Click"
                                        ValidationGroup="save" />--%>
                                    </td>
                                    <td>
                                        <%-- <asp:Button ID="btnCancel1" CssClass="button" OnClick="Cancel_Click" CausesValidation="false" Visible="false"
                                Text="Cancel" runat="server"  />--%>
                                        <asp:Button ID="btnCancel1" CssClass="button" OnClick="Cancel_Click" Visible="false"
                                            Text="Cancel" runat="server" CausesValidation="false" />
                                    </td>
                                    <%-- <asp:Button ID="btndgCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />--%>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%--</td> </tr> </table>--%>
                <telerik:RadPanelBar ID="Maintenance" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                    runat="server">
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information">
                            <Items>
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table width="100%" class="box1">
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                <asp:CheckBox ID="chkInactive" runat="server" Text="Inactive" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <span class="mnd_text">Travel Coordinator Code</span>
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbCode" OnTextChanged="tbCode_TextChanged" AutoPostBack="true" runat="server"
                                                                                MaxLength="5" CssClass="text50" ValidationGroup="save">
                                                                            </asp:TextBox>
                                                                            <asp:HiddenField ID="hdnTravelCoordinatorID" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="Code is Required"
                                                                                Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbCode" CssClass="alert-text"
                                                                                ValidationGroup="save"></asp:RequiredFieldValidator>
                                                                            <asp:CustomValidator ID="cvAircraftCode" runat="server" ControlToValidate="tbCode"
                                                                                ErrorMessage="Unique Code is Required" Display="Dynamic" CssClass="alert-text"
                                                                                ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:Label ID="lbHomeBase" runat="server" Text="Home Base"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel200" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" CssClass="text50" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                OnTextChanged="tbHomeBase_OnTextChanged" AutoPostBack="true">
                                                                            </asp:TextBox>
                                                                            <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin();return false;"
                                                                                Enabled="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                                ErrorMessage="Home Base is invalid" Display="Dynamic" CssClass="alert-text" ValidationGroup="save"
                                                                                SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <%-- <asp:Image ID="imghomesearch" runat="server" ImageUrl="~/App_Themes/Default/images/browse_button.png" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                    <div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <span class="mnd_text">First Name</span>
                                                            </td>
                                                            <td class="tdLabel160" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbFirstName" runat="server" MaxLength="30" CssClass="text100" ValidationGroup="save">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ErrorMessage="Enter Your First Name"
                                                                                Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbFirstName" CssClass="alert-text"
                                                                                ValidationGroup="save">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel80" valign="top">
                                                                <asp:Label ID="lbMiddleName" runat="server" Text="Middle Name"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel150" valign="top">
                                                                <asp:TextBox ID="tbMiddleName" runat="server" MaxLength="30" CssClass="text100">
                                                                </asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel80" valign="top">
                                                                <span class="mnd_text">Last Name</span>
                                                            </td>
                                                            <td valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbLastName" runat="server" MaxLength="30" CssClass="text100" ValidationGroup="save">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ErrorMessage="Enter Your Last Name"
                                                                                Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbLastName" CssClass="alert-text"
                                                                                ValidationGroup="save">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:Label ID="lbBusinessPhone" runat="server" Text="Business Phone"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbBusinessPhone" runat="server" MaxLength="25" CssClass="text200"
                                                                                ValidationGroup="save" onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <%--    <asp:RegularExpressionValidator ID="revPhone" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                                    ControlToValidate="tbPhone" ForeColor="Red" ValidationGroup="save" ValidationExpression="\+\d*">
                                                                </asp:RegularExpressionValidator>--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                <asp:Label ID="lbPhone" runat="server" Text="Home Phone"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbPhone" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save"
                                                                                onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:Label ID="lbOtherPhone" runat="server" Text="Other Phone"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbOtherPhone" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save"
                                                                                onKeyPress="return fnAllowPhoneFormat(this,event)">

                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                <asp:Label ID="lbPager" runat="server" Text="Pager No."></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbPager" runat="server" MaxLength="25" CssClass="text200" onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:Label ID="lbMobile" runat="server" Text="Primary Mobile/Cell"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbMobile" runat="server" MaxLength="25" CssClass="text200" onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                </asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                <asp:Label ID="lbCellPhoneNum2" runat="server" Text="Secondary Mobile/Cell"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbCellPhoneNum2" runat="server" MaxLength="25" CssClass="text200"
                                                                                ValidationGroup="save" onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:Label ID="lbEmailAddress" runat="server" Text="Business E-mail"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbEmail" runat="server" MaxLength="100" CssClass="text200" ValidationGroup="save">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                                                ControlToValidate="tbEmail" CssClass="alert-text" ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:Label ID="lbBusinessEmail" runat="server" Text="Business E-mail"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbBusinessEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                                ValidationGroup="save">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RegularExpressionValidator ID="regBusinessEmail" runat="server" Display="Dynamic"
                                                                                ErrorMessage="Invalid Format" ControlToValidate="tbBusinessEmail" CssClass="alert-text"
                                                                                ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                <asp:Label ID="lbPersonalEmail" runat="server" Text="Personal E-mail"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbPersonalEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                                ValidationGroup="save">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RegularExpressionValidator ID="regPersonalEmail" runat="server" Display="Dynamic"
                                                                                ErrorMessage="Invalid Format" ControlToValidate="tbPersonalEmail" CssClass="alert-text"
                                                                                ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:Label ID="lbOtherEmail" runat="server" Text="Other E-mail"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbOtherEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                                ValidationGroup="save">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RegularExpressionValidator ID="regOtherEmail" runat="server" Display="Dynamic"
                                                                                ErrorMessage="Invalid Format" ControlToValidate="tbOtherEmail" CssClass="alert-text"
                                                                                ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:Label ID="lbFax" runat="server" Text="Business Fax"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbFax" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save"
                                                                                onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                <asp:Label ID="lbHomeFax" runat="server" Text="Home Fax"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbHomeFax" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save"
                                                                                onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <%--<asp:RegularExpressionValidator ID="revFax" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                                    ControlToValidate="tbFax" ForeColor="Red" ValidationGroup="save" ValidationExpression="\+\d*">
                                                                </asp:RegularExpressionValidator>--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                    <div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                Assign Requestors
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top">
                                                                            Available:
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <telerik:RadListBox runat="server" ID="lstRequestorAvailable" Height="200px" Width="200px"
                                                                                Enabled="true" CssClass="outline_radlist">
                                                                            </telerik:RadListBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="lstRequestorAvailablehidden" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="Button2" runat="server" CssClass="browse-button" Enabled="false"
                                                                                OnClientClick="javascript:openWin1();return false;" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                Assign Aircraft
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left">
                                                                            Available:
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <telerik:RadListBox runat="server" ID="lstAircraftAvailable" Height="200px" Width="200px"
                                                                                Enabled="true" CssClass="outline_radlist">
                                                                            </telerik:RadListBox>
                                                                            <%--<asp:ListBox ID="lstAircraftAvailable" SelectionMode="Single" runat="server" Width="200px"
                                                                    Height="200px"></asp:ListBox>--%>
                                                                            <asp:HiddenField ID="lstAircraftAvailableHidden" runat="server" />
                                                                            <td>
                                                                                <asp:Button ID="Button1" runat="server" CssClass="browse-button" Enabled="false"
                                                                                    OnClientClick="javascript:openWin2();return false;" />
                                                                            </td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
                <table cellspacing="0" cellpadding="0" style="width: 100%;">
                    <tr>
                        <td align="left">
                            <div class="nav-space">
                            </div>
                        </td>
                    </tr>
                </table>
                <telerik:RadPanelBar ID="pnlbarnotes" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                    runat="server">
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="false" Text="Notes">
                            <Items>
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table width="100%" class="note-box" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbNotes" TextMode="MultiLine" runat="server" CssClass="textarea-db">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveChanges" Visible="false" CssClass="button" Text="Save" runat="server"
                                OnClick="SaveChanges_Click" ValidationGroup="save" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                            <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" CssClass="button" OnClick="Cancel_Click" Visible="false"
                                Text="Cancel" runat="server" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
                <div id="toolbar" class="scr_esp_down_db">
                    <div class="floating_main_db">
                        <div class="floating_bar_db">
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                            <span class="padright_20">
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                            <span>
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
                <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
