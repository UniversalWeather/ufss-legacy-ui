﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head runat="server">
    <title>Passenger/Requestor</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
      <style type="text/css">
         #gridPassenger tr td:first-child {text-align: center!important}

    </style>
    <script type="text/javascript">
        var passengerRequestorId = null;
        var passengerName = null;
        var selectedRowData = "";
        var jqgridTableId = '#gridPassenger';
        var selectedRowMultiplePassenger = "";
        var isopenlookup = false;
        var ismultiSelect = false;
        var passengerCD = [];
        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });
            UserIdentity(setIsRequestorOnly);
            function setIsRequestorOnly(Identity) {
                $("#chkRequestorOnly").prop('checked', Identity._fpSettings._IsReqOnly);
            }

            var selectedRowData = decodeURI(getQuerystring("PassengerRequestorCD", ""));
            selectedRowMultiplePassenger = $.trim(decodeURI(getQuerystring("PassengerRequestorCD", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultiplePassenger);
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            if (ismultiSelect)
            {
                if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                    selectedRowData = jQuery("#hdnselectedfccd").value;
                }
                passengerCD = selectedRowData.split(',');
            }
            
            $("#btnSubmit").click(function () {
                var selr = $('#gridPassenger').jqGrid('getGridParam', 'selrow');
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultiplePassenger.length > 0)) {
                    var rowData = $('#gridPassenger').getRowData(selr);
                    returnToParent(rowData,0);
                }
                else {
                    showMessageBox('Please select a passenger.', popupTitle);
                }
                return false;
            });

            $("#btnClientCode").click(function () {
                $("#errorMsg").removeClass().addClass('hideDiv');
                //var oWnd = popupwindowSearch("/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("clientCD").value, 650, 480, ClientCodePopupClose);
                var oWnd = radopen("/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("clientCD").value, "RadClientCodePopup");
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add", popupTitle, widthDoc + 100, 700, jqgridTableId);
                return false;
            });

            $("#recordDelete").click(function () {
                
                var selr = $('#gridPassenger').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridPassenger').getRowData(selr);
                passengerRequestorId = rowData['PassengerRequestorID'];
                passengerName = rowData['PassengerName'];
                var isActive = rowData['IsActive'];
                var widthDoc = $(document).width();
                if (isActive == 'Yes') {
                    showMessageBox('Active passenger cannot be deleted.', popupTitle);
                    return false;
                }

                if (passengerRequestorId != undefined && passengerRequestorId != null && passengerRequestorId != '' && passengerRequestorId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a passenger.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        async: true,
                        type: 'Get',
                        crossDomain: true,
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Passenger&passengerRequestorId=' + passengerRequestorId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (!IsNullOrEmptyOrUndefined(content)) {
                                if (content.indexOf('404'))
                                    showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                            }
                        }
                    });
                }
            }

            $("#recordEdit").click(function () {
                var selr = $('#gridPassenger').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridPassenger').getRowData(selr);
                passengerRequestorId = rowData['PassengerRequestorID'];
                var widthDoc = $(document).width();
                if (passengerRequestorId != undefined && passengerRequestorId != null && passengerRequestorId != '' && passengerRequestorId != 0) {
                    popupwindow("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=&PassengerRequestorID=" + passengerRequestorId, popupTitle, widthDoc + 100, 700, jqgridTableId);
                    $('#gridPassenger').trigger('reloadGrid');
                }
                else {
                    showMessageBox('Please select a passenger.', popupTitle);
                }
                return false;
            });

            jQuery("#gridPassenger").jqGrid({

                url: '/Views/Utilities/ApiCallerwithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $("#gridPassenger").jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }

                    postData.sendIcaoId = $("#chkHomebaseOnly").is(':checked') ? true : false;
                    postData.apiType = 'fss';
                    postData.method = 'Passengers';
                    postData.cQCustomerID = 0;
                    postData.passengerId = 0;
                    postData.passengerCD = function () { if (ismultiSelect && selectedRowData.length > 0) { return passengerCD[0]; } else { return selectedRowData; } };
                    postData.activeOnly = $("#chkActiveOnly").is(':checked') ? true : false;
                    postData.requestorOnly = $("#chkRequestorOnly").is(':checked') ? true : false;
                    postData.paxGroupId = 0;
                    postData.clientId = $("#clientId").val();
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },

                height: 320,
                width: 900,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect:ismultiSelect,
                pager: "#pg_gridPager",
                enableclear: true,
                emptyrecords: "No records to view.",
                colNames: ['PassengerRequestorID', 'PAX Code', 'Passenger/Requestor', 'Dept', 'Dept. Desc', 'AdditionalPhone', 'Phone', 'Home Base', 'Active', 'Req'],
                colModel: [
                    { name: 'PassengerRequestorID', index: 'PassengerRequestorID', key: true, hidden: true },
                    { name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', align: 'left' },
                    { name: 'PassengerName', index: 'PassengerName', width: 220, align: 'left' },
                    { name: 'DepartmentCD', index: 'DepartmentCD', align: 'left' },
                    { name: 'DepartmentName', index: 'DepartmentName', align: 'left', width: 220 },
                    { name: 'AdditionalPhoneNum', index: 'AdditionalPhoneNum', align: 'left', hidden: true },
                    { name: 'PhoneNum', index: 'PhoneNum', align: 'left' },
                    { name: 'HomeBaseCD', index: 'HomeBaseCD' },
                    { name: 'IsActive', index: 'IsActive', align: 'center', editable: true, edittype: 'checkbox', formatter: "checkbox", formatoptions: { disabled: true }, search: false, width: 70 },
                    { name: 'IsRequestor', index: 'IsRequestor', align: 'center', editable: true, edittype: 'checkbox', formatter: "checkbox", formatoptions: { disabled: true }, search: false, width: 50 }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData,1);
                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    selectedRowMultiplePassenger = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultiplePassenger, rowData.PassengerRequestorCD);
                  
                },
                onSelectAll: function (id, status) {
                    selectedRowMultiplePassenger = JqgridSelectAll(selectedRowMultiplePassenger, jqgridTableId, id, status, 'PassengerRequestorCD');
                    jQuery("#hdnselectedfccd").val(selectedRowMultiplePassenger);
                },
                afterInsertRow: function (rowid, rowObject) {
                    JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultiplePassenger, rowid, rowObject.PassengerRequestorCD, jqgridTableId);
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                }
            });

            $("#pagesizebox").insertBefore('.ui-paging-info');
            $("#gridPassenger").jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
            
            $("#clientCD").blur(function () {
                var clientCd = $.trim($("#clientCD").val());
                if (clientCd != null && clientCd != '' && clientCd != undefined) {
                    $.ajax({
                        async: true,
                        type: 'Get',
                        url: '/Views/Utilities/GetApi.aspx?apiType=FSS&method=ClientCode&clientId=0&clientCD=' + clientCd + '&isInActive=false',
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            if (!IsNullOrEmptyOrUndefined(data)) {
                                var NotFound = data.indexOf('Failed to execute api');
                                if (NotFound == 0) {
                                    $("#clientId").val(0);
                                    $("#errorMsg").addClass('showDiv');
                                } else {
                                    var data = JSON.parse($('<div/>').html(data).text());
                                    if (data != 0) {
                                        $("#clientId").val(data);
                                        $("#clientCD").val(clientCd.toUpperCase());
                                        $("#errorMsg").removeClass().addClass('hideDiv');
                                    }
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (!IsNullOrEmptyOrUndefined(content) || !IsNullOrEmptyOrUndefined(textStatus)) {
                                if ((!IsNullOrEmptyOrUndefined(content) && content.indexOf('404')) || !IsNullOrEmptyOrUndefined(textStatus)) {
                                    $("#clientId").val(0);
                                    $("#errorMsg").removeClass().addClass('showDiv');
                                }
                            }
                        }
                    });
                }
                else {
                    $("#clientId").val(0);
                    $("#errorMsg").removeClass().addClass('hideDiv');
                }

            });

        });

        function reloadPassengers() {
            $(jqgridTableId).jqGrid().trigger('reloadGrid');
            selectedRowMultiplePassenger = jQuery("#hdnselectedfccd").val();
            var clientcd = $("#clientCD").val();
            if (clientcd == '')
                $("#errorMsg").removeClass().addClass('hideDiv');
        }

    </script>

    <script type="text/javascript">

        function returnToParent(rowData, one) {
            selectedRowMultiplePassenger = jQuery.trim(selectedRowMultiplePassenger);
            if (selectedRowMultiplePassenger.lastIndexOf(",") == selectedRowMultiplePassenger.length - 1)
                selectedRowMultiplePassenger = selectedRowMultiplePassenger.substr(0, selectedRowMultiplePassenger.length - 1);

            var oArg = new Object();
            var oWnd = GetRadWindow();
            if (one==0) {
                oArg.PassengerRequestorCD = selectedRowMultiplePassenger;
            }
            else {
                oArg.PassengerRequestorCD = rowData["PassengerRequestorCD"];
            }
            oArg.PassengerRequestorID = rowData["PassengerRequestorID"];
            oArg.PassengerName = rowData["PassengerName"];
            oArg.DepartmentCD = rowData["DepartmentCD"];
            oArg.DepartmentName = rowData["DepartmentName"];
            oArg.AdditionalPhoneNum = rowData["AdditionalPhoneNum"];
            oArg.PhoneNum = rowData["PhoneNum"];
            oArg.Arg1 = oArg.PassengerRequestorCD;
            oArg.CallingButton = "PassengerRequestorCD";
            
            var val = getUrlVarsWithParam(oWnd.GetUrl())["ControlName"];
            if (val === 'RequestorCD') {
                oArg.Arg1 = oArg.PassengerRequestorCD;
                oArg.CallingButton = "RequestorCD";
            }
            if (oArg) {
                oWnd.close(oArg);
            }
        }

    </script>

    <style type="text/css">
        .showDiv {
            text-align: right;
            margin-right: 115px;
            color: red;
            visibility: visible !important;
        }

        .hideDiv {
            visibility: hidden;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true" />
        
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
                function ClientCodePopupClose(oWnd, args) {
                    $("#clientCD").val("");
                    $("#clientId").val("");
                    var arg = args.get_argument();
                    if (arg != null) {
                        if (arg) {
                            $("#clientCD").val(arg.ClientCD);
                            $("#clientId").val(arg.ClientID);
                        }
                    }
                }
            </script>
        </telerik:RadCodeBlock>

        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true" ShowContentDuringLoad="false">
            <Windows>
                <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="ClientCodePopupClose" AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>

        <div class="jqgrid">
            <input type="hidden" id="hdnselectedfccd" value=""/>
            <div>
                <table class="box1">
                    <tr>
                        <td align="left">
                            <div>
                                <input type="checkbox" name="Active" value="Active Only" checked="checked" id="chkActiveOnly" />
                                Active Only
                            <input type="checkbox" name="Homebase" value="Home Base Only" id="chkHomebaseOnly" />
                                Home Base Only
                            <input type="checkbox" name="Request" value="Requestor Only" id="chkRequestorOnly" checked="checked"/>
                                Requestor Only
                            </div>
                            <div style="text-align: right; margin-top: -26px">
                                <input id="clientId" type="hidden" value="" />
                                Client Code:
                                <input id="clientCD" type="text" style="width: 50px" value="" maxlength="5" />
                                <input type="button" class="browse-button" id="btnClientCode" value="" name="btnClientCode" />
                                <input id="btnSearch" class="button" value="Search" type="submit" name="btnSearch" onclick="reloadPassengers(); return false;" />
                            </div>
                            <div id="errorMsg" class="hideDiv">Invalid client code.!</div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridPassenger" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                            </div>
                            <div style="text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
