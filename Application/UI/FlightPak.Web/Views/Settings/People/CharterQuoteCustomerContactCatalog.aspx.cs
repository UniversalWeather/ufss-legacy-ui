﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Security.Policy;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CharterQuoteCustomerContactCatalog : BaseSecuredPage
    {
        #region "Declare Constants,Variables,Objects"
        private ExceptionManager exManager;
        private string ExpDateFormat = "00/00";
        private bool _selectLastModified = false;
        #endregion

        #region "RadAjax Events"
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCQCustomerContacts;
                        }
                        else if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCQCustomerContacts;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCQCustomerContacts, dgCQCustomerContacts, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCQCustomerContacts.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewCQCustomerContactCatalog);
                            SetAsDefault(false);
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }

        /// <summary>
        /// Method for the setting default for the page.
        /// </summary>
        /// <param name="Enable"></param>    
        private void SetAsDefault(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((UserPrincipal.Identity != null) && (UserPrincipal.Identity._fpSettings != null))
                {

                }
                else
                {

                }
                if (Request.QueryString["CQCustomerCode"] != null)
                {
                    tbCQCustomerCode.Text = Server.UrlDecode(Request.QueryString["CQCustomerCode"].ToString());
                    CheckCQCustomerExist();
                }
                if (Request.QueryString["CQCustomerName"] != null)
                {
                    tbCQCustomerContacts.Text = Server.UrlDecode(Request.QueryString["CQCustomerName"].ToString());
                }
                tbCQCustomerCode.Enabled = false;
                tbCQCustomerContacts.Enabled = false;
            }
        }

        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                if (BindDataSwitch)
                {
                    dgCQCustomerContacts.Rebind();
                }
                if (dgCQCustomerContacts.MasterTableView.Items.Count > 0)
                {
                    //if (!IsPostBack)
                    //{
                    //    Session["CQCustomerContactID"] = null;
                    //}
                    if (Session["CQCustomerContactID"] == null)
                    {
                        dgCQCustomerContacts.SelectedIndexes.Add(0);
                        Session["CQCustomerContactID"] = dgCQCustomerContacts.Items[0].GetDataKeyValue("CQCustomerContactID").ToString();
                    }

                    if (dgCQCustomerContacts.SelectedIndexes.Count == 0)
                        dgCQCustomerContacts.SelectedIndexes.Add(0);

                    LoadControlData();
                    EnableForm(false);
                    GridEnable(true, true, true);
                }
                else
                {
                    ClearForm();
                    EnableForm(false);
                    GridEnable(true, true, true);
                }
            }
        }

        private void DefaultSelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CQCustomerContactID"] != null)
                {
                    string ID = Session["CQCustomerContactID"].ToString();
                    foreach (GridDataItem Item in dgCQCustomerContacts.MasterTableView.Items)
                    {
                        if (Item["CQCustomerContactID"].Text.Trim() == ID)
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                    if (dgCQCustomerContacts.SelectedItems.Count == 0)
                    {
                        DefaultSelection(false);
                    }
                }
                else
                {
                    DefaultSelection(false);
                }
            }
        }

        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton lbtnInsertCtl = (LinkButton)dgCQCustomerContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                LinkButton lbtnDelCtl = (LinkButton)dgCQCustomerContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                LinkButton lbtnEditCtl = (LinkButton)dgCQCustomerContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                if (IsAuthorized(Permission.Database.AddCQCustomerContactCatalog) || true)
                {
                    lbtnInsertCtl.Visible = true;
                    if (Add)
                    {
                        lbtnInsertCtl.Enabled = true;
                    }
                    else
                    {
                        lbtnInsertCtl.Enabled = false;
                    }
                }
                else
                {
                    lbtnInsertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteCQCustomerContactCatalog) || true)
                {
                    lbtnDelCtl.Visible = true;
                    if (Delete)
                    {
                        lbtnDelCtl.Enabled = true;
                        lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        lbtnDelCtl.Enabled = false;
                        lbtnDelCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnDelCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditCQCustomerContactCatalog) || true)
                {
                    lbtnEditCtl.Visible = true;
                    if (Edit)
                    {
                        lbtnEditCtl.Enabled = true;
                        lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        lbtnEditCtl.Enabled = false;
                        lbtnEditCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnEditCtl.Visible = false;
                }
            }
        }

        private void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                tbCQCustomerCode.Enabled = false;
                tbCQCustomerContacts.Enabled = false;

                chkMainChoice.Enabled = Enable;
                tbCode.Enabled = false;
                tbContactName.Enabled = Enable;
                tbFirstName.Enabled = Enable;
                tbMiddleName.Enabled = Enable;
                tbLastName.Enabled = Enable;
                tbTitle.Enabled = Enable;
                tbAddr1.Enabled = Enable;
                tbAddr2.Enabled = Enable;
                tbAddr3.Enabled = Enable;
                tbCity.Enabled = Enable;
                tbState.Enabled = Enable;
                tbCountryCode.Enabled = Enable;
                btnCountry.Enabled = Enable;
                tbPostal.Enabled = Enable;
                tbHomePhone.Enabled = Enable;
                tbBusinessPhone.Enabled = Enable;
                tbOtherPhone.Enabled = Enable;
                tbCellPhoneNum.Enabled = Enable;
                tbCellPhoneNum2.Enabled = Enable;
                tbBusinessEmail.Enabled = Enable;
                tbPersonalEmail.Enabled = Enable;
                tbOtherEmail.Enabled = Enable;
                tbHomeFax.Enabled = Enable;
                tbBusinessFax.Enabled = Enable;
                tbEmailId.Enabled = Enable;
                tbAdditionalContact.Enabled = Enable;
                tbCreditName1.Enabled = Enable;
                tbCreditNum1.Enabled = Enable;
                tbCreditCardType1.Enabled = Enable;
                rmtbCreditCardExpirationDate1.Enabled = Enable;
                tbCreditSecurityCode1.Enabled = Enable;
                tbCreditName2.Enabled = Enable;
                tbCreditNum2.Enabled = Enable;
                tbCreditCardType2.Enabled = Enable;
                rmtbCreditCardExpirationDate2.Enabled = Enable;
                tbCreditSecurityCode2.Enabled = Enable;
                tbNotes.Enabled = Enable;

                btnSaveChanges.Visible = Enable;
                btnSaveChangesTop.Visible = Enable;
                btnCancel.Visible = Enable;
                btnCancelTop.Visible = Enable;
            }
        }

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string EmptyString = string.Empty;
                hdnSave.Value = EmptyString;

                //tbCQCustomerCode.Text = EmptyString;
                //tbCQCustomerContacts.Text = EmptyString;

                //hdnCQCustomerID.Value = EmptyString;
                chkMainChoice.Checked = false;
                tbCode.Text = "0";
                hdnCQCustomerContactID.Value = EmptyString;
                tbContactName.Text = EmptyString;
                tbFirstName.Text = EmptyString;
                tbMiddleName.Text = EmptyString;
                tbLastName.Text = EmptyString;
                tbTitle.Text = EmptyString;
                tbAddr1.Text = EmptyString;
                tbAddr2.Text = EmptyString;
                tbAddr3.Text = EmptyString;
                tbCity.Text = EmptyString;
                tbState.Text = EmptyString;
                tbCountryCode.Text = EmptyString;
                hdnCountryID.Value = EmptyString;
                tbPostal.Text = EmptyString;
                tbHomePhone.Text = EmptyString;
                tbBusinessPhone.Text = EmptyString;
                tbOtherPhone.Text = EmptyString;
                tbCellPhoneNum.Text = EmptyString;
                tbCellPhoneNum2.Text = EmptyString;
                tbBusinessEmail.Text = EmptyString;
                tbPersonalEmail.Text = EmptyString;
                tbOtherEmail.Text = EmptyString;
                tbHomeFax.Text = EmptyString;
                tbBusinessFax.Text = EmptyString;
                tbEmailId.Text = EmptyString;
                tbAdditionalContact.Text = EmptyString;
                tbCreditName1.Text = EmptyString;
                tbCreditNum1.Text = EmptyString;
                tbCreditCardType1.Text = EmptyString;
                rmtbCreditCardExpirationDate1.Text = ExpDateFormat;
                tbCreditSecurityCode1.Text = EmptyString;
                tbCreditName2.Text = EmptyString;
                tbCreditNum2.Text = EmptyString;
                tbCreditCardType2.Text = EmptyString;
                rmtbCreditCardExpirationDate2.Text = ExpDateFormat;
                tbCreditSecurityCode2.Text = EmptyString;
                tbNotes.Text = EmptyString;
            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                CQCustomerContact oCQCustomerContact = new CQCustomerContact();
                if (Session["CQCustomerContactID"] != null)
                {
                    oCQCustomerContact.CQCustomerContactID = Convert.ToInt64(Session["CQCustomerContactID"].ToString());

                    List<GetCQCustomerContactByCQCustomerContactID> CustomerContactList = new List<GetCQCustomerContactByCQCustomerContactID>();
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        CustomerContactList = objService.GetCQCustomerContactByCQCustomerContactID(oCQCustomerContact).EntityList;
                        if (CustomerContactList.Count != 0)
                        {
                            ClearForm();
                            if (CustomerContactList[0].CQCustomerID != null)
                            {
                                hdnCQCustomerContactID.Value = CustomerContactList[0].CQCustomerContactID.ToString().Trim();
                            }
                            if (CustomerContactList[0].IsChoice != null)
                            {
                                chkMainChoice.Checked = CustomerContactList[0].IsChoice.Value;
                            }
                            else
                            {
                                chkMainChoice.Checked = false;
                            }
                            if (CustomerContactList[0].CQCustomerID != null)
                            {
                                hdnCQCustomerID.Value = CustomerContactList[0].CQCustomerID.ToString().Trim();
                            }
                            if (CustomerContactList[0].CQCustomerContactCD != null)
                            {
                                tbCode.Text = CustomerContactList[0].CQCustomerContactCD.ToString().Trim();
                            }
                            if (CustomerContactList[0].FirstName != null)
                            {
                                tbFirstName.Text = CustomerContactList[0].FirstName.ToString().Trim();
                            }
                            if (CustomerContactList[0].MiddleName != null)
                            {
                                tbMiddleName.Text = CustomerContactList[0].MiddleName.ToString().Trim();
                            }
                            if (CustomerContactList[0].LastName != null)
                            {
                                tbLastName.Text = CustomerContactList[0].LastName.ToString().Trim();
                            }
                            if (CustomerContactList[0].Title != null)
                            {
                                tbTitle.Text = CustomerContactList[0].Title.ToString().Trim();
                            }
                            if (CustomerContactList[0].Addr1 != null)
                            {
                                tbAddr1.Text = CustomerContactList[0].Addr1.ToString().Trim();
                            }
                            if (CustomerContactList[0].Addr2 != null)
                            {
                                tbAddr2.Text = CustomerContactList[0].Addr2.ToString().Trim();
                            }
                            if (CustomerContactList[0].Addr3 != null)
                            {
                                tbAddr3.Text = CustomerContactList[0].Addr3.ToString().Trim();
                            }
                            if (CustomerContactList[0].CityName != null)
                            {
                                tbCity.Text = CustomerContactList[0].CityName.ToString().Trim();
                            }
                            if (CustomerContactList[0].StateName != null)
                            {
                                tbState.Text = CustomerContactList[0].StateName.ToString().Trim();
                            }
                            if (CustomerContactList[0].CountryID != null)
                            {
                                hdnCountryID.Value = CustomerContactList[0].CountryID.ToString().Trim();
                            }
                            if (CustomerContactList[0].CountryCD != null)
                            {
                                tbCountryCode.Text = CustomerContactList[0].CountryCD.ToString().Trim();
                            }
                            if (CustomerContactList[0].PostalZipCD != null)
                            {
                                tbPostal.Text = CustomerContactList[0].PostalZipCD.ToString().Trim();
                            }
                            if (CustomerContactList[0].PhoneNum != null)
                            {
                                tbHomePhone.Text = CustomerContactList[0].PhoneNum.ToString().Trim();
                            }
                            if (CustomerContactList[0].BusinessPhoneNum != null)
                            {
                                tbBusinessPhone.Text = CustomerContactList[0].BusinessPhoneNum.ToString().Trim();
                            }
                            if (CustomerContactList[0].OtherPhoneNum != null)
                            {
                                tbOtherPhone.Text = CustomerContactList[0].OtherPhoneNum.ToString().Trim();
                            }
                            if (CustomerContactList[0].PrimaryMobileNum != null)
                            {
                                tbCellPhoneNum.Text = CustomerContactList[0].PrimaryMobileNum.ToString().Trim();
                            }
                            if (CustomerContactList[0].SecondaryMobileNum != null)
                            {
                                tbCellPhoneNum2.Text = CustomerContactList[0].SecondaryMobileNum.ToString().Trim();
                            }
                            if (CustomerContactList[0].EmailID != null)
                            {
                                tbEmailId.Text = CustomerContactList[0].EmailID.ToString().Trim();
                            }
                            if (CustomerContactList[0].PersonalEmailID != null)
                            {
                                tbPersonalEmail.Text = CustomerContactList[0].PersonalEmailID.ToString().Trim();
                            }
                            if (CustomerContactList[0].OtherEmailID != null)
                            {
                                tbOtherEmail.Text = CustomerContactList[0].OtherEmailID.ToString().Trim();
                            }
                            if (CustomerContactList[0].FaxNum != null)
                            {
                                tbHomeFax.Text = CustomerContactList[0].FaxNum.ToString().Trim();
                            }
                            if (CustomerContactList[0].BusinessFaxNum != null)
                            {
                                tbBusinessFax.Text = CustomerContactList[0].BusinessFaxNum.ToString().Trim();
                            }
                            if (CustomerContactList[0].BusinessEmailID != null)
                            {
                                tbBusinessEmail.Text = CustomerContactList[0].BusinessEmailID.ToString().Trim();
                            }
                            // need to check on this entity exist?
                            if (CustomerContactList[0].Notes != null)
                            {
                                tbNotes.Text = CustomerContactList[0].Notes.ToString().Trim();
                            }
                            if (CustomerContactList[0].CreditName1 != null)
                            {
                                tbCreditName1.Text = CustomerContactList[0].CreditName1.ToString().Trim();
                            }
                            if (CustomerContactList[0].CreditNum1 != null)
                            {
                                tbCreditNum1.Text = CustomerContactList[0].CreditNum1.ToString().Trim();
                            }
                            if (CustomerContactList[0].CardType1 != null)
                            {
                                tbCreditCardType1.Text = CustomerContactList[0].CardType1.ToString().Trim();
                            }
                            if (CustomerContactList[0].ExpirationDate1 != null)
                            {
                                rmtbCreditCardExpirationDate1.Text = CustomerContactList[0].ExpirationDate1.ToString().Trim();
                            }
                            if (CustomerContactList[0].SecurityCode1 != null)
                            {
                                tbCreditSecurityCode1.Text = CustomerContactList[0].SecurityCode1.ToString().Trim();
                            }
                            if (CustomerContactList[0].CreditName2 != null)
                            {
                                tbCreditName2.Text = CustomerContactList[0].CreditName2.ToString().Trim();
                            }
                            if (CustomerContactList[0].CreditNum2 != null)
                            {
                                tbCreditNum2.Text = CustomerContactList[0].CreditNum2.ToString().Trim();
                            }
                            if (CustomerContactList[0].CardType2 != null)
                            {
                                tbCreditCardType2.Text = CustomerContactList[0].CardType2.ToString().Trim();
                            }
                            if (CustomerContactList[0].ExpirationDate2 != null)
                            {
                                rmtbCreditCardExpirationDate2.Text = CustomerContactList[0].ExpirationDate2.ToString().Trim();
                            }
                            if (CustomerContactList[0].SecurityCode2 != null)
                            {
                                tbCreditSecurityCode2.Text = CustomerContactList[0].SecurityCode2.ToString().Trim();
                            }
                            if (CustomerContactList[0].MoreInfo != null)
                            {
                                tbAdditionalContact.Text = CustomerContactList[0].MoreInfo.ToString().Trim();
                            }
                            Label lbLastUpdatedUser;
                            lbLastUpdatedUser = (Label)dgCQCustomerContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (CustomerContactList[0].LastUpdUID != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + CustomerContactList[0].LastUpdUID.ToString());
                            }
                            else
                            {
                                lbLastUpdatedUser.Text = string.Empty;
                            }
                            if (CustomerContactList[0].LastUpdTS != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(CustomerContactList[0].LastUpdTS.ToString())));
                            }
                            lbColumnName1.Text = System.Web.HttpUtility.HtmlEncode("First Name");
                            lbColumnName2.Text = System.Web.HttpUtility.HtmlEncode("Last Name");
                            lbColumnValue1.Text = System.Web.HttpUtility.HtmlEncode(Convert.ToString(CustomerContactList[0].FirstName));
                            lbColumnValue2.Text = System.Web.HttpUtility.HtmlEncode(Convert.ToString(CustomerContactList[0].LastName));
                        }
                    }
                }
            }
        }

        private CQCustomerContact GetItems(CQCustomerContact oCQCustomerContact)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQCustomerContact))
            {
                if (hdnSave.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnCQCustomerContactID.Value))
                    {
                        oCQCustomerContact.CQCustomerContactID = Convert.ToInt64(hdnCQCustomerContactID.Value);
                    }
                }
                if (!string.IsNullOrEmpty(hdnCQCustomerID.Value))
                {
                    oCQCustomerContact.CQCustomerID = Convert.ToInt64(hdnCQCustomerID.Value);
                }
                oCQCustomerContact.IsChoice = chkMainChoice.Checked;
                if (!string.IsNullOrEmpty(tbCode.Text.Trim()))
                {
                    oCQCustomerContact.CQCustomerContactCD = Convert.ToInt32(tbCode.Text.Trim());
                }
                if (!string.IsNullOrEmpty(tbContactName.Text.Trim()))
                {
                    oCQCustomerContact.ContactName = tbContactName.Text.Trim();
                }

                if (!string.IsNullOrEmpty(tbFirstName.Text.Trim()))
                {
                    oCQCustomerContact.FirstName = tbFirstName.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbMiddleName.Text.Trim()))
                {
                    oCQCustomerContact.MiddleName = tbMiddleName.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbLastName.Text.Trim()))
                {
                    oCQCustomerContact.LastName = tbLastName.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbTitle.Text.Trim()))
                {
                    oCQCustomerContact.Title = tbTitle.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbAddr1.Text.Trim()))
                {
                    oCQCustomerContact.Addr1 = tbAddr1.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbAddr2.Text.Trim()))
                {
                    oCQCustomerContact.Addr2 = tbAddr2.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbAddr3.Text.Trim()))
                {
                    oCQCustomerContact.Addr3 = tbAddr3.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCity.Text.Trim()))
                {
                    oCQCustomerContact.CityName = tbCity.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbState.Text.Trim()))
                {
                    oCQCustomerContact.StateName = tbState.Text.Trim();
                }
                if (!string.IsNullOrEmpty(hdnCountryID.Value.Trim()))
                {
                    oCQCustomerContact.CountryID = Convert.ToInt64(hdnCountryID.Value.Trim());
                }
                if (!string.IsNullOrEmpty(tbPostal.Text.Trim()))
                {
                    oCQCustomerContact.PostalZipCD = tbPostal.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbHomePhone.Text.Trim()))
                {
                    oCQCustomerContact.PhoneNum = tbHomePhone.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBusinessPhone.Text.Trim()))
                {
                    oCQCustomerContact.BusinessPhoneNum = tbBusinessPhone.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbOtherPhone.Text.Trim()))
                {
                    oCQCustomerContact.OtherPhoneNum = tbOtherPhone.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCellPhoneNum.Text.Trim()))
                {
                    oCQCustomerContact.PrimaryMobileNum = tbCellPhoneNum.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCellPhoneNum2.Text.Trim()))
                {
                    oCQCustomerContact.SecondaryMobileNum = tbCellPhoneNum2.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBusinessEmail.Text.Trim()))
                {
                    oCQCustomerContact.BusinessEmailID = tbBusinessEmail.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbPersonalEmail.Text.Trim()))
                {
                    oCQCustomerContact.PersonalEmailID = tbPersonalEmail.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbOtherEmail.Text.Trim()))
                {
                    oCQCustomerContact.OtherEmailID = tbOtherEmail.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbHomeFax.Text.Trim()))
                {
                    oCQCustomerContact.FaxNum = tbHomeFax.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbBusinessFax.Text.Trim()))
                {
                    oCQCustomerContact.BusinessFaxNum = tbBusinessFax.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbEmailId.Text.Trim()))
                {
                    oCQCustomerContact.EmailID = tbEmailId.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbAdditionalContact.Text.Trim()))
                {
                    oCQCustomerContact.MoreInfo = tbAdditionalContact.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCreditName1.Text.Trim()))
                {
                    oCQCustomerContact.CreditName1 = tbCreditName1.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCreditNum1.Text.Trim()))
                {
                    oCQCustomerContact.CreditNum1 = tbCreditNum1.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCreditCardType1.Text.Trim()))
                {
                    oCQCustomerContact.CardType1 = tbCreditCardType1.Text.Trim();
                }
                if (!string.IsNullOrEmpty(rmtbCreditCardExpirationDate1.Text.Trim()))
                {
                    oCQCustomerContact.ExpirationDate1 = rmtbCreditCardExpirationDate1.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCreditSecurityCode1.Text.Trim()))
                {
                    oCQCustomerContact.SecurityCode1 = tbCreditSecurityCode1.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCreditName2.Text.Trim()))
                {
                    oCQCustomerContact.CreditName2 = tbCreditName2.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCreditNum2.Text.Trim()))
                {
                    oCQCustomerContact.CreditNum2 = tbCreditNum2.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCreditCardType2.Text.Trim()))
                {
                    oCQCustomerContact.CardType2 = tbCreditCardType2.Text.Trim();
                }
                if (!string.IsNullOrEmpty(rmtbCreditCardExpirationDate2.Text.Trim()))
                {
                    oCQCustomerContact.ExpirationDate2 = rmtbCreditCardExpirationDate2.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbCreditSecurityCode2.Text.Trim()))
                {
                    oCQCustomerContact.SecurityCode2 = tbCreditSecurityCode2.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbNotes.Text.Trim()))
                {
                    oCQCustomerContact.Notes = tbNotes.Text.Trim();
                }
                oCQCustomerContact.IsDeleted = false;
                return oCQCustomerContact;
            }
        }

        #region "Grid Events"

        protected void dgCQCustomerContacts_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }

        protected void dgCQCustomerContacts_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<GetCQCustomerContactByCQCustomerID> CQCustomerContactList = new List<GetCQCustomerContactByCQCustomerID>();
                            CQCustomerContact oCQCustomerContact = new CQCustomerContact();
                            if (Request.QueryString["CQCustomerID"] != null)
                            {
                                oCQCustomerContact.CQCustomerID = Convert.ToInt64(Server.UrlDecode(Request.QueryString["CQCustomerID"].ToString()));
                            }
                            var objCQCustomerContact = objService.GetCQCustomerContactByCQCustomerID(oCQCustomerContact);
                            if (objCQCustomerContact.ReturnFlag == true)
                            {
                                CQCustomerContactList = objCQCustomerContact.EntityList;
                            }
                            dgCQCustomerContacts.DataSource = CQCustomerContactList;
                            Session["CQCustomerContact"] = CQCustomerContactList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }

        protected void dgCQCustomerContacts_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {

                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgCQCustomerContacts.SelectedIndexes.Clear();
                                hdnSave.Value = "Save";
                                ClearForm();
                                EnableForm(true);
                                GridEnable(true, false, false);
                                tbContactName.Focus();
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["CQCustomerContactID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CQCustomerContact, Convert.ToInt64(Session["CQCustomerContactID"].ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CQCustomerContact);
                                            DefaultSelectItem();
                                            return;
                                        }
                                        LoadControlData();
                                        hdnSave.Value = "Update";
                                        hdnRedirect.Value = "";
                                        EnableForm(true);
                                        GridEnable(false, true, false);
                                        tbContactName.Focus();
                                        DefaultSelectItem();
                                    }
                                }
                                break;
                            case "Filter":
                                foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }

        protected void dgCQCustomerContacts_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        IsValidate = ValidateAll();
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                CQCustomerContact oCQCustomerContact = new CQCustomerContact();
                                oCQCustomerContact = GetItems(oCQCustomerContact);
                                var objResult = objService.AddCQCustomerContact(oCQCustomerContact);
                                //For Data Anotation
                                if (objResult.ReturnFlag == true)
                                {
                                    dgCQCustomerContacts.Rebind();
                                    DefaultSelection(false);
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objResult.ErrorMessage, ModuleNameConstants.Database.CQCustomerContact);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }

        protected void dgCQCustomerContacts_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        IsValidate = ValidateAll();
                        if (IsValidate)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                CQCustomerContact oCQCustomerContact = new CQCustomerContact();
                                oCQCustomerContact = GetItems(oCQCustomerContact);
                                var objResult = objService.UpdateCQCustomerContact(oCQCustomerContact);
                                //For Data Anotation
                                if (objResult.ReturnFlag == true)
                                {
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    ///////Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        if (Session["CQCustomerContactID"] != null)
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.CQCustomerContact, Convert.ToInt64(Session["CQCustomerContactID"].ToString().Trim()));
                                        }
                                    }
                                    GridEnable(true, true, true);
                                    DefaultSelectItem();
                                    LoadControlData();
                                    EnableForm(false);
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objResult.ErrorMessage, ModuleNameConstants.Database.CQCustomerContact);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }

        }

        protected void dgCQCustomerContacts_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CQCustomerContactID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                CQCustomerContact oCQCustomerContact = new CQCustomerContact();
                                oCQCustomerContact.CQCustomerContactID = Convert.ToInt64(Session["CQCustomerContactID"].ToString());
                                oCQCustomerContact.IsChoice = chkMainChoice.Checked;
                                oCQCustomerContact.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.CQCustomerContact, Convert.ToInt64(Session["CQCustomerContactID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CQCustomerContact);
                                        return;
                                    }
                                }
                                objService.DeleteCQCustomerContact(oCQCustomerContact);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                dgCQCustomerContacts.Rebind();
                                DefaultSelection(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.CQCustomerContact, Convert.ToInt64(Session["CQCustomerContactID"].ToString()));
                    }
                }
            }
        }

        protected void dgCQCustomerContacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                DefaultSelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {

                            if ((btnSaveChanges.Visible == false) && (btnSaveChangesTop.Visible == false))
                            {
                                GridDataItem Item = dgCQCustomerContacts.SelectedItems[0] as GridDataItem;
                                if (Item.GetDataKeyValue("CQCustomerContactID") != null)
                                {
                                    Session["CQCustomerContactID"] = Item.GetDataKeyValue("CQCustomerContactID").ToString();
                                    LoadControlData();
                                    EnableForm(false);
                                    GridEnable(true, true, true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                    }
                }
            }
        }

        protected void dgCQCustomerContact_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCQCustomerContacts.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }

        protected void dgCQCustomerContact_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCQCustomerContacts, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }
        #endregion

        #region "Validations"

        private bool ValidateAll()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnCustomValidateReturn = true;
                if (CheckCountryExist() == false)
                {
                    return ReturnCustomValidateReturn = false;
                }
                return ReturnCustomValidateReturn;
            }
        }
        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCountryCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        return CheckCountryExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }
        private bool CheckCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(tbCountryCode.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<Country> CountryList = new List<Country>();
                        var objRetVal = objservice.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper().Equals(tbCountryCode.Text.ToString().ToUpper().Trim())).ToList();
                        if (objRetVal.Count() != 0)
                        {
                            CountryList = (List<Country>)objRetVal;
                            hdnCountryID.Value = CountryList[0].CountryID.ToString();
                            RadAjaxManager1.FocusControl(tbPostal.ClientID);
                            ReturnValue = true;
                        }
                        else
                        {
                            cvCountry.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCountryCode.ClientID);
                            ReturnValue = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }

        private bool CheckCQCustomerExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(tbCQCustomerCode.Text))
                {
                    CQCustomer objCQCustomer = new CQCustomer();
                    objCQCustomer.CQCustomerCD = tbCQCustomerCode.Text.Trim();
                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objservice.GetCQCustomerByCQCustomerCD(objCQCustomer).EntityList;
                        if (objRetVal.Count() != 0)
                        {
                            hdnCQCustomerID.Value = objRetVal[0].CQCustomerID.ToString();
                            ReturnValue = false;
                        }
                        else
                        {
                            ReturnValue = true;
                        }
                    }
                }
                return ReturnValue;
            }
        }

        #endregion

        /// <summary>
        /// Command Event Trigger for Save or Update CQCustomer Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgCQCustomerContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgCQCustomerContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }
        }

        /// <summary>
        /// Cancel CQCustomer Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["CQCustomerContactID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.CQCustomerContact, Convert.ToInt64(Session["CQCustomerContactID"].ToString().Trim()));
                                    DefaultSelection(false);
                                }
                            }
                        }
                        DefaultSelection(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        protected void QueryStringButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect("CharterQuoteCustomerCatalog.aspx?Screen=CharterQuote", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CQCustomerContact);
                }
            }

        }
        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCQCustomerContacts.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            CQCustomerContact oCQCustomerContact = new CQCustomerContact();
            if (Request.QueryString["CQCustomerID"] != null)
            {
                oCQCustomerContact.CQCustomerID = Convert.ToInt64(Server.UrlDecode(Request.QueryString["CQCustomerID"].ToString()));
            }

            var CQCustomerContactValue = FPKMstService.GetCQCustomerContactByCQCustomerID(oCQCustomerContact);
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CQCustomerContactValue);
            List<FlightPakMasterService.GetCQCustomerContactByCQCustomerID> filteredList = GetFilteredList(CQCustomerContactValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CQCustomerContactID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCQCustomerContacts.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCQCustomerContacts.CurrentPageIndex = PageNumber;
            dgCQCustomerContacts.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetCQCustomerContactByCQCustomerID CQCustomerContactValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["CQCustomerContactID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CQCustomerContactValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().CQCustomerContactID;
                Session["CQCustomerContactID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetCQCustomerContactByCQCustomerID> GetFilteredList(ReturnValueOfGetCQCustomerContactByCQCustomerID CQCustomerContactValue)
        {
            List<FlightPakMasterService.GetCQCustomerContactByCQCustomerID> filteredList = new List<FlightPakMasterService.GetCQCustomerContactByCQCustomerID>();

            if (CQCustomerContactValue.ReturnFlag)
            {
                filteredList = CQCustomerContactValue.EntityList;
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCQCustomerContacts.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    DefaultSelection(false);
                }
            }
        }
    }

}
