﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PassengerChecklistPop.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.PassengerChecklistPop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PAX Additional Information</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <style type="text/css">
        .RadGrid .rgWrap {
        padding:0px !important;
        }
        .RadGrid .rgPager .rgPagerButton {
        margin:0px !important;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
     
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCrewCheckList.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "PassengerInfoCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "PassengerDescription")
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "PassengerInformationID")
                    //var cell3 = MasterTable.getCellByColumnUniqueName(row, "AdditionalINFOValue")
                }

                if (selectedRows.length > 0) {
                    oArg.AdditionalINFOCD = cell1.innerHTML;
                    oArg.AdditionalINFODescription = cell2.innerHTML;
                    oArg.AdditionalINFOID = cell3.innerHTML;
                    //oArg.AdditionalINFOValue = cell3.innerHTML;
                }
                else {
                    oArg.AdditionalINFOCD = "";
                    oArg.AdditionalINFODescription = "";
                    oArg.AdditionalINFOID = "";
                    //oArg.AdditionalINFOValue = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }

            }
            function RowDblClick() {
                var masterTable = $find("<%= dgCrewCheckList.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
         <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgCrewCheckList" runat="server" AllowSorting="true" Visible="true"
            OnNeedDataSource="dgCrewCheckList_BindData" OnItemCommand="dgCrewCheckList_ItemCommand"
            OnInsertCommand="dgCrewCheckList_InsertCommand" PageSize="10" Width="500px"
            AllowPaging="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"
            AutoGenerateColumns="false" Height="341px" OnPreRender="dgCrewCheckList_PreRender">
            <MasterTableView DataKeyNames="PassengerInformationID,CustomerID,PassengerInfoCD,PassengerDescription,ClientID,LastUpdUID,LastUpdTS,IsShowOnTrip,IsDeleted" CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="PassengerInfoCD" HeaderText="Code" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px" FilterDelay="500"
                        FilterControlWidth="80px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PassengerDescription" HeaderText="Description" FilterDelay="500"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="400px" FilterControlWidth="380px">
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="AdditionalINFOValue" HeaderText="Additional Information"
                        AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="PassengerInformationID" HeaderText="PassengerInformationID" FilterDelay="500"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo" Display="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;">
                        <%--                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                        Ok</button>--%>
                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick = "RowDblClick" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
             <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Label ID="InjectScript" runat="server"></asp:Label>
        <br />
        <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    </form>
</body>
</html>