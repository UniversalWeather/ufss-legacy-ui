﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewMemberMultipleSelectionPopup : System.Web.UI.Page
    {
        private string CrewID;
        private string CrewCD;
        List<string> crewRoaster = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["CrewID"] ))
            {
                CrewID = Request.QueryString["CrewID"].Trim();
            }
        }

        /// <summary>
        /// Bind Crew Roaster Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void CrewMember_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var CrewMemberVal = CrewService.GetCrewList();
                if (CrewMemberVal.ReturnFlag == true)
                {
                    dgCrewMember.DataSource = CrewMemberVal.EntityList.Where(x => x.IsDeleted == false).ToList();
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            e.Updated = dgCrewMember;
            SelectItem();
        }
        private void SelectItem()
        {

            if (!string.IsNullOrEmpty(CrewID))
            {

                foreach (GridDataItem item in dgCrewMember.MasterTableView.Items)
                {
                    List<string> lstID = new List<string>(CrewID.Split(','));

                    foreach (string strId in lstID)
                    {
                        if (item["CrewID"].Text.Trim() == strId)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                    CrewCD = item["CrewCD"].Text.Trim();
                }
            }
            else
            {
                if (dgCrewMember.MasterTableView.Items.Count > 0)
                {
                    dgCrewMember.SelectedIndexes.Add(0);

                }
            }
        }

    }
}