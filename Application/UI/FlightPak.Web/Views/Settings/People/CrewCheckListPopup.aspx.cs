﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewCheckListPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            lbMessage.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

        }

        /// <summary>
        /// Method to Bind Grid Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewCheckListService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CheckListValue = CrewCheckListService.GetCrewChecklistList();
                            List<FlightPakMasterService.CrewCheckList> lstCrewCheckList = new List<FlightPakMasterService.CrewCheckList>();
                            if (CheckListValue.ReturnFlag == true)
                            {
                                lstCrewCheckList = CheckListValue.EntityList; 
                            }
                            dgCrewCheckList.DataSource = lstCrewCheckList;
                            Session["CrewCheckListEntity"] = lstCrewCheckList;
                            if (chkSearchActiveOnly.Checked == true)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

        }

        /// <summary>
        /// Method to trigger Grid Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

        }

        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

        }

        /// <summary>
        /// Method to Bind into "CrewNewCheckList" session, and 
        /// Call "CloseAndRebind" javascript function to Close and
        /// Rebind the selected record into parent page.
        /// </summary>
        private void InsertSelectedRow()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewCheckList.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];

                        if (CheckIfExists(Item.GetDataKeyValue("CrewCheckCD").ToString()))
                        {
                            lbMessage.Text = "Warning, Checklist Code Already Exists.";
                            lbMessage.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            bool IsExist = false;
                            List<FlightPakMasterService.GetCrewCheckListDate> GetCrewCheckListDateInfo = (List<FlightPakMasterService.GetCrewCheckListDate>)Session["CrewCheckList"];
                            for (int Index = 0; Index < GetCrewCheckListDateInfo.Count; Index++)
                            {
                                if (GetCrewCheckListDateInfo[Index].CheckListCD == Item.GetDataKeyValue("CrewCheckCD").ToString())
                                {
                                    GetCrewCheckListDateInfo[Index].PreviousCheckDT = null;
                                    GetCrewCheckListDateInfo[Index].DueDT = null;
                                    GetCrewCheckListDateInfo[Index].AlertDT = null;
                                    GetCrewCheckListDateInfo[Index].GraceDT = null;
                                    GetCrewCheckListDateInfo[Index].AlertDays = 0;
                                    GetCrewCheckListDateInfo[Index].AlertDays = Convert.ToInt32("0");
                                    if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays != null)
                                    {
                                        GetCrewCheckListDateInfo[Index].AlertDays = Convert.ToInt32(UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays);
                                    }
                                    GetCrewCheckListDateInfo[Index].GraceDays = 0;
                                    GetCrewCheckListDateInfo[Index].FrequencyMonth = 0;
                                    GetCrewCheckListDateInfo[Index].BaseMonthDT = null;
                                    GetCrewCheckListDateInfo[Index].TotalREQFlightHrs = 0;
                                    GetCrewCheckListDateInfo[Index].IsMonthEnd = false;
                                    GetCrewCheckListDateInfo[Index].IsStopCALC = false;
                                    GetCrewCheckListDateInfo[Index].IsOneTimeEvent = false;
                                    GetCrewCheckListDateInfo[Index].IsNoConflictEvent = false;
                                    GetCrewCheckListDateInfo[Index].IsNoCrewCheckListREPTt = false;
                                    GetCrewCheckListDateInfo[Index].IsNoChecklistREPT = false;
                                    GetCrewCheckListDateInfo[Index].IsInActive = false;
                                    GetCrewCheckListDateInfo[Index].Specific = 0;
                                    GetCrewCheckListDateInfo[Index].IsPassedDueAlert = false;
                                    GetCrewCheckListDateInfo[Index].IsPilotInCommandFAR91 = false;
                                    GetCrewCheckListDateInfo[Index].IsPilotInCommandFAR135 = false;
                                    GetCrewCheckListDateInfo[Index].IsSecondInCommandFAR91 = false;
                                    GetCrewCheckListDateInfo[Index].IsSecondInCommandFAR135 = false;
                                    GetCrewCheckListDateInfo[Index].IsCompleted = false;
                                    GetCrewCheckListDateInfo[Index].OriginalDT = null;
                                    GetCrewCheckListDateInfo[Index].IsPrintStatus = false;
                                    GetCrewCheckListDateInfo[Index].Frequency = 1;
                                    GetCrewCheckListDateInfo[Index].IsNextMonth = false;
                                    GetCrewCheckListDateInfo[Index].IsEndCalendarYear = false;
                                    GetCrewCheckListDateInfo[Index].IsScheduleCheck = false;
                                    GetCrewCheckListDateInfo[Index].IsDeleted = false;


                                    
                                    IsExist = true;
                                }
                            }

                            Session["CrewCheckList"] = GetCrewCheckListDateInfo;

                            if (IsExist == false)
                            {
                                List<GetCrewCheckListDate> GetCrewCheckListDate = new List<GetCrewCheckListDate>();
                                GetCrewCheckListDate GetCrewCheckListDateDef = new GetCrewCheckListDate();
                                //GetCrewCheckListDateDef.CheckListID = Convert.ToInt64(Item.GetDataKeyValue("CrewCheckID").ToString());
                                GetCrewCheckListDateDef.CheckListCD = Item.GetDataKeyValue("CrewCheckCD").ToString();
                                GetCrewCheckListDateDef.CrewChecklistDescription = Item.GetDataKeyValue("CrewChecklistDescription").ToString();
                                GetCrewCheckListDateDef.PreviousCheckDT = null;
                                GetCrewCheckListDateDef.DueDT = null;
                                GetCrewCheckListDateDef.AlertDT = null;
                                GetCrewCheckListDateDef.GraceDT = null;
                                GetCrewCheckListDateDef.AlertDays = 0;
                                GetCrewCheckListDateDef.AlertDays = 0;
                                if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays != null)
                                {
                                    GetCrewCheckListDateDef.AlertDays = Convert.ToInt32(UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays);
                                }
                                GetCrewCheckListDateDef.GraceDays = 0;
                                GetCrewCheckListDateDef.FrequencyMonth = 0;
                                GetCrewCheckListDateDef.BaseMonthDT = null;
                                GetCrewCheckListDateDef.TotalREQFlightHrs = 0;
                                GetCrewCheckListDateDef.IsMonthEnd = false;
                                GetCrewCheckListDateDef.IsStopCALC = false;
                                GetCrewCheckListDateDef.IsOneTimeEvent = false;
                                GetCrewCheckListDateDef.IsNoConflictEvent = false;
                                GetCrewCheckListDateDef.IsNoCrewCheckListREPTt = false;
                                GetCrewCheckListDateDef.IsNoChecklistREPT = false;
                                GetCrewCheckListDateDef.IsInActive = false;
                                GetCrewCheckListDateDef.Specific = 0;
                                GetCrewCheckListDateDef.IsPassedDueAlert = false;
                                GetCrewCheckListDateDef.IsPilotInCommandFAR91 = false;
                                GetCrewCheckListDateDef.IsPilotInCommandFAR135 = false;
                                GetCrewCheckListDateDef.IsSecondInCommandFAR91 = false;
                                GetCrewCheckListDateDef.IsSecondInCommandFAR135 = false;
                                GetCrewCheckListDateDef.IsCompleted = false;
                                GetCrewCheckListDateDef.OriginalDT = null;
                                GetCrewCheckListDateDef.IsPrintStatus = false;
                                GetCrewCheckListDateDef.Frequency = 1;
                                GetCrewCheckListDateDef.IsNextMonth = false;
                                GetCrewCheckListDateDef.IsEndCalendarYear = false;
                                GetCrewCheckListDateDef.IsScheduleCheck = false;
                                GetCrewCheckListDateDef.IsDeleted = false;
                                GetCrewCheckListDate.Add(GetCrewCheckListDateDef);
                                Session["CrewNewCheckList"] = GetCrewCheckListDate;
                            }
                            if (Item.GetDataKeyValue("CrewCheckCD") != null)
                            {
                                Session["CrewCheckListCode"] = Item.GetDataKeyValue("CrewCheckCD").ToString();
                            }
                            InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }

        /// <summary>
        /// Function to Check if Crew Roster Check List Code alerady exists or not, by passing Crew Code
        /// </summary>
        /// <returns>True / False</returns>
        private Boolean CheckIfExists(string Code)
        {
            bool ReturnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Code))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    if (Session["CrewCheckList"] != null)
                    {
                        List<GetCrewCheckListDate> CrewCheckList = (List<GetCrewCheckListDate>)Session["CrewCheckList"];
                        var result = (from crew in CrewCheckList
                                      where crew.CheckListCD.ToUpper().Trim() == Code.ToUpper().Trim() &&
                                            crew.IsDeleted == false
                                      select crew);

                        if (result.Count() > 0)
                        { ReturnValue = true; }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }

        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.CrewCheckList> lstCrewCheckList = new List<FlightPakMasterService.CrewCheckList>();
                if (Session["CrewCheckListEntity"] != null)
                {
                    lstCrewCheckList = (List<FlightPakMasterService.CrewCheckList>)Session["CrewCheckListEntity"];
                }
                if (lstCrewCheckList.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstCrewCheckList = lstCrewCheckList.Where(x => x.IsInActive == false).ToList<FlightPakMasterService.CrewCheckList>(); }
                    dgCrewCheckList.DataSource = lstCrewCheckList;
                    if (IsDataBind)
                    {
                        dgCrewCheckList.DataBind();
                    }
                }
                return false;
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            SearchAndFilter(true);
        }
        #endregion

        protected void dgCrewCheckList_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewCheckList, Page.Session);
        }
    }
}