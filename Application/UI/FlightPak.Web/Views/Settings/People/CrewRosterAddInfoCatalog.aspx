﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="CrewRosterAddInfoCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.CrewRosterAddInfoCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">

        function Clicking(sender, args) {
            var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                if (shouldSubmit) {
                    document.getElementById('<%=hdnValue.ClientID%>').value = "Yes";
                    this.click();
                }
                else {
                    document.getElementById('<%=hdnValue.ClientID%>').value = "No";
                    this.click();
                }
            });

            var text = "Add New Additional Information Code To All Crew Members?";
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            oManager.radconfirm(text, callBackFunction, 300, 100, null, "FlightPak System");
            args.set_cancel(true);
        }

        //        function CheckPassportChoice() {            
        //            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;

        //            if ((txtCode != "")) {
        //                Msg = "Add New Additional Information Code To All Crew Members?";
        //                //var IsResult = confirm(Msg, "Crew");
        //                radconfirm(Msg, callBackFn1, 330, 100, '', 'Save');
        //                return false;

        //            }
        //            return false;
        //        }
        //        function callBackFn1(confirmed) {
        //            if (confirmed) {
        //                //var grid = $find(window['gridId']);
        //                //grid.get_masterTableView().fireCommand("DeleteSelected");

        //                document.getElementById('<%=hdnValue.ClientID%>').value = "Yes";
        //            }
        //            //             else {
        //            //                    document.getElementById('<%=hdnValue.ClientID%>').value = "No";
        //            //                }

        //        }

        function CheckTxtBox(control) {
            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);

                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);

                } return false;
            }
        }
    </script>
   
    <script type="text/javascript">
        function pageLoad() {
            $(document).ready(function () {
                var addId = getQuerystring('IsPopup', 'NOT');
                if (addId != 'NOT') {
                    $("#art-sheet").css("width", "100%");
                    $("#art-contentLayout_popup").css("width", "100%");
                    $(".art-Sheet-body").css("width", "412px");
                    $(".art-Sheet-body").css("min-height", "100px");
                    $(".art-Sheet-body").css("height", "157px"); 
                    $(".tblButtonArea").css("margin-right", "10px");
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgCrewRosterAddInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCrewRosterAddInfo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewRosterAddInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="cvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table style="width: 100%" cellpadding="0" cellspacing="0" runat="server" id="table1">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Additional Info Codes</span> <span class="tab-nav-icons">
                        <!--<a href="#"
                        class="search-icon"></a><a href="#" class="save-icon"></a><a href="#" class="print-icon">
                        </a>-->
                        <a href="../../Help/ViewHelp.aspx?Screen=CrewAddInfoHelp" target="_blank" class="help-icon" title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadGrid ID="dgCrewRosterAddInfo" runat="server" OnItemCreated="CrewRosterAddInfo_ItemCreated"
        OnPreRender="CrewAddInfo_PreRender" Visible="true" OnNeedDataSource="CrewRosterAddInfo_BindData"
        OnItemCommand="CrewRosterAddInfo_ItemCommand" Height="341px" OnUpdateCommand="dgCrewRosterAddInfo_UpdateCommand"
        OnInsertCommand="CrewRosterAddInfo_InsertCommand" OnPageIndexChanged="CrewAddInfo_PageIndexChanged"
        OnDeleteCommand="dgCrewRosterAddInfo_DeleteCommand" OnSelectedIndexChanged="dgCrewRosterAddInfo_SelectedIndexChanged">
        <MasterTableView DataKeyNames="CustomerID,CrewInfoID,CrewInfoCD,CrewInformationDescription,LastUpdUID,LastUpdTS"
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="CrewInfoCD" HeaderText="Crew Additional Info Code"
                    AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith" ShowFilterIcon="false"
                    HeaderStyle-Width="120px" FilterControlWidth="100px" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CrewInfoID" HeaderText="CrewInfoID" Display="false"
                    AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo" ShowFilterIcon="false" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CrewInformationDescription" HeaderText="Crew Additional Info"
                    AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith" ShowFilterIcon="false"
                    HeaderStyle-Width="640px" FilterControlWidth="620px" FilterDelay="500">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                        Visible='<%# IsAuthorized(Permission.Database.AddCrewRosterAddInfoCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        Visible='<%# IsAuthorized(Permission.Database.EditCrewRosterAddInfoCatalog)%>'
                        ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete('This Will Delete The Code From All Associated Crew Members. Continue Delete?');"
                        Visible='<%# IsAuthorized(Permission.Database.DeleteCrewRosterAddInfoCatalog)%>'
                        runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                </div>
                <div>
                    <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
    </telerik:RadGrid>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td valign="top" class="tdLabel130">
                        <span class="mnd_text">Additional Info Code</span>
                    </td>
                    <td align="left">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbCode" runat="server" MaxLength="3" CssClass="text40" OnTextChanged="CrewRosterAddInfoCode_TextChanged"
                                        AutoPostBack="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="save" ControlToValidate="tbCode"
                                        Text="Additional Info Code is Required." Display="Dynamic" CssClass="alert-text"
                                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Additional Info Code is Required"
                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <span class="mnd_text">Description</span>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbDescription" runat="server" MaxLength="40" CssClass="text225"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                        ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr align="right">
                    <td class="custom_radbutton">
                        <telerik:RadButton ID="btnSaveChanges" runat="server" Text="Save" ValidationGroup="save"
                            OnClientClicking="Clicking" OnClick="SaveChanges_Click" class="custom_radbutton" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false"
                            OnClick="Cancel_Click" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnValue" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                    </td>
                </tr>
            </table>
            <table id="tblHidden" style="display: none;">
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
