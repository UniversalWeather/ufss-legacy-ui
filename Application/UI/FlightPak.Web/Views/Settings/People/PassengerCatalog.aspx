﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="PassengerCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.PassengerCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <style type="text/css">
        .RadAjax_Sunset {
        height:570px !important;
        }
    </style>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                var deptId = document.getElementById("<%=hdnDepartment.ClientID%>").value;
                var authId = document.getElementById("<%=tbAuth.ClientID%>").value;
                
            }
            function browserName() {
                var agt = navigator.userAgent.toLowerCase();
                if (agt.indexOf("msie") != -1) return 'Internet Explorer';
                if (agt.indexOf("chrome") != -1) return 'Chrome';
                if (agt.indexOf("opera") != -1) return 'Opera';
                if (agt.indexOf("firefox") != -1) return 'Firefox';
                if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
                if (agt.indexOf("netscape") != -1) return 'Netscape';
                if (agt.indexOf("safari") != -1) return 'Safari';
                if (agt.indexOf("staroffice") != -1) return 'Star Office';
                if (agt.indexOf("webtv") != -1) return 'WebTV';
                if (agt.indexOf("beonex") != -1) return 'Beonex';
                if (agt.indexOf("chimera") != -1) return 'Chimera';
                if (agt.indexOf("netpositive") != -1) return 'NetPositive';
                if (agt.indexOf("phoenix") != -1) return 'Phoenix';
                if (agt.indexOf("skipstone") != -1) return 'SkipStone';
                if (agt.indexOf('\/') != -1) {
                    if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                        return navigator.userAgent.substr(0, agt.indexOf('\/'));
                    }
                    else return 'Netscape';
                } else if (agt.indexOf(' ') != -1)
                    return navigator.userAgent.substr(0, agt.indexOf(' '));
                else return navigator.userAgent;
            }

            function GetBrowserName() {
                document.getElementById('<%=hdnBrowserName.ClientID%>').value = browserName();
            }

            function openDuplicates() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                if (document.getElementById('<%=hdnWarning.ClientID%>').value == "1") {
                    var oWnd = oManager.open("../People/PassengerNamePopup.aspx?PaxLastName=" + document.getElementById("<%=tbLastName.ClientID%>").value, "RadCountryMasterPopup1"); 
                }
                else if (document.getElementById('<%=hdnWarning.ClientID%>').value == "0") {
                    oManager.radalert('No Potential Duplicate Passenger Records Found.', 360, 50, 'Passenger Catalog'); 
                }
                else {
                    var oWnd = oManager.open("../People/PassengerNamePopup.aspx", "RadCountryMasterPopup1"); 
                }

            }

            function ShowChecklistSettingsPopup(sender, args) {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../People/PassengerchecklistSettings.aspx", "RadChecklistPopUp");
                return false;

            }

            function ShowCrewCheckListPopup(sender, args) {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../People/PassengerChecklistPop.aspx", "RadCrewCheckListPopup");
                return false;
            }

            //this function is used to display the format if the textbox is empty
            function ValidateEmptyTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0.00";
                }
            }


            function CLickingChecklistDelete(sender, args) {
                var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                    if (shouldSubmit) {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "Yes";
                        this.click();
                    }
                    else {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "No";
                        this.click();
                    }

                });

                var text = "Are you sure you want to delete this PASSENGER CHECKLIST record?";
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.radconfirm(text, callBackFunction, 400, 100, null, "Delete");
                args.set_cancel(true);
            }


            function confirmNameLengthFn(arg) {
                if (arg == true) {

                    document.getElementById('<%=btnSaveChangesdummy.ClientID%>').click();
                }
                else {
                    if (document.getElementById('<%=hdnFirstOrLast.ClientID%>').value == "First") {
                        document.getElementById('<%=tbFirstName.ClientID%>').focus();
                    }
                    else if (document.getElementById('<%=hdnFirstOrLast.ClientID%>').value == "Middle") {
                        document.getElementById('<%=tbMiddleName.ClientID%>').focus();
                    }
                    else if (document.getElementById('<%=hdnFirstOrLast.ClientID%>').value == "Last") {
                        document.getElementById('<%=tbLastName.ClientID%>').focus();
                    }
                }
            }

            function OnClientClick(strPanelToExpand) {
                var PanelBar1 = $find("<%= pnlMain.ClientID %>");
                var PanelBar2 = $find("<%= pnlAdditionalInfo.ClientID %>");
                var PanelBar3 = $find("<%= pnlVisa.ClientID %>");
                var PanelBar4 = $find("<%= Notes.ClientID %>");
                var PanelBar5 = $find("<%= PaxItems.ClientID %>");
                var PanelBar6 = $find("<%= pnlPassport.ClientID %>");
                var PanelBar7 = $find("<%= pnlImage.ClientID %>");
                var PanelBar8 = $find("<%= pnlChecklist.ClientID %>");
                PanelBar1.get_items().getItem(0).set_expanded(false);
                PanelBar2.get_items().getItem(0).set_expanded(false);
                PanelBar3.get_items().getItem(0).set_expanded(false);
                PanelBar4.get_items().getItem(0).set_expanded(false);
                PanelBar5.get_items().getItem(0).set_expanded(false);
                PanelBar6.get_items().getItem(0).set_expanded(false);
                PanelBar7.get_items().getItem(0).set_expanded(false);
                PanelBar8.get_items().getItem(0).set_expanded(false);
                if (strPanelToExpand == "AddInfo") {
                    PanelBar2.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "Visa") {
                    PanelBar3.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "Notes") {
                    PanelBar4.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "Pax Alerts") {
                    PanelBar5.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "Passport") {
                    PanelBar6.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "Image") {
                    PanelBar7.get_items().getItem(0).set_expanded(true);
                }
                return false;
            }
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadNationalityMasterPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbNationality.ClientID%>').value;
                }
                else if (radWin == "RadGreenNationalityMasterPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbGreenNationality.ClientID%>').value;
                }
                else if (radWin == "RadCountryMasterPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbCityOfResi.ClientID%>').value;
                }
                else if (radWin == "RadClientCodeMasterPopup") {
                    url = '../Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbClientCde.ClientID%>').value;
                }
                else if (radWin == "radAirportPopup") {
                    url = '../Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbHomeBase.ClientID%>').value;
                }
                else if (radWin == "RadFlightPurposeMasterPopup") {
                    url = '../People/FlightPurposePopup.aspx?FlightPurposeCD=' + document.getElementById('<%=tbFlightPurpose.ClientID%>').value;
                }
                else if (radWin == "radCQCPopups") {
                    url = '../../CharterQuote/CharterQuoteCustomerPopup.aspx?Code=' + document.getElementById('<%=tbCQCustomer.ClientID%>').value;
                }
                else if (radWin == "RadDepartmentMasterPopup") {
                    url = '../Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=' + document.getElementById('<%=tbDepartment.ClientID%>').value;
                }
                else if (radWin == "radDepartmentPopup") {
                    url = '../Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=' + document.getElementById('<%=tbDepartment.ClientID%>').value;
                }
                //else if (radWin == "RadAuthorizationMasterPopup") {
                    //url = '../Company/AuthorizationPopup.aspx?AuthorizationCD=' + document.getElementById('<%=tbAuth.ClientID%>').value + '&deptId=' + document.getElementById('<%=hdnDepartment.ClientID%>').value;
                //}
                else if (radWin == "radAuthorizationPopup") {
                    url = '../Company/AuthorizationPopup.aspx?AuthorizationCD=' + document.getElementById('<%=tbAuth.ClientID%>').value + '&deptId=' + document.getElementById('<%=hdnDepartment.ClientID%>').value;
                     }
                else if (radWin == "radAccountMasterPopup") {
                    url = '../Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbAccountNumber.ClientID%>').value;
                }
                else if (radWin == "RadAssociateMasterPopup") {
                    url = '../People/PassengerAssociatePopup.aspx?PassengerRequestorCD=' + document.getElementById('<%=tbAssociated.ClientID%>').value;
                }
                else if (radWin == "RadAddlInfo") {
                    url = '../People/PassengerAddInfoPopup.aspx';
                }
                else if (radWin == "RadSearchClientCodeMasterPopup") {
                    url = '../Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbSearchClientCode.ClientID%>').value;
                }
                else if (radWin == "RadExportData") {
                    url = "../../Reports/ExportReportInformation.aspx?Report=RptDBPassengerRequestorIExport&PassengerCD=" + document.getElementById('<%=tbCode.ClientID%>').value + "&UserCD=UC";
                }
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }
            function ShowAddlInfoPopup(sender, args) {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../People/PassengerAddInfoPopup.aspx", "RadAddlInfo");
                return false;
            }
            function confirmCallBackFn(arg) {
                if (arg == false) {
                    window.location.href = "PassengerCatalog.aspx";
                }
                else {
                    return false;
                }
            }
            function ValidateEmptyAircraftTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0.00";
                }
            }
            function ValidateEmptyTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0.0";
                }
            }
            function ValidateEmptyLatTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0";
                }
            }
            function ValidateEmptyTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "00.0";
                }
            }
            function fncClientCheckDate(sender, args) {
                var DateOfBirth;
                DateOfBirth = new Date(document.getElementById('<%= tbDateOfBirth.ClientID %>').value);
                var Today = new Date();
                if (DateOfBirth >= Today) {
                    args.IsValid = false;
                    return;
                }
                args.IsValid = true;
            }

            function allnumeric(inputtxt, e) {
                var numbers = /^-?[0-9]+$/;
                if (inputtxt.value.match(numbers)) {
                    return true;
                }
                else {
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radalert('Please input valid numeric characters only.', 360, 50, 'Crew Roster Catalog');
                    inputtxt.value = "0";
                    return false;
                }
            }

            function fncResidentSinceYear(sender, args) {
                var ResidentYear;
                ResidentYear = document.getElementById('<%= tbResidentSinceYear.ClientID %>').value;

                var Today = new Date();

                if (ResidentYear > Today.getFullYear()) {
                    document.getElementById('<%= tbResidentSinceYear.ClientID %>').focus();
                    args.IsValid = false;
                    return;
                }
                args.IsValid = true;
            }
            var currentTextBox = null;
            var currentDatePicker = null;
            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html
                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);
                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;
                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker
                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));
                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }
            //this handler is used to set the text of the TextBox to the value of selected from the popup
            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {


                if (currentTextBox != null) {
                    var step = "Date_TextChanged";

                    if (currentTextBox.value != args.get_newValue()) {

                        currentTextBox.value = args.get_newValue();
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                }
            }

            function tbDate_OnKeyDown(sender, event) {
                if (event.keyCode == 9) {
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    return true;
                }
            }

            function ToUpper(ControlName) {
                var Value = ControlName.value;
                ControlName.value = Value.toUpperCase();
            }
        </script>
        <script type="text/javascript">
            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }
            function OnClientCloseNationalityPopup(oWnd, args) {
                var combo = $find("<%= tbNationality.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbNationality.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=tbNationalityName.ClientID%>").innerHTML = arg.CountryName;
                        document.getElementById("<%=hdnNationality.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=cvNationality.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbNationality.ClientID%>").value = "";
                        document.getElementById("<%=tbNationalityName.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnNationality.ClientID%>").value = arg.CountryName;
                        document.getElementById("<%=cvNationality.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseGreenNationalityPopup(oWnd, args) {
                var combo = $find("<%= tbGreenNationality.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbGreenNationality.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=hdnGreenNationality.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=cvGreenNationality.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbGreenNationality.ClientID%>").value = "";
                        document.getElementById("<%=hdnGreenNationality.ClientID%>").value = "";
                        document.getElementById("<%=cvGreenNationality.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }


            function OnClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbCityOfResi.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCityOfResi.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=tbCityOfResiName.ClientID%>").innerHTML = arg.CountryName;
                        document.getElementById("<%=hdnCityOfResi.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=cvCityOfResi.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCityOfResi.ClientID%>").value = "";
                        document.getElementById("<%=tbCityOfResiName.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCityOfResi.ClientID%>").value = "";
                        document.getElementById("<%=cvCityOfResi.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseClientCodePopup(oWnd, args) {
                var combo = $find("<%= tbClientCde.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCde.ClientID%>").value = htmlDecode(arg.ClientCD)
                        document.getElementById("<%=hdnClientCde.ClientID%>").value = arg.ClientID
                        document.getElementById("<%=cvClientCde.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbClientCde.ClientID%>").value = "";
                        document.getElementById("<%=hdnClientCde.ClientID%>").value = "";
                        document.getElementById("<%=cvClientCde.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseHomeBasePopup(oWnd, args) {
                var combo = $find("<%= tbHomeBase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnHomeBase.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseFlightPurposePopup(oWnd, args) {
                var combo = $find("<%= tbFlightPurpose.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbFlightPurpose.ClientID%>").value = arg.FlightPurposeCD;
                        document.getElementById("<%=hdnFlightPurpose.ClientID%>").value = arg.FlightPurposeID;
                        document.getElementById("<%=cvFlightPurpose.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbFlightPurpose.ClientID%>").value = "";
                        document.getElementById("<%=hdnFlightPurpose.ClientID%>").value = "";
                        document.getElementById("<%=cvFlightPurpose.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseCQPopup(oWnd, args) {
                var combo = $find("<%= tbCQCustomer.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCQCustomer.ClientID%>").value = arg.CQCustomerCD;
                        document.getElementById("<%=hdnCQCustomerID.ClientID%>").value = arg.CQCustomerID;
                        document.getElementById("<%=cvCQCustomer.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCQCustomer.ClientID%>").value = "";
                        document.getElementById("<%=hdnCQCustomerID.ClientID%>").value = "";
                        document.getElementById("<%=cvCQCustomer.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }


            function OnClientCloseDepartmentPopup(oWnd, args) {
                var combo = $find("<%= tbDepartment.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDepartment.ClientID%>").value = arg.DepartmentCD;
                        document.getElementById("<%=tbDepartmentDesc.ClientID%>").value = arg.DepartmentName;
                        document.getElementById("<%=tbAuth.ClientID%>").value = "";
                        document.getElementById("<%=tbAuthDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnDepartment.ClientID%>").value = arg.DepartmentID;
                        document.getElementById("<%=hdnAuth.ClientID%>").value = "";
                        document.getElementById("<%=lbcvDepartment.ClientID%>").innerHTML = "";
                        document.getElementById("<%=cvAuth.ClientID%>").innerHTML = "";
                        if (arg.DepartmentCD != null)
                            document.getElementById("<%=lbcvDepartment.ClientID%>").innerHTML = "";
                        var step = "tbDepartment_OnTextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                         if (step) {
                             ajaxManager.ajaxRequest(step);
                         }
                    }
                    else {
                        document.getElementById("<%=tbDepartment.ClientID%>").value = "";
                        document.getElementById("<%=tbDepartmentDesc.ClientID%>").value = "";
                        document.getElementById("<%=tbAuth.ClientID%>").value = "";
                        document.getElementById("<%=tbAuthDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnDepartment.ClientID%>").value = "";
                        document.getElementById("<%=hdnAuth.ClientID%>").value = "";
                        document.getElementById("<%=lbcvDepartment.ClientID%>").innerHTML = "";
                        document.getElementById("<%=cvAuth.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function setAuthorization(deptId) {
                $.ajax({
                    type: "GET",
                    contentType: 'text/html',
                    url: "/Views/Utilities/GetApi.aspx?apiType=fss&method=authorization&_search=false&nd=1408618208051&size=20&page=1&filters=[]&departmentId=" + deptId + "&clientId=&showInactive=false&authorizationCD=&markSelectedRecord=false&orders=",
                    success: function (data) {
                        if (data != "") {
                            verifyReturnedResultForJqgrid(data);
                            var jsonObj=JSON.parse($('<div/>').html(data).text());
                            document.getElementById("<%=tbAuth.ClientID%>").value=jsonObj["results"][0]["AuthorizationCD"];
                            document.getElementById("<%=hdnAuth.ClientID%>").value=jsonObj["results"][0]["AuthorizationID"];
                        }
                    }
                });
            }

            function OnClientCloseAuthorizationPopup(oWnd, args) {
                var combo = $find("<%= tbAuth.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAuth.ClientID%>").value = arg.AuthorizationCD;
                        document.getElementById("<%=tbAuthDesc.ClientID%>").innerHTML = arg.DeptAuthDescription;
                        document.getElementById("<%=hdnAuth.ClientID%>").value = arg.AuthorizationID;
                        document.getElementById("<%=cvAuth.ClientID%>").innerHTML = "";
                        CheckDeptAuthorization();
                    }
                    else {
                        document.getElementById("<%=tbAuth.ClientID%>").value = "";
                        document.getElementById("<%=tbAuthDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnAuth.ClientID%>").value = "";
                        document.getElementById("<%=cvAuth.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseAccountNumberPopup(oWnd, args) {
                var combo = $find("<%= tbAccountNumber.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAccountNumber.ClientID%>").value = arg.AccountNum;
                        document.getElementById("<%=hdnAccountNumber.ClientID%>").value = arg.AccountNum;
                        document.getElementById("<%=cvAccountNumber.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbAccountNumber.ClientID%>").value = "";
                        document.getElementById("<%=hdnAccountNumber.ClientID%>").value = "";
                        document.getElementById("<%=cvAccountNumber.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseAssociatePopup(oWnd, args) {
                var combo = $find("<%= tbAssociated.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAssociated.ClientID%>").value = arg.PassengerRequestorCD;
                        document.getElementById("<%=hdnAssociated.ClientID%>").value = arg.PassengerRequestorID;
                        document.getElementById("<%=cvAssociated.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbAssociated.ClientID%>").value = "";
                        document.getElementById("<%=hdnAssociated.ClientID%>").value = "";
                        document.getElementById("<%=cvAssociated.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseSearchClientCodePopup(oWnd, args) {
                var combo = $find("<%= tbSearchClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbSearchClientCode.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=cvSearchClientCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbSearchClientCode.ClientID%>").value = "";
                        document.getElementById("<%=cvSearchClientCode.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                jQuery("#<%=tbSearchClientCode.ClientID%>").change();
            }
            function OnClientVisaCountryPopup(oWnd, args) {
                var combo = GControlId;
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        GControlId.value = arg.CountryCD;
                        GhdnControlId.value = arg.CountryID;
                    }
                    else {
                        GControlId.value = "";
                        GhdnControlId.value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseExportData(oWnd, args) {
                document.getElementById('<%=btnShowReports.ClientID%>').click();
                return false;
            }
            function OnClientReportPopup(oWnd, args) {
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnReportParameters.ClientID%>").value = arg.Arg1;
                    }
                    else {
                        document.getElementById("<%=hdnReportParameters.ClientID%>").value = "";
                    }
                    if ((arg.Arg1 != null) && (arg.Arg1 != "")) {
                        if (document.getElementById("<%=hdnReportFormat.ClientID%>").value == "EXPORT") {
                            url = "../../Reports/ExportReportInformation.aspx";
                            
                            var oManager = $find("<%= RadWindowManager1.ClientID %>");
                            var oWnd = oManager.open(url, 'RadExportData');
                            return false;
                        }
                        else {
                            document.getElementById('<%=btnShowReports.ClientID%>').click();
                            return false;
                        }
                    }
                }
            }
            function ShowReports(radWin, ReportFormat, ReportName) {
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;

                url = "../People/PassengerRequestorsPopup.aspx?IsUIReports=1";

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            // this function is used to the refresh the currency grid
            function refreshGrid(arg) {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest('Rebind');
            }
        </script>
        <script type="text/javascript">
            function EnableAssociate(TaxType) {
                if (TaxType == 'Guest') {
                    document.getElementById('<%=tbAssociated.ClientID%>').disabled = false;
                    document.getElementById('<%=btnAssociated.ClientID%>').disabled = false;
                }
                else {
                    document.getElementById('<%=tbAssociated.ClientID%>').disabled = true;
                    document.getElementById('<%=btnAssociated.ClientID%>').disabled = true;
                    document.getElementById('<%=tbAssociated.ClientID%>').value = "";
                }
                return true;
            }
            
            var GControlId, GhdnControlId;
            function openWinGrd(radWin, ControlId) {
                if (ControlId.id.indexOf("btn") != -1) {
                    GControlId = document.getElementById(ControlId.id.replace("btn", "tb"));
                }
                else {
                    GControlId = ControlId;
                }
                var ControlCheck = ControlId.id;
                if (ControlCheck.indexOf("Passport") != -1) {
                    GhdnControlId = ControlCheck.replace("tbPassportCountry", "hdnPassportCountry");
                }
                else {
                    GhdnControlId = ControlCheck.replace("tbCountryCD", "hdnCountryID");
                }
                var url = '';
                if (radWin == "RadVisaCountryMasterPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + ControlId.value;
                }
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }
        </script>
        <script type="text/javascript">
            function File_onchange() {
                __doPostBack('__Page', 'LoadImage');
            }
            function CheckName() {
                var nam = document.getElementById('<%=tbImgName.ClientID%>').value;
                if (nam != "") {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = false;
                }
                else {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = true;
                }
            }
            // this function is related to Image 
            function OpenRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
                oWnd.show();
            }
            // this function is related to Image 
            function CloseRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = null;
                oWnd.close();
            }
            function uncheckOther(chk) {
                var status = chk.checked;
                var checkBoxes = $("input[id*='chkChoice']");
                $.each(checkBoxes, function () {
                    $(this).attr('checked', false);
                });
                chk.checked = status;
                document.getElementById('<%=hdnIsPassportChoiceChanged.ClientID%>').value = "Yes";
                document.getElementById('<%=hdnIsPassportChanged.ClientID%>').value = "Yes";
            }
            function CheckPassportChanged(sender, args) {
                var ReturnValue = true;
                document.getElementById('<%=hdnIsPassportChanged.ClientID%>').value = "Yes";
                return ReturnValue;
            }
            function ValidatePassport(sender, args) {
                var ReturnValue = true;
                var grid = $find("<%= dgPassport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var GrdCtrl;
                var PassportNum, IssueCity, IssueDate, ExpiryDT, CountryName;
                var TodayDate = new Date();
                var DateFormat = document.getElementById("<%=hdnDateFormat.ClientID%>").value;
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                if (DateFormat == "") {
                    DateFormat = "MM/dd/yyyy";
                }
                TodayDate = TodayDate.format(DateFormat);
                var ValidatePassportCallBackFn = Function.createDelegate(sender, function (shouldSubmit) {
                    GrdCtrl.focus();
                    return ReturnValue;
                });
                for (var i = 0; i < length; i++) {
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbPassportNum")
                    PassportNum = GrdCtrl.value;
                    if (PassportNum == "") {
                        oManager.radalert('Passport Number should not be Empty in Passport Section', 360, 50, 'Passenger/ Requestor', ValidatePassportCallBackFn); 
                        args.set_cancel(true);
                        ReturnValue = false;
                        break;
                    }
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbPassportCountry")
                    CountryName = GrdCtrl.value;
                    if (CountryName != "") {
                        CountryName = CountryName.replace(/^\s+|\s+$/g, '');
                        CountryName = CountryName.toUpperCase();
                        var PassportNumber;
                        if (CountryName == "US") {

                        }
                    }
                    else {
                        oManager.radalert('Passport Country should not be Empty in Passport Section', 360, 50, 'Passenger/ Requestor', ValidatePassportCallBackFn); 
                        args.set_cancel(true);
                        ReturnValue = false;
                        break;
                    }
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbPassportExpiryDT")
                    ExpiryDT = GrdCtrl.value;
                    if (ExpiryDT == "") {
                        oManager.radalert('Passport Expiry Date should not be Empty in Passport Section', 360, 50, 'Passenger/ Requestor', ValidatePassportCallBackFn); 
                        args.set_cancel(true);
                        ReturnValue = false;
                        break;
                    }
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbIssueCity")
                    IssueCity = GrdCtrl.value;
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbIssueDT")
                    IssueDate = GrdCtrl.value;

                }
                return ReturnValue;
            }
            function ValidateVisa(sender, args) {
                var ReturnValue = true;
                var grid = $find("<%= dgVisa.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var GrdCtrl;
                var CountryCD, VisaNum, IssueDate, ExpiryDT;
                var TodayDate = new Date();
                var DateFormat = document.getElementById("<%=hdnDateFormat.ClientID%>").value;
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                if (DateFormat == "") {
                    DateFormat = "MM/dd/yyyy";
                }
                TodayDate = TodayDate.format(DateFormat);
                var ValidateVisaCallBackFn = Function.createDelegate(sender, function (shouldSubmit) {
                    GrdCtrl.focus();
                    return ReturnValue;
                });
                for (var i = 0; i < length; i++) {
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbCountryCD");
                    CountryCD = GrdCtrl.value;
                    if (CountryCD == "") {
                        oManager.radalert('Visa Country should not be Empty in Visa Section', 360, 50, 'Passenger/ Requestor', ValidateVisaCallBackFn); 
                        args.set_cancel(true);
                        ReturnValue = false;
                        break;
                    }
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbVisaNum");
                    VisaNum = GrdCtrl.value;
                    if (VisaNum == "") {
                        oManager.radalert('Visa Number should not be Empty in Visa Section', 360, 50, 'Passenger/ Requestor', ValidateVisaCallBackFn); 
                        args.set_cancel(true);
                        ReturnValue = false;
                        break;
                    }
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbExpiryDT");
                    ExpiryDT = GrdCtrl.value;
                    if (ExpiryDT == "") {
                        oManager.radalert('Visa Expiry Date should not be Empty in Visa Section', 360, 50, 'Passenger/ Requestor', ValidateVisaCallBackFn); 
                        args.set_cancel(true);
                        ReturnValue = false;
                        break;
                    }
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbIssuePlace");
                    IssuePlace = GrdCtrl.value;
                    //                    if (IssuePlace == "") {
                    //                        oManager.radalert('Visa Issuing City/Authority should not be empty', 360, 50, 'Passenger/ Requestor', ValidateVisaCallBackFn);
                    //                        args.set_cancel(true);
                    //                        ReturnValue = false;
                    //                        break;
                    //                    }
                    GrdCtrl = MasterTable.get_dataItems()[i].findElement("tbIssueDate");
                    IssueDate = GrdCtrl.value;
                    if (IssueDate == "") {
                        oManager.radalert('Visa Issue Date should not be Empty in Visa Section', 360, 50, 'Passenger/ Requestor', ValidateVisaCallBackFn); 
                        args.set_cancel(true);
                        ReturnValue = false;
                        break;
                    }
                    if ((IssueDate != "") && (TodayDate != "")) {
                        if (Date.parse(IssueDate) > Date.parse(TodayDate)) {
                            oManager.radalert('Visa Issue Date should not be Future Date in Visa Section.', 360, 50, 'Passenger/ Requestor', ValidateVisaCallBackFn); 
                            args.set_cancel(true);
                            ReturnValue = false;
                            break;
                        }
                    }
                    if ((IssueDate != "") && (ExpiryDT != "")) {
                        if (Date.parse(IssueDate) > Date.parse(ExpiryDT)) {
                            oManager.radalert('Visa Issue Date should be before Expiry Date in Visa Section', 360, 50, 'Passenger/ Requestor', ValidateVisaCallBackFn); 
                            args.set_cancel(true);
                            ReturnValue = false;
                            break;
                        }
                    }
                }
                return ReturnValue;
            }
            function CheckPassportChoice(sender, args) {
                var ReturnValue = false;
                ReturnValue = ValidatePassport(sender, args);
                if (ReturnValue == true) {
                    ReturnValue = ValidateVisa(sender, args);
                    if (ReturnValue == false) {
                        return ReturnValue;
                    }
                }
                else {
                    return ReturnValue;
                }
                var grid = $find("<%=dgPassport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var IsPassportChoice = false;
                var PassportNum;
                var chkChoice;
                var Msg = "";
                for (var i = 0; i < length; i++) {
                    PassportNum = MasterTable.get_dataItems()[0].findElement("tbPassportNum").value;
                    chkChoice = MasterTable.get_dataItems()[i].findElement("chkChoice");
                    if (chkChoice.checked == true) {
                        IsPassportChoice = true;
                        PassportNum = MasterTable.get_dataItems()[i].findElement("tbPassportNum").value;
                        break;
                    }
                }
                if ((document.getElementById('<%=hdnIsPassportChanged.ClientID%>').value == "Yes") && (document.getElementById('<%=hdnSave.ClientID%>').value == "Update")) {
                    if (length > 0 && document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "" && document.getElementById('<%=hdnIsPassportChoiceChanged.ClientID%>').value == "Yes") {
                        if (IsPassportChoice == true) {
                            Msg = "The choice passport for this passenger has changed. <br>Would you like to update future tripsheet passenger information with the new passport number [" + PassportNum + "]?";
                        }
                        else {
                            Msg = "There is no choice passport for this Passenger. <br>Would you like to update future tripsheet Passenger information with First Available passport number [" + PassportNum + "]?";
                        }
                    }
                    else if (length > 0 && document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "1" && document.getElementById('<%=hdnIsPassportChoiceChanged.ClientID%>').value == "") {
                        if (IsPassportChoice == true) {
                            Msg = "Passport [" + document.getElementById('<%=hdnIsPassportChoiceDeleted.ClientID%>').value + "] has been deleted for this Passenger. <br>Would you like to update future tripsheet passenger information with the new passport number [" + PassportNum + "]?";
                        }
                        else {
                            Msg = "Passport [" + document.getElementById('<%=hdnIsPassportChoiceDeleted.ClientID%>').value + "] has been deleted for this Passenger. <br>Would you like to update future tripsheet Passenger information with First Available passport number [" + PassportNum + "]?";
                        }
                    }
                    else if (length == 0 && document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "1" && document.getElementById('<%=hdnIsPassportChoiceChanged.ClientID%>').value == "") {
                        Msg = "There is no passport for this Passenger. <br>Would you like to update future tripsheet Passenger information by Deleting All Passport Information?";
                    }
                    else if (length == 0 && document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "") {
                        Msg = "There is no passport for this Passenger. <br>Would you like to update future tripsheet Passenger information by Deleting All Passport Information?";
                    }
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            document.getElementById('<%=hdnIsPassportChoice.ClientID%>').value = "Yes";
                        }
                        else {
                            document.getElementById('<%=hdnIsPassportChoice.ClientID%>').value = "No";
                        }
                        this.click();
                    });
                    if (Msg != "") {
                        
                        var oManager = $find("<%= RadWindowManager1.ClientID %>");
                        oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Passengers/Requestors");
                        args.set_cancel(true);
                    }
                    else {
                        callBackFunction(sender);
                        args.set_cancel(true);
                    }
                }
                return ReturnValue;
            }
            function CheckPassportChoiceDelete() {
                var grid = $find("<%=dgPassport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var IsPassportChoice = false;
                var PassportNum;
                var chkChoice;
                for (var i = 0; i < length; i++) {
                    if (MasterTable.get_dataItems()[i].get_selected() == true) {
                        PassportNum = MasterTable.get_dataItems()[0].findElement("tbPassportNum").value;
                        chkChoice = MasterTable.get_dataItems()[i].findElement("chkChoice");
                        if (chkChoice.checked == true) {
                            IsPassportChoice = true;
                            PassportNum = MasterTable.get_dataItems()[i].findElement("tbPassportNum").value;
                            break;
                        }
                    }
                }
                if ((IsPassportChoice == true) && (length != 0)) {
                    if (document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "") {
                        document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value = "1";
                        document.getElementById('<%=hdnIsPassportChoiceDeleted.ClientID%>').value = PassportNum;
                    }
                }
                else if ((IsPassportChoice == false) && (length != 0)) {
                    if (document.getElementById('<%=hdnIsPassportChanged.ClientID%>').value == "Yes") {
                        document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value = "";
                        document.getElementById('<%=hdnIsPassportChoiceDeleted.ClientID%>').value = PassportNum;
                    }
                }
                return true;
            }
            function CheckPassportDuplicate(txtPassport, args) {
                var ReturnValue = false;
                var PassportNumber = txtPassport.value;
                var grid = $find("<%=dgPassport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var Country, CountryName;
                var OthPassportID, OthPassportNum, OthCountry, OthCountryName;
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var callBackFunction = Function.createDelegate(txtPassport, function (shouldSubmit) {
                    txtPassport.value = "";
                    return ReturnValue;
                });
                for (var i = 0; i < length; i++) {
                    OthPassportID = MasterTable.get_dataItems()[i].findElement("tbPassportNum");
                    if (txtPassport.id == OthPassportID.id) {
                        Country = MasterTable.get_dataItems()[i].findElement("tbPassportCountry");
                        CountryName = MasterTable.get_dataItems()[i].findElement("tbPassportCountry").value;
                        CountryName = CountryName.replace(/^\s+|\s+$/g, '');
                        CountryName = CountryName.toUpperCase();
                        break;
                    }
                }
                for (var i = 0; i < length; i++) {
                    OthPassportID = MasterTable.get_dataItems()[i].findElement("tbPassportNum");
                    OthPassportNum = MasterTable.get_dataItems()[i].findElement("tbPassportNum").value;
                    OthCountry = MasterTable.get_dataItems()[i].findElement("tbPassportCountry");
                    OthCountryName = MasterTable.get_dataItems()[i].findElement("tbPassportCountry").value;
                    OthCountryName = OthCountryName.replace(/^\s+|\s+$/g, '');
                    OthCountryName = OthCountryName.toUpperCase();
                    if (PassportNumber == "") {
                        oManager.radalert("Passport Number should not be empty.", 400, 100, "Passengers/Requestors", callBackFunction);
                        return ReturnValue;
                    }
                    if (txtPassport.id != OthPassportID.id) {
                        //                        if ((PassportNumber == OthPassportNum) && (CountryName == OthCountryName)) {
                        //                            oManager.radalert("Country Code / Passport Number must be unique - Enter valid Country Code and Passport Number.", 400, 100, "Passengers/Requestors", callBackFunction);
                        //                            return ReturnValue;
                        //                        }
                        if (PassportNumber == OthPassportNum) {
                            oManager.radalert("Passport Number Must Be Unique.", 400, 100, "Passengers/Requestors", callBackFunction);
                            return ReturnValue;
                        }
                    }
                }
                return ReturnValue;
            }
            function CheckVisaDuplicate(txtVisa, args) {
                var ReturnValue = false;
                var VisaNumber = txtVisa.value;
                var grid = $find("<%=dgVisa.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var Country, CountryName;
                var OthVisaID, OthVisaNum, OthCountryID, OthCountryName;
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var callBackFunction = Function.createDelegate(txtVisa, function (shouldSubmit) {
                    txtVisa.value = "";
                    return ReturnValue;
                });
                for (var i = 0; i < length; i++) {
                    OthVisaID = MasterTable.get_dataItems()[i].findElement("tbVisaNum");
                    if (txtVisa.id == OthVisaID.id) {
                        Country = MasterTable.get_dataItems()[i].findElement("tbCountryCD");
                        CountryName = MasterTable.get_dataItems()[i].findElement("tbCountryCD").value;
                        CountryName = CountryName.replace(/^\s+|\s+$/g, '');
                        CountryName = CountryName.toUpperCase();
                        break;
                    }
                }
                for (var i = 0; i < length; i++) {
                    OthVisaID = MasterTable.get_dataItems()[i].findElement("tbVisaNum");
                    OthVisaNum = MasterTable.get_dataItems()[i].findElement("tbVisaNum").value;
                    OthCountryID = MasterTable.get_dataItems()[i].findElement("tbCountryCD");
                    OthCountryName = MasterTable.get_dataItems()[i].findElement("tbCountryCD").value;
                    OthCountryName = OthCountryName.replace(/^\s+|\s+$/g, '');
                    OthCountryName = OthCountryName.toUpperCase();
                    if (VisaNumber == "") {
                        oManager.radalert("Visa Number should not be empty.", 400, 100, "Passengers/Requestors", callBackFunction);
                        return ReturnValue;
                    }
                    if (txtVisa.id != OthVisaID.id) {
                        //if ((VisaNumber == OthVisaNum) && (CountryName == OthCountryName)) {
                        //  oManager.radalert("Country Code / Visa Number must be unique - Enter valid Country Code and Visa Number.", 400, 100, "Passengers/Requestors", callBackFunction);
                        //  return ReturnValue;
                        //}
                        if (VisaNumber == OthVisaNum) {
                            oManager.radalert("Visa Number Must Be Unique.", 400, 100, "Passengers/Requestors", callBackFunction);
                            return ReturnValue;
                        }
                    }
                }
                return ReturnValue;
            }
            function tbDepartment_onchange() {
                if (document.getElementById('<%=tbDepartment.ClientID%>').value == "") {
                    document.getElementById('<%=tbAuth.ClientID%>').value = "";
                }
            }
            function SetNextFocus(control) {
                var controlid = control.id;
                if (controlid.indexOf('tbPassportExpiryDT') != -1) {
                    controlid = controlid.replace('tbPassportExpiryDT', 'tbIssueCity');
                }
                else if (controlid.indexOf('tbIssueDT') != -1) {
                    controlid = controlid.replace('tbIssueDT', 'tbPassportCountry');
                }
                else if (controlid.indexOf('tbExpiryDT') != -1) {
                    controlid = controlid.replace('tbExpiryDT', 'tbIssuePlace');
                }
                else if (controlid.indexOf('tbIssueDate') != -1) {
                    controlid = controlid.replace('tbIssueDate', 'tbNotes');
                }
                document.getElementById(controlid).focus();
            }
            function ValidateDate(control) {
                var MinDate = new Date("01/01/1900");
                var MaxDate = new Date("12/31/2100");
                var SelectedDate;
                if (control.value != "") {
                    SelectedDate = new Date(control.value);
                    if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                        
                        var oManager = $find("<%= RadWindowManager1.ClientID %>");
                        oManager.radalert("Please enter or select Date between 01/01/1900 and 12/31/2100", 400, 100, "Passengers/Requestors");
                        control.value = "";
                    }
                }
                return false;
            }
            function CheckDeptAuthorization() {
                if (document.getElementById('<%=tbDepartment.ClientID%>').value == "") {
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radalert("Please enter the Department Code before entering the Authorization Code.", 400, 100, "Passengers/Requestors");
                    document.getElementById('<%=tbAuth.ClientID%>').value = "";
                    document.getElementById('<%=tbDepartment.ClientID%>').focus();
                    return false;
                }
            }
            function fnValidateAuth() {
                if (document.getElementById("<%=hdnDepartment.ClientID%>").value == "") {
                    radalert("Please enter the Department Code before entering the Authorization Code.", 330, 100, "Passengers/Requestors", "");
                    document.getElementById("<%=hdnDepartment.ClientID%>").focus();
                }
                else {
                    openWin('radAuthorizationPopup');
                }
            }
            function printImage() {
                var image = document.getElementById('<%=ImgPopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlImg = document.getElementById('<%=ddlImg.ClientID%>');
                if (ddlImg.length > 0) {
                    var ImageName = ddlImg.options[ddlImg.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Passengers/Requestors");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }

        </script>
        <script type="text/javascript">
            function DeleteInfoConfirm(sender, args) {
                var ReturnValue = false;
                var grid = $find("<%=dgPassengerAddlInfo.ClientID %>").get_masterTableView();
                var length = grid.get_selectedItems().length;
                if (length != 0) {
                    var Msg = "Are you sure you want to delete this ADDITIONAL INFO record?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            this.click();
                        }
                    });
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Passengers/Requestors");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }
            function DeletePassportConfirm(sender, args) {
                var ReturnValue = false;
                CheckPassportChanged();
                var grid = $find("<%=dgPassport.ClientID %>").get_masterTableView();
                var length = grid.get_selectedItems().length;
                if (length != 0) {
                    var Msg = "Are you sure you want to delete this PASSPORT record?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            CheckPassportChoiceDelete();
                            this.click();
                        }
                    });
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Passengers/Requestors");
                    args.set_cancel(true);
                }
                ReturnValue = false;
                return ReturnValue;
            }
            function DeleteVisaConfirm(sender, args) {
                var ReturnValue = false;
                var grid = $find("<%=dgVisa.ClientID %>").get_masterTableView();
                var length = grid.get_selectedItems().length;
                if (length != 0) {
                    var Msg = "Are you sure you want to delete this VISA record?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            this.click();
                        }
                    });
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Passengers/Requestors");
                    args.set_cancel(true);
                }
                ReturnValue = false;
                return ReturnValue;
            }


        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            //To display plane image at right place
            var isMainInformationExpanded = true;
            var isAttachmentsExpanded = true;
            var isAdditionalInformationExpanded = true;
            var isPassengerChecklistExpanded = true;
            var isPassportExpanded = true;
            var isVisaExpanded = true;
            var isNotesExpanded = false;
            var isPaxAlertsExpanded = false;

            function pnlMainExpend() {
                isMainInformationExpanded = true;
            }
            function pnlMainCollapse() {
                isMainInformationExpanded = false;
            }
            function pnlImageExpend() {
                isAttachmentsExpanded = true;
            }
            function pnlImageCollapse() {
                isAttachmentsExpanded = false;
            }
            function pnlAdditionalInfoExpend() {
                isAdditionalInformationExpanded = true;
            }
            function pnlAdditionalInfoCollapse() {
                isAdditionalInformationExpanded = false;
            }
            function pnlChecklistExpend() {
                isPassengerChecklistExpanded = true;
            }
            function pnlChecklistCollapse() {
                isPassengerChecklistExpanded = false;
            }
            function pnlPassportExpend() {
                isPassportExpanded = true;
            }
            function pnlPassportCollapse() {
                isPassportExpanded = false;
            }
            function pnlVisaExpend() {
                isVisaExpanded = true;
            }
            function pnlVisaCollapse() {
                isVisaExpanded = false;
            }
            function pnlNotesExpend() {
                isNotesExpanded = true;
            }
            function pnlNotesCollapse() {
                isNotesExpanded = false;
            }
            function pnlPaxItemsExpend() {
                isPaxAlertsExpanded = true;
            }
            function pnlPaxItemsCollapse() {
                isPaxAlertsExpanded = false;
            }

            $(document).ready(function showPlaneImage() {
                if ($.trim(decodeURI(getQuerystring("PaxName", ""))) != null && $.trim(decodeURI(getQuerystring("PaxName", ""))) != undefined && $.trim(decodeURI(getQuerystring("PaxName", ""))) != "")
                {
                    $(document).prop('title', 'Pax Passport Visa' + " - " + $.trim(decodeURI(getQuerystring("PaxName", ""))));
                }
               
                //top checkboxes
                $('#ctl00_ctl00_MainContent_SettingBodyContent_chkSearchActiveOnly,' +
                    '#ctl00_ctl00_MainContent_SettingBodyContent_chkSearchHomebaseOnly,' +
                    '#ctl00_ctl00_MainContent_SettingBodyContent_chkSearchRequestorOnly').on('change', setPlaneImage);

                $('#ctl00_ctl00_MainContent_SettingBodyContent_dgPassenger_ctl00_ctl03_ctl01_lnkInitEdit,' +
                    '#ctl00_ctl00_MainContent_SettingBodyContent_dgPassenger_ctl00_ctl03_ctl01_lnkInitInsert,' +
                    '#ctl00_ctl00_MainContent_SettingBodyContent_dgPassenger_ctl00_ctl03_ctl01_lnkDelete,' +
                    '#ctl00_ctl00_MainContent_SettingBodyContent_btnSaveChangesTop_input,' +
                    '#ctl00_ctl00_MainContent_SettingBodyContent_btnCancelTop,' +
                    '#ctl00_ctl00_MainContent_SettingBodyContent_btnEdit,' +
                    '#ctl00_ctl00_MainContent_SettingBodyContent_btnSaveChanges_input,' +
                    '#ctl00_ctl00_MainContent_SettingBodyContent_btnCancel').on('click', setPlaneImage);

                // top grid row
                $('.rgRow').on('click', function () {
                    var topMargin = $(window).scrollTop() - 55;
                    $('div.raDiv').attr('style', 'margin-top: ' + topMargin + 'px !important');
                });

                // top check boxes
                $('#ctl00_ctl00_MainContent_SettingBodyContent_pnlMain_i0_i0_radType,' +
                   '#ctl00_ctl00_MainContent_SettingBodyContent_pnlMain_i0_i0_radNonControlled,' +
                   '#ctl00_ctl00_MainContent_SettingBodyContent_pnlMain_i0_i0_radGuest').on('change', setPlaneImage);

                $('#btndeleteImage_input').on('click', setPlaneImage);

                //Add Info and Delete Info Button in addtional information panel
                $('#ctl00_ctl00_MainContent_SettingBodyContent_pnlAdditionalInfo_i0_i0_btnDeleteInfo_input').on('click', setPlaneImage);

                //Check Box in Passanger Checklist
                $('#ctl00_ctl00_MainContent_SettingBodyContent_pnlChecklist_i0_i0_chkDisplayInactiveChecklist').on('change', setPlaneImage);

                $('#ctl00_ctl00_MainContent_SettingBodyContent_pnlChecklist_i0_i0_btnAddChecklist_input,' +
                  '#ctl00_ctl00_MainContent_SettingBodyContent_pnlChecklist_i0_i0_btnDeleteChecklist_input,' +
                  '#ctl00_ctl00_MainContent_SettingBodyContent_pnlPassport_i0_i0_btnAddPassport_input,' +
                  '#ctl00_ctl00_MainContent_SettingBodyContent_pnlPassport_i0_i0_btnDeletePassport_input' +
                  '#ctl00_ctl00_MainContent_SettingBodyContent_pnlVisa_i0_i0_btnAddVisa_input,' +
                  '#ctl00_ctl00_MainContent_SettingBodyContent_pnlVisa_i0_i0_btnDeleteVisa_input').on('click', setPlaneImage);
                if (document.getElementById('<%=hdnPaxPassport.ClientID%>').value == "Yes") {
                    window.OnClientClick('Passport');
                } else if (document.getElementById('<%=hdnPaxPassport.ClientID%>').value == "No") {
                    window.OnClientClick('Visa');
                }
                
            });

            function setPlaneImage() {

                var scrollTop = $(window).scrollTop();
                var topMargin = 0;
                
                if (isNotesExpanded) {
                    topMargin -= 60;
                }
                if (isAttachmentsExpanded) {
                    topMargin -= 40;
                }
                if (isMainInformationExpanded) {
                    topMargin -= 530;
                    if ($.browser.webkit) {
                        topMargin -= 90;
                    }
                }
                if (isAdditionalInformationExpanded) {
                    topMargin -= 140;
                }
                if (isPassengerChecklistExpanded) {
                    topMargin -= 320;
                }
                if (isPassportExpanded) {
                    topMargin -= 80;
                    if ($.browser.webkit) {
                        topMargin -= 10;
                    }
                }
                if (isVisaExpanded) {
                    topMargin -= 75;
                    if ($.browser.webkit) {
                        topMargin -= 10;
                    }
                }
                if (isPaxAlertsExpanded) {
                    topMargin -= 60;
                }

                if ($.browser.webkit) {
                    topMargin += 30;
                }

                topMargin += scrollTop - 210; //-210 is value when all are collapsed
                $('div.raDiv').attr('style', 'margin-top: ' + topMargin + 'px !important');
            }

            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && redirectUrl.toLowerCase().indexOf("javascript") !== 0 && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

	    </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="divExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" MinDate="01/01/1900"
        MaxDate="12/31/2100" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadNationalityMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseNationalityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadGreenNationalityMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseGreenNationalityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadClientCodeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseClientCodePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseHomeBasePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCountryMasterPopup1" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Select Multiple Pax" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerNamePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadFlightPurposeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFlightPurposePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/FlightPurposePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCrewCheckListPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadChecklistPopUp" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Title="Select Checklist Settings & Pax Members"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerchecklistSettings.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCQCPopups" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCQPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/CharterQuoteCustomerPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCQCPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radDepartmentPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseDepartmentPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radDepartmentCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAuthorizationPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAuthorizationCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAccountNumberPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadAssociateMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAssociatePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerAssociatePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadAddlInfo" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadSearchClientCodeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseSearchClientCodePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExportData" Title="Export Report Information" KeepInScreenBounds="true"
                AutoSize="false" Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadVisaCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientVisaCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printImage();return false;" /></div>
                                    <asp:Image ID="ImgPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientReportPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Passengers/Requestors</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('radPaxInfoPopup','PDF','RptDBPassengerRequestorI');return false;"
                            class="search-icon" title="Preview Report" />
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" OnClientClick="javascript:ShowReports('RadPassengerReport','EXPORT','RptDBPassengerRequestorIExport');return false;"
                            class="save-icon" title="Export Report" />
                        <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                            Style="display: none;" />
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                        <a href="../../Help/ViewHelp.aspx?Screen=PassengerRequestorHelp" target="_blank"
                            class="help-icon" title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="head-sub-menu">
        <tr>
            <td>
                <div class="tags_select">
                    <%--<a href="#">Additional Info</a>|<a href="#">Passport</a>|--%><asp:LinkButton
                        ID="lbtnTravelSense" runat="server" Text="Travel$ense" OnClick="lbtnTravelSense_Click" /></div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlSearchPanel" runat="server" Visible="true">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_5">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td class="tdLabel100">
                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                            AutoPostBack="true" />
                    </td>
                    <td class="tdLabel120">
                        <asp:CheckBox ID="chkSearchHomebaseOnly" runat="server" Text="Home Base Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                            AutoPostBack="true" />
                    </td>
                    <td class="tdLabel120">
                        <asp:CheckBox ID="chkSearchRequestorOnly" runat="server" Text="Requestor Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                            AutoPostBack="true" />
                    </td>
                    <td class="tdLabel80">
                        <asp:Label ID="lbSearchClientCode" runat="server" Text="Client Code:" />
                    </td>
                    <td class="text120">
                        <asp:TextBox ID="tbSearchClientCode" runat="server" CssClass="text80" MaxLength="5"
                            OnTextChanged="FilterByClient_OnTextChanged" AutoPostBack="true" />
                        <asp:Button ID="btnSearchClientCode" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadSearchClientCodeMasterPopup');return false;" />
                    </td>
                    <td>
                        <asp:CustomValidator ID="cvSearchClientCode" runat="server" ControlToValidate="tbSearchClientCode"
                            ErrorMessage="Invalid Client Code." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                    </td>
                    <td class="text80" style="display: none;">
                        <asp:Button ID="btnFilterClientCode" runat="server" CssClass="ui_nav" Text="Search"
                            OnClick="FilterByClient_OnTextChanged" />
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_5">
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <telerik:RadGrid ID="dgPassenger" runat="server" AllowSorting="true" OnItemCreated="dgPassenger_ItemCreated"
            OnNeedDataSource="dgPassenger_BindData" OnItemCommand="dgPassenger_ItemCommand"
            OnUpdateCommand="dgPassenger_UpdateCommand" OnInsertCommand="dgPassenger_InsertCommand"
            OnDeleteCommand="dgPassenger_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
            AllowPaging="true" OnSelectedIndexChanged="dgPassenger_SelectedIndexChanged"
            OnPreRender="dgPassenger_PreRender" OnPageIndexChanged="dgPassenger_PageIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="341px">
            <MasterTableView DataKeyNames="PaxTitle,PassengerRequestorID,PassengerRequestorCD,PassengerName,CustomerID,DepartmentID,PassengerDescription,PhoneNum,IsEmployeeType,
        StandardBilling,AuthorizationID,AuthorizationDescription,Notes,LastName,FirstName,MiddleInitial,DateOfBirth,ClientID,AssociatedWithCD,HomebaseID,IsActive,
        FlightPurposeID,SSN,Title,SalaryLevel,IsPassengerType,LastUpdUID,LastUpdTS,IsScheduledServiceCoord,CompanyName,EmployeeID,FaxNum,EmailAddress,PersonalIDNum,
        IsSpouseDependant,AccountID,CountryID,IsSIFL,PassengerAlert,Gender,AdditionalPhoneNum,IsRequestor,CountryOfResidenceID,PaxScanDoc,TSAStatus,TSADTTM,Addr1,Addr2,
        City,StateName,PostalZipCD,IsDeleted, NationalityName,NationalityCD,ClientCD,ClientDescription,AccountNum,AccountDescription,DepartmentCD,DepartmentName,
        AuthorizationCD,DeptAuthDescription,FlightPurposeCD,FlightPurposeDescription,ResidenceCountryName,ResidenceCountryCD,HomeBaseName,HomeBaseCD,Addr3,OtherEmail,PersonalEmail,OtherPhone,CellPhoneNum2,BusinessFax,PrimaryMobile"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="PAX Code" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="60px" FilterControlWidth="40px"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PaxTitle" HeaderText="Title" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="60px" FilterControlWidth="40px"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PassengerName" HeaderText="Passengers/Requestors" FilterDelay="500"
                        CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                        HeaderStyle-Width="160px" FilterControlWidth="140px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="Department Code" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="80px" FilterControlWidth="60px"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DepartmentName" HeaderText="Department Desc." FilterDelay="500"
                        CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                        HeaderStyle-Width="100px" FilterControlWidth="80px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AdditionalPhoneNum" HeaderText="Business Phone" FilterDelay="500"
                        CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                        HeaderStyle-Width="100px" FilterControlWidth="80px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EmailAddress" HeaderText="Business E-mail" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="100px" FilterDelay="500"
                        FilterControlWidth="80px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="60px" FilterControlWidth="40px"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="Active" AllowFiltering="false"
                        HeaderStyle-Width="50px">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridCheckBoxColumn DataField="IsRequestor" HeaderText="Req" AllowFiltering="false"
                        HeaderStyle-Width="40px">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridBoundColumn DataField="PassengerRequestorID" HeaderText="PassengerRequestorID"
                        Display="false" CurrentFilterFunction="EqualTo" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CustomerID" HeaderText="CustomerID" Display="false" FilterDelay="500"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div class="grid_icon">
                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                            CommandName="InitInsert" Visible='<%# IsAuthorized(Permission.Database.AddPassengerRequestor)%>'></asp:LinkButton>
                        <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            Visible='<%# IsAuthorized(Permission.Database.EditPassengerRequestor)%>' ToolTip="Edit"
                            CssClass="edit-icon-grid" CommandName="Edit"></asp:LinkButton>
                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                            Visible='<%# IsAuthorized(Permission.Database.DeletePassengerRequestor)%>' runat="server"
                            CommandName="DeleteSelected" ToolTip="Delete" CssClass="delete-icon-grid"></asp:LinkButton>
                    </div>
                    <div>
                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbIsEnableTSAPX" Font-Bold="true" runat="server" Text=""></asp:Label>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0" class="tblButtonArea">
                            <tr>
                                <td>
                                    <asp:Button ID="btnImage" runat="server" Text="Attachments" CssClass="ui_nav" OnClientClick="javascript:OnClientClick('Image'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnAddlInform" runat="server" Text="Additional Information" CssClass="ui_nav"
                                        OnClientClick="javascript:OnClientClick('AddInfo'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnPassport" runat="server" Text="Passport" CssClass="ui_nav" OnClientClick="javascript:OnClientClick('Passport'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnVisa" runat="server" Text="Visa" CssClass="ui_nav" OnClientClick="javascript:OnClientClick('Visa'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnNotes" runat="server" Text="Notes" CssClass="ui_nav" OnClientClick="javascript:OnClientClick('Notes'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnPaxAlerts" runat="server" Text="PAX Alerts" CssClass="ui_nav"
                                        OnClientClick="javascript:OnClientClick('Pax Alerts'); return false;" />
                                </td>
                                <td class="custom_radbutton">
                                    <telerik:RadButton ID="btnSaveChangesTop" runat="server" Text="Save" ValidationGroup="Save"
                                        OnClick="btnSaveChangesTop_Click" OnClientClicking="CheckPassportChoice" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" Text="Cancel" CssClass="button" OnClick="Cancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlMain" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                runat="server" OnClientItemExpand="pnlMainExpend" OnClientItemCollapse="pnlMainCollapse">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information">
                        <Items>
                            <telerik:RadPanelItem Value="Maintenance" runat="server">
                                <ContentTemplate>
                                    <table width="100%" class="box1">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel100">
                                                            <asp:CheckBox ID="chkRequestor" runat="server" Text="Requestor" />
                                                        </td>
                                                        <td class="tdLabel100">
                                                            <asp:CheckBox ID="chkActive1" runat="server" Text="Active" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkSchdServCoord" runat="server" Text="Sched Serv Coord." Visible="false"
                                                                Checked="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150">
                                                            <span class="mnd_text">PAX Code</span>
                                                        </td>
                                                        <td class="tdLabel150">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCode" runat="server" MaxLength="5" CssClass="text50" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                            onBlur="return RemoveSpecialChars(this),CheckTxtBox('code');" AutoPostBack="true" OnTextChanged="tbCode_TextChanged"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnCode" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%--<asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="PAX Code is Required."
                                                                ControlToValidate="tbCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                                                                        <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="PAX Code already exists."
                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel80">
                                                            <span>Title</span>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPaxTitle" runat="server"  CssClass="text341"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <span>
                                                                <asp:Label ID="lbFirstName" Text="First Name" runat="server"></asp:Label>
                                                            </span>
                                                        </td>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:TextBox ID="tbFirstName" CssClass="text120" runat="server" MaxLength="20"></asp:TextBox>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ErrorMessage="PAX First Name is Required."
                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbFirstName" ValidationGroup="Save">
                                                                        </asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="revFirstName"  runat="server" ErrorMessage="Enter alphanumeric, character like @'&#. and space" 
                                                                             Display="Dynamic" CssClass="alert-text" ControlToValidate="tbFirstName" ValidationGroup="Save" ValidationExpression="^[a-zA-Z0-9'@&#.\s]{1,20}$"  />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel80" valign="top">
                                                            <asp:Label ID="lbMiddleName" Text="Middle Name" runat="server"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:TextBox ID="tbMiddleName" CssClass="text120" runat="server" MaxLength="20"></asp:TextBox>
                                                             <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="revMiddleName"  runat="server" ErrorMessage="Enter alphanumeric, character like @'&#. and space" 
                                                                             Display="Dynamic" CssClass="alert-text" ControlToValidate="tbMiddleName" ValidationGroup="Save" ValidationExpression="^[a-zA-Z0-9'@&#.\s]{0,20}$"  />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel70" valign="top">
                                                            <span>
                                                                <asp:Label ID="lbLastName" Text="Last Name" runat="server"></asp:Label></span>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbLastName" CssClass="text120" OnTextChanged="LastName_TextChanged"
                                                                AutoPostBack="true" runat="server" MaxLength="20"></asp:TextBox>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ErrorMessage="PAX Last Name is Required."
                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbLastName" ValidationGroup="Save">
                                                                        </asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="revLastName" runat="server" ErrorMessage="Enter alphanumeric, character like @'&#. and space" 
                                                                             Display="Dynamic" CssClass="alert-text" ControlToValidate="tbLastName" ValidationGroup="Save"  ValidationExpression="^[a-zA-Z0-9'@&#.\s]{1,20}$"  />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel140">
                                                            <asp:Button ID="btnCountry" Text="Show Dupes" runat="server" OnClientClick="javascript:openDuplicates();return false;"
                                                                CssClass="button"></asp:Button>
                                                        </td>
                                                        <td class="tdLabel400">
                                                            <asp:Label ID="lbWarningMessage" Visible="false" Text="Duplicate Passenger Record May Exist."
                                                                runat="server" CssClass="alert-text"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150">
                                                            <span>Nickname</span>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbNickname" runat="server" MaxLength="50" CssClass="text150" Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_5">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>
                                                                    <asp:Label ID="lbContactInfo" runat="server" Text="Contact Information" ForeColor="Black"></asp:Label></legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Company
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbCompany" CssClass="text200" runat="server" MaxLength="60"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Employee ID
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbEmpId" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Business Phone
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbAddlPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                            onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Home Phone
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPaxPhone" CssClass="text200" runat="server" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                            onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Primary Mobile/Cell
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbPrimaryMobile" CssClass="text200" runat="server" MaxLength="25"
                                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Secondary Mobile/Cell
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSecondaryMobile" runat="server" CssClass="text200" MaxLength="25"
                                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Business Fax
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbBusinessFax" CssClass="text200" runat="server" MaxLength="25"
                                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Other Phone
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                            onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Business E-mail
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Home Fax
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPaxFax" CssClass="text200" runat="server" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                            onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                    </td>
                                                                                    <td class="tdLabel140">
                                                                                        <asp:RegularExpressionValidator ID="regEmail" runat="server" ErrorMessage="Invalid E-mail format"
                                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbEmail" ValidationGroup="Save"
                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Personal E-mail
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbPersonalEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Other E-mail
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                                                    </td>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:RegularExpressionValidator ID="revPersonalEmail" runat="server" ControlToValidate="tbPersonalEmail"
                                                                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revOtherEmail" runat="server" ControlToValidate="tbOtherEmail"
                                                                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        <asp:Label ID="lbCountryOfResidence" Text="Country Of Residence" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbCityOfResi" CssClass="text170" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                            OnTextChanged="tbCityOfResi_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnCityOfResi" runat="server" />
                                                                                        <asp:Button ID="btnCityOfResi" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadCountryMasterPopup');return false;" />
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        <asp:Label ID="lbNationality" Text="Nationality" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbNationality" CssClass="text170" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                            OnTextChanged="tbNationality_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnNationality" runat="server" />
                                                                                        <asp:Button ID="BtnNationality" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadNationalityMasterPopup');return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:CustomValidator ID="cvCityOfResi" runat="server" ControlToValidate="tbCityOfResi"
                                                                                            ErrorMessage="Invalid Country Code." Display="Dynamic" CssClass="alert-text"
                                                                                            ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                        <asp:Label ID="tbCityOfResiName" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvNationality" runat="server" ControlToValidate="tbNationality"
                                                                                            ErrorMessage="Invalid Nationality." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                        <asp:Label ID="tbNationalityName" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        <asp:Label ID="lbAddressLine1" Text="Address Line 1" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbAddress1" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Address Line 2
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbAddress2" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Address Line 3
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbAddress3" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        <asp:Label ID="lbCity" Text="City" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbCity" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        <asp:Label ID="lbStateProvince" Text="State/Province" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbState" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        <asp:Label ID="lbPostal" Text="Postal" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbPostal" CssClass="text200" runat="server" MaxLength="15"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        PIN No.
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPinNumber" CssClass="text200" runat="server" MaxLength="40"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        <asp:Label ID="lbDateOfBirth" Text="Date Of Birth" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <%--<uc:DatePicker ID="ucDateOfBirth" runat="server"></uc:DatePicker>--%>
                                                                                        <asp:TextBox ID="tbDateOfBirth" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                            onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                            onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        <asp:Label ID="lbGender" Text="Gender" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="rbMale" runat="server" Text="Male" GroupName="Gender" Checked="true" />
                                                                                        <asp:RadioButton ID="rbFemale" runat="server" Text="Female" GroupName="Gender" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        <asp:Label ID="lbCityOfBirth" Text="City Of Birth" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbCityOfBirth" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvDateOfBirth" runat="server" ErrorMessage="Date Of Birth should not be future date."
                                                                                            ClientValidationFunction="fncClientCheckDate" ValidationGroup="Save" CssClass="alert-text"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Client Code
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbClientCde" runat="server" CssClass="text80" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                            OnTextChanged="tbClientCde_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnClientCde" runat="server" />
                                                                                        <asp:Button ID="btnClientCde" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadClientCodeMasterPopup');return false;" />
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Home Base
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbHomeBase" runat="server" CssClass="text80" MaxLength="4" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                            OnTextChanged="tbHomeBase_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnHomeBase" runat="server" />
                                                                                        <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radAirportPopup');return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:CustomValidator ID="cvClientCde" runat="server" ControlToValidate="tbClientCde"
                                                                                            ErrorMessage="Invalid Client Code." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                                            ErrorMessage="Invalid Home Base." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Default Flight Purpose
                                                                                    </td>
                                                                                    <td align="left" class="tdLabel240">
                                                                                        <asp:TextBox ID="tbFlightPurpose" CssClass="text80" runat="server" MaxLength="2"
                                                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)" OnTextChanged="tbFlightPurpose_OnTextChanged"
                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnFlightPurpose" runat="server" />
                                                                                        <asp:Button ID="btnFlightPurpose" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadFlightPurposeMasterPopup');return false;" />
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Charter Quote Customer
                                                                                    </td>
                                                                                    <td align="left">
                                                                                        <asp:TextBox ID="tbCQCustomer" CssClass="text80" runat="server" MaxLength="5" OnTextChanged="tbCQ_OnTextChanged"
                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnCQCustomerID" runat="server" />
                                                                                        <asp:Button ID="btnCQCustomer" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radCQCPopups');return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:CustomValidator ID="cvFlightPurpose" runat="server" ControlToValidate="tbFlightPurpose"
                                                                                            ErrorMessage="Invalid Flight Purpose Code." Display="Dynamic" CssClass="alert-text"
                                                                                            ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvCQCustomer" runat="server" ControlToValidate="tbCQCustomer"
                                                                                            ErrorMessage="Invalid Charter Quote Customer Code." Display="Dynamic" CssClass="alert-text"
                                                                                            ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr style="display: none;">
                                                                                    <td>
                                                                                        <%--<uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>--%>
                                                                                        <asp:TextBox ID="tbDatePicker" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                            onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                            onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Passenger Weight
                                                                                    </td>
                                                                                    <td valign="top" class="tdLabel240">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbPaxWeight" CssClass="text200" ValidationGroup="Save" Text="000.0"
                                                                                                        onBlur="return ValidateEmptyTextbox(this, event)" runat="server" MaxLength="5"
                                                                                                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:RegularExpressionValidator ID="revPaxWeight" runat="server" Display="Dynamic"
                                                                                                        ErrorMessage="Invalid Format(999.9)" ControlToValidate="tbPaxWeight" CssClass="alert-text"
                                                                                                        ValidationExpression="^[0-9]{0,3}(\.[0-9]{1,1})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Passenger Weight Unit
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="dpPaxWeightUnits" runat="server" CssClass="text90">
                                                                                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                                                            <asp:ListItem Text="Kilo" Value="1"></asp:ListItem>
                                                                                            <asp:ListItem Text="Lbs" Value="2"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Emergency Contacts
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbEmergencyContact" CssClass="text200" runat="server" MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Credit Card Info
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCreditCardInfo" CssClass="text200" runat="server" MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Catering Preferences
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbCateringPreferences" CssClass="text200" runat="server" MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Hotel Preferences
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbHotelPreferences" CssClass="text200" runat="server" MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        <asp:Label ID="Anniversaies" Text="Anniversary" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <%--<uc:DatePicker ID="ucAnniversaries" runat="server"></uc:DatePicker>--%>
                                                                                        <asp:TextBox ID="tbAnniversaries" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                            onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                            onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130" style="display: none;">
                                                                                        <asp:Label ID="Birthday" Text="Date Of Birth" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td style="display: none;">
                                                                                        <%--<uc:DatePicker ID="ucBirthday" runat="server"></uc:DatePicker>--%>
                                                                                        <asp:TextBox ID="tbBirthday" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                            onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                            onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>
                                                                    <asp:Label ID="lbBillingInformation" runat="server" Text="Billing Information" ForeColor="Black"></asp:Label></legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140" valign="top">
                                                                                        Department
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbDepartment" CssClass="text80" runat="server" MaxLength="8" OnTextChanged="tbDepartment_OnTextChanged"
                                                                                                        AutoPostBack="true" onchange="tbDepartment_onchange"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnDepartment" runat="server" />
                                                                                                    <asp:Button ID="btnDepartment" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radDepartmentPopup');return false;" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="lbcvDepartment" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                                                    <asp:Label ID="tbDepartmentDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Authorization
                                                                                    </td>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbAuth" CssClass="text80" runat="server" MaxLength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                        OnTextChanged="tbAuth_OnTextChanged" AutoPostBack="true" onchange="CheckDeptAuthorization();"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnAuth" runat="server" />
                                                                                                    <asp:Button ID="btnAuth" runat="server" CssClass="browse-button" OnClientClick="javascript:fnValidateAuth();return false;" />

                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CustomValidator ID="cvAuth" runat="server" ControlToValidate="tbAuth" ErrorMessage="Invalid Authorization Code."
                                                                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                    <asp:Label ID="tbAuthDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Standard Billing
                                                                                    </td>
                                                                                    <td class="tdLabel240">
                                                                                        <asp:TextBox ID="tbStdBilling" CssClass="text80" runat="server" MaxLength="25"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        Account No.
                                                                                    </td>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbAccountNumber" CssClass="text80" runat="server" MaxLength="32"
                                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.!')" OnTextChanged="tbAccountNumber_OnTextChanged"
                                                                                                        AutoPostBack="true"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnAccountNumber" runat="server" />
                                                                                                    <asp:Button ID="btnAccount" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radAccountMasterPopup');return false;" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CustomValidator ID="cvAccountNumber" runat="server" ControlToValidate="tbAccountNumber"
                                                                                                        ErrorMessage="Invalid Account No." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                                        SetFocusOnError="true"></asp:CustomValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td width="53%" valign="top">
                                                            <fieldset>
                                                                <legend>
                                                                    <asp:Label ID="lbTaxInformation" runat="server" Text="Tax Information" ForeColor="Black"></asp:Label></legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel60">
                                                                                        Type
                                                                                    </td>
                                                                                    <td class="tdLabel100">
                                                                                        <asp:RadioButton ID="radType" runat="server" Text="Control" OnCheckedChanged="TaxType_OnCheckedChanged"
                                                                                            AutoPostBack="true" GroupName="TaxType" />
                                                                                    </td>
                                                                                    <td class="tdLabel130">
                                                                                        <asp:RadioButton ID="radNonControlled" runat="server" Text="Non-Control" OnCheckedChanged="TaxType_OnCheckedChanged"
                                                                                            AutoPostBack="true" GroupName="TaxType" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="radGuest" runat="server" Text="Guest" OnCheckedChanged="TaxType_OnCheckedChanged"
                                                                                            AutoPostBack="true" GroupName="TaxType" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel140">
                                                                                        Associated With
                                                                                    </td>
                                                                                    <%-- <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>--%>
                                                                                    <td class="tdLabel120">
                                                                                        <asp:TextBox ID="tbAssociated" runat="server" Width="80" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                            AutoPostBack="true" OnTextChanged="tbAssociated_OnTextChanged"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnAssociated" runat="server" />
                                                                                        <asp:Button ID="btnAssociated" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadAssociateMasterPopup');return false;" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkSiflSecurity" runat="server" Text="SIFL Security" />
                                                                                    </td>
                                                                                </tr>
                                                                                <%-- </tr>
                                                                                            <tr>--%>
                                                                                <%--</tr>
                                                                                        </table>
                                                                                    </td>--%>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvAssociated" runat="server" ControlToValidate="tbAssociated"
                                                                                            ErrorMessage="Invalid Association." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td width="47%" valign="top">
                                                            <fieldset>
                                                                <legend>
                                                                    <asp:Label ID="lbTravelSenseInformation" runat="server" Text="Travel$ense Information"
                                                                        ForeColor="Black"> </asp:Label></legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel80">
                                                                                        T$ense ID
                                                                                    </td>
                                                                                    <td class="tdLabel100">
                                                                                        <asp:TextBox ID="tbTsenseid" CssClass="text80" runat="server" MaxLength="9" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel50">
                                                                                        Title
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTitle" CssClass="text80" runat="server" MaxLength="30" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel80">
                                                                                        Salary Level
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSalaryLevel" CssClass="text80" runat="server" MaxLength="4" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:RangeValidator ID="rvSalaryLevel" runat="server" ControlToValidate="tbSalaryLevel"
                                                                                            Type="double" MinimumValue="1" MaximumValue="9999" ValidationGroup="Save" CssClass="alert-text"
                                                                                            SetFocusOnError="true" ErrorMessage="Salary Level Must Be Within the Range 1 to 9999."
                                                                                            Display="Dynamic" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>
                                                                    <asp:Label ID="lbAlienRegistrationCard" runat="server" Text="Permanent Residence Card"
                                                                        ForeColor="Black"></asp:Label></legend>
                                                                <table>
                                                                    <tr>
                                                                        <td class="tdLabel140">
                                                                            INS A#
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <asp:TextBox ID="tbINSANumber" runat="server" MaxLength="60"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel140">
                                                                            Category
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <asp:TextBox ID="tbCategory" runat="server" MaxLength="30"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdLabel140">
                                                                            Card Expiration
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <%--<uc:DatePicker ID="ucCardExpires" runat="server"></uc:DatePicker>--%>
                                                                            <asp:TextBox ID="tbCardExpires" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel140">
                                                                            Resident Since Year
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <asp:TextBox ID="tbResidentSinceYear" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                onBlur="return RemoveSpecialChars(this);" MaxLength="4"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdLabel140">
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                        </td>
                                                                        <td class="tdLabel140">
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <asp:RangeValidator ID="rvYear" runat="server" ControlToValidate="tbResidentSinceYear"
                                                                                Type="Double" MinimumValue="1900" MaximumValue="2100" ValidationGroup="Save"
                                                                                CssClass="alert-text" ErrorMessage="Year should be 1900 to 2100" Display="Dynamic"></asp:RangeValidator>
                                                                            <asp:CustomValidator ID="cvyear" runat="server" ErrorMessage="Resident Since Year should not be future year."
                                                                                ClientValidationFunction="fncResidentSinceYear" ValidationGroup="Save" CssClass="alert-text"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <%-- <td class="tdLabel140" valign="top">
                                                                            Gender
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <asp:RadioButton ID="rbPaxMale" runat="server" Text="Male" GroupName="PaxGender"
                                                                                Checked="true" />
                                                                            <asp:RadioButton ID="rbPaxFemale" runat="server" Text="Female" GroupName="PaxGender" />
                                                                        </td>--%>
                                                                        <td class="tdLabel140">
                                                                            Country of Birth
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbGreenNationality" CssClass="text170" runat="server" MaxLength="3"
                                                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)" OnTextChanged="tbGreenNationality_OnTextChanged"
                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnGreenNationality" runat="server" />
                                                                                        <asp:Button ID="btnGreenNationality" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadGreenNationalityMasterPopup');return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvGreenNationality" runat="server" ControlToValidate="tbGreenNationality"
                                                                                            ErrorMessage="Invalid Country Code" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                    </td> </tr> </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                runat="server" OnClientItemExpand="pnlImageExpend" OnClientItemCollapse="pnlImageCollapse">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Attachments" CssClass="PanelHeaderStyle">
                        <ContentTemplate>
                            <table width="100%" class="box1">
                                <tr style="display: none;">
                                    <td class="tdLabel100">
                                        Document Name
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text210" OnBlur="CheckName();"
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="tdLabel270">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:FileUpload ID="fileUL" runat="server" Enabled="false" ClientIDMode="Static"
                                                        onchange="javascript:return File_onchange();" />
                                                    <asp:Label ID="lblError" CssClass="alert-text" runat="server" Style="float: left;">All file types are allowed.</asp:Label>
                                                    <asp:HiddenField ID="hdnUrl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" class="tdLabel110">
                                        <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                            OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text200"
                                            ClientIDMode="AutoID">
                                        </asp:DropDownList>
                                    </td>
                                    <td valign="top" class="tdLabel80">
                                        <asp:Image ID="imgFile" Width="50px" Height="50px" runat="server" OnClick="OpenRadWindow();"
                                            AlternateText="" ClientIDMode="Static" />
                                        <asp:HyperLink ID="lnkFileName" runat="server" ClientIDMode="Static">Download File</asp:HyperLink>
                                    </td>
                                    <td valign="top" align="right" class="custom_radbutton">
                                        <telerik:RadButton ID="btndeleteImage" runat="server" Text="Delete" Enabled="false"
                                            OnClientClicking="ImageDeleteConfirm" OnClick="DeleteImage_Click" ClientIDMode="Static" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlAdditionalInfo" Width="100%" ExpandAnimation-Type="None"
                            CollapseAnimation-Type="None" runat="server" OnClientItemExpand="pnlAdditionalInfoExpend" OnClientItemCollapse="pnlAdditionalInfoCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Additional Information">
                                    <Items>
                                        <telerik:RadPanelItem Value="AddInfo" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" class="box1">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="dgPassengerAddlInfo" runat="server" EnableAJAX="True" AllowMultiRowSelection="false"
                                                                Height="245px">
                                                                <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                                                    DataKeyNames="PassengerAdditionalInfoID,CustomerID,PassengerRequestorID,AdditionalINFOCD,AdditionalINFODescription,AdditionalINFOValue,ClientID,LastUpdUID,LastUptTS,IsDeleted"
                                                                    CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false" AllowPaging="false">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn DataField="PassengerAdditionalInfoID" UniqueName="PassengerAdditionalInfoID"
                                                                            HeaderText="Code" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" AllowFiltering="false" Display="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="PassengerInformationID" UniqueName="PassengerInformationID"
                                                                            HeaderText="Code" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" AllowFiltering="false" Display="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="AdditionalINFOCD" UniqueName="AdditionalINFOCD"
                                                                            HeaderText="Code" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="80px">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="AdditionalINFODescription" UniqueName="AdditionalINFODescription"
                                                                            HeaderText="Description" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="250px">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Additional Information" CurrentFilterFunction="Contains"
                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="AdditionalINFOValue"
                                                                            HeaderStyle-Width="250px" AllowFiltering="false">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbAddlInfo" runat="server" CssClass="text230" Text='<%# Eval("AdditionalINFOValue") %>'
                                                                                    MaxLength="25"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Show in Tripsheet Report" CurrentFilterFunction="Contains"
                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IsPrint" AllowFiltering="false"
                                                                            HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkIsPrint" runat="server" Checked='<%# Eval("IsPrint") != null ? Eval("IsPrint") : false %>' />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                                <GroupingSettings CaseSensitive="false" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnAddlInfo" runat="server" Text="Add Info." CausesValidation="false"
                                                                            OnClientClicking="ShowAddlInfoPopup" />
                                                                    </td>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnDeleteInfo" runat="server" Text="Delete Info." CausesValidation="false"
                                                                            OnClientClicking="DeleteInfoConfirm" OnClick="DeleteInfo_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlChecklist" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            runat="server" OnClientItemExpand="pnlChecklistExpend" OnClientItemCollapse="pnlChecklistCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Passenger Checklist">
                                    <Items>
                                        <telerik:RadPanelItem Value="Checklist" runat="server">
                                            <ContentTemplate>
                                                <table id="tblCrewCheckList" runat="server" class="border-box" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td class="tdLabel60">
                                                                        Code :
                                                                    </td>
                                                                    <td class="tdLabel70">
                                                                        <asp:Label ID="lbCheckListCode" runat="server" CssClass="text60"></asp:Label>
                                                                    </td>
                                                                    <td class="tdLabel700">
                                                                        Description :
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lbCheckListDescription" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:CheckBox ID="chkDisplayInactiveChecklist" AutoPostBack="true" OnCheckedChanged="chkDisplayInactiveChecklist_CheckedChanged"
                                                                            runat="server" Text="Active Only Checklist" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0" class="border-box">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel90">
                                                                                                Original Date
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbOriginalDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                                    OnTextChanged="OriginalDate_TextChanged" AutoPostBack="true" onclick="showPopup(this, event);"
                                                                                                    onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel90">
                                                                                                Frequency
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:RadioButtonList ID="radFreq" runat="server" OnSelectedIndexChanged="radFreq_SelectedIndexChanged"
                                                                                                    RepeatDirection="Horizontal" AutoPostBack="true">
                                                                                                    <asp:ListItem Selected="True" Value="1" Text="Months"></asp:ListItem>
                                                                                                    <asp:ListItem Value="2" Text="Days"></asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdLabel90">
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbFreq" runat="server" CssClass="text70" MaxLength="3" AutoPostBack="true"
                                                                                                    onKeyPress="return fnAllowNumeric(this, event)" OnTextChanged="Freq_TextChanged"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel90">
                                                                                                Previous
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbPrevious" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" onclick="showPopup(this, event);"
                                                                                                    onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                                    OnTextChanged="Previous_TextChanged" MaxLength="10"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel90">
                                                                                                Due Next
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbDueNext" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                                    AutoPostBack="true" OnTextChanged="DueNext_TextChanged" onclick="showPopup(this, event);"
                                                                                                    onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    Alert Date
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbAlertDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                        AutoPostBack="true" OnTextChanged="AlertDate_TextChanged" onclick="showPopup(this, event);"
                                                                                        onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                        onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Alert Days
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbAlertDays" runat="server" CssClass="text70" AutoPostBack="true"
                                                                                        onBlur="return allnumeric(this, event)" onKeyPress="return fnAllowNumericAndChar(this,event,'-')"
                                                                                        MaxLength="3" OnTextChanged="AlertDays_TextChanged">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Grace Date
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbGraceDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                        AutoPostBack="true" OnTextChanged="GraceDate_TextChanged" onclick="showPopup(this, event);"
                                                                                        onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                        onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Grace Days
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbGraceDays" runat="server" CssClass="text70" AutoPostBack="true"
                                                                                        onBlur="return allnumeric(this, event)" onKeyPress="return fnAllowNumericAndChar(this,event,'-')"
                                                                                        MaxLength="3" OnTextChanged="GraceDays_TextChanged">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                        </table>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInactive" runat="server" Text="Inactive" AutoPostBack="true"
                                                                                        OnCheckedChanged="ChecklistchkInactive_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkSetToEndOfMonth" runat="server" Text="Set To End Of Month" AutoPostBack="true"
                                                                                        OnCheckedChanged="SetToEndOfMonth_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkSetToNextOfMonth" runat="server" Text="Set To 1st Of Next Month"
                                                                                        AutoPostBack="true" OnCheckedChanged="SetToNextOfMonth_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkSetToEndOfCalenderYear" runat="server" Text="Set To End Of Calendar Year"
                                                                                        AutoPostBack="true" OnCheckedChanged="SetToEndOfCalenderYear_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ckhDisableDateCalculation" runat="server" Text="Disable Date Calculation"
                                                                                        AutoPostBack="true" OnCheckedChanged="ckhDisableDateCalculation_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel120">
                                                                                                <asp:CheckBox ID="chkOneTimeEvent" runat="server" Text="One Time Event" OnCheckedChanged="OneTimeEvent_CheckChanged"
                                                                                                    AutoPostBack="true" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:CheckBox ID="chkCompleted" runat="server" Enabled="false" Text="Completed" OnCheckedChanged="ChecklistchkCompleted_CheckChanged"
                                                                                                    AutoPostBack="true" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkNonConflictedEvent" runat="server" Text="Non-Conflicted Event"
                                                                                        AutoPostBack="true" OnCheckedChanged="ChecklistchkNonConflictedEvent_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRemoveFromChecklistRept" runat="server" Text="Remove From Checklist Report"
                                                                                        AutoPostBack="true" OnCheckedChanged="ChecklistchkRemoveFromChecklistRept_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkDoNotPrintPassDueAlert" runat="server" Text="Do Not Print Past Due Alert"
                                                                                        AutoPostBack="true" OnCheckedChanged="ChecklistchkDoNotPrintPassDueAlert_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkPrintOneTimeEventCompleted" runat="server" Enabled="false" Text="Print One Time Event/Completed"
                                                                                        AutoPostBack="true" OnCheckedChanged="ChecklistchkPrintOneTimeEventCompleted_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left">
                                                            <telerik:RadGrid ID="dgCrewCheckList" runat="server" EnableAJAX="True" AllowFilteringByColumn="false"
                                                                AllowPaging="false" PageSize="999" AutoGenerateColumns="false" OnSelectedIndexChanged="CrewCheckList_SelectedIndexChanged"
                                                                OnItemDataBound="CrewCheckList_ItemDataBound" Width="768px" Height="341px">
                                                                <MasterTableView CommandItemDisplay="None" ShowFooter="false" AllowPaging="false"
                                                                    AllowFilteringByColumn="false" AllowSorting="false" ItemStyle-HorizontalAlign="Left"
                                                                    DataKeyNames="CustomerID,PassengerID,PassengerCheckListDetailID,PassengerAdditionalInfoID,PassengerChecklistCD,OriginalDT,PreviousCheckDT,DueDT,AlertDT,GraceDT,IsEndCalendarYear,IsNextMonth,Frequency,IsPassedDueAlert,IsPrintStatus,IsCompleted,IsInActive,IsNoChecklistREPT,IsNoConflictEvent,IsOneTimeEvent,IsStopCALC,IsMonthEnd">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn DataField="PassengerCheckListDetailID" HeaderText="Code"
                                                                            Display="false" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="60px">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="PassengerAdditionalInfoID" HeaderText="Code"
                                                                            Display="false" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="60px">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="PassengerChecklistCD" HeaderText="Code" CurrentFilterFunction="Contains"
                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="60px">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Description" CurrentFilterFunction="Contains"
                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="CrewChecklistDescription"
                                                                            HeaderStyle-Width="200px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbCrewChecklistDescription" runat="server" CssClass="text200" Text='<%# Eval("PassengerChecklistDescription") %>'
                                                                                    MaxLength="40"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Previous" CurrentFilterFunction="Contains"
                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="PreviousCheckDT"
                                                                            HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbPreviousCheckDT" runat="server" CssClass="text60" MaxLength="40"
                                                                                    Text='<%# Eval("PreviousCheckDT") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Due Next" CurrentFilterFunction="Contains"
                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="DueDT" HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbDueDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("DueDT") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Alert" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" UniqueName="AlertDT" HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbAlertDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("AlertDT") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Grace" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" UniqueName="GraceDT" HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbGraceDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("GraceDT") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Alert" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" UniqueName="AlertDays" HeaderStyle-Width="60px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbAlertDays" runat="server" CssClass="text60" Text='<%# Eval("AlertDays") %>'
                                                                                    MaxLength="40"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Grace" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" UniqueName="GraceDays" HeaderStyle-Width="60px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbGraceDays" runat="server" CssClass="text60" Text='<%# Eval("GraceDays") %>'
                                                                                    MaxLength="40"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Freq" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" UniqueName="FrequencyMonth" HeaderStyle-Width="60px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbFrequencyMonth" runat="server" CssClass="text60" Text='<%# Eval("FrequencyMonth") %>'
                                                                                    MaxLength="40"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Original Date" CurrentFilterFunction="Contains"
                                                                            Display="false" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="OriginalDT"
                                                                            HeaderStyle-Width="80px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbOriginalDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("OriginalDT") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Tot Req Hrs" Display="false" ShowFilterIcon="false"
                                                                            UniqueName="HiddenCheckboxValues" HeaderStyle-Width="80px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="RadFreq" runat="server" Text='<%# Eval("Frequency") %>' />
                                                                                <asp:CheckBox ID="chkPassedDueAlert" runat="server" Checked='<%# Eval("IsPassedDueAlert") != null ? Eval("IsPassedDueAlert") : false %>' />
                                                                                <asp:CheckBox ID="chkInActive" runat="server" Checked='<%# Eval("IsInActive") != null ? Eval("IsInActive") : false %>' />
                                                                                <asp:CheckBox ID="chkMonthEnd" runat="server" Checked='<%# Eval("IsMonthEnd") != null ? Eval("IsMonthEnd") : false %>' />
                                                                                <asp:CheckBox ID="chkStopCALC" runat="server" Checked='<%# Eval("IsStopCALC") != null ? Eval("IsStopCALC") : false %>' />
                                                                                <asp:CheckBox ID="chkOneTimeEvent" runat="server" Checked='<%# Eval("IsOneTimeEvent") != null ? Eval("IsOneTimeEvent") : false %>' />
                                                                                <asp:CheckBox ID="chkCompleted" runat="server" Checked='<%# Eval("IsCompleted") != null ? Eval("IsCompleted") : false %>' />
                                                                                <asp:CheckBox ID="chkNoConflictEvent" runat="server" Checked='<%# Eval("IsNoConflictEvent") != null ? Eval("IsNoConflictEvent") : false %>' />
                                                                                <asp:CheckBox ID="chkNoChecklistREPT" runat="server" Checked='<%# Eval("IsNoChecklistREPT") != null ? Eval("IsNoChecklistREPT") : false %>' />
                                                                                <asp:CheckBox ID="chkPrintStatus" runat="server" Checked='<%# Eval("IsPrintStatus") != null ? Eval("IsPrintStatus") : false %>' />
                                                                                <asp:CheckBox ID="chkNextMonth" runat="server" Checked='<%# Eval("IsNextMonth") != null ? Eval("IsNextMonth") : false %>' />
                                                                                <asp:CheckBox ID="chkEndCalendarYear" runat="server" Checked='<%# Eval("IsEndCalendarYear") != null ? Eval("IsEndCalendarYear") : false %>' />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                </MasterTableView>
                                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                                <GroupingSettings CaseSensitive="false" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td align="right">
                                                            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                                                <tr>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnApplyToAdditionalCrew" runat="server" Text="Apply To Additional Pax"
                                                                            CausesValidation="false" OnClientClicking="ShowChecklistSettingsPopup" />
                                                                    </td>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnAddChecklist" runat="server" Text="Add Checklist" CausesValidation="false"
                                                                            OnClientClicking="ShowCrewCheckListPopup" />
                                                                    </td>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnDeleteChecklist" runat="server" Text="Delete Checklist"
                                                                            ValidationGroup="Save" OnClientClicking="CLickingChecklistDelete" OnClick="DeleteChecklist_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlPassport" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                            runat="server" OnClientItemExpand="pnlPassportExpend" OnClientItemCollapse="pnlPassportCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Passport">
                                    <Items>
                                        <telerik:RadPanelItem Value="Passport" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" class="box1">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="dgPassport" runat="server" OnNeedDataSource="Passport_BindData"
                                                                OnItemDataBound="dgPassport_ItemDataBound" EnableAJAX="True" AutoGenerateColumns="false"
                                                                AllowFilteringByColumn="false" Height="140px" OnPageIndexChanged="dgPassport_PageIndexChanged">
                                                                <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                                                    DataKeyNames="PassportID,CrewID,PassengerRequestorID,CustomerID,Choice,IssueCity,PassportNum,PassportExpiryDT,CountryID,IsDefaultPassport,
                                                                    PilotLicenseNum,LastUpdUID,LastUpdTS,IssueDT,IsDeleted,CountryCD,CountryName"
                                                                    CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false" AllowPaging="false">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn HeaderText="Passport No." CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="150px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                            UniqueName="PassportNUM">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbPassportNum" runat="server" CssClass="text160" Text='<%# Eval("PassportNUM") %>'
                                                                                    MaxLength="25" onblur="javascript:return CheckPassportChanged();"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Choice" CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="40px" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IsChoice">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkChoice" runat="server" Checked='<%# Eval("Choice") != null ? Eval("Choice") : false %>'
                                                                                    onchange="javascript:CheckPassportChanged();" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Expiration Date" CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="80px" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="ExpiryDT">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbPassportExpiryDT" runat="server" CssClass="text65" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event);ValidateDate(this);SetNextFocus(this);CheckPassportChanged();"
                                                                                    MaxLength="12" Text='<%# Eval("PassportExpiryDT") %>' onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Issuing City/Authority" CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="180px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                            UniqueName="IssueCity">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbIssueCity" runat="server" CssClass="text200" Text='<%# Eval("IssueCity") %>'
                                                                                    MaxLength="40" onblur="CheckPassportChanged();"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Issue Date" CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="70px" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IssueDT">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbIssueDT" runat="server" CssClass="text65" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event);ValidateDate(this);SetNextFocus(this);CheckPassportChanged();"
                                                                                    MaxLength="12" Text='<%# Eval("IssueDT") %>' onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Country" CurrentFilterFunction="Contains"
                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="CountryCD" HeaderStyle-Width="70px">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbPassportCountry" runat="server" CssClass="text30" Text='<%# Eval("CountryCD") %>'
                                                                                    AutoPostBack="true" OnTextChanged="Nation_TextChanged" MaxLength="8" onblur="CheckPassportChanged();"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdnPassportCountry" runat="server" Value='<%# Eval("CountryID") %>' />
                                                                                <asp:Button ID="btnPassportCountry" runat="server" CssClass="browse-button" OnClientClick="javascript:CheckPassportChanged();openWinGrd('RadVisaCountryMasterPopup',this);return false;" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                </MasterTableView>
                                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                                <GroupingSettings CaseSensitive="false" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnAddPassport" runat="server" Text="Add Passport" CausesValidation="false"
                                                                            OnClientClicking="CheckPassportChanged" OnClick="AddPassport_Click" />
                                                                        <asp:HiddenField ID="hdnPaxPassport" runat="server"/>
                                                                    </td>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnDeletePassport" runat="server" Text="Delete Passport" CausesValidation="false"
                                                                            OnClientClicking="DeletePassportConfirm" OnClick="DeletePassport_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlVisa" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                            runat="server" OnClientItemExpand="pnlVisaExpend" OnClientItemCollapse="pnlVisaCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Visa">
                                    <Items>
                                        <telerik:RadPanelItem Value="Visa" runat="server">
                                            <ContentTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="center" width="100%">
                                                            <telerik:RadGrid ID="dgVisa" runat="server" OnNeedDataSource="Visa_BindData" EnableAJAX="True"
                                                                Width="761px" OnItemDataBound="dgVisa_ItemDataBound" Height="140px" OnPageIndexChanged="dgVisa_PageIndexChanged">
                                                                <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Auto"
                                                                    DataKeyNames="VisaID,CrewID,PassengerRequestorID,CustomerID,VisaTYPE,VisaNum,LastUpdUID,LastUpdTS,CountryID,ExpiryDT,Notes,IssuePlace,IssueDate,IsDeleted,CountryCD,CountryName,TypeOfVisa,EntriesAllowedInPassport,VisaExpireInDays"
                                                                    CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false" AllowPaging="false">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn DataField="VisaID" UniqueName="VisaID" HeaderText="VisaID"
                                                                            CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                            AllowFiltering="false" Display="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Country" CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="70px" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="CountryCD">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbCountryCD" runat="server" CssClass="text20" AutoPostBack="true"
                                                                                    Text='<%# Eval("CountryCD") %>' OnTextChanged="CountryCD_TextChanged" MaxLength="6"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                                                                <asp:Button ID="btnCountryCD" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinGrd('RadVisaCountryMasterPopup',this);return false;" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Visa No." CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="185px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                            UniqueName="VisaNum">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbVisaNum" runat="server" CssClass="text160" Text='<%# Eval("VisaNum") %>'
                                                                                    MaxLength="25" onblur="javascript:return CheckVisaDuplicate(this, event);"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Expiration Date" CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="85px" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="ExpiryDT">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbExpiryDT" runat="server" CssClass="text65" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event);ValidateDate(this);SetNextFocus(this);"
                                                                                    MaxLength="12" Text='<%# Eval("ExpiryDT") %>' onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Issuing City/Authority" CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="152px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                            UniqueName="IssuePlace">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbIssuePlace" runat="server" CssClass="tdtext120" Text='<%# Eval("IssuePlace") %>'
                                                                                    MaxLength="40"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Issue Date" CurrentFilterFunction="Contains"
                                                                            HeaderStyle-Width="85px" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IssueDate">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbIssueDate" runat="server" CssClass="text65" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event);ValidateDate(this);SetNextFocus(this);"
                                                                                    MaxLength="12" Text='<%# Eval("IssueDate") %>' onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn FilterControlAltText="Choose Visa Type" HeaderText="Type Of Visa"
                                                                            HeaderStyle-Width="90px" UniqueName="TypeOfVisa" CurrentFilterFunction="Contains"
                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddlTypeOfVisa" runat="server" CssClass="dropdown100" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Number of Entries Allowed" ShowFilterIcon="false"
                                                                            UniqueName="EntriesAllowedInPassport" HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbEntriesAllowedInPassport" runat="server" CssClass="text40" onKeyPress="return fnAllowNumeric(this, event,'')"
                                                                                    onblur="parseInt(this, event);" MaxLength="5" Text='<%# Eval("EntriesAllowedInPassport") %>'>
                                                                                </asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Visa Activation Days" ShowFilterIcon="false"
                                                                            UniqueName="VisaExpireInDays" HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbVisaExpireInDays" runat="server" CssClass="text40" onKeyPress="return fnAllowNumeric(this, event,'')"
                                                                                    onblur="parseInt(this, event);" MaxLength="5" Text='<%# Eval("VisaExpireInDays") %>'
                                                                                    ToolTip="Will expire if not used within x days from issue date">
                                                                                </asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Notes" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" UniqueName="Notes" HeaderStyle-Width="145px">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbNotes" runat="server" CssClass="text120" Text='<%# Eval("Notes") %>'
                                                                                    MaxLength="40"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                                <GroupingSettings CaseSensitive="false" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnAddVisa" runat="server" Text="Add Visa" CausesValidation="false"
                                                                            OnClick="AddVisa_Click" />
                                                                    </td>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnDeleteVisa" runat="server" Text="Delete Visa" CausesValidation="false"
                                                                            OnClientClicking="DeleteVisaConfirm" OnClick="DeleteVisa_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="Notes" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            runat="server" OnClientItemExpand="pnlNotesExpend" OnClientItemCollapse="pnlNotesCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="false" Text="Notes" CssClass="PanelHeaderCrewRoster">
                                    <Items>
                                        <telerik:RadPanelItem Value="Notes" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0" class="note-box">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="tbNotes" TextMode="MultiLine" runat="server" CssClass="textarea-db"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="PaxItems" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            runat="server" OnClientItemExpand="pnlPaxItemsExpend" OnClientItemCollapse="pnlPaxItemsCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="false" Text="PAX Alerts" CssClass="PanelHeaderCrewRoster">
                                    <Items>
                                        <telerik:RadPanelItem Value="PaxItems" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0" class="note-box">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="tbPaxItems" TextMode="MultiLine" runat="server" CssClass="textarea-db"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="button" OnClick="Edit_Click" />
                    </td>
                    <td class="custom_radbutton">
                        <telerik:RadButton ID="btnSaveChanges" runat="server" Text="Save" ValidationGroup="Save"
                            OnClick="btnSaveChangesTop_Click" OnClientClicking="CheckPassportChoice" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="Cancel_Click" />
                    </td>
                </tr>
            </table>
            <table id="tblHidden" style="display: none;">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChangesdummy" runat="server" Text="Save" OnClick="Save_Click" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnFirstOrLast" runat="server" />
            <asp:HiddenField ID="hdnTSAStatus" runat="server" />
            <asp:HiddenField ID="hdnWarning" runat="server" />
            <asp:HiddenField ID="hdnTSATSADTTM" runat="server" />
            <asp:HiddenField ID="hdnSave" runat="server" />
            <asp:HiddenField ID="hdnDate" runat="server" />
            <asp:HiddenField ID="hdnDateFormat" runat="server" />
            <asp:HiddenField ID="hdnIsPassportChoiceChanged" runat="server" />
            <asp:HiddenField ID="hdnIsPassportChoice" runat="server" />
            <asp:HiddenField ID="hdnIsPassportChanged" runat="server" />
            <asp:HiddenField ID="hdnIsPassportChoiceDeletedCount" runat="server" />
            <asp:HiddenField ID="hdnIsPassportChoiceDeleted" runat="server" />
            <asp:HiddenField ID="hdnBrowserName" runat="server" />
            <asp:HiddenField ID="hdnDelete" runat="server" />
            <asp:HiddenField ID="hdnTemp" runat="server" />
            <asp:HiddenField ID="hdnRedirect" runat="server" /> 
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
            <script type="text/javascript" src="../../../Scripts/jquery.browser.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
