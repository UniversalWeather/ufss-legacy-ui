﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.People
{
    public partial class PassengerAddInfoPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            lblMessage.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerAdditionalInfo);
                }
            }
        }
        /// <summary>
        /// Method to Bind Grid Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PassengerAddInfo_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objPassengerAddInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objPassengerAddInfoVal = objPassengerAddInfoService.GetPassengerAdditionalInfoList();
                            if (objPassengerAddInfoVal.ReturnFlag == true)
                            {
                                dgPassengerAddInfo.DataSource = objPassengerAddInfoVal.EntityList.Where(x => x.IsDeleted == false && x.IsCheckList != true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerAdditionalInfo);
                }
            }
        }
        /// <summary>
        /// Method to trigger Grid Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PassengerAddInfo_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerAdditionalInfo);
                }
            }
        }
        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void PassengerAddInfo_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerAdditionalInfo);
                }
            }
        }
        /// <summary>
        /// Method to Bind into "CrewNewAddlInfo" session, and 
        /// Call "CloseAndRebind" javascript function to Close and
        /// Rebind the selected record into parent page.
        /// </summary>
        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgPassengerAddInfo.SelectedItems.Count > 0)
                {
                    GridDataItem item = (GridDataItem)dgPassengerAddInfo.SelectedItems[0];
                    if (CheckIfExists(item.GetDataKeyValue("PassengerInfoCD").ToString()))
                    {
                        lblMessage.Text = "Warning, Additional Info. Code Already Exists.";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        // Code for Adding the existing Passenger Additional Info if deleted
                        bool IsExist = false;
                        if (Session["PassengerAdditionalInfo"] != null)
                        {
                            List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAdditionalInfo = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfo"];
                            for (int Index = 0; Index < PassengerAdditionalInfo.Count; Index++)
                            {
                                if (PassengerAdditionalInfo[Index].AdditionalINFOCD == item.GetDataKeyValue("PassengerInfoCD").ToString())
                                {
                                    PassengerAdditionalInfo[Index].PassengerInformationID = Convert.ToInt64(item.GetDataKeyValue("PassengerInformationID").ToString());
                                    PassengerAdditionalInfo[Index].AdditionalINFOValue = string.Empty;
                                    PassengerAdditionalInfo[Index].IsDeleted = false;
                                    IsExist = true;
                                }
                            }
                            Session["PassengerAdditionalInfo"] = PassengerAdditionalInfo;
                        }
                        if (IsExist == false)
                        {
                            List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAdditionalInfoList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                            FlightPakMasterService.PassengerAdditionalInfo PassengerAdditionalInfoDef = new FlightPakMasterService.PassengerAdditionalInfo();
                            PassengerAdditionalInfoDef.AdditionalINFOCD = item.GetDataKeyValue("PassengerInfoCD").ToString();
                            PassengerAdditionalInfoDef.PassengerInformationID = Convert.ToInt64(item.GetDataKeyValue("PassengerInformationID").ToString());
                            PassengerAdditionalInfoDef.AdditionalINFODescription = item.GetDataKeyValue("PassengerDescription").ToString();
                            PassengerAdditionalInfoDef.AdditionalINFOValue = string.Empty;
                            //crewDef.IsReptFilter = false;
                            PassengerAdditionalInfoDef.IsDeleted = false;
                            PassengerAdditionalInfoList.Add(PassengerAdditionalInfoDef);

                            if (Session["PassengerAdditionalInfo"] == null)
                            {
                                Session["PassengerAdditionalInfo"] = PassengerAdditionalInfoList;
                            }
                            else
                            {
                                Session["PassengerAdditionalInfoNew"] = PassengerAdditionalInfoList;
                            }
                        }
                        InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                    }
                }
            }
        }
        /// <summary>
        /// Function to Check if Crew Information Code alerady exists or not, by passing Crew Code
        /// </summary>
        /// <returns>True / False</returns>
        private bool CheckIfExists(string infoCode)
        {
            bool returnVal = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(infoCode))
            {
                if (Session["PassengerAdditionalInfo"] != null)
                {
                    List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAdditionalInfoDef = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfo"];
                    var result = (from PassengerAdditionalInf in PassengerAdditionalInfoDef
                                  where PassengerAdditionalInf.AdditionalINFOCD.ToUpper().Trim() == infoCode.ToUpper().Trim() &&
                                        PassengerAdditionalInf.IsDeleted == false
                                  select PassengerAdditionalInf);
                    if (result.Count() > 0)
                    { returnVal = true; }
                }
            }
            return returnVal;
        }

        protected void dgPassengerAddInfo_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPassengerAddInfo, Page.Session);
        }
    }
}
