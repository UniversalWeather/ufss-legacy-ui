﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewNamePopUp : BaseSecuredPage
    {

        List<string> crewRoaster = new List<string>();
        private ExceptionManager exManager;
        private string VendorCD;
        private string CrewLastName = string.Empty;
        public bool showCommandItems = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        showCommandItems = true;
                        if (Request.QueryString["CrewLastName"] != null)
                        {
                            CrewLastName = Convert.ToString(Request.QueryString["CrewLastName"]);
                        }



                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }



        }

        /// <summary>
        /// Bound Row styles, based on Checklist information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewRoster_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRoster_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAircraftType);
                }
            }

        }

        /// <summary>
        /// Method to Bind into "CrewNewTypeRating" session, and 
        /// Call "CloseAndRebind" javascript function to Close and
        /// Rebind the selected record into parent page.
        /// </summary>
        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewRoster.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgCrewRoster.SelectedItems[0];
                        Session["NewCrewRosterSelectID"] = Convert.ToString(Item.GetDataKeyValue("CrewID"));
                        //InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "CloseAndRebind('navigateToInserted')", true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }

        /// <summary>
        /// Bind Crew Roaster Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgCrewRoster_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewRosterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (CrewRosterService != null)
                            {
                                var CrewData = CrewRosterService.GetCrewList();
                                if (CrewData != null)
                                {
                                    var CrewList = CrewData.EntityList.Where(x => x.IsDeleted == false).ToList();
                                    if (CrewData.ReturnFlag == true)
                                    {
                                        dgCrewRoster.DataSource = CrewList;
                                    }
                                    Session["CrewListPopUp"] = (List<GetAllCrew>)CrewList;
                                    DoSearchfilter(false);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        protected void dgCrewRoster_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCrewRoster.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCrewRoster;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    if (dgCrewRoster.Items.Count > 0)
            //    {
            //        dgCrewRoster.SelectedIndexes.Add(0);
            //        GridDataItem Item = (GridDataItem)dgCrewRoster.SelectedItems[0];
            //        Item.Selected = true;

            //    }
            //}

            if (!IsPostBack)
            {
                if (dgCrewRoster.Items.Count > 0)
                {
                    SelectItem();
                }
                //else
                //{
                //    dgCrewRoster.SelectedIndexes.Add(0);
                //}
            }
        }

        protected void SelectItem()
        {
            if (!string.IsNullOrEmpty(VendorCD))
            {

                foreach (GridDataItem item in dgCrewRoster.MasterTableView.Items)
                {
                    if (item["CrewCD"].Text.Trim().ToUpper() == VendorCD)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (dgCrewRoster.MasterTableView.Items.Count > 0)
                {
                    dgCrewRoster.SelectedIndexes.Add(0);

                }
            }
        }

        #region Filters

        protected void ActiveOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchfilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void HomeBaseOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        DoSearchfilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }
        protected void FixedWingOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        DoSearchfilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void RotaryWing_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        DoSearchfilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }



        protected void ClientCodeFilter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckClientFilterCodeExist())
                        {
                            cvClientCodeFilter.IsValid = false;
                            tbClientCodeFilter.Text = "";
                            tbClientCodeFilter.Focus();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        public bool DoSearchfilter(bool IsDatabind)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient Service1 = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                List<GetAllCrew> FilterCrew = new List<GetAllCrew>();
                List<GetCrewGroupbasedCrew> FilterGroupCrew = new List<GetCrewGroupbasedCrew>();
                FilterCrew = (List<GetAllCrew>)Session["CrewListPopUp"];
                CheckClientFilterCodeExist();

                if (chkShowAllCrew.Checked == false && CrewLastName != string.Empty)
                {
                    FilterCrew = FilterCrew.Where(x => (x.LastName != null)).ToList();
                    FilterCrew = FilterCrew.Where(x => (x.LastName.Trim().ToUpper().Equals(CrewLastName.ToString().ToUpper().Trim())) && (x.IsDeleted == false)).ToList();
                }
                else if (chkShowAllCrew.Checked == false && CrewLastName == string.Empty)
                {
                    FilterCrew = FilterCrew.Where(x => (x.LastName == null) || ((x.LastName != null) && (x.LastName.Trim().ToUpper().Equals(CrewLastName.ToString().ToUpper().Trim())))).ToList();
                }
                else if (chkShowAllCrew.Checked == true)
                {

                }

                if (tbClientCodeFilter.Text.Trim() != "")
                {
                    FilterCrew = FilterCrew.Where(x => (x.ClientCD != null)).ToList();
                    FilterCrew = FilterCrew.Where(x => (x.ClientCD.Trim().ToUpper().Equals(tbClientCodeFilter.Text.ToString().ToUpper().Trim())) && (x.IsDeleted == false)).ToList();
                }
                dgCrewRoster.DataSource = FilterCrew;
                if (IsDatabind)
                {
                    dgCrewRoster.DataBind();
                }
            }

            return false;
        }

        private bool CheckClientFilterCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClientCodeFilter.Text.Trim() != string.Empty) && (tbClientCodeFilter.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValueFilter = ClientService.GetClientCodeList().EntityList.Where(x => (x.ClientCD != null));

                            var ClientValue = ClientValueFilter.Where(x => x.ClientCD.Trim().ToUpper().Equals(tbClientCodeFilter.Text.ToString().ToUpper().Trim()));
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                RetVal = true;
                            }
                            else
                            {
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            DoSearchfilter(true);
        }

        #endregion

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewRoster_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["CrewCD"] != null)
                {
                    if (Request.QueryString["CrewCD"].ToString() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }

        protected void dgCrewRoster_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAircraftType);
                }
            }

        }

        protected void dgCrewRoster_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewRoster, Page.Session);
        }

    }
}
