﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewRosterAddInfoCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private List<string> lstCrewRosterAddInfoCodes = new List<string>();
        private ExceptionManager exManager;
        private bool CrewAddInfoPageNavigated = false;
        private string strCrewAddInfoID = "";
        private string strCrewAddInfoCD = "";
        private bool _selectLastModified = false;

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrewRosterAddInfo, dgCrewRosterAddInfo, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCrewRosterAddInfo.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewCrewRosterAddInfoCatalog);
                          //  if (IsPopUp) Session["SelectedCrewInfoID"] = Request.QueryString["CrewInfoId"].ToString();
                            if (Session["SelectedCrewInfoID"] != null)
                            {
                                if (!IsPopUp)
                                    Session.Remove("SelectedCrewInfoID");
                            }
                             // Method to display first record in read only format   
                            if (Session["SelectedCrewInfoID"] != null)
                            {
                                dgCrewRosterAddInfo.Rebind();
                                if(!IsPopUp)
                                    ReadOnlyForm();                                
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }

        }

        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgCrewRosterAddInfo.Rebind();
                    }
                    if (dgCrewRosterAddInfo.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedCrewInfoID"] = null;
                        //}
                        if (Session["SelectedCrewInfoID"] == null)
                        {
                            dgCrewRosterAddInfo.SelectedIndexes.Add(0);
                            Session["SelectedCrewInfoID"] = dgCrewRosterAddInfo.Items[0].GetDataKeyValue("CrewInfoID").ToString();
                        }

                        if (dgCrewRosterAddInfo.SelectedIndexes.Count == 0)
                            dgCrewRosterAddInfo.SelectedIndexes.Add(0);
                        if(!IsPopUp)
                            ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    target.Focus();
                    IsEmptyCheck = false;
                }

            }
        }

        /// <summary>
        /// To check unique Code  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfoCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            checkAllReadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }

        }

        /// <summary>
        /// To check whether the Crew already exists 
        /// </summary>
        /// <returns></returns>
        private bool checkAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objCrewRostersvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objCrewRostersvc.GetCrewRosterAddInfoList().EntityList.Where(x => x.CrewInfoCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                        if (objRetVal.Count() > 0 && objRetVal != null)
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                            returnVal = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }
        /// <summary>
        /// Crew Roster Additional Info Item created Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }
        }

        /// <summary>
        /// Crew Roster Additional Info Data Bind event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCrewRosterAddInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (objCrewRosterAddInfoService.GetCrewRosterAddInfoList().ReturnFlag == true)
                            {
                                dgCrewRosterAddInfo.DataSource = objCrewRosterAddInfoService.GetCrewRosterAddInfoList().EntityList;
                            }
                            Session.Remove("CrewRosterAddInfoCodes");
                            lstCrewRosterAddInfoCodes = new List<string>();
                            foreach (CrewInformation crewRosterAddInfoEntity in objCrewRosterAddInfoService.GetCrewRosterAddInfoList().EntityList)
                            {
                                lstCrewRosterAddInfoCodes.Add(crewRosterAddInfoEntity.CrewInfoCD.ToLower().ToString().Trim());

                            }
                        }
                        Session["CrewRosterAddInfoCodes"] = lstCrewRosterAddInfoCodes;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }
        }


        private void SelectItem()
        {
            if (Session["SelectedCrewInfoID"] != null)
            {
                string ID = Session["SelectedCrewInfoID"].ToString();
                foreach (GridDataItem Item in dgCrewRosterAddInfo.MasterTableView.Items)
                {
                    if (Item["CrewInfoID"].Text.Trim() == ID)
                    {
                        Item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                DefaultSelection(false);
            }
        }

        /// <summary>
        /// Crew Roster Additional Info Item command Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedCrewInfoID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CrewInformation, Convert.ToInt64(Session["SelectedCrewInfoID"].ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewInformation);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        SelectItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgCrewRosterAddInfo.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                tbCode.Focus();
                                break;
                            case "Filter":
                                foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }
        }

        /// <summary>
        /// Crew Roster Additional Info Update Event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewRosterAddInfo_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedCrewInfoID"] != null)
                        {
                            e.Canceled = true;
                            using (FlightPakMasterService.MasterCatalogServiceClient objCrewRosterAddInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (hdnValue.Value == "No")
                                {
                                    var objRetVal = objCrewRosterAddInfoService.UpdateCrewRosterAddInfo(GetItems());
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.CrewInformation, Convert.ToInt64(Session["SelectedCrewInfoID"].ToString().Trim()));
                                            GridEnable(true, true, true);
                                            SelectItem();
                                            ReadOnlyForm();
                                            //DefaultSelection(true);
                                            _selectLastModified = true;
                                        }
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewInformation);
                                    }
                                }
                                else
                                {
                                    var objRetVal = objCrewRosterAddInfoService.UpdateforAllCrewMembers(GetItems());
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();
                                        _selectLastModified = true;
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.CrewInformation, Convert.ToInt64(Session["SelectedCrewInfoID"].ToString().Trim()));
                                            GridEnable(true, true, true);
                                            SelectItem();
                                            ReadOnlyForm();
                                            //DefaultSelection(true);
                                        }
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewInformation);
                                    }
                                    // objCrewRosterAddInfoService.updatefo(GetItems());
                                }
                                if (IsPopUp)
                                {
                                    //Clear session & close browser
                                    Session.Remove("SelectedCrewInfoID");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow();oWnd.close(); ", true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }
        }



        /// <summary>
        /// Crew Roster Additional Info Insert Command
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (checkAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                if (hdnValue.Value == "No")
                                {
                                    var objRetVal = objService.AddCrewRosterAddInfo(GetItems());
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                    DefaultSelection(false);
                                    dgCrewRosterAddInfo.Rebind();
                                    
                                        ShowSuccessMessage();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewInformation);
                                    }
                                }
                                else
                                {
                                    var objRetVal = objService.AddforAllCrewMembers(GetItems());
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        DefaultSelection(false);
                                        dgCrewRosterAddInfo.Rebind();

                                        ShowSuccessMessage();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewInformation);
                                    }
                                }

                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            //Session.Remove("SelectedCrewInfoID");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow();oWnd.close(); ", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }
        }

        /// <summary>
        /// Crew Roster Additional Info delete command
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewRosterAddInfo_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedCrewInfoID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CrewInformationService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.CrewInformation CrewInformation = new FlightPakMasterService.CrewInformation();
                                string Code = Session["SelectedCrewInfoID"].ToString();
                                strCrewAddInfoCD = "";
                                strCrewAddInfoID = "";
                                foreach (GridDataItem Item in dgCrewRosterAddInfo.MasterTableView.Items)
                                {
                                    if (Item["CrewInfoID"].Text.Trim() == Code.Trim())
                                    {
                                        strCrewAddInfoCD = Item["CrewInfoCD"].Text.Trim();
                                        strCrewAddInfoID = Item["CrewInfoID"].Text.Trim();
                                        CrewInformation.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                        break;
                                    }
                                }
                                CrewInformation.CrewInfoCD = strCrewAddInfoCD;
                                CrewInformation.CrewInfoID = Convert.ToInt64(strCrewAddInfoID);
                                CrewInformation.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.CrewInformation, Convert.ToInt64(Session["SelectedCrewInfoID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewInformation);
                                        return;
                                    }
                                    CrewInformationService.DeleteCrewRosterAddInfo(CrewInformation);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgCrewRosterAddInfo.Rebind();
                                    DefaultSelection(false);
                                    GridEnable(true, true, true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.CrewInformation, Convert.ToInt64(Session["SelectedCrewInfoID"].ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Crew Roster Additional Info Item selected changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewRosterAddInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem Item = dgCrewRosterAddInfo.SelectedItems[0] as GridDataItem;
                                Session["SelectedCrewInfoID"] = Item["CrewInfoID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                    }
                }
            }
        }

        /// <summary>
        /// Method to insert the form Values
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Crew Roster Additional Info Save changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgCrewRosterAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgCrewRosterAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }

        }



        protected void Yes_Click(object sender, EventArgs e)
        {



        }

        protected void No_Click(object sender, EventArgs e)
        {



        }


        /// <summary>
        /// Crew Roster Additional Info Cancel event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            //Session["SelectedItem"] = null;
            //DefaultSelection();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedCrewInfoID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.CrewInformation, Convert.ToInt64(Session["SelectedCrewInfoID"].ToString().Trim()));
                                    DefaultSelection(false);
                                }
                            }
                        }
                        DefaultSelection(false);
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            //Session.Remove("SelectedCrewInfoID");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow();oWnd.close(); ", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        /// <summary>
        /// Rad Ajax creating event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCrewRosterAddInfo;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }
        }

        /// <summary>
        /// Crew Roster Additional Info Get items method
        /// </summary>
        /// <param name="PassengerAdditionalInfoCode"></param>
        /// <returns></returns>
        private CrewInformation GetItems()
        {
            FlightPakMasterService.CrewInformation objCrewRosterAddInfo = new FlightPakMasterService.CrewInformation();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (hdnSave.Value == "Update")
                    {
                        if (Session["SelectedCrewInfoID"] != null)
                        {
                            objCrewRosterAddInfo.CrewInfoID = Convert.ToInt64(Session["SelectedCrewInfoID"].ToString());
                        }
                    }
                    objCrewRosterAddInfo.CrewInfoCD = tbCode.Text.Trim();
                    objCrewRosterAddInfo.CrewInformationDescription = tbDescription.Text.Trim();
                    objCrewRosterAddInfo.IsDeleted = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            return objCrewRosterAddInfo;
        }

        /// <summary>
        /// Crew Roster Additional Info Edit form 
        /// </summary>s
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    tbCode.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Grid enable event
        /// </summary>
        /// <param name="add"></param>
        /// <param name="edit"></param>
        /// <param name="delete"></param>



        private void GridEnable(bool Add, bool Edit, bool Delete)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgCrewRosterAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDelCtl = (LinkButton)dgCrewRosterAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgCrewRosterAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddCrewAdditionalInformation))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteCrewAdditionalInformation))
                    {
                        lbtnDelCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDelCtl.Enabled = true;
                            lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete('This Will Delete The Code From All Associated Crew Members. Continue Delete?');";
                        }
                        else
                        {
                            lbtnDelCtl.Enabled = false;
                            lbtnDelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditCrewAdditionalInformation))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
               
                //if (Request.QueryString["CrewID"] != null)
                //{
                //    Session["SelectedCrewRosterID"] = Request.QueryString["CrewID"].Trim();
                //}
            }
        }

        protected void CrewAddInfo_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CrewAddInfoPageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewRosterAddInfo, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }
        }

        protected void CrewAddInfo_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCrewRosterAddInfo.ClientSettings.Scrolling.ScrollTop = "0";
                        CrewAddInfoPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }
        }

        /// <summary>
        /// Read only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    GridDataItem Item = dgCrewRosterAddInfo.SelectedItems[0] as GridDataItem;
                    Label lbLastUpdatedUser = (Label)dgCrewRosterAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text =System.Web.HttpUtility.HtmlEncode( "Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Clear form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Enable form
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbDescription.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }





        /// <summary>
        /// To assign the data to controls 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewInfoID"] != null)
                    {
                        strCrewAddInfoCD = "";
                        foreach (GridDataItem Item in dgCrewRosterAddInfo.MasterTableView.Items)
                        {
                            if (Item["CrewInfoID"].Text.Trim() == Session["SelectedCrewInfoID"].ToString().Trim())
                            {

                                tbCode.Text = Item.GetDataKeyValue("CrewInfoCD").ToString();
                                if (Item.GetDataKeyValue("CrewInformationDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("CrewInformationDescription").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                Item.Selected = true;

                                lbColumnName1.Text = "Additional Info Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["CrewInfoCD"].Text;
                                lbColumnValue2.Text = Item["CrewInformationDescription"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCrewRosterAddInfo.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var CrewInfoValue = FPKMstService.GetCrewRosterAddInfoList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CrewInfoValue);
            List<FlightPakMasterService.CrewInformation> filteredList = GetFilteredList(CrewInfoValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CrewInfoID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCrewRosterAddInfo.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCrewRosterAddInfo.CurrentPageIndex = PageNumber;
            dgCrewRosterAddInfo.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfCrewInformation CrewInfoValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedCrewInfoID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CrewInfoValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().CrewInfoID;
                Session["SelectedCrewInfoID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.CrewInformation> GetFilteredList(ReturnValueOfCrewInformation CrewInfoValue)
        {
            List<FlightPakMasterService.CrewInformation> filteredList = new List<FlightPakMasterService.CrewInformation>();

            if (CrewInfoValue.ReturnFlag)
            {
                filteredList = CrewInfoValue.EntityList;
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = false;
                                dgCrewRosterAddInfo.Visible = false;
                                if (IsAdd)
                                {
                                    (dgCrewRosterAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    Session["SelectedCrewInfoID"] = Request.QueryString["CrewInfoId"].ToString();
                                    SelectItem();
                                    (dgCrewRosterAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCrewRosterAddInfo.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgCrewRosterAddInfo.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}







