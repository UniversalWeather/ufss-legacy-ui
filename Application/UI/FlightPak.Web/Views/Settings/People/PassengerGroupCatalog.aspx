﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="PassengerGroupCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.PassengerGroupCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
     <style type="text/css">
        span#ctl00_ctl00_MainContent_SettingBodyContent_tbAvailableFilter_wrapper{width:326px !important;margin-bottom:10px;}
        .RadListBox span.rlbText em
{
    background-color: #E5E5E5;
    font-weight: bold;
    font-style: normal;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Passenger Group</span> <span class="tab-nav-icons">
                        <!--<a href="#"
                        class="search-icon"></a><a href="#" class="save-icon"></a><a href="#" class="print-icon">
                        </a>-->
                        <a href="../../Help/ViewHelp.aspx?Screen=PassengerGroupHelp" target="_blank" class="help-icon"
                            title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <div class="divGridPanel">
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgPaxGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgPaxGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgPaxGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgPaxGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgPaxGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgPaxGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgPaxGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbCode">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbHomeBase">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="tbHomeBase" />
                        <telerik:AjaxUpdatedControl ControlID="hdnHomeBase" />
                        <telerik:AjaxUpdatedControl ControlID="hdnHomeBaseID" />
                        <telerik:AjaxUpdatedControl ControlID="cvHomeBase" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnNext">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnLast">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnFirst">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnPrevious">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">

                function openWin() {
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    var oWnd = oManager.open("../Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=tbHomeBase.ClientID%>").value, "radCompanyMasterPopup");
                }

                function ConfirmClose(WinName) {
                    var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                    var oWnd = oManager.GetWindowByName(WinName);
                    //Find the Close button on the page and attach to the 
                    //onclick event
                    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                    CloseButton.onclick = function () {
                        CurrentWinName = oWnd.Id;
                        //radconfirm is non-blocking, so you will need to provide a callback function
                        oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                    }
                }


                function OnClientClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg) {
                        if (arg.HomeBase != "" && arg.HomeBase != "undefined") {
                            document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.HomeBase;
                            document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnHomeBase.ClientID%>").value = arg.HomebaseID;
                            document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.HomebaseID;
                        }
                        else {
                            document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                            document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnHomeBase.ClientID%>").value = "";
                            document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                        }
                    }
                }
                function GetDimensions(sender, args) {
                    var bounds = sender.getWindowBounds();
                    return;
                }
                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }
                
                $(document).ready(function UpdateConfirm() {
                    $("a").click(function (event) {
                        var redirectUrl = $(this).attr("href");
                        if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                            var msg = "Do you want to save changes to the current record?";
                            document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                            var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                            oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                            //for blocking redirection
                            return false;
                        }
                    });
                });
                
                function UpdateConfirmCallBackFn(arg) {
                    if (arg === 1) {
                        document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                    }
                    else if (arg === 2) {
                        document.getElementById('<%=btnCancel.ClientID%>').click();
                    }
                    //for blocking redirection
                    return false;
                }

                function prepareSearchInput(input) { input.value = input.value; }

                function filterList() {
                    var listbox = $find("<%= lstAvailable.ClientID %>");
                    var textbox = $find('<%= tbAvailableFilter.ClientID %>');

                    clearListEmphasis(listbox);
                    createMatchingList(listbox, textbox.get_textBoxValue());
                }
                // Remove emphasis from matching text in ListBox
                function clearListEmphasis(listbox) {
                    var re = new RegExp("</{0,1}em>", "gi");
                    var items = listbox.get_items();
                    var itemText;

                    items.forEach
                    (
                        function (item) {
                            itemText = item.get_text();
                            item.set_text(itemText.replace(re, ""));
                        }
                    )
                }
                // Emphasize matching text in ListBox and hide non-matching items
                function createMatchingList(listbox, filterText) {
                    if (filterText != "") {
                        filterText = escapeRegExCharacters(filterText);

                        var items = listbox.get_items();
                        var re = new RegExp(filterText, "i");

                        items.forEach
                        (
                            function (item) {
                                var itemText = item.get_text();

                                if (itemText.match(re)) {
                                    item.set_text(itemText.replace(re, "<em>" + itemText.match(re) + "</em>"));
                                    item.set_visible(true);
                                }
                                else {
                                    item.set_visible(false);
                                }
                            }
                        )
                    }
                    else {
                        var items = listbox.get_items();

                        items.forEach
                        (
                            function (item) {
                                item.set_visible(true);
                            }
                        )
                    }
                }
                // Escapes RegEx character classes and shorthand characters
                function escapeRegExCharacters(text) {
                    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
                }
                function lstAvailable_OnClientTransferring(sender, eventArgs) {
                    // Transferred items retain the emphasized text, so it needs to be cleared.
                    clearListEmphasis(sender);
                    // Clear the list. Optional, but prevents follow up situation.
                    clearFilterText();
                    createMatchingList(sender, "");
                }


                // Clears the text from the filter.
                function clearFilterText() {
                    var textbox = $find('<%= tbAvailableFilter.ClientID %>');
                    textbox.clear();
                }
            </script>
        </telerik:RadCodeBlock>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        
        <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                        <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
        </telerik:RadWindowManager>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                    <tr>
                        <td>
                            <div class="status-list">
                                <span>
                                    <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                        AutoPostBack="true" /></span>
                            </div>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="dgPaxGroup" runat="server" AllowSorting="true" OnItemCreated="dgPaxGroup_ItemCreated"
                    OnNeedDataSource="dgPaxGroup_BindData" OnItemCommand="dgPaxGroup_ItemCommand"
                    OnUpdateCommand="dgPaxGroup_UpdateCommand" OnInsertCommand="dgPaxGroup_InsertCommand"
                    OnDeleteCommand="dgPaxGroup_DeleteCommand" OnPreRender="dgPaxGroup_PreRender"
                    OnPageIndexChanged="dgPaxGroup_PageIndexChanged" AutoGenerateColumns="false"
                    PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgPaxGroup_SelectedIndexChanged"
                    AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="341px">
                    <MasterTableView DataKeyNames="PassengerGroupID,CustomerID,HomebaseID,PassengerGroupCD,PassengerGroupName,LastUpdUID,LastUpdTS,IsDeleted,HomeBaseCD,BaseDescription,IsInActive"
                        CommandItemDisplay="Bottom">
                        <Columns>
                            <telerik:GridBoundColumn DataField="PassengerGroupID" HeaderText="PAX GroupID" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="EqualTo" Display="false" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PassengerGroupCD" HeaderText="PAX Group Code"
                                AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500"
                                HeaderStyle-Width="120px" FilterControlWidth="100px">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PassengerGroupName" HeaderText="Description" FilterDelay="500"
                                AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                HeaderStyle-Width="460px" FilterControlWidth="440px">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px" FilterControlWidth="80px"
                                FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                                ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" HeaderStyle-Width="80px">
                            </telerik:GridCheckBoxColumn>
                        </Columns>
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px; float: left; clear: both;">
                                <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                    Visible='<%# IsAuthorized(Permission.Database.AddPassengerGroupCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                    ToolTip="Edit" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditPassengerGroupCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                                    runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeletePassengerGroupCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                            </div>
                            <div>
                                <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                            </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings CaseSensitive="false" />
                </telerik:RadGrid>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="tdLabel160">
                            <div id="tdSuccessMessage" class="success_msg">
                                Record saved successfully.</div>
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table class="border-box">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkPaxInactive" runat="server" Text="Inactive" Font-Size="12px"
                                            ForeColor="Black" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="tdLabel150">
                            <span class="mnd_text">Passenger Group Code</span>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbCode" runat="server" MaxLength="4" CssClass="text80" OnTextChanged="Code_TextChanged"
                                            AutoPostBack="true" ValidationGroup="Save" TabIndex="11"></asp:TextBox>
                                        <asp:HiddenField ID="hdnPassengerGroupID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="Save" ControlToValidate="tbCode"
                                            Text="Code is Required." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Code is Required"
                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <span class="mnd_text">Description</span>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbDescription" runat="server" MaxLength="30" CssClass="text180"
                                            ValidationGroup="Save" TabIndex="12"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="Save"
                                            Text="Description is Required." ControlToValidate="tbDescription" Display="Dynamic"
                                            CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Home Base
                        </td>
                        <td align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" CssClass="text80" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            onBlur="return RemoveSpecialChars(this)" OnTextChanged="HomeBase_TextChanged"
                                            AutoPostBack="true" TabIndex="13"></asp:TextBox>
                                        <asp:HiddenField ID="hdnHomeBase" runat="server" />
                                        <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnHomeBase" runat="server" OnClientClick="javascript:openWin();return false;"
                                            CssClass="browse-button" TabIndex="13" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                            ErrorMessage="Invalid Home Base Code." Display="Dynamic" CssClass="alert-text"
                                            ValidationGroup="Save"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Label ID="lbOrderDetails" runat="server" Text="Order Details" />
                        </td>
                        <td align="left">
                            <table cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxAvailable" runat="server" Text="Available:" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbPaxSelected" runat="server" Text="Selected:" />
                                    </td>
                                </tr>
                                 <tr>
                                            <td>
                                                <telerik:RadTextBox ID="tbAvailableFilter" runat="server" 
                    EmptyMessage="Search Pax..."
                    autocomplete="off"
                    onkeyup="filterList();" />
                                            </td>
                                        </tr>
                                <tr>
                                    <td>
                                        <telerik:RadListBox ID="lstAvailable" SelectionMode="Multiple" runat="server" Width="356px"
                                            AllowReorder="false" EnableDragAndDrop="true" AllowTransferOnDoubleClick="true"
                                            AllowTransfer="true" TransferMode="Move" AllowTransferDuplicates="false" Height="200px" OnClientTransferring="lstAvailable_OnClientTransferring"
                                            TabIndex="14" TransferToID="lstSelected">
                                            <ButtonSettings ShowDelete="false" ShowReorder="false" />
                                        </telerik:RadListBox>
                                    </td>
                                    <td valign="middle">
                                        <%-- <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnNext" runat="server" CssClass="icon-next" OnClick="btnNext_click"
                                                        ToolTip="Add Selected items" TabIndex="15" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnLast" runat="server" CssClass="icon-last" OnClick="btnLast_click"
                                                        ToolTip="Add All Items" TabIndex="16" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnFirst" runat="server" CssClass="icon-first" OnClick="btnFirst_click"
                                                        ToolTip="Remove All Items" TabIndex="17" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnPrevious" runat="server" CssClass="icon-prev" OnClick="btnPrevious_click"
                                                        ToolTip="Remove Selected Items" TabIndex="18" />
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </td>
                                    <td>
                                        <telerik:RadListBox ID="lstSelected" runat="server" Width="275px" Height="200px"
                                            AllowReorder="true" EnableDragAndDrop="true" AllowTransferOnDoubleClick="true"
                                            TransferToID="lstAvailable" AllowTransfer="true" TransferMode="Move" AllowTransferDuplicates="false"
                                            TabIndex="19">
                                            <ButtonSettings ShowDelete="false" ShowReorder="false" ShowTransfer="false" ShowTransferAll="false" />
                                        </telerik:RadListBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                                CssClass="button" ValidationGroup="Save" TabIndex="20" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                                OnClick="Cancel_Click" CssClass="button" TabIndex="21" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                            <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                        </td>
                    </tr>
                </table>
                <div id="toolbar" class="scr_esp_down_db">
                    <div class="floating_main_db">
                        <div class="floating_bar_db">
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                            <span class="padright_20">
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                            <span>
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
                <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
