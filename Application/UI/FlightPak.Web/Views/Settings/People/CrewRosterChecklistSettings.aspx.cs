﻿ using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewRosterChecklistSettiongs : BaseSecuredPage
    {
        string CrewCodeValue = "";
        private ExceptionManager exManager;
        Int64 CrewID;
        string CrewChecklistCodeValue = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            lblMessage.Text = string.Empty;
                        }
                        CrewCodeValue = (string)(Session["SelectedCrewRosterID"]);
                        CrewChecklistCodeValue = (string)(Session["CrewChecklistCD"]);
                        hdnChecklistCode.Value = (string)(Session["CrewChecklistCD"]);
                        hdnCrewCode.Value = (string)(Session["SelectedCrewRosterCD"]);
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Bind Crew Check list Data, Static
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChecklistSettings_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Description", typeof(string));

                        dt.Rows.Add("Original Date");
                        dt.Rows.Add("Chg. Base Date");
                        dt.Rows.Add("Freq.");
                        dt.Rows.Add("Previous");
                        dt.Rows.Add("Due Next");
                        dt.Rows.Add("Alert Date");
                        dt.Rows.Add("Alert Days");
                        dt.Rows.Add("Grace Date");
                        dt.Rows.Add("Grace Days");
                        dt.Rows.Add("Aircraft Type");
                        dt.Rows.Add("Total Req. Flight Hrs.");
                        dgChecklistSettings.DataSource = dt;
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Bind Crew Roster Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MasterCatalogServiceClient Service = new MasterCatalogServiceClient();
                        var objCrew = Service.GetCrewList();
                        if (objCrew.ReturnFlag == true)
                        {
                            dgCrewRoster.DataSource = objCrew.EntityList;
                        }
                        foreach (GetAllCrew CrewVal in objCrew.EntityList)
                        {
                            if (CrewVal.CrewCD == CrewCodeValue)
                            {
                                Session["CrewID"] = CrewVal.CrewID;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to assign the selected 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OK_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int EntityIndex = -1;
                        int OldEntityIndex = -1;
                        List<string> SelectedChecklistCode = new List<string>();
                        foreach (GridDataItem Item in dgChecklistSettings.MasterTableView.Items)
                        {
                            if (Item.Selected == true)
                            {
                                SelectedChecklistCode.Add(Item.GetDataKeyValue("Description").ToString().Trim());
                            }
                        }

                        List<Int64> SelectedCrewCode = new List<Int64>();
                        foreach (GridDataItem Item in dgCrewRoster.MasterTableView.Items)
                        {
                            if (Item.Selected == true)
                            {
                                SelectedCrewCode.Add(Convert.ToInt64(Item.GetDataKeyValue("CrewID").ToString().Trim()));
                            }
                            //if (((CheckBox)Item["Choice"].FindControl("chkChoice")).Checked)
                            //{
                            //    SelectedCrewCode.Add(Convert.ToInt64(Item.GetDataKeyValue("CrewID").ToString().Trim()));
                            //}
                        }

                        int x = SelectedCrewCode.Count();


                        using (FlightPakMasterService.MasterCatalogServiceClient ServiceExisting = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<GetCrewCheckListDate> lstOldCrewChecklistDate = new List<GetCrewCheckListDate>();
                            GetCrewCheckListDate objOldCrewChecklistDate = new GetCrewCheckListDate();

                            objOldCrewChecklistDate.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                            objOldCrewChecklistDate.CheckListCD = CrewChecklistCodeValue; // doesn't accept empty value, set as dummy value
                            objOldCrewChecklistDate.CrewChecklistDescription = "null"; // doesn't accept empty value, set as dummy value
                            var objCrewChecklistOldValues = ServiceExisting.GetCrewCheckListDate(objOldCrewChecklistDate);
                            lstOldCrewChecklistDate = objCrewChecklistOldValues.EntityList.ToList();
                            var objCrewChecklistOldValue = (from code in lstOldCrewChecklistDate
                                                            where (code.CheckListCD.Trim().Equals(CrewChecklistCodeValue.Trim()))
                                                            select code);

                            for (int y = 0; y < objCrewChecklistOldValues.EntityList.Count(); y++)
                            {
                                if (objCrewChecklistOldValues.EntityList[y].CheckListCD == CrewChecklistCodeValue)
                                {
                                    OldEntityIndex = y;
                                }
                            }

                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<GetCrewCheckListDate> lstCrewChecklistDate = new List<GetCrewCheckListDate>();
                                GetCrewCheckListDate objCrewChecklistDate = new GetCrewCheckListDate();

                                if (OldEntityIndex > -1)
                                {
                                    for (int CrewCode = 0; CrewCode < SelectedCrewCode.Count(); CrewCode++)
                                    {
                                        EntityIndex = -1;
                                        objCrewChecklistDate.CrewID = SelectedCrewCode[CrewCode];
                                        objCrewChecklistDate.CheckListCD = CrewChecklistCodeValue; // doesn't accept empty value, set as dummy value
                                        objCrewChecklistDate.CrewChecklistDescription = "null";
                                        var objCrewChecklistValues = Service.GetCrewCheckListDate(objCrewChecklistDate);
                                        if (objCrewChecklistValues.ReturnFlag == true)
                                        {
                                            lstCrewChecklistDate = objCrewChecklistValues.EntityList.ToList();
                                            for (int y = 0; y < objCrewChecklistValues.EntityList.Count(); y++)
                                            {
                                                if (objCrewChecklistValues.EntityList[y].CheckListCD == CrewChecklistCodeValue)
                                                {
                                                    EntityIndex = y;
                                                }
                                            }
                                            //lstCrewChecklistDate = objCrewChecklistValues.EntityList.ToList();
                                        }

                                        if (EntityIndex > -1)
                                        {
                                            CrewCheckListDetail CrewChecklistDate = new CrewCheckListDetail();
                                            CrewChecklistDate.CrewID = SelectedCrewCode[CrewCode];
                                            CrewChecklistDate.CheckListCD = objCrewChecklistValues.EntityList[EntityIndex].CheckListCD.Trim();
                                            CrewChecklistDate.CheckListID = objCrewChecklistValues.EntityList[EntityIndex].CheckListID;
                                            if (SelectedChecklistCode.Contains("Previous"))
                                            {
                                                CrewChecklistDate.PreviousCheckDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].PreviousCheckDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.PreviousCheckDT = objCrewChecklistValues.EntityList[EntityIndex].PreviousCheckDT;
                                            }
                                            if (SelectedChecklistCode.Contains("Due Next"))
                                            {
                                                CrewChecklistDate.DueDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].DueDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.DueDT = objCrewChecklistValues.EntityList[EntityIndex].DueDT;
                                            }
                                            if (SelectedChecklistCode.Contains("Alert Date"))
                                            {
                                                CrewChecklistDate.AlertDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].AlertDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.AlertDT = objCrewChecklistValues.EntityList[EntityIndex].AlertDT;
                                            }
                                            if (SelectedChecklistCode.Contains("Grace Date"))
                                            {
                                                CrewChecklistDate.GraceDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].GraceDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.GraceDT = objCrewChecklistValues.EntityList[EntityIndex].GraceDT;
                                            }
                                            if (SelectedChecklistCode.Contains("Grace Days"))
                                            {
                                                CrewChecklistDate.GraceDays = objCrewChecklistOldValues.EntityList[OldEntityIndex].GraceDays;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.GraceDays = objCrewChecklistValues.EntityList[EntityIndex].GraceDays;
                                            }
                                            if (SelectedChecklistCode.Contains("Alert Days"))
                                            {
                                                CrewChecklistDate.AlertDays = objCrewChecklistOldValues.EntityList[OldEntityIndex].AlertDays;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.AlertDays = objCrewChecklistValues.EntityList[EntityIndex].AlertDays;
                                            }

                                            if (SelectedChecklistCode.Contains("Freq."))
                                            {
                                                CrewChecklistDate.FrequencyMonth = objCrewChecklistOldValues.EntityList[OldEntityIndex].FrequencyMonth;
                                                CrewChecklistDate.Frequency = objCrewChecklistOldValues.EntityList[OldEntityIndex].Frequency;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.FrequencyMonth = objCrewChecklistValues.EntityList[EntityIndex].FrequencyMonth;
                                                CrewChecklistDate.Frequency = objCrewChecklistOldValues.EntityList[EntityIndex].Frequency;
                                            }
                                            if (SelectedChecklistCode.Contains("Aircraft Type"))
                                            {
                                                CrewChecklistDate.AircraftID = objCrewChecklistOldValues.EntityList[OldEntityIndex].AircraftID;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.AircraftID = objCrewChecklistValues.EntityList[EntityIndex].AircraftID;
                                            }
                                            if (SelectedChecklistCode.Contains("Original Date"))
                                            {
                                                CrewChecklistDate.OriginalDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].OriginalDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.OriginalDT = objCrewChecklistValues.EntityList[EntityIndex].OriginalDT;
                                            }
                                            if (SelectedChecklistCode.Contains("Chg. Base Date"))
                                            {
                                                CrewChecklistDate.BaseMonthDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].BaseMonthDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.BaseMonthDT = objCrewChecklistValues.EntityList[EntityIndex].BaseMonthDT;
                                            }
                                            CrewChecklistDate.IsDeleted = false;

                                            CrewChecklistDate.IsPassedDueAlert = false;
                                            CrewChecklistDate.IsPilotInCommandFAR91 = false;
                                            CrewChecklistDate.IsPilotInCommandFAR135 = false;
                                            CrewChecklistDate.IsSecondInCommandFAR91 = false;
                                            CrewChecklistDate.IsSecondInCommandFAR135 = false;
                                            CrewChecklistDate.IsInActive = false;
                                            CrewChecklistDate.IsMonthEnd = false;
                                            CrewChecklistDate.IsStopCALC = false;
                                            CrewChecklistDate.IsOneTimeEvent = false;
                                            CrewChecklistDate.IsCompleted = false;
                                            CrewChecklistDate.IsNoConflictEvent = false;
                                            CrewChecklistDate.IsNoCrewCheckListREPTt = false;
                                            CrewChecklistDate.IsNoChecklistREPT = false;
                                            CrewChecklistDate.IsPrintStatus = false;
                                            CrewChecklistDate.IsNextMonth = false;
                                            CrewChecklistDate.IsEndCalendarYear = false;

                                            Service.UpdateCrewCheckListDate(CrewChecklistDate);
                                            //Service.AddCrewCheckListDate(CrewChecklistDate);
                                        }
                                    }
                                }
                            }
                            InjectScript.Visible = true;
                            InjectScript.Text = "<script type='text/javascript'>Close()</" + "script>";
                            //Session["SelectedCrewRosterID"] = null;
                            Session.Remove("CrewChecklistCD");
                        }
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
    }
}