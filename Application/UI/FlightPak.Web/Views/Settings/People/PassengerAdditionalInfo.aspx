﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="PassengerAdditionalInfo.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.PassengerAdditionalInfo"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">

        function Clicking(sender, args) {
           
            var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
               
                if (shouldSubmit) {
                    document.getElementById('<%=hdnValue.ClientID%>').value = "Yes";
                    this.click();
                }
                else {
                    document.getElementById('<%=hdnValue.ClientID%>').value = "No";
                    this.click();
                }
            });
            if (!IsNullOrEmpty(document.getElementById('<%=tbDescription.ClientID%>').value) && !IsNullOrEmpty(document.getElementById('<%=tbDescription.ClientID%>').value)) {
                var text = "Add New Additional Information / Checklist Code To All Passengers?";

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.radconfirm(text, callBackFunction, 300, 100, null, "Flight Scheduling Software");
                args.set_cancel(true);
            }
            else {
                return false;
            }
                    
        }


        function CheckTxtBox(state) {
            ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'), state);
        }
        function openReport() {

            url = "../../Reports/ExportReportInformation.aspx?Report=RptDBPaxAdditionalInfoExport&UserCD=UC";
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open(url, "RadExportData");
        }
        
        function GetDimensions(sender, args) {
            var bounds = sender.getWindowBounds();
            //alert(new String("Width:" + bounds.width + " " + "Height: " + bounds.height));
            return;
        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
        function ShowReports(radWin, ReportFormat, ReportName) {
            document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
            document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
            if (ReportFormat == "EXPORT") {
                url = "../../Reports/ExportReportInformation.aspx";
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, 'RadExportData');
                return true;
            }
            else {
                return true;
            }
        }
        function OnClientCloseExportReport(oWnd, args) {
            document.getElementById("<%=btnShowReports.ClientID%>").click();
        }
    </script>
    <script type="text/javascript">
        function CheckTxtBox(control) {
            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);
                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);
                } return false;
            }
        }
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>

    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Passenger Additional Info</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBPaxAdditionalInfoExport');"
                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBPaxAdditionalInfoExport');return false;"
                            OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                        <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                            Style="display: none;" />
                        <a href="../../Help/ViewHelp.aspx?Screen=PassengerAddInfoHelp"
                            target="_blank" title="Help" class="help-icon"></a>
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                    </span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgPassengerAdditionalInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgPassengerAdditionalInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgPassengerAdditionalInfo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgPassengerAdditionalInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExportReport" Title="Export Report Information" KeepInScreenBounds="true"
                AutoSize="false" Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadGrid ID="dgPassengerAdditionalInfo" runat="server" OnItemCreated="dgPassengerAdditionalInfo_ItemCreated" CssClass="passenger-additionalInfo"
        Visible="true" OnNeedDataSource="dgPassengerAdditionalInfo_BindData" OnItemCommand="dgPassengerAdditionalInfo_ItemCommand"
        OnUpdateCommand="dgPassengerAdditionalInfo_UpdateCommand" OnInsertCommand="dgPassengerAdditionalInfo_InsertCommand"
        OnDeleteCommand="dgPassengerAdditionalInfo_DeleteCommand" OnSelectedIndexChanged="dgPassengerAdditionalInfo_SelectedIndexChanged"
        OnPreRender="dgPassengerAdditionalInfo_PreRender" OnPageIndexChanged="dgPassengerAdditionalInfo_PageIndexChanged"
        Height="341px">
        <MasterTableView DataKeyNames="PassengerInformationID,CustomerID,ClientID,PassengerInfoCD,PassengerDescription,LastUpdUID,LastUpdTS,IsShowOnTrip,IsDeleted,IsCheckList"
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="PassengerInfoCD" HeaderText="Additional Info Code"
                    HeaderStyle-Width="100px" FilterControlWidth="80px" CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PassengerDescription" HeaderText="Description" HeaderStyle-Width="580px"
                    FilterControlWidth="560px" CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false" FilterDelay="500"
                    ShowFilterIcon="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PassengerInformationID" HeaderText="PassengerInformationID"
                    CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterDelay="500"
                    Display="false">
                </telerik:GridBoundColumn>
                 <telerik:GridCheckBoxColumn DataField="IsCheckList" HeaderText="Checklist" AutoPostBackOnFilter="true"
                            AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                            HeaderStyle-Width="80px">
                        </telerik:GridCheckBoxColumn>
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                        Visible='<%# IsAuthorized(Permission.Database.AddPassengerAdditionalInfo)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        ToolTip="Edit" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditPassengerAdditionalInfo)%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete('This will delete the code from all associated PAX members. Continue Delete?');"
                        runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeletePassengerAdditionalInfo)%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                </div>
                <div>
                    <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
    </telerik:RadGrid>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel160" valign="top">
                                    <asp:CheckBox runat="server" ID="chkShowinTripSheetReport" Text="Show in Tripsheet Report" />
                                </td>
                                 <td class="tdLabel160" valign="top">
                                    <asp:CheckBox runat="server" ID="chkIsCheckList" Text="Checklist" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    <span class="mnd_text">Additional Info Code</span>
                                </td>
                                <td class="tdLabel180" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCode" runat="server" MaxLength="3" CssClass="text40" OnTextChanged="PassengerAddInfoCode_TextChanged"
                                                    AutoPostBack="true"></asp:TextBox>
                                                <asp:HiddenField ID="hdnPassengerAdditionalInfoId" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="Save" ControlToValidate="tbCode"
                                                    Text="Additional Info Code is Required." Display="Dynamic" CssClass="alert-text"
                                                    SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Additional Info Code is Required"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel100" valign="top">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" runat="server" MaxLength="25" CssClass="text180"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="Save"
                                                    ControlToValidate="tbDescription" Text="Description is Required." Display="Dynamic"
                                                    CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr align="right">
                    <td class="custom_radbutton">
                        <telerik:RadButton ID="btnSaveChanges" runat="server" Text="Save" ValidationGroup="Save"
                            OnClientClicking="Clicking" OnClick="SaveChanges_Click" class="custom_radbutton" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                            OnClick="Cancel_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnValue" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
