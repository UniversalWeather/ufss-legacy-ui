﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="CharterQuoteCustomerCatalog.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.CharterQuoteCustomerCatalog" ClientIDMode="AutoID"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            $(document).ready(function () {
                $('html, body', parent.document).scrollTop($('#RadWindowWrapper_ctl00_ctl00_MainContent_SettingBodyContent_radCQCPopups', parent.document).position().top - 15);
                $('.rwCloseButton').on('click', function () {
                    $('.RadWindow_Metro').eq(0).css({ left: ($(window.parent).width() - $('.RadWindow_Metro').width()) / 2, top: $('#RadWindowWrapper_ctl00_ctl00_MainContent_SettingBodyContent_radCQCPopups', parent.document).position().top+50 })
                })
            })
            function OnClientClick(strPanelToExpand) {

                var PanelBar1 = $find("<%= pnlNotes.ClientID %>");
                var PanelBar2 = $find("<%= pnlMainContact.ClientID %>");
                var PanelBar3 = $find("<%= pnlMaintenance.ClientID %>");
                var PanelBar4 = $find("<%= pnlImage.ClientID %>");
                PanelBar1.get_items().getItem(0).set_expanded(false);
                PanelBar2.get_items().getItem(0).set_expanded(false);
                PanelBar3.get_items().getItem(0).set_expanded(false);
                PanelBar4.get_items().getItem(0).set_expanded(false);
                if (strPanelToExpand == "Notes") {
                    PanelBar1.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "Main Contact") {
                    PanelBar2.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "Image") {
                    PanelBar4.get_items().getItem(0).set_expanded(true);
                }
                return false;
            }

            function openWin(url, value, radWin) {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url + value, radWin);
            }

            function openHomeBase() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/AirportMasterPopup.aspx?IcaoID=" + document.getElementById("<%=tbHomeBase.ClientID%>").value, "radHomeBasePopup");

            }

            function ValidateEmptyTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0.0";
                }
            }
            function openClosestIcao() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/AirportMasterPopup.aspx?IcaoID=" + document.getElementById("<%=tbClosestIcao.ClientID%>").value, "radAirportPopup");

            }
            function openCountry() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CountryMasterPopup.aspx?CountryCD=" + document.getElementById("<%=tbCountry.ClientID%>").value, "RadWindow2");
            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }
            function OnClientCloseHomeBasePopup(oWnd, args) {
                var combo = $find("<%= tbHomeBase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.AirportID;
                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                    }
                }
            }
            function OnClientCloseAirportMasterPopup(oWnd, args) {
                var combo = $find("<%= tbClosestIcao.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvClosestIcao.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnClosestAirportID.ClientID%>").value = arg.AirportID;

                    }
                    else {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = "";
                        document.getElementById("<%=cvClosestIcao.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnClosestAirportID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }

            }
            function OnClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbBillingCountry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbBillingCountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=cvBillingCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnBillingCountryID.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbBillingCountry.ClientID%>").value = "";
                        document.getElementById("<%=cvBillingCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnBillingCountryID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }


            function File_onchange() {
                __doPostBack('__Page', 'LoadImage');
            }
            function CheckName() {
                var nam = document.getElementById('<%=tbImgName.ClientID%>').value;
                if (nam != "") {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = false;
                }
                else {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = true;
                }
            }
            // this function is related to Image 
            function OpenRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
                oWnd.show();
            }
            // this function is related to Image 
            function CloseRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = null;
                oWnd.close();
            }

            function printImage() {
                var image = document.getElementById('<%=ImgPopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlImg = document.getElementById('<%=ddlImg.ClientID%>');
                if (ddlImg.length > 0) {
                    var ImageName = ddlImg.options[ddlImg.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "CharterQuoteCustomer");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }

            //To display plane image at right place

            var isMainInformationExpanded = true;
            var isMainContactInformationExpanded = false;
            var isAttachmentsExpanded = true;
            var isNotesExpanded = false;

            function pnlMaintenanceExpand() {
                isMainInformationExpanded = true;
            }

            function pnlMaintenanceCollapse() {
                isMainInformationExpanded = false;
            }

            function pnlMainContactExpand() {
                isMainContactInformationExpanded = true;
            }

            function pnlMainContactCollapse() {
                isMainContactInformationExpanded = false;
            }

            function pnlImageExpand() {
                isAttachmentsExpanded = true;
            }

            function pnlImageCollapse() {
                isAttachmentsExpanded = false;
            }

            function pnlNotesExpand() {
                isNotesExpanded = true;
            }

            function pnlNotesCollapse() {
                isNotesExpanded = false;
            }

            $(document).ready(function showPlaneImage() {
                $('#ctl00_ctl00_MainContent_SettingBodyContent_chkSearchHomebaseOnly,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_chkSearchActiveOnly').on('change', setPlaneImage);

                $('.rgRow').on('click', function () {
                    var topMargin = $(window).scrollTop() - 30;
                    $('div.raDiv').attr('style', 'margin-top: ' + topMargin + 'px !important');
                });

                $('#ctl00_ctl00_MainContent_SettingBodyContent_dgCQCustomer_ctl00_ctl03_ctl01_lbtnInitInsert,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_dgCQCustomer_ctl00_ctl03_ctl01_lbtnInitEdit,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_dgCQCustomer_ctl00_ctl03_ctl01_lbtnDelete,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnCancel,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnSaveChangesTop,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnCancelTop,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnSaveChanges').on('click', setPlaneImage);
            });

            function setPlaneImage() {
                var scrollTop = $(window).scrollTop();
                var topMargin = 0;

                if (isNotesExpanded) {
                    topMargin -= 0;
                }
                if (isAttachmentsExpanded) {
                    topMargin -= 40;
                }
                if (isMainContactInformationExpanded) {
                    topMargin -= 270;
                    if ($.browser.webkit) {
                        topMargin -= 50;
                    }
                }
                if (isMainInformationExpanded) {
                    topMargin -= 210;
                    if ($.browser.webkit) {
                        topMargin -= 30;
                    }
                }

                topMargin += scrollTop - 80; //-80 is value when all are collapsed
                $('div.raDiv').attr('style', 'margin-top: ' + topMargin + 'px !important');
            }

            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCQCPopup" runat="server" OnClientResizeEnd="GetDimensions"
                NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHomeBasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseHomeBasePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printImage();return false;" /></div>
                                    <asp:Image ID="ImgPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Charter Quote Customer</span><span class="tab-nav-icons">
                        <!--<a href="#" class="search-icon">
                    </a><a href="#" class="save-icon"></a><a href="#" class="print-icon"></a>-->
                        <a href="../../Help/ViewHelp.aspx?Screen=CustomerHelp" class="help-icon" target="_blank"
                            title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0" class="head-sub-menu">
            <tr>
                <td>
                    <div class="fleet_select">
                        <asp:LinkButton ID="lnkCQCustomerContacts" CssClass="fleet_link" runat="server" Text="Contacts"
                            OnClick="lnkCQCustomerContacts_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lbNewCharterRates" CssClass="fleet_link" OnClick="Charterrates_Click"
                            runat="server" Visible="false">Charter Rates</asp:LinkButton></div>
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlFilterForm" runat="server">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchHomebaseOnly" runat="server" Text="Home Base Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span><span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                            AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <telerik:RadGrid ID="dgCQCustomer" runat="server" AllowSorting="true" OnNeedDataSource="dgCQCustomer_BindData"
            OnItemCommand="dgCQCustomer_ItemCommand" OnItemCreated="dgCQCustomer_ItemCreated"
            OnPreRender="dgCQCustomer_PreRender" OnUpdateCommand="dgCQCustomer_UpdateCommand"
            Height="341px" OnInsertCommand="dgCQCustomer_InsertCommand" OnPageIndexChanged="dgCQCustomer_PageIndexChanged"
            OnDeleteCommand="dgCQCustomer_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
            AllowPaging="true" OnSelectedIndexChanged="dgCQCustomer_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" AllowMultiRowSelection="false">
            <MasterTableView ClientDataKeyNames="" DataKeyNames="CQCustomerID,CustomerID,CQCustomerCD,CQCustomerName,IsApplicationFiled,IsApproved,BillingName,BillingAddr1,BillingAddr2,BillingAddr3,BillingCity
                ,BillingState,BillingZip,CountryID,BillingPhoneNum,BillingFaxNum,Notes,Credit,DiscountPercentage,IsInActive,HomebaseID,AirportID,DateAddedDT,WebAddress,EmailID
                ,TollFreePhone,LastUpdUID,LastUpdTS,IsDeleted,CountryCD,CountryName,BaseDescription,HomeBaseIcaoID,AirportIcaoID"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="CQCustomerCD" HeaderText="Code" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="120px" FilterControlWidth="100px" AutoPostBackOnFilter="false" FilterDelay="500"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CQCustomerName" HeaderText="Customer Name" CurrentFilterFunction="StartsWith"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="230px" FilterDelay="500"
                        FilterControlWidth="210px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="BillingName" HeaderText="Contact Name" CurrentFilterFunction="StartsWith"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="230px" FilterDelay="500"
                        FilterControlWidth="210px">
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Active" CurrentFilterFunction="EqualTo"
                    AutoPostBackOnFilter="true" ShowFilterIcon="false">
                </telerik:GridCheckBoxColumn>--%>
                    <telerik:GridBoundColumn DataField="HomeBaseIcaoID" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="100px" FilterControlWidth="80px" AutoPostBackOnFilter="false" FilterDelay="500"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CQCustomerID" HeaderText="CQCustomer Code" CurrentFilterFunction="EqualTo"
                        UniqueName="CQCustomerID" Display="false" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                        ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" HeaderStyle-Width="80px">
                    </telerik:GridCheckBoxColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            Visible='<%# IsAuthorized(Permission.Database.AddCQCustomerCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            Visible='<%# IsAuthorized(Permission.Database.EditCQCustomerCatalog)%>' ToolTip="Edit"
                            CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                            Visible='<%# IsAuthorized(Permission.Database.DeleteCQCustomerCatalog)%>' runat="server"
                            CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                    <div>
                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea-nw">
                            <tr>
                                <td>
                                    <asp:Button ID="btnImage" runat="server" Text="Attachments" CssClass="ui_nav" OnClientClick="javascript:OnClientClick('Image'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnMainContact" runat="server" CssClass="ui_nav" Text="Main Contact"
                                        OnClientClick="javascript:OnClientClick('Main Contact');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnNotes" runat="server" CssClass="ui_nav" Text="Notes" OnClientClick="javascript:OnClientClick('Notes');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveChangesTop" runat="server" CssClass="button" Text="Save" ValidationGroup="save"
                                        OnClick="SaveChanges_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                        OnClick="Cancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlMaintenance" Width="100%" ExpandAnimation-Type="none"
                ValidationGroup="save" CollapseAnimation-Type="none" OnClientItemExpand="pnlMaintenanceExpand" OnClientItemCollapse="pnlMaintenanceCollapse" runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                        <Items>
                            <telerik:RadPanelItem Value="Maintenance" runat="server">
                                <ContentTemplate>
                                    <table width="100%" class="box1">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <asp:CheckBox ID="chkIsActive" runat="server" Text="Active" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            <span class="mnd_text">Customer Code</span>
                                                        </td>
                                                        <td class="tdLabel174" valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCode" runat="server" CssClass="text40" MaxLength="5" OnTextChanged="tbCode_TextChanged"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnCQCustomerID" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Customer Code is Required"
                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="tbCode"
                                                                            ValidationGroup="save" Text="Customer Code is Required" SetFocusOnError="true"
                                                                            Display="Dynamic" CssClass="alert-text"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel100" valign="top">
                                                            <span class="mnd_text">Customer Name</span>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbName" runat="server" CssClass="text150" MaxLength="40"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="tbName"
                                                                            ValidationGroup="save" Text="Customer Name is Required" SetFocusOnError="true"
                                                                            Display="Dynamic" CssClass="alert-text"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            <span>Discount %</span>
                                                        </td>
                                                        <td class="tdLabel174" valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbDiscount" runat="server" CssClass="text40" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event, '.');"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="regDiscount" runat="server" ErrorMessage="Invalid Format"
                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbDiscount" ValidationGroup="save"
                                                                            ValidationExpression="^\d{0,2}(\.\d{0,2})?$" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel100" valign="top">
                                                            <span>Credit Limit</span>
                                                        </td>
                                                        <td class="pr_radtextbox_150">
                                                            <table cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <%--<asp:TextBox ID="tbCreditLimit" runat="server" CssClass="text150" MaxLength="17"></asp:TextBox>--%>
                                                                        <telerik:RadNumericTextBox ID="tbCreditLimit" runat="server" Type="Currency" Culture="en-US"
                                                                            MaxLength="14" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%-- <asp:RegularExpressionValidator ID="regCreditLimit" runat="server" ErrorMessage="Invalid Format"
                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbCreditLimit" ValidationGroup="save"
                                                                            ValidationExpression="^\d{0,14}(\.\d{0,2})?$" />--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            Home Base
                                                        </td>
                                                        <td class="tdLabel174">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel110">
                                                                        <asp:TextBox ID="tbHomeBase" runat="server" CssClass="text40" MaxLength="4" OnTextChanged="tbHomeBase_TextChanged"
                                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true">
                                                                        </asp:TextBox>
                                                                        <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openHomeBase();return false;" />
                                                                        <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                            ErrorMessage="Invalid Home Base Code" Display="Dynamic" CssClass="alert-text"
                                                                            ValidationGroup="save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel100">
                                                            Closest ICAO
                                                        </td>
                                                        <td class="tdLabel220">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel110">
                                                                        <asp:TextBox ID="tbClosestIcao" runat="server" CssClass="text40" MaxLength="4" OnTextChanged="tbClosestIcao_TextChanged"
                                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true">
                                                                        </asp:TextBox>
                                                                        <asp:Button ID="btnClosestIcao" runat="server" OnClientClick="javascript:openClosestIcao();return false;"
                                                                            CssClass="browse-button"></asp:Button>
                                                                        <asp:HiddenField ID="hdnClosestAirportID" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvClosestIcao" runat="server" ControlToValidate="tbClosestIcao"
                                                                            ErrorMessage="Invalid Closest ICAO Code" Display="Dynamic" CssClass="alert-text"
                                                                            ValidationGroup="save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Credit
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel130" valign="top">
                                                                        <asp:CheckBox ID="chkApplication" runat="server" Text="Application on File" />
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:CheckBox ID="chkApproval" runat="server" Text="Approval" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="display: none">
                                                            Customer Type
                                                        </td>
                                                        <td style="display: none">
                                                            <asp:DropDownList ID="ddlCustomerType" runat="server">
                                                                <asp:ListItem Value="0">Select</asp:ListItem>
                                                                <asp:ListItem Value="1">broker</asp:ListItem>
                                                                <asp:ListItem Value="2">block CQ client</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <%--<tr>
                                                        <td>
                                                            Margin Percentage
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbMarginPercentage" runat="server" Text="0.0" MaxLength="8" ValidationGroup="Save"
                                                                onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                                                CssClass="text80"></asp:TextBox>
                                                        </td>
                                                    </tr>--%>
                                                    <%--<tr>
                                                        <td>
                                                            <asp:RegularExpressionValidator ID="revtbMarginPercentage" runat="server" Display="Dynamic"
                                                                ErrorMessage="Invalid Format (NNNNNNN.NN)" ControlToValidate="tbMarginPercentage"
                                                                CssClass="alert-text" ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>--%>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <fieldset>
                                                    <legend>Billing Information</legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            Name
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbBillingName" runat="server" CssClass="text200" MaxLength="40"
                                                                                            ></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            Address 1
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <asp:TextBox ID="tbBillingAddress1" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            Address 2
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <asp:TextBox ID="tbBillingAddress2" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td class="tdLabel120">
                                                                            <asp:Label ID="lbBillingAddr3" runat="server" Text="Address Line 3" />
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbBillingAddress3" runat="server" MaxLength="40" CssClass="text200"
                                                                                            ValidationGroup="save" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            City
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <asp:TextBox ID="tbBillingCity" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            State/Province
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <asp:TextBox ID="tbBillingState" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            Country
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel110">
                                                                                        <asp:TextBox ID="tbBillingCountry" runat="server" CssClass="text50" MaxLength="3"
                                                                                            ValidationGroup="save" OnTextChanged="tbBillingCountry_TextChanged" onBlur="return RemoveSpecialChars(this)"
                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:Button ID="btnBillingCountry" runat="server" OnClientClick="javascript:openCountry();return false;"
                                                                                            CssClass="browse-button"></asp:Button>
                                                                                        <asp:HiddenField ID="hdnBillingCountryID" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvBillingCountry" runat="server" ControlToValidate="tbBillingCountry"
                                                                                            ErrorMessage="Invalid Country Code" Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            Postal
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <asp:TextBox ID="tbBillingPostal" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120">
                                                                            Business Phone
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <asp:TextBox ID="tbBillingPhone" runat="server" MaxLength="25" CssClass="text200"
                                                                                onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel130">
                                                                            <asp:Label ID="lbTollFreePhone" runat="server" Text="Toll-free Phone" />
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbBillingTollFreePhone" runat="server" MaxLength="25" CssClass="text200"
                                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" ValidationGroup="save" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            Business E-mail
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbBillingEmailAddress" runat="server" CssClass="text200" ValidationGroup="save"
                                                                                            MaxLength="40">
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="regBillingEmailAddress" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbBillingEmailAddress" CssClass="alert-text"
                                                                                            ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            Web site
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbBillingWebAddress" runat="server" CssClass="text200" MaxLength="200"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%-- <asp:RegularExpressionValidator ID="revwebsite" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbWebAddress" CssClass="alert-text"
                                                                                            ValidationGroup="save" ValidationExpression="([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120">
                                                                            Business Fax
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbBillingFax" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlMainContact" Width="100%" ExpandAnimation-Type="none"
                CollapseAnimation-Type="none" OnClientItemExpand="pnlMainContactExpand"  OnClientItemCollapse="pnlMainContactCollapse" runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Main Contact Information" Expanded="false">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" class="box1">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            First Name
                                                        </td>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:TextBox ID="tbFirstName" runat="server" ReadOnly="true" CssClass="text100" MaxLength="40"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                            <asp:HiddenField ID="hdnContactName" runat="server" />
                                                        </td>
                                                        <td class="tdLabel80" valign="top">
                                                            Middle Name
                                                        </td>
                                                        <td class="tdLabel140" valign="top">
                                                            <asp:TextBox ID="tbMiddleName" runat="server" ReadOnly="true" CssClass="text100"
                                                                MaxLength="40" onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel70" valign="top">
                                                            Last Name
                                                        </td>
                                                        <td class="tdLabel140" valign="top">
                                                            <asp:TextBox ID="tbLastName" runat="server" ReadOnly="true" CssClass="text100" MaxLength="40"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Title
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbTitle" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Address 1
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbAddr1" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            Address 2
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                            <asp:TextBox ID="tbAddr2" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Address Line 3
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbAddr3" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            City
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbCity" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            State/Province
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                            <asp:TextBox ID="tbState" runat="server" ReadOnly="true" CssClass="text200" MaxLength="10"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Country
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbCountry" runat="server" ReadOnly="true" CssClass="text50" MaxLength="3"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            Postal
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                            <asp:TextBox ID="tbPostal" runat="server" ReadOnly="true" CssClass="text200" MaxLength="15"
                                                                onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbBusinessPhone" runat="server" Text="Business Phone" />
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <asp:TextBox ID="tbBusinessPhone" runat="server" MaxLength="25" CssClass="text200"
                                                                onKeyPress="return fnAllowPhoneFormat(this,event)" ValidationGroup="save" />
                                                        </td>
                                                        <td class="tdLabel130">
                                                            Home Phone
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPhone" runat="server" ReadOnly="true" CssClass="text200" MaxLength="25"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbOtherPhone" runat="server" Text="Other Phone"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbOtherPhone" runat="server" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                            MaxLength="25" CssClass="text200" ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbCellPhoneNum" runat="server" Text="Primary Mobile/Cell" />
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCellPhoneNum" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                            runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbCellPhoneNum2" runat="server" Text="Secondary Mobile/Cell" />
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCellPhoneNum2" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                            runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            E-mail Address
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbEmail" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbBusinessEmail" runat="server" Text="Business E-mail"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbBusinessEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                            ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbPersonalEmail" runat="server" Text="Personal E-mail" />
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPersonalEmail" runat="server" MaxLength="25" CssClass="text200"
                                                                            ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbOtherEmail" runat="server" Text="Other E-mail" />
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbOtherEmail" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            Business Fax
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbFax" ReadOnly="true" runat="server" CssClass="text200" MaxLength="25"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbHomeFax" runat="server" Text="Home Fax" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbHomeFax" runat="server" MaxLength="25" CssClass="text200" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                ValidationGroup="save" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbWebsite" runat="server" Text="Web Site" />
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbWebsite" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbMCBusinessEmail" runat="server" Text="Business E-mail" />
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbMCBusinessEmail" runat="server" MaxLength="25" CssClass="text200"
                                                                            ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="top">
                                                            Additional Contact Information
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbAdditionalContact" runat="server" ReadOnly="true" TextMode="MultiLine"
                                                                CssClass="textarea720x80"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel120" valign="top">
                                                            Cr. Name
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCreditName1" runat="server" CssClass="text200" ValidationGroup="save"
                                                                            MaxLength="25">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            Cr. Name
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCreditName2" runat="server" CssClass="text200" ValidationGroup="save"
                                                                            MaxLength="25">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel120" valign="top">
                                                            No./Exp
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCreditNum1" runat="server" CssClass="text200" ValidationGroup="save"
                                                                            MaxLength="30">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            No./Exp
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCreditNum2" runat="server" CssClass="text200" ValidationGroup="save"
                                                                            MaxLength="30">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>           
            <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"  OnClientItemExpand="pnlImageExpand" OnClientItemCollapse="pnlImageCollapse"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Attachments" CssClass="PanelHeaderStyle">
                        <ContentTemplate>
                            <table width="100%" class="box1">                                          
                                <tr style="display: none;">
                                    <td class="tdLabel100">
                                        Document Name
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text210" OnBlur="CheckName();"
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="tdLabel270">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:FileUpload ID="fileUL" runat="server" Enabled="false" ClientIDMode="Static" onchange="javascript:return File_onchange();" />
                                                    <asp:Label ID="lblError" CssClass="alert-text" runat="server" Style="float: left;">All file types are allowed.</asp:Label>
                                                    <asp:HiddenField ID="hdnUrl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" class="tdLabel110">                                       
                                        <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                            OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text200"
                                            ClientIDMode="AutoID">
                                        </asp:DropDownList>
                                    </td>
                                    <td valign="top" class="tdLabel80">
                                        <asp:Image ID="imgFile" CssClass="maxWidth240" runat="server" OnClick="OpenRadWindow();"
                                            AlternateText="" ClientIDMode="Static" />
                                        <asp:HyperLink ID="lnkFileName" runat="server" ClientIDMode="Static">Download File</asp:HyperLink>
                                    </td>
                                    <td valign="top" align="right" class="custom_radbutton">
                                        <telerik:RadButton ID="btndeleteImage" runat="server" Text="Delete" Enabled="false"
                                            OnClientClicking="ImageDeleteConfirm" OnClick="DeleteImage_Click" ClientIDMode="Static" />
                                    </td>
                                </tr>
                            </table>                                                                         
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>                    
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlNotes" Width="100%" ExpandAnimation-Type="none" OnClientItemExpand="pnlNotesExpand" OnClientItemCollapse="pnlNotesCollapse" CollapseAnimation-Type="none"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Notes" Expanded="false">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" cellspacing="0" class="note-box">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea-db">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="save"
                            OnClick="SaveChanges_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" CausesValidation="false"
                            OnClick="Cancel_Click" />
                        <asp:HiddenField ID="hdnBrowserName" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
