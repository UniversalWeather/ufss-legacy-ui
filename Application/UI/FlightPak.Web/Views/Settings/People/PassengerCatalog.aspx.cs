﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels;
using FlightPak.Web.BusinessLite;
using System.Text;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class PassengerCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private bool IsValidateCustom = true;
        private bool SaveConfirm = false;
        private bool IsUpdate = true;
        bool isPaxPassportPopup = false;
        string DateFormat;
        private ExceptionManager exManager;
        private bool PassengerRequestorPageNavigated = false;
        private bool _selectLastModified = false;
        protected FlightPakMasterService.Company CompanySett = new FlightPakMasterService.Company();
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPassenger, dgPassenger, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgPassenger.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                        bool check = IsAuthorized(Permission.DatabaseReports.ViewPassengerReport);
                        lbtnReports.Visible = true;
                        lbtnSaveReports.Visible = false;

                        if (btnSaveChanges.Visible == true)
                        {
                            chkDisplayInactiveChecklist.Enabled = true;
                        }
                        else
                        {
                            chkDisplayInactiveChecklist.Enabled = false;
                        }
                        GetCompanyProfileInfo();
                        isPaxPassportPopup = Convert.ToBoolean(Request.QueryString["IsPaxPassport"]) == null ? false : Convert.ToBoolean(Request.QueryString["IsPaxPassport"]);
                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewPassengerRequestor);
                            // Method to display first record in read only format
                            // Date Format and Pax Code format settings

                            GetCompanyProfileInfo();
                            ClearAddInfoAndPaxChecklistSession();
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                chkSearchHomebaseOnly.Checked = false;
                                chkSearchActiveOnly.Checked = false;
                                dgPassenger.Rebind();
                                ReadOnlyForm();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                                {
                                    chkSearchHomebaseOnly.Checked = true;
                                }
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                            CreateDictionayForImgUpload();
                            LoadDefaultData();
                            if (IsPopUp)
                            {
                                //Hide Controls
                                dgPassenger.Visible = false;
                                chkSearchHomebaseOnly.Visible = false;
                                chkSearchActiveOnly.Visible = false;
                                lbtnReports.Visible = false;
                                lbtnSaveReports.Visible = false;
                                btnShowReports.Visible = false;
                                lbtnTravelSense.Visible = false;
                                chkSearchRequestorOnly.Visible = false;
                                lbSearchClientCode.Visible = false;
                                tbSearchClientCode.Visible = false;
                                btnSearchClientCode.Visible = false;
                                hdnPaxPassport.Value = "Yes";
                                // Show Insert Form
                                //DisplayInsertForm();
                                //tbAccountNumber.Focus();
                                if (IsAdd)
                                {
                                    (dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    if (Request.QueryString["IsPassport"] == null && Convert.ToBoolean(Request.QueryString["IsPassport"]) != true)
                                    {
                                        hdnPaxPassport.Value = "No";
                                    }
                                    else if (Convert.ToBoolean(Request.QueryString["IsPassport"]) == false)
                                    {
                                        hdnPaxPassport.Value = "No";
                                    }
                                    (dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                                if (Request.QueryString["IsPassport"] == null)
                                {
                                    if (isPaxPassportPopup)
                                    {
                                        hdnPaxPassport.Value = "Yes";
                                    }
                                    else
                                    {
                                        hdnPaxPassport.Value = "No";
                                    }
                                }
                                if (IsAdd)
                                {
                                    hdnPaxPassport.Value = string.Empty;
                                }
                                else if (hdnSave.Value == "Update" && Request.QueryString["IsPassport"] == null && Request.QueryString["IsPaxPassport"]==null)
                                {
                                    hdnPaxPassport.Value = string.Empty;
                                }
                            }
                        }
                        //else
                        //{
                        //    // Date Format and Pax Code format settings
                        //    GetCompanyProfileInfo();
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                if (Request.QueryString["PassengerRequestorID"] != null)
                {
                    Session["NewPaxSelectID"] = Request.QueryString["PassengerRequestorID"].Trim();
                }
            }
        }

        protected void LastName_TextChanged(object sender, EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient Service1 = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                List<GetAllPassenger> FilterCrew = new List<GetAllPassenger>();
                FilterCrew = (List<GetAllPassenger>)Session["PassengerList"];
                if (CompanySett.IsAutoPassenger == true)
                {
                    if (tbLastName.Text.Trim() != string.Empty)
                    {
                        FilterCrew = FilterCrew.Where(x => (x.LastName != null)).ToList();
                        FilterCrew = FilterCrew.Where(x => (x.LastName.Trim().ToUpper().Equals(tbLastName.Text.ToString().ToUpper().Trim()))).ToList();

                        if (FilterCrew != null && FilterCrew.Count > 0)
                        {
                            hdnWarning.Value = "1";
                            lbWarningMessage.Visible = true;
                        }
                        else
                        {
                            hdnWarning.Value = "0";
                            lbWarningMessage.Visible = false;
                        }
                    }
                    else
                    {
                        hdnWarning.Value = "1";
                        lbWarningMessage.Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// To Display first record as default
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgPassenger.Rebind();
                    }
                    if (dgPassenger.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["PassengerRequestorID"] = null;
                        //}
                        if (Session["PassengerRequestorID"] == null)
                        {
                            dgPassenger.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                            {
                                // if ViewState["addedPsngrReqId"] is null then Ist value will be dispalyed else newly added value
                                //if (ViewState["addedPsngrReqId"]==null)
                                Session["PassengerRequestorID"] = dgPassenger.Items[0].GetDataKeyValue("PassengerRequestorID").ToString();
                                //else
                                //    Session["PassengerRequestorID"] =Convert.ToString(ViewState["addedPsngrReqId"]);
                            }
                        }

                        if (dgPassenger.SelectedIndexes.Count == 0)
                            dgPassenger.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the selected item which they selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["PassengerRequestorID"] != null)
                    {
                        string ID = Session["PassengerRequestorID"].ToString();
                        foreach (GridDataItem Item in dgPassenger.MasterTableView.Items)
                        {
                            if (Item["PassengerRequestorID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        //if (dgPassenger.SelectedItems.Count == 0)
                        //{
                        //   // DefaultSelection(false);
                        //    dgPassenger.SelectedIndexes.Add(0);
                        //}
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, editCtl, delCtl;
                    insertCtl = (LinkButton)dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    delCtl = (LinkButton)dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    editCtl = (LinkButton)dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddPassengerRequestor))
                    {
                        insertCtl.Visible = true;
                        if (add)
                            insertCtl.Enabled = true;
                        else
                            insertCtl.Enabled = false;
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeletePassengerRequestor))
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditPassengerRequestor))
                    {
                        editCtl.Visible = true;
                        if (edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// <summary>
        /// To Empty all the fields
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ClearAddInfoAndPaxChecklistSession();
                    tbCode.ReadOnly = CompanySett.IsAutoPassenger == null ? false : CompanySett.IsAutoPassenger.Value;
                    hdnSave.Value = "Save";
                    ClearForm();
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    EnableForm(true);
                    GetDefaultFlightPurpose();
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.ReadOnly = true;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    LoadControlData();
                    EnableForm(true);
                    btnApplyToAdditionalCrew.Enabled = false;
                    if (dgCrewCheckList.Items.Count > 0)
                    {
                        dgCrewCheckList.SelectedIndexes.Add(0);
                        BindSelectedCrewChecklistItem();
                        EnableCheckListFields(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbFirstName.Enabled = enable;
                    tbMiddleName.Enabled = enable;
                    tbLastName.Enabled = enable;
                    tbCompany.Enabled = enable;
                    chkActive1.Enabled = enable;
                    chkRequestor.Enabled = enable;
                    tbEmpId.Enabled = enable;
                    tbPaxPhone.Enabled = enable;
                    tbCityOfBirth.Enabled = enable;
                    tbPaxTitle.Enabled = enable;
                    tbNickname.Enabled = enable;
                    tbAddlPhone.Enabled = enable;
                    tbPaxFax.Enabled = enable;
                    tbEmail.Enabled = enable;
                    tbNationality.Enabled = enable;
                    BtnNationality.Enabled = enable;
                    btnCountry.Enabled = enable;
                    tbCityOfResi.Enabled = enable;
                    btnCityOfResi.Enabled = enable;
                    tbAddress1.Enabled = enable;
                    tbAddress2.Enabled = enable;
                    tbCity.Enabled = enable;
                    tbState.Enabled = enable;
                    tbPostal.Enabled = enable;
                    rbFemale.Enabled = enable;
                    rbMale.Enabled = enable;
                    tbPinNumber.Enabled = enable;
                    tbCQCustomer.Enabled = enable;
                    btnCQCustomer.Enabled = enable;
                    tbHotelPreferences.Enabled = enable;
                    tbCateringPreferences.Enabled = enable;
                    tbEmergencyContact.Enabled = enable;
                    tbCreditCardInfo.Enabled = enable;
                    tbPaxWeight.Enabled = enable;
                    dpPaxWeightUnits.Enabled = enable;
                    tbBirthday.Enabled = enable;
                    tbAnniversaries.Enabled = enable;
                    tbCardExpires.Enabled = enable;
                    //((TextBox)ucBirthday.FindControl("tbDate")).Enabled = enable;
                    //((TextBox)ucAnniversaries.FindControl("tbDate")).Enabled = enable;
                    //((TextBox)ucCardExpires.FindControl("tbDate")).Enabled = enable;
                    btnGreenNationality.Enabled = enable;
                    tbGreenNationality.Enabled = enable;
                    tbINSANumber.Enabled = enable;
                    //rbPaxMale.Enabled = enable; 3088
                    //rbPaxFemale.Enabled = enable;
                    tbResidentSinceYear.Enabled = enable;
                    tbCategory.Enabled = enable;
                    tbDateOfBirth.Enabled = enable;
                    //((TextBox)ucDateOfBirth.FindControl("tbDate")).Enabled = enable;
                    if (UserPrincipal != null && UserPrincipal.Identity._clientId != null && UserPrincipal.Identity._clientId != 0)
                    {
                        if (!string.IsNullOrEmpty(tbClientCde.Text))
                        {
                            if (tbClientCde.Text.Trim() == UserPrincipal.Identity._clientCd.ToString().Trim())
                            {
                                tbClientCde.Enabled = false;
                            }
                            else
                            {
                                tbClientCde.Enabled = true;
                            }
                        }
                        tbClientCde.Text = UserPrincipal.Identity._clientCd.ToString().Trim();
                        hdnClientCde.Value = UserPrincipal.Identity._clientId.ToString().Trim();
                        btnClientCde.Enabled = false;
                    }
                    else
                    {
                        tbClientCde.Enabled = enable;
                        btnClientCde.Enabled = enable;
                    }
                    if (hdnSave.Value == "Save")
                    {
                        if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportId != 0)
                        {
                            tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd.ToString().Trim();
                            hdnHomeBase.Value = UserPrincipal.Identity._airportId.ToString().Trim();
                        }
                    }
                    tbHomeBase.Enabled = enable;
                    btnHomeBase.Enabled = enable;
                    tbFlightPurpose.Enabled = enable;
                    btnFlightPurpose.Enabled = enable;
                    tbDepartment.Enabled = enable;
                    btnDepartment.Enabled = enable;
                    tbAuth.Enabled = enable;
                    btnAuth.Enabled = enable;
                    tbStdBilling.Enabled = enable;
                    tbAccountNumber.Enabled = enable;
                    btnAccount.Enabled = enable;
                    radType.Enabled = enable;
                    radNonControlled.Enabled = enable;
                    radGuest.Enabled = enable;
                    tbAssociated.Enabled = enable;
                    btnAssociated.Enabled = enable;
                    if ((hdnSave.Value == "Save" || hdnSave.Value == "Update") && (enable == true))
                    {
                        if (radGuest.Checked == true)
                        {
                            tbAssociated.Enabled = true;
                            btnAssociated.Enabled = true;
                        }
                        else
                        {
                            tbAssociated.Enabled = false;
                            btnAssociated.Enabled = false;
                        }
                    }
                    btnAddChecklist.Enabled = enable;
                    EnableCheckListFields(enable);
                    chkSiflSecurity.Enabled = enable;
                    tbTsenseid.Enabled = enable;
                    tbTitle.Enabled = enable;
                    tbSalaryLevel.Enabled = enable;
                    tbNotes.Enabled = enable;
                    tbPaxItems.Enabled = enable;
                    dgPassengerAddlInfo.Enabled = enable;
                    btnAddlInfo.Enabled = enable;
                    btnDeleteInfo.Enabled = enable;
                    foreach (GridDataItem item in dgVisa.MasterTableView.Items)
                    {
                        item.Enabled = enable;
                    }
                    btnAddVisa.Enabled = enable;
                    btnDeleteVisa.Enabled = enable;
                    dgCrewCheckList.Enabled = enable;
                    btnAddChecklist.Enabled = enable;
                    if (dgCrewCheckList.Items.Count > 0)
                        btnDeleteChecklist.Enabled = enable;
                    dgPassport.Enabled = enable;
                    btnAddPassport.Enabled = enable;
                    btnDeletePassport.Enabled = enable;
                    btnSaveChangesTop.Visible = enable;
                    btnCancelTop.Visible = enable;
                    btnSaveChanges.Visible = enable;
                    btnCancel.Visible = enable;
                    ddlImg.Enabled = enable;
                    tbImgName.Enabled = enable;
                    btndeleteImage.Enabled = enable;
                    fileUL.Enabled = enable;
                    //<new field>
                    tbAddress3.Enabled = enable;
                    tbOtherEmail.Enabled = enable;
                    tbPersonalEmail.Enabled = enable;
                    tbOtherPhone.Enabled = enable;
                    tbSecondaryMobile.Enabled = enable;
                    tbBusinessFax.Enabled = enable;
                    tbPrimaryMobile.Enabled = enable;
                    //<\new field>
                    pnlSearchPanel.Enabled = !(enable);
                    btndeleteImage.Visible = enable;
                    btnAddlInfo.Visible = enable;
                    btnDeleteInfo.Visible = enable;
                    btnAddPassport.Visible = enable;
                    btnDeletePassport.Visible = enable;
                    btnAddVisa.Visible = enable;
                    btnDeleteChecklist.Visible = enable;
                    btnDeleteVisa.Visible = enable;
                    //if (enable)
                    //{
                    //    btndeleteImage.CssClass = "button";//enabled
                    //    btnAddlInfo.CssClass = "ui_nav";
                    //    btnDeleteInfo.CssClass = "ui_nav";
                    //    btnAddPassport.CssClass = "ui_nav";
                    //    btnDeletePassport.CssClass = "ui_nav";
                    //    btnAddVisa.CssClass = "ui_nav";
                    //    btnDeleteVisa.CssClass = "ui_nav";
                    //}
                    //else
                    //{
                    //    btndeleteImage.CssClass = "button-disable";//disabled
                    //    btnAddlInfo.CssClass = "ui_nav_disable";
                    //    btnDeleteInfo.CssClass = "ui_nav_disable";
                    //    btnAddPassport.CssClass = "ui_nav_disable";
                    //    btnDeletePassport.CssClass = "ui_nav_disable";
                    //    btnAddVisa.CssClass = "ui_nav_disable";
                    //    btnDeleteVisa.CssClass = "ui_nav_disable";
                    //}
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear the form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    hdnCode.Value = string.Empty;
                    tbFirstName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbCompany.Text = string.Empty;
                    chkActive1.Checked = true;
                    chkRequestor.Checked = false;
                    tbEmpId.Text = string.Empty;
                    tbPaxPhone.Text = string.Empty;
                    tbAddlPhone.Text = string.Empty;
                    tbPaxFax.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbNationality.Text = string.Empty;
                    tbNationalityName.Text = string.Empty;
                    hdnNationality.Value = string.Empty;
                    tbCityOfResi.Text = string.Empty;
                    tbCityOfResiName.Text = string.Empty;
                    hdnCityOfResi.Value = string.Empty;
                    tbAddress1.Text = string.Empty;
                    tbAddress2.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbPostal.Text = string.Empty;
                    rbMale.Checked = true;
                    rbFemale.Checked = false;
                    tbPinNumber.Text = string.Empty;
                    tbDateOfBirth.Text = string.Empty;
                    //((TextBox)ucDateOfBirth.FindControl("tbDate")).Text = string.Empty;
                    tbClientCde.Text = string.Empty;
                    hdnClientCde.Value = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    hdnHomeBase.Value = string.Empty;
                    tbFlightPurpose.Text = string.Empty;
                    hdnFlightPurpose.Value = string.Empty;
                    tbDepartment.Text = string.Empty;
                    tbDepartmentDesc.Text = string.Empty;
                    hdnDepartment.Value = string.Empty;
                    tbAuth.Text = string.Empty;
                    tbAuthDesc.Text = string.Empty;
                    hdnAuth.Value = string.Empty;
                    tbStdBilling.Text = string.Empty;
                    tbAccountNumber.Text = string.Empty;
                    hdnAccountNumber.Value = string.Empty;
                    radType.Checked = false;
                    radNonControlled.Checked = true;
                    radGuest.Checked = false;
                    tbAssociated.Text = string.Empty;
                    chkSiflSecurity.Checked = false;
                    tbTsenseid.Text = string.Empty;
                    tbTitle.Text = string.Empty;
                    tbSalaryLevel.Text = string.Empty;
                    tbCityOfBirth.Text = "";
                    tbPaxTitle.Text = "";
                    tbNickname.Text = "";
                    tbNotes.Text = string.Empty;
                    tbPaxItems.Text = string.Empty;
                    lbIsEnableTSAPX.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    lbIsEnableTSAPX.ForeColor = Color.Black;
                    lbIsEnableTSAPX.BackColor = Color.Transparent;
                    // <new fields>
                    tbAddress3.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    tbSecondaryMobile.Text = string.Empty;
                    tbBusinessFax.Text = string.Empty;
                    tbPrimaryMobile.Text = string.Empty;
                    // <\new fields>
                    hdnTSAStatus.Value = string.Empty;
                    hdnTSATSADTTM.Value = string.Empty;
                    //Clear Passenger Additional Information grid
                    //Updated by Manish V Performance Optimization 
                    //This could have been done in a better way
                    //but due to the stray html need to call this with
                    //fake id but the impact is minimized since I've modified the 
                    //base function to limit the fetched data
                    //Session.Remove("PassengerAdditionalInfo");
                    //BindAdditionalInfoGrid(0);
                    //Clear Passenger Visa grid

                    #region "Crew Checklist grid"
                    //Session.Remove("PaxCheckList");
                    //ClearCheckListFields();
                    //BindCrewCheckListGrid(0);
                    #endregion


                    Session.Remove("PassengerVisa");
                    BindVisaGrid(0);
                    //Clear Passenger Additional Information grid
                    Session.Remove("PassengerPassport");
                    BindPassportGrid(0);
                    hdnIsPassportChoice.Value = string.Empty;
                    hdnIsPassportChanged.Value = string.Empty;
                    hdnIsPassportChoiceDeletedCount.Value = string.Empty;
                    hdnIsPassportChoiceDeleted.Value = string.Empty;
                    hdnIsPassportChoiceChanged.Value = string.Empty;


                    tbHotelPreferences.Text = string.Empty;
                    tbCateringPreferences.Text = string.Empty;
                    tbEmergencyContact.Text = string.Empty;
                    tbCreditCardInfo.Text = string.Empty;
                    tbPaxWeight.Text = string.Empty;
                    dpPaxWeightUnits.SelectedValue = "0";
                    tbBirthday.Text = string.Empty;
                    tbAnniversaries.Text = string.Empty;
                    //((TextBox)ucBirthday.FindControl("tbDate")).Text = string.Empty;
                    //((TextBox)ucAnniversaries.FindControl("tbDate")).Text = string.Empty;


                    //Clear Image related data
                    CreateDictionayForImgUpload();
                    //fileUL.Enabled = false;   //Commented for removing document name
                    ddlImg.Enabled = false;
                    imgFile.ImageUrl = "";
                    ImgPopup.ImageUrl = null;
                    lnkFileName.NavigateUrl = "";
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbNotes.Text = "";
                    tbImgName.Text = "";
                    tbCardExpires.Text = string.Empty;
                    //((TextBox)ucCardExpires.FindControl("tbDate")).Text = string.Empty;
                    tbGreenNationality.Text = string.Empty;
                    tbINSANumber.Text = string.Empty;
                    //rbPaxMale.Checked = true; 3088
                    tbResidentSinceYear.Text = string.Empty;
                    tbCategory.Text = string.Empty;
                    tbCQCustomer.Text = string.Empty;
                    hdnCQCustomerID.Value = string.Empty;
                    hdnGreenNationality.Value = "";

                    hdnWarning.Value = "1";
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    DefaultChecklistSelection();
                    EnableForm(false);
                    if (dgCrewCheckList.Items.Count > 0)
                    {
                        btnApplyToAdditionalCrew.Enabled = true;
                    }
                    else
                    {
                        btnApplyToAdditionalCrew.Enabled = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// To assign the data to controls 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    ClearForm();
                    if (dgPassenger.SelectedItems.Count == 0)
                    {
                        DefaultSelection(false);
                        //dgPassenger.SelectedIndexes.Add(0);
                    }
                    if (Session["NewPaxSelectID"] != null)
                    {
                        Session["PassengerRequestorID"] = Session["NewPaxSelectID"];
                    }
                    if (Session["PassengerRequestorID"] != null)
                    {
                        List<FlightPakMasterService.GetPassengerByPassengerRequestorID> objPassenger = new List<GetPassengerByPassengerRequestorID>();
                        using (FlightPakMasterService.MasterCatalogServiceClient objPassengerService = new MasterCatalogServiceClient())
                        {
                            objPassenger = objPassengerService.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Session["PassengerRequestorID"].ToString())).EntityList;
                            if (objPassenger.Count != 0)
                            {
                                //if (objPassenger[0].PassengerRequestorID != null)
                                //{
                                hdnCode.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].PassengerRequestorID.ToString());
                                //}
                                if (objPassenger[0].PassengerRequestorCD != null)
                                { tbCode.Text = objPassenger[0].PassengerRequestorCD.ToString(); }
                                if (objPassenger[0].FirstName != null)
                                { tbFirstName.Text = objPassenger[0].FirstName.ToString(); }
                                if (objPassenger[0].MiddleInitial != null)
                                { tbMiddleName.Text = objPassenger[0].MiddleInitial.ToString(); }
                                if (objPassenger[0].LastName != null)
                                { tbLastName.Text = objPassenger[0].LastName.ToString(); }
                                if (objPassenger[0].CompanyName != null)
                                { tbCompany.Text = objPassenger[0].CompanyName.ToString(); }
                                if (objPassenger[0].IsActive != null)
                                { chkActive1.Checked = (bool)objPassenger[0].IsActive; }
                                else
                                { chkActive1.Checked = false; }
                                if (objPassenger[0].IsRequestor != null)
                                { chkRequestor.Checked = (bool)objPassenger[0].IsRequestor; }
                                else
                                { chkRequestor.Checked = false; }
                                if (objPassenger[0].EmployeeID != null)
                                { tbEmpId.Text = objPassenger[0].EmployeeID.ToString(); }
                                if (objPassenger[0].PhoneNum != null)
                                { tbPaxPhone.Text = objPassenger[0].PhoneNum.ToString(); }
                                if (objPassenger[0].AdditionalPhoneNum != null)
                                { tbAddlPhone.Text = objPassenger[0].AdditionalPhoneNum.ToString(); }
                                if (objPassenger[0].FaxNum != null)
                                { tbPaxFax.Text = objPassenger[0].FaxNum.ToString(); }

                                if (objPassenger[0].CityOfBirth != null)
                                    tbCityOfBirth.Text = objPassenger[0].CityOfBirth;
                                else
                                    tbCityOfBirth.Text = "";

                                if (objPassenger[0].Nickname != null)
                                    tbNickname.Text = objPassenger[0].Nickname;
                                else
                                    tbNickname.Text = "";

                                if (objPassenger[0].PaxTitle != null)
                                    tbPaxTitle.Text = objPassenger[0].PaxTitle.Trim();
                                else
                                    tbPaxTitle.Text = "";



                                if (objPassenger[0].EmailAddress != null)
                                { tbEmail.Text = objPassenger[0].EmailAddress.ToString(); }
                                if (objPassenger[0].NationalityCD != null)
                                { tbNationality.Text = objPassenger[0].NationalityCD.ToString(); }
                                if (objPassenger[0].NationalityName != null)
                                { tbNationalityName.Text = System.Web.HttpUtility.HtmlEncode(objPassenger[0].NationalityName.ToString()); }
                                if (objPassenger[0].CountryID != null)
                                { hdnNationality.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].CountryID.ToString()); }
                                if (objPassenger[0].ResidenceCountryCD != null)
                                { tbCityOfResi.Text = objPassenger[0].ResidenceCountryCD.ToString(); }
                                if (objPassenger[0].ResidenceCountryName != null)
                                { tbCityOfResiName.Text = System.Web.HttpUtility.HtmlEncode(objPassenger[0].ResidenceCountryName.ToString()); }
                                if (objPassenger[0].CountryOfResidenceID != null)
                                { hdnCityOfResi.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].CountryOfResidenceID.ToString()); }
                                if (objPassenger[0].Addr1 != null)
                                { tbAddress1.Text = objPassenger[0].Addr1.ToString(); }
                                if (objPassenger[0].Addr2 != null)
                                { tbAddress2.Text = objPassenger[0].Addr2.ToString(); }
                                if (objPassenger[0].City != null)
                                { tbCity.Text = objPassenger[0].City.ToString(); }
                                if (objPassenger[0].StateName != null)
                                { tbState.Text = objPassenger[0].StateName.ToString(); }
                                if (objPassenger[0].PostalZipCD != null)
                                { tbPostal.Text = objPassenger[0].PostalZipCD.ToString(); }
                                if (objPassenger[0].Gender != null)
                                {
                                    if (objPassenger[0].Gender.ToString() == "M")
                                    { rbMale.Checked = true; }
                                    else
                                    { rbFemale.Checked = true; }
                                }
                                else
                                { rbMale.Checked = true; }
                                if (objPassenger[0].PersonalIDNum != null)
                                { tbPinNumber.Text = objPassenger[0].PersonalIDNum.ToString(); }
                                ////if (objPassenger[0].DateOfBirth != null)
                                ////{
                                ////    if (DateFormat != null)
                                ////    { ((TextBox)ucDateOfBirth.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(objPassenger[0].DateOfBirth.ToString())); }
                                ////    else
                                ////    { ((TextBox)ucDateOfBirth.FindControl("tbDate")).Text = objPassenger[0].DateOfBirth.ToString(); }
                                ////}
                                if (objPassenger[0].DateOfBirth != null)
                                {
                                    if (DateFormat != null)
                                    { tbDateOfBirth.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(objPassenger[0].DateOfBirth.ToString())); }
                                    else
                                    { tbDateOfBirth.Text = objPassenger[0].DateOfBirth.ToString(); }
                                }

                                if (objPassenger[0].ClientCD != null)
                                { tbClientCde.Text = objPassenger[0].ClientCD.ToString(); }
                                if (objPassenger[0].ClientID != null)
                                { hdnClientCde.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].ClientID.ToString()); }
                                if (objPassenger[0].HomeBaseCD != null)
                                { tbHomeBase.Text = objPassenger[0].HomeBaseCD.ToString(); }
                                if (objPassenger[0].HomebaseID != null)
                                { hdnHomeBase.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].HomebaseID.ToString()); }

                                if (objPassenger[0].CQCustomerCD != null)
                                { tbCQCustomer.Text = System.Web.HttpUtility.HtmlEncode(objPassenger[0].CQCustomerCD.ToString()); }
                                if (objPassenger[0].cqcustomerid != null)
                                { hdnCQCustomerID.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].cqcustomerid.ToString()); }


                                if (objPassenger[0].FlightPurposeCD != null)
                                { tbFlightPurpose.Text = objPassenger[0].FlightPurposeCD.ToString(); }
                                if (objPassenger[0].FlightPurposeID != null)
                                { hdnFlightPurpose.Value = objPassenger[0].FlightPurposeID.ToString(); }
                                if (objPassenger[0].DepartmentCD != null)
                                { tbDepartment.Text = objPassenger[0].DepartmentCD.ToString(); }
                                if (objPassenger[0].DepartmentName != null)
                                { tbDepartmentDesc.Text = objPassenger[0].DepartmentName.ToString(); }
                                if (objPassenger[0].DepartmentID != null)
                                { hdnDepartment.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].DepartmentID.ToString()); }
                                if (objPassenger[0].AuthorizationCD != null)
                                { tbAuth.Text = objPassenger[0].AuthorizationCD.ToString(); }
                                if (objPassenger[0].DeptAuthDescription != null)
                                { tbAuthDesc.Text = objPassenger[0].DeptAuthDescription.ToString(); }
                                if (objPassenger[0].AuthorizationID != null)
                                { hdnAuth.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].AuthorizationID.ToString()); }
                                if (objPassenger[0].StandardBilling != null)
                                { tbStdBilling.Text = objPassenger[0].StandardBilling.ToString(); }
                                if (objPassenger[0].AccountNum != null)
                                { tbAccountNumber.Text = objPassenger[0].AccountNum.ToString(); }
                                if (objPassenger[0].AccountID != null)
                                { hdnAccountNumber.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].AccountID.ToString()); }
                                if (objPassenger[0].IsEmployeeType != null)
                                {
                                    if (objPassenger[0].IsEmployeeType.ToString() == "C")
                                    {
                                        radType.Checked = true;
                                        radNonControlled.Checked = false;
                                        radGuest.Checked = false;
                                        tbAssociated.Text = string.Empty;
                                        tbAssociated.Enabled = false;
                                        btnAssociated.Enabled = false;
                                    }
                                    else if (objPassenger[0].IsEmployeeType.ToString() == "N")
                                    {
                                        radNonControlled.Checked = true;
                                        radType.Checked = false;
                                        radGuest.Checked = false;
                                        tbAssociated.Text = string.Empty;
                                        tbAssociated.Enabled = false;
                                        btnAssociated.Enabled = false;
                                    }
                                    else if (objPassenger[0].IsEmployeeType.ToString() == "G")
                                    {
                                        radGuest.Checked = true;
                                        radType.Checked = false;
                                        radNonControlled.Checked = false;
                                        if (objPassenger[0].AssociatedWithCD != null)
                                        {
                                            hdnAssociated.Value = System.Web.HttpUtility.HtmlEncode(objPassenger[0].AssociatedWithCD.ToString());
                                            tbAssociated.Text = objPassenger[0].AssociatedWithCD.ToString();
                                        }
                                        tbAssociated.Enabled = true;
                                        btnAssociated.Enabled = true;
                                    }
                                }
                                else
                                {
                                    radNonControlled.Checked = false;
                                    radType.Checked = false;
                                    radGuest.Checked = false;
                                    tbAssociated.Text = string.Empty;
                                    tbAssociated.Enabled = false;
                                    btnAssociated.Enabled = false;
                                }
                                if (objPassenger[0].IsSIFL != null)
                                { chkSiflSecurity.Checked = (bool)objPassenger[0].IsSIFL; }
                                else
                                { chkSiflSecurity.Checked = false; }
                                if (objPassenger[0].SSN != null)
                                { tbTsenseid.Text = objPassenger[0].SSN.ToString(); }
                                if (objPassenger[0].Title != null)
                                { tbTitle.Text = objPassenger[0].Title.ToString(); }
                                if (objPassenger[0].SalaryLevel != null)
                                { tbSalaryLevel.Text = objPassenger[0].SalaryLevel.ToString(); }
                                if (objPassenger[0].Notes != null)
                                { tbNotes.Text = objPassenger[0].Notes.ToString(); }
                                if (objPassenger[0].PassengerAlert != null)
                                { tbPaxItems.Text = objPassenger[0].PassengerAlert.ToString(); }
                                if (tbPaxItems.Text.Trim() != "")
                                {
                                    btnPaxAlerts.CssClass = "ui_nav_ForeColor_Red";
                                }
                                else
                                {
                                    btnPaxAlerts.CssClass = "ui_nav";
                                }
                                if (tbNotes.Text.Trim() != "")
                                {
                                    btnNotes.CssClass = "ui_nav_ForeColor_Red";
                                }
                                else
                                {
                                    btnNotes.CssClass = "ui_nav";
                                }
                                // <new fields>
                                if (objPassenger[0].Addr3 != null)
                                { tbAddress3.Text = objPassenger[0].Addr3.ToString(); }
                                if (objPassenger[0].OtherEmail != null)
                                { tbOtherEmail.Text = objPassenger[0].OtherEmail.ToString(); }
                                if (objPassenger[0].PersonalEmail != null)
                                { tbPersonalEmail.Text = objPassenger[0].PersonalEmail.ToString(); }
                                if (objPassenger[0].OtherPhone != null)
                                { tbOtherPhone.Text = objPassenger[0].OtherPhone.ToString(); }
                                if (objPassenger[0].CellPhoneNum2 != null)
                                { tbSecondaryMobile.Text = objPassenger[0].CellPhoneNum2.ToString(); }
                                if (objPassenger[0].BusinessFax != null)
                                { tbBusinessFax.Text = objPassenger[0].BusinessFax.ToString(); }
                                if (objPassenger[0].PrimaryMobile != null)
                                { tbPrimaryMobile.Text = objPassenger[0].PrimaryMobile.ToString(); }

                                if (objPassenger[0].HotelPreferences != null)
                                { tbHotelPreferences.Text = objPassenger[0].HotelPreferences.ToString(); }
                                if (objPassenger[0].CateringPreferences != null)
                                { tbCateringPreferences.Text = objPassenger[0].CateringPreferences.ToString(); }
                                if (objPassenger[0].EmergencyContacts != null)
                                { tbEmergencyContact.Text = objPassenger[0].EmergencyContacts.ToString(); }
                                if (objPassenger[0].CreditcardInfo != null)
                                { tbCreditCardInfo.Text = objPassenger[0].CreditcardInfo.ToString(); }

                                if (objPassenger[0].PassengerWeight != null)
                                {
                                    tbPaxWeight.Text = Convert.ToString(Math.Round(Convert.ToDecimal(objPassenger[0].PassengerWeight.ToString()), 1));
                                }
                                else
                                {
                                    tbPaxWeight.Text = "000.0";
                                }
                                if (objPassenger[0].PassengerWeightUnit != null)
                                {
                                    if (objPassenger[0].PassengerWeightUnit.ToString().Trim() == "0")
                                    {
                                        dpPaxWeightUnits.SelectedValue = "0";
                                    }
                                    if (objPassenger[0].PassengerWeightUnit.ToString().Trim() == "1")
                                    {
                                        dpPaxWeightUnits.SelectedValue = "1";
                                    }
                                    if (objPassenger[0].PassengerWeightUnit.ToString().Trim() == "2")
                                    {
                                        dpPaxWeightUnits.SelectedValue = "2";
                                    }

                                }
                                if (objPassenger[0].Birthday != null)
                                {
                                    if (DateFormat != null)
                                    { tbBirthday.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(objPassenger[0].Birthday.ToString())); }
                                    //{ ((TextBox)ucBirthday.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(objPassenger[0].Birthday.ToString())); }
                                    else
                                    { tbBirthday.Text = objPassenger[0].Birthday.ToString(); }
                                    //{ ((TextBox)ucBirthday.FindControl("tbDate")).Text = objPassenger[0].Birthday.ToString(); }
                                }

                                if (objPassenger[0].Anniversaries != null)
                                {
                                    if (DateFormat != null)
                                    { tbAnniversaries.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(objPassenger[0].Anniversaries.ToString())); }
                                    //{ ((TextBox)ucAnniversaries.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(objPassenger[0].Anniversaries.ToString())); }
                                    else
                                    //{ ((TextBox)ucAnniversaries.FindControl("tbDate")).Text = objPassenger[0].Anniversaries.ToString(); }
                                    { tbAnniversaries.Text = objPassenger[0].Anniversaries.ToString(); }
                                }


                                if (objPassenger[0].CardExpires != null)
                                {
                                    if (DateFormat != null)
                                    { tbCardExpires.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(objPassenger[0].CardExpires.ToString())); }
                                    //{ ((TextBox)ucCardExpires.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(objPassenger[0].CardExpires.ToString())); }
                                    else
                                    { tbCardExpires.Text = objPassenger[0].CardExpires.ToString(); }
                                    //{ ((TextBox)ucCardExpires.FindControl("tbDate")).Text = objPassenger[0].CardExpires.ToString(); }
                                }
                                if (!string.IsNullOrEmpty(objPassenger[0].GreenNationalityCD))
                                {
                                    tbGreenNationality.Text = objPassenger[0].GreenNationalityCD;
                                }
                                if (!string.IsNullOrEmpty(objPassenger[0].INSANumber))
                                {
                                    tbINSANumber.Text = objPassenger[0].INSANumber;
                                }
                                //3088
                                //if (objPassenger[0].GreenCardGender != null)
                                //{
                                //    if (objPassenger[0].GreenCardGender == "0")
                                //    {
                                //        rbPaxMale.Checked = true;
                                //        rbPaxFemale.Checked = false;
                                //    }
                                //    else
                                //    {
                                //        rbPaxFemale.Checked = true;
                                //        rbPaxMale.Checked = false;
                                //    }
                                //}
                                //else
                                //{
                                //    rbPaxMale.Checked = true;
                                //}
                                if (objPassenger[0].ResidentSinceYear != null)
                                {
                                    tbResidentSinceYear.Text = objPassenger[0].ResidentSinceYear.ToString();
                                }
                                if (!string.IsNullOrEmpty(objPassenger[0].Category))
                                {
                                    tbCategory.Text = objPassenger[0].Category;
                                }


                                // <\new fields>
                                //if (objPassenger[0].TSADTTM != null)
                                //{
                                //    if (DateFormat != null)
                                //    { hdnTSATSADTTM.Value = String.Format("{0:" + DateFormat + "}", GetDateWithoutSeconds(Convert.ToDateTime(objPassenger[0].TSADTTM.ToString()))); }
                                //    else
                                //    { hdnTSATSADTTM.Value = GetDateWithoutSeconds(Convert.ToDateTime(objPassenger[0].TSADTTM.ToString())); }
                                //}
                                //if (objPassenger[0].TSAStatus != null)
                                //{
                                //    hdnTSAStatus.Value = objPassenger[0].TSAStatus.ToString();
                                //}
                                if (CompanySett.IsEnableTSAPX != null)
                                {
                                    if (CompanySett.IsEnableTSAPX == true)
                                    {
                                        if (objPassenger[0].TSAStatus != null)
                                        {
                                            string LastUpdTS = string.Empty;
                                            if (objPassenger[0].TSADTTM != null)
                                            {
                                                if (DateFormat != null)
                                                { LastUpdTS = String.Format("{0:" + DateFormat + "}", GetDateWithoutSeconds(Convert.ToDateTime(objPassenger[0].TSADTTM.ToString()))); }
                                                else
                                                { LastUpdTS = GetDateWithoutSeconds(Convert.ToDateTime(objPassenger[0].TSADTTM.ToString())); }
                                            }
                                            if (objPassenger[0].TSAStatus.ToString() == "R")
                                            {
                                                lbIsEnableTSAPX.Text = System.Web.HttpUtility.HtmlEncode("Potentially Restricted from flying by TSA as per list date " + LastUpdTS);
                                                lbIsEnableTSAPX.ForeColor = Color.Red;
                                                lbIsEnableTSAPX.BackColor = Color.Transparent;
                                            }
                                            else if (objPassenger[0].TSAStatus.ToString() == "S")
                                            {
                                                lbIsEnableTSAPX.Text = System.Web.HttpUtility.HtmlEncode("Part of selectee list, potentially restricted for flying by TSA as Per List Dated " + LastUpdTS);
                                                lbIsEnableTSAPX.ForeColor = Color.Black;
                                                lbIsEnableTSAPX.BackColor = Color.Yellow;
                                            }
                                            else if (objPassenger[0].TSAStatus.ToString() == "C")
                                            {
                                                lbIsEnableTSAPX.Text = System.Web.HttpUtility.HtmlEncode("Previously Restricted, now Cleared for flying by TSA as per list dated " + LastUpdTS);
                                                lbIsEnableTSAPX.ForeColor = Color.Green;
                                                lbIsEnableTSAPX.BackColor = Color.Transparent;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        lbIsEnableTSAPX.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        lbIsEnableTSAPX.ForeColor = Color.Black;
                                        lbIsEnableTSAPX.BackColor = Color.Transparent;
                                    }
                                }

                                // Bind Passenger Additional Information, Visa, Passport from DB
                                ClearCheckListFields();
                                //Session.Remove("PaxCheckList");
                                BindCrewCheckListGrid(objPassenger[0].PassengerRequestorID);
                                BindAdditionalInfoGrid(Convert.ToInt64(objPassenger[0].PassengerRequestorID));
                                BindVisaGrid(Convert.ToInt64(objPassenger[0].PassengerRequestorID));
                                BindPassportGrid(Convert.ToInt64(objPassenger[0].PassengerRequestorID));
                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (objPassenger[0].LastUpdUID != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + objPassenger[0].LastUpdUID.ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                }
                                if (objPassenger[0].LastUpdTS != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(objPassenger[0].LastUpdTS.ToString())));// objPassenger[0].LastUpdTS").ToString();
                                }
                                #region Get Image from Database
                                CreateDictionayForImgUpload();
                                imgFile.ImageUrl = "";
                                ImgPopup.ImageUrl = null;
                                lnkFileName.NavigateUrl = "";
                                using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var ReturnImage = ImgService.GetFileWarehouseList("Passenger", Convert.ToInt64(Session["PassengerRequestorID"].ToString())).EntityList;
                                    ddlImg.Items.Clear();
                                    ddlImg.Text = "";
                                    int i = 0;
                                    Session["Base64"] = null;   //added for image issue
                                    foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImage)
                                    {
                                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                                        string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FWH.UWAFileName);
                                        ddlImg.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                                        DicImage.Add(FWH.UWAFileName, FWH.UWAFilePath);
                                        if (i == 0)
                                        {
                                            byte[] picture = FWH.UWAFilePath;

                                            //Ramesh: Set the URL for image file or link based on the file type
                                            SetURL(FWH.UWAFileName, picture);

                                            ////start of modification for image issue
                                            //if (hdnBrowserName.Value == "Chrome")
                                            //{
                                            //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                            //}
                                            //else
                                            //{
                                            //    Session["Base64"] = picture;
                                            //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?cd=" + Guid.NewGuid());
                                            //}
                                            ////end of modification for image issue
                                        }
                                        i = i + 1;
                                    }
                                    if (ddlImg.Items.Count > 0)
                                    {
                                        ddlImg.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                        lnkFileName.NavigateUrl = "";
                                    }
                                }
                                #endregion
                                //break;

                                lbColumnName1.Text = System.Web.HttpUtility.HtmlEncode("PAX Code");
                                lbColumnName2.Text = System.Web.HttpUtility.HtmlEncode("Passengers/Requestors");
                                lbColumnValue1.Text = System.Web.HttpUtility.HtmlEncode(objPassenger[0].PassengerRequestorCD ?? "");
                                lbColumnValue2.Text = System.Web.HttpUtility.HtmlEncode(objPassenger[0].PassengerName ?? "");
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private FlightPakMasterService.Passenger GetItems(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {
                    if (IsEmptyCheck)
                    {
                        if (hdnSave.Value == "Update")
                        {
                           // System.Web.HttpUtility.HtmlEncode(objPassenger[0].PassengerRequestorID.ToString());
                           // dgPassenger
                            oPassenger.PassengerRequestorID = Convert.ToInt64(hdnCode.Value);
                            oPassenger.CustomerID = Convert.ToInt64(UserPrincipal.Identity._customerID);
                            
                        }
                        oPassenger.PassengerRequestorCD = GetPassengerCode(CompanySett);
                        oPassenger.FirstName = tbFirstName.Text;
                        oPassenger.LastName = tbLastName.Text;
                        if (!String.IsNullOrEmpty(tbMiddleName.Text))
                        {
                            oPassenger.MiddleInitial = tbMiddleName.Text;
                        }
                        oPassenger.PassengerName = tbLastName.Text + ", " + tbFirstName.Text + " " + tbMiddleName.Text;
                        if (!String.IsNullOrEmpty(tbCompany.Text))
                        {
                            oPassenger.CompanyName = tbCompany.Text;
                        }
                        oPassenger.CityOfBirth = tbCityOfBirth.Text;
                        oPassenger.Nickname = tbNickname.Text;
                        oPassenger.PaxTitle = tbPaxTitle.Text.Trim();



                        oPassenger.IsActive = chkActive1.Checked;
                        oPassenger.IsRequestor = chkRequestor.Checked;
                        oPassenger.IsScheduledServiceCoord = chkSchdServCoord.Checked;
                        if (!String.IsNullOrEmpty(tbEmpId.Text))
                        {
                            oPassenger.EmployeeID = tbEmpId.Text;
                        }
                        if (!String.IsNullOrEmpty(tbPaxPhone.Text))
                        {
                            oPassenger.PhoneNum = tbPaxPhone.Text;
                        }
                        if (!String.IsNullOrEmpty(hdnCQCustomerID.Value))
                        {
                            oPassenger.CQCustomerID = Convert.ToInt64(hdnCQCustomerID.Value);
                        }
                        if (!String.IsNullOrEmpty(tbAddlPhone.Text))
                        {
                            oPassenger.AdditionalPhoneNum = tbAddlPhone.Text;
                        }
                        if (!String.IsNullOrEmpty(tbPaxFax.Text))
                        {
                            oPassenger.FaxNum = tbPaxFax.Text;
                        }
                        if (!String.IsNullOrEmpty(tbEmail.Text))
                        {
                            oPassenger.EmailAddress = tbEmail.Text;
                        }
                        if (!String.IsNullOrEmpty(tbNationality.Text))
                        {
                            oPassenger.CountryID = Convert.ToInt64(hdnNationality.Value);
                        }
                        if (!String.IsNullOrEmpty(tbCityOfResi.Text))
                        {
                            oPassenger.CountryOfResidenceID = Convert.ToInt64(hdnCityOfResi.Value);
                        }
                        if (!String.IsNullOrEmpty(tbAddress1.Text))
                        {
                            oPassenger.Addr1 = tbAddress1.Text;
                        }
                        if (!String.IsNullOrEmpty(tbAddress2.Text))
                        {
                            oPassenger.Addr2 = tbAddress2.Text;
                        }
                        if (!String.IsNullOrEmpty(tbCity.Text))
                        {
                            oPassenger.City = tbCity.Text;
                        }
                        if (!String.IsNullOrEmpty(tbState.Text))
                        {
                            oPassenger.StateName = tbState.Text;
                        }
                        if (!String.IsNullOrEmpty(tbPostal.Text))
                        {
                            oPassenger.PostalZipCD = tbPostal.Text;
                        }
                        if (rbMale.Checked == true)
                        {
                            oPassenger.Gender = "M";
                        }
                        else
                        {
                            oPassenger.Gender = "F";
                        }
                        if (!String.IsNullOrEmpty(tbPinNumber.Text))
                        {
                            oPassenger.PersonalIDNum = tbPinNumber.Text;
                        }
                        ////if (!String.IsNullOrEmpty(((TextBox)ucDateOfBirth.FindControl("tbDate")).Text))
                        ////{
                        ////    oPassenger.DateOfBirth = Convert.ToDateTime(FormatDate(((TextBox)ucDateOfBirth.FindControl("tbDate")).Text, DateFormat));
                        ////}
                        if (!String.IsNullOrEmpty(tbDateOfBirth.Text))
                        {
                            oPassenger.DateOfBirth = Convert.ToDateTime(FormatDate(tbDateOfBirth.Text, DateFormat));
                        }
                        if (!String.IsNullOrEmpty(tbClientCde.Text))
                        {
                            oPassenger.ClientID = Convert.ToInt64(hdnClientCde.Value);
                        }
                        if (!String.IsNullOrEmpty(tbHomeBase.Text))
                        {
                            oPassenger.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                        }
                        if (!String.IsNullOrEmpty(tbFlightPurpose.Text))
                        {
                            oPassenger.FlightPurposeID = Convert.ToInt64(hdnFlightPurpose.Value);
                        }
                        if (!String.IsNullOrEmpty(tbDepartment.Text))
                        {
                            oPassenger.DepartmentID = Convert.ToInt64(hdnDepartment.Value);
                            oPassenger.PassengerDescription = tbDepartment.Text;
                        }
                        if (!String.IsNullOrEmpty(tbAuth.Text))
                        {
                            oPassenger.AuthorizationID = Convert.ToInt64(hdnAuth.Value);
                            oPassenger.AuthorizationDescription = tbAuth.Text;
                        }
                        if (!String.IsNullOrEmpty(tbStdBilling.Text))
                        {
                            oPassenger.StandardBilling = tbStdBilling.Text;
                        }
                        if (!String.IsNullOrEmpty(tbAccountNumber.Text))
                        {
                            oPassenger.AccountID = Convert.ToInt64(hdnAccountNumber.Value);
                        }
                        if (!String.IsNullOrEmpty(tbNotes.Text))
                        {
                            oPassenger.Notes = tbNotes.Text;
                        }
                        if (!String.IsNullOrEmpty(tbPaxItems.Text))
                        {
                            oPassenger.PassengerAlert = tbPaxItems.Text;
                        }
                        if (radGuest.Checked == true)
                        {
                            oPassenger.IsEmployeeType = "G";
                            if (!String.IsNullOrEmpty(tbAssociated.Text))
                            {
                                oPassenger.AssociatedWithCD = tbAssociated.Text;
                            }
                        }
                        else if (radNonControlled.Checked == true)
                        {
                            oPassenger.IsEmployeeType = "N";
                        }
                        else if (radType.Checked == true)
                        {
                            oPassenger.IsEmployeeType = "C";
                        }
                        oPassenger.IsSIFL = chkSiflSecurity.Checked;
                        if (!String.IsNullOrEmpty(tbTsenseid.Text))
                        {
                            oPassenger.SSN = tbTsenseid.Text;
                        }
                        if (!String.IsNullOrEmpty(tbTitle.Text))
                        {
                            oPassenger.Title = tbTitle.Text;
                        }
                        if (!String.IsNullOrEmpty(tbSalaryLevel.Text))
                        {
                            oPassenger.SalaryLevel = Convert.ToDecimal(tbSalaryLevel.Text);
                        }
                        //else
                        //{
                        //    oPassenger.SalaryLevel = 0;
                        //}
                        // <new fields added>
                        //if (!string.IsNullOrEmpty(hdnTSAStatus.Value))
                        //{
                        //    oPassenger.TSAStatus = hdnTSAStatus.Value;
                        //}
                        //if (!string.IsNullOrEmpty(hdnTSATSADTTM.Value))
                        //{
                        //    oPassenger.TSADTTM = Convert.ToDateTime(hdnTSATSADTTM.Value);
                        //}
                        if (!string.IsNullOrEmpty(tbAddress3.Text))
                        {
                            oPassenger.Addr3 = tbAddress3.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOtherEmail.Text))
                        {
                            oPassenger.OtherEmail = tbOtherEmail.Text;
                        }
                        if (!string.IsNullOrEmpty(tbPersonalEmail.Text))
                        {
                            oPassenger.PersonalEmail = tbPersonalEmail.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOtherPhone.Text))
                        {
                            oPassenger.OtherPhone = tbOtherPhone.Text;
                        }
                        if (!string.IsNullOrEmpty(tbSecondaryMobile.Text))
                        {
                            oPassenger.CellPhoneNum2 = tbSecondaryMobile.Text;  // Secondary Mobile / Cell
                        }
                        if (!string.IsNullOrEmpty(tbBusinessFax.Text))
                        {
                            oPassenger.BusinessFax = tbBusinessFax.Text;
                        }
                        if (!string.IsNullOrEmpty(tbPrimaryMobile.Text))
                        {
                            oPassenger.PrimaryMobile = tbPrimaryMobile.Text;
                        }


                        if (!string.IsNullOrEmpty(tbHotelPreferences.Text))
                        {
                            oPassenger.HotelPreferences = tbHotelPreferences.Text;
                        }
                        if (!string.IsNullOrEmpty(tbCateringPreferences.Text))
                        {
                            oPassenger.CateringPreferences = tbCateringPreferences.Text;
                        }
                        if (!string.IsNullOrEmpty(tbEmergencyContact.Text))
                        {
                            oPassenger.EmergencyContacts = tbEmergencyContact.Text;
                        }
                        if (!string.IsNullOrEmpty(tbCreditCardInfo.Text))
                        {
                            oPassenger.CreditcardInfo = tbCreditCardInfo.Text;
                        }


                        if (!string.IsNullOrEmpty(tbPaxWeight.Text))
                        {
                            oPassenger.PassengerWeight = Convert.ToDecimal(tbPaxWeight.Text);
                        }
                        if (dpPaxWeightUnits.SelectedValue == "0")
                        {
                            oPassenger.PassengerWeightUnit = "0";
                        }
                        if (dpPaxWeightUnits.SelectedValue == "1")
                        {
                            oPassenger.PassengerWeightUnit = "1";
                        }
                        if (dpPaxWeightUnits.SelectedValue == "2")
                        {
                            oPassenger.PassengerWeightUnit = "2";
                        }

                        //if (!String.IsNullOrEmpty(((TextBox)ucBirthday.FindControl("tbDate")).Text))
                        //{
                        //    oPassenger.Birthday = Convert.ToDateTime(FormatDate(((TextBox)ucBirthday.FindControl("tbDate")).Text, DateFormat));
                        //}
                        //if (!String.IsNullOrEmpty(((TextBox)ucAnniversaries.FindControl("tbDate")).Text))
                        //{
                        //    oPassenger.Anniversaries = Convert.ToDateTime(FormatDate(((TextBox)ucAnniversaries.FindControl("tbDate")).Text, DateFormat));
                        //}

                        if (!String.IsNullOrEmpty(tbBirthday.Text))
                        {
                            oPassenger.Birthday = Convert.ToDateTime(FormatDate(tbBirthday.Text, DateFormat));
                        }
                        if (!String.IsNullOrEmpty(tbAnniversaries.Text))
                        {
                            oPassenger.Anniversaries = Convert.ToDateTime(FormatDate(tbAnniversaries.Text, DateFormat));
                        }



                        //if (!String.IsNullOrEmpty(((TextBox)ucCardExpires.FindControl("tbDate")).Text))
                        //{
                        //    oPassenger.CardExpires = Convert.ToDateTime(FormatDate(((TextBox)ucCardExpires.FindControl("tbDate")).Text, DateFormat));
                        //}
                        if (!String.IsNullOrEmpty(tbCardExpires.Text))
                        {
                            oPassenger.CardExpires = Convert.ToDateTime(FormatDate(tbCardExpires.Text, DateFormat));
                        }
                        if (!string.IsNullOrEmpty(hdnGreenNationality.Value))
                        {
                            oPassenger.GreenCardCountryID = Convert.ToInt64(hdnGreenNationality.Value);
                        }
                        if (!string.IsNullOrEmpty(tbINSANumber.Text))
                        {
                            oPassenger.INSANumber = tbINSANumber.Text;
                        }
                        //3088
                        //if (rbPaxMale.Checked == true)
                        //{
                        //    oPassenger.GreenCardGender = "0";
                        //}
                        //else
                        //{
                        //    oPassenger.GreenCardGender = "1";
                        //}
                        if (!string.IsNullOrEmpty(tbResidentSinceYear.Text))
                        {
                            oPassenger.ResidentSinceYear = Convert.ToInt32(tbResidentSinceYear.Text);
                        }
                        if (!string.IsNullOrEmpty(tbCategory.Text))
                        {
                            oPassenger.Category = Convert.ToString(tbCategory.Text);
                        }

                        // </new fields>
                        oPassenger.IsDeleted = false;
                    }
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private string GetPassengerCode(FlightPakMasterService.Company Company)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Company))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<string>(() =>
                {
                    string PassengerCD = "";
                    if (CompanySett.IsAutoPassenger != true) 	
                    {
                        return PassengerCD = tbCode.Text.Trim();
                    }
                    //int FirstNameLen = 0, MiddleNameLen = 0, LastNameLen = 0;
                    int Count = 1;
                    while (Count <= 3)
                    {
                        if (Company.PassengerOFullName == Count)
                        {
                            if (tbFirstName.Text.Length > 0)
                            {
                                if ((Company.PassengerOLastFirst != null) && (Company.PassengerOLastFirst.Value > 0))
                                {
                                    //FirstNameLen = Company.PassengerOLastFirst.Value;
                                    PassengerCD = PassengerCD + MiscUtils.GetRegexExtractAlphaNumeric(tbFirstName.Text, Convert.ToInt32(Company.PassengerOLastFirst));
                                }
                                else if ((Company.PassengerOLastFirst.Value == 0) && (Company.PassengerOLastMiddle.Value == 0) && (Company.PassengerOLastLast.Value == 0))
                                {
                                    //FirstNameLen = 0;
                                    PassengerCD = PassengerCD + MiscUtils.GetRegexExtractAlphaNumeric(tbFirstName.Text, 1);
                                }
                            }
                        }
                        if (Company.PassengerOMiddle == Count)
                        {
                            if (tbMiddleName.Text.Length > 0)
                            {
                                if ((Company.PassengerOLastMiddle != null) && (Company.PassengerOLastMiddle.Value > 0))
                                {
                                    //MiddleNameLen = Company.PassengerOLastMiddle.Value;
                                    PassengerCD = PassengerCD + MiscUtils.GetRegexExtractAlphaNumeric(tbMiddleName.Text, Convert.ToInt32(Company.PassengerOLastMiddle));
                                }
                                else if ((Company.PassengerOLastFirst.Value == 0) && (Company.PassengerOLastMiddle.Value == 0) && (Company.PassengerOLastLast.Value == 0))
                                {
                                    //MiddleNameLen = 0;
                                    PassengerCD = PassengerCD + MiscUtils.GetRegexExtractAlphaNumeric(tbMiddleName.Text, 1);
                                }
                            }
                        }
                        if (Company.PassengerOLast == Count)
                        {
                            if (tbLastName.Text.Length > 0)
                            {
                                if ((Company.PassengerOLastLast != null) && (Company.PassengerOLastLast.Value > 0))
                                {
                                    //LastNameLen = Company.PassengerOLastLast.Value;
                                    PassengerCD = PassengerCD + MiscUtils.GetRegexExtractAlphaNumeric(tbLastName.Text, Convert.ToInt32(Company.PassengerOLastLast));
                                }
                                else if ((Company.PassengerOLastFirst.Value == 0) && (Company.PassengerOLastMiddle.Value == 0) && (Company.PassengerOLastLast.Value == 0))
                                {
                                    //LastNameLen = 0;
                                    PassengerCD = PassengerCD + MiscUtils.GetRegexExtractAlphaNumeric(tbLastName.Text, 1);
                                }
                            }
                        }
                        Count++;
                    }

                    //Added by Ramesh for Issue #2569, need to validate and proceed
                    //if (PassengerCD.Length <= (FirstNameLen + MiddleNameLen + LastNameLen))
                    //{
                    //    PassengerCD = PassengerCD.PadRight((FirstNameLen + MiddleNameLen + LastNameLen), '0');
                    //}
                    return PassengerCD;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void GetCompanyProfileInfo()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                {
                    DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                    hdnDateFormat.Value = DateFormat;
                    RadDatePicker1.DateInput.DateFormat = DateFormat;

                }
                else
                {
                    DateFormat = "MM/dd/yyyy";
                    hdnDateFormat.Value = DateFormat;
                    RadDatePicker1.DateInput.DateFormat = DateFormat;
                }
                using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == UserPrincipal.Identity._customerID && x.HomebaseID == UserPrincipal.Identity._homeBaseId).ToList();
                    if (objCompany.Count != 0)
                    {
                        CompanySett.PassengerOFullName = objCompany[0].PassengerOFullName;
                        CompanySett.PassengerOMiddle = objCompany[0].PassengerOMiddle;
                        CompanySett.PassengerOLast = objCompany[0].PassengerOLast;
                        CompanySett.PassengerOLastFirst = objCompany[0].PassengerOLastFirst;
                        CompanySett.PassengerOLastMiddle = objCompany[0].PassengerOLastMiddle;
                        CompanySett.PassengerOLastLast = objCompany[0].PassengerOLastLast;
                        CompanySett.IsEnableTSAPX = objCompany[0].IsEnableTSAPX;
                        CompanySett.IsAutoPassenger = objCompany[0].IsAutoPassenger == null ? false : objCompany[0].IsAutoPassenger;
                    }
                }
            }
        }
        private void GetDefaultFlightPurpose()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objFlightPurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    List<GetAllFlightPurposes> lstGetAllFlightPurposes = new List<GetAllFlightPurposes>();
                    lstGetAllFlightPurposes = objFlightPurposeService.GetAllFlightPurposeList().EntityList.Where(x => x.IsDefaultPurpose == true).ToList();
                    if (lstGetAllFlightPurposes.Count != 0)
                    {
                        lstGetAllFlightPurposes = lstGetAllFlightPurposes.OrderBy(x => x.FlightPurposeCD).ToList();
                        if (lstGetAllFlightPurposes.Count != 0)
                        {
                            tbFlightPurpose.Text = lstGetAllFlightPurposes[0].FlightPurposeCD;
                        }
                        else
                        {
                            tbFlightPurpose.Text = string.Empty;
                        }
                    }
                }
            }
        }
        #region "Passenger Grid"
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        protected void dgPassenger_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Data into Grid
        /// </summary>
        protected void dgPassenger_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetAllPassenger> PassengerList = new List<GetAllPassenger>();
                        //if (Session["PassengerList"] != null)
                        //{
                        //    PassengerList = (List<GetAllPassenger>)Session["PassengerList"];
                        //}
                        //else
                        //{
                        using (FlightPakMasterService.MasterCatalogServiceClient PassengerService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var PassengerInfo = PassengerService.GetPassengerList();
                            if (PassengerInfo.ReturnFlag == true)
                            {
                                PassengerList = PassengerInfo.EntityList.Where(x => x.IsDeleted == false).ToList<GetAllPassenger>();
                            }
                            dgPassenger.DataSource = PassengerList;
                            Session["PassengerList"] = PassengerList;
                            if (((chkSearchActiveOnly.Checked == true) || (chkSearchRequestorOnly.Checked == true) || (chkSearchHomebaseOnly.Checked == true) || (tbSearchClientCode.Text != string.Empty)) && !IsPopUp)
                            {
                                FilterAndSearch(false);
                            }
                        }
                        //}                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Data into Grid
        /// </summary>
        protected void dgPassenger_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                if (Session["PassengerRequestorID"] != null)
                                {
                                    btnEdit.Visible = false;
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                                        Session["IsEditLockPassengerRequestor"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                                            return;
                                        }
                                        SelectItem();
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        ddlImg.Enabled = true;
                                        if (imgFile.ImageUrl.Trim() != "" || lnkFileName.NavigateUrl.Trim() != "")
                                        {
                                            btndeleteImage.Enabled = true;
                                        }
                                        tbImgName.Enabled = true;
                                        SelectItem();
                                        //fileUL.Enabled = false;   //Commented for removing document name
                                        RadAjaxManager1.FocusControl(tbFirstName.ClientID); //tbFirstName.Focus();
                                        lbWarningMessage.Visible = false;
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgPassenger.SelectedIndexes.Clear();
                                btnEdit.Visible = false;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                EnableForm(true);
                                tbImgName.Enabled = true;
                                ddlImg.Enabled = true;
                                btndeleteImage.Enabled = true;
                                lbWarningMessage.Visible = false;
                                //fileUL.Enabled = false;   //Commented for removing document name
                                RadAjaxManager1.FocusControl(tbFirstName.ClientID); //tbFirstName.Focus();
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Update Command for Passenger Grid
        /// </summary>
        protected void dgPassenger_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if ((CheckCountryExist(tbNationality, cvNationality)) && (IsUpdate))
                        {
                            cvNationality.IsValid = false;
                            RadAjaxManager1.FocusControl(tbNationality.ClientID); //tbNationality.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckCountryExist(tbGreenNationality, cvGreenNationality)) && (IsUpdate))
                        {
                            cvGreenNationality.IsValid = false;
                            RadAjaxManager1.FocusControl(tbGreenNationality.ClientID); //tbNationality.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckCountryExist(tbCityOfResi, cvCityOfResi)) && (IsUpdate))
                        {
                            cvCityOfResi.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCityOfResi.ClientID); //tbCityOfResi.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckClientCodeExist(tbClientCde, cvClientCde)) && (IsUpdate))
                        {
                            cvClientCde.IsValid = false;
                            RadAjaxManager1.FocusControl(tbClientCde.ClientID); //tbClientCde.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckhomebaseExist(tbHomeBase)) && (IsUpdate))
                        {
                            cvHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckFlightPurposeExist(tbFlightPurpose)) && (IsUpdate))
                        {
                            cvFlightPurpose.IsValid = false;
                            RadAjaxManager1.FocusControl(tbFlightPurpose.ClientID); //tbFlightPurpose.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckDepartmentExist(tbDepartment)) && (IsUpdate))
                        {
                            RadAjaxManager1.FocusControl(tbDepartment.ClientID); //tbDepartment.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAuthorizationExist(tbAuth)) && (IsUpdate))
                        {
                            cvAuth.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAuth.ClientID); //tbAuth.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckCQExist(tbCQCustomer)) && (IsUpdate))
                        {
                            cvCQCustomer.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQCustomer.ClientID); //tbAuth.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAccountNumberExist(tbAccountNumber)) && (IsUpdate))
                        {
                            cvAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAccountNumber.ClientID); //tbAccountNumber.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAssociatedExist(tbAssociated)) && (IsUpdate))
                        {
                            cvAssociated.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAssociated.ClientID); //tbAssociated.Focus();
                            IsValidateCustom = false;
                        }
                        if (!ValidateVisa())
                        {
                            IsValidateCustom = false;
                        }
                        if (!ValidatePassport())
                        {
                            IsValidateCustom = false;
                        }
                        if (!DuplicatePassport() && IsValidateCustom == true)
                        {
                            //tabCrewRoster.SelectedIndex = 5;
                            //pnlPassport.ExpandMode = PanelBarExpandMode.FullExpandedItem;
                            IsValidateCustom = false;
                        }
                        if (IsValidateCustom)
                        {
                            using (MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient())
                            {
                                // Updating Passenger - AddlInfo, Visa, Passport
                                FlightPakMasterService.Passenger oPassenger = new FlightPakMasterService.Passenger();
                                oPassenger = GetItems(oPassenger);
                                SavePassengerAddlInfo(oPassenger);
                                SaveCrewChecklist(oPassenger);
                                SaveCrewVisa(oPassenger);
                                SavePassengerPassport(oPassenger);
                                var Result = PassengerService.UpdatePassengerRequestor(oPassenger);
                                if (Result.ReturnFlag == true)
                                {
                                    ResetPassengerSession(Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                                    UpdateFutureTripsheet(oPassenger);
                                    #region Image Upload in Edit Mode
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        foreach (var DicItem in dicImg)
                                        {
                                            //objService = new FlightPakMasterService.MasterCatalogServiceClient();
                                            FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                            Service.FileWarehouseID = 0;
                                            Service.RecordType = "Passenger";
                                            Service.UWAFileName = DicItem.Key;
                                            Service.RecordID = Convert.ToInt64(oPassenger.PassengerRequestorID);
                                            Service.UWAWebpageName = "PassengerCatalog.aspx";
                                            Service.UWAFilePath = DicItem.Value;
                                            Service.IsDeleted = false;
                                            Service.FileWarehouseID = 0;
                                            objService.AddFWHType(Service);
                                        }
                                    }
                                    #endregion
                                    #region Image Upload in Delete
                                    using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                        Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                        foreach (var DicItem in dicImgDelete)
                                        {
                                            objFWHType.UWAFileName = DicItem.Key;
                                            objFWHType.RecordID = Convert.ToInt64(oPassenger.PassengerRequestorID);
                                            objFWHType.RecordType = "Passenger";
                                            objFWHType.IsDeleted = true;
                                            objFWHTypeService.DeleteFWHType(objFWHType);
                                        }
                                    }
                                    #endregion
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                                        Session["IsEditLockPassengerRequestor"] = "False";
                                    }
                                    // Remove Passport Cache of Passenger
                                    CacheLite cacheObj = new CacheLite();
                                    foreach (var passport in oPassenger.CrewPassengerPassport)
                                    {                                     
                                        cacheObj.RemovePaxPassportFromCache(passport.PassportID);   
                                    }
                                    ShowSuccessMessage();

                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    GridEnable(true, true, true);
                                    dgPassenger.Rebind();
                                    btnEdit.Visible = true;
                                    SelectItem();
                                    ReadOnlyForm();
                                    _selectLastModified = true;
                                    ClearHdnSaveValue();
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.PassengerRequestor);
                                }
                            }
                            btnEdit.Visible = true;
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                Session.Remove("PassengerRequestorID");
                                if (Request.QueryString.HasKeys() && Request.QueryString["OpenFrom"] != null)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                }
                                else if (Request.QueryString.HasKeys() && Request.QueryString["IsPopup"] != null && !isPaxPassportPopup)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                }
                                else
                                {

                                    if (!isPaxPassportPopup)
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('rdPaxPassport');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        private void ResetPassengerSession(Int64 PassengerID)
        {
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (Trip.PreflightLegs != null)
                {
                    // Trip.PreflightLegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                    var Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    for (int LegCnt = 1; LegCnt <= Preflegs.Count; LegCnt++)
                    {
                        if (Preflegs[LegCnt - 1].IsDeleted == false)
                        {
                            if (Trip.PreflightLegPassengers.ContainsKey(LegCnt.ToString()))
                            {
                                PassengerViewModel objMatchedPass = Trip.PreflightLegPassengers[LegCnt.ToString()].Where(x => x.PassengerID == PassengerID).FirstOrDefault();
                                if (objMatchedPass != null)
                                {
                                    objMatchedPass.PassengerFirstName = tbFirstName.Text;
                                    objMatchedPass.PassengerLastName = tbLastName.Text;
                                    objMatchedPass.PassengerMiddleName = tbMiddleName.Text;
                                    objMatchedPass.PaxName = string.Format("{0}, {1} {2}", objMatchedPass.PassengerLastName, objMatchedPass.PassengerFirstName, objMatchedPass.PassengerMiddleName);
                                    objMatchedPass.PaxCode = tbCode.Text;
                                    Preflegs[LegCnt - 1].State = FlightPak.Web.PreflightService.TripEntityState.Modified;
                                    objMatchedPass.State = FlightPak.Web.PreflightService.TripEntityState.Modified;
                                }
                            }
                        }
                    }
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
                }

            }
        }
        /// <summary>
        /// Update Command for Passenger Grid
        /// </summary>
        protected void dgPassenger_InsertCommand(object source, GridCommandEventArgs e)
        {
            string passengerRequestorID = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if ((CheckCountryExist(tbNationality, cvNationality)) && (IsUpdate))
                        {
                            cvNationality.IsValid = false;
                            RadAjaxManager1.FocusControl(tbNationality.ClientID); //tbNationality.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckCountryExist(tbGreenNationality, cvGreenNationality)) && (IsUpdate))
                        {
                            cvGreenNationality.IsValid = false;
                            RadAjaxManager1.FocusControl(tbGreenNationality.ClientID); //tbNationality.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckCountryExist(tbCityOfResi, cvCityOfResi)) && (IsUpdate))
                        {
                            cvCityOfResi.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCityOfResi.ClientID); //tbCityOfResi.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckClientCodeExist(tbClientCde, cvClientCde)) && (IsUpdate))
                        {
                            cvClientCde.IsValid = false;
                            RadAjaxManager1.FocusControl(tbClientCde.ClientID); //tbClientCde.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckhomebaseExist(tbHomeBase)) && (IsUpdate))
                        {
                            cvHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckFlightPurposeExist(tbFlightPurpose)) && (IsUpdate))
                        {
                            cvFlightPurpose.IsValid = false;
                            RadAjaxManager1.FocusControl(tbFlightPurpose.ClientID); //tbFlightPurpose.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckDepartmentExist(tbDepartment)) && (IsUpdate))
                        {
                            RadAjaxManager1.FocusControl(tbDepartment.ClientID); //tbDepartment.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAuthorizationExist(tbAuth)) && (IsUpdate))
                        {
                            cvAuth.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAuth.ClientID); //tbAuth.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckCQExist(tbCQCustomer)) && (IsUpdate))
                        {
                            cvCQCustomer.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQCustomer.ClientID); //tbAuth.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAccountNumberExist(tbAccountNumber)) && (IsUpdate))
                        {
                            cvAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAccountNumber.ClientID); //tbAccountNumber.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAssociatedExist(tbAssociated)) && (IsUpdate))
                        {
                            cvAssociated.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAssociated.ClientID); //tbAssociated.Focus();
                            IsValidateCustom = false;
                        }
                        if (!ValidateVisa())
                        {
                            IsValidateCustom = false;
                        }
                        if (!ValidatePassport())
                        {
                            IsValidateCustom = false;
                        }
                        if (!DuplicatePassport() && IsValidateCustom == true)
                        {
                            //tabCrewRoster.SelectedIndex = 5;
                            pnlPassport.ExpandMode = PanelBarExpandMode.FullExpandedItem;
                            IsValidateCustom = false;
                        }
                        if (SaveConfirm == false)
                        {
                            
                            string FirstName = tbFirstName.Text.Trim();
                            string MiddleName = tbMiddleName.Text.Trim();
                            string LastName = tbLastName.Text.Trim();
                            int FirstNameLength = 0;
                            int MiddleNameLength = 0;
                            int LastNameLength = 0;

                            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._PassengerOLastFirst != null)
                            {
                                FirstNameLength = UserPrincipal.Identity._fpSettings._PassengerOLastFirst.Value;
                            }
                            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._PassengerOLastMiddle != null)
                            {
                                MiddleNameLength = UserPrincipal.Identity._fpSettings._PassengerOLastMiddle.Value;
                            }
                            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._PassengerOLastLast != null)
                            {
                                LastNameLength = UserPrincipal.Identity._fpSettings._PassengerOLastLast.Value;
                            }
                            hdnFirstOrLast.Value = "";
                            string Message = "";
                            if (FirstName.Length < FirstNameLength)
                            {
                                hdnFirstOrLast.Value = "First";
                                Message = "Your First Name length should not be less than  " + FirstNameLength;
                                //IsValidateCustom = false;
                            }
                            if (MiddleName.Length < MiddleNameLength)
                            {
                                hdnFirstOrLast.Value = "Middle";
                                Message += "<br/>Your Middle Name length should not be less than " + MiddleNameLength;
                                //IsValidateCustom = false;
                            }
                            if (LastName.Length < LastNameLength)
                            {

                                hdnFirstOrLast.Value = "Last";
                                Message += "<br/>Your Last Name length should not be less than " + LastNameLength;
                                //IsValidateCustom = false;
                            }

                            var PaxCode = GetPassengerCode(CompanySett);
                            if (string.IsNullOrWhiteSpace(PaxCode))
                            {
                                Message += "<br/>Your PAX code is not valid, Please check your company settings and Entered First, Middle & Last name.<br/>";
                            }

                            if (Message.Trim() != "")
                            {
                                RadWindowManager1.RadConfirm(Message + ", do you want to continue?", "confirmNameLengthFn", 330, 100, null, "Confirmation!");
                                IsValidateCustom = false;
                            }
                        }
                        if (IsValidateCustom)
                        {
                            using (MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient())
                            {
                                // Updating Passenger - AddlInfo, Visa, Passport
                                FlightPakMasterService.Passenger oPassenger = new FlightPakMasterService.Passenger();
                                oPassenger = GetItems(oPassenger);
                                SavePassengerAddlInfo(oPassenger);
                                SaveCrewChecklist(oPassenger);
                                SaveCrewVisa(oPassenger);
                                SavePassengerPassport(oPassenger);
                                var Result = PassengerService.AddPassengerRequestor(oPassenger);

                                // this value will be used to show newly added value in same page
                                //ViewState["addedPsngrReqId"] = Convert.ToString(Result.EntityInfo.PassengerRequestorID);
                                
                                if (Result.ReturnFlag == true)
                                {
                                    #region Image Upload in Edit Mode
                                    oPassenger.PassengerRequestorID = Result.EntityInfo.PassengerRequestorID;
                                    Session["NewPassengerRequestorID"] = Result.EntityInfo.PassengerRequestorID;
                                    Session["PassengerRequestorID"] = Result.EntityInfo.PassengerRequestorID;

                                    //Get actual passenger requestor cd 
                                    var actualResult =
                                        PassengerService.GetPassengerbyPassengerRequestorID(Result.EntityInfo.PassengerRequestorID);
                                    passengerRequestorID = actualResult.EntityList[0].PassengerRequestorCD;

                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        foreach (var DicItem in dicImg)
                                        {
                                            FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                            Service.FileWarehouseID = 0;
                                            Service.RecordType = "Passenger";
                                            Service.UWAFileName = DicItem.Key;
                                            Service.RecordID = Convert.ToInt64(oPassenger.PassengerRequestorID);
                                            Service.UWAWebpageName = "PassengerCatalog.aspx";
                                            Service.UWAFilePath = DicItem.Value;
                                            Service.IsDeleted = false;
                                            Service.FileWarehouseID = 0;
                                            objService.AddFWHType(Service);
                                        }
                                    }
                                    #endregion

                                    ShowSuccessMessage();

                                    DefaultSelection(true);

                                    dgPassenger.Rebind();
                                    btnEdit.Visible = true;
                                    
                                    GridEnable(true, true, true);
                                    _selectLastModified = true;
                                    ClearHdnSaveValue();
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.PassengerRequestor);
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.argument = '" + passengerRequestorID.Trim() + "'; oWnd.close();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
                finally
                {
                    dgPassenger.Rebind();
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        protected void dgPassenger_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            if (Session["PassengerRequestorID"] != null)
                            {
                                MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient();
                                Passenger oPassenger = new Passenger();
                                GridDataItem item = dgPassenger.SelectedItems[0] as GridDataItem;
                                oPassenger.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString());
                                oPassenger.PassengerRequestorCD = item["PassengerRequestorCD"].Text;
                                if (item.GetDataKeyValue("IsActive") != null)
                                {
                                    if (Convert.ToBoolean(item.GetDataKeyValue("IsActive")) == true)
                                    {
                                        
                                        string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                            + @" oManager.radalert('Active passenger cannot be deleted.', 360, 50, 'Passenger/ Requestor');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                        return;
                                    }
                                }
                                oPassenger.IsDeleted = true;
                                var returnValue = CommonService.Lock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                    if (IsPopUp)
                                        ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                                    else
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                                    return;
                                }
                                PassengerService.DeletePassengerRequestor(oPassenger);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(false);
                                lbWarningMessage.Visible = false;
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                    }
                    finally
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        protected void dgPassenger_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";
            ClearAddInfoAndPaxChecklistSession();
            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if ((btnSaveChanges.Visible == false) && (btnSaveChangesTop.Visible == false))
                            {
                                GridDataItem item = dgPassenger.SelectedItems[0] as GridDataItem;
                                Session["PassengerRequestorID"] = item["PassengerRequestorID"].Text;
                                Session["PassengerRequestorCD"] = item["PassengerRequestorCD"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        protected void dgPassenger_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            string color = System.Configuration.ConfigurationManager.AppSettings["UWAColor"];
                            GridDataItem dataItem = e.Item as GridDataItem;
                            GridColumn column = dgPassenger.MasterTableView.GetColumn("PassengerRequestorCD");
                            string PassengerRequestorCDValue = dataItem["PassengerRequestorCD"].Text;
                            if (PassengerRequestorCDValue.ToUpper().Trim() == "UWA")
                            {
                                System.Drawing.ColorConverter colConvert = new ColorConverter();
                                GridDataItem item = (GridDataItem)e.Item;
                                TableCell cell = (TableCell)item["PassengerRequestorCD"];
                                cell.ForeColor = (System.Drawing.Color)colConvert.ConvertFromString(color);
                                cell.Font.Bold = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void dgPassenger_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPassenger.ClientSettings.Scrolling.ScrollTop = "0";
                        PassengerRequestorPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void dgPassenger_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (PassengerRequestorPageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPassenger, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        #endregion
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgPassenger;
                        }
                        if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgPassenger;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sourceControl, eventArgument))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.RaisePostBackEvent(sourceControl, eventArgument);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        /// <summary>
        /// Command Event Trigger for Confirm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveConfirm = true;
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                    
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }

        }

        protected void btnSaveChangesTop_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Cancel Airport Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["PassengerRequestorID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim()));
                            }
                        }
                        ClearAddInfoAndPaxChecklistSession();
                        //Session.Remove("PassengerRequestorID");
                        DefaultSelection(false);
                        btnEdit.Visible = true;
                        lbWarningMessage.Visible = false;
                        if (IsPopUp)
                        {
                            if (Request.QueryString["IsPassport"] != null)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                            else if (Request.QueryString["IsPaxPassport"] != null)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('rdPaxPassport');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.reloadPassengers();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }

            ClearHdnSaveValue();
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        private void ClearHdnSaveValue()
        {
            hdnSave.Value = "";
        }

        private void ClearAddInfoAndPaxChecklistSession()
        {
            Session.Remove("PassengerAdditionalInfo");
            Session.Remove("PaxCheckList");
            BindAdditionalInfoGrid(0);
            BindCrewCheckListGrid(0);
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            if (Session["PassengerRequestorID"] != null)
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    var returnValue = CommonService.Lock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                    Session["IsEditLockPassengerRequestor"] = "True";
                    if (!returnValue.ReturnFlag)
                    {
                        if (IsPopUp)
                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                        else
                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                        return;
                    }
                    SelectItem();
                    DisplayEditForm();
                    GridEnable(false, true, false);
                    ddlImg.Enabled = true;
                    if (imgFile.ImageUrl.Trim() != "" || lnkFileName.NavigateUrl.Trim() != "")
                    {
                        btndeleteImage.Enabled = true;
                    }
                    tbImgName.Enabled = true;
                    btnEdit.Visible = false;
                    SelectItem();
                    //fileUL.Enabled = false;   //Commented for removing document name
                    RadAjaxManager1.FocusControl(tbFirstName.ClientID); //tbFirstName.Focus();
                }
            }
        }

        protected void lbtnTravelSense_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                string FilePath = string.Empty;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Passenger = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var PassengerInfo = Passenger.GetPassengerRequestorList();
                            string Quotes = "\"";
                            string Comma = ",";
                            System.Text.StringBuilder sbPassenger = new System.Text.StringBuilder();
                            if (PassengerInfo.ReturnFlag == true)
                            {
                                foreach (Passenger PassengerList in PassengerInfo.EntityList)
                                {
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.SSN);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.LastName);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.FirstName);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    if (!String.IsNullOrEmpty(PassengerList.Title))
                                    {
                                        sbPassenger.Append(PassengerList.Title);
                                    }
                                    else
                                    {
                                        sbPassenger.Append("PASSENGER");
                                    }
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.PassengerDescription);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.SalaryLevel);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.AppendLine();
                                }
                            }
                            //FilePath = Server.MapPath("~/" + "T$Pass.txt");
                            DateTime dt = DateTime.Today;
                            string ServerPath = Server.MapPath("~");
                            string Location = "Uploads\\" + "TPass\\" + dt.ToString("dd-MM-yyyy") + "\\";
                            string FolderPath = ServerPath + Location;
                            FilePath = FolderPath + "T$Pass.txt";

                            if (!Directory.Exists(FolderPath))
                            {
                                Directory.CreateDirectory(FolderPath);
                            }
                            if (File.Exists(FilePath))
                            {
                                File.Delete(FilePath);
                            }

                            File.WriteAllText(FilePath, sbPassenger.ToString(), System.Text.Encoding.ASCII);
                            Response.AppendHeader("content-disposition", "attachment; filename=" + "T$Pass.txt");
                            Response.ContentType = "text/plain";
                            Response.WriteFile(FilePath);
                            Response.Flush();
                            //Response.End();

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
                finally
                {
                    //Delete the File Creatd in Server : Karthik
                    File.Delete(FilePath);
                }
            }
        }
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Method to validate country code in the Passport grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void RadDatePicker1_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DateTime dt = new DateTime();
                        dt = DateTime.Now;
                        DateTime? bdate = new DateTime();
                        bdate = RadDatePicker1.SelectedDate;
                        TimeSpan age = new TimeSpan();
                        age = dt.Subtract(bdate.Value);
                        int years = (int)age.TotalDays / 365;
                        int months = ((int)age.TotalDays % 365) / 30;

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// To validate the whether the date is valid or not
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public bool IsValidDate(string datePart, string monthPart, string yearPart)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(datePart, monthPart, yearPart))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    DateTime date;
                    string strDate = string.Format("{0}-{1}-{2}", datePart, monthPart, yearPart, CultureInfo.CurrentCulture);
                    if (DateTime.TryParseExact(strDate, "dd-MM-yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None, out date))
                    {
                        return true;
                    }
                    else
                    {
                        if (strDate == "00-00-0000")
                        {
                            return false;
                            hdnDate.Value = string.Empty;
                        }
                        else
                        {
                            hdnDate.Value = "Error";
                            return false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #region "Validations"
        protected void btnshowdupes_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool checkUWARecord = true;
                        string CustomerID = "";
                        foreach (GridDataItem Item in dgPassenger.MasterTableView.Items)
                        {
                            if (Item["PassengerRequestorID"].Text.Trim() == Session["PassengerRequestorID"].ToString().Trim())
                            {
                                CustomerID = Item["CustomerID"].Text.Trim();
                                break;
                            }
                        }
                        string custnum = CustomerID;
                        if (tbCode.Text != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objPassenger = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objCustVal = objPassenger.GetPassengerRequestorList().EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Equals(tbCode.Text.ToString().ToUpper().Trim()) && x.CustomerID.ToString().ToUpper().Equals(custnum.ToString().ToUpper()));
                                if (objCustVal.Count() > 0 && objCustVal != null)
                                {
                                    cvCode.IsValid = false;
                                    checkUWARecord = false;
                                }
                                if (checkUWARecord)
                                {
                                    var objUWAVal = objPassenger.GetPassengerRequestorList().EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                                    if (objUWAVal.Count() > 0 && objUWAVal != null)
                                    {
                                        RadWindowManager1.RadConfirm("Passenger Code already exist. Please create code unique.", "confirmCallBackFn", 500, 30, null, "My Confirm");
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// To check unique Passenger Code on Tab Out
        /// </summary>
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (CompanySett.IsAutoPassenger != true)
                        {
                            string CustomerID = "";
                            foreach (GridDataItem Item in dgPassenger.MasterTableView.Items)
                            {
                                if (Item["PassengerRequestorID"].Text.Trim() == Session["PassengerRequestorID"].ToString().Trim())
                                {
                                    CustomerID = Item["CustomerID"].Text.Trim();
                                    break;
                                }
                            }
                            string custnum = CustomerID;
                            if (tbCode.Text != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objPassenger = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objPassVal = objPassenger.GetPassengerRequestorByPassengerRequestorCD(tbCode.Text.ToString().ToUpper().Trim());
                                    var objCustVal = objPassVal.EntityList.Where(x => x.CustomerID.ToString().ToUpper().Equals(custnum.ToString().ToUpper()));
                                    if (objCustVal.Count() > 0 && objCustVal != null)
                                    {
                                        cvCode.IsValid = false;
                                        string msgToDisplay = "Passenger Code already exists, please create unique code.";
                                        RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Passenger/Requestor", null, "Confirmation!");
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void tbNationality_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckCountryExist(tbNationality, cvNationality);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        protected void tbGreenNationality_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        CheckCountryExist(tbGreenNationality, cvGreenNationality);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void tbCityOfResi_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckCountryExist(tbCityOfResi, cvCityOfResi);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void tbClientCde_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckClientCodeExist(tbClientCde, cvClientCde);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void tbHomeBase_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckhomebaseExist(tbHomeBase);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void tbFlightPurpose_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckFlightPurposeExist(tbFlightPurpose);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        protected void tbCQ_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckCQExist(tbCQCustomer);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        protected void tbDepartment_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckDepartmentExist(tbDepartment);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void tbAuth_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAuthorizationExist(tbAuth);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void tbAccountNumber_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAccountNumberExist(tbAccountNumber);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void tbAssociated_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAssociatedExist(tbAssociated);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        private bool CheckCountryExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx, cval))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper().ToString() == txtbx.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                CountryList = (List<FlightPakMasterService.Country>)objRetVal.ToList();
                                if (CountryList != null && CountryList.Count > 0)
                                {
                                    if (txtbx.ID.Equals("tbNationality"))
                                    {
                                        hdnNationality.Value = CountryList[0].CountryID.ToString();
                                        tbNationalityName.Text = !string.IsNullOrEmpty(CountryList[0].CountryName) ? System.Web.HttpUtility.HtmlEncode(CountryList[0].CountryName) : string.Empty;
                                        tbNationality.Text = CountryList[0].CountryCD;
                                    }
                                    else if (txtbx.ID.Equals("tbGreenNationality"))
                                    {
                                        hdnGreenNationality.Value = CountryList[0].CountryID.ToString();
                                        tbGreenNationality.Text = CountryList[0].CountryCD;
                                    }
                                    else if (txtbx.ID.Equals("tbCityOfResi"))
                                    {
                                        hdnCityOfResi.Value = CountryList[0].CountryID.ToString();
                                        tbCityOfResi.Text = CountryList[0].CountryCD;
                                        tbCityOfResiName.Text = !string.IsNullOrEmpty(CountryList[0].CountryName) ? System.Web.HttpUtility.HtmlEncode(CountryList[0].CountryName) : string.Empty;
                                    }
                                }

                                returnVal = false;
                            }
                            else
                            {
                                if (txtbx.ID.Equals("tbNationality"))
                                {
                                    tbNationalityName.Text = string.Empty;
                                }
                                else if (txtbx.ID.Equals("tbCityOfResi"))
                                {
                                    tbCityOfResiName.Text = string.Empty;
                                }
                                cval.IsValid = false;
                                RadAjaxManager1.FocusControl(txtbx.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckClientCodeExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetClientByClientCD(txtbx.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.ClientByClientCDResult> ClientList = new List<FlightPakMasterService.ClientByClientCDResult>();
                                ClientList = (List<FlightPakMasterService.ClientByClientCDResult>)objRetVal.ToList();
                                if (txtbx.ID.Equals("tbClientCde"))
                                {
                                    hdnClientCde.Value = ClientList[0].ClientID.ToString();
                                    txtbx.Text = ClientList[0].ClientCD;
                                }
                                if (txtbx.ID.Equals("tbSearchClientCode"))
                                {
                                    txtbx.Text = ClientList[0].ClientCD;
                                }
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckhomebaseExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetAirportByAirportICaoID(txtbx.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetAllAirport> AirportList = new List<FlightPakMasterService.GetAllAirport>();
                                AirportList = (List<FlightPakMasterService.GetAllAirport>)objRetVal.ToList();
                                hdnHomeBase.Value = AirportList[0].AirportID.ToString();
                                txtbx.Text = AirportList[0].IcaoID;
                                returnVal = false;
                            }
                            else
                            {
                                cvHomeBase.IsValid = false;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckFlightPurposeExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeCD.Trim().ToUpper().ToString() == txtbx.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.FlightPurpose> FlightPurposeList = new List<FlightPakMasterService.FlightPurpose>();
                                FlightPurposeList = (List<FlightPakMasterService.FlightPurpose>)objRetVal.ToList();
                                hdnFlightPurpose.Value = FlightPurposeList[0].FlightPurposeID.ToString();
                                txtbx.Text = FlightPurposeList[0].FlightPurposeCD;
                                returnVal = false;
                            }
                            else
                            {
                                cvFlightPurpose.IsValid = false;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        private bool CheckCQExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            CQCustomer cq = new CQCustomer();
                            cq.CQCustomerCD = txtbx.Text.Trim();
                            var objRetVal = objCountrysvc.GetCQCustomerByCQCustomerCD(cq).EntityList.Where(x => x.CQCustomerCD.Trim().ToUpper().ToString() == txtbx.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetCQCustomerByCQCustomerCD> FlightPurposeList = new List<FlightPakMasterService.GetCQCustomerByCQCustomerCD>();
                                FlightPurposeList = (List<FlightPakMasterService.GetCQCustomerByCQCustomerCD>)objRetVal.ToList();
                                hdnCQCustomerID.Value = FlightPurposeList[0].CQCustomerID.ToString();
                                txtbx.Text = FlightPurposeList[0].CQCustomerCD;
                                returnVal = false;
                            }
                            else
                            {
                                cvCQCustomer.IsValid = false;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        private bool CheckDepartmentExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetAllDeptByDeptCD(txtbx.Text.Trim().ToUpper().ToString()).EntityList.Where(x => x.IsInActive == false).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetAllDeptByDeptCD> DepartmentList = new List<FlightPakMasterService.GetAllDeptByDeptCD>();
                                DepartmentList = (List<FlightPakMasterService.GetAllDeptByDeptCD>)objRetVal.ToList();
                                hdnDepartment.Value = DepartmentList[0].DepartmentID.ToString();
                                txtbx.Text = DepartmentList[0].DepartmentCD;
                                lbcvDepartment.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                tbDepartmentDesc.Text = !string.IsNullOrEmpty(DepartmentList[0].DepartmentName) ? System.Web.HttpUtility.HtmlEncode(DepartmentList[0].DepartmentName) : string.Empty;
                                returnVal = false;
                            }
                            else
                            {
                                hdnDepartment.Value = string.Empty;
                                lbcvDepartment.Text = System.Web.HttpUtility.HtmlEncode("Invalid Department Code");
                                tbAuth.Text = string.Empty;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    else
                    {
                        hdnDepartment.Value = string.Empty;
                        lbcvDepartment.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbAuth.Text = string.Empty;
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckAuthorizationExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetDepartmentAuthorizationList().EntityList.Where(x => x.AuthorizationCD.Trim().ToUpper().ToString() == txtbx.Text.Trim().ToUpper().ToString() && x.DepartmentID == Convert.ToInt64(hdnDepartment.Value) && x.IsInActive == false).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.DepartmentAuthorization> DepartmentAuthorizationList = new List<FlightPakMasterService.DepartmentAuthorization>();
                                DepartmentAuthorizationList = (List<FlightPakMasterService.DepartmentAuthorization>)objRetVal.ToList();
                                hdnAuth.Value = DepartmentAuthorizationList[0].AuthorizationID.ToString();
                                tbDepartmentDesc.Text = !string.IsNullOrEmpty(DepartmentAuthorizationList[0].DeptAuthDescription) ? System.Web.HttpUtility.HtmlEncode(DepartmentAuthorizationList[0].DeptAuthDescription) : string.Empty;
                                txtbx.Text = DepartmentAuthorizationList[0].AuthorizationCD;
                                returnVal = false;
                            }
                            else
                            {
                                cvAuth.IsValid = false;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckAccountNumberExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().ToString() == txtbx.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.Account> AccountList = new List<FlightPakMasterService.Account>();
                                AccountList = (List<FlightPakMasterService.Account>)objRetVal.ToList();
                                hdnAccountNumber.Value = AccountList[0].AccountID.ToString();
                                returnVal = false;
                            }
                            else
                            {
                                cvAccountNumber.IsValid = false;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckAssociatedExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetPassengerRequestorByPassengerRequestorCD(txtbx.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.PassengerRequestorResult> PassengerList = new List<FlightPakMasterService.PassengerRequestorResult>();
                                PassengerList = (List<FlightPakMasterService.PassengerRequestorResult>)objRetVal.ToList();
                                hdnAssociated.Value = PassengerList[0].PassengerRequestorID.ToString();
                                returnVal = false;
                                if (!string.IsNullOrEmpty(hdnAssociated.Value))
                                {
                                    var objRetVal1 = objCountrysvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(hdnAssociated.Value)).EntityList.Where(x => x.IsDeleted == false && x.IsEmployeeType != "G").ToList();
                                    if (objRetVal1.Count != 0)
                                    {
                                        List<FlightPakMasterService.GetPassengerByPassengerRequestorID> PassengerRequestorList = new List<FlightPakMasterService.GetPassengerByPassengerRequestorID>();
                                        PassengerRequestorList = (List<FlightPakMasterService.GetPassengerByPassengerRequestorID>)objRetVal1.ToList();
                                        hdnAssociated.Value = PassengerRequestorList[0].PassengerRequestorID.ToString();
                                        txtbx.Text = PassengerRequestorList[0].PassengerRequestorCD;
                                        returnVal = false;
                                    }
                                    else
                                    {
                                        hdnAssociated.Value = string.Empty;
                                        cvAssociated.IsValid = false;
                                        RadAjaxManager1.FocusControl(txtbx.ClientID);
                                        returnVal = true;
                                    }
                                }
                            }
                            else
                            {
                                cvAssociated.IsValid = false;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to check custom Validator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckCustomValidator()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((CheckCountryExist(tbNationality, cvNationality)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        RadAjaxManager1.FocusControl(tbNationality.ClientID); //tbNationality.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckCountryExist(tbCityOfResi, cvCityOfResi)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        RadAjaxManager1.FocusControl(tbCityOfResi.ClientID); //tbCityOfResi.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckClientCodeExist(tbClientCde, cvClientCde)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        RadAjaxManager1.FocusControl(tbClientCde.ClientID); //tbClientCde.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckhomebaseExist(tbHomeBase)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckFlightPurposeExist(tbFlightPurpose)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        RadAjaxManager1.FocusControl(tbFlightPurpose.ClientID); //tbFlightPurpose.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckDepartmentExist(tbDepartment)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        RadAjaxManager1.FocusControl(tbDepartment.ClientID); //tbDepartment.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckAuthorizationExist(tbAuth)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        RadAjaxManager1.FocusControl(tbAuth.ClientID); //tbAuth.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckAccountNumberExist(tbAccountNumber)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        RadAjaxManager1.FocusControl(tbAccountNumber.ClientID); //tbAccountNumber.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckAssociatedExist(tbAccountNumber)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        RadAjaxManager1.FocusControl(tbAccountNumber.ClientID); //tbAccountNumber.Focus();
                        IsValidateCustom = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Passenger Additional Information"
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                // Additional Info
                                // Declaring Lists
                                List<FlightPakMasterService.PassengerAdditionalInfo> finalList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                                List<FlightPakMasterService.PassengerAdditionalInfo> retainList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                                List<FlightPakMasterService.PassengerAdditionalInfo> newList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                                // Retain Grid Items and bind into List and add into Final List
                                retainList = RetainPassengerAddlInfoGrid(dgPassengerAddlInfo);
                                if (retainList != null)
                                {
                                    finalList.AddRange(retainList);
                                    // Bind Selected New Item and add into Final List
                                    if (Session["PassengerAdditionalInfoNew"] != null)
                                    {
                                        newList = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfoNew"];
                                        finalList.AddRange(newList);
                                    }
                                    // Bind final list into Session
                                    Session["PassengerAdditionalInfo"] = finalList;
                                    // Bind final list into Grid
                                    dgPassengerAddlInfo.DataSource = finalList.Where(x => x.IsDeleted == false).ToList();
                                    dgPassengerAddlInfo.DataBind();
                                    // Empty Newly added Session item
                                    Session.Remove("PassengerAdditionalInfoNew");
                                }
                                List<GetPaxChecklistDate> FinalList2 = new List<GetPaxChecklistDate>();
                                List<GetPaxChecklistDate> RetainList2 = new List<GetPaxChecklistDate>();
                                List<GetPaxChecklistDate> NewList2 = new List<GetPaxChecklistDate>();
                                // Retain Grid Items and bind into List and add into Final List
                                RetainList2 = RetainCrewCheckListGrid(dgCrewCheckList);
                                if (RetainList2 != null)
                                {
                                    FinalList2.AddRange(RetainList2);
                                    if (Session["PaxNewCheckList"] != null)
                                    {
                                        NewList2 = (List<GetPaxChecklistDate>)Session["PaxNewCheckList"];
                                        FinalList2.InsertRange(0, NewList2);
                                    }
                                    Session["PaxCheckList"] = FinalList2;
                                    dgCrewCheckList.DataSource = FinalList2.Where(x => x.IsDeleted == false).ToList();
                                    dgCrewCheckList.DataBind();
                                    Session.Remove("PaxNewCheckList");
                                    DefaultChecklistSelection();
                                }
                                //Case for Switch Operation
                                if (Session["NewPaxSelectID"] != null)
                                {
                                    dgPassenger.Rebind();
                                    ReadOnlyForm();
                                    DisplayEditForm();
                                    lbWarningMessage.Visible = false;
                                    Session.Remove("NewPaxSelectID");
                                }

                                break;
                        }

                        if (e.Argument.ToString() == "tbDepartment_OnTextChanged")
                            tbDepartment_OnTextChanged(sender, e);
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Delete Selected Crew Roster Additional Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteInfo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgPassengerAddlInfo.SelectedItems.Count > 0)
                        {
                            GridDataItem item = (GridDataItem)dgPassengerAddlInfo.SelectedItems[0];
                            Int64 AdditionalINFOID = Convert.ToInt64(item.GetDataKeyValue("PassengerAdditionalInfoID").ToString());
                            string AdditionalINFOCD = item.GetDataKeyValue("AdditionalINFOCD").ToString();
                            List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAddlInfo = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                            if (Session["PassengerAdditionalInfo"] == null)
                            {
                                if (Session["PassengerRequestorID"] != null)
                                    LoadPassengerAddInfoFromDB(Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                            }
                            PassengerAddlInfo = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfo"];
                            for (int Index = 0; Index < PassengerAddlInfo.Count; Index++)
                            {
                                if (PassengerAddlInfo[Index].AdditionalINFOCD == AdditionalINFOCD)
                                {
                                    PassengerAddlInfo[Index].IsDeleted = true;
                                }
                            }
                            Session["PassengerAdditionalInfo"] = PassengerAddlInfo;
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAdditionalInfoList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                            PassengerAdditionalInfoList = RetainPassengerAddlInfoGrid(dgPassengerAddlInfo);
                            dgPassengerAddlInfo.DataSource = PassengerAdditionalInfoList.Where(x => x.IsDeleted == false).ToList();
                            dgPassengerAddlInfo.DataBind();
                            Session["PassengerAdditionalInfo"] = PassengerAddlInfo;
                        }
                        else
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Please select the record from Additional Info table', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Optimized on 5/9/12 by Manish Varma
        /// Method to Bind Additional Information Grid, based on Crew Code
        /// </summary>
        /// <param name="crewCode">Pass Crew Code</param>
        private void BindAdditionalInfoGrid(Int64 PassengerRequestorID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorID))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["PassengerAdditionalInfo"] != null)
                    {
                        List<FlightPak.Web.FlightPakMasterService.PassengerAdditionalInfo> passengerInfo = 
                            (List<FlightPak.Web.FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfo"];
                        dgPassengerAddlInfo.DataSource = passengerInfo;
                        dgPassengerAddlInfo.DataBind();
                        //Session.Remove("PassengerAdditionalInfo");
                    }
                    else
                    {
                        LoadPassengerAddInfoFromDB(PassengerRequestorID);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void LoadPassengerAddInfoFromDB(Int64 PassengerRequestorID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorID))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                    {
                        var PassengerAddlInfo = Service.GetPassengerAddlInfoListReqID(PassengerRequestorID);
                        if (PassengerAddlInfo.ReturnFlag == true)
                        {
                            dgPassengerAddlInfo.DataSource = PassengerAddlInfo.EntityList.ToList();
                            dgPassengerAddlInfo.DataBind();
                            if (PassengerAddlInfo.EntityList.ToList().Count > 0)
                                Session["PassengerAdditionalInfo"] = PassengerAddlInfo.EntityList.ToList();
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Retain Additional Info Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Definition List</returns>
        private List<FlightPakMasterService.PassengerAdditionalInfo> RetainPassengerAddlInfoGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.PassengerAdditionalInfo>>(() =>
                {
                    List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAddlInfo = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                    PassengerAddlInfo = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfo"];
                    if (PassengerAddlInfo != null && PassengerAddlInfo.Count > 0)
                    {
                        for (int Index = 0; Index < PassengerAddlInfo.Count; Index++)
                        {
                            foreach (GridDataItem item in grid.MasterTableView.Items)
                            {
                                if (item.GetDataKeyValue("AdditionalINFOCD").ToString() == PassengerAddlInfo[Index].AdditionalINFOCD)
                                {
                                    PassengerAddlInfo[Index].AdditionalINFOValue = ((TextBox)item["AdditionalINFOValue"].FindControl("tbAddlInfo")).Text;
                                    PassengerAddlInfo[Index].IsPrint = ((CheckBox)item["IsPrint"].FindControl("chkIsPrint")).Checked;
                                    //PassengerAddlInfo[Index].PassengerInformationID = Convert.ToInt64(item["PassengerInformationID"].Text);
                                    break;
                                }
                            }
                        }
                    }
                    return PassengerAddlInfo;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        private FlightPakMasterService.Passenger SaveCrewChecklist(FlightPakMasterService.Passenger oPassenger)
        {
            //Soft Delete the previous Crew Checklist Items 
            //belongs to the specified Crew Code.

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (chkDisplayInactiveChecklist.Checked == true)
                    {
                        chkDisplayInactiveChecklist.Checked = false;
                        if (Session["PaxCheckList"] != null)
                        {
                            List<GetPaxChecklistDate> CrewCheckList = (List<GetPaxChecklistDate>)Session["PaxCheckList"];
                            dgCrewCheckList.DataSource = CrewCheckList;
                            dgCrewCheckList.DataBind();

                        }
                    }
                    oPassenger.PassengerCheckListDetail = new List<PassengerCheckListDetail>();
                    Int64 Indentity = 0;
                    List<FlightPakMasterService.GetPaxChecklistDate> CrewDefinitionInfoList = new List<FlightPakMasterService.GetPaxChecklistDate>();
                    FlightPakMasterService.PassengerCheckListDetail CrewCheckListDetail = new FlightPakMasterService.PassengerCheckListDetail();
                    CrewDefinitionInfoList = (List<FlightPakMasterService.GetPaxChecklistDate>)Session["PaxCheckList"];
                    if (CrewDefinitionInfoList == null || CrewDefinitionInfoList.Count == 0)
                        return oPassenger;
                    bool IsExist = false;
                    for (int Index = 0; Index < CrewDefinitionInfoList.Count; Index++)
                    {
                        IsExist = false;
                        CrewCheckListDetail = new PassengerCheckListDetail();
                        CrewCheckListDetail.PassengerID = oPassenger.PassengerRequestorID;
                        CrewCheckListDetail.CustomerID = Convert.ToInt64(UserPrincipal.Identity._customerID);
                        foreach (GridDataItem Item in dgCrewCheckList.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("PassengerChecklistCD").ToString() == CrewDefinitionInfoList[Index].PassengerChecklistCD.ToString()
                                && Item.GetDataKeyValue("PassengerCheckListDetailID").ToString() == CrewDefinitionInfoList[Index].PassengerCheckListDetailID.ToString())
                            {
                                
                                if (Convert.ToInt64(Item.GetDataKeyValue("PassengerCheckListDetailID").ToString()) != 0)
                                {
                                    CrewCheckListDetail.PassengerCheckListDetailID = Convert.ToInt64(Item.GetDataKeyValue("PassengerCheckListDetailID").ToString());
                                }
                                else
                                {
                                    Indentity = Indentity - 1;
                                    CrewCheckListDetail.PassengerCheckListDetailID = Indentity; // CrewINFO ID
                                }

                               if (CrewDefinitionInfoList[Index].IsDeleted)
                                {
                                    CrewCheckListDetail.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted;
                                }
                                if (Item.GetDataKeyValue("PassengerAdditionalInfoID") != null)
                                {
                                    CrewCheckListDetail.PassengerAdditionalInfoID = Convert.ToInt64((Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString()));
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.OriginalDT = Convert.ToDateTime(FormatDate(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.OriginalDT = Convert.ToDateTime(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.PreviousCheckDT = Convert.ToDateTime(FormatDate(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.PreviousCheckDT = Convert.ToDateTime(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["DueDT"].FindControl("lbDueDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.DueDT = Convert.ToDateTime(FormatDate(((Label)Item["DueDT"].FindControl("lbDueDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.DueDT = Convert.ToDateTime(((Label)Item["DueDT"].FindControl("lbDueDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.AlertDT = Convert.ToDateTime(FormatDate(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.AlertDT = Convert.ToDateTime(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                                {
                                    CrewCheckListDetail.Frequency = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text);
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.GraceDT = Convert.ToDateTime(FormatDate(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.GraceDT = Convert.ToDateTime(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text);
                                    }
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                                {
                                    CrewCheckListDetail.GraceDays = Convert.ToInt32(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                                {
                                    CrewCheckListDetail.AlertDays = Convert.ToInt32(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                                {
                                    CrewCheckListDetail.FrequencyMonth = Convert.ToInt32(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text);
                                }
                                CrewCheckListDetail.IsInActive = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                                CrewCheckListDetail.IsMonthEnd = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                                CrewCheckListDetail.IsStopCALC = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                                CrewCheckListDetail.IsOneTimeEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                                CrewCheckListDetail.IsCompleted = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                                CrewCheckListDetail.IsNoConflictEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                                CrewCheckListDetail.IsNoChecklistREPT = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                                CrewCheckListDetail.IsPassedDueAlert = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                                // CrewCheckListDetail.IsPrintStatus = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked;
                                CrewCheckListDetail.IsNextMonth = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                                CrewCheckListDetail.IsEndCalendarYear = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                                CrewCheckListDetail.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                                CrewCheckListDetail.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                                CrewCheckListDetail.IsDeleted = false;
                                IsExist = true;
                                break;
                            }
                        }
                        //IsExist
                        if (IsExist == false)
                        {
                            if (CrewDefinitionInfoList[Index].PassengerCheckListDetailID != 0)
                            {
                                CrewCheckListDetail.PassengerCheckListDetailID = CrewDefinitionInfoList[Index].PassengerCheckListDetailID;
                            }
                            else
                            {
                                Indentity = Indentity - 1;
                                CrewCheckListDetail.PassengerCheckListDetailID = Indentity; //CrewInfoID
                            }
                            //CrewCheckListDetail.PassengerID = CrewDefinitionInfoList[Index].PassengerID;
                            CrewCheckListDetail.CustomerID = CrewDefinitionInfoList[Index].CustomerID;

                            if (CrewDefinitionInfoList[Index].IsDeleted)
                            {
                                CrewCheckListDetail.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted;
                            }
                            CrewCheckListDetail.PassengerAdditionalInfoID = CrewDefinitionInfoList[Index].PassengerAdditionalInfoID;
                            CrewCheckListDetail.PreviousCheckDT = CrewDefinitionInfoList[Index].PreviousCheckDT;
                            CrewCheckListDetail.Frequency = CrewDefinitionInfoList[Index].Frequency;
                            CrewCheckListDetail.DueDT = CrewDefinitionInfoList[Index].DueDT;
                            CrewCheckListDetail.AlertDT = CrewDefinitionInfoList[Index].AlertDT;
                            CrewCheckListDetail.GraceDT = CrewDefinitionInfoList[Index].GraceDT;
                            CrewCheckListDetail.GraceDays = CrewDefinitionInfoList[Index].GraceDays;
                            CrewCheckListDetail.AlertDays = CrewDefinitionInfoList[Index].AlertDays;
                            CrewCheckListDetail.FrequencyMonth = CrewDefinitionInfoList[Index].FrequencyMonth;
                            CrewCheckListDetail.OriginalDT = CrewDefinitionInfoList[Index].OriginalDT;
                            CrewCheckListDetail.IsInActive = CrewDefinitionInfoList[Index].IsInActive;
                            CrewCheckListDetail.IsMonthEnd = CrewDefinitionInfoList[Index].IsMonthEnd;
                            CrewCheckListDetail.IsStopCALC = CrewDefinitionInfoList[Index].IsStopCALC;
                            CrewCheckListDetail.IsOneTimeEvent = CrewDefinitionInfoList[Index].IsOneTimeEvent;
                            CrewCheckListDetail.IsCompleted = CrewDefinitionInfoList[Index].IsCompleted;
                            CrewCheckListDetail.IsNoConflictEvent = CrewDefinitionInfoList[Index].IsNoConflictEvent;
                            CrewCheckListDetail.IsNoChecklistREPT = CrewDefinitionInfoList[Index].IsNoChecklistREPT;
                            CrewCheckListDetail.IsPassedDueAlert = CrewDefinitionInfoList[Index].IsPassedDueAlert;
                            CrewCheckListDetail.IsPrintStatus = CrewDefinitionInfoList[Index].IsPrintStatus;
                            CrewCheckListDetail.IsNextMonth = CrewDefinitionInfoList[Index].IsNextMonth;
                            CrewCheckListDetail.IsEndCalendarYear = CrewDefinitionInfoList[Index].IsEndCalendarYear;
                            CrewCheckListDetail.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                            CrewCheckListDetail.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                        }
                        if (CrewCheckListDetail.PassengerCheckListDetailID == 0)
                        {
                            Indentity = Indentity - 1;
                            CrewCheckListDetail.PassengerCheckListDetailID = Indentity;
                        }
                        oPassenger.PassengerCheckListDetail.Add(CrewCheckListDetail);
                    }
                    Session.Remove("PaxCheckList");
                    return oPassenger;

                }, FlightPak.Common.Constants.Policy.UILayer);
                return oPassenger;
            }
        }
        /// <summary>
        /// Method to Save Crew Definiton Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private FlightPakMasterService.Passenger SavePassengerAddlInfo(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {
                    // added information items from the Grid to List
                    oPassenger.PassengerAdditionalInfo = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                    Int64 Indentity = 0;
                    List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAdditionalInfoList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                    FlightPakMasterService.PassengerAdditionalInfo PassengerAddlInfoDef = new FlightPakMasterService.PassengerAdditionalInfo();
                    PassengerAdditionalInfoList = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfo"];
                    if (PassengerAdditionalInfoList == null || PassengerAdditionalInfoList.Count == 0)
                        return oPassenger;
                    for (int Index = 0; Index < PassengerAdditionalInfoList.Count; Index++)
                    {
                        foreach (GridDataItem Item in dgPassengerAddlInfo.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("AdditionalINFOCD").ToString() == PassengerAdditionalInfoList[Index].AdditionalINFOCD.ToString() && Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString() == PassengerAdditionalInfoList[Index].PassengerAdditionalInfoID.ToString())
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString()) != 0)
                                {
                                    PassengerAdditionalInfoList[Index].PassengerAdditionalInfoID = Convert.ToInt64(Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString());
                                }
                                else
                                {
                                    Indentity = Indentity - 1;
                                    PassengerAdditionalInfoList[Index].PassengerAdditionalInfoID = Indentity;
                                }
                                PassengerAdditionalInfoList[Index].PassengerRequestorID = oPassenger.PassengerRequestorID;
                                if (Item["PassengerInformationID"] != null && Item["PassengerInformationID"].Text != "&nbsp;" && Item["PassengerInformationID"].Text.Trim() != "")
                                {
                                    PassengerAdditionalInfoList[Index].PassengerInformationID = Convert.ToInt64(Item["PassengerInformationID"].Text);
                                }
                                else
                                {
                                    PassengerAdditionalInfoList[Index].PassengerInformationID = null;
                                }
                                if (Item.GetDataKeyValue("AdditionalINFODescription") != null)
                                {
                                    PassengerAdditionalInfoList[Index].AdditionalINFODescription = Item.GetDataKeyValue("AdditionalINFODescription").ToString();
                                }
                                if (((CheckBox)Item["IsPrint"].FindControl("chkIsPrint")) != null)
                                    PassengerAdditionalInfoList[Index].IsPrint = ((CheckBox)Item["IsPrint"].FindControl("chkIsPrint")).Checked;
                                else
                                    PassengerAdditionalInfoList[Index].IsPrint = false;
                                PassengerAdditionalInfoList[Index].AdditionalINFOValue = ((TextBox)Item["AdditionalINFOValue"].FindControl("tbAddlInfo")).Text;
                                PassengerAdditionalInfoList[Index].IsDeleted = false;
                                PassengerAdditionalInfoList[Index].CustomerID = oPassenger.CustomerID;
                                break;
                            }
                        }
                        if (PassengerAdditionalInfoList[Index].PassengerAdditionalInfoID == 0)
                        {
                            Indentity = Indentity - 1;
                            PassengerAdditionalInfoList[Index].PassengerAdditionalInfoID = Indentity;
                            PassengerAdditionalInfoList[Index].PassengerRequestorID = oPassenger.PassengerRequestorID;
                        }
                    }
                    oPassenger.PassengerAdditionalInfo = PassengerAdditionalInfoList;
                    Session.Remove("PassengerAdditionalInfo");
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Passenger Visa"
        /// <summary>
        /// Method to Add New Visa Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddVisa_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ValidateVisa())
                        {
                            // Declaring Lists
                            List<GetAllCrewPaxVisa> FinalList = new List<GetAllCrewPaxVisa>();
                            List<GetAllCrewPaxVisa> RetainList = new List<GetAllCrewPaxVisa>();
                            List<GetAllCrewPaxVisa> NewList = new List<GetAllCrewPaxVisa>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainVisaGrid(dgVisa);
                            FinalList.AddRange(RetainList);
                            // Bind Selected New Item and add into Final List
                            GetAllCrewPaxVisa CrewPaxVisa = new GetAllCrewPaxVisa();
                            CrewPaxVisa.VisaID = 0;
                            CrewPaxVisa.CrewID = null;
                            if (Session["PassengerRequestorID"] != null)
                            {
                                CrewPaxVisa.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                            }
                            else
                            {
                                CrewPaxVisa.PassengerRequestorID = 0;
                            }
                            NewList.Add(CrewPaxVisa);
                            FinalList.AddRange(NewList);
                            // Bind final list into Session
                            //Session["PassengerVisa"] = FinalList;
                            dgVisa.DataSource = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgVisa.Rebind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Method to Delete Visa Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteVisa_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgVisa.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgVisa.SelectedItems[0];
                            string VisaNumber = ((TextBox)(Item["VisaNum"].FindControl("tbVisaNum"))).Text;
                            Int64 CountryId = 0;
                            if (!String.IsNullOrEmpty(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value))
                            //if (((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value != null)
                            {
                                CountryId = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value);
                            }
                            List<GetAllCrewPaxVisa> CrewPaxVisaList = new List<GetAllCrewPaxVisa>();
                            CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["PassengerVisa"];
                            for (int Index = 0; Index < CrewPaxVisaList.Count; Index++)
                            {
                                if (CrewPaxVisaList[Index].VisaID != 0)
                                {
                                    if ((CrewPaxVisaList[Index].VisaNum == VisaNumber) && (CrewPaxVisaList[Index].CountryID.Value == CountryId))
                                    {
                                        CrewPaxVisaList[Index].IsDeleted = true;
                                    }
                                }
                                else
                                {
                                    if (CrewPaxVisaList[Index].VisaNum == VisaNumber)
                                    {
                                        CrewPaxVisaList[Index].IsDeleted = true;
                                    }
                                }
                            }
                            Session["PassengerVisa"] = CrewPaxVisaList;
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<GetAllCrewPaxVisa> CrewPaxList = new List<GetAllCrewPaxVisa>();
                            CrewPaxList = RetainVisaGrid(dgVisa);
                            for (int Index = 0; Index < CrewPaxList.Count; Index++)
                            {
                                if (CrewPaxList[Index].VisaID != 0)
                                {
                                    if ((CrewPaxList[Index].VisaNum == VisaNumber) && (CrewPaxList[Index].CountryID.Value == CountryId))
                                    {
                                        CrewPaxList[Index].IsDeleted = true;
                                    }
                                }
                                else
                                {
                                    if (CrewPaxList[Index].VisaNum == VisaNumber)
                                    {
                                        CrewPaxList[Index].IsDeleted = true;
                                    }
                                }
                            }
                            //CrewPaxList.RemoveAll(x => x.VisaNum.ToString().Trim().ToUpper() == VisaNumber.Trim().ToUpper());
                            dgVisa.DataSource = CrewPaxList.Where(x => x.IsDeleted != true).ToList();
                            dgVisa.DataBind();
                            Session["PassengerVisa"] = CrewPaxVisaList;
                        }
                        else
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Please select the record from Visa table', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Bind Crew Additional Information into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Visa_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //BindVisaGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Bind Crew Visa based on Crew Code
        /// </summary>
        /// <param name="crewCode">Pass Crew Code</param>
        private void BindVisaGrid(Int64 PassengerRequestorID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //if (Session["PassengerVisa"] != null)
                    //{
                    //    List<GetAllCrewPaxVisa> CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["PassengerVisa"];
                    //    dgVisa.DataSource = CrewPaxVisaList;
                    //    dgVisa.DataBind();
                    //}
                    //else
                    //{
                    using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                    {
                        List<GetAllCrewPaxVisa> CrewPassengerVisaList = new List<GetAllCrewPaxVisa>();
                        CrewPassengerVisa CrewVisa = new CrewPassengerVisa();
                        CrewVisa.CrewID = null;
                        CrewVisa.PassengerRequestorID = PassengerRequestorID;
                        var CrewPaxVisaList = Service.GetCrewVisaListInfo(CrewVisa);
                        if (CrewPaxVisaList.ReturnFlag == true)
                        {
                            CrewPassengerVisaList = CrewPaxVisaList.EntityList.Where(x => x.PassengerRequestorID == PassengerRequestorID && x.IsDeleted == false).ToList();
                            dgVisa.DataSource = CrewPassengerVisaList;
                            dgVisa.DataBind();
                            Session["PassengerVisa"] = CrewPassengerVisaList;
                        }
                    }
                    //}
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Retain Crew Passenger Visa Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Pax Visa List</returns>
        private List<GetAllCrewPaxVisa> RetainVisaGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<GetAllCrewPaxVisa>>(() =>
                {
                    List<GetAllCrewPaxVisa> CrewPaxVisaList = new List<GetAllCrewPaxVisa>();
                    GetAllCrewPaxVisa CrewPaxVisaListDef = new GetAllCrewPaxVisa();
                    //CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["PassengerVisa"];
                    //bool IsExist = false;
                    //for (int Index = 0; Index < CrewPaxVisaList.Count; Index++)
                    //{
                    foreach (GridDataItem Item in grid.MasterTableView.Items)
                    {
                        CrewPaxVisaListDef = new GetAllCrewPaxVisa();
                        //if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text == CrewPaxVisaList[Index].VisaNum) //Item.GetDataKeyValue("VisaNum")
                        //if ((Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString()) == CrewPaxVisaList[Index].VisaID) && (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text == CrewPaxVisaList[Index].VisaNum))
                        //if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text == CrewPaxVisaList[Index].VisaNum)
                        //{
                        CrewPaxVisaListDef.VisaID = Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString());
                        CrewPaxVisaListDef.CrewID = null;
                        if (Session["PassengerRequestorID"] != null)
                        {
                            CrewPaxVisaListDef.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                        }
                        else
                        {
                            CrewPaxVisaListDef.PassengerRequestorID = 0;
                        }
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value != string.Empty)
                        {
                            CrewPaxVisaListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value);
                            CrewPaxVisaListDef.CountryCD = ((TextBox)Item["CountryCD"].FindControl("tbCountryCD")).Text;
                        }
                        CrewPaxVisaListDef.VisaTYPE = string.Empty;
                        if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text != null)
                        {
                            CrewPaxVisaListDef.VisaNum = ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text;
                        }
                        else
                        {
                            CrewPaxVisaListDef.VisaNum = string.Empty;
                        }
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaListDef.ExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxVisaListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.IssuePlace = ((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text;
                        }
                        if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaListDef.IssueDate = Convert.ToDateTime(FormatDate(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxVisaListDef.IssueDate = Convert.ToDateTime(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text);
                            }
                        }

                        if (((DropDownList)Item["TypeOfVisa"].FindControl("ddlTypeOfVisa")).SelectedValue != "Select")
                        {
                            CrewPaxVisaListDef.TypeOfVisa = ((DropDownList)Item["TypeOfVisa"].FindControl("ddlTypeOfVisa")).SelectedValue.ToString();
                        }
                        else
                        {
                            CrewPaxVisaListDef.TypeOfVisa = "";
                        }

                        if (((TextBox)Item["EntriesAllowedInPassport"].FindControl("tbEntriesAllowedInPassport")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.EntriesAllowedInPassport = Convert.ToInt32(((TextBox)Item["EntriesAllowedInPassport"].FindControl("tbEntriesAllowedInPassport")).Text);
                        }

                        if (((TextBox)Item["VisaExpireInDays"].FindControl("tbVisaExpireInDays")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.VisaExpireInDays = Convert.ToInt32(((TextBox)Item["VisaExpireInDays"].FindControl("tbVisaExpireInDays")).Text);
                        }

                        if (((TextBox)Item["Notes"].FindControl("tbNotes")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.Notes = ((TextBox)Item["Notes"].FindControl("tbNotes")).Text;
                        }
                        CrewPaxVisaListDef.IsDeleted = false;
                        CrewPaxVisaList.Add(CrewPaxVisaListDef);
                        //IsExist = true;
                        //break;
                        //}
                    }
                    //if (IsExist == false)
                    //{
                    //}
                    //}
                    return CrewPaxVisaList;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Save Crew Visa Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private FlightPakMasterService.Passenger SaveCrewVisa(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {
                    // added information items from the Grid to List
                    oPassenger.CrewPassengerVisa = new List<FlightPakMasterService.CrewPassengerVisa>();
                    FlightPakMasterService.CrewPassengerVisa CrewPaxVisaInfoListDef = new FlightPakMasterService.CrewPassengerVisa();
                    List<FlightPakMasterService.GetAllCrewPaxVisa> CrewPaxVisaInfoList = new List<FlightPakMasterService.GetAllCrewPaxVisa>();
                    CrewPaxVisaInfoList = (List<GetAllCrewPaxVisa>)Session["PassengerVisa"];
                    List<FlightPakMasterService.CrewPassengerVisa> CrewPaxVisaGridInfo = new List<FlightPakMasterService.CrewPassengerVisa>();
                    Int64 Indentity = 0;
                    bool IsExist = false;
                    foreach (GridDataItem Item in dgVisa.MasterTableView.Items)
                    {
                        CrewPaxVisaInfoListDef = new CrewPassengerVisa();
                        if (Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString()) != 0)
                        {
                            CrewPaxVisaInfoListDef.VisaID = Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString());
                        }
                        else
                        {
                            Indentity = Indentity - 1;
                            CrewPaxVisaInfoListDef.VisaID = Indentity;
                        }
                        CrewPaxVisaInfoListDef.CrewID = null;
                        CrewPaxVisaInfoListDef.PassengerRequestorID = oPassenger.PassengerRequestorID;  //Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                        CrewPaxVisaInfoListDef.CustomerID = oPassenger.CustomerID;
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value);
                        }
                        CrewPaxVisaInfoListDef.VisaTYPE = string.Empty;
                        if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text != null)
                        {
                            CrewPaxVisaInfoListDef.VisaNum = ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text;
                        }
                        else
                        {
                            CrewPaxVisaInfoListDef.VisaNum = string.Empty;
                        }
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaInfoListDef.ExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxVisaInfoListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.IssuePlace = ((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text;
                        }
                        if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaInfoListDef.IssueDate = Convert.ToDateTime(FormatDate(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxVisaInfoListDef.IssueDate = Convert.ToDateTime(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text);
                            }
                        }
                        if (((TextBox)Item["EntriesAllowedInPassport"].FindControl("tbEntriesAllowedInPassport")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.EntriesAllowedInPassport = Convert.ToInt32(((TextBox)Item["EntriesAllowedInPassport"].FindControl("tbEntriesAllowedInPassport")).Text);
                        }
                        if (((TextBox)Item["VisaExpireInDays"].FindControl("tbVisaExpireInDays")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.VisaExpireInDays = Convert.ToInt32(((TextBox)Item["VisaExpireInDays"].FindControl("tbVisaExpireInDays")).Text);
                        }
                        if (((DropDownList)Item["TypeOfVisa"].FindControl("ddlTypeOfVisa")).SelectedValue != "Select")
                        {
                            CrewPaxVisaInfoListDef.TypeOfVisa = ((DropDownList)Item["TypeOfVisa"].FindControl("ddlTypeOfVisa")).SelectedValue.ToString();
                        }
                        else
                        {
                            CrewPaxVisaInfoListDef.TypeOfVisa = "";
                        }
                        if (((TextBox)Item["Notes"].FindControl("tbNotes")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.Notes = ((TextBox)Item["Notes"].FindControl("tbNotes")).Text;
                        }
                        CrewPaxVisaInfoListDef.IsDeleted = false;
                        CrewPaxVisaGridInfo.Add(CrewPaxVisaInfoListDef);
                    }
                    if (CrewPaxVisaInfoList != null)
                    {
                        for (int sIndex = 0; sIndex < CrewPaxVisaInfoList.Count; sIndex++)
                        {
                            IsExist = false;
                            for (int gIndex = 0; gIndex < CrewPaxVisaGridInfo.Count; gIndex++)
                            {
                                if ((CrewPaxVisaInfoList[sIndex].VisaNum == CrewPaxVisaGridInfo[gIndex].VisaNum) && (CrewPaxVisaInfoList[sIndex].VisaID == CrewPaxVisaGridInfo[gIndex].VisaID))
                                {
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false)
                            {
                                if (CrewPaxVisaInfoList[sIndex].VisaNum != null)
                                {
                                    if (CrewPaxVisaInfoList[sIndex].VisaID != 0)
                                    {
                                        CrewPaxVisaInfoListDef = new CrewPassengerVisa();
                                        CrewPaxVisaInfoListDef.CountryID = CrewPaxVisaInfoList[sIndex].CountryID;
                                        CrewPaxVisaInfoListDef.CrewID = CrewPaxVisaInfoList[sIndex].CrewID;
                                        CrewPaxVisaInfoListDef.CustomerID = CrewPaxVisaInfoList[sIndex].CustomerID;
                                        CrewPaxVisaInfoListDef.ExpiryDT = CrewPaxVisaInfoList[sIndex].ExpiryDT;
                                        CrewPaxVisaInfoListDef.IsDeleted = true;// CrewPaxVisaInfoList[sIndex].IsDeleted;
                                        CrewPaxVisaInfoListDef.IssueDate = CrewPaxVisaInfoList[sIndex].IssueDate;
                                        CrewPaxVisaInfoListDef.IssuePlace = CrewPaxVisaInfoList[sIndex].IssuePlace;
                                        CrewPaxVisaInfoListDef.LastUpdTS = CrewPaxVisaInfoList[sIndex].LastUpdTS;
                                        CrewPaxVisaInfoListDef.LastUpdUID = CrewPaxVisaInfoList[sIndex].LastUpdUID;
                                        CrewPaxVisaInfoListDef.Notes = CrewPaxVisaInfoList[sIndex].Notes;
                                        CrewPaxVisaInfoListDef.PassengerRequestorID = CrewPaxVisaInfoList[sIndex].PassengerRequestorID;
                                        if (CrewPaxVisaInfoList[sIndex].VisaID != 0)
                                        {
                                            CrewPaxVisaInfoListDef.VisaID = CrewPaxVisaInfoList[sIndex].VisaID;
                                        }
                                        else
                                        {
                                            Indentity = Indentity - 1;
                                            CrewPaxVisaInfoListDef.VisaID = Indentity;
                                        }
                                        CrewPaxVisaInfoListDef.VisaNum = CrewPaxVisaInfoList[sIndex].VisaNum;
                                        CrewPaxVisaInfoListDef.VisaTYPE = CrewPaxVisaInfoList[sIndex].VisaTYPE;
                                        CrewPaxVisaInfoListDef.EntriesAllowedInPassport = CrewPaxVisaInfoList[sIndex].EntriesAllowedInPassport;
                                        CrewPaxVisaInfoListDef.TypeOfVisa = CrewPaxVisaInfoList[sIndex].TypeOfVisa;
                                        CrewPaxVisaInfoListDef.VisaExpireInDays = CrewPaxVisaInfoList[sIndex].VisaExpireInDays;
                                        CrewPaxVisaGridInfo.Add(CrewPaxVisaInfoListDef);
                                    }
                                }
                            }
                        }
                    }
                    oPassenger.CrewPassengerVisa = CrewPaxVisaGridInfo;
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to validate country code in the Visa grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountryCD_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TextBox countryCD = new TextBox();
                        int rowIndex = 0;
                        int i = 0;
                        for (i = 0; i <= dgVisa.Items.Count - 1; i += 1)
                        {
                            countryCD.Text = ((TextBox)dgVisa.Items[rowIndex].Cells[0].FindControl("tbCountryCD")).Text;
                            if (countryCD.Text != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var RetVal = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper() == (countryCD.Text.Trim().ToUpper()));
                                    if (RetVal.Count() <= 0)
                                    {
                                        
                                        string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                            + @" oManager.radalert('Invalid Country Code', 360, 50, 'Passenger / Requestor');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                        ((TextBox)dgVisa.Items[rowIndex].Cells[0].FindControl("tbCountryCD")).Text = string.Empty;
                                        //((TextBox)dgVisa.Items[rowIndex].Cells[0].FindControl("tbCountryCD")).Focus();
                                        RadAjaxManager1.FocusControl(((TextBox)dgVisa.Items[rowIndex].Cells[0].FindControl("tbCountryCD")).ClientID);
                                    }
                                    else
                                    {
                                        List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                        CountryList = (List<FlightPakMasterService.Country>)RetVal.ToList();
                                        ((HiddenField)dgVisa.Items[rowIndex].Cells[0].FindControl("hdnCountryID")).Value = CountryList[0].CountryID.ToString();
                                        //((TextBox)dgVisa.Items[rowIndex].Cells[0].FindControl("tbVisaNum")).Focus();
                                        RadAjaxManager1.FocusControl(((TextBox)dgVisa.Items[rowIndex].Cells[0].FindControl("tbVisaNum")).ClientID);
                                    }
                                }
                            }
                            rowIndex += 1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Method to validate Issue Date and Expiry Date in the Visa grid
        /// </summary>
        protected bool ValidateVisa()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    string VisaNum = string.Empty;
                    string CountryCode = string.Empty;
                    string IssueDate = string.Empty;
                    string ExpiryDT = string.Empty;
                    foreach (GridDataItem item in dgVisa.Items)
                    {
                        VisaNum = ((TextBox)item.FindControl("tbVisaNum")).Text;
                        CountryCode = ((TextBox)item.FindControl("tbCountryCD")).Text;
                        IssueDate = ((TextBox)item.FindControl("tbIssueDate")).Text;
                        ExpiryDT = ((TextBox)item.FindControl("tbExpiryDT")).Text;
                        if (String.IsNullOrEmpty(CountryCode))
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Visa Country should not be Empty in Visa Section', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(VisaNum))
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Visa Number should not be Empty in Visa Section', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(ExpiryDT))
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Visa Expiry Date should not be Empty in Visa Section', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(IssueDate))
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Visa Issue Date should not be Empty in Visa Section', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateStart = new DateTime();
                        DateTime DateEnd = new DateTime();
                        if (!string.IsNullOrEmpty(IssueDate))
                        {
                            if (DateFormat != null)
                            {
                                DateStart = Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
                            }
                            else
                            {
                                DateStart = Convert.ToDateTime(IssueDate);
                            }
                        }
                        if (!string.IsNullOrEmpty(ExpiryDT))
                        {
                            if (DateFormat != null)
                            {
                                DateEnd = Convert.ToDateTime(FormatDate(ExpiryDT, DateFormat));
                            }
                            else
                            {
                                DateEnd = Convert.ToDateTime(ExpiryDT);
                            }
                        }
                        if (!String.IsNullOrEmpty(IssueDate))
                        {
                            if (DateStart > DateTime.Today)
                            {
                                
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Visa Issue Date should not be Future Date in Visa Section.', 360, 50, 'Passenger/ Requestor');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                        if (!(String.IsNullOrEmpty(IssueDate)) && !(String.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateStart > DateEnd)
                            {
                                
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Visa Issue Date should be before Expiry Date in Visa Section', 360, 50, 'Passenger/ Requestor');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                    }
                    return true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgVisa_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Item.GetDataKeyValue("ExpiryDT") != null)
                            {
                                ((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("ExpiryDT").ToString()));
                                //String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("ExpiryDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("IssueDate") != null)
                            {
                                ((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("IssueDate").ToString()));
                                //String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("IssueDate").ToString())));
                            }
                            if (Item.GetDataKeyValue("VisaID") != null)
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("VisaID")) <= 0)
                                {
                                    ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Enabled = true;
                                }
                                else
                                {
                                    ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Enabled = false;
                                }
                            }
                            GridEditableItem item = e.Item as GridEditableItem;
                            DropDownList list = item.FindControl("ddlTypeOfVisa") as DropDownList;
                            list.Items.Add("Select");
                            list.Items.Add("Business");
                            list.Items.Add("Crew");
                            list.Items.Add("Private");
                            list.Items.Add("Tourist");
                            list.Items.Add("Work");

                            string CurrentType;
                            CurrentType = Convert.ToString(Item.GetDataKeyValue("TypeOfVisa"));

                            if (!string.IsNullOrEmpty(CurrentType))
                            {
                                list.SelectedValue = Item.GetDataKeyValue("TypeOfVisa").ToString();
                            }
                            else
                            {
                                list.SelectedValue = "Select";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void dgVisa_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //dgVisa.CurrentPageIndex = e.NewPageIndex;
                        //dgVisa.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        #endregion
        #region "Passenger Passport"
        /// <summary>
        /// Method for Add New Passport Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddPassport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ValidatePassport())
                        {
                            // Declaring Lists
                            List<FlightPakMasterService.GetAllCrewPassport> FinalList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            List<FlightPakMasterService.GetAllCrewPassport> RetainList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            List<FlightPakMasterService.GetAllCrewPassport> NewList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainPassengerPassportGrid(dgPassport);
                            FinalList.AddRange(RetainList);
                            // Bind Selected New Item and add into Final List
                            FlightPakMasterService.GetAllCrewPassport PassengerPassport = new FlightPakMasterService.GetAllCrewPassport();
                            PassengerPassport.PassportID = FinalList.Count() + 1;
                            PassengerPassport.CrewID = null;
                            if (Session["PassengerRequestorID"] != null)
                            {
                                PassengerPassport.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                            }
                            else
                            {
                                PassengerPassport.PassengerRequestorID = 0;
                            }
                            NewList.Add(PassengerPassport);
                            FinalList.AddRange(NewList);
                            // Bind final list into Session
                            //Session["PassengerPassport"] = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.DataSource = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.Rebind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Method To Delete Passport Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeletePassport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgPassport.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgPassport.SelectedItems[0];
                            string PassportNumber = ((TextBox)(Item.FindControl("tbPassportNum"))).Text.Trim();

                            if (Item.GetDataKeyValue("PassportID") != null)
                            {
                                PassportNumber = Item.GetDataKeyValue("PassportID").ToString().Trim();
                            }

                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.GetAllCrewPassport> CrewPassportList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            CrewPassportList = (List<FlightPakMasterService.GetAllCrewPassport>)Session["PassengerPassport"];
                            for (int Index = 0; Index < CrewPassportList.Count; Index++)
                            {
                                if (CrewPassportList[Index].PassportID == Convert.ToInt64(PassportNumber))
                                {
                                    CrewPassportList[Index].IsDeleted = true;
                                }
                            }
                            Session["PassengerPassport"] = CrewPassportList;
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.GetAllCrewPassport> PassengerPassportList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            PassengerPassportList = RetainPassengerPassportGrid(dgPassport);
                            for (int Index = 0; Index < PassengerPassportList.Count; Index++)
                            {
                                if (PassengerPassportList[Index].PassportID == Convert.ToInt64(PassportNumber))
                                {
                                    PassengerPassportList[Index].IsDeleted = true;
                                }
                            }
                            //PassengerPassportList.RemoveAll(x => x.PassportNum.ToString().Trim().ToUpper() == PassportNumber.Trim().ToUpper());
                            dgPassport.DataSource = PassengerPassportList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.DataBind();
                            Session["PassengerPassport"] = CrewPassportList;
                        }
                        else
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Please select the record from Passport table.', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Bind Crew Additional Information into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Passport_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //BindPassportGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Bind Passport Info into Grid based on Crew Code
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private void BindPassportGrid(Int64 PassengerRequestorID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //if (Session["PassengerPassport"] != null)
                    //{
                    //    List<FlightPakMasterService.GetAllCrewPassport> PassengerPassport = (List<FlightPakMasterService.GetAllCrewPassport>)Session["PassengerPassport"];
                    //    dgPassport.DataSource = PassengerPassport;
                    //    dgPassport.DataBind();
                    //}
                    //else
                    //{
                    using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                    {
                        List<GetAllCrewPassport> PassengerPassportList = new List<GetAllCrewPassport>();
                        CrewPassengerPassport PassengerPassport = new CrewPassengerPassport();
                        PassengerPassport.CrewID = null;
                        PassengerPassport.PassengerRequestorID = PassengerRequestorID;
                        var objPassengerPassport = Service.GetCrewPassportListInfo(PassengerPassport);
                        if (objPassengerPassport.ReturnFlag == true)
                        {
                            PassengerPassportList = objPassengerPassport.EntityList.Where(x => x.PassengerRequestorID == PassengerRequestorID && x.IsDeleted == false).ToList();
                            dgPassport.DataSource = PassengerPassportList;
                            dgPassport.DataBind();
                            Session["PassengerPassport"] = PassengerPassportList;
                        }
                    }
                    // }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Retain Crew Passenger Passport Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        private List<FlightPakMasterService.GetAllCrewPassport> RetainPassengerPassportGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.GetAllCrewPassport>>(() =>
                {
                    List<GetAllCrewPassport> CrewPassportList = new List<GetAllCrewPassport>();
                    GetAllCrewPassport CrewPassportListDef = new GetAllCrewPassport();
                    //CrewPassportList = (List<GetAllCrewPassport>)Session["PassengerPassport"];
                    //for (int Index = 0; Index < CrewPassportList.Count; Index++)
                    //{
                    foreach (GridDataItem Item in grid.MasterTableView.Items)
                    {
                        CrewPassportListDef = new GetAllCrewPassport();
                        //if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text == CrewPassportList[Index].PassportNum)
                        //{
                        CrewPassportListDef.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                        if (Session["PassengerRequestorID"] != null)
                        {
                            CrewPassportListDef.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                        }
                        else
                        {
                            CrewPassportListDef.PassengerRequestorID = 0;
                        }
                        CrewPassportListDef.CrewID = null;
                        if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text != string.Empty)
                        {
                            CrewPassportListDef.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                        }
                        else
                        {
                            CrewPassportListDef.PassportNum = string.Empty;
                        }
                        CrewPassportListDef.Choice = ((CheckBox)Item["IsChoice"].FindControl("chkChoice")).Checked;
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPassportListDef.PassportExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPassportListDef.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text != string.Empty)
                        {
                            CrewPassportListDef.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                        }
                        if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPassportListDef.IssueDT = Convert.ToDateTime(FormatDate(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPassportListDef.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                            }
                        }
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value != string.Empty)
                        {
                            CrewPassportListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value);
                            CrewPassportListDef.CountryCD = ((TextBox)Item["CountryCD"].FindControl("tbPassportCountry")).Text;
                        }
                        CrewPassportListDef.IsDeleted = false;
                        CrewPassportList.Add(CrewPassportListDef);
                        //}
                    }
                    //}
                    return CrewPassportList;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Save Crew Passport Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private FlightPakMasterService.Passenger SavePassengerPassport(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {
                    // added information items from the Grid to List
                    oPassenger.CrewPassengerPassport = new List<FlightPakMasterService.CrewPassengerPassport>();
                    FlightPakMasterService.CrewPassengerPassport CrewPaxPassportInfoListDef = new FlightPakMasterService.CrewPassengerPassport();
                    List<FlightPakMasterService.GetAllCrewPassport> CrewPaxPassportInfoList = new List<FlightPakMasterService.GetAllCrewPassport>();
                    CrewPaxPassportInfoList = (List<GetAllCrewPassport>)Session["PassengerPassport"];
                    List<FlightPakMasterService.CrewPassengerPassport> CrewPaxPassportGridInfo = new List<FlightPakMasterService.CrewPassengerPassport>();
                    Int64 Indentity = 0;
                    bool IsExist = false;
                    foreach (GridDataItem Item in dgPassport.MasterTableView.Items)
                    {
                        CrewPaxPassportInfoListDef = new CrewPassengerPassport();
                        if (Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString()) >= 1000)
                        {
                            CrewPaxPassportInfoListDef.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                        }
                        else
                        {
                            Indentity = Indentity - 1;
                            CrewPaxPassportInfoListDef.PassportID = Indentity;
                        }
                        CrewPaxPassportInfoListDef.PassengerRequestorID = oPassenger.PassengerRequestorID;  //Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                        CrewPaxPassportInfoListDef.CustomerID = oPassenger.CustomerID;
                        CrewPaxPassportInfoListDef.CrewID = null;
                        if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                        }
                        else
                        {
                            CrewPaxPassportInfoListDef.PassportNum = string.Empty;
                        }
                        CrewPaxPassportInfoListDef.Choice = ((CheckBox)Item["IsChoice"].FindControl("chkChoice")).Checked;
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxPassportInfoListDef.PassportExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxPassportInfoListDef.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                        }
                        if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxPassportInfoListDef.IssueDT = Convert.ToDateTime(FormatDate(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text, DateFormat));
                            }
                            else
                            {
                                CrewPaxPassportInfoListDef.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                            }
                        }
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value);
                        }
                        CrewPaxPassportInfoListDef.IsDeleted = false;
                        CrewPaxPassportGridInfo.Add(CrewPaxPassportInfoListDef);
                    }
                    if (CrewPaxPassportInfoList != null)
                    {
                        for (int sIndex = 0; sIndex < CrewPaxPassportInfoList.Count; sIndex++)
                        {
                            IsExist = false;
                            for (int gIndex = 0; gIndex < CrewPaxPassportGridInfo.Count; gIndex++)
                            {
                                if ((CrewPaxPassportInfoList[sIndex].PassportID == CrewPaxPassportGridInfo[gIndex].PassportID) && (CrewPaxPassportInfoList[sIndex].PassportID == CrewPaxPassportGridInfo[gIndex].PassportID))
                                {
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false)
                            {
                                if (CrewPaxPassportInfoList[sIndex].PassportID != null)
                                {
                                    if (CrewPaxPassportInfoList[sIndex].PassportID >= 0)
                                    {
                                        CrewPaxPassportInfoListDef = new CrewPassengerPassport();
                                        CrewPaxPassportInfoListDef.CountryID = CrewPaxPassportInfoList[sIndex].CountryID;
                                        CrewPaxPassportInfoListDef.Choice = CrewPaxPassportInfoList[sIndex].Choice;
                                        CrewPaxPassportInfoListDef.CountryID = CrewPaxPassportInfoList[sIndex].CountryID;
                                        CrewPaxPassportInfoListDef.CrewID = CrewPaxPassportInfoList[sIndex].CrewID;
                                        CrewPaxPassportInfoListDef.CustomerID = CrewPaxPassportInfoList[sIndex].CustomerID;
                                        CrewPaxPassportInfoListDef.IsDefaultPassport = CrewPaxPassportInfoList[sIndex].IsDefaultPassport;
                                        CrewPaxPassportInfoListDef.IsDeleted = true;// CrewPaxPassportInfoList[sIndex].IsDeleted;
                                        CrewPaxPassportInfoListDef.IssueCity = CrewPaxPassportInfoList[sIndex].IssueCity;
                                        CrewPaxPassportInfoListDef.IssueDT = CrewPaxPassportInfoList[sIndex].IssueDT;
                                        CrewPaxPassportInfoListDef.LastUpdTS = CrewPaxPassportInfoList[sIndex].LastUpdTS;
                                        CrewPaxPassportInfoListDef.LastUpdUID = CrewPaxPassportInfoList[sIndex].LastUpdUID;
                                        CrewPaxPassportInfoListDef.PassengerRequestorID = CrewPaxPassportInfoList[sIndex].PassengerRequestorID;
                                        CrewPaxPassportInfoListDef.PassportExpiryDT = CrewPaxPassportInfoList[sIndex].PassportExpiryDT;
                                        CrewPaxPassportInfoListDef.PassportNum = CrewPaxPassportInfoList[sIndex].PassportNum;
                                        CrewPaxPassportInfoListDef.PilotLicenseNum = CrewPaxPassportInfoList[sIndex].PilotLicenseNum;
                                        if (CrewPaxPassportInfoList[sIndex].PassportID >= 0)
                                        {
                                            CrewPaxPassportInfoListDef.PassportID = CrewPaxPassportInfoList[sIndex].PassportID;
                                        }
                                        else
                                        {
                                            Indentity = Indentity - 1;
                                            CrewPaxPassportInfoListDef.PassportID = Indentity;
                                        }
                                        CrewPaxPassportGridInfo.Add(CrewPaxPassportInfoListDef);
                                    }
                                }
                            }
                        }
                    }
                    oPassenger.CrewPassengerPassport = CrewPaxPassportGridInfo;
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void Nation_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TextBox countryCD = new TextBox();
                        int rowIndex = 0;
                        int i = 0;
                        for (i = 0; i <= dgPassport.Items.Count - 1; i += 1)
                        {
                            countryCD.Text = ((TextBox)dgPassport.Items[rowIndex].Cells[0].FindControl("tbPassportCountry")).Text;
                            if (countryCD.Text != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var RetVal = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper() == (countryCD.Text.Trim().ToUpper()));
                                    if (RetVal.Count() <= 0)
                                    {
                                        
                                        string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                            + @" oManager.radalert('Invalid Country Code', 360, 50, 'Passenger / Requestor');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                        ((TextBox)dgPassport.Items[rowIndex].Cells[0].FindControl("tbPassportCountry")).Text = string.Empty;
                                        //((TextBox)dgPassport.Items[rowIndex].Cells[0].FindControl("tbPassportCountry")).Focus();
                                        RadAjaxManager1.FocusControl(((TextBox)dgPassport.Items[rowIndex].Cells[0].FindControl("tbPassportCountry")).ClientID);
                                    }
                                    else
                                    {
                                        List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                        CountryList = (List<FlightPakMasterService.Country>)RetVal.ToList();
                                        ((HiddenField)dgPassport.Items[rowIndex].Cells[0].FindControl("hdnPassportCountry")).Value = CountryList[0].CountryID.ToString();
                                        //((TextBox)dgPassport.Items[rowIndex].Cells[0].FindControl("tbPassportCountry")).Focus();
                                        RadAjaxManager1.FocusControl(((TextBox)dgPassport.Items[rowIndex].Cells[0].FindControl("tbPassportCountry")).ClientID);
                                    }
                                }
                            }
                            rowIndex += 1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// Method to validate Issue Date and Expiry Date in the Passport grid
        /// </summary>
        protected bool ValidatePassport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    string PassportNum = string.Empty;
                    string IssueDate = string.Empty;
                    string ExpiryDT = string.Empty;
                    string IssueCity = string.Empty;
                    string CountryName = string.Empty;
                    foreach (GridDataItem item in dgPassport.Items)
                    {
                        PassportNum = ((TextBox)item.FindControl("tbPassportNum")).Text;
                        IssueDate = ((TextBox)item.FindControl("tbIssueDT")).Text;
                        ExpiryDT = ((TextBox)item.FindControl("tbPassportExpiryDT")).Text;
                        IssueCity = ((TextBox)item.FindControl("tbIssueCity")).Text;
                        CountryName = ((TextBox)item.FindControl("tbPassportCountry")).Text;
                        if (String.IsNullOrEmpty(PassportNum))
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Passport Number should not be Empty in Passport Section', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(ExpiryDT))
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Passport Expiry Date should not be Empty in Passport Section', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        //if (String.IsNullOrEmpty(IssueDate))
                        //{
                        //    string alertMsg = "radalert('Passport Issue Date should not be empty', 360, 50, 'Passenger/ Requestor');";
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        //    return false;
                        //}
                        //if (String.IsNullOrEmpty(IssueCity))
                        //{
                        //    string alertMsg = "radalert('Passport Issue City should not be empty', 360, 50, 'Passenger/ Requestor');";
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        //    return false;
                        //}
                        if (!String.IsNullOrEmpty(CountryName))
                        {
                            //if (CountryName.Trim().ToUpper() == "US")
                            //{
                            //    Int64 Validity;
                            //    bool IsValidUSPassportNumber = Int64.TryParse(PassportNum, out Validity);
                            //    bool IsValidUSPassportLength = true;
                            //    if (PassportNum.Length != 9)
                            //    {
                            //        IsValidUSPassportLength = false;
                            //    }
                            //    if (IsValidUSPassportNumber == false || IsValidUSPassportLength == false)
                            //    {
                            //        string alertMsg = "radalert('Passport Number is not valid US Passport Number in Passport Section.', 360, 50, 'Passenger/ Requestor');";
                            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            //        return false;
                            //    }
                            //}
                        }
                        else
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Passport Country should not be Empty in Passport Section', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateStart = new DateTime();
                        DateTime DateEnd = new DateTime();
                        if (!(string.IsNullOrEmpty(IssueDate)))
                        {
                            if (DateFormat != null)
                            {
                                DateStart = Convert.ToDateTime(FormatDate(IssueDate, DateFormat));
                            }
                            else
                            {
                                DateStart = Convert.ToDateTime(IssueDate);
                            }
                        }
                        if (!(string.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateFormat != null)
                            {
                                DateEnd = Convert.ToDateTime(FormatDate(ExpiryDT, DateFormat));
                            }
                            else
                            {
                                DateEnd = Convert.ToDateTime(ExpiryDT);
                            }
                        }
                        if (!String.IsNullOrEmpty(IssueDate))
                        {
                            if (DateStart > DateTime.Today)
                            {
                                
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Passport Issue Date should not be Future Date in Passport Section.', 360, 50, 'Passenger/ Requestor');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                        if (!(String.IsNullOrEmpty(IssueDate)) && !(String.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateStart > DateEnd)
                            {
                                
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + " oManager.radalert('Passport Issue Date should be before Expiry Date in Passport Section', 360, 50, 'Passenger/ Requestor');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                    }
                    return true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        protected bool DuplicatePassport()
        {
            List<FlightPakMasterService.CrewPassengerPassport> CrewPassportList = new List<FlightPakMasterService.CrewPassengerPassport>();
            List<FlightPakMasterService.CrewPassengerPassport> DupePassportGridInfo = new List<FlightPakMasterService.CrewPassengerPassport>();


            foreach (GridDataItem Item in dgPassport.MasterTableView.Items)
            {
                CrewPassengerPassport CrewPassportListDef = new CrewPassengerPassport();
                CrewPassportListDef.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                if (Session["PassengerRequestorID"] != null)
                {
                    CrewPassportListDef.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                }
                else
                {
                    CrewPassportListDef.PassengerRequestorID = 0;
                }
                CrewPassportListDef.CrewID = null;
                if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text != string.Empty)
                {
                    CrewPassportListDef.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                }
                else
                {
                    CrewPassportListDef.PassportNum = string.Empty;
                }
                CrewPassportListDef.Choice = ((CheckBox)Item["IsChoice"].FindControl("chkChoice")).Checked;
                if (((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    if (DateFormat != null)
                    {
                        CrewPassportListDef.PassportExpiryDT = Convert.ToDateTime(FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat));
                    }
                    else
                    {
                        CrewPassportListDef.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                    }
                }
                if (((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text != string.Empty)
                {
                    CrewPassportListDef.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                }
                if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    if (DateFormat != null)
                    {
                        CrewPassportListDef.IssueDT = Convert.ToDateTime(FormatDate(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text, DateFormat));
                    }
                    else
                    {
                        CrewPassportListDef.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                    }
                }
                if (((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value != string.Empty)
                {
                    CrewPassportListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value);
                }
                CrewPassportListDef.IsDeleted = false;
                CrewPassportList.Add(CrewPassportListDef);
            }


            foreach (FlightPakMasterService.CrewPassengerPassport Item in CrewPassportList)
            {
                DupePassportGridInfo = CrewPassportList.Where(x => x.PassportNum == Item.PassportNum && x.CountryID == Item.CountryID && x.PassportExpiryDT > DateTime.Now).ToList();
                if (DupePassportGridInfo.Count >= 2)
                {
                    
                    string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                        + @" oManager.radalert('Passport Number " + Item.PassportNum + " Must Be Unique in Passport/Visa Tab.', 360, 50, 'Passenger');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                    return false;
                }
            }
            return true;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPassport_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Item.GetDataKeyValue("PassportExpiryDT") != null)
                            {
                                ((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("PassportExpiryDT").ToString()));
                                //String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("PassportExpiryDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("IssueDT") != null)
                            {
                                ((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("IssueDT").ToString()));
                                //String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("IssueDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("PassportID") != null)
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("PassportID")) <= 1000)
                                {
                                    ((TextBox)Item["PassportNUM"].FindControl("tbPassportNum")).Enabled = true;
                                }
                                else
                                {
                                    ((TextBox)Item["PassportNUM"].FindControl("tbPassportNum")).Enabled = false;
                                }
                            }
                            CheckBox checkColumn = Item.FindControl("chkChoice") as CheckBox;
                            checkColumn.Attributes.Add("onclick", "uncheckOther(this);");
                        }
                        if (e.Item is GridHeaderItem)
                        {
                            if (UserPrincipal.Identity._fpSettings._IsAPISSupport == true)
                            {
                                GridHeaderItem Item = (GridHeaderItem)e.Item;
                                GridDataItem DataItem = e.Item as GridDataItem;
                                GridColumn PassportNumColumn = dgPassport.MasterTableView.GetColumn("PassportNUM");
                                GridColumn ExpiryDTColumn = dgPassport.MasterTableView.GetColumn("ExpiryDT");
                                GridColumn PassportCountryColumn = dgPassport.MasterTableView.GetColumn("CountryCD");
                                PassportNumColumn.HeaderStyle.CssClass = "important-bold-text";
                                PassportCountryColumn.HeaderStyle.CssClass = "important-bold-text";
                                ExpiryDTColumn.HeaderStyle.CssClass = "important-bold-text";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void dgPassport_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //List<FlightPakMasterService.GetAllCrewPassport> lstCrewPassport = (List<FlightPakMasterService.GetAllCrewPassport>)Session["PagingPassengerPassport"];
                        //dgPassport.CurrentPageIndex = e.NewPageIndex;
                        //dgPassport.DataSource = lstCrewPassport;
                        //dgPassport.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        private FlightPakMasterService.Passenger UpdateFutureTripsheet(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                    {
                        List<GetAllCrewPassport> PassengerPassportList = new List<GetAllCrewPassport>();
                        CrewPassengerPassport PassengerPassport = new CrewPassengerPassport();
                        PassengerPassport.CrewID = null;
                        PassengerPassport.PassengerRequestorID = oPassenger.PassengerRequestorID;
                        var objPassengerPassport = objService.GetCrewPassportListInfo(PassengerPassport);
                        if (objPassengerPassport.ReturnFlag == true)
                        {
                            PassengerPassportList = objPassengerPassport.EntityList.Where(x => x.PassengerRequestorID == oPassenger.PassengerRequestorID && x.IsDeleted == false).ToList();
                        }
                        Int64 PassportID = 0;
                        for (int Index = 0; Index < PassengerPassportList.Count; Index++)
                        {
                            PassportID = PassengerPassportList[0].PassportID;
                            if ((PassengerPassportList[Index].IsDeleted == false) && (PassengerPassportList[Index].Choice == true))
                            {
                                PassportID = PassengerPassportList[Index].PassportID;
                                break;
                            }
                        }
                        if (hdnIsPassportChoice.Value == "Yes")
                        {
                            if (dgPassport.Items.Count != 0)
                            {
                                objService.UpdatePassportForFutureTrip(PassportID, oPassenger.PassengerRequestorID, 0);
                            }
                            else if ((dgPassport.Items.Count == 0) || hdnIsPassportChoiceDeletedCount.Value == "1")
                            {
                                objService.UpdatePassportForFutureTrip(-1, oPassenger.PassengerRequestorID, 0);
                            }
                            
                            if (Session[WebSessionKeys.CurrentPreflightTrip] != null)
                            {
                                PreflightTripViewModel Trip = (PreflightTripViewModel)Session[WebSessionKeys.CurrentPreflightTrip];
                                if (Trip.PreflightLegs != null)
                                {
                                    foreach (PreflightLegViewModel Leg in Trip.PreflightLegs.Where(l=>l.IsDeleted==false).ToList())
                                    {
                                        if (Trip.PreflightLegPassengers.ContainsKey(Leg.LegNUM.ToString()))
                                        {
                                            foreach (PassengerViewModel PrefPass in Trip.PreflightLegPassengers[Leg.LegNUM.ToString()])
                                            {
                                                if (PrefPass.PassengerID == (long)oPassenger.PassengerRequestorID)
                                                {
                                                    PrefPass.PassportID = PassportID;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Image Upload"
        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                            //code commented by viswa for image issue
                            //else if (eventArgument == "DeleteImage_Click")
                            //{
                            //    DeleteImage_Click(sender, e);
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgPassenger.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgPassenger.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> DicImgage = new Dictionary<string, byte[]>();
                Session["DicImg"] = DicImgage;
                Dictionary<string, string> DicImageDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = DicImageDelete;
            }
        }
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                        int count = DicImage.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            DicImage.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = DicImage;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                            {
                                dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            }
                            Session["DicImgDelete"] = dicImgDelete;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in DicImage)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            if (ddlImg.Items.Count > 0)
                            {
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                Session["Base64"] = null;   //added for image issue
                                if (Count > 0)
                                {
                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                            }
                            else
                            {
                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                lnkFileName.NavigateUrl = "";
                            }
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream MyStream;
                Int32 FileLen;
                Session["Base64"] = null;   //added for image issue
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        string FileName = fileUL.FileName;
                        FileLen = fileUL.PostedFile.ContentLength;
                        Byte[] Input = new Byte[FileLen];
                        MyStream = fileUL.FileContent;
                        MyStream.Read(Input, 0, FileLen);

                        //Ramesh: Set the URL for image file or link based on the file type
                        SetURL(FileName, Input);

                        ////start of modification for image issue
                        //if (hdnBrowserName.Value == "Chrome")
                        //{
                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                        //}
                        //else
                        //{
                        //    Session["Base64"] = Input;
                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                        //}
                        ////end of modification for image issue
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                        //Commented for removing document name
                        //if (tbImgName.Text.Trim() != "")
                        //{
                        //    FileName = tbImgName.Text.Trim();
                        //}
                        int Count = DicImage.Count(D => D.Key.Equals(FileName));
                        if (Count > 0)
                        {
                            DicImage[FileName] = Input;
                        }
                        else
                        {
                            DicImage.Add(FileName, Input);
                            string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FileName);
                            ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(encodedFileName, encodedFileName));

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);
                        }
                        tbImgName.Text = "";
                        if (ddlImg.Items.Count != 0)
                        {
                            //ddlImg.SelectedIndex = ddlImg.Items.Count - 1;
                            ddlImg.SelectedValue = FileName;
                        }
                        btndeleteImage.Enabled = true;
                    }
                }
            }
        }
        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool ImgFound = false;
                            Session["Base64"] = null;   //added for image issue
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (Session["PassengerRequestorID"] != null)
                                {
                                    //removed tolower conversion for image issue
                                    var ReturnImg = ImgService.GetFileWarehouseList("Passenger", Convert.ToInt64(Session["PassengerRequestorID"].ToString())).EntityList.Where(x => x.UWAFileName == ddlImg.Text.Trim());
                                    foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImg)
                                    {
                                        ImgFound = true;
                                        byte[] Picture = FWH.UWAFilePath;

                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(FWH.UWAFileName, Picture);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Picture);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = Picture;
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }
                                }
                                //tbImgName.Text = ddlImg.Text.Trim();   //Commented for removing document name
                                if (!ImgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (Count > 0)
                                    {
                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        #endregion
        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FilterAndSearch(true);
                        return false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        protected void FilterByClient_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckClientCodeExist(tbSearchClientCode, cvSearchClientCode))
                        {
                            cvSearchClientCode.IsValid = false;
                        }
                        else
                        {
                            FilterAndSearch(true);
                            DefaultSelection(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool FilterAndSearch(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsDataBind))
            {
                List<GetAllPassenger> PassengerList = new List<GetAllPassenger>();
                if (Session["PassengerList"] != null)
                {
                    PassengerList = (List<FlightPakMasterService.GetAllPassenger>)Session["PassengerList"];
                }
                if (PassengerList.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { PassengerList = PassengerList.Where(x => x.IsActive == true).ToList<GetAllPassenger>(); }
                    if (chkSearchRequestorOnly.Checked == true) { PassengerList = PassengerList.Where(x => x.IsRequestor == true).ToList<GetAllPassenger>(); }
                    if (chkSearchHomebaseOnly.Checked == true) { PassengerList = PassengerList.Where(x => x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllPassenger>(); }
                    if (!string.IsNullOrEmpty(tbSearchClientCode.Text))
                    {
                        PassengerList = PassengerList.Where(x => x.ClientCD != null && x.ClientCD.Trim() == tbSearchClientCode.Text.Trim()).ToList<GetAllPassenger>();
                    }
                    dgPassenger.DataSource = PassengerList;
                    if (IsDataBind)
                    {
                        dgPassenger.DataBind();
                    }
                }
                return false;
            }
        }
        #endregion
        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";PassengerRequestorCD=" + hdnReportParameters.Value;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        #endregion
        #region "Date formatting"
        private string DisplayFormattedDate(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                string[] ISplitFormat = new string[3];
                string IFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToString();
                string ReturnFormat = string.Empty;
                string Seperator = string.Empty;
                if (IFormat.Contains("/"))
                {
                    ISplitFormat = IFormat.Split('/');
                }
                else if (IFormat.Contains("-"))
                {
                    ISplitFormat = IFormat.Split('-');
                }
                else if (IFormat.Contains("."))
                {
                    ISplitFormat = IFormat.Split('.');
                }
                if (Format.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                    Seperator = "/";
                }
                else if (Format.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                    Seperator = "-";
                }
                else if (Format.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                    Seperator = ".";
                }
                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                }
                string dd = "", mm = "", yyyy = "";
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = SplitDate[Index];
                        for (int idxD = 0; idxD < ISplitFormat.Count(); idxD++)
                        {
                            if (ISplitFormat[idxD].ToString().ToLower().Contains("d"))
                            {
                                dd = SplitDate[idxD];
                                break;
                            }
                        }
                        ReturnFormat = ReturnFormat + dd.ToString() + Seperator;
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = SplitDate[Index];
                        for (int idxM = 0; idxM < ISplitFormat.Count(); idxM++)
                        {
                            if (ISplitFormat[idxM].ToString().ToLower().Contains("m"))
                            {
                                mm = SplitDate[idxM];
                                break;
                            }
                        }
                        ReturnFormat = ReturnFormat + mm.ToString() + Seperator;
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = SplitDate[Index];
                        for (int idxY = 0; idxY < ISplitFormat.Count(); idxY++)
                        {
                            if (ISplitFormat[idxY].ToString().ToLower().Contains("y"))
                            {
                                yyyy = SplitDate[idxY];
                                break;
                            }
                        }
                        ReturnFormat = ReturnFormat + yyyy.ToString() + Seperator;
                    }
                }
                ReturnFormat = ReturnFormat.Remove(ReturnFormat.LastIndexOf(Seperator));
                return ReturnFormat;
            }
        }
        #endregion
        protected void TaxType_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (radGuest.Checked == true)
                        {
                            tbAssociated.Enabled = true;
                            btnAssociated.Enabled = true;
                        }
                        else if (radNonControlled.Checked == true)
                        {
                            tbAssociated.Text = string.Empty;
                            tbAssociated.Enabled = false;
                            btnAssociated.Enabled = false;
                        }
                        else if (radType.Checked == true)
                        {
                            tbAssociated.Text = string.Empty;
                            tbAssociated.Enabled = false;
                            btnAssociated.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private void LoadDefaultData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                if ((UserPrincipal != null && UserPrincipal.Identity._clientId != null) && (UserPrincipal.Identity._clientId != 0))
                {
                    tbSearchClientCode.Text = UserPrincipal.Identity._clientCd.ToString().Trim();
                    tbSearchClientCode.Enabled = false;
                }
                else
                {
                    tbSearchClientCode.Text = string.Empty;
                    tbSearchClientCode.Enabled = true;
                }
                if ((UserPrincipal.Identity._fpSettings._IsAPISSupport != null) && (UserPrincipal.Identity._fpSettings._IsAPISSupport == true))
                {
                    lbFirstName.CssClass = "important-bold-text";
                    lbMiddleName.CssClass = "important-text";
                    lbLastName.CssClass = "important-bold-text";
                    lbAddressLine1.CssClass = "important-text";
                    lbCity.CssClass = "important-text";
                    lbPostal.CssClass = "important-text";
                    lbDateOfBirth.CssClass = "important-text";
                    lbStateProvince.CssClass = "important-text";
                    lbCountryOfResidence.CssClass = "important-text";
                    lbGender.CssClass = "important-text";
                    lbNationality.CssClass = "important-text";
                }
                else
                {
                    lbFirstName.CssClass = "mnd_text";
                    lbLastName.CssClass = "mnd_text";
                    lbMiddleName.ForeColor = System.Drawing.Color.Black;
                    lbAddressLine1.ForeColor = System.Drawing.Color.Black;
                    lbCity.ForeColor = System.Drawing.Color.Black;
                    lbPostal.ForeColor = System.Drawing.Color.Black;
                    lbDateOfBirth.ForeColor = System.Drawing.Color.Black;
                    lbStateProvince.ForeColor = System.Drawing.Color.Black;
                    lbCountryOfResidence.ForeColor = System.Drawing.Color.Black;
                    lbGender.ForeColor = System.Drawing.Color.Black;
                    lbNationality.ForeColor = System.Drawing.Color.Black;
                }
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["NewPaxSelectID"] != null)
                {
                    dgPassenger.SelectedIndexes.Clear();


                    Session["PassengerRequestorID"] = Session["NewPaxSelectID"];
                    var CrewRosterValue = FPKMstService.GetAllCrewForGrid();
                    Int64 PrimaryKeyValue = Convert.ToInt64(Session["NewPaxSelectID"]);
                    int iIndex = 0;
                    int FullItemIndex = 0;
                    foreach (var Item in CrewRosterValue.EntityList)
                    {
                        if (PrimaryKeyValue == Item.CrewID)
                        {
                            FullItemIndex = iIndex;
                            break;
                        }
                        iIndex++;
                    }

                    int PageSize = dgPassenger.PageSize;
                    int PageNumber = FullItemIndex / PageSize;
                    int ItemIndex = FullItemIndex % PageSize;

                    dgPassenger.CurrentPageIndex = PageNumber;

                    int[] iItemIndexes;
                    iItemIndexes = new int[1];
                    iItemIndexes[0] = ItemIndex;
                    dgPassenger.SelectedIndexes.Add(iItemIndexes);
                }
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    
                    dgPassenger.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var PassengerRequestorValue = FPKMstService.GetPassengerList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, PassengerRequestorValue);
            List<FlightPakMasterService.GetAllPassenger> filteredList = GetFilteredList(PassengerRequestorValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.PassengerRequestorID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgPassenger.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgPassenger.CurrentPageIndex = PageNumber;
            dgPassenger.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllPassenger PassengerRequestorValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["PassengerRequestorID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = PassengerRequestorValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().PassengerRequestorID;
                Session["PassengerRequestorID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllPassenger> GetFilteredList(ReturnValueOfGetAllPassenger PassengerRequestorValue)
        {
            List<FlightPakMasterService.GetAllPassenger> filteredList = new List<FlightPakMasterService.GetAllPassenger>();

            if (PassengerRequestorValue.ReturnFlag)
            {
                filteredList = PassengerRequestorValue.EntityList.Where(x => x.IsDeleted == false).ToList<GetAllPassenger>();
            }

            if ((chkSearchActiveOnly.Checked || chkSearchRequestorOnly.Checked || chkSearchHomebaseOnly.Checked || tbSearchClientCode.Text != string.Empty) && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsActive == true).ToList<GetAllPassenger>(); }
                    if (chkSearchRequestorOnly.Checked) { filteredList = filteredList.Where(x => x.IsRequestor == true).ToList<GetAllPassenger>(); }
                    if (chkSearchHomebaseOnly.Checked) { filteredList = filteredList.Where(x => x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllPassenger>(); }
                    if (!string.IsNullOrEmpty(tbSearchClientCode.Text))
                    {
                        filteredList = filteredList.Where(x => x.ClientCD != null && x.ClientCD.Trim() == tbSearchClientCode.Text.Trim()).ToList<GetAllPassenger>();
                    }
                }
            }

            return filteredList;
        }

        #region"Passenger Checklist"


        protected void CrewCheckList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            if (e.Item is GridDataItem)
                            {
                                GridDataItem Item = (GridDataItem)e.Item;
                                string TestDueDT = ((Label)(Item["DueDT"].FindControl("lbDueDT"))).Text;
                                string TestAlertDT = ((Label)(Item["AlertDT"].FindControl("lbAlertDT"))).Text;
                                string TestGraceDT = ((Label)(Item["GraceDT"].FindControl("lbGraceDT"))).Text;
                                string TestPreviousDT = ((Label)(Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT"))).Text;

                                bool NonConflictEvent = Convert.ToBoolean(((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked, CultureInfo.CurrentCulture);
                                if (TestDueDT != string.Empty)
                                {
                                    ((Label)(Item["DueDT"].FindControl("lbDueDT"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestDueDT)));
                                }

                                if (TestAlertDT != string.Empty)
                                {
                                    ((Label)(Item["AlertDT"].FindControl("lbAlertDT"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestAlertDT)));
                                }

                                if (TestGraceDT != string.Empty)
                                {
                                    ((Label)(Item["GraceDT"].FindControl("lbGraceDT"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestGraceDT)));
                                }

                                if (TestPreviousDT != string.Empty)
                                {
                                    ((Label)(Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT"))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestPreviousDT)));
                                }


                                if ((!string.IsNullOrEmpty(TestDueDT)) && (!string.IsNullOrEmpty(TestAlertDT)) && (!string.IsNullOrEmpty(TestGraceDT)))
                                {
                                    DateTime DueNextSource = DateTime.MinValue;
                                    DueNextSource = Convert.ToDateTime(FormatDate(((Label)(Item["DueDT"].FindControl("lbDueDT"))).Text, DateFormat));

                                    DateTime AlertSource1 = DateTime.MinValue;
                                    AlertSource1 = Convert.ToDateTime(FormatDate(((Label)(Item["AlertDT"].FindControl("lbAlertDT"))).Text, DateFormat));

                                    DateTime GraceSource2 = DateTime.MinValue;
                                    GraceSource2 = Convert.ToDateTime(FormatDate(((Label)(Item["GraceDT"].FindControl("lbGraceDT"))).Text, DateFormat));

                                    if ((DueNextSource > System.DateTime.Now) && (AlertSource1 > System.DateTime.Now) && (GraceSource2 > System.DateTime.Now))
                                    {
                                        if (NonConflictEvent != true)
                                        {
                                            Item.BackColor = System.Drawing.Color.White;
                                        }
                                    }
                                    if (((DueNextSource < System.DateTime.Now) && (System.DateTime.Now < GraceSource2)) || ((GraceSource2 < System.DateTime.Now) && (System.DateTime.Now < DueNextSource)))
                                    {
                                        if (NonConflictEvent != true)
                                        {
                                            Item.BackColor = System.Drawing.Color.Orange;
                                        }
                                    }
                                    if ((System.DateTime.Now > DueNextSource) && (GraceSource2 < System.DateTime.Now))
                                    {
                                        if (NonConflictEvent != true)
                                        {
                                            Item.BackColor = System.Drawing.Color.Red;
                                        }
                                    }
                                    if (((DueNextSource < System.DateTime.Now) && (System.DateTime.Now < AlertSource1)) || ((AlertSource1 < System.DateTime.Now) && (System.DateTime.Now < DueNextSource)))
                                    {
                                        if (NonConflictEvent != true)
                                        {
                                            Item.BackColor = System.Drawing.Color.Yellow;
                                        }
                                    }
                                }
                                if (Item.GetDataKeyValue("OriginalDT") != null)
                                {
                                    ((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("OriginalDT").ToString()))));
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void DeleteChecklist_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnDelete.Value == "Yes")
                        {
                            if (dgCrewCheckList.SelectedItems.Count > 0)
                            {
                                GridDataItem item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                                Int64 ChecklistID = Convert.ToInt64(item.GetDataKeyValue("PassengerCheckListDetailID").ToString());
                                string CheckListCD = item.GetDataKeyValue("PassengerAdditionalInfoID").ToString();
                                List<FlightPakMasterService.GetPaxChecklistDate> CrewCheckListDateList = new List<FlightPakMasterService.GetPaxChecklistDate>();
                                CrewCheckListDateList = (List<FlightPakMasterService.GetPaxChecklistDate>)Session["PaxCheckList"];
                                for (int Index = 0; Index < CrewCheckListDateList.Count; Index++)
                                {
                                    if (CrewCheckListDateList[Index].PassengerAdditionalInfoID == Convert.ToInt64(CheckListCD))
                                    {
                                        CrewCheckListDateList[Index].IsDeleted = true;
                                    }
                                }
                                Session["PaxCheckList"] = CrewCheckListDateList;
                                List<FlightPakMasterService.GetPaxChecklistDate> CrewDefinitionInfoList = new List<FlightPakMasterService.GetPaxChecklistDate>();
                                // Retain the Grid Items and bind into List to remove the selected item.
                                CrewDefinitionInfoList = RetainCrewCheckListGrid(dgCrewCheckList);
                                dgCrewCheckList.DataSource = CrewDefinitionInfoList.Where(x => x.IsDeleted == false).ToList();
                                dgCrewCheckList.DataBind();
                                Session["PaxCheckList"] = CrewCheckListDateList;
                                ClearCheckListFields();
                                DefaultChecklistSelection();
                            }
                            else
                            {
                                
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Please select the record from Checklist table', 360, 50, 'Passenger');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        private void DefaultChecklistSelection()
        {

            if (btnSaveChanges.Visible == true)
            {
                if (hdnSave.Value == "Update")
                {
                    if (dgCrewCheckList.Items.Count > 0)
                    {
                        dgCrewCheckList.SelectedIndexes.Add(0);
                        BindSelectedCrewChecklistItem();
                        EnableCheckListFields(true);
                        btnDeleteChecklist.Enabled = true;
                    }
                    else
                    {
                        EnableCheckListFields(false);
                        btnDeleteChecklist.Enabled = false;
                    }
                }
                else
                {
                    if (dgCrewCheckList.Items.Count > 0)
                    {
                        dgCrewCheckList.SelectedIndexes.Add(0);
                        BindSelectedCrewChecklistItem();
                        EnableCheckListFields(true);
                        btnDeleteChecklist.Enabled = true;
                    }
                    else
                    {
                        EnableCheckListFields(false);
                        btnDeleteChecklist.Enabled = false;
                    }
                }
            }
            else
            {
                if (dgCrewCheckList.Items.Count > 0)
                {
                    dgCrewCheckList.SelectedIndexes.Add(0);
                    BindSelectedCrewChecklistItem();
                    EnableCheckListFields(false);
                    btnDeleteChecklist.Enabled = true;
                }
                else
                {
                    EnableCheckListFields(false);
                    btnDeleteChecklist.Enabled = false;
                }
            }


        }

        private List<GetPaxChecklistDate> RetainCrewCheckListGrid(RadGrid grid)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<GetPaxChecklistDate> CrewCheckListDateList = new List<GetPaxChecklistDate>();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.GetPaxChecklistDate>>(() =>
                {
                    List<FlightPakMasterService.GetPaxChecklistDate> GetPaxChecklistDateRetain = new List<FlightPakMasterService.GetPaxChecklistDate>();
                    GetPaxChecklistDateRetain = (List<FlightPakMasterService.GetPaxChecklistDate>)Session["PaxCheckList"];
                    List<FlightPakMasterService.GetPaxChecklistDate> CrewCheckListRetainList = new List<FlightPakMasterService.GetPaxChecklistDate>();
                    if (GetPaxChecklistDateRetain != null && GetPaxChecklistDateRetain.Count > 0)
                    {
                        for (int Index = 0; Index < GetPaxChecklistDateRetain.Count; Index++)
                        {
                            foreach (GridDataItem Item in grid.MasterTableView.Items)
                            {
                                IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                if (Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString() == GetPaxChecklistDateRetain[Index].PassengerAdditionalInfoID.ToString())
                                {
                                    GetPaxChecklistDateRetain[Index].PassengerID = GetPaxChecklistDateRetain[Index].PassengerID;
                                    GetPaxChecklistDateRetain[Index].PassengerChecklistDescription = ((Label)Item["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text;
                                    if (!string.IsNullOrEmpty(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text))
                                    {
                                        if (DateFormat != null)
                                        {
                                            GetPaxChecklistDateRetain[Index].OriginalDT = Convert.ToDateTime(FormatDate(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text, DateFormat));
                                        }
                                        else
                                        {
                                            GetPaxChecklistDateRetain[Index].OriginalDT = Convert.ToDateTime(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text);
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text))
                                    {
                                        if (DateFormat != null)
                                        {
                                            GetPaxChecklistDateRetain[Index].PreviousCheckDT = Convert.ToDateTime(FormatDate(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text, DateFormat));
                                        }
                                        else
                                        {
                                            GetPaxChecklistDateRetain[Index].PreviousCheckDT = Convert.ToDateTime(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text);
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(((Label)Item["DueDT"].FindControl("lbDueDT")).Text))
                                    {
                                        if (DateFormat != null)
                                        {
                                            GetPaxChecklistDateRetain[Index].DueDT = Convert.ToDateTime(FormatDate(((Label)Item["DueDT"].FindControl("lbDueDT")).Text, DateFormat));
                                        }
                                        else
                                        {
                                            GetPaxChecklistDateRetain[Index].DueDT = Convert.ToDateTime(((Label)Item["DueDT"].FindControl("lbDueDT")).Text);
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text))
                                    {
                                        if (DateFormat != null)
                                        {
                                            GetPaxChecklistDateRetain[Index].AlertDT = Convert.ToDateTime(FormatDate(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text, DateFormat));
                                        }
                                        else
                                        {
                                            GetPaxChecklistDateRetain[Index].AlertDT = Convert.ToDateTime(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text);
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text))
                                    {
                                        if (DateFormat != null)
                                        {
                                            GetPaxChecklistDateRetain[Index].GraceDT = Convert.ToDateTime(FormatDate(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text, DateFormat));
                                        }
                                        else
                                        {
                                            GetPaxChecklistDateRetain[Index].GraceDT = Convert.ToDateTime(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text);
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                                    {
                                        GetPaxChecklistDateRetain[Index].AlertDays = Convert.ToInt32(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text);
                                    }
                                    if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                                    {
                                        GetPaxChecklistDateRetain[Index].GraceDays = Convert.ToInt32(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text);
                                    }
                                    if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                                    {
                                        GetPaxChecklistDateRetain[Index].FrequencyMonth = Convert.ToInt32(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text);
                                    }
                                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                                    {
                                        GetPaxChecklistDateRetain[Index].Frequency = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text);
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsInActive = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsMonthEnd = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsStopCALC = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsOneTimeEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsCompleted = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsNoConflictEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsNoChecklistREPT = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsPrintStatus = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsPassedDueAlert = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsNextMonth = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                                    }
                                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))) != null)
                                    {
                                        GetPaxChecklistDateRetain[Index].IsEndCalendarYear = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    return GetPaxChecklistDateRetain;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }



        protected void SaveChecklisttoSession()
        {
            List<FlightPakMasterService.GetPaxChecklistDate> GetPaxChecklistDateRetains = new List<FlightPakMasterService.GetPaxChecklistDate>();
            GetPaxChecklistDateRetains = (List<FlightPakMasterService.GetPaxChecklistDate>)Session["PaxCheckList"];
            if (dgCrewCheckList.SelectedItems.Count > 0)
            {
                foreach (GetPaxChecklistDate GetPaxChecklistDateRetain in GetPaxChecklistDateRetains)
                {
                    GridDataItem Item = dgCrewCheckList.SelectedItems[0] as GridDataItem;
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    if (Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString() == GetPaxChecklistDateRetain.PassengerAdditionalInfoID.ToString())
                    {
                        GetPaxChecklistDateRetain.PassengerID = GetPaxChecklistDateRetain.PassengerID;
                        GetPaxChecklistDateRetain.PassengerChecklistDescription = ((Label)Item["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text;
                        if (!string.IsNullOrEmpty(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text))
                        {
                            if (DateFormat != null)
                            {
                                GetPaxChecklistDateRetain.OriginalDT = Convert.ToDateTime(FormatDate(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text, DateFormat));
                            }
                            else
                            {
                                GetPaxChecklistDateRetain.OriginalDT = Convert.ToDateTime(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text);
                            }
                        }
                        if (!string.IsNullOrEmpty(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text))
                        {
                            if (DateFormat != null)
                            {
                                GetPaxChecklistDateRetain.PreviousCheckDT = Convert.ToDateTime(FormatDate(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text, DateFormat));
                            }
                            else
                            {
                                GetPaxChecklistDateRetain.PreviousCheckDT = Convert.ToDateTime(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text);
                            }
                        }
                        if (!string.IsNullOrEmpty(((Label)Item["DueDT"].FindControl("lbDueDT")).Text))
                        {
                            if (DateFormat != null)
                            {
                                GetPaxChecklistDateRetain.DueDT = Convert.ToDateTime(FormatDate(((Label)Item["DueDT"].FindControl("lbDueDT")).Text, DateFormat));
                            }
                            else
                            {
                                GetPaxChecklistDateRetain.DueDT = Convert.ToDateTime(((Label)Item["DueDT"].FindControl("lbDueDT")).Text);
                            }
                        }
                        if (!string.IsNullOrEmpty(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text))
                        {
                            if (DateFormat != null)
                            {
                                GetPaxChecklistDateRetain.AlertDT = Convert.ToDateTime(FormatDate(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text, DateFormat));
                            }
                            else
                            {
                                GetPaxChecklistDateRetain.AlertDT = Convert.ToDateTime(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text);
                            }
                        }

                        if (!string.IsNullOrEmpty(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text))
                        {
                            if (DateFormat != null)
                            {
                                GetPaxChecklistDateRetain.GraceDT = Convert.ToDateTime(FormatDate(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text, DateFormat));
                            }
                            else
                            {
                                GetPaxChecklistDateRetain.GraceDT = Convert.ToDateTime(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text);
                            }
                        }
                        if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                        {
                            GetPaxChecklistDateRetain.AlertDays = Convert.ToInt32(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text);
                        }
                        if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                        {
                            GetPaxChecklistDateRetain.GraceDays = Convert.ToInt32(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text);
                        }
                        if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                        {
                            GetPaxChecklistDateRetain.FrequencyMonth = Convert.ToInt32(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text);
                        }
                        if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                        {
                            GetPaxChecklistDateRetain.Frequency = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text);
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsInActive = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsMonthEnd = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsStopCALC = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsOneTimeEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsCompleted = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsNoConflictEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsNoChecklistREPT = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsPrintStatus = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsPassedDueAlert = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsNextMonth = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                        }
                        if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))) != null)
                        {
                            GetPaxChecklistDateRetain.IsEndCalendarYear = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                        }

                        break;
                    }
                }
                Session["PaxCheckList"] = GetPaxChecklistDateRetains;
            }
        }

        protected void CrewCheckList_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            EnableCheckListFields(true);
                            BindSelectedCrewChecklistItem();
                            GridDataItem InsertItem = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            //Session["CrewCD"] = tbCrewCode.Text;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void EnableCheckListFields(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Enable == true)
                    {
                        if (btnSaveChanges.Visible == true)
                        {
                            Enable = true;
                        }
                        else
                        {
                            Enable = false;
                        }
                    }
                    tbOriginalDate.Enabled = Enable;
                    tbFreq.Enabled = Enable;
                    radFreq.Enabled = Enable;
                    tbPrevious.Enabled = Enable;
                    tbDueNext.Enabled = Enable;
                    tbAlertDate.Enabled = Enable;
                    tbAlertDays.Enabled = Enable;
                    tbGraceDate.Enabled = Enable;
                    tbGraceDays.Enabled = Enable;
                    chkInactive.Enabled = Enable;
                    chkSetToEndOfMonth.Enabled = Enable;
                    chkSetToNextOfMonth.Enabled = Enable;
                    chkSetToEndOfCalenderYear.Enabled = Enable;
                    ckhDisableDateCalculation.Enabled = Enable;
                    chkOneTimeEvent.Enabled = Enable;
                    chkNonConflictedEvent.Enabled = Enable;
                    chkRemoveFromChecklistRept.Enabled = Enable;
                    chkDoNotPrintPassDueAlert.Enabled = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        private void BindSelectedCrewChecklistItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ClearCheckListFields();
                    GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                    Item.Selected = true;
                    if (Item.GetDataKeyValue("PassengerChecklistCD") != null)
                    {
                        lbCheckListCode.Text = System.Web.HttpUtility.HtmlEncode(Item.GetDataKeyValue("PassengerChecklistCD").ToString());
                        Session["PassengerChecklistCD"] = Item.GetDataKeyValue("PassengerChecklistCD").ToString();

                        if (Item.GetDataKeyValue("PassengerCheckListDetailID") != null)
                        {
                            Session["PassengerChecklistID"] = Item.GetDataKeyValue("PassengerCheckListDetailID").ToString();
                        }
                        if (Item.GetDataKeyValue("PassengerAdditionalInfoID") != null)
                        {
                            Session["PassengerAdditionalInfoID"] = Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString();
                        }

                    }
                    lbCheckListDescription.Text = ((Label)Item["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text;
                    if (!string.IsNullOrEmpty(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text))
                    {
                        tbPrevious.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbPrevious.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["DueDT"].FindControl("lbDueDT")).Text))
                    {
                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["DueDT"].FindControl("lbDueDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbDueNext.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text))
                    {
                        tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbAlertDate.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text))
                    {
                        tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbGraceDate.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                    {
                        tbAlertDays.Text = ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text;
                    }
                    else
                    {
                        tbAlertDays.Text = "0";
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                    {
                        tbGraceDays.Text = ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text;
                    }
                    else
                    {
                        tbGraceDays.Text = "0";
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                    {
                        tbFreq.Text = ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text;
                    }
                    else
                    {
                        tbFreq.Text = "0";
                    }

                    if (((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text != string.Empty)
                    {
                        tbOriginalDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(FormatDate(((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text, DateFormat)));
                    }
                    else
                    {
                        tbOriginalDate.Text = string.Empty;
                    }


                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                    {
                        if (((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text == "1")
                        {
                            radFreq.Items[0].Selected = true;
                        }
                        else
                        {
                            radFreq.Items[1].Selected = true;
                        }
                    }
                    else
                    {
                        radFreq.Items[0].Selected = true;
                    }



                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))) != null)
                    {
                        chkRemoveFromChecklistRept.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                    }
                    else
                    {
                        chkRemoveFromChecklistRept.Checked = false;
                    }


                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))) != null)
                    {
                        chkPrintOneTimeEventCompleted.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked;
                    }
                    else
                    {
                        chkPrintOneTimeEventCompleted.Checked = false;
                    }



                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))) != null)
                    {
                        chkInactive.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                    }
                    else
                    {
                        chkInactive.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))) != null)
                    {
                        chkSetToEndOfMonth.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                    }
                    else
                    {
                        chkSetToEndOfMonth.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))) != null)
                    {
                        ckhDisableDateCalculation.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                    }
                    else
                    {
                        ckhDisableDateCalculation.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))) != null)
                    {
                        chkOneTimeEvent.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                    }
                    else
                    {
                        chkOneTimeEvent.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))) != null)
                    {
                        chkCompleted.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                    }
                    else
                    {
                        chkCompleted.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))) != null)
                    {
                        chkNonConflictedEvent.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                    }
                    else
                    {
                        chkNonConflictedEvent.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))) != null)
                    {
                        chkDoNotPrintPassDueAlert.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                    }
                    else
                    {
                        chkDoNotPrintPassDueAlert.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))) != null)
                    {
                        chkSetToNextOfMonth.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                    }
                    else
                    {
                        chkSetToNextOfMonth.Checked = false;
                    }
                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))) != null)
                    {
                        chkSetToEndOfCalenderYear.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                    }
                    else
                    {
                        chkSetToEndOfCalenderYear.Checked = false;
                    }
                    if (btnSaveChanges.Visible == true)
                    {
                        if (chkOneTimeEvent.Checked == true)
                        {
                            chkPrintOneTimeEventCompleted.Enabled = true;
                            chkCompleted.Enabled = true;
                        }
                        else
                        {
                            chkPrintOneTimeEventCompleted.Enabled = false;
                            chkCompleted.Enabled = false;
                        }
                    }
                    else
                    {
                        chkPrintOneTimeEventCompleted.Enabled = false;
                        chkCompleted.Enabled = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void ClearCheckListFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    lbCheckListCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    lbCheckListDescription.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    tbFreq.Text = "0";
                    radFreq.Items[0].Selected = false;
                    radFreq.Items[1].Selected = false;
                    tbPrevious.Text = string.Empty;
                    tbDueNext.Text = string.Empty;
                    tbAlertDate.Text = string.Empty;
                    tbAlertDays.Text = string.Empty;
                    tbGraceDate.Text = string.Empty;
                    tbGraceDays.Text = string.Empty;
                    tbOriginalDate.Text = string.Empty;
                    chkInactive.Checked = false;
                    chkSetToEndOfMonth.Checked = false;
                    chkSetToNextOfMonth.Checked = false;
                    chkSetToEndOfCalenderYear.Checked = false;
                    ckhDisableDateCalculation.Checked = false;
                    chkOneTimeEvent.Checked = false;
                    chkCompleted.Checked = false;
                    chkNonConflictedEvent.Checked = false;
                    chkRemoveFromChecklistRept.Checked = false;
                    chkDoNotPrintPassDueAlert.Checked = false;
                    chkPrintOneTimeEventCompleted.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        protected void chkDisplayInactiveChecklist_CheckedChanged(object sender, EventArgs e)
        {
            if (Session["PaxCheckList"] != null)
            {

                if (chkDisplayInactiveChecklist.Checked == true)
                {
                    List<GetPaxChecklistDate> CheckList = new List<GetPaxChecklistDate>();
                    CheckList = RetainCrewCheckListGrid(dgCrewCheckList);
                    Session["PaxCheckList"] = CheckList;
                    dgCrewCheckList.DataSource = CheckList.Where(x => x.IsInActive == false || x.IsInActive == null);
                }
                else
                {
                    List<GetPaxChecklistDate> CrewCheckList = (List<GetPaxChecklistDate>)Session["PaxCheckList"];
                    dgCrewCheckList.DataSource = CrewCheckList;
                }
                dgCrewCheckList.DataBind();
            }
        }

        protected void OriginalDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((Label)Item["OriginalDT"].FindControl("lbOriginalDT")).Text = System.Web.HttpUtility.HtmlEncode(tbOriginalDate.Text);
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void radFreq_SelectedIndexChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgCrewCheckList.SelectedItems[0] as GridDataItem;
                            if (radFreq.SelectedItem.Value == "1")
                            {
                                if ((chkSetToEndOfCalenderYear.Checked == false) && (chkSetToEndOfMonth.Checked == false) && (chkSetToNextOfMonth.Checked == false))
                                {
                                    FreqChangeCalculation();
                                }
                                else
                                {
                                    FreqChangeCalculationonChecked();
                                }
                                ((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text = System.Web.HttpUtility.HtmlEncode("1");
                            }
                            else
                            {
                                if ((chkSetToEndOfCalenderYear.Checked == false) && (chkSetToEndOfMonth.Checked == false) && (chkSetToNextOfMonth.Checked == false))
                                {
                                    FreqChangeCalculation();
                                }
                                else
                                {
                                    FreqChangeCalculationonChecked();
                                }
                                ((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text = System.Web.HttpUtility.HtmlEncode("2");
                            }
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void FreqChangeCalculationonChecked()
        {
            if (dgCrewCheckList.SelectedItems.Count > 0)
            {
                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                if (ckhDisableDateCalculation.Checked == false)
                {

                    string DateCalculation = "";
                    if (tbPrevious.Text.Trim() != string.Empty)
                    {
                        DateCalculation = tbPrevious.Text.Trim();
                    }

                    if (DateCalculation != string.Empty)
                    {
                        if (radFreq.SelectedItem.Text == "Months")
                        {
                            int Alerts = 0;
                            int GraceDay = 0;
                            int Months = 0;
                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                            {
                                Months = Convert.ToInt32(tbFreq.Text);
                            }
                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                            {
                                Alerts = Convert.ToInt32(tbAlertDays.Text);
                            }
                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                            {
                                GraceDay = Convert.ToInt32(tbGraceDays.Text);
                            }
                            DateTime DateSource = DateTime.MinValue;
                            DateTime DueNext = DateTime.MinValue;
                            DateTime Alert = DateTime.MinValue;
                            DateTime Grace = DateTime.MinValue;
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = DueNext.AddMonths(Months);
                            if (chkSetToEndOfMonth.Checked == true)
                            {
                                int Year = DueNext.Year;
                                int Month = DueNext.Month;
                                int numberOfDays = DateTime.DaysInMonth(Year, Month);
                                DateTime value = new DateTime(Year, Month, numberOfDays);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                            if (chkSetToNextOfMonth.Checked == true)
                            {

                                int Year = DueNext.Year;
                                int Month = DueNext.Month;
                                DateTime value = new DateTime(Year, Month + 1, 1);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                            if (chkSetToEndOfCalenderYear.Checked == true)
                            {
                                int Year = DueNext.Year;
                                DateTime value = new DateTime(Year, 12, 31);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                        }
                        else
                        {
                            int Days = 0;
                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                            {
                                Days = Convert.ToInt32(tbFreq.Text);
                            }
                            DateTime DateSource = DateTime.MinValue;
                            DateTime DueNext = DateTime.MinValue;
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = DateSource.AddDays(Days);
                            if (chkSetToEndOfMonth.Checked == true)
                            {
                                int Year = DueNext.Year;
                                int Month = DueNext.Month;
                                int numberOfDays = DateTime.DaysInMonth(Year, Month);
                                DateTime value = new DateTime(Year, Month, numberOfDays);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                            if (chkSetToNextOfMonth.Checked == true)
                            {

                                int Year = DueNext.Year;
                                int Month = DueNext.Month;
                                DateTime value = new DateTime(Year, Month + 1, 1);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                            if (chkSetToEndOfCalenderYear.Checked == true)
                            {
                                int Year = DueNext.Year;
                                DateTime value = new DateTime(Year, 12, 31);
                                tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                DueNextCalculation();
                            }
                        }
                    }
                }
                ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
            }

        }

        private void DueNextCalculation()
        {
            if (tbDueNext.Text.Trim() != "")
            {
                int GraceDay = 0;
                int Days = 0;
                int Alerts = 0;
                if (ckhDisableDateCalculation.Checked == false)
                {
                    if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                    {
                        Days = Convert.ToInt32(tbFreq.Text);
                    }
                    if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                    {
                        GraceDay = Convert.ToInt32(tbGraceDays.Text);
                        GraceDay = GraceDay + Days;
                    }
                    if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                    {
                        Alerts = Convert.ToInt32(tbAlertDays.Text);
                        Alerts = Alerts - Days;
                    }
                    DateTime DateSource = DateTime.MinValue;
                    DateTime DueNext = DateTime.MinValue;
                    DateTime Alert = DateTime.MinValue;
                    DateTime Grace = DateTime.MinValue;
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    DueNext = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                    Alert = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                    Grace = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                    tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                    Alert = Alert.AddDays(-Alerts);
                    tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                    Grace = Grace.AddDays(GraceDay);
                    tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                }
            }
            else
            {
                tbAlertDate.Text = "";
                tbGraceDate.Text = "";
            }

        }

        protected void AlertDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {

                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];

                            if (tbDueNext.Text.Trim() != string.Empty || ckhDisableDateCalculation.Checked == true)
                            {
                                if (tbDueNext.Text.Trim() != string.Empty && tbAlertDate.Text != string.Empty)
                                {
                                    ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                    DateTime DueNext = DateTime.MinValue;
                                    DateTime Alert = DateTime.MinValue;

                                    DueNext = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                                    Alert = Convert.ToDateTime(FormatDate(tbAlertDate.Text, DateFormat));

                                    TimeSpan t = DueNext - Alert;
                                    int NrOfDays = t.Days;

                                    tbAlertDays.Text = NrOfDays.ToString();
                                    ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                                }
                            }
                            else
                            {
                                
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Enter a Due Next Date or check Disable Date Calculation', 360, 50, 'Passenger');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                tbAlertDate.Text = "";
                            }
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void DueNext_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            if (ckhDisableDateCalculation.Checked == false)
                            {
                                DateTime FirstDay = DateTime.MinValue;
                                string DateCalculation = "";
                                if (tbPrevious.Text.Trim() != "")
                                {
                                    DateCalculation = tbPrevious.Text.Trim();
                                }

                                if (DateCalculation.Trim() != "")
                                {
                                    FirstDay = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                    if (radFreq.SelectedItem.Text == "Months")
                                    {
                                        int Months = 0;
                                        if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                        {
                                            Months = Convert.ToInt32(tbFreq.Text);
                                        }
                                        FirstDay = FirstDay.AddMonths(Months);
                                    }
                                    else
                                    {
                                        int Days = 0;
                                        if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                        {
                                            Days = Convert.ToInt32(tbFreq.Text);
                                        }
                                        FirstDay = FirstDay.AddDays(Days);
                                    }
                                }
                                if (chkSetToEndOfMonth.Checked == true)
                                {
                                    if (DateCalculation != "")
                                    {
                                        int Year = FirstDay.Year;
                                        int Month = FirstDay.Month;
                                        int numberOfDays = DateTime.DaysInMonth(Year, Month);
                                        DateTime value = new DateTime(Year, Month, numberOfDays);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }

                                }
                                else if (chkSetToNextOfMonth.Checked == true)
                                {
                                    if (DateCalculation != "")
                                    {

                                        int Year = FirstDay.Year;
                                        int Month = FirstDay.Month;
                                        DateTime value = new DateTime(Year, Month + 1, 1);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }
                                else if (chkSetToEndOfCalenderYear.Checked == true)
                                {
                                    if (DateCalculation != "")
                                    {
                                        int Year = FirstDay.Year;
                                        DateTime value = new DateTime(Year, 12, 31);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }
                                else
                                {
                                    if (tbPrevious.Text.Trim() == string.Empty)
                                    {
                                        DueNextCalculation();
                                        ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                                    }
                                    else
                                    {
                                        tbDueNext.Text = ((Label)Item["DueDT"].FindControl("lbDueDT")).Text;
                                    }
                                }


                                ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                                ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                            }
                            else
                            {
                                ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                            }
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void ChecklistchkNonConflictedEvent_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked = chkNonConflictedEvent.Checked;
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        protected void ChecklistchkRemoveFromChecklistRept_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked = chkRemoveFromChecklistRept.Checked;
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void ChecklistchkDoNotPrintPassDueAlert_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked = chkDoNotPrintPassDueAlert.Checked;
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkPrintOneTimeEventCompleted_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked = chkPrintOneTimeEventCompleted.Checked;
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void Previous_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            if (chkSetToEndOfMonth.Checked == true)
                            {

                                if (ckhDisableDateCalculation.Checked == false)
                                {
                                    if (tbPrevious.Text.Trim() != "")
                                    {
                                        DateTime FirstDay = Convert.ToDateTime(FormatDate(tbPrevious.Text, DateFormat));
                                        int Year = FirstDay.Year;
                                        int Month = FirstDay.Month;
                                        int numberOfDays = DateTime.DaysInMonth(Year, Month);
                                        DateTime value = new DateTime(Year, Month, numberOfDays);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }

                            }
                            else if (chkSetToNextOfMonth.Checked == true)
                            {

                                if (ckhDisableDateCalculation.Checked == false)
                                {
                                    if (tbPrevious.Text.Trim() != "")
                                    {
                                        DateTime FirstDay = Convert.ToDateTime(FormatDate(tbPrevious.Text, DateFormat));
                                        int Year = FirstDay.Year;
                                        int Month = FirstDay.Month;
                                        DateTime value = new DateTime(Year, Month + 1, 1);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }

                            }
                            else if (chkSetToEndOfCalenderYear.Checked == true)
                            {

                                if (ckhDisableDateCalculation.Checked == false)
                                {
                                    if (tbPrevious.Text.Trim() != "")
                                    {
                                        DateTime FirstDay = Convert.ToDateTime(FormatDate(tbPrevious.Text, DateFormat));
                                        int Year = FirstDay.Year;
                                        DateTime value = new DateTime(Year, 12, 31);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", value);
                                        DueNextCalculation();
                                    }
                                }

                            }
                            else
                            {

                                if (ckhDisableDateCalculation.Checked == false)
                                {
                                    string DateCalculation = tbPrevious.Text.Trim();
                                    if (DateCalculation.Trim() != "")
                                    {
                                        Label lbPreviousDT = new Label();
                                        ((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                                        if (radFreq.SelectedItem.Text == "Months")
                                        {
                                            int Alerts = 0;
                                            int GraceDay = 0;
                                            int Months = 0;
                                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                            {
                                                Months = Convert.ToInt32(tbFreq.Text);
                                            }
                                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                                            {
                                                Alerts = Convert.ToInt32(tbAlertDays.Text);
                                            }
                                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                                            {
                                                GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                            }
                                            DateTime DateSource = DateTime.MinValue;
                                            DateTime DueNext = DateTime.MinValue;
                                            DateTime Alert = DateTime.MinValue;
                                            DateTime Grace = DateTime.MinValue;
                                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                            Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                            Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                            DueNext = DateSource.AddMonths(Months);
                                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                                            Alert = DateSource.AddMonths(Months);
                                            Alert = Alert.AddDays(-Alerts);
                                            tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                                            Grace = DateSource.AddMonths(Months);
                                            Grace = Grace.AddDays(GraceDay);
                                            tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                                        }
                                        else
                                        {
                                            int Alerts = 0;
                                            int GraceDay = 0;
                                            int Days = 0;
                                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                                            {
                                                Days = Convert.ToInt32(tbFreq.Text);
                                            }
                                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                                            {
                                                Alerts = Convert.ToInt32(tbAlertDays.Text);
                                                Alerts = Alerts - Days;
                                            }
                                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                                            {
                                                GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                                GraceDay = GraceDay + Days;
                                            }
                                            DateTime DateSource = DateTime.MinValue;
                                            DateTime DueNext = DateTime.MinValue;
                                            DateTime Alert = DateTime.MinValue;
                                            DateTime Grace = DateTime.MinValue;
                                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                            Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                            Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                                            DueNext = DateSource.AddDays(Days);
                                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                                            {
                                                Alert = Alert.AddDays(-Alerts);
                                            }
                                            else
                                            {
                                                Alert = Alert.AddDays(Days);
                                            }
                                            tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                                            {
                                                Grace = Grace.AddDays(GraceDay);
                                            }
                                            else
                                            {
                                                Grace = Grace.AddDays(Days);
                                            }
                                            tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                                        }
                                        ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                                        ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                        ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                                        if (tbGraceDays.Text.Trim() != "")
                                        {
                                            ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                                        }
                                        if (tbAlertDays.Text.Trim() != "")
                                        {
                                            ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                                        }
                                        ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                                    }
                                }

                            }

                            ((Label)Item["PreviousCheckDT"].FindControl("lbPreviousCheckDT")).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void Freq_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((chkSetToEndOfCalenderYear.Checked == false) && (chkSetToEndOfMonth.Checked == false) && (chkSetToNextOfMonth.Checked == false))
                        {
                            FreqChangeCalculation();
                        }
                        else
                        {
                            FreqChangeCalculationonChecked();
                        }
                        SaveChecklisttoSession();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void AlertDays_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            if (ckhDisableDateCalculation.Checked == false)
                            {
                                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                                ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                                ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                AlertDaysFunction();
                            }
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void AlertDaysFunction()
        {
            if (tbAlertDate.Text != string.Empty)
            {
                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                int AlertDays = 0;
                if (tbDueNext.Text.Trim() != "")
                {
                    string StartDate = tbDueNext.Text;
                    DateTime AlertDate = Convert.ToDateTime(FormatDate(StartDate, DateFormat));
                    if (tbAlertDate.Text != string.Empty)
                    {
                        AlertDays = Convert.ToInt32(tbAlertDays.Text);
                    }
                    tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime((AlertDate.AddDays(-AlertDays))));
                    ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                    ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                }
            }
        }

        protected void GraceDaysFunction()
        {
            if (tbGraceDate.Text != string.Empty)
            {
                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                int GraceDays = 0;
                if (tbDueNext.Text.Trim() != "")
                {
                    string StartDate = tbDueNext.Text;
                    DateTime GraceDate = Convert.ToDateTime(FormatDate(StartDate, DateFormat));
                    if (tbGraceDays.Text != string.Empty)
                    {
                        GraceDays = Convert.ToInt32(tbGraceDays.Text);
                    }
                    tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime((GraceDate.AddDays(GraceDays))));
                    ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                    ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                }
            }
        }


        protected void GraceDays_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            if (ckhDisableDateCalculation.Checked == false && dgCrewCheckList.SelectedItems.Count > 0)
                            {
                                GridDataItem Item = dgCrewCheckList.SelectedItems[0] as GridDataItem;
                                GraceDaysFunction();
                                ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                                ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                            }
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }


        protected void ChecklistchkInactive_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked = chkInactive.Checked;

                            if (Session["PaxCheckList"] != null && Item["PassengerChecklistCD"].Text != null)
                            {
                                List<GetPaxChecklistDate> CrewCheckList = (List<GetPaxChecklistDate>)Session["PaxCheckList"];
                                foreach (GetPaxChecklistDate CheckItem in CrewCheckList)
                                {
                                    if (CheckItem.IsDeleted == false && CheckItem.PassengerAdditionalInfoID.ToString().Trim() == Item["PassengerAdditionalInfoID"].Text.Trim())
                                    {
                                        if (chkInactive.Checked == true)
                                        {
                                            CheckItem.IsInActive = true;
                                        }
                                        else
                                        {
                                            CheckItem.IsInActive = false;
                                        }
                                    }
                                }
                                Session["PaxCheckList"] = CrewCheckList;
                            }
                            //BindCrewCheckListGrid(Convert.ToInt64(Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString()));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void SetToEndOfMonth_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSetToNextOfMonth.Checked = false;
                        chkSetToEndOfCalenderYear.Checked = false;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;
                            if (chkSetToEndOfMonth.Checked == true)
                            {
                                DueNext_TextChanged(sender, e);
                            }
                            else
                            {
                                FreqChangeCalculation();
                            }
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        protected void SetToNextOfMonth_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSetToEndOfMonth.Checked = false;
                        chkSetToEndOfCalenderYear.Checked = false;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;
                            if (chkSetToNextOfMonth.Checked == true)
                            {
                                DueNext_TextChanged(sender, e);
                            }
                            else
                            {
                                FreqChangeCalculation();
                            }
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        protected void SetToEndOfCalenderYear_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSetToEndOfMonth.Checked = false;
                        chkSetToNextOfMonth.Checked = false;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;
                            if (chkSetToEndOfCalenderYear.Checked == true)
                            {
                                DueNext_TextChanged(sender, e);
                            }
                            else
                            {
                                FreqChangeCalculation();
                            }
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void ckhDisableDateCalculation_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked = ckhDisableDateCalculation.Checked;
                            SaveChecklisttoSession();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void OneTimeEvent_CheckChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkOneTimeEvent.Checked == true)
                        {
                            chkCompleted.Enabled = true;
                            chkPrintOneTimeEventCompleted.Enabled = true;
                            radFreq.Enabled = false;
                            tbFreq.Enabled = false;
                            tbDueNext.Enabled = false;
                            tbAlertDate.Enabled = false;
                            tbAlertDays.Enabled = false;
                            tbGraceDate.Enabled = false;
                            tbGraceDays.Enabled = false;
                        }
                        else if (chkOneTimeEvent.Checked == false)
                        {
                            chkCompleted.Enabled = false;
                            chkPrintOneTimeEventCompleted.Enabled = false;
                            radFreq.Enabled = true;
                            tbFreq.Enabled = true;
                            tbDueNext.Enabled = true;
                            tbAlertDate.Enabled = true;
                            tbAlertDays.Enabled = true;
                            tbGraceDate.Enabled = true;
                            tbGraceDays.Enabled = true;

                        }
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked = chkOneTimeEvent.Checked;
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        protected void ChecklistchkCompleted_CheckChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked = chkCompleted.Checked;
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        private void BindCrewCheckListGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Session["PaxCheckList"] != null)
                        {
                            List<GetPaxChecklistDate> CrewCheckList = (List<GetPaxChecklistDate>)Session["PaxCheckList"];
                            if (chkDisplayInactiveChecklist.Checked == true)
                            {
                                dgCrewCheckList.DataSource = CrewCheckList.Where(x => x.IsInActive == false);
                            }
                            else
                            {
                                dgCrewCheckList.DataSource = CrewCheckList;
                            }


                            dgCrewCheckList.DataBind();
                            if (dgCrewCheckList.Items.Count > 0) btnDeleteChecklist.Enabled = true;
                            else btnDeleteChecklist.Enabled = false;
                        }
                        else
                        {
                            LoadCrewCheckListInfoFromDB(CrewID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        private void LoadCrewCheckListInfoFromDB(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                    {
                        FlightPakMasterService.GetPaxChecklistDate CrewChecklistDate = new FlightPakMasterService.GetPaxChecklistDate();
                        CrewChecklistDate.PassengerID = CrewID;
                        CrewChecklistDate.PassengerChecklistCD = "null";
                        CrewChecklistDate.PassengerChecklistDescription = "null";
                        var CheckListValue = CrewRosterService.GetPaxCheckListDate(CrewChecklistDate);
                        List<FlightPakMasterService.GetPaxChecklistDate> CrewChecklistDateList = new List<FlightPakMasterService.GetPaxChecklistDate>();

                        if (CheckListValue.ReturnFlag == true)
                        {
                            CrewChecklistDateList = CheckListValue.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.GetPaxChecklistDate>();
                            if (chkDisplayInactiveChecklist.Checked == true)
                            {
                                dgCrewCheckList.DataSource = CrewChecklistDateList.Where(x => x.IsInActive == false);
                            }
                            else
                            {
                                dgCrewCheckList.DataSource = CrewChecklistDateList;
                            }
                            dgCrewCheckList.DataBind();
                            if (dgCrewCheckList.Items.Count > 0) 
                                btnDeleteChecklist.Enabled = true;
                            else btnDeleteChecklist.Enabled = false;
                            if (CheckListValue.EntityList.ToList().Count > 0)
                                Session["PaxCheckList"] = CrewChecklistDateList;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void GraceDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];


                            if (tbDueNext.Text.Trim() != string.Empty || ckhDisableDateCalculation.Checked == true)
                            {
                                if (tbDueNext.Text.Trim() != string.Empty && tbGraceDate.Text != string.Empty)
                                {
                                    ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                                    DateTime DueNext = DateTime.MinValue;
                                    DateTime GraceDate = DateTime.MinValue;
                                    DueNext = Convert.ToDateTime(FormatDate(tbDueNext.Text, DateFormat));
                                    GraceDate = Convert.ToDateTime(FormatDate(tbGraceDate.Text, DateFormat));
                                    TimeSpan t = GraceDate - DueNext;
                                    int NrOfDays = t.Days;
                                    tbGraceDays.Text = NrOfDays.ToString();
                                    ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                                }
                            }
                            else
                            {
                                tbGraceDate.Text = "";
                                
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Enter a Due Next Date or check Disable Date Calculation', 360, 50, 'Passenger');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                            SaveChecklisttoSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void FreqChangeCalculation()
        {
            if (dgCrewCheckList.SelectedItems.Count > 0)
            {
                if (ckhDisableDateCalculation.Checked == false)
                {
                    string DateCalculation = "";
                    if (tbPrevious.Text.Trim() != string.Empty)
                    {
                        DateCalculation = tbPrevious.Text.Trim();
                    }

                    if (DateCalculation != string.Empty)
                    {
                        GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                        if (radFreq.SelectedItem.Text == "Months")
                        {
                            int Alerts = 0;
                            int GraceDay = 0;
                            int Months = 0;
                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                            {
                                Months = Convert.ToInt32(tbFreq.Text);
                            }
                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                            {
                                Alerts = Convert.ToInt32(tbAlertDays.Text);
                            }
                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                            {
                                GraceDay = Convert.ToInt32(tbGraceDays.Text);
                            }
                            DateTime DateSource = DateTime.MinValue;
                            DateTime DueNext = DateTime.MinValue;
                            DateTime Alert = DateTime.MinValue;
                            DateTime Grace = DateTime.MinValue;
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = DateSource.AddMonths(Months);
                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                            Alert = DateSource.AddMonths(Months);
                            Alert = Alert.AddDays(-Alerts);
                            tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                            Grace = DateSource.AddMonths(Months);
                            Grace = Grace.AddDays(GraceDay);
                            tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                        }
                        else
                        {
                            int Alerts = 0;
                            int GraceDay = 0;
                            int Days = 0;
                            if (tbFreq.Text.Trim() != "" && tbFreq.Text.Trim() != "0")
                            {
                                Days = Convert.ToInt32(tbFreq.Text);
                            }
                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                            {
                                Alerts = Convert.ToInt32(tbAlertDays.Text);
                                Alerts = Alerts - Days;
                            }
                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                            {
                                GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                GraceDay = GraceDay + Days;
                            }
                            DateTime DateSource = DateTime.MinValue;
                            DateTime DueNext = DateTime.MinValue;
                            DateTime Alert = DateTime.MinValue;
                            DateTime Grace = DateTime.MinValue;
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateSource = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Alert = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            Grace = Convert.ToDateTime(FormatDate(DateCalculation, DateFormat));
                            DueNext = DateSource.AddDays(Days);
                            tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);
                            if (tbAlertDays.Text.Trim() != "" && tbAlertDays.Text.Trim() != "0")
                            {
                                Alert = Alert.AddDays(-Alerts);
                            }
                            else
                            {
                                Alert = Alert.AddDays(Days);
                            }
                            tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);
                            if (tbGraceDays.Text.Trim() != "" && tbGraceDays.Text.Trim() != "0")
                            {
                                Grace = Grace.AddDays(GraceDay);
                            }
                            else
                            {
                                Grace = Grace.AddDays(Days);
                            }
                            tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                        }
                        ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);
                        ((Label)Item["AlertDT"].FindControl("lbAlertDT")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                        ((Label)Item["GraceDT"].FindControl("lbGraceDT")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                        ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                        ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                        ((Label)Item["DueDT"].FindControl("lbDueDT")).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                    }
                }
            }

        }
        #endregion

        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            //Ramesh: Code to allow any file type for upload control.
            int iIndex = filename.Trim().IndexOf('.');
            //string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);  
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            if (regMatch.Success)
            {
                if (Request.UserAgent.Contains("Chrome"))
                {
                    hdnBrowserName.Value = "Chrome";
                }
                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                else
                {
                    Session["Base64"] = filedata;
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid();
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgFile.Visible = false;
                lnkFileName.Visible = true;
                Session["Base64"] = filedata;
                lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();
            }
        }

        
    }
}

