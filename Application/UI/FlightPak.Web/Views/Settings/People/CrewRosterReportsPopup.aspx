﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewRosterReportsPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.CrewRosterReportsPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
    #form1 .box1 {
        height: auto !important;
    }
    </style>
</head>
<body style="background-color: #fff !important">
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();


            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg.CrewRosterRpt = document.getElementById("<%= chkCrewRosterRpt.ClientID %>").checked == true;

                oArg.CrewChecklistRpt = document.getElementById("<%= chkCrewChecklistRpt.ClientID %>").checked == true;

                oArg.InactiveChecklistRpt = document.getElementById("<%= chkInactiveChecklistRpt.ClientID %>").checked == true;
                
                oArg.CrewCurrencyRpt = document.getElementById("<%= chkCrewCurrencyRpt.ClientID %>").checked == true;
                
                oArg.CrewTypeRatingsRpt = document.getElementById("<%= chkCrewTypeRatingsRpt.ClientID %>").checked == true;

                var oWnd = GetRadWindow();
                oWnd.close(oArg);
            }

            function Close() {
                GetRadWindow().Close();
            }
            
        </script>
    </telerik:RadCodeBlock>
    <asp:ScriptManager ID="scr1" runat="server">
    </asp:ScriptManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table style="background-color: #fff;width: 100%">
        <tr>
            <td>
                <table  cellspacing="5px" cellpadding="5px">
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkCrewRosterRpt" Text="Crew Roster Report" runat="server" Checked="True" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCrewChecklistRpt" Text="Crew Checklist Report" runat="server" Checked="True" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:CheckBox ID="chkInactiveChecklistRpt" Text="Show Inactive Checklist Items" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkCrewCurrencyRpt" Text="Crew Currency Report" runat="server" Checked="True" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCrewTypeRatingsRpt" Text="Crew Type Ratings Report" runat="server" Checked="True" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="right">
                            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                <tr align="right">
                                    <td>
                                        <asp:Button ID="btnContinue" Text="Continue" runat="server" CssClass="button" OnClientClick="returnToParent(); return false;" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="false"
                                            CssClass="button" OnClick="Cancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Label ID="lbScript" Visible="false" runat="server"></asp:Label>
    </form>
</body>
</html>
