﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewRosterChecklistSettings.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.CrewRosterChecklistSettiongs" ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select Checklist Settings</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }
            function Close() {
                //alert('clos called');
                // GetRadWindow().BrowserWindow.refreshGrid();
                GetRadWindow().Close();
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            function CheckApplytoAdditionalCrew() {
                var grid = $find("<%=dgCrewRoster.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var IsPassportChoice = false;
                var CrewName;
                var CrewCD;
                var chkchoice;
                var Crew = "";
                for (var i = 0; i < length; i++) {
                    CrewName = MasterTable.get_dataItems()[i].getDataKeyValue('CrewName'); // //MasterTable.get_dataItems()[i].findElement("CrewName");
                    CrewCD = MasterTable.get_dataItems()[i].getDataKeyValue('CrewCD');  //MasterTable.get_dataItems()[i].findElement("CrewCD");
                    //chkchoice = MasterTable.get_dataItems()[i].findElement("CrewChoice");
                    //if (chkchoice.checked == true) {
                    if (MasterTable.get_dataItems()[i].get_selected() == true) {
                        Crew = Crew + CrewName + " " + "(" + CrewCD + ")" + "\n";
                    }
                    //}
                }

                if (Crew != "") {
                    Msg = "Apply Crew member checklist settings for code " + document.getElementById('<%=hdnChecklistCode.ClientID%>').value + " from Crew member " + document.getElementById('<%=hdnCrewCode.ClientID%>').value + " to the following Crew members \n " + Crew + "";
                }
                else {
                    GetRadWindow().Close();
                }

                if (Crew != "") {
                    var IsResult = confirm(Msg, "Crew");
                    if (IsResult == true) {
                        document.getElementById('<%=btndummySubmit.ClientID%>').click();
                    }
                    else {
                        GetRadWindow().Close();
                    }
                }
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCrewRoster.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "CrewCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "CrewName");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "PhoneNum");
                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "HomeBase");
                }

                if (selectedRows.length > 0) {
                    oArg.CrewInfoCD = cell1.innerHTML;
                    oArg.CrewInfoDesc = cell2.innerHTML;
                }
                else {
                    oArg.CrewInfoCD = "";
                    oArg.CrewInfoDesc = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }

            }
        </script>
    </telerik:RadCodeBlock>
    <asp:ScriptManager ID="scr1" runat="server">
    </asp:ScriptManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <asp:Label ID="lbSelectChecklist" runat="server" Text="Select Checklist Settings"></asp:Label>
    <telerik:RadGrid ID="dgChecklistSettings" runat="server" AllowSorting="true" Visible="true"
        OnNeedDataSource="ChecklistSettings_BindData" Width="500px" AllowMultiRowSelection="true"
        CommandItemTemplate="None" AllowFilteringByColumn="False" PagerStyle-AlwaysVisible="true"
        AutoGenerateColumns="false" AllowPaging="false">
        <MasterTableView AllowPaging="false" DataKeyNames="Description" CommandItemDisplay="None"
            AllowFilteringByColumn="false">
            <Columns>
                <telerik:GridClientSelectColumn HeaderText="Selected" ShowFilterIcon="false">
                </telerik:GridClientSelectColumn>
                <telerik:GridBoundColumn DataField="Description" HeaderText="Description" ShowFilterIcon="false"
                    AllowFiltering="false">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
        </MasterTableView>
        <ClientSettings>
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
    </telerik:RadGrid>
    <br />
    <asp:Label ID="lbSelectCrewMembers" runat="server" Text="Select Crew Members"></asp:Label>
    <telerik:RadGrid ID="dgCrewRoster" runat="server" AllowSorting="true" OnNeedDataSource="CrewRoster_BindData"
        AllowMultiRowSelection="true" AutoGenerateColumns="false" AllowFilteringByColumn="true"
        PagerStyle-AlwaysVisible="true">
        <MasterTableView AllowPaging="false" DataKeyNames="CustomerID,CrewID,CrewCD,PhoneNum,CrewTypeCD,LastUpdUID,LastUpdTS,ClientCD,ClientID,HomeBaseID,HomeBaseCD"
            ClientDataKeyNames="CrewName,CrewCD" CommandItemDisplay="None" AllowFilteringByColumn="false">
            <Columns>
                <telerik:GridClientSelectColumn HeaderText="Selected Crew" UniqueName="CrewChoice"
                    ShowFilterIcon="false">
                </telerik:GridClientSelectColumn>
                <telerik:GridBoundColumn DataField="CrewCD" HeaderText="Code" CurrentFilterFunction="Contains"
                    UniqueName="CrewCD" AutoPostBackOnFilter="true" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CrewName" HeaderText="Crew Name" CurrentFilterFunction="Contains"
                    UniqueName="CrewName" AllowSorting="true" AutoPostBackOnFilter="true" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Phone" CurrentFilterFunction="Contains"
                    AutoPostBackOnFilter="true" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" CurrentFilterFunction="Contains"
                    AutoPostBackOnFilter="true" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
        </MasterTableView>
        <ClientSettings>
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
    </telerik:RadGrid>
    <br />
    <div style="padding: 5px 5px; text-align: right; width: 500px;">
        <asp:Button ID="btnSubmit" runat="server" Text="OK" OnClientClick="javascript:CheckApplytoAdditionalCrew();return false;" />
        <asp:Button ID="btndummySubmit" runat="server" OnClick="OK_Click" Style="display: none;" />
    </div>
    <asp:Label ID="InjectScript" runat="server"></asp:Label>
    <br />
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    <asp:HiddenField ID="hdnIsChoice" runat="server" />
    <asp:HiddenField ID="hdnChecklistCode" runat="server" />
    <asp:HiddenField ID="hdnCrewCode" runat="server" />
    </form>
</body>
</html>
