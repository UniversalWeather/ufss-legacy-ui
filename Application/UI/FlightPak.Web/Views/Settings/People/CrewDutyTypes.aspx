﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="CrewDutyTypes.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.CrewDutyTypes"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function forecolorChange(sender, args) {

            $get("<%=tbForeColor.ClientID%>").value = sender.get_selectedColor();
            $get("<%=hdnForeColor.ClientID%>").value = sender.get_selectedColor();

        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
        function validateHourAndMinute(oSrc, args) {
            var array = args.Value.split(":");
            if (array[0] > 23 && array[1] > 59) {
                args.IsValid = false;
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    oSrc.textContent = "Hour entry must be 0-23; Minute entry must be 0-59";
                } else {
                    oSrc.innerText = "Hour entry must be 0-23; Minute entry must be 0-59";
                }
                if (oSrc.id.indexOf("tbStart") != -1) {
                    setTimeout("document.getElementById('<%=rmtbStartTime.ClientID%>').focus()", 0);
                }
                else if (oSrc.id.indexOf("tbEnd") != -1) {
                    setTimeout("document.getElementById('<%=rmtbEndTime.ClientID%>').focus()", 0);
                }
                return false;
            }
            else if ((array[0] > 23)) {
                args.IsValid = false;
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    oSrc.textContent = "Hour entry must be 0-23";
                } else {
                    oSrc.innerText = "Hour entry must be 0-23";
                }
                if (oSrc.id.indexOf("tbStart") != -1) {
                    setTimeout("document.getElementById('<%=rmtbStartTime.ClientID%>').focus()", 0);
                }
                else if (oSrc.id.indexOf("tbEnd") != -1) {
                    setTimeout("document.getElementById('<%=rmtbEndTime.ClientID%>').focus()", 0);
                }
                return false;
            }
            else {
                if (array[1] > 59) {
                    args.IsValid = false;
                    if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                        oSrc.textContent = "Minute entry must be 0-59";
                    } else {
                        oSrc.innerText = "Minute entry must be 0-59";
                    }
                    if (oSrc.id.indexOf("tbStart") != -1) {
                        setTimeout("document.getElementById('<%=rmtbStartTime.ClientID%>').focus()", 0);
                    }
                    else if (oSrc.id.indexOf("tbEnd") != -1) {
                        setTimeout("document.getElementById('<%=rmtbEndTime.ClientID%>').focus()", 0);
                    }
                    return false;
                }
            }
        }
        function backcolorChange(sender, args) {

            $get("<%=tbBackColor.ClientID%>").value = sender.get_selectedColor();
            $get("<%=hdnBackColor.ClientID%>").value = sender.get_selectedColor();
        }  
    </script>
    <script type="text/javascript">
        function CheckTxtBox(state) {
            ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'), state);

        }
        function openReport() {

            url = "../../Reports/ExportReportInformation.aspx?Report=RptDBCrewDutyTypeExport&UserCD=UC";
            //       url = "../../Reports/ExportReportInformation.aspx?Report=RptDBCrewDutyType";
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open(url, "RadExportData");
        }
        
        function ShowReports(radWin, ReportFormat, ReportName) {
            document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
            document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
            if (ReportFormat == "EXPORT") {
                url = "../../Reports/ExportReportInformation.aspx";
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, 'RadExportData');
                return true;
            }
            else {
                return true;
            }
        }
        function OnClientCloseExportReport(oWnd, args) {
            document.getElementById("<%=btnShowReports.ClientID%>").click();
        }
    </script>
    <script type="text/javascript">
        function CheckTxtBox(control) {
            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);

                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);

                } return false;
            }
        }
        function GetDimensions(sender, args) {
            var bounds = sender.getWindowBounds();
            return;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCrewDutyTypes">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%-- <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgCrewDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCrewDutyTypes">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="cvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkWorkLoadIndex">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkOfficeDuty">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Duty Types</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBCrewDutyType');"
                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBCrewDutyTypeExport');return false;"
                            OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                        <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                            Style="display: none;" />
                        <a href="../../Help/ViewHelp.aspx?Screen=CrewDutyTypesHelp" target="_blank" title="Help"
                            class="help-icon"></a>
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                    </span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" AutoSize="false"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td class="tdLabel100" align="left">
                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                            AutoPostBack="true" />
                    </td>

                    <td class="tdLabel150" align="left">
                        <asp:CheckBox ID="chkIsCrewDuty" runat="server" Text="Crew Duty Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                            AutoPostBack="true" />
                    </td>
                    <td align="left">
                        <asp:CheckBox ID="chkIsWorkLoadIndex" runat="server" Text="Workload Index" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
AutoPostBack="true" />
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgCrewDutyTypes" runat="server" AllowSorting="true" OnItemCreated="dgCrewDutyTypes_ItemCreated"
                Visible="true" OnNeedDataSource="dgCrewDutyTypes_BindData" OnItemCommand="dgCrewDutyTypes_ItemCommand"
                OnUpdateCommand="dgCrewDutyTypes_UpdateCommand" OnInsertCommand="dgCrewDutyTypes_InsertCommand"
                OnDeleteCommand="dgCrewDutyTypes_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
                AllowPaging="true" OnSelectedIndexChanged="dgCrewDutyTypes_SelectedIndexChanged"
                OnItemDataBound="dgCrewDutyTypes_ItemDataBound" AllowFilteringByColumn="true"
                OnPageIndexChanged="dgCrewDutyTypes_PageIndexChanged" OnPreRender="dgCrewDutyTypes_PreRender"
                PagerStyle-AlwaysVisible="true" Height="341px">
                <MasterTableView DataKeyNames="DutyTypeID,DutyTypeCD,CustomerID,DutyTypesDescription,ValuePTS,GraphColor,WeekendPTS,IsWorkLoadIndex,IsOfficeDuty,LastUpdUID
        ,LastUpdTS,ForeGrndCustomColor,BackgroundCustomColor,IsCrewCurrency,HolidayPTS,IsCrewDuty,CalendarEntry,DutyStartTM,DutyEndTM,IsStandby,IsDeleted,IsInActive"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="DutyTypeCD" HeaderText="Code" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                            FilterControlWidth="80px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="DutyTypesDescription" HeaderText="Crew Duty Type"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="400px" FilterControlWidth="380px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ValuePTS" HeaderText="Value Points" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="EqualTo" HeaderStyle-Width="100px"
                            FilterControlWidth="80px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsCrewDuty" HeaderText="Crew Duty" AutoPostBackOnFilter="true"
                            ShowFilterIcon="false" CurrentFilterFunction="EqualTo" HeaderStyle-Width="80px"
                            AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" HeaderStyle-Width="80px">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddCrewDutyTypes)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                Visible='<%# IsAuthorized(Permission.Database.EditCrewDutyTypes)%>' ToolTip="Edit"
                                CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                Visible='<%# IsAuthorized(Permission.Database.DeleteCrewDutyTypes)%>' runat="server"
                                CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    <asp:CheckBox ID="chkWorkLoadIndex" AutoPostBack="true" Checked="false" runat="server"
                                        Text="Workload Index" OnCheckedChanged="chkWorkLoadIndex_CheckedChanged" TabIndex="9" />
                                </td>
                                <td class="tdLabel150" valign="top">
                                    <asp:CheckBox ID="chkCrewDuty" runat="server" Text="Crew Duty Only" TabIndex="10" />
                                </td>
                                <td class="tdLabel90" valign="top">
                                    <asp:CheckBox ID="chkOfficeDuty" AutoPostBack="true" Checked="false" runat="server"
                                        Text="Office Duty" OnCheckedChanged="chkOfficeDuty_CheckedChanged" TabIndex="11" />
                                </td>
                                <!--<td class="tdLabel160" valign="top">
                                    <asp:CheckBox ID="chkCrewCurrencyPlanner" runat="server" Text="Crew Currency Planner" TabIndex="4" />
                                </td>-->
                                <td class="tdLabel150" valign="top">
                                    <asp:CheckBox ID="chkQuickCalendarEntry" runat="server" Text="Quick Calendar Entry"
                                        TabIndex="12" />
                                </td>
                                <td class="tdLabel100" valign="top">
                                    <asp:CheckBox ID="chkCrewStandBy" runat="server" Text="Crew Standby" TabIndex="13" />
                                </td>
                                <td valign="top">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" TabIndex="14" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    <span class="mnd_text">Crew Duty Type Code</span>
                                </td>
                                <td class="tdLabel250" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCode" runat="server" MaxLength="2" CssClass="text80" ValidationGroup="Save"
                                                    AutoPostBack="true" OnTextChanged="Code_TextChanged" TabIndex="15"></asp:TextBox>
                                                <asp:HiddenField ID="hdnCDTID" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="Save" ControlToValidate="tbCode"
                                                    Text="Code is Required." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Code is Required"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel80" valign="top">
                                    Start Time
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top">
                                                <telerik:RadMaskedTextBox ID="rmtbStartTime" PromptChar="" runat="server" SelectionOnFocus="SelectAll"
                                                    Mask="##:##" TabIndex="17">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="reqtbStart" runat="server" ValidationGroup="Save" ControlToValidate="rmtbStartTime"
                                                    CssClass="alert-text" Display="Dynamic" ClientValidationFunction="validateHourAndMinute"
                                                    ErrorMessage="">
                                                </asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td class="tdLabel250" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" runat="server" MaxLength="25" CssClass="text200"
                                                    onBlur="CheckTxtBox('desc');" TabIndex="16"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="tbDescription"
                                                    ValidationGroup="Save" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Description is Required.</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel80" valign="top">
                                    End Time
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <telerik:RadMaskedTextBox ID="rmtbEndTime" PromptChar="" runat="server" SelectionOnFocus="SelectAll"
                                                    ValidationGroup="Save" Mask="##:##" CssClass="RadMaskedTextBox50" TabIndex="18">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="reqtbEnd" runat="server" ValidationGroup="Save" ControlToValidate="rmtbEndTime"
                                                    CssClass="alert-text" ClientValidationFunction="validateHourAndMinute" ErrorMessage="Hour entry must be 0-23; Minute entry must be 0-59">
                                                </asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    Value Points
                                </td>
                                <td class="tdLabel120">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="tdLabel170">
                                                <asp:TextBox ID="tbValuePoints" Enabled="false" runat="server" CssClass="text40"
                                                    MaxLength="2" onKeyPress="return fnAllowNumericAndChar(this,event,'-')" TabIndex="19"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%-- <asp:RegularExpressionValidator ID="revValuePoints" runat="server" ErrorMessage="Invalid Value Points"
                                            ControlToValidate="tbValuePoints" ForeColor="Red" ValidationExpression="^(\d|10)$"
                                            ValidationGroup="Save" SetFocusOnError="True">
                                        </asp:RegularExpressionValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" class="tdLabel140">
                                    Weekend Value Points
                                </td>
                                <td valign="middle" class="tdLabel100">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="tdLabel100">
                                                <asp:TextBox ID="tbWeekendValuePoints" runat="server" CssClass="text40" MaxLength="2"
                                                    onKeyPress="return fnAllowNumericAndChar(this,event,'-')" TabIndex="20"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%--<asp:RegularExpressionValidator ID="revWeekendValuePoints" runat="server" ErrorMessage="Invalid Weekend Value Points"
                                            ControlToValidate="tbWeekendValuePoints" ForeColor="Red" ValidationExpression="^(\d|10)$"
                                            ValidationGroup="Save" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" class="tdLabel130">
                                    Holiday Value Points
                                </td>
                                <td valign="middle">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbHolidayValuePoints" runat="server" CssClass="text40" MaxLength="2"
                                                    onKeyPress="return fnAllowNumericAndChar(this,event,'-')" TabIndex="21"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%-- <asp:RegularExpressionValidator ID="revHolidayValuePoints" runat="server" ErrorMessage="Invalid Holiday Value Points"
                                            ControlToValidate="tbHolidayValuePoints" ForeColor="Red" ValidationExpression="^(\d|10)$"
                                            ValidationGroup="Save" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    Set Calendar Colors
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    Foreground Color
                                </td>
                                <td class="tdLabel75">
                                    <asp:TextBox ID="tbForeColor" runat="server" MaxLength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                        ReadOnly="true" CssClass="text60" Text="HEXVALUE" TabIndex="22"></asp:TextBox>
                                    <asp:HiddenField ID="hdnForeColor" runat="server" />
                                </td>
                                <td>
                                    <telerik:RadColorPicker runat="server" ID="rcpForeColor" OnClientColorChange="forecolorChange"
                                        ShowIcon="true" TabIndex="23" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130">
                                    Background Color
                                </td>
                                <td class="tdLabel75">
                                    <asp:TextBox ID="tbBackColor" runat="server" MaxLength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                        ReadOnly="true" CssClass="text60" Text="HEXVALUE" TabIndex="24"></asp:TextBox>
                                    <asp:HiddenField ID="hdnBackColor" runat="server" />
                                </td>
                                <td align="left" style="z-index: 999;">
                                    <telerik:RadColorPicker runat="server" ID="rcpBackColor" OnClientColorChange="backcolorChange"
                                        ShowIcon="true" TabIndex="25" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="tblButtonArea">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            CssClass="button" ValidationGroup="Save" TabIndex="26" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click" CssClass="button"
                            CausesValidation="false" TabIndex="27" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnGridEnable" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
