﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class PassengerNamePopup : BaseSecuredPage
    {



        List<string> crewRoaster = new List<string>();
        private ExceptionManager exManager;
        private string VendorCD;
        private string PaxLastName = string.Empty;
        public bool showCommandItems = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        showCommandItems = true;
                        if (Request.QueryString["PaxLastName"] != null)
                        {
                            PaxLastName = Convert.ToString(Request.QueryString["PaxLastName"]);
                        }
                        if (!IsPostBack)
                        {
                            List<GetAllPassenger> PassengerList = new List<GetAllPassenger>();
                            using (FlightPakMasterService.MasterCatalogServiceClient PassengerService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var PassengerInfo = PassengerService.GetPassengerList();
                                if (PassengerInfo.ReturnFlag == true)
                                    PassengerList = PassengerInfo.EntityList.Where(x => x.IsDeleted == false).ToList<GetAllPassenger>();
                                Session["PassengerList"] = PassengerList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }



        }

        /// <summary>
        /// Bound Row styles, based on Checklist information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewRoster_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRoster_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAircraftType);
                }
            }

        }

        /// <summary>
        /// Method to Bind into "CrewNewTypeRating" session, and 
        /// Call "CloseAndRebind" javascript function to Close and
        /// Rebind the selected record into parent page.
        /// </summary>
        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewRoster.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgCrewRoster.SelectedItems[0];
                        Session["NewPaxSelectID"] = Convert.ToString(Item.GetDataKeyValue("PassengerRequestorID"));
                        //InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "CloseAndRebind('navigateToInserted')", true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }

        /// <summary>
        /// Bind Crew Roaster Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgCrewRoster_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewRosterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FilterAndSearch(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        private bool FilterAndSearch(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsDataBind))
            {
                
                    List<GetAllPassenger> PassengerList = new List<GetAllPassenger>();
                    if (Session["PassengerList"] != null)
                    {
                        PassengerList = (List<FlightPakMasterService.GetAllPassenger>)Session["PassengerList"];
                    }
                    CheckClientFilterCodeExist();
                    if (PassengerList.Count != 0)
                    {

                        if (chkShowAllCrew.Checked == false && PaxLastName != string.Empty)
                        {
                            PassengerList = PassengerList.Where(x => (x.LastName != null)).ToList();
                            PassengerList = PassengerList.Where(x => (x.LastName.Trim().ToUpper().Equals(PaxLastName.ToString().ToUpper().Trim())) && (x.IsDeleted == false)).ToList();
                        }
                        else if (chkShowAllCrew.Checked == false && PaxLastName == string.Empty)
                        {
                            PassengerList = PassengerList.Where(x => (x.LastName == null) || ((x.LastName != null) && (x.LastName.Trim().ToUpper().Equals(PaxLastName.ToString().ToUpper().Trim())))).ToList();
                        }
                        else if (chkShowAllCrew.Checked == true)
                        {

                        }
                       
                        if (!string.IsNullOrEmpty(tbClientCodeFilter.Text))
                        {
                            PassengerList = PassengerList.Where(x => x.ClientCD != null && x.ClientCD.Trim() == tbClientCodeFilter.Text.Trim()).ToList<GetAllPassenger>();
                        }

                        dgCrewRoster.DataSource = PassengerList;
                        if (IsDataBind)
                        {
                            dgCrewRoster.DataBind();
                        }
                    }
                
                return false;
            }
        }

        protected void dgCrewRoster_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCrewRoster.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCrewRoster;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    if (dgCrewRoster.Items.Count > 0)
            //    {
            //        dgCrewRoster.SelectedIndexes.Add(0);
            //        GridDataItem Item = (GridDataItem)dgCrewRoster.SelectedItems[0];
            //        Item.Selected = true;

            //    }
            //}

            if (!IsPostBack)
            {
                if (dgCrewRoster.Items.Count > 0)
                {
                    SelectItem();
                }
                //else
                //{
                //    dgCrewRoster.SelectedIndexes.Add(0);
                //}
            }
        }

        protected void SelectItem()
        {
            if (!string.IsNullOrEmpty(VendorCD))
            {

                foreach (GridDataItem item in dgCrewRoster.MasterTableView.Items)
                {
                    if (item["PassengerRequestorCD"].Text.Trim().ToUpper() == VendorCD)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (dgCrewRoster.MasterTableView.Items.Count > 0)
                {
                    dgCrewRoster.SelectedIndexes.Add(0);

                }
            }
        }

        #region Filters


        protected void ClientCodeFilter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckClientFilterCodeExist())
                        {
                            cvClientCodeFilter.IsValid = false;
                            tbClientCodeFilter.Text = "";
                            tbClientCodeFilter.Focus();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

       

        private bool CheckClientFilterCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClientCodeFilter.Text.Trim() != string.Empty) && (tbClientCodeFilter.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValueFilter = ClientService.GetClientCodeList().EntityList.Where(x => (x.ClientCD != null));

                            var ClientValue = ClientValueFilter.Where(x => x.ClientCD.Trim().ToUpper().Equals(tbClientCodeFilter.Text.ToString().ToUpper().Trim()));
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                RetVal = true;
                            }
                            else
                            {
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            FilterAndSearch(true);
        }

        #endregion

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewRoster_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");
            }
        }

        protected void dgCrewRoster_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAircraftType);
                }
            }

        }

    }
}
