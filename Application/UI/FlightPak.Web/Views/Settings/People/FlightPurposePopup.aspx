﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FlightPurposePopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.FlightPurposePopup" ClientIDMode="AutoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Flight Purposes</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgFlightPurpose.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgFlightPurpose.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "FlightPurposeCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "FlightPurposeID");
                        if (i == 0)
                            oArg.FlightPurposeCD = cell1.innerHTML + ",";
                        else
                            oArg.FlightPurposeCD += cell1.innerHTML + ",";
                        oArg.FlightPurposeID += cell2.innerHTML + ",";
                    }
                }
                else {
                    oArg.FlightPurposeCD = "";
                    oArg.FlightPurposeID = "";
                }
                if (oArg.FlightPurposeCD != "")
                    oArg.FlightPurposeCD = oArg.FlightPurposeCD.substring(0, oArg.FlightPurposeCD.length - 1)
                else
                    oArg.AircraftCD = "";

                oArg.Arg1 = oArg.FlightPurposeCD;
                oArg.CallingButton = "FlightPurposeCD";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgFlightPurpose">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFlightPurpose" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFlightPurpose" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" /></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                    OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgFlightPurpose" runat="server" AllowMultiRowSelection="true"
                        AllowSorting="true" AutoGenerateColumns="false" Height="341px" AllowPaging="false" 
                        OnItemCreated="dgFlightPurpose_ItemCreated" OnPreRender="dgFlightPurpose_PreRender" OnItemCommand="dgFlightPurpose_ItemCommand"
                        Width="700px" AllowFilteringByColumn="true" OnNeedDataSource="dgFlightPurpose_BindData">
                        <MasterTableView DataKeyNames="FlightPurposeID,FlightPurposeCD,FlightPurposeDescription,CustomerID,IsDefaultPurpose,IsPersonalTravel,ClientID,LastUpdUID,LastUpdTS,IsWaitList,IsDeleted"
                            CommandItemDisplay="None" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn DataField="FlightPurposeCD" HeaderText="Code" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FlightPurposeDescription" HeaderText="Description"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="440px" FilterControlWidth="420px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="IsDefaultPurpose" HeaderText="Def." AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="EqualTo" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridCheckBoxColumn DataField="IsPersonalTravel" HeaderText="Personal" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="EqualTo" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="FlightPurposeID" HeaderText="FlightPurposeID"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                    Display="false" FilterDelay="500">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grd_ok">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                        Ok</button>
                                    <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                                </div>
                                <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                        visible="false">
                        Use CTRL key to multi select</div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent"  />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
