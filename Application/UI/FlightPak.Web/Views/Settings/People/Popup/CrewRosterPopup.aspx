﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewRosterPopup.aspx.cs"
    ClientIDMode="AutoID" Inherits="FlightPak.Web.Views.Settings.People.Popup.CrewRosterPopup" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Crew Roster</title>
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
    <link href="../../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgCrewRoster.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadCrewGroupPopup") {
                    url = "../People/CrewGroupPopup.aspx?CrewGroupCD=" + document.getElementById("<%=tbCrewGroupFilter.ClientID%>").value;
                }
                else if (radWin == "RadClientCodePopup") {
                    url = "../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCodeFilter.ClientID%>").value;
                }
                var oWnd = radopen(url, radWin);
            }
            function CrewGroupPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCrewGroupFilter.ClientID%>").value = arg.CrewGroupCD;
                        document.getElementById("<%=hdnCrewGroupID.ClientID%>").value = arg.CrewGroupID;
                        document.getElementById("<%=cvCrewGroupCodeFilter.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCrewGroupFilter.ClientID%>").value = "";
                        document.getElementById("<%=cvCrewGroupCodeFilter.ClientID%>").innerHTML = "";
                    }
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function ClientCodeFilterPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=cvClientCodeFilter.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = "";
                        document.getElementById("<%=cvClientCodeFilter.ClientID%>").innerHTML = "";
                    }
                }
            }
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCrewRoster.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "CrewCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "HomeBaseCD");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "CrewName");
                        var cell4 = MasterTable.getCellByColumnUniqueName(row, "CrewID");
                        if (i == 0) {
                            oArg.CrewCD = cell1.innerHTML + ",";
                            oArg.HomeBase = cell2.innerHTML;
                            oArg.CrewName = cell3.innerHTML;
                            oArg.CrewID = cell4.innerHTML;
                        }
                        else {
                            oArg.CrewCD += cell1.innerHTML + ",";
                            oArg.HomeBase += cell2.innerHTML + ",";
                            oArg.CrewName += cell3.innerHTML;
                            oArg.CrewID += cell4.innerHTML + ",";
                        }
                    }
                }
                else {
                    oArg.CrewCD = "";
                    oArg.HomeBase = "";
                    oArg.CrewName = "";
                    oArg.CrewID = "";
                }
                if (oArg.CrewCD != "")
                    oArg.CrewCD = oArg.CrewCD.substring(0, oArg.CrewCD.length - 1)
                else
                    oArg.CrewCD = "";

                oArg.Arg1 = oArg.CrewCD;
                oArg.CallingButton = "CrewCD"; // ( the name TypeCode should be same as popup tag)

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgCrewRoster.ClientID %>").get_masterTableView();
                masterTable.rebind();
                masterTable.clearSelectedItems();
                masterTable.selectItem(masterTable.get_dataItems()[0].get_element());
            }
            function GetGridId() {
                return $find("<%= dgCrewRoster.ClientID %>");
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <asp:ScriptManager ID="scr1" runat="server">
    </asp:ScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating" DefaultLoadingPanelID = "RadAjaxLoadingPanel1"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <%--<telerik:AjaxSetting AjaxControlID="divExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="dgCrewRoster">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbClientCodeFilter">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCrewGroupFilter">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCrewGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="CrewGroupPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewGroupPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCodeFilterPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlSearchPanel" runat="server" Visible="true">
            <div class="divGridPanel">
                <table class="box1">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="tdLabel100" valign="top">
                                        <asp:CheckBox ID="chkActiveOnly" runat="server" Checked="false" Text="Active Only" />
                                    </td>
                                    <td class="tdLabel120" valign="top">
                                        <asp:CheckBox ID="chkHomeBaseOnly" runat="server" Checked="false" Text="Home Base Only" />
                                    </td>
                                    <td class="tdLabel90" valign="top">
                                        <asp:CheckBox ID="chkFixedWingOnly" runat="server" Checked="false" Text="Fixed Wing" />
                                    </td>
                                    <td class="tdLabel100" valign="top">
                                        <asp:CheckBox ID="chkRotaryWingOnly" runat="server" Checked="false" Text="Rotary Wing" />
                                    </td>
                                    <td valign="top">
                                        <table id="Table1" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    <asp:Label runat="server" ID="lbClientCode">Client Code :</asp:Label>
                                                </td>
                                                <td class="tdLabel100">
                                                    <asp:TextBox ID="tbClientCodeFilter" runat="server" MaxLength="5" ValidationGroup="Save"
                                                        OnTextChanged="ClientCodeFilter_TextChanged" AutoPostBack="true" CssClass="text50"></asp:TextBox>
                                                    <asp:Button ID="btnClientCodeFilter" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:CustomValidator ID="cvClientCodeFilter" runat="server" ControlToValidate="tbClientCodeFilter"
                                                        ErrorMessage="Invalid ClientCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                        <table id="Exception" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    <asp:Label runat="server" ID="Label2">Crew Group :</asp:Label>
                                                </td>
                                                <td class="tdLabel100">
                                                    <asp:TextBox ID="tbCrewGroupFilter" CssClass="text50" runat="server" OnTextChanged="CrewGroupFilter_TextChanged"
                                                        AutoPostBack="true" />
                                                    <asp:Button ID="btnCrewGroupFilter" OnClientClick="javascript:openWin('RadCrewGroupPopup');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:CustomValidator ID="cvCrewGroupCodeFilter" runat="server" ControlToValidate="tbCrewGroupFilter"
                                                        ErrorMessage="Invalid Crew Group" Display="Dynamic" CssClass="alert-text"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel100" valign="top">
                                        <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                            ValidationGroup="Save" OnClick="Search_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadGrid ID="dgCrewRoster" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
                                OnItemDataBound="dgCrewRoster_ItemDataBound" OnNeedDataSource="dgCrewRoster_BindData"
                                OnItemCommand="dgCrewRoster_ItemCommand" AutoGenerateColumns="false" Height="330px"
                                OnPageIndexChanged="dgCrewRoster_PageIndexChanged" PagerStyle-AlwaysVisible="true"
                                AllowPaging="false" Width="850px" AllowFilteringByColumn="true" OnDeleteCommand="dgCrewRoster_DeleteCommand"
                                OnItemCreated="dgCrewRoster_ItemCreated" OnPreRender="dgCrewRoster_PreRender">
                                <MasterTableView DataKeyNames="CrewID,CrewCD,PhoneNum,HomeBaseID,IsStatus,HomeBaseCD,LastName,FirstName,MiddleInitial"
                                    CommandItemDisplay="Bottom">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="CrewCD" HeaderText="Code" AutoPostBackOnFilter="true"
                                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                            FilterControlWidth="80px">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CrewName" HeaderText="Crew Name" CurrentFilterFunction="StartsWith"
                                            AllowSorting="true" AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="380px"
                                            FilterControlWidth="360px">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Phone" AutoPostBackOnFilter="true"
                                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px"
                                            FilterControlWidth="100px">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" AutoPostBackOnFilter="true"
                                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                            FilterControlWidth="80px">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridCheckBoxColumn DataField="IsStatus" HeaderText="Active" AutoPostBackOnFilter="true"
                                            AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                            HeaderStyle-Width="65px">
                                        </telerik:GridCheckBoxColumn>
                                        <telerik:GridTemplateColumn CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="true"
                                            HeaderStyle-Font-Bold="true" HeaderStyle-Width="60px" ShowFilterIcon="false"
                                            HeaderText="Checklist" HeaderStyle-HorizontalAlign="Center" AllowFiltering="false">
                                            <ItemTemplate>
                                                <center>
                                                    <asp:Label ID="lbCheckList" runat="server" Text='<%# Eval("CheckList") %>'></asp:Label>
                                                </center>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="CrewID" HeaderText="CrewID" AutoPostBackOnFilter="true"
                                            Display="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                        <div class="grid_icon">
                                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                                CommandName="PerformInsert" Visible='<%# showCommandItems && IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.AddCrewRoster)%>'></asp:LinkButton>
                                            <%-- --%>
                                            <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                                CommandName="Edit" Visible='<%# showCommandItems && IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.EditCrewRoster)%>'
                                                OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                                            <%-- --%>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgCrewRoster', 'radCrewRosterPopup');"
                                                CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# showCommandItems && IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.DeleteCrewRoster)%>'></asp:LinkButton>
                                            <%-- CommandName="DeleteSelected" --%>
                                        </div>
                                        <div class="grd_ok">
                                            <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                                Ok</button>
                                            <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                                        </div>
                                        <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                            visible="false">
                                            Use CTRL key to multi select</div>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings>
                                    <ClientEvents OnRowDblClick="returnToParent" />
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                                <GroupingSettings CaseSensitive="false"></GroupingSettings>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnCrewGroupID" runat="server" />
                <asp:HiddenField ID="hdnClientID" runat="server" />
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
