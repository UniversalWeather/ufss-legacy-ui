﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PassengerRequestorsPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.Popup.PassengerRequestorsPopup" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Passenger/Requestor</title>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgRequestor.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgRequestor.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "PassengerRequestorCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "PassengerName");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "AdditionalPhoneNum");
                        var cell4 = MasterTable.getCellByColumnUniqueName(row, "PassengerRequestorID");

                        if (i == 0) {
                            oArg.PassengerRequestorCD = cell1.innerHTML + ",";
                            oArg.PassengerName = cell2.innerHTML;
                            oArg.PhoneNum = cell3.innerHTML;
                            oArg.PassengerRequestorID = cell4.innerHTML;
                        }
                        else {
                            oArg.PassengerRequestorCD += cell1.innerHTML + ",";
                            oArg.PassengerName += cell2.innerHTML + ",";
                            oArg.PhoneNum += cell3.innerHTML + ",";
                            oArg.PassengerRequestorID += cell4.innerHTML + ",";
                        }
                    }
                }
                else {
                    oArg.PassengerRequestorCD = "";
                    oArg.PassengerName = "";
                    oArg.PhoneNum = "";
                    oArg.PassengerRequestorID = "";

                }
                if (oArg.PassengerRequestorCD != "")
                    oArg.PassengerRequestorCD = oArg.PassengerRequestorCD.substring(0, oArg.PassengerRequestorCD.length - 1)
                else
                    oArg.PassengerRequestorCD = "";

                oArg.Arg1 = oArg.PassengerRequestorCD;
                oArg.CallingButton = "<%=Microsoft.Security.Application.Encoder.HtmlEncode(Param) %>";
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function ClientCodeFilterPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=cvClientCodeFilter.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = "";
                        document.getElementById("<%=cvClientCodeFilter.ClientID%>").innerHTML = "";
                    }
                }
            }
            function openWin(radWin) {
                var url = '';

                if (radWin == "RadClientCodePopup") {
                    url = "../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCodeFilter.ClientID%>").value;
                }
                var oWnd = radopen(url, radWin);
            }

            function Close() {
                GetRadWindow().Close();
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgRequestor.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            function GetGridId() {
                return $find("<%= dgRequestor.ClientID %>");
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgRequestor">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgRequestor" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgRequestor" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="ClientCodeFilterPopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="tdLabel150" valign="top">
                                <table id="tblSelection" runat="server" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rbtnSelected" runat="server" GroupName="ReportSelect" Text="Selected"
                                                Checked="true" OnCheckedChanged="Selected_CheckedChanged" AutoPostBack="true" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rbtnSelectall" runat="server" GroupName="ReportSelect" Text="All"
                                                OnCheckedChanged="Selected1_CheckedChanged" AutoPostBack="true" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdLabel470" valign="top">
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Checked="true" Text="Active Only" />
                                <asp:CheckBox ID="chkHomeBaseOnly" runat="server" Text="Home Base Only" />
                                <asp:CheckBox ID="chkRequestorOnly" runat="server" Text="Requestor Only" />
                                <asp:CheckBox ID="chkCQCustomerOnly" runat="server" Text="Customer Only" />
                            </td>
                            <td class="tdLabel180" valign="top">
                                <table id="tblClient" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="tdLabel80">
                                            <asp:Label runat="server" ID="lbClientCode">Client Code :</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbClientCodeFilter" runat="server" MaxLength="5" ValidationGroup="Save"
                                                OnTextChanged="ClientCodeFilter_TextChanged" AutoPostBack="true" CssClass="text50"></asp:TextBox>
                                            <asp:Button ID="btnClientCodeFilter" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                CssClass="browse-button" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CustomValidator ID="cvClientCodeFilter" runat="server" ControlToValidate="tbClientCodeFilter"
                                                ErrorMessage="Invalid ClientCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" CssClass="button" Text="Search"
                                    OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                    <telerik:RadGrid ID="dgRequestor" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
                        OnNeedDataSource="dgRequestor_BindData" AutoGenerateColumns="false" Width="900px"
                        AllowPaging="true" PageSize="10" PagerStyle-AlwaysVisible="true" Height="341px"
                        OnPageIndexChanged="dgRequestor_PageIndexChanged" OnItemCreated="dgRequestor_ItemCreated"
                        OnItemCommand="dgPassengerCatalog_ItemCommand" OnDeleteCommand="dgPassengerCatalog_DeleteCommand">
                        <MasterTableView AllowPaging="true" DataKeyNames="PassengerRequestorID,PassengerRequestorCD,PassengerName,PhoneNum,IsRequestor,AdditionalPhoneNum"
                            CommandItemDisplay="Bottom">
                            <Columns>
                                <telerik:GridBoundColumn DataField="PassengerRequestorID" HeaderText="PassengerRequestorID"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="PAX Code" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PassengerName" HeaderText="Passenger/Requestor"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                    HeaderStyle-Width="200px" FilterControlWidth="180px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="Dept" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DepartmentName" HeaderText="Dept. Desc" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="200px"
                                    FilterControlWidth="180px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AdditionalPhoneNum" HeaderText="Phone" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="Active" AutoPostBackOnFilter="true"
                                    AllowFiltering="False" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                    HeaderStyle-Width="60px">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridCheckBoxColumn DataField="IsRequestor" HeaderText="Req" AutoPostBackOnFilter="true"
                                    AllowFiltering="False" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                    HeaderStyle-Width="60px">
                                </telerik:GridCheckBoxColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grid_icon">
                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                        CommandName="PerformInsert" Visible='<%# showCommandItems && IsUIReports && IsAuthorized(Permission.Database.AddPassengerRequestor)%>'></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                        CommandName="Edit" Visible='<%# showCommandItems && IsUIReports && IsAuthorized(Permission.Database.EditPassengerRequestor)%>'
                                        OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgRequestor', 'radPaxInfoPopup');"
                                        CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# showCommandItems && IsUIReports && IsAuthorized(Permission.Database.DeletePassengerRequestor)%>'></asp:LinkButton>
                                    <%-- CommandName="DeleteSelected" --%>
                                </div>
                                <div class="grd_ok">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                        Ok</button>
                                </div>
                                <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                    visible="false">
                                    Use CTRL key to multi select</div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
