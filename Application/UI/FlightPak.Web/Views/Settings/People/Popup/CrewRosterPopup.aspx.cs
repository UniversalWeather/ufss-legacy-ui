﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People.Popup
{
    public partial class CrewRosterPopup : BaseSecuredPage
    {
        List<string> crewRoaster = new List<string>();
        private ExceptionManager exManager;
        private string VendorCD;
        public bool showCommandItems = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["CrewCD"] != null)
            {
                if (Request.QueryString["CrewCD"].ToString() == "1")
                {
                    dgCrewRoster.AllowMultiRowSelection = true;
                }
                else
                {
                    dgCrewRoster.AllowMultiRowSelection = false;
                }
            }
            else
            {
                dgCrewRoster.AllowMultiRowSelection = false;
            }
            if (!IsPostBack)
            {
                chkActiveOnly.Checked = true;
            }
            //Added for Reassign the Selected Value and highlight the specified row in the Grid           

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        showCommandItems = true;
                        if (Request.QueryString["CrewCD"] != null)
                        {
                            //VendorCD = Request.QueryString["CrewCD"];

                            //if (!string.IsNullOrEmpty(VendorCD))
                            //{
                            //    dgCrewRoster.Rebind();

                            //    foreach (GridDataItem item in dgCrewRoster.MasterTableView.Items)
                            //    {
                            //        if (item["CrewCD"].Text.Trim().ToUpper() == VendorCD)
                            //        {
                            //            item.Selected = true;
                            //            break;
                            //        }
                            //    }
                            //}
                        }
                        else
                        {
                            //if (dgCrewRoster.MasterTableView.Items.Count > 0)
                            //{
                            //    dgCrewRoster.SelectedIndexes.Add(0);

                            //}
                        }
                        //Ramesh: Defect# 7146, Commented code to display CRUD operation in System Tools
                        //if (!string.IsNullOrEmpty(Request.QueryString["fromPage"]) && Request.QueryString["fromPage"] == "SystemTools")
                        //{
                        //    showCommandItems = false;
                        //}

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }



        }

        /// <summary>
        /// Bound Row styles, based on Checklist information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewRoster_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            Label lbCheckList = (Label)Item.FindControl("lbCheckList");
                            if (lbCheckList != null && lbCheckList.Text.Trim() == "0")
                            {
                                lbCheckList.Text = "!";
                                lbCheckList.ForeColor = System.Drawing.Color.Red;
                                Item.ForeColor = System.Drawing.Color.Red;
                            }
                            else
                            {
                                lbCheckList.Text = "";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Bind Crew Roaster Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgCrewRoster_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewRosterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (CrewRosterService != null)
                            {
                                var CrewData = CrewRosterService.GetCrewList();
                                if (CrewData != null)
                                {
                                    var CrewList = CrewData.EntityList.Where(x => x.IsDeleted == false).ToList();
                                    if (CrewData.ReturnFlag == true)
                                    {
                                        dgCrewRoster.DataSource = CrewList;
                                    }
                                    Session["CrewListPopUp"] = (List<GetAllCrew>)CrewList;
                                    DoSearchfilter(false);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        protected void dgCrewRoster_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCrewRoster.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCrewRoster;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    if (dgCrewRoster.Items.Count > 0)
            //    {
            //        dgCrewRoster.SelectedIndexes.Add(0);
            //        GridDataItem Item = (GridDataItem)dgCrewRoster.SelectedItems[0];
            //        Item.Selected = true;

            //    }
            //}

            if (!IsPostBack)
            {
                if (dgCrewRoster.Items.Count > 0)
                {
                    SelectItem();
                }
                //else
                //{
                //    dgCrewRoster.SelectedIndexes.Add(0);
                //}
            }
        }

        protected void SelectItem()
        {
            if (!string.IsNullOrEmpty(VendorCD))
            {

                foreach (GridDataItem item in dgCrewRoster.MasterTableView.Items)
                {
                    if (item["CrewCD"].Text.Trim().ToUpper() == VendorCD)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (dgCrewRoster.MasterTableView.Items.Count > 0)
                {
                    dgCrewRoster.SelectedIndexes.Add(0);

                }
            }
        }

        #region Filters

        protected void ActiveOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchfilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void HomeBaseOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        DoSearchfilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }
        protected void FixedWingOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        DoSearchfilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void RotaryWing_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        DoSearchfilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void CrewGroupFilter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckCrewGroupFilterCodeExist())
                        {
                            cvCrewGroupCodeFilter.IsValid = false;
                            tbCrewGroupFilter.Text = "";
                            tbCrewGroupFilter.Focus();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void ClientCodeFilter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckClientFilterCodeExist())
                        {
                            cvClientCodeFilter.IsValid = false;
                            tbClientCodeFilter.Text = "";
                            tbClientCodeFilter.Focus();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        public bool DoSearchfilter(bool IsDatabind)
        {
            using (PreflightService.PreflightServiceClient Service1 = new PreflightService.PreflightServiceClient())
            {
                List<PreflightService.GetAllCrewForPopup> FilterCrew = new List<PreflightService.GetAllCrewForPopup>();

                CheckCrewGroupFilterCodeExist();
                CheckClientFilterCodeExist();

                if ((tbCrewGroupFilter.Text.Trim() != ""))
                {
                    var objRetVal = Service1.GetAllCrewForPopup(hdnClientID.Value == string.Empty? 0: Convert.ToInt64(hdnClientID.Value) ,chkActiveOnly.Checked, chkRotaryWingOnly.Checked, chkFixedWingOnly.Checked, chkHomeBaseOnly.Checked? UserPrincipal.Identity._airportICAOCd: string.Empty, Convert.ToInt64(hdnCrewGroupID.Value));
                    if (objRetVal.ReturnFlag)
                    {
                        FilterCrew = objRetVal.EntityList;
                    }
                }
                else
                {
                    var objRetVal = Service1.GetAllCrewForPopup(hdnClientID.Value == string.Empty ? 0 : Convert.ToInt64(hdnClientID.Value), chkActiveOnly.Checked, chkRotaryWingOnly.Checked, chkFixedWingOnly.Checked, chkHomeBaseOnly.Checked ? UserPrincipal.Identity._airportICAOCd : string.Empty, 0);
                    if (objRetVal.ReturnFlag)
                    {
                        FilterCrew = objRetVal.EntityList;
                    }
                }

                dgCrewRoster.DataSource = FilterCrew;
                if (IsDatabind)
                {
                    dgCrewRoster.DataBind();
                }
            }
            return false;
        }

        private bool CheckClientFilterCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClientCodeFilter.Text.Trim() != string.Empty) && (tbClientCodeFilter.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValueFilter = ClientService.GetClientCodeList().EntityList.Where(x => (x.ClientCD != null)).ToList();

                            var ClientValue = ClientValueFilter.Where(x => x.ClientCD.Trim().ToUpper().Equals(tbClientCodeFilter.Text.ToString().ToUpper().Trim())).SingleOrDefault();
                            if ( ClientValue == null)
                            {
                                hdnClientID.Value = string.Empty;
                                RetVal = false;
                            }
                            else
                            {
                                hdnClientID.Value = ClientValue.ClientID.ToString();
                                RetVal = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        private bool CheckCrewGroupFilterCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbCrewGroupFilter.Text.Trim() != string.Empty) && (tbCrewGroupFilter.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValue = ClientService.GetCrewGroupInfo().EntityList.Where(x => x.CrewGroupCD.Trim().ToUpper().Equals(tbCrewGroupFilter.Text.ToString().ToUpper().Trim()) && x.IsInActive == false).ToList();
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                RetVal = false;
                            }
                            else
                            {
                                hdnCrewGroupID.Value = ClientValue[0].CrewGroupID.ToString();
                                RetVal = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            DoSearchfilter(true);
        }

        #endregion

        protected void dgCrewRoster_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    string resolvedurl = string.Empty;
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                //e.Item.Selected = true;

                                GridDataItem item = dgCrewRoster.SelectedItems[0] as GridDataItem;
                                Session["SelectedCrewRosterID"] = item["CrewID"].Text;
                                TryResolveUrl("/Views/Settings/People/CrewRoster.aspx?IsPopup=", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "ProcessPopupAdd('AccountsCatalog.aspx?IsPopup=', 'Add Accounts', '1000','500', '50', '50');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radCrewRosterCRUDPopup');", true);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/People/CrewRoster.aspx?IsPopup=Add", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radCrewRosterCRUDPopup');", true);
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void dgCrewRoster_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string SelectedCrewRosterID = string.Empty;
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            GridDataItem item = dgCrewRoster.SelectedItems[0] as GridDataItem;
                            SelectedCrewRosterID = item["CrewID"].Text;
                            Crew Crew = new Crew();
                            string CrewID = SelectedCrewRosterID;
                            string CrewCD = string.Empty;
                            Crew.CrewID = Convert.ToInt64(CrewID);
                            Crew.CrewCD = item["CrewCD"].Text;                    // Doubt
                            Crew.IsDeleted = true;
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.Crew, Convert.ToInt64(SelectedCrewRosterID));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                                    return;
                                }
                            }
                            CrewRosterService.DeleteCrew(Crew);
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
                finally
                {
                    if (Session["SelectedCrewRosterID"] != null)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.Crew, Convert.ToInt64(SelectedCrewRosterID));
                        }
                    }
                }

            }
        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewRoster_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["CrewCD"] != null)
                {
                    if (Request.QueryString["CrewCD"].ToString() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }

        protected void dgCrewRoster_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewRoster, Page.Session);
        }

    }
}