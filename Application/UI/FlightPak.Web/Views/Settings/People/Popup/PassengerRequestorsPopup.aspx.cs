﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Web.UI.HtmlControls;

namespace FlightPak.Web.Views.Settings.People.Popup
{
    public partial class PassengerRequestorsPopup : BaseSecuredPage
    {
        string homebaseCD = string.Empty;
        string homebaseID = string.Empty;
        private ExceptionManager exManager;
        private string PassengerRequestorID;
        private bool blShowRequestor = false;
        private bool FilterCR = false;
        public string Param = string.Empty;
        List<Passenger> filtered = new List<Passenger>();

        public bool showCommandItems = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Request.QueryString["PassengerRequestorID"] != null)
                        {
                            if (Request.QueryString["PassengerRequestorID"].ToString().Length > 0)
                            {
                                dgRequestor.AllowMultiRowSelection = true;
                            }
                            else
                            {
                                dgRequestor.AllowMultiRowSelection = false;
                            }
                            tblSelection.Visible = true;
                        }
                        else
                        {
                            dgRequestor.AllowMultiRowSelection = false;
                            tblSelection.Visible = false;
                        }
                        if (Request.QueryString["ControlName"] != null)
                        {
                            Param = Request.QueryString["ControlName"];

                        }
                        if (Request.QueryString["fromPage"] != null && Request.QueryString["fromPage"] == "CQPax")
                        {
                            dgRequestor.AllowMultiRowSelection = false;
                            tblSelection.Visible = false;
                        }
                        if (Request.QueryString["fromPage"] != null && Request.QueryString["fromPage"] == "CRMain")
                        {
                            FilterCR = true;
                        }
                        showCommandItems = true;

                        if (!IsPostBack)
                        {
                            if (UserPrincipal.Identity._isSysAdmin == false && UserPrincipal.Identity._travelCoordID != null && FilterCR == true)
                            {
                                List<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR> PassengerList = new List<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR>();
                                using (CorporateRequestService.CorporateRequestServiceClient PassengerService = new CorporateRequestService.CorporateRequestServiceClient())
                                {
                                    var PassengerInfo = PassengerService.GetCRPaxbyPaxID(Convert.ToInt64(UserPrincipal.Identity._travelCoordID));
                                    if (PassengerInfo.ReturnFlag == true)
                                        PassengerList = PassengerInfo.EntityList.Where(x => x.IsDeleted == false).ToList<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR>();
                                    Session["CRPassengerList"] = PassengerList;
                                }
                            }
                            else
                            {
                                //List<GetAllPassenger> PassengerList = new List<GetAllPassenger>();
                                //using (FlightPakMasterService.MasterCatalogServiceClient PassengerService = new FlightPakMasterService.MasterCatalogServiceClient())
                                //{
                                //    var PassengerInfo = PassengerService.GetPassengerList();
                                //    if (PassengerInfo.ReturnFlag == true)
                                //        PassengerList = PassengerInfo.EntityList.Where(x => x.IsDeleted == false).ToList<GetAllPassenger>();
                                //    Session["PassengerList"] = PassengerList;
                                //}
                            }

                            //Added for Reassign the Selected Value and highlight the specified row in the Grid
                            if (Request.QueryString["PassengerRequestorID"] != null)
                            {
                                PassengerRequestorID = Request.QueryString["PassengerRequestorID"];
                            }
                            if (!string.IsNullOrEmpty(Request.QueryString["ShowRequestor"]))
                            {
                                blShowRequestor = Convert.ToBoolean(Request.QueryString["ShowRequestor"]);

                                //Added for Preflight
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == true)
                                {
                                    chkHomeBaseOnly.Checked = false;
                                }
                                else
                                {
                                    chkHomeBaseOnly.Checked = true;
                                }
                                if (UserPrincipal.Identity._fpSettings._IsReqOnly == true)
                                {
                                    chkRequestorOnly.Checked = true;
                                }
                                else
                                {
                                    chkRequestorOnly.Checked = false;
                                }
                            }

                            if (!string.IsNullOrEmpty(Request.QueryString["fromPage"]) && (Request.QueryString["fromPage"] == "SystemTools" || Request.QueryString["fromPage"] == "Report"))
                            {
                                // Added seperately for System Tools
                                if (blShowRequestor)
                                {
                                    chkRequestorOnly.Checked = true;
                                    //Ramesh: Defect# 7146, Added to display CRUD opertaions from System Tools                                    
                                    showCommandItems = true;
                                }
                                else
                                {
                                    chkRequestorOnly.Checked = false;
                                    showCommandItems = false;
                                }
                            }

                            //MR19032012 - Added for CQ Customer based filter
                            chkCQCustomerOnly.Visible = false;
                            if (Request.QueryString["CQCustomerID"] != null && !string.IsNullOrEmpty(Request.QueryString["CQCustomerID"]))
                            {
                                chkCQCustomerOnly.Visible = true;
                                //chkCQCustomerOnly.Checked = true;
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }








        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgRequestor_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //List<GetAllPassenger> PassengerList = new List<GetAllPassenger>();

                        //using (FlightPakMasterService.MasterCatalogServiceClient PassengerService = new FlightPakMasterService.MasterCatalogServiceClient())
                        //{
                        //    var PassengerInfo = PassengerService.GetPassengerList();
                        //    if (PassengerInfo.ReturnFlag == true)
                        //    {
                        //        PassengerList = PassengerInfo.EntityList.Where(x => x.IsDeleted == false).ToList<GetAllPassenger>();
                        //    }
                        //    dgRequestor.DataSource = PassengerList;
                        //    Session["PassengerList"] = PassengerList;
                        //if ((chkSearchActiveOnly.Checked == true) || (chkRequestorOnly.Checked == true) || (chkHomeBaseOnly.Checked == true) || (tbClientCodeFilter.Text != string.Empty))
                        //{
                        FilterAndSearch(false);
                        //}
                        //}

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgRequestor;
                        if (!IsPostBack)
                        {
                            SelectItem();
                            if (rbtnSelectall.Checked == true)
                            {
                                foreach (GridDataItem item in dgRequestor.MasterTableView.Items)
                                {
                                    item.Selected = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(PassengerRequestorID))
                {
                    foreach (GridDataItem item in dgRequestor.MasterTableView.Items)
                    {
                        if (item["PassengerRequestorID"].Text.Trim() == PassengerRequestorID)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgRequestor.MasterTableView.Items.Count > 0)
                    {
                        dgRequestor.SelectedIndexes.Add(0);

                    }
                }

            }
        }

        private bool FilterAndSearch(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsDataBind))
            {
                if (!FilterCR)
                {
                    string HomebaseCD = string.Empty;
                    long CQCustomerID = 0;
                    
                    if (chkHomeBaseOnly.Checked == true)  
                        HomebaseCD= UserPrincipal.Identity._airportICAOCd;

                    
                   if ((Request.QueryString["CQCustomerID"] != null && !string.IsNullOrEmpty(Request.QueryString["CQCustomerID"].ToString())) && chkCQCustomerOnly != null && chkCQCustomerOnly.Checked == true)
                        CQCustomerID= Convert.ToInt64(Request.QueryString["CQCustomerID"].ToString());



                    List<PreflightService.GetAllPassengerForPopup> PassengerList = new List<PreflightService.GetAllPassengerForPopup>();
                    using (PreflightService.PreflightServiceClient PassengerService = new PreflightService.PreflightServiceClient())
                    {
                        var PassengerInfo = PassengerService.GetAllPassengerForPopup(chkSearchActiveOnly.Checked, chkRequestorOnly.Checked, HomebaseCD,CQCustomerID);
                        if (PassengerInfo.ReturnFlag == true)
                            PassengerList = PassengerInfo.EntityList;

                        if (PassengerList != null && PassengerList.Count>0)
                        {
                            dgRequestor.DataSource = PassengerList;
                            if (IsDataBind)
                            {
                                dgRequestor.DataBind();
                            }
                        }
                    }
                }
                else
                {
                    if (UserPrincipal.Identity._isSysAdmin == false && UserPrincipal.Identity._travelCoordID != null)
                    {
                        List<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR> PassengerList = new List<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR>();
                        if (Session["CRPassengerList"] != null)
                        {
                            PassengerList = (List<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR>)Session["CRPassengerList"];
                        }
                        if (PassengerList.Count != 0)
                        {
                            if (chkSearchActiveOnly.Checked == true) { PassengerList = PassengerList.Where(x => x.IsActive == true).ToList<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR>(); }
                            if (chkRequestorOnly.Checked == true) { PassengerList = PassengerList.Where(x => x.IsRequestor == true).ToList<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR>(); }
                            if (chkHomeBaseOnly.Checked == true) { PassengerList = PassengerList.Where(x => x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd).ToList<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR>(); }
                            if (!string.IsNullOrEmpty(tbClientCodeFilter.Text))
                            {
                                PassengerList = PassengerList.Where(x => x.ClientCD != null && x.ClientCD.Trim() == tbClientCodeFilter.Text.Trim()).ToList<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR>();
                            }

                            dgRequestor.DataSource = PassengerList;
                            if (IsDataBind)
                            {
                                dgRequestor.DataBind();
                            }
                        }
                    }
                    else
                    {
                        string HomebaseCD = string.Empty;
                        long CQCustomerID = 0;
                    
                        if (chkHomeBaseOnly.Checked == true)  
                            HomebaseCD= UserPrincipal.Identity._airportICAOCd;

                    
                        if ((Request.QueryString["CQCustomerID"] != null && !string.IsNullOrEmpty(Request.QueryString["CQCustomerID"].ToString())) && chkCQCustomerOnly != null && chkCQCustomerOnly.Checked == true)
                            CQCustomerID= Convert.ToInt64(Request.QueryString["CQCustomerID"].ToString());

                        
                        List<PreflightService.GetAllPassengerForPopup> PassengerList = new List<PreflightService.GetAllPassengerForPopup>();
                        using (PreflightService.PreflightServiceClient PassengerService = new PreflightService.PreflightServiceClient())
                        {
                            var PassengerInfo = PassengerService.GetAllPassengerForPopup(chkSearchActiveOnly.Checked, chkRequestorOnly.Checked, HomebaseCD,CQCustomerID);
                            if (PassengerInfo.ReturnFlag == true)
                                PassengerList = PassengerInfo.EntityList;

                            dgRequestor.DataSource = PassengerList;
                            if (IsDataBind)
                            {
                                dgRequestor.DataBind();
                            }
                        }
                    }
                }
                return false;
            }
        }
        protected void Selected_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (rbtnSelectall.Checked == true)
                        {
                            foreach (GridDataItem item in dgRequestor.MasterTableView.Items)
                            {
                                item.Selected = true;
                            }
                        }
                        else
                        {
                            foreach (GridDataItem item in dgRequestor.MasterTableView.Items)
                            {
                                item.Selected = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void Selected1_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (rbtnSelectall.Checked == true)
                        {
                            foreach (GridDataItem item in dgRequestor.MasterTableView.Items)
                            {
                                item.Selected = true;
                            }
                        }
                        else
                        {
                            foreach (GridDataItem item in dgRequestor.MasterTableView.Items)
                            {
                                item.Selected = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        protected void dgRequestor_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgRequestor.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FilterAndSearch(true);
                        return false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }


        protected void ClientCodeFilter_TextChanged(object sender, EventArgs e)
        {
            if (CheckClientFilterCodeExist())
            {
                cvClientCodeFilter.IsValid = false;
                tbClientCodeFilter.Text = "";
                tbClientCodeFilter.Focus();
            }
        }

        private bool CheckClientFilterCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClientCodeFilter.Text.Trim() != string.Empty) && (tbClientCodeFilter.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValueFilter = ClientService.GetClientCodeList().EntityList.Where(x => (x.ClientCD != null));

                            var ClientValue = ClientValueFilter.Where(x => x.ClientCD.Trim().ToUpper().Equals(tbClientCodeFilter.Text.ToString().ToUpper().Trim()));
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                RetVal = true;
                            }
                            else
                            {
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        protected void dgPassengerCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string resolvedurl = string.Empty;
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                GridDataItem item = dgRequestor.SelectedItems[0] as GridDataItem;
                                Session["PassengerRequestorID"] = item["PassengerRequestorID"].Text;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                //catch (System.NullReferenceException ex) { }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgPassengerCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            string PassengerRequestorID = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient();
                            Passenger oPassenger = new Passenger();
                            GridDataItem item = dgRequestor.SelectedItems[0] as GridDataItem;
                            PassengerRequestorID = item.GetDataKeyValue("PassengerRequestorID").ToString();
                            oPassenger.PassengerRequestorID = Convert.ToInt64(item.GetDataKeyValue("PassengerRequestorID").ToString());
                            oPassenger.PassengerRequestorCD = item.GetDataKeyValue("PassengerRequestorCD").ToString();
                            if (item.GetDataKeyValue("IsActive") != null)
                            {
                                if (Convert.ToBoolean(item.GetDataKeyValue("IsActive")) == true)
                                {
                                    string alertMsg = "Active passenger cannot be deleted";
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                    ShowLockAlertPopup(alertMsg, ModuleNameConstants.Database.Passenger);
                                    return;
                                }
                            }
                            oPassenger.IsDeleted = true;
                            var returnValue = CommonService.Lock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(item.GetDataKeyValue("PassengerRequestorID").ToString()));
                            if (!returnValue.ReturnFlag)
                            {
                                e.Item.Selected = true;
                                SelectItem();
                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                                return;
                            }
                            PassengerService.DeletePassengerRequestor(oPassenger);
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            SelectItem();

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(PassengerRequestorID));

                    }
                }
            }
        }

        protected void dgRequestor_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                if (IsUIReports)
                {
                    ((LinkButton)(((GridCommandItem)e.Item).FindControl("lnkInitInsert"))).Visible = false;
                    ((LinkButton)(((GridCommandItem)e.Item).FindControl("lnkDelete"))).Visible = false;
                    ((LinkButton)(((GridCommandItem)e.Item).FindControl("lnkInitEdit"))).Visible = false;
                }
                else
                {
                    ((LinkButton)(((GridCommandItem)e.Item).FindControl("lnkInitInsert"))).Visible = true;
                    ((LinkButton)(((GridCommandItem)e.Item).FindControl("lnkDelete"))).Visible = true;
                    ((LinkButton)(((GridCommandItem)e.Item).FindControl("lnkInitEdit"))).Visible = true;
                }
            }
        }
        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgRequestor_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["PassengerRequestorID"] != null)
                {
                    if (Request.QueryString["PassengerRequestorID"].ToString().Length > 0)
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }




                //if (IsUIReports)
                //{
                //    Param = Request.QueryString["ControlName"];
                //    GridCommandItem cmditm = (GridCommandItem)e.Item;
                //    LinkButton Add = (LinkButton)cmditm.FindControl("lnkInitInsert");
                //    Add.Visible = false;
                //    LinkButton Del = (LinkButton)cmditm.FindControl("lnkDelete");
                //    Del.Visible = false;
                //    LinkButton Edit = (LinkButton)cmditm.FindControl("lnkInitEdit");
                //    Edit.Visible = false;
                //}
                //else
                //{
                //    GridCommandItem cmditm = (GridCommandItem)e.Item;
                //    LinkButton Add = (LinkButton)cmditm.FindControl("lnkInitInsert");
                //    Add.Visible = true;
                //    LinkButton Del = (LinkButton)cmditm.FindControl("lnkDelete");
                //    Del.Visible = true;
                //    LinkButton Edit = (LinkButton)cmditm.FindControl("lnkInitEdit");
                //    Edit.Visible = true;
                //}
            }
        }
    }
}