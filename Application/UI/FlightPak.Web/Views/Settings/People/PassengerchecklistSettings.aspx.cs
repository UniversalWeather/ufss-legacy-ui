﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class PassengerchecklistSettings : BaseSecuredPage
    {
        string CrewCodeValue = "";
        private ExceptionManager exManager;
        Int64 CrewID;
        string CrewChecklistCodeValue = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            lblMessage.Text = string.Empty;
                        }
                        CrewCodeValue = (string)(Session["PassengerRequestorID"]);
                        CrewChecklistCodeValue = (string)(Session["PassengerChecklistCD"]);
                        hdnChecklistCode.Value = (string)(Session["PassengerChecklistCD"]);
                        hdnCrewCode.Value = (string)(Session["PassengerRequestorCD"]);
                        
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Bind Crew Check list Data, Static
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChecklistSettings_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Description", typeof(string));
                        dt.Rows.Add("Original Date");
                        dt.Rows.Add("Freq.");
                        dt.Rows.Add("Previous");
                        dt.Rows.Add("Due Next");
                        dt.Rows.Add("Alert Date");
                        dt.Rows.Add("Alert Days");
                        dt.Rows.Add("Grace Date");
                        dt.Rows.Add("Grace Days");
                        dgChecklistSettings.DataSource = dt;
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Bind Crew Roster Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetAllPassenger> GetAllPax = new List<GetAllPassenger>();
                        GetAllPax = (List<GetAllPassenger>)Session["PassengerList"];
                        dgCrewRoster.DataSource = GetAllPax;
                        foreach (GetAllPassenger CrewVal in GetAllPax)
                        {
                            if (CrewVal.PassengerRequestorCD == CrewCodeValue)
                            {
                                Session["CrewID"] = CrewVal.PassengerRequestorID;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to assign the selected 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OK_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int EntityIndex = -1;
                        int OldEntityIndex = -1;
                        List<string> SelectedChecklistCode = new List<string>();
                        foreach (GridDataItem Item in dgChecklistSettings.MasterTableView.Items)
                        {
                            if (Item.Selected == true)
                            {
                                SelectedChecklistCode.Add(Item.GetDataKeyValue("Description").ToString().Trim());
                            }
                        }

                        List<Int64> SelectedCrewCode = new List<Int64>();
                        foreach (GridDataItem Item in dgCrewRoster.MasterTableView.Items)
                        {
                            if (Item.Selected == true)
                            {
                                SelectedCrewCode.Add(Convert.ToInt64(Item.GetDataKeyValue("PassengerRequestorID").ToString().Trim()));
                            }
                        }

                        int x = SelectedCrewCode.Count();


                        using (FlightPakMasterService.MasterCatalogServiceClient ServiceExisting = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<GetPaxChecklistDate> lstOldCrewChecklistDate = new List<GetPaxChecklistDate>();
                            GetPaxChecklistDate objOldCrewChecklistDate = new GetPaxChecklistDate();

                            objOldCrewChecklistDate.PassengerID = Convert.ToInt64(Session["PassengerRequestorID"].ToString());
                            objOldCrewChecklistDate.PassengerChecklistCD = CrewChecklistCodeValue;
                            objOldCrewChecklistDate.PassengerAdditionalInfoID = Convert.ToInt64(Session["PassengerAdditionalInfoID"]);
                            objOldCrewChecklistDate.PassengerChecklistDescription = "null";
                            var objCrewChecklistOldValues = ServiceExisting.GetPaxCheckListDate(objOldCrewChecklistDate);
                            lstOldCrewChecklistDate = objCrewChecklistOldValues.EntityList.ToList();
                            var objCrewChecklistOldValue = (from code in lstOldCrewChecklistDate
                                                            where (code.PassengerAdditionalInfoID.Equals(Convert.ToInt64(Session["PassengerAdditionalInfoID"])))
                                                            select code);

                            for (int y = 0; y < objCrewChecklistOldValues.EntityList.Count(); y++)
                            {
                                if (objCrewChecklistOldValues.EntityList[y].PassengerAdditionalInfoID == Convert.ToInt64(Session["PassengerAdditionalInfoID"]))
                                {
                                    OldEntityIndex = y;
                                }
                            }

                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<GetPaxChecklistDate> lstCrewChecklistDate = new List<GetPaxChecklistDate>();
                                GetPaxChecklistDate objCrewChecklistDate = new GetPaxChecklistDate();

                                if (OldEntityIndex > -1)
                                {
                                    for (int CrewCode = 0; CrewCode < SelectedCrewCode.Count(); CrewCode++)
                                    {
                                        EntityIndex = -1;
                                        objCrewChecklistDate.PassengerID = SelectedCrewCode[CrewCode];
                                        objCrewChecklistDate.PassengerChecklistCD = CrewChecklistCodeValue;
                                        objCrewChecklistDate.PassengerAdditionalInfoID = Convert.ToInt64(Session["PassengerAdditionalInfoID"]);



                                        objCrewChecklistDate.PassengerChecklistDescription = "null";
                                        var objCrewChecklistValues = Service.GetPaxCheckListDate(objCrewChecklistDate);
                                        if (objCrewChecklistValues.ReturnFlag == true)
                                        {
                                            lstCrewChecklistDate = objCrewChecklistValues.EntityList.ToList();
                                            for (int y = 0; y < objCrewChecklistValues.EntityList.Count(); y++)
                                            {
                                                if (objCrewChecklistValues.EntityList[y].PassengerChecklistCD == CrewChecklistCodeValue)
                                                {
                                                    EntityIndex = y;
                                                }
                                            }
                                        }

                                        if (EntityIndex > -1)
                                        {
                                            PassengerCheckListDetail CrewChecklistDate = new PassengerCheckListDetail();
                                            CrewChecklistDate.PassengerID = SelectedCrewCode[CrewCode];
                                            CrewChecklistDate.PassengerAdditionalInfoID = objCrewChecklistValues.EntityList[EntityIndex].PassengerAdditionalInfoID;
                                            CrewChecklistDate.PassengerCheckListDetailID = objCrewChecklistValues.EntityList[EntityIndex].PassengerCheckListDetailID;
                                            if (SelectedChecklistCode.Contains("Previous"))
                                            {
                                                CrewChecklistDate.PreviousCheckDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].PreviousCheckDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.PreviousCheckDT = objCrewChecklistValues.EntityList[EntityIndex].PreviousCheckDT;
                                            }
                                            if (SelectedChecklistCode.Contains("Due Next"))
                                            {
                                                CrewChecklistDate.DueDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].DueDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.DueDT = objCrewChecklistValues.EntityList[EntityIndex].DueDT;
                                            }
                                            if (SelectedChecklistCode.Contains("Alert Date"))
                                            {
                                                CrewChecklistDate.AlertDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].AlertDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.AlertDT = objCrewChecklistValues.EntityList[EntityIndex].AlertDT;
                                            }
                                            if (SelectedChecklistCode.Contains("Grace Date"))
                                            {
                                                CrewChecklistDate.GraceDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].GraceDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.GraceDT = objCrewChecklistValues.EntityList[EntityIndex].GraceDT;
                                            }
                                            if (SelectedChecklistCode.Contains("Grace Days"))
                                            {
                                                CrewChecklistDate.GraceDays = objCrewChecklistOldValues.EntityList[OldEntityIndex].GraceDays;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.GraceDays = objCrewChecklistValues.EntityList[EntityIndex].GraceDays;
                                            }
                                            if (SelectedChecklistCode.Contains("Alert Days"))
                                            {
                                                CrewChecklistDate.AlertDays = objCrewChecklistOldValues.EntityList[OldEntityIndex].AlertDays;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.AlertDays = objCrewChecklistValues.EntityList[EntityIndex].AlertDays;
                                            }

                                            if (SelectedChecklistCode.Contains("Freq."))
                                            {
                                                CrewChecklistDate.FrequencyMonth = objCrewChecklistOldValues.EntityList[OldEntityIndex].FrequencyMonth;
                                                CrewChecklistDate.Frequency = objCrewChecklistOldValues.EntityList[OldEntityIndex].Frequency;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.FrequencyMonth = objCrewChecklistValues.EntityList[EntityIndex].FrequencyMonth;
                                                CrewChecklistDate.Frequency = objCrewChecklistOldValues.EntityList[EntityIndex].Frequency;
                                            }

                                            if (SelectedChecklistCode.Contains("Original Date"))
                                            {
                                                CrewChecklistDate.OriginalDT = objCrewChecklistOldValues.EntityList[OldEntityIndex].OriginalDT;
                                            }
                                            else
                                            {
                                                CrewChecklistDate.OriginalDT = objCrewChecklistValues.EntityList[EntityIndex].OriginalDT;
                                            }

                                            CrewChecklistDate.IsDeleted = false;

                                            CrewChecklistDate.IsPassedDueAlert = false;

                                            CrewChecklistDate.IsInActive = false;
                                            CrewChecklistDate.IsMonthEnd = false;
                                            CrewChecklistDate.IsStopCALC = false;
                                            CrewChecklistDate.IsOneTimeEvent = false;
                                            CrewChecklistDate.IsCompleted = false;
                                            CrewChecklistDate.IsNoConflictEvent = false;

                                            CrewChecklistDate.IsNoChecklistREPT = false;
                                            CrewChecklistDate.IsPrintStatus = false;
                                            CrewChecklistDate.IsNextMonth = false;
                                            CrewChecklistDate.IsEndCalendarYear = false;

                                            Service.UpdatePaxCheckListDate(CrewChecklistDate);
                                        }
                                    }
                                }
                            }
                            
                        }
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    
                }
                InjectScript.Visible = true;
                InjectScript.Text = "<script type='text/javascript'>Close()</" + "script>";
                Session.Remove("PassengerChecklistCD");
                Session.Remove("PassengerAdditionalInfoID");
            }

        }
    }
}