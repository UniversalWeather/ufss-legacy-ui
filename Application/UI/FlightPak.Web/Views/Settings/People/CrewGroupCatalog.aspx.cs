﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Collections;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewGroupCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private bool CrewGroupPageNavigated = false;
        Int64 customerID;
        private string strCrewGroupTypeID = "";
        private string strCrewGroupTypeCD = "";
        private bool str = false;
        private bool _selectLastModified = false;
        Int64 CrewGroupID;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrewGroup, dgCrewGroup, this.Page.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                        //store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCrewGroup.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewCrewGroupCatalog);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgCrewGroup.Rebind();                                
                                ReadOnlyForm();
                                LoadSelectedList();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// To select the selected Item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewGroupID"] != null)
                    {
                        string ID = Session["SelectedCrewGroupID"].ToString();
                        foreach (GridDataItem item in dgCrewGroup.MasterTableView.Items)
                        {
                            if (item["CrewGroupID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Prerender method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewGroup_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEnable(true, true, true);
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CrewGroupPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewGroup, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>                
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgCrewGroup.Rebind();
                    }
                    if (dgCrewGroup.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedCrewGroupID"] = null;
                        //}
                        if (Session["SelectedCrewGroupID"] == null)
                        {
                            dgCrewGroup.SelectedIndexes.Add(0);
                            Session["SelectedCrewGroupID"] = dgCrewGroup.Items[0].GetDataKeyValue("CrewGroupID").ToString();
                        }

                        if (dgCrewGroup.SelectedIndexes.Count == 0)
                            dgCrewGroup.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        LoadSelectedList();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnDelete = (LinkButton)dgCrewGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1 || e.Initiator.ID.IndexOf("lbtnDelete", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCrewGroup;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewGroup_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Bind Crew Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgCrewGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objCrewGroupVal = CrewService.GetCrewGroupInfo();
                            List<FlightPakMasterService.GetAllCrewGroup> lstCrewGroup = new List<FlightPakMasterService.GetAllCrewGroup>();
                            if (objCrewGroupVal.ReturnFlag == true)
                            {
                                lstCrewGroup = objCrewGroupVal.EntityList.ToList();
                                //dgCrewGroup.DataSource = objCrewGroupVal.EntityList.Where(x => x.IsInActive == false);
                            }
                            dgCrewGroup.DataSource = lstCrewGroup;
                            Session["CrewGroupCodes"] = lstCrewGroup;
                            if (chkSearchActiveOnly.Checked == true)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// Item Command for Crew Group Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnHomeBase.Value = string.Empty;
                                hdnSave.Value = "Update";
                                if (Session["SelectedCrewGroupID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CrewGroup, Convert.ToInt64(Session["SelectedCrewGroupID"].ToString().Trim()));
                                        Session["IsCrewGroupEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewGroup);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        SelectItem();
                                        tbCode.Enabled = false;
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgCrewGroup.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                BindAvailableList();
                                GridEnable(true, false, false);
                                lstSelected.Items.Clear();
                                tbCode.Focus();
                                DisableLinks();
                                break;
                            case "Filter":
                                foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// Update Command for Crew Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewGroup_UpdateCommand(object source, GridCommandEventArgs e)
        {
            e.Canceled = true;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedCrewGroupID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                //GridDataItem item = (GridDataItem)Session["SelectedItem"];
                                var RetVal = CrewService.UpdateCrewType(GetFormItems());
                                if (RetVal.ReturnFlag == true)
                                {
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    //////Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.CrewGroup, Convert.ToInt64(Session["SelectedCrewGroupID"].ToString().Trim()));
                                        SaveCrewOrders(Convert.ToInt64(Session["SelectedCrewGroupID"].ToString().Trim()));
                                        LoadSelectedList();
                                        GridEnable(true, true, true);
                                        DefaultSelection(false);
                                        dgCrewGroup.Rebind();
                                    }

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    ShowHideControls(true);
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(RetVal.ErrorMessage, ModuleNameConstants.Database.CrewGroup);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// changing the page index on Paging
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewGroup_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgCrewGroup.ClientSettings.Scrolling.ScrollTop = "0";
            CrewGroupPageNavigated = true;
        }
        /// <summary>
        /// Insert Command for Crew Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        /// <summary>
        /// Insert Command for Crew Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewGroup_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.CrewGroup CrewGroup = new FlightPakMasterService.CrewGroup();
                                CrewGroup.CrewGroupCD = tbCode.Text;
                                CrewGroup.CrewGroupDescription = tbDescription.Text;
                                if (!string.IsNullOrEmpty(hdnHomeBase.Value))
                                {
                                    CrewGroup.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                                }
                                CrewGroup.IsDeleted = false;
                                CrewGroup.IsInActive = chkCrewInactive.Checked;
                                var RetVal = CrewService.AddCrewType(CrewGroup);
                                if (RetVal.ReturnFlag == true)
                                {
                                    //var CrewGroupID;
                                    
                                    FlightPakMasterService.MasterCatalogServiceClient CrewService1 = new FlightPakMasterService.MasterCatalogServiceClient();
                                    var Results = CrewService1.GetCrewGroupInfo().EntityList.Where(x => x.CrewGroupCD.Trim().ToUpper() == (tbCode.Text.ToString().ToUpper().Trim()) && x.IsInActive == false);
                                    foreach (var item in Results)
                                    {
                                        CrewGroupID = item.CrewGroupID;
                                    }

                                    SaveCrewOrders(CrewGroupID);
                                    GridEnable(true, true, true);
                                    DefaultSelection(false);
                                    dgCrewGroup.Rebind();

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    ShowHideControls(true);
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(RetVal.ErrorMessage, ModuleNameConstants.Database.CrewGroup);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewGroup_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedCrewGroupID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CrewGroupTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.CrewGroup CrewGroup = new FlightPakMasterService.CrewGroup();
                                string Code = Session["SelectedCrewGroupID"].ToString();
                                strCrewGroupTypeCD = "";
                                strCrewGroupTypeID = "";
                                string strDescription = "";
                                foreach (GridDataItem Item in dgCrewGroup.MasterTableView.Items)
                                {
                                    if (Item["CrewGroupID"].Text.Trim() == Code.Trim())
                                    {
                                        strCrewGroupTypeCD = Item["CrewGroupCD"].Text.Trim();
                                        strCrewGroupTypeID = Item["CrewGroupID"].Text.Trim();
                                        strDescription = Item["CrewGroupDescription"].Text.Trim();
                                        break;
                                    }
                                }
                                CrewGroup.CrewGroupCD = strCrewGroupTypeCD;
                                CrewGroup.CrewGroupID = Convert.ToInt64(strCrewGroupTypeID);
                                CrewGroup.CrewGroupDescription = strDescription;
                                CrewGroup.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.CrewGroup, Convert.ToInt64(strCrewGroupTypeID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewGroup);
                                        return;
                                    }
                                    CrewGroupTypeService.DeleteCrewType(CrewGroup);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                   
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.CrewGroup, Convert.ToInt64(strCrewGroupTypeID));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgCrewGroup.SelectedItems[0] as GridDataItem;

                                Session["SelectedCrewGroupID"] = item["CrewGroupID"].Text;
                                ReadOnlyForm();
                                LoadSelectedList();
                                GridEnable(true, true, true);
                                ShowHideControls(true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                    }
                }
            }
        }
        /// <summary>
        /// Load Selected List Items in the Listbox
        /// </summary>
        protected void LoadSelectedList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    foreach (GridDataItem item in dgCrewGroup.MasterTableView.Items)
                    {
                        if (item.Selected)
                        {
                            Session["CrewGroupID"] = item["CrewGroupID"].Text;
                            item.Selected = true;
                        }
                    }
                    lstAvailable.Items.Clear();
                    lstSelected.Items.Clear();
                    List<FlightPakMasterService.Crew> CrewAvailableList = new List<FlightPakMasterService.Crew>();
                    List<FlightPakMasterService.Crew> CrewSelectedList = new List<FlightPakMasterService.Crew>();
                    List<object> TempAvlList = new List<object>();
                    List<object> TempSelList = new List<object>();
                    ArrayList crewGroupArray = new ArrayList();
                    CrewAvailableList = GetAvailableListByCode(customerID, Convert.ToInt64(Session["CrewGroupID"]));
                   
                    RadListBoxItem lstItem;
                    if (CrewAvailableList != null)
                    {
                        //CrewAvailableList = CrewAvailableList.OrderBy(x => x.LastName).ThenBy(n => n.FirstName).ThenBy(y => y.CrewCD).ToList();
                        foreach (FlightPakMasterService.Crew crewList in CrewAvailableList)
                        {
                            lstItem = new RadListBoxItem();
                            //lstItem.Text = (crewList.LastName + "," + crewList.FirstName + " " +crewList.MiddleInitial + "-(" + crewList.CrewCD + ")");
                            lstItem.Text = (crewList.FirstName + " " + crewList.MiddleInitial + " " + crewList.LastName + " - (" + crewList.CrewCD + ")");
                            lstItem.Value = crewList.CrewID.ToString().Trim();
                            lstAvailable.Items.Add(lstItem);
                        }
                    }
                    Session["CrewGroupArray"] = crewGroupArray;
                    CrewSelectedList = GetSelectedListByCode(customerID, Convert.ToInt64(Session["CrewGroupID"]));
                    if (CrewSelectedList != null)
                    {
                        //CrewSelectedList = CrewSelectedList.OrderBy(x => x.LastName).ThenBy(n => n.FirstName).ThenBy(y => y.CrewCD).ToList();
                        foreach (FlightPakMasterService.Crew dSelList in CrewSelectedList)
                        {
                            lstItem = new RadListBoxItem();
                            lstItem.Text = (dSelList.FirstName + " " + dSelList.MiddleInitial + " " + dSelList.LastName + " - (" + dSelList.CrewCD + ")");
                            //lstItem.Text = (dSelList.LastName + "," + dSelList.FirstName + " " + dSelList.MiddleInitial + "-(" + dSelList.CrewCD + ")");
                            lstItem.Value = dSelList.CrewID.ToString().Trim();
                            lstSelected.Items.Add(lstItem);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportICAOCd.Trim() != string.Empty)
                    {
                        tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd.ToString().Trim();
                        if (UserPrincipal != null && UserPrincipal.Identity._homeBaseId != 0)
                        {
                            hdnHomeBase.Value = Convert.ToString(UserPrincipal.Identity._homeBaseId);
                        }
                    }
                    else
                    {
                        tbHomeBase.Text = string.Empty;

                    }
                    EnableForm(true);
                    if (hdnSave.Value == "Save")
                        ShowHideControls(true);
                    else
                        ShowHideControls(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Edit Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();

                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    ShowHideControls(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void BindAvailableList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.Crew> CrewAvailableList = new List<FlightPakMasterService.Crew>();
                    CrewAvailableList = GetAvailableListByCode(customerID, Convert.ToInt64(Session["CrewGroupID"]));
                    RadListBoxItem lstItem;
                    if (CrewAvailableList != null)
                    {
                        foreach (FlightPakMasterService.Crew crewList in CrewAvailableList)
                        {
                            lstItem = new RadListBoxItem();
                            lstItem.Text = (crewList.FirstName + " " + crewList.MiddleInitial + " " + crewList.LastName + " - (" + crewList.CrewCD + ")");
                            lstItem.Value = crewList.CrewID.ToString().Trim();
                            lstAvailable.Items.Add(lstItem);
                        }
                    }                   
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Crew Group Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgCrewGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgCrewGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// Cancel Crew Group Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedCrewGroupID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.CrewGroup, Convert.ToInt64(Session["SelectedCrewGroupID"].ToString().Trim()));
                                }
                            }
                        }
                        //Session["SelectedItem"] = null;
                        DefaultSelection(false);
                        ShowHideControls(true);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// Function to Check if Passenger Group Code Alerady Exists
        /// </summary>
        /// <returns></returns>
        private Boolean CheckAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    bool ReturnValue = false;
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
                        {
                            var Results = CrewService.GetCrewGroupInfo().EntityList.Where(x => x.CrewGroupCD.Trim().ToUpper() == (tbCode.Text.ToString().ToUpper().Trim()) && x.IsInActive == false);
                            if (Results.Count() > 0 && Results != null)
                            {
                                cvCode.IsValid = false;
                                tbCode.Focus();
                                ReturnValue = true;
                            }
                        }
                        else
                        {
                            ReturnValue = false;
                        }
                        return ReturnValue;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    return ReturnValue;
                }
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtninsertCtl, lbtndelCtl, lbtneditCtl;
                    lbtninsertCtl = (LinkButton)dgCrewGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    lbtndelCtl = (LinkButton)dgCrewGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    lbtneditCtl = (LinkButton)dgCrewGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddCrewGroupCatalog))
                    {
                        lbtninsertCtl.Visible = true;
                        if (add)
                        {
                            lbtninsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtninsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtninsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteCrewGroupCatalog))
                    {
                        lbtndelCtl.Visible = true;
                        if (delete)
                        {
                            lbtndelCtl.Enabled = true;
                            lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtndelCtl.Enabled = false;
                            lbtndelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtndelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditCrewGroupCatalog))
                    {
                        lbtneditCtl.Visible = true;
                        if (edit)
                        {
                            lbtneditCtl.Enabled = true;
                            lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtneditCtl.Enabled = false;
                            lbtneditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtneditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Function to Enable / Diable the form fields
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = Enable;
                    tbDescription.Enabled = Enable;
                    tbHomeBase.Enabled = Enable;
                    lstAvailable.Enabled = Enable;
                    tbAvailableFilter.Enabled = Enable;
                    lstSelected.Enabled = Enable;
                    btnHomeBase.Enabled = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChanges.Visible = Enable;
                    //btnNext.Enabled = Enable;
                    //btnPrevious.Enabled = Enable;
                    //btnFirst.Enabled = Enable;
                    //btnLast.Enabled = Enable;
                    chkCrewInactive.Enabled = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Function to Get Form Items
        /// </summary>
        /// <returns></returns>
        private FlightPakMasterService.CrewGroup GetFormItems()
        {
            FlightPakMasterService.CrewGroup CrewGroup = new FlightPakMasterService.CrewGroup();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    CrewGroup.CrewGroupCD = tbCode.Text;
                    CrewGroup.CrewGroupDescription = tbDescription.Text;
                    if (!string.IsNullOrEmpty(hdnHomeBase.Value))
                    {
                        CrewGroup.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                    }
                    foreach (GridDataItem Item in dgCrewGroup.MasterTableView.Items)
                    {
                        if (Item["CrewGroupID"].Text.Trim() == Session["SelectedCrewGroupID"].ToString().Trim())
                        {
                            CrewGroup.CrewGroupID = Convert.ToInt64(Item["CrewGroupID"].Text.Trim());
                            break;
                        }
                    }
                    CrewGroup.IsDeleted = false;
                    CrewGroup.IsInActive = chkCrewInactive.Checked;
                    return CrewGroup;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return CrewGroup;
            }
        }
        /// <summary>
        ///  Delete from CrewGroupOrder Table, based on CreWCode and CrewGroupCode
        /// </summary>
        /// <param name="CrewCode"></param>
        /// <param name="CrewGroupCode"></param>
        protected void DeleteSeletedItem(string CrewCode, string CrewGroupCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewCode, CrewGroupCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        FlightPakMasterService.CrewGroupOrder CrewGroupOrder = new FlightPakMasterService.CrewGroupOrder();
                        if (CrewGroupCode != null && CrewGroupCode != string.Empty)
                        {
                            CrewGroupOrder.CrewGroupOrderID = Convert.ToInt64(CrewGroupCode);
                        }
                        else
                        {
                            CrewGroupOrder.CrewGroupOrderID = 0;
                        }
                        CrewGroupOrder.CrewGroupID = Convert.ToInt64(CrewCode);
                        CrewService.DeleteCrewGroupOrderType(CrewGroupOrder);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Get Available List by CrewCode
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <param name="CrewCode"></param>
        /// <returns></returns>
        public List<FlightPakMasterService.Crew> GetAvailableListByCode(Int64 CustomerID, Int64 CrewCode)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var ReturnValue = CrewService.GetcrewavailableList(CustomerID, CrewCode);
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ReturnValue != null)
                        {
                            return ReturnValue.EntityList.ToList();
                        }
                        return null;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    return ReturnValue.EntityList.ToList();
                }
            }
        }
        /// <summary>
        /// To Get Selected List by CrewCode
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <param name="CrewCode"></param>
        /// <returns></returns>
        public List<FlightPakMasterService.Crew> GetSelectedListByCode(Int64 CustomerID, Int64 CrewCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, CrewCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ReturnValue = CrewService.GetcrewselectedList(CustomerID, CrewCode);
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ReturnValue != null)
                        {
                            return ReturnValue.EntityList.ToList();
                        }
                        return null;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    return ReturnValue.EntityList.ToList();
                }
            }
        }
        /// <summary>
        /// SaveCrewOrders
        /// </summary>
        /// <param name="CrewGroupID"></param>
        protected void SaveCrewOrders(Int64 CrewGroupID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewGroupID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        FlightPakMasterService.CrewGroupOrder CrewGroupOrder = new FlightPakMasterService.CrewGroupOrder();
                        DeleteSeletedItem(Session["CrewGroupID"].ToString().Trim(), string.Empty);
                        string SelectedValue = string.Empty;
                        for (int Index = 0; Index < lstSelected.Items.Count; Index++)
                        {
                            CrewGroupOrder.CrewGroupID = Convert.ToInt64(CrewGroupID.ToString());
                            CrewGroupOrder.OrderNum = Index;
                            CrewGroupOrder.CrewID = Convert.ToInt64(lstSelected.Items[Index].Value);
                            var ReturnValue = CrewService.AddCrewGroupOrderType(CrewGroupOrder);
                        }
                        dgCrewGroup.Rebind();
                        GridEnable(true, true, true);
                        if(hdnSave.Value != "Save")
                        DisplayInsertForm();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>        
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    GridDataItem Item = dgCrewGroup.SelectedItems[0] as GridDataItem;
                    Label lbLastUpdatedUser = (Label)dgCrewGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Load Selected Item Data into the Form Fields
        /// </summary>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewGroupID"] != null)
                    {
                        foreach (GridDataItem Item in dgCrewGroup.MasterTableView.Items)
                        {
                            if (Item["CrewGroupID"].Text.Trim() == Session["SelectedCrewGroupID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("CrewGroupCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("CrewGroupCD").ToString().Trim();
                                }
                                else
                                {
                                    tbCode.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CrewGroupID") != null)
                                {
                                    strCrewGroupTypeID = Item.GetDataKeyValue("CrewGroupID").ToString().Trim();
                                }
                                else
                                {
                                    strCrewGroupTypeID = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CrewGroupDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("CrewGroupDescription").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HomebaseCD") != null)
                                {
                                    tbHomeBase.Text = Item.GetDataKeyValue("HomebaseCD").ToString().Trim();
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HomebaseID") != null)
                                {
                                    hdnHomeBase.Value = Item.GetDataKeyValue("HomebaseID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null && Item.GetDataKeyValue("IsInActive").ToString().ToUpper().Trim() == "TRUE")
                                {
                                    chkCrewInactive.Checked = true;
                                }
                                else
                                {
                                    chkCrewInactive.Checked = false;
                                }

                                lbColumnName1.Text = "Crew Group Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["CrewGroupCD"].Text;
                                lbColumnValue2.Text = Item["CrewGroupDescription"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To move a single record from available list to selected list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNext_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lstAvailable.SelectedItem != null)
                        {
                            string SelectedValue = string.Empty;
                            ArrayList SelectedArrayList = new ArrayList();
                            for (int Index = lstAvailable.Items.Count - 1; Index >= 0; Index--)
                            //for (int Index = 0; Index <= lstAvailable.Items.Count - 1; Index++)
                            {
                                if (lstAvailable.Items[Index].Selected)
                                {
                                    //SelectedValue = lstAvailable.Items[Index].ToString();
                                    SelectedValue = lstAvailable.Items[Index].Text;
                                    ArrayList TempSelectedList = new ArrayList(SelectedValue.Split(new System.Char[] { '-' }));
                                    SelectedArrayList.Add(TempSelectedList[1].ToString().Trim().Replace("(", string.Empty).Replace(")", string.Empty));
                                    lstSelected.Items.Add(lstAvailable.Items[Index]);
                                    lstAvailable.Items.Remove(lstAvailable.Items[Index]);
                                }
                            }
                            lstSelected.SelectedIndex = -1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// To move a single record from selected list to available list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrevious_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lstSelected.SelectedItem != null)
                        {
                            string SelectedValue = string.Empty;
                            ArrayList SelectedArrayList = new ArrayList();
                            for (int Index = lstSelected.Items.Count - 1; Index >= 0; Index--)
                           // for (int Index = 0; Index <= lstSelected.Items.Count - 1; Index++)
                            {
                                if (lstSelected.Items[Index].Selected)
                                {
                                    //SelectedValue = lstSelected.Items[Index].ToString();
                                    SelectedValue = lstSelected.Items[Index].Text;
                                    ArrayList TempSelectedList = new ArrayList(SelectedValue.Split(new System.Char[] { '-' }));
                                    SelectedArrayList.Add(TempSelectedList[1].ToString().Trim().Replace("(", string.Empty).Replace(")", string.Empty));
                                    lstAvailable.Items.Add(lstSelected.Items[Index]);
                                    lstSelected.Items.Remove(lstSelected.Items[Index]);
                                }
                            }
                            lstAvailable.SelectedIndex = -1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// To move all records from available list to selected list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLast_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ArrayList SelectedArrayList = new ArrayList();
                        for (int Index = lstAvailable.Items.Count - 1; Index >= 0; Index--)
                       // for (int Index = 0; Index <= lstAvailable.Items.Count - 1; Index++)
                        {
                            //string SelectedValue = lstAvailable.Items[Index].ToString();
                            string SelectedValue = lstAvailable.Items[Index].Text;
                            ArrayList TempSelectedArrayList = new ArrayList(SelectedValue.Split(new System.Char[] { '-' }));
                            SelectedArrayList.Add(TempSelectedArrayList[1].ToString().Trim().Replace("(", string.Empty).Replace(")", string.Empty));
                            
                            foreach (RadListBoxItem item in SelectedArrayList)
                            {
                                lstAvailable.Items.Remove(item);
                                lstSelected.Items.Add(item);
                            } 

                            //lstSelected.Items.Add(lstAvailable.Items[Index]);
                            //lstAvailable.Items.Remove(lstAvailable.Items[Index]);
                        }
                        lstSelected.SelectedIndex = -1;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// To move all records from selected list to available list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFirst_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ArrayList SelectedArrayList = new ArrayList();
                        for (int Index = lstSelected.Items.Count - 1; Index >= 0; Index--)
                        //for (int Index = 0; Index <= lstSelected.Items.Count - 1; Index++)
                        {
                            //string SelectedValue = lstSelected.Items[Index].ToString();
                            string SelectedValue = lstSelected.Items[Index].Text;
                            ArrayList TempSelectedArrayList = new ArrayList(SelectedValue.Split(new System.Char[] { '-' }));
                            SelectedArrayList.Add(TempSelectedArrayList[1].ToString().Trim().Replace("(", string.Empty).Replace(")", string.Empty));
                            lstAvailable.Items.Add(lstSelected.Items[Index]);
                            lstSelected.Items.Remove(lstSelected.Items[Index]);
                        }
                        lstAvailable.SelectedIndex = -1;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// To Clear the Form
        /// </summary>        
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    hdnHomeBase.Value = string.Empty;
                    lstAvailable.Items.Clear();
                    tbAvailableFilter.Text = string.Empty;
                    lstSelected.Items.Clear();
                    chkCrewInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
                        {
                            if (CheckAllReadyExist())
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// To check Valid Home Base
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbHomeBase.Text != null && tbHomeBase.Text.Trim() != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CrewGroupService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ReturnValue = CrewGroupService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.Trim().ToUpper() == (tbHomeBase.Text.Trim().ToUpper())).ToList();
                                if (ReturnValue.Count() > 0 && ReturnValue != null)
                                {
                                    hdnHomeBase.Value = ((FlightPakMasterService.GetAllCompanyMaster)ReturnValue[0]).HomebaseID.ToString().Trim();
                                    tbHomeBase.Text = ((FlightPakMasterService.GetAllCompanyMaster)ReturnValue[0]).HomebaseCD;
                                }
                                else
                                {
                                    cvHomeBase.IsValid = false;
                                    tbHomeBase.Focus();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }

        /// <summary>
        /// Method to show and hide controls
        /// </summary>
        /// <param name="Visibility"></param>
        private void ShowHideControls(bool Visibility)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Visibility))
            {
                lstAvailable.Visible = Visibility;
                tbAvailableFilter.Visible = Visibility;
                lstSelected.Visible = Visibility;
                //btnNext.Visible = Visibility;
                //btnPrevious.Visible = Visibility;
                //btnLast.Visible = Visibility;
                //btnFirst.Visible = Visibility;
                lbOrderDetails.Visible = Visibility;
                lbPaxAvailable.Visible = Visibility;
                lbPaxSelected.Visible = Visibility;
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllCrewGroup> lstCrewGroup = new List<FlightPakMasterService.GetAllCrewGroup>();
                if (Session["CrewGroupCodes"] != null)
                {
                    lstCrewGroup = (List<FlightPakMasterService.GetAllCrewGroup>)Session["CrewGroupCodes"];
                }
                if (lstCrewGroup.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstCrewGroup = lstCrewGroup.Where(x => x.IsInActive == false).ToList<GetAllCrewGroup>(); }
                    else
                    {
                        lstCrewGroup = lstCrewGroup.ToList<GetAllCrewGroup>();
                    }

                    dgCrewGroup.DataSource = lstCrewGroup;
                    if (IsDataBind)
                    {
                        dgCrewGroup.DataBind();
                    }
                }
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCrewGroup.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = -1;

            var CrewGroupValue = FPKMstService.GetCrewGroupInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CrewGroupValue);
            List<FlightPakMasterService.GetAllCrewGroup> filteredList = GetFilteredList(CrewGroupValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CrewGroupID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            if (FullItemIndex < 0 && filteredList.Count > 0)
            {
                PrimaryKeyValue = filteredList[0].CrewGroupID;
                Session["SelectedCrewGroupID"] = PrimaryKeyValue;
                FullItemIndex = 0;
            }

            int PageSize = dgCrewGroup.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCrewGroup.CurrentPageIndex = PageNumber;
            dgCrewGroup.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllCrewGroup CrewGroupValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedCrewGroupID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CrewGroupValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().CrewGroupID;
                Session["SelectedCrewGroupID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllCrewGroup> GetFilteredList(ReturnValueOfGetAllCrewGroup CrewGroupValue)
        {
            List<FlightPakMasterService.GetAllCrewGroup> filteredList = new List<FlightPakMasterService.GetAllCrewGroup>();

            if (CrewGroupValue.ReturnFlag)
            {
                filteredList = CrewGroupValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetAllCrewGroup>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCrewGroup.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    //dgCrewGroup.Rebind();
                    SelectItem();
                    DefaultSelection(false);
                    //ReadOnlyForm();
                }
            }
        }
    }
}