﻿﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Crew Group</title>

        <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
        <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
        <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
        <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="/Scripts/Common.js"></script>
        <script type="text/javascript">

            var jqgridTableId = '#gridCrewGroups';
            var selectedItems = [];
            var ismultiSelect = false;
            $(document).ready(function () {
                ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;

                $.extend(jQuery.jgrid.defaults, {
                    prmNames: {
                        page: "page", rows: "size", order: "dir", sort: "sort"
                    }
                });

                selectedItems = getSelectedItems('CrewGroupCD', '');

                $("#btnSubmit").click(function () {
                    var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                    if (selr != null) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData);
                    }
                    else {
                        showMessageBox('Please select a crew group.', popupTitle);
                    }
                    return false;
                });

                $("#btnActiveOnly").click(function () {
                    $(jqgridTableId).trigger('reloadGrid');
                });

                jQuery(jqgridTableId).jqGrid({
                    url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                    mtype: 'GET',
                    datatype: "json",
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                    serializeGridData: function (postData) {
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }
                        postData.apiType = 'fss';
                        postData.method = 'crewGroups';
                        postData.crewGroupid = 0;
                        postData.activeOnly = $("#chkActiveOnly").prop('checked');
                        return postData;
                    },
                    height: 254,
                    width: 530,
                    viewrecords: true,
                    rowNum: $("#rowNum").val(),
                    multiselect: ismultiSelect,
                    pager: "#pg_gridPager",
                    colNames: ['CrewGroupID', 'Code', 'Name', 'Home Base'],
                    colModel: [
                        { name: 'CrewGroupID', index: 'CrewGroupID', key: true, hidden: true },
                        { name: 'CrewGroupCD', index: 'CrewGroupCD', width: 100, fixed: true},
                        { name: 'CrewGroupDescription', index: 'CrewGroupDescription', width: 311, fixed: true},
                        { name: 'HomebaseCD', index: 'IcaoId', width: 80, fixed: true}
                    ],
                    ondblClickRow: function (rowId) {
                        var rowData = jQuery(this).getRowData(rowId);
                        updateIdsOfSelectedRows(rowId, false, rowData);
                        returnToParent(rowData);
                    },
                    onSelectRow: function (id, status) {
                        var rowData = $(this).getRowData(id);
                        var lastSel = rowData['CrewGroupID'];//replace name with any column

                        if (id !== lastSel) {
                            $(this).find(".selected").removeClass('selected');
                            $('#results_table').jqGrid('resetSelection', lastSel, true);
                            $(this).find('.ui-state-highlight').addClass('selected');
                            lastSel = id;
                        }

                        updateIdsOfSelectedRows(id, status, rowData);
                    },
                    onSelectAll: function (aRowids, status) {
                        var i, count, id;
                        for (i = 0, count = aRowids.length; i < count; i++) {
                            id = aRowids[i];
                            var rowData = $(this).getRowData(id);
                            updateIdsOfSelectedRows(id, status, rowData);
                        }
                    },
                    afterInsertRow: function (rowid, rowdata, rowelem) {
                        if (idsOfSelectedRows.contains(rowid)) {
                                $(this).jqGrid('setSelection', rowid, true);
                        } else {
                            for (var i = 0; i < selectedItems.length; i++) {
                                if ($.trim(selectedItems[i]) == $.trim(rowelem.CrewGroupCD)) {
                                    updateIdsOfSelectedRows(rowid, true, rowelem);
                                    $(this).jqGrid('setSelection', rowid, true);
                                    break;
                                }
                            }
                        }
                    }
                });
                $("#pagesizebox").insertBefore('.ui-paging-info');
                $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'cn', searchOnEnter: false, stringResult: true });

            });

            function returnToParent(rowData) {
                var crewGroupCD = '';
                if (ismultiSelect) {
                    for (var i = 0; i < selectedObjects.length; i++) {
                        if (i == 0) {
                            crewGroupCD = $.trim(selectedObjects[i].CrewGroupCD);
                        } else {
                            crewGroupCD += "," + $.trim(selectedObjects[i].CrewGroupCD);
                        }
                    }
                }
                else {
                    crewGroupCD = $.trim(rowData["CrewGroupCD"]);
                }

                var oArg = new Object();
                oArg.CrewGroupID = $.trim(rowData["CrewGroupID"]);
                oArg.CrewGroupCD = crewGroupCD;
                oArg.Arg1 = crewGroupCD;
                oArg.CallingButton = "CrewGroupCD";
                var oWnd = GetRadWindow();
                if (oArg) {
                    parent.radCrewRosterPopup.document.getElementById('errorMsg1').className = 'hideDiv';
                    oWnd.close(oArg);

                    selectedObjects = [];
                    idsOfSelectedRows = [];
                }
            }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="divGridPanel">
            <div class="jqgrid">
                <div>
                    <table class="box1">
                        <tr>
                            <td align="left">
                                <div>
                                    <input type="checkbox" name="chkActiveOnly" id="chkActiveOnly" checked="checked" />
                                    Active Only
                                    <input id="btnActiveOnly" name="btnActiveOnly" class="button" value="Search" type="button"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="gridCrewGroups" style="width: 500px !important;" class="table table-striped table-hover table-bordered"></table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="grid_icon">
                                    <div role="group" id="pg_gridPager"></div>
                                    <span class="Span">Page Size:</span>
                                    <input class="PageSize" id="rowNum" type="text" value="10" maxlength="5" />
                                    <input id="btnChange" class="btnChange" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <input id="btnSubmit" class="button" value="OK" type="button" style="width: 60px;" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
