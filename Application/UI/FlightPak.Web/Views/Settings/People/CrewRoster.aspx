﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="CrewRoster.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.CrewRoster"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Framework/Masters/Settings.master" %>
<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Import Namespace="System.Web.Optimization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <style type="text/css">
        
        #tblCrewCheckList div.RadGrid .rgHoveredRow
        {
            background: none !important;
        }
         .Normal-rec td{
             background-color: rgb(255,255,255);
             border-style: solid;
             border-width: 0 0 1px 1px;
             border-color: #a4abb2;
         }
        .Normal-rec:hover td{
              background-color: rgba(255,255,255,0.7);
         }
        .Normal-rec-select td{
            background-color: rgba(255,255,255,0.7);
        }

         .Expired-rec td{
             background-color: rgb(255,0,0);
             border-style: solid;
             border-width: 0 0 1px 1px;
             border-color: #a4abb2;
            color:rgb(255,255,255) !important;
         }
        .Expired-rec:hover td{
              background-color: rgba(255,0, 0, 0.7);
         }
        .Expired-rec-select td{
            background-color: rgba(255,0, 0, 0.7);
            color:rgb(255,255,255) !important;
        }
          .Expired-rec-select span {
            color: rgb(255,255,255) !important;
        }
        .Expired-rec span {     
            color: rgb(255,255,255) !important;
        }
        
        .Alert-rec td{
             background-color: rgb(255,255,0);
             border-style: solid;
             border-width: 0 0 1px 1px;
             border-color: #a4abb2;
         }
        .Alert-rec:hover td{
             background-color: rgba(255,255, 0, 0.7);
         }
        .Alert-rec-select td{
             background-color: rgba(255,255, 0, 0.7);
             
        }
        .Grace-rec td{
             background-color: rgb(255,153,0);
             border-style: solid;
             border-width: 0 0 1px 1px;
             border-color: #a4abb2;
         }
        .Grace-rec:hover td{
              background-color: rgba(255,153, 0, 0.7);
         }
        .Grace-rec-select td{
            background-color: rgba(255,153, 0, 0.7);
        }
        .rightmarginclass { float: right; margin-right: 7px !important;}
        .ExportWindow {
        min-width:350px;
        }

    </style>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">

            function ProcessCrewCheckDelete(customMsg) {

                //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
                var grid = $find(window['gridId']);

                var msg = 'Are you sure you want to delete this record?';

                if (customMsg != null) {
                    msg = customMsg;
                }

                var oManager;

                if (window['windowManagerId'] != null) {
                    oManager = $find(window['windowManagerId']);
                }

                if (grid.get_masterTableView().get_selectedItems().length > 0){
                var id = grid.get_masterTableView().get_selectedItems()[0].getDataKeyValue("CrewID");

                $.ajax({
                    type: 'Get',
                    async: false,
                    crossDomain: true,
                    url: '/Views/Utilities/GETApi.aspx?apiType=FSS&method=CheckCrew&clientId=0&crewid=' + id,
                    contentType: "application/json; charset=utf-8",
                    success: function (result) {
                        if (parseInt(result) == 0) {
                            
                                if (oManager == null) {
                                    radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
                                }
                                else {
                                    oManager.radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
                                }
                                return false;

                        }
                        else {
                            if (oManager == null) {
                                radalert('This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.', 330, 100, 'Crew');
                            }
                            else {
                                radalert('This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.', 330, 100, 'Crew');
                            }
                            return false;
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var content = jqXHR.responseText;

                        if (content == "Conflict" || content == "NotFound" || content == "InternalServerError") {
                            showMessageBox(content, popupTitle);
                        }
                    }
                });


                }
                else {
                    if (oManager == null) {
                        radalert('Please select a record from the above table.', 330, 100, "Delete", "");
                    }
                    else {
                        oManager.radalert('Please select a record from the above table.', 330, 100, "Delete", "");
                    }

                    return false;
                }
                return false;
            }


            // this function is used to display the format when the relevent textbox is empty.
            function validateEmptyTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0.0";
                }
            }

            function openDuplicates() {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");

                if (document.getElementById('<%=hdnWarning.ClientID%>').value == "1") {
                    var oWnd = oManager.open("../People/CrewNamePopUp.aspx?CrewLastName=" + document.getElementById("<%=tbLastName.ClientID%>").value, "RadCountryMasterPopup");
                }
                else if (document.getElementById('<%=hdnWarning.ClientID%>').value == "0") {
                    oManager.radalert('No Potential Duplicate Crew Records Found.', 360, 50, 'Crew Roster Catalog');
                }
                else {
                    var oWnd = oManager.open("../People/CrewNamePopUp.aspx", "RadCountryMasterPopup");
                }

            }
			/*
            function SetFocus(arg) {
                document.Form1[arg].focus();
            }
			*/
            function confirmNameLengthFn(arg) {
                if (arg == true) {

                    document.getElementById('<%=btnSaveChangesdummy.ClientID%>').click();
                }
                else {
                    if (document.getElementById('<%=hdnFirstOrLast.ClientID%>').value == "First") {
                        document.getElementById('<%=tbFirstName.ClientID%>').focus();
                    }
                    else if (document.getElementById('<%=hdnFirstOrLast.ClientID%>').value == "Middle") {
                        document.getElementById('<%=tbMiddleName.ClientID%>').focus();
                    }
                    else if (document.getElementById('<%=hdnFirstOrLast.ClientID%>').value == "Last") {
                        document.getElementById('<%=tbLastName.ClientID%>').focus();
                    }
        }
    }
	
	function fncResidentSinceYear(sender, args) {
        var ResidentYear;
        ResidentYear = document.getElementById('<%= tbResidentSinceYear.ClientID %>').value;

        var Today = new Date();

        if (ResidentYear > Today.getFullYear()) {
            document.getElementById('<%= tbResidentSinceYear.ClientID %>').focus();
            args.IsValid = false;
            return;
        }
        args.IsValid = true;
   }
	
	function OnClientCloseGreenNationalityPopup(oWnd, args) {
        var combo = $find("<%= tbGreenNationality.ClientID %>");
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("<%=tbGreenNationality.ClientID%>").value = arg.CountryCD;
                document.getElementById("<%=hdnGreenNationality.ClientID%>").value = arg.CountryID;
                document.getElementById("<%=cvGreenNationality.ClientID%>").innerHTML = "";
            }
			else {
                document.getElementById("<%=tbGreenNationality.ClientID%>").value = "";
                document.getElementById("<%=hdnGreenNationality.ClientID%>").value = "";
                document.getElementById("<%=cvGreenNationality.ClientID%>").innerHTML = "";
                combo.clearSelection();
            }
        }
    }


    function alertCrewCallBackFn(arg) {

        if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbTotalHrsInType") {
            document.getElementById('<%=tbTotalHrsInType.ClientID%>').value = "";
            document.getElementById('<%=tbTotalHrsInType.ClientID%>').focus();
        }
        else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbTotalHrsDay") {
            document.getElementById('<%=tbTotalHrsDay.ClientID%>').value = "";
            document.getElementById('<%=tbTotalHrsDay.ClientID%>').focus();
        }
        else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbTotalHrsNight") {
            document.getElementById('<%=tbTotalHrsNight.ClientID%>').value = "";
            document.getElementById('<%=tbTotalHrsNight.ClientID%>').focus();
        }
        else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbTotalHrsInstrument") {
            document.getElementById('<%=tbTotalHrsInstrument.ClientID%>').value = "";
            document.getElementById('<%=tbTotalHrsInstrument.ClientID%>').focus();
        }

            //5
        else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbPICHrsInType") {
            document.getElementById('<%=tbPICHrsInType.ClientID%>').value = "";
                    document.getElementById('<%=tbPICHrsInType.ClientID%>').focus();
                }
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbPICHrsDay") {
                    document.getElementById('<%=tbPICHrsDay.ClientID%>').value = "";
                    document.getElementById('<%=tbPICHrsDay.ClientID%>').focus();
                }
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbPICHrsNight") {
                    document.getElementById('<%=tbPICHrsNight.ClientID%>').value = "";
                    document.getElementById('<%=tbPICHrsNight.ClientID%>').focus();
                }
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbPICHrsInstrument") {
                    document.getElementById('<%=tbPICHrsInstrument.ClientID%>').value = "";
                    document.getElementById('<%=tbPICHrsInstrument.ClientID%>').focus();
                }

                    //9
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbSICInTypeHrs") {
                    document.getElementById('<%=tbSICInTypeHrs.ClientID%>').value = "";
                    document.getElementById('<%=tbSICInTypeHrs.ClientID%>').focus();
                }
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbSICHrsDay") {
                    document.getElementById('<%=tbSICHrsDay.ClientID%>').value = "";
                    document.getElementById('<%=tbSICHrsDay.ClientID%>').focus();
                }
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbSICHrsNight") {
                    document.getElementById('<%=tbSICHrsNight.ClientID%>').value = "";
                    document.getElementById('<%=tbSICHrsNight.ClientID%>').focus();
                }
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbSICHrsInstrument") {
                    document.getElementById('<%=tbSICHrsInstrument.ClientID%>').value = "";
                    document.getElementById('<%=tbSICHrsInstrument.ClientID%>').focus();
                }

                    //13
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbOtherHrsSimulator") {
                    document.getElementById('<%=tbOtherHrsSimulator.ClientID%>').value = "";
                    document.getElementById('<%=tbOtherHrsSimulator.ClientID%>').focus();
                }
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbOtherHrsFltEngineer") {
                    document.getElementById('<%=tbOtherHrsFltEngineer.ClientID%>').value = "";
                    document.getElementById('<%=tbOtherHrsFltEngineer.ClientID%>').focus();
                }
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbOtherHrsFltInstructor") {
                    document.getElementById('<%=tbOtherHrsFltInstructor.ClientID%>').value = "";
                    document.getElementById('<%=tbOtherHrsFltInstructor.ClientID%>').focus();
                }
                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbOtherHrsFltAttendant") {
                    document.getElementById('<%=tbOtherHrsFltAttendant.ClientID%>').value = "";
                    document.getElementById('<%=tbOtherHrsFltAttendant.ClientID%>').focus();
                }

                else if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbTotalReqFlightHrs") {
                    document.getElementById('<%=tbTotalReqFlightHrs.ClientID%>').value = "";
                    document.getElementById('<%=tbTotalReqFlightHrs.ClientID%>').focus();
                }

}


function validateHour(Ctrl) {
    var AssignValue = $find(Ctrl.id);
    var charExists = (AssignValue._projectedValue.indexOf(':') >= 0) ? true : false;
    if (charExists == true) {
        var array = AssignValue._projectedValue.split(":");
        if ((array[0] == '       ')) {
            AssignValue.set_value('       :' + array[1]);
        }
        if ((array[1] == '  ' || array[1] > 59)) {

            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            oManager.radalert('Minute entry must be between 0 and 59.', 360, 50, 'Crew Roster Catalog');
            AssignValue.set_value('       :  ');
        }
    }
    return true;
}

function allnumeric(inputtxt, e) {
    var numbers = /^-?[0-9]+$/;
    if (inputtxt.value.match(numbers)) {
        return true;
    }
    else {

        var oManager = $find("<%= RadWindowManager1.ClientID %>");
        oManager.radalert('Please input valid numeric characters only.', 360, 50, 'Crew Roster Catalog');
        inputtxt.value = "0";
        return false;
    }
}
function ClickingDelete(sender, args) {
    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
        if (shouldSubmit) {
            document.getElementById('<%=hdnDelete.ClientID%>').value = "Yes";
            this.click();
        }
        else {
            document.getElementById('<%=hdnDelete.ClientID%>').value = "No";
            this.click();
        }

    });

    var text = "Are you sure you want to delete this VISA record?";

    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.radconfirm(text, callBackFunction, 400, 100, null, "Delete");
                args.set_cancel(true);
            }

            function Clicking_old(sender, args) {
                var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                    if (shouldSubmit) {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "Yes";
                        this.click();
                    }
                    else {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "No";
                        this.click();
                    }

                });

                var text = "Are you sure you want to delete this PASSPORT record?";

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.radconfirm(text, callBackFunction, 400, 100, null, "Delete");
                args.set_cancel(true);
            }

            function Clicking(sender, args) {
                var ReturnValue = false;
                CheckPassportChanged();
                var grid = $find("<%=dgPassport.ClientID %>").get_masterTableView();
                var length = grid.get_selectedItems().length;
                if (length != 0) {
                    var Msg = "Are you sure you want to delete this PASSPORT record?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            CheckPassportChoiceDelete();
                            this.click();
                        }
                    });

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Crew");
                    args.set_cancel(true);
                }
                ReturnValue = false;
                return ReturnValue;
            }

            function ClickingAdditionalInfoDelete(sender, args) {
                var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                    if (shouldSubmit) {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "Yes";
                        this.click();
                    }
                    else {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "No";
                        this.click();
                    }

                });

                var text = "Are you sure you want to delete this Additional Info record?";

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.radconfirm(text, callBackFunction, 400, 100, null, "Delete");
                args.set_cancel(true);
            }

            function ClickingDeleteTypeRating(sender, args) {
                var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                    if (shouldSubmit) {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "Yes";
                        this.click();
                    }
                    else {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "No";
                        this.click();
                    }

                });

                var text = "Are you sure you want to delete this CREW RATING record?";

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.radconfirm(text, callBackFunction, 400, 100, null, "Delete");
                args.set_cancel(true);
            }

            function CLickingChecklistDelete(sender, args) {
                var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                    if (shouldSubmit) {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "Yes";
                        this.click();
                    }
                    else {
                        document.getElementById('<%=hdnDelete.ClientID%>').value = "No";
                        this.click();
                    }

                });

                var text = "Are you sure you want to delete this CREW CHECKLIST record?";

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.radconfirm(text, callBackFunction, 400, 100, null, "Delete");
                args.set_cancel(true);
            }

            function CheckPassportDuplicate(txtPassport, args) {
                var ReturnValue = false;
                var PassportNumber = txtPassport.value;
                var grid = $find("<%=dgPassport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var Country, CountryName;
                var OthPassportID, OthPassportNum, OthCountry, OthCountryName;
                var callBackFunction = Function.createDelegate(txtPassport, function (shouldSubmit) {
                    txtPassport.value = "";
                    return ReturnValue;
                });
                for (var i = 0; i < length; i++) {
                    OthPassportID = MasterTable.get_dataItems()[i].findElement("tbPassportNum");
                    if (txtPassport.id == OthPassportID.id) {
                        Country = MasterTable.get_dataItems()[i].findElement("tbPassportCountry");
                        CountryName = MasterTable.get_dataItems()[i].findElement("tbPassportCountry").value;
                        CountryName = CountryName.replace(/^\s+|\s+$/g, '');
                        CountryName = CountryName.toUpperCase();
                        break;
                    }
                }

                var oManager = $find("<%= RadWindowManager1.ClientID %>");

                for (var i = 0; i < length; i++) {
                    OthPassportID = MasterTable.get_dataItems()[i].findElement("tbPassportNum");
                    OthPassportNum = MasterTable.get_dataItems()[i].findElement("tbPassportNum").value;
                    OthCountry = MasterTable.get_dataItems()[i].findElement("tbPassportCountry");
                    OthCountryName = MasterTable.get_dataItems()[i].findElement("tbPassportCountry").value;
                    OthCountryName = OthCountryName.replace(/^\s+|\s+$/g, '');
                    OthCountryName = OthCountryName.toUpperCase();
                    if (PassportNumber == "") {
                        oManager.radalert("Passport Number should not be Empty in Passport/Visa Tab.", 400, 100, "Crew", callBackFunction);
                        return ReturnValue;
                    }
                    if (txtPassport.id != OthPassportID.id) {
                        //                        if ((PassportNumber == OthPassportNum) && (CountryName == OthCountryName)) {
                        //                            radalert("Country Code / Passport Number must be unique - Enter valid Country Code and Passport Number.", 400, 100, "Crew", callBackFunction);
                        //                            return ReturnValue;
                        //                        }
                        if (PassportNumber == OthPassportNum) {
                            oManager.radalert("Passport Number Must Be Unique in Passport/Visa Tab.", 400, 100, "Crew", callBackFunction);
                            return ReturnValue;
                        }
                    }
                }
                return ReturnValue;
            }
            function CheckVisaDuplicate(txtVisa, args) {
                var ReturnValue = false;
                var VisaNumber = txtVisa.value;
                var grid = $find("<%=dgVisa.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var Country, CountryName;
                var OthVisaID, OthVisaNum, OthCountryID, OthCountryName;
                var callBackFunction = Function.createDelegate(txtVisa, function (shouldSubmit) {
                    txtVisa.value = "";
                    return ReturnValue;
                });
                for (var i = 0; i < length; i++) {
                    OthVisaID = MasterTable.get_dataItems()[i].findElement("tbVisaNum");
                    if (txtVisa.id == OthVisaID.id) {
                        Country = MasterTable.get_dataItems()[i].findElement("tbCountryCD");
                        CountryName = MasterTable.get_dataItems()[i].findElement("tbCountryCD").value;
                        CountryName = CountryName.replace(/^\s+|\s+$/g, '');
                        CountryName = CountryName.toUpperCase();
                        break;
                    }
                }

                var oManager = $find("<%= RadWindowManager1.ClientID %>");

                for (var i = 0; i < length; i++) {
                    OthVisaID = MasterTable.get_dataItems()[i].findElement("tbVisaNum");
                    OthVisaNum = MasterTable.get_dataItems()[i].findElement("tbVisaNum").value;
                    OthCountryID = MasterTable.get_dataItems()[i].findElement("tbCountryCD");
                    OthCountryName = MasterTable.get_dataItems()[i].findElement("tbCountryCD").value;
                    OthCountryName = OthCountryName.replace(/^\s+|\s+$/g, '');
                    OthCountryName = OthCountryName.toUpperCase();
                    if (VisaNumber == "") {
                        oManager.radalert("Visa Number should not be Empty in Passport/Visa Tab.", 400, 100, "Crew", callBackFunction);
                        return ReturnValue;
                    }
                    if (txtVisa.id != OthVisaID.id) {
                        //if ((VisaNumber == OthVisaNum) && (CountryName == OthCountryName)) {
                        //  radalert("Country Code / Visa Number must be unique - Enter valid Country Code and Visa Number.", 400, 100, "Passengers/Requestors", callBackFunction);
                        //  return ReturnValue;
                        //}
                        if (VisaNumber == OthVisaNum) {
                            oManager.radalert("Visa Number Must Be Unique in Passport/Visa Tab.", 400, 100, "Crew", callBackFunction);
                            return ReturnValue;
                        }
                    }
                }
                return ReturnValue;
            }

            function CheckPassportChoice(sender, args) {
                var ReturnValue = false;
                var grid = $find("<%=dgPassport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                var IsPassportChoice = false;
                var PassportNum;
                var chkChoice;
                var Msg = "";
                for (var i = 0; i < length; i++) {
                    PassportNum = MasterTable.get_dataItems()[0].findElement("tbPassportNum").value;
                    chkChoice = MasterTable.get_dataItems()[i].findElement("chkChoice");
                    if (chkChoice.checked == true) {
                        IsPassportChoice = true;
                        PassportNum = MasterTable.get_dataItems()[i].findElement("tbPassportNum").value;
                        break;
                    }
                }
                if ((document.getElementById('<%=hdnIsPassportChanged.ClientID%>').value == "Yes") && (document.getElementById('<%=hdnSave.ClientID%>').value == "Update")) {
                    if (length > 0 && document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "" && document.getElementById('<%=hdnIsPassportChoiceChanged.ClientID%>').value == "Yes") {
                        if (IsPassportChoice == true) {
                            Msg = "The choice passport for this Crew has changed. <br>Would you like to update future tripsheet Crew information with the new passport number [" + PassportNum + "]?";
                        }
                        else {
                            Msg = "There is no choice passport for this Crew. <br>Would you like to update future tripsheet Crew information with First Available passport number [" + PassportNum + "]?";
                        }
                    }
                    else if (length > 0 && document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "1" && document.getElementById('<%=hdnIsPassportChoiceChanged.ClientID%>').value == "Yes") {
                        if (IsPassportChoice == true) {
                            Msg = "Passport [" + document.getElementById('<%=hdnIsPassportChoiceDeleted.ClientID%>').value + "] has been deleted for this Crew. <br>Would you like to update future tripsheet Crew information with the new passport number [" + PassportNum + "]?";
                        }
                        else {
                            Msg = "Passport [" + document.getElementById('<%=hdnIsPassportChoiceDeleted.ClientID%>').value + "] has been deleted for this Crew. <br>Would you like to update future tripsheet Crew information with First Available passport number [" + PassportNum + "]?";
                        }
                    }
                    else if (length > 0 && document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "" && document.getElementById('<%=hdnIsPassportChoiceChanged.ClientID%>').value == "") {
                        if (IsPassportChoice == true) {
                            Msg = "The choice passport for this Crew has changed. <br>Would you like to update future tripsheet Crew information with the new passport number [" + PassportNum + "]?";
                        }
                        else {
                            Msg = "There is no choice passport for this Crew. <br>Would you like to update future tripsheet Crew information with First Available passport number [" + PassportNum + "]?";
                        }
                    }
                    else if (length == 0 && document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "1" && document.getElementById('<%=hdnIsPassportChoiceChanged.ClientID%>').value == "Yes") {
                        Msg = "There is no passport for this Crew. <br>Would you like to update future tripsheet Crew information by Deleting All Passport Information?";
                    }
                    else if (length == 0 && document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "") {
                        Msg = "There is no passport for this Crew. <br>Would you like to update future tripsheet Crew information by Deleting All Passport Information?";
                    }
            }
            var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                if (shouldSubmit) {
                    if (Msg != "")
                    document.getElementById('<%=hdnIsPassportChoice.ClientID%>').value = "Yes";
                    else
                    document.getElementById('<%=hdnIsPassportChoice.ClientID%>').value = "No";
                    }
                else {
                    document.getElementById('<%=hdnIsPassportChoice.ClientID%>').value = "No";
                }
                this.click();
                });
            if (Msg != "") {

               var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Crew");
                args.set_cancel(true);
            }
            else {
                shouldSubmit = false;
                callBackFunction(sender);
                args.set_cancel(true);
            }

            return ReturnValue;
        }

        function CheckPassportChoiceDelete() {
            var grid = $find("<%=dgPassport.ClientID %>");
        var MasterTable = grid.get_masterTableView();
        var length = MasterTable.get_dataItems().length;
        var IsPassportChoice = false;
        var PassportNum;
        var chkChoice;
        for (var i = 0; i < length; i++) {
            if (MasterTable.get_dataItems()[i].get_selected() == true) {
                PassportNum = MasterTable.get_dataItems()[0].findElement("tbPassportNum").value;
                chkChoice = MasterTable.get_dataItems()[i].findElement("chkChoice");
                if (chkChoice.checked == true) {
                    IsPassportChoice = true;
                    PassportNum = MasterTable.get_dataItems()[i].findElement("tbPassportNum").value;
                    break;
                }
            }
        }
        if ((IsPassportChoice == true) && (length != 0)) {
            if (document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value == "") {
                document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value = "1";
                document.getElementById('<%=hdnIsPassportChoiceDeleted.ClientID%>').value = PassportNum;
            }
        }
        else if ((IsPassportChoice == false) && (length != 0)) {
            if (document.getElementById('<%=hdnIsPassportChanged.ClientID%>').value == "Yes") {
                        document.getElementById('<%=hdnIsPassportChoiceDeletedCount.ClientID %>').value = "";
                        document.getElementById('<%=hdnIsPassportChoiceDeleted.ClientID%>').value = PassportNum;
                    }
                }
            return true;
        }

        function uncheckOther(chk) {
            var status = chk.checked;

            var checkBoxes = $("input[id*='chkChoice']");

            $.each(checkBoxes, function () {
                $(this).attr('checked', false);
            });

            chk.checked = status;
            document.getElementById('<%=hdnIsPassportChoiceChanged.ClientID%>').value = "Yes";
            document.getElementById('<%=hdnIsPassportChanged.ClientID%>').value = "Yes";
        }


        //this function is used to parse the date entered or selected by the user
        function parseDate(sender, e) {
            if (currentDatePicker != null) {
                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                var dateInput = currentDatePicker.get_dateInput();
                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                sender.value = formattedDate;
            }
        }
        // To Store Image
        function File_onchange() {
            __doPostBack('__Page', 'LoadImage');
        }

        function CheckName() {
            var nam = document.getElementById('<%=tbImgName.ClientID%>').value;

            if (nam != "") {
                document.getElementById("<%=fileUL.ClientID %>").disabled = false;
            }
            else {
                document.getElementById("<%=fileUL.ClientID %>").disabled = true;
            }
        }

        function ValidateDate(control) {
            var MinDate = new Date("01/01/1900");
            var MaxDate = new Date("12/31/2100");
            var SelectedDate;
            if (control.value != "") {
                SelectedDate = new Date(control.value);
                if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radalert("Please enter / select Date between 01/01/1900 and 12/31/2100", 400, 100, "Date");
                    control.value = "";
                }
            }
            return false;
        }

        // this function is used to allow user to check only one checkbox
        function CheckOne(obj) {
            var grid = obj.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (obj.checked && inputs[i] != obj && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }

        //this function is used to navigate to pop up screen's with the selected code
        function openWin(radWin) {
            var url = '';
            if (radWin == "RadCountryPopup") {
                url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbCountry.ClientID%>').value;
                }
                else if (radWin == "radAirportPopup") {
                    url = '../Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbHomeBase.ClientID%>').value;
                }
                else if (radWin == "RadCrewGroupPopup") {

                    url = "../People/CrewGroupPopup.aspx?CrewGroupCD=" + document.getElementById("<%=tbCrewGroupFilter.ClientID%>").value;
                }
				else if (radWin == "RadGreenNationalityMasterPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbGreenNationality.ClientID%>').value;
                }
                else if (radWin == "RadClientCodePopup") {
                    url = "../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCode.ClientID%>").value;
                }
                else if (radWin == "RadFilterClientPopup") {
                    url = "../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCodeFilter.ClientID%>").value;
                }
                else if (radWin == "RadCitizenPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbCitizenCode.ClientID%>').value;
                }
                else if (radWin == "RadLicCountryPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbLicCountry.ClientID%>').value;
                }
                else if (radWin == "RadAddLicCountryPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbAddLicCountry.ClientID%>').value;
                }
                else if (radWin == "RadCountryOfBirthPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbCountryBirth.ClientID%>').value;
                }
                else if (radWin == "RadDeptPopup") {
                    url = '../Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=' + document.getElementById('<%=tbDepartment.ClientID%>').value;
                }
                else if (radWin == "RadAddlInfo") {
                    url = '../People/CrewRosterAddInfoPopup.aspx';
                }
                else if (radWin == "RadAircraftTypePopup") {
                    url = '../Fleet/CrewRosterAircraftTypePopup.aspx';
                }
                else if (radWin == "RadCrewCheckListPopup") {
                    url = '../People/CrewCheckListPopup.aspx';
                }
                else if (radWin == "RadAicraftType") {
                    url = '../Fleet/CrewRosterAircraftTypePopup.aspx?AircraftTypeCD=' + document.getElementById('<%=tbAircraftType.ClientID%>').value;
                }
                else if (radWin == "RadAicraftTypePopup") {
                    url = '../Fleet/AircraftPopup.aspx?AircraftCD=' + document.getElementById('<%=tbAircraftType.ClientID%>').value;
                }
                else if (radWin == "RadChecklistPopUp") {
                    url = '../People/CrewRosterChecklistSettings.aspx?CrewCD=' + document.getElementById('<%=tbCrewCode.ClientID%>').value + '&CrewChecklistCD =' + document.getElementById('<%=lbCheckListCode.ClientID%>').value;
                }
                else if (radWin == "RadExportData") {
                    url = "../../Reports/ExportReportInformation.aspx?Report=RptDBCrewRoster&CrewCD=" + document.getElementById('<%=tbCrewCode.ClientID%>').value;
                }
                else if (radWin == "RadCrewRosterReportsPopup") {
                    url = "../People/CrewRosterReportsPopup.aspx?UserCD=UC&CrewCD=" + document.getElementById('<%=tbCrewCode.ClientID%>').value;
                }

    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }

            // this function is related to Image 
            function OpenRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
                oWnd.show();
            }

            // this function is related to Image 
            function CloseRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = null;
                oWnd.close();
            }
            // this function is used to display the value of selected country code from popup
            function CountryPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").innerHTML = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbCountry.ClientID%>").value = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").innerHTML = "";
                    }
                }
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup


            // this function is used to display the value of selected country code from popup
            function LicCountryPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbLicCountry.ClientID%>").value = arg.CountryCD;
                        <%--document.getElementById("<%=cvLicCountry.ClientID%>").innerHTML = "";--%>
                        document.getElementById("<%=hdnLicCountryID.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbLicCountry.ClientID%>").value = "";
                        document.getElementById("<%=hdnLicCountryID.ClientID%>").value = "";
                    }
                }
            }

            // this function is used to display the value of selected country code from popup
            function AddLicCountryPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAddLicCountry.ClientID%>").value = arg.CountryCD;
                        <%--document.getElementById("<%=cvAddLicCountry.ClientID%>").innerHTML = "";--%>
                        document.getElementById("<%=hdnAddLicCountryID.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbAddLicCountry.ClientID%>").value = "";
                        document.getElementById("<%=hdnAddLicCountryID.ClientID%>").value = "";
                    }
                }
            }

            // this function is used to display the value of selected country code from popup
            function AddCountryOfBirthPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCountryBirth.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=cvCountryBirth.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnResidenceCountryID.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbCountryBirth.ClientID%>").value = "";
                        document.getElementById("<%=hdnResidenceCountryID.ClientID%>").value = "";
                    }
                }
            }

            // this function is used to display the value of selected homebase code from popup
            function HomeBasePopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.AirportID;

                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                    }
                }
            }





            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {


                if (currentTextBox != null) {
                    var step = "Date_TextChanged";

                    if (currentTextBox.value != args.get_newValue()) {

                        currentTextBox.value = args.get_newValue();
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                }
            }

            function setSelectedDate(control) {
                control.value = selectedDate;
            }

            function CrewGroupPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCrewGroupFilter.ClientID%>").value = arg.CrewGroupCD;
                        document.getElementById("<%=hdnCrewGroupID.ClientID%>").value = arg.ClientID;


                        var step = "CrewGroupFilter_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }

                    }
                    else {
                        document.getElementById("<%=tbCrewGroupFilter.ClientID%>").value = "";
                    }
                }
            }



            // this function is used to display the value of selected client code from popup
            function ClientCodePopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = htmlDecode(arg.ClientCD);
                        document.getElementById("<%=cvClientCode.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnClientID.ClientID%>").value = arg.ClientID;
                    }
                }
            }

            function ClientCodeFilterPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnClientFilterID.ClientID%>").value = arg.ClientID;
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = arg.ClientCD;
                        var step = "ClientCodeFilter_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                }
            }
            
            // this function is used to display the value of selected country code from popup
            function CitizenPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCitizenCode.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=lbCitizenDesc.ClientID%>").innerHTML = arg.CountryName;
                        document.getElementById("<%=cvCitizenCode.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCitizenshipID.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbCitizenCode.ClientID%>").value = "";
                        document.getElementById("<%=lbCitizenDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCitizenshipID.ClientID%>").value = "";
                    }
                }
            }

            // this function is used to display the value of selected aircrafttype code from popup
            function AicraftTypePopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();

                var step = "AircraftType_TextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");

                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAircraftType.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=cvAssociateTypeCode.ClientID%>").innerHTML = "";

                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }

                }
            }


            function SingleAicraftTypePopupClose(oWnd, args) {


                var arg = args.get_argument();

                var step = "AircraftType_TextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");

                if (arg != null) {
                    document.getElementById("<%=tbAircraftType.ClientID%>").value = arg.AircraftCD;
                    document.getElementById("<%=cvAssociateTypeCode.ClientID%>").innerHTML = "";
                }
                else {
                    document.getElementById("<%=cvAssociateTypeCode.ClientID%>").innerHTML = "";
                }


                if (step) {
                    ajaxManager.ajaxRequest(step);
                }


            }


            // this function is used to display the value of selected department code from popup
            function DeptPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDepartment.ClientID%>").value = arg.DepartmentCD;
                        document.getElementById("<%=cvDepartmentCode.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnDepartmentID.ClientID%>").value = arg.DepartmentID;
                    }
                    else {
                        document.getElementById("<%=tbDepartment.ClientID%>").value = "";
                        document.getElementById("<%=hdnDepartmentID.ClientID%>").value = "";
                    }
                }
            }

            // this function is to validate the required field on tab out
            function CheckTxtBox(control) {

                if (control == 'lastName') {
                    ValidatorEnable(document.getElementById('<%=rfvLastName.ClientID%>'));
                }
            }

            function confirmClick(sender, eventArgs) {

                var day = eventArgs.get_renderDay();
                if (day.get_isSelectable()) {
                    var date = day.get_date();
                    var dfi = sender.DateTimeFormatInfo;
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dfi.FormatDate(day.get_date(), dateInput.get_displayDateFormat());

                    //            var date1 = currentDatePicker.get_dateInput().parseDate(formattedDate);

                    //            var formattedDate1 = dateInput.get_dateFormatInfo().FormatDate(formattedDate, dateInput.get_displayDateFormat());
                    currentTextBox.value = formattedDate;
                    eventArgs.set_cancel(false);

                }
            }



            var currentTextBox = null;
            var currentDatePicker = null;
            var selectedDate = null;
            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker
                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }


            // this function is used to display the crew additional information popup 
            function ShowAddlInfoPopup(sender, args) {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../People/CrewRosterAddInfoPopup.aspx", "RadAddlInfo");
                return false;
            }

            // this function is used to display the crew checklist settings popup 
            function ShowChecklistSettingsPopup(sender, args) {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../People/CrewRosterChecklistSettings.aspx", "RadChecklistPopUp");
                return false;

            }

            // this function is used to display the aircraft type popup 
            function ShowAircraftTypePopup(sender, args) {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../Fleet/CrewRosterAircraftTypePopup.aspx", "RadAircraftTypePopup");
                return false;
            }

            // this function is used to display the crew checklist popup 
            function ShowCrewCheckListPopup(sender, args) {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../People/CrewCheckListPopup.aspx", "RadCrewCheckListPopup");
                return false;
            }

            // this function is used to the refresh the currency grid
            function refreshGrid(arg) {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest('Rebind');
            }

            // this function is used to get the dimensions
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            //this function is used to resize the pop-up window
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            //this function is used to expand the panel on click
            function OnClientClick(strPanelToExpand) {
                var PanelBar1 = $find("<%= pnlAdditionalInfo.ClientID %>");
                var PanelBar2 = $find("<%= pnlCurrency.ClientID %>");
                var PanelBar3 = $find("<%= pnlAdditionalNotes.ClientID %>");
                var PanelBar4 = $find("<%= pnlMaintenance.ClientID %>");

                var PanelBar5 = $find("<%= pnlImage.ClientID %>");

                PanelBar1.get_items().getItem(0).set_expanded(false);
                PanelBar2.get_items().getItem(0).set_expanded(false);
                PanelBar3.get_items().getItem(0).set_expanded(false);
                PanelBar4.get_items().getItem(0).set_expanded(false);
                PanelBar5.get_items().getItem(0).set_expanded(false);
                if (strPanelToExpand == "AdditionalNotes") {
                    PanelBar3.get_items().getItem(0).set_expanded(true);
                    $('html,body').animate({ scrollTop: $("#<%= pnlAdditionalNotes.ClientID %>").offset().top - 50 }, 1000);
                }
                else if (strPanelToExpand == "Currency") {
                    PanelBar2.get_items().getItem(0).set_expanded(true);
                    $('html,body').animate({ scrollTop: $("#<%= pnlCurrency.ClientID %>").offset().top - 50 }, 1000);
                }
                else if (strPanelToExpand == "AdditionalInformation") {
                    PanelBar1.get_items().getItem(0).set_expanded(true);
                    $('html,body').animate({ scrollTop: $("#<%= pnlAdditionalInfo.ClientID %>").offset().top - 50 }, 1000);
                }
                else if (strPanelToExpand == "Image") {
                    PanelBar5.get_items().getItem(0).set_expanded(true);
                    $('html,body').animate({ scrollTop: $("#<%= pnlImage.ClientID %>").offset().top - 50 }, 1000);
                }
    return false;
}


function SelectAll(obj) {

    if (obj.checked == true) {
        document.getElementById("<%= chkHomeArrival.ClientID %>").checked = true;
                    document.getElementById("<%= chkHomeDeparture.ClientID %>").checked = true;
                    document.getElementById("<%= chkDepartmentAuthorization.ClientID %>").checked = true;
                    document.getElementById("<%= chkRequestorPhone.ClientID %>").checked = true;
                    document.getElementById("<%= chkAccountNo.ClientID %>").checked = true;
                    document.getElementById("<%= chkCancellationDesc.ClientID %>").checked = true;
                    document.getElementById("<%= chkStatus.ClientID %>").checked = true;
                    document.getElementById("<%= chkAirport.ClientID %>").checked = true;
                    document.getElementById("<%= chkChecklist.ClientID %>").checked = true;
                    document.getElementById("<%= chkRunway.ClientID %>").checked = true;
                    document.getElementById("<%= chkEndDuty.ClientID %>").checked = true;
                    document.getElementById("<%= chkOverride.ClientID %>").checked = true;
                    document.getElementById("<%= chkCrewRules.ClientID %>").checked = true;
                    document.getElementById("<%= chkFAR.ClientID %>").checked = true;
                    document.getElementById("<%= chkDutyHours.ClientID %>").checked = true;
                    document.getElementById("<%= chkFlightHours.ClientID %>").checked = true;
                    document.getElementById("<%= chkRestHours.ClientID %>").checked = true;
                    document.getElementById("<%= chkAssociatedCrew.ClientID %>").checked = true;
                    document.getElementById("<%= chkCrewTransport.ClientID %>").checked = true;
                    document.getElementById("<%= chkCrewArrival.ClientID %>").checked = true;
                    document.getElementById("<%= chkHotel.ClientID %>").checked = true;
                    document.getElementById("<%= chkPassengerTransport.ClientID %>").checked = true;
                    document.getElementById("<%= chkPassengerArrival.ClientID %>").checked = true;
                    document.getElementById("<%= chkPassengerHotel.ClientID %>").checked = true;
                    document.getElementById("<%= chkPassengerDetails.ClientID %>").checked = true;
                    document.getElementById("<%= chkPassengerPaxPhone.ClientID %>").checked = true;
                    document.getElementById("<%= chkDepartureInformation.ClientID %>").checked = true;
                    document.getElementById("<%= chkArrivalInformation.ClientID %>").checked = true;
                    document.getElementById("<%= chkDepartureCatering.ClientID %>").checked = true;
                    document.getElementById("<%= chkArrivalCatering.ClientID %>").checked = true;
                    document.getElementById("<%= chkNEWSPAPER.ClientID %>").checked = true;
                    document.getElementById("<%= chkCOFFEE.ClientID %>").checked = true;
                    document.getElementById("<%= chkJUICE.ClientID %>").checked = true;
                    document.getElementById("<%= chkArrivalDepartureTime.ClientID %>").checked = true;
                    document.getElementById("<%= chkArrivalDepartureICAO.ClientID %>").checked = true;
                    document.getElementById("<%= chkAircraft.ClientID %>").checked = true;
                    document.getElementById("<%= chkGeneralCrew.ClientID %>").checked = true;
                    document.getElementById("<%= chkGeneralPassenger.ClientID %>").checked = true;
                    document.getElementById("<%= chkOutBoundInstructions.ClientID %>").checked = true;
                    document.getElementById("<%= chkGeneralChecklist.ClientID %>").checked = true;
                    document.getElementById("<%= chkLogisticsFBO.ClientID %>").checked = true;
                    document.getElementById("<%= chkLogisticsHotel.ClientID %>").checked = true;
                    document.getElementById("<%= chkLogisticsTransportation.ClientID %>").checked = true;
                    document.getElementById("<%= chkLogisticsCatering.ClientID %>").checked = true;
                    document.getElementById("<%= chkCrewHotelConfirm.ClientID %>").checked = true;
                    document.getElementById("<%= chkCrewHotelComments.ClientID %>").checked = true;
                    document.getElementById("<%= chkCrewDeptTransConfirm.ClientID %>").checked = true;
                    document.getElementById("<%= chkOutboundInstComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkTripNotes.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXNotes.ClientID %>").checked = false;
                    document.getElementById("<%= chkTripAlerts.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXArrTransComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrCaterComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXArrTransConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrCaterConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXDeptTransComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptCaterComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXDeptTransConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptCaterConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXHotelComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptFBOComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXHotelConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptFBOConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewNotes.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrFBOComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewArrTransComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrFBOConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewArrTransConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrAirportAlerts.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewDeptTransComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptAirportAlerts.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptAirportNotes.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrAirportNotes.ClientID %>").checked = false;

                }
                else {

                    document.getElementById("<%= chkHomeArrival.ClientID %>").checked = false;
                    document.getElementById("<%= chkHomeDeparture.ClientID %>").checked = false;
                    document.getElementById("<%= chkDepartmentAuthorization.ClientID %>").checked = false;
                    document.getElementById("<%= chkRequestorPhone.ClientID %>").checked = false;
                    document.getElementById("<%= chkAccountNo.ClientID %>").checked = false;
                    document.getElementById("<%= chkCancellationDesc.ClientID %>").checked = false;
                    document.getElementById("<%= chkStatus.ClientID %>").checked = false;
                    document.getElementById("<%= chkAirport.ClientID %>").checked = false;
                    document.getElementById("<%= chkChecklist.ClientID %>").checked = false;
                    document.getElementById("<%= chkRunway.ClientID %>").checked = false;
                    document.getElementById("<%= chkEndDuty.ClientID %>").checked = false;
                    document.getElementById("<%= chkOverride.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewRules.ClientID %>").checked = false;
                    document.getElementById("<%= chkFAR.ClientID %>").checked = false;
                    document.getElementById("<%= chkDutyHours.ClientID %>").checked = false;
                    document.getElementById("<%= chkFlightHours.ClientID %>").checked = false;
                    document.getElementById("<%= chkRestHours.ClientID %>").checked = false;
                    document.getElementById("<%= chkAssociatedCrew.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewTransport.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewArrival.ClientID %>").checked = false;
                    document.getElementById("<%= chkHotel.ClientID %>").checked = false;
                    document.getElementById("<%= chkPassengerTransport.ClientID %>").checked = false;
                    document.getElementById("<%= chkPassengerArrival.ClientID %>").checked = false;
                    document.getElementById("<%= chkPassengerHotel.ClientID %>").checked = false;
                    document.getElementById("<%= chkPassengerDetails.ClientID %>").checked = false;
                    document.getElementById("<%= chkPassengerPaxPhone.ClientID %>").checked = false;
                    document.getElementById("<%= chkDepartureInformation.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrivalInformation.ClientID %>").checked = false;
                    document.getElementById("<%= chkDepartureCatering.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrivalCatering.ClientID %>").checked = false;
                    document.getElementById("<%= chkNEWSPAPER.ClientID %>").checked = false;
                    document.getElementById("<%= chkCOFFEE.ClientID %>").checked = false;
                    document.getElementById("<%= chkJUICE.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrivalDepartureTime.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrivalDepartureICAO.ClientID %>").checked = false;
                    document.getElementById("<%= chkAircraft.ClientID %>").checked = false;
                    document.getElementById("<%= chkGeneralCrew.ClientID %>").checked = false;
                    document.getElementById("<%= chkGeneralPassenger.ClientID %>").checked = false;
                    document.getElementById("<%= chkOutBoundInstructions.ClientID %>").checked = false;
                    document.getElementById("<%= chkGeneralChecklist.ClientID %>").checked = false;
                    document.getElementById("<%= chkLogisticsFBO.ClientID %>").checked = false;
                    document.getElementById("<%= chkLogisticsHotel.ClientID %>").checked = false;
                    document.getElementById("<%= chkLogisticsTransportation.ClientID %>").checked = false;
                    document.getElementById("<%= chkLogisticsCatering.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewHotelConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewHotelComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewDeptTransConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptAirportNotes.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrAirportNotes.ClientID %>").checked = false;

                    document.getElementById("<%= chkOutboundInstComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkTripNotes.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXNotes.ClientID %>").checked = false;
                    document.getElementById("<%= chkTripAlerts.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXArrTransComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrCaterComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXArrTransConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrCaterConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXDeptTransComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptCaterComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXDeptTransConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptCaterConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXHotelComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptFBOComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkPAXHotelConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptFBOConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewNotes.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrFBOComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewArrTransComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrFBOConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewArrTransConfirm.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrAirportAlerts.ClientID %>").checked = false;
                    document.getElementById("<%= chkCrewDeptTransComments.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptAirportAlerts.ClientID %>").checked = false;
                    document.getElementById("<%= chkDeptAirportNotes.ClientID %>").checked = false;
                    document.getElementById("<%= chkArrAirportNotes.ClientID %>").checked = false;
                }

            }

            // this function is used to limit the number of checkboxes to be checked
            function chkcontrol(obj) {

                var checkCount = 0

                //maximum number of allowed checked boxes
                var maxChecks = 3

                if (document.getElementById("<%= chkCrewHotelConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }


                if (document.getElementById("<%= chkDeptAirportNotes.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkCrewHotelComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkArrAirportNotes.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkCrewDeptTransConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkDeptAirportAlerts.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkCrewDeptTransComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkArrAirportAlerts.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkCrewArrTransConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkArrFBOConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkCrewArrTransComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkArrFBOComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkCrewNotes.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkDeptFBOConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkPAXHotelConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }


                if (document.getElementById("<%= chkDeptFBOComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkPAXHotelComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkDeptCaterConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkPAXDeptTransConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkDeptCaterComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }


                if (document.getElementById("<%= chkPAXDeptTransComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkArrCaterConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkPAXArrTransConfirm.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }


                if (document.getElementById("<%= chkArrCaterComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkPAXArrTransComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkTripAlerts.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }


                if (document.getElementById("<%= chkPAXNotes.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkTripNotes.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }

                if (document.getElementById("<%= chkOutboundInstComments.ClientID %>").checked == true) {
                    checkCount = checkCount + 1
                }



                //if they checked a 4th box, uncheck the box, then decrement checkcount and pop alert
                //if (checkCount > maxChecks) {
                //    obj.checked = false
                //    checkCount = checkCount - 1
                //    alert('Maximim 3 options can be selected from Additional Info')
                //}
            }



            // this function is used to limit the number of checkboxes to be checked
            function Deselectthis(obj) {

                //if they checked a 4th box, uncheck the box, then decrement checkcount and pop alert
                if (obj.checked == false) {
                    document.getElementById("<%= chkSelectAll.ClientID %>").checked = false;
                }
            }






            var GControlId, GhdnControlId;
            function openWinGrd(radWin, ControlId) {
                if (ControlId.id.indexOf("btn") != -1) {
                    GControlId = document.getElementById(ControlId.id.replace("btn", "tb"));
                }
                else {
                    GControlId = ControlId;
                }
                var ControlCheck = ControlId.id;
                if (ControlCheck.indexOf("Passport") != -1) {
                    GhdnControlId = ControlCheck.replace("tbPassportCountry", "hdnPassportCountry");
                }
                else {
                    GhdnControlId = ControlCheck.replace("tbCountryCD", "hdnCountryID");
                }
                var url = '';
                if (radWin == "RadVisaCountryMasterPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + ControlId.value;
                }

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }

            function OnClientVisaCountryPopup(oWnd, args) {
                var combo = GControlId;
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        GControlId.value = arg.CountryCD;
                        GhdnControlId.value = arg.CountryID;
                    }
                    else {
                        GControlId.value = "";
                        GhdnControlId.value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseExportData(oWnd, args) {
                document.getElementById('<%=btnShowReports.ClientID%>').click();
                return false;
            }
            function OnClientReportPopup(oWnd, args) {
                var arg = args.get_argument();
                if (arg != null) {
                    var ReportName = document.getElementById("<%=hdnReportName.ClientID%>").value;
                    var CrewCD = document.getElementById("<%=tbCrewCode.ClientID%>").value;
                    document.getElementById("<%=hdnCrewRosterRpt.ClientID%>").value = arg.CrewRosterRpt;
                    document.getElementById("<%=hdnCrewChecklistRpt.ClientID%>").value = arg.CrewChecklistRpt;
                    document.getElementById("<%=hdnInactiveChecklistRpt.ClientID%>").value = arg.InactiveChecklistRpt;
                    document.getElementById("<%=hdnCrewCurrencyRpt.ClientID%>").value = arg.CrewCurrencyRpt;
                    document.getElementById("<%=hdnCrewTypeRatingsRpt.ClientID%>").value = arg.CrewTypeRatingsRpt;
                    if (document.getElementById("<%=hdnReportFormat.ClientID%>").value == "EXPORT") {
                        url = "../../Reports/ExportReportInformation.aspx?Report=" + ReportName + "&P1=" + CrewCD +
                            "&P2=" + arg.CrewRosterRpt + "&P3=" + arg.CrewChecklistRpt + "&P4=" + arg.InactiveChecklistRpt +
                            "&P5=" + arg.CrewCurrencyRpt + "&P6=" + arg.CrewTypeRatingsRpt;

                        var oManager = $find("<%= RadWindowManager1.ClientID %>");
                        var oWnd = oManager.open(url, 'RadExportData');
                        return false;
                    }
                    else {
                        document.getElementById('<%=btnShowReports.ClientID%>').click();
                        return false;
                    }
                }


            }
            function ShowReports(radWin, ReportFormat, ReportName) {
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                url = "../People/CrewRosterReportsPopup.aspx";

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }

            function tbDate_OnKeyDown(sender, event) {
                if (event.keyCode == 9) {
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    return true;
                }
            }

            function ZuluChange() {
                var radZULU = document.getElementById('<%=radZULU.ClientID%>').checked;
                var radLocal = document.getElementById('<%=radLocal.ClientID%>').checked;

                var oManager = $find("<%= RadWindowManager1.ClientID %>");

                if (radZULU == true) {
                    oManager.radalert('E-Mail sent will now be in UTC time. E-Mail sent prior to this change may be inconsistent.', 360, 50, 'Crew Roster Catalog');
                }
                else { //if (radLocal == true) 
                    oManager.radalert('E-Mail sent will now be in Local time. E-Mail sent prior to this change may be inconsistent.', 360, 50, 'Crew Roster Catalog');
                }
                return false;
            }
            function printImage() {
                var image = document.getElementById('<%=ImgPopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlImg = document.getElementById('<%=ddlImg.ClientID%>');
                if (ddlImg.length > 0) {
                    var ImageName = ddlImg.options[ddlImg.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Crew");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }
            function CheckPassportChanged(sender, args) {
                var ReturnValue = true;
                document.getElementById('<%=hdnIsPassportChanged.ClientID%>').value = "Yes";
                return ReturnValue;
            }
        </script>
        <script type="text/javascript">
            function browserName() {
                var agt = navigator.userAgent.toLowerCase();
                if (agt.indexOf("msie") != -1) return 'Internet Explorer';
                if (agt.indexOf("chrome") != -1) return 'Chrome';
                if (agt.indexOf("opera") != -1) return 'Opera';
                if (agt.indexOf("firefox") != -1) return 'Firefox';
                if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
                if (agt.indexOf("netscape") != -1) return 'Netscape';
                if (agt.indexOf("safari") != -1) return 'Safari';
                if (agt.indexOf("staroffice") != -1) return 'Star Office';
                if (agt.indexOf("webtv") != -1) return 'WebTV';
                if (agt.indexOf("beonex") != -1) return 'Beonex';
                if (agt.indexOf("chimera") != -1) return 'Chimera';
                if (agt.indexOf("netpositive") != -1) return 'NetPositive';
                if (agt.indexOf("phoenix") != -1) return 'Phoenix';
                if (agt.indexOf("skipstone") != -1) return 'SkipStone';
                if (agt.indexOf('\/') != -1) {
                    if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                        return navigator.userAgent.substr(0, agt.indexOf('\/'));
                    }
                    else return 'Netscape';
                } else if (agt.indexOf(' ') != -1)
                    return navigator.userAgent.substr(0, agt.indexOf(' '));
                else return navigator.userAgent;
            }
            function GetBrowserName() {
                document.getElementById('<%=hdnBrowserName.ClientID%>').value = browserName();
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
                    <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
            <script type="text/javascript" src="../../../Scripts/jquery.browser.min.js"></script>
        <%=Scripts.Render("~/bundles/scriptFleetProfileCatalogSelect4") %>
        <%=Styles.Render("~/bundles/StyleFleetProfileCatalogSelect2") %>

        <script type="text/javascript">
            //To display plane image at right place
            var isMaintenanceExpanded = true;
            var isAdditionalInformationExpanded = false;
            var isAttachmentsExpanded = true;
            var isCurrencyExpanded = false;
            var isAdditionalNotesExpanded = false;

            function pnlMaintenanceExpand() {
                isMaintenanceExpanded = true;
            }

            function pnlMaintenanceCollapse() {
                isMaintenanceExpanded = false;
            }

            function pnlAdditionalInfoExpand() {
                isAdditionalInformationExpanded = true;
            }

            function pnlAdditionalInfoCollapse() {
                isAdditionalInformationExpanded = false;
            }

            function pnlImageExpand() {
                isAttachmentsExpanded = true;
            }

            function pnlImageCollapse() {
                isAttachmentsExpanded = false;
            }

            function pnlCurrencyExpand() {
                isCurrencyExpanded = true;
                $('#btnRefreshView').trigger('click');
            }

            function pnlCurrencyCollapse() {
                isCurrencyExpanded = false;
            }

            function pnlAdditionalNotesExpand() {
                isAdditionalNotesExpanded = true;
            }

            function pnlAdditionalNotesCollapse() {
                isAdditionalNotesExpanded = false;
            }

            $(document).ready(function showPlaneImage() {
                if ($.trim(decodeURI(getQuerystring("CrewName", ""))) != null && $.trim(decodeURI(getQuerystring("CrewName", ""))) != undefined && $.trim(decodeURI(getQuerystring("CrewName", "")))!="")
                {
                    $(document).prop('title', 'Crew Passport Visa' + " - " + $.trim(decodeURI(getQuerystring("CrewName", ""))));
                }
                
                $('#ctl00_ctl00_MainContent_SettingBodyContent_dgCrewRoster_ctl00_ctl03_ctl01_lbtnInitEdit,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_dgCrewRoster_ctl00_ctl03_ctl01_lbtnInitInsert,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_dgCrewRoster_ctl00_ctl03_ctl01_lbtnDelete,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnSaveChangesTop_input,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnCancelTop,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnEdit,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_pnlCurrency_i0_i0_btnRefreshView,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnSaveChanges_input,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnCancel').on('click', setPlaneImage);

                $('#ctl00_ctl00_MainContent_SettingBodyContent_chkActiveOnly,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_chkHomeBaseOnly,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_chkFixedWingOnly,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_chkRotaryWingOnly').on('change', setPlaneImage);

                $(".rgRow").on('click', function () {
                    var topMargin = $(window).scrollTop() - 10;
                    $('div.raDiv').attr('style', 'margin-top: ' + topMargin + 'px !important');
                });
            });

            function setPlaneImage() {

                var scrollTop = $(window).scrollTop();
                var topMargin = 0;
                var multiPage = $find("<%=RadMultiPage1.ClientID %>");
                var selectedPageIndex = -1;

                if (multiPage !== null) {
                    selectedPageIndex = multiPage.get_selectedIndex();
                }
                if (isAdditionalNotesExpanded) {
                    topMargin -= 60;
                }
                if (isAttachmentsExpanded) {
                    topMargin -= 40;
                }
                if (isCurrencyExpanded) {
                    topMargin -= 230;
                }
                if (isAdditionalInformationExpanded) {
                    topMargin -= 190;
                    if ($.browser.webkit) {
                        topMargin -= 30;
                    }
                }

                switch (selectedPageIndex) {
                    case 0:
                        if (isMaintenanceExpanded) {
                            topMargin -= 440;
                            if ($.browser.webkit) {
                                topMargin -= 30;
                            }
                        }
                        break;
                    case 1:
                        topMargin -= 330;
                        break;
                    case 2:
                        topMargin -= 310;
                        break;
                    case 3:
                        topMargin -= 180;
                        break;
                    case 4:
                        topMargin -= 170;
                        break;
                    case 5:
                        topMargin -= 470;
                        break;
                }

                if ($.browser.webkit) {
                    topMargin -= 10;
                }

                topMargin += scrollTop - 130; //-130 is value when all are collapsed
                $('div.raDiv').attr('style', 'margin-top: ' + topMargin + 'px !important');
            }

            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && redirectUrl.toLowerCase().indexOf("javascript") !== 0 && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });

            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
            return false;
        }

        function prepareSearchInput(input) { input.value = input.value; }

        </script>
        <script type="text/javascript">
            function pageLoad() {
                $(document).ready(function () {
                    var postData = new Object();
                    postData["apiType"] = 'fss';
                    postData["method"] = 'countries';
                    postData["countrycd"] = '';
                    postData["markSelectedRecord"] = false;
                    $("#tbLicCountry").createSelect2AutoComplete({
                        idField: "CountryID",
                        textField: "CountryName",
                        textHiddenId: $("#hdntbLicCountryName"),
                        valueHiddenId: $("#hdntbLicCountryId"),
                        postData: postData,
                        searchFieldName: "CountryName",
                        formatFunction: formateCountry,
                        extraParaNames: "CountryCD",
                        formatSelection: formateCountrySelection
                    });
                    $("#tbAddLicCountry").createSelect2AutoComplete({
                        idField: "CountryID",
                        textField: "CountryName",
                        textHiddenId: $("#hdntbAddLicCountryName"),
                        valueHiddenId: $("#hdntbAddLicCountryId"),
                        postData: postData,
                        searchFieldName: "CountryName",
                        formatFunction: formateCountry,
                        extraParaNames: "CountryCD",
                        formatSelection: formateCountrySelectionadd
                    });
                });
                function formateCountry(state) {
                    if (!state.id) { return state.text; }
                    var $state = $('<div class="img-thumbnail flag flag-icon-background flag-icon-' + state.CountryCD.toLowerCase() + '" title="' + state.CountryCD.toLowerCase() + '" id="' + state.CountryCD.toLowerCase() + '"><span style="margin-left: 25px;display: inline-block;height: 14px;overflow: hidden;width:110px">' + state.text + '</span></div>');
                    return $state;
                }
                function formateCountrySelection(state) {
                    if (!state.id) { return state.text; }
                    if (state.CountryCD != null) {
                        $("#hdnLICCountryCode").val(state.CountryCD.toLowerCase());
                    }   
                    if ($("#hdnLICCountryCode").val() != "") {
                        var $state = $('<div class="img-thumbnail flag flag-icon-background flag-icon-' + $("#hdnLICCountryCode").val().toLowerCase() + '" title="' + $("#hdnLICCountryCode").val() + '" id="' + $("#hdnLICCountryCode").val() + '"><span style="margin-left: 25px;display: inline-block;height: 25px;overflow: hidden;width:74px;">' + state.text + '</span></div>');
                        return $state;
                    }
                    else
                        return state.text;
                }
                function formateCountrySelectionadd(state) {
                    if (!state.id) { return state.text; }
                    if (state.CountryCD != null) {
                        $("#hdnADDLICCountryCode").val(state.CountryCD.toLowerCase());
                    }
                    if ($("#hdnADDLICCountryCode").val() != "") {
                        var $state = $('<div class="img-thumbnail flag flag-icon-background flag-icon-' + $("#hdnADDLICCountryCode").val().toLowerCase() + '" title="' + $("#hdnADDLICCountryCode").val() + '" id="' + $("#hdnADDLICCountryCode").val() + '"><span style="margin-left: 25px;display: inline-block;height: 25px;overflow: hidden;width:74px;">' + state.text + '</span></div>');
                        return $state;
                    }
                    else
                        return state.text;
                }
            }
    </script>
    </telerik:RadCodeBlock>
    <table width="100%" cellpadding="0" cellspacing="0" runat="server" id="table1">
        <tr style="display: none;">
            <td>
                <%--<uc:datepicker id="ucDatePicker" runat="server"></uc:datepicker>--%>
            </td>
        </tr>
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Roster</span><%--<span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:openWin('RadCrewRosterReportsPopup');return false;"
                            title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" OnClientClick="javascript:openWin('RadExportData');return false;"
                            title="Export Report" class="save-icon"></asp:LinkButton>
                            <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" Style="display: none;" />
                        <a href="#" class="help-icon" title="Help"></a></span>--%>
                    <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('RadCrewRosterReportsPopup','PDF','RptDBCrewRoster');return false;"
                            class="search-icon" title="Preview Report" />
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" OnClientClick="javascript:ShowReports('RadCrewRosterReportsPopup','EXPORT','RptDBCrewRosterExport');return false;"
                            class="save-icon" title="Export Report" />
                        <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                            Style="display: none;" />
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnCrewRosterRpt" runat="server" />
                        <asp:HiddenField ID="hdnCrewChecklistRpt" runat="server" />
                        <asp:HiddenField ID="hdnInactiveChecklistRpt" runat="server" />
                        <asp:HiddenField ID="hdnCrewCurrencyRpt" runat="server" />
                        <asp:HiddenField ID="hdnCrewTypeRatingsRpt" runat="server" />
                        <a href="../../Help/ViewHelp.aspx?Screen=CrewRosterHelp" target="_blank" class="help-icon"
                            title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" runat="server" clientidmode="Static" id="hdnLICCountryCode" />
    <input type="hidden" runat="server" clientidmode="Static" id="hdnADDLICCountryCode" />
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" MinDate="01/01/1900"
        MaxDate="12/31/2100" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="divExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCountryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="CountryPopupClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
		    <telerik:RadWindow ID="RadGreenNationalityMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseGreenNationalityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Select Multiple Crew" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewNamePopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadLicCountryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="LicCountryPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadAddLicCountryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="AddLicCountryPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCountryOfBirthPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="AddCountryOfBirthPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCrewGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="CrewGroupPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewGroupPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCodePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFilterClientPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCodeFilterPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCrewRosterReportsPopup" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Title="Crew Roster Reports" Modal="true" OnClientClose="OnClientReportPopup" Width="396px" Height="212px"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterReportsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCitizenPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="CitizenPopupClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadVisaCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientVisaCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadDeptPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="DeptPopupClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadAddlInfo" runat="server" OnClientResizeEnd="GetDimensions" ShowContentDuringLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>

            <telerik:RadWindow ID="RadAircraftTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCrewCheckListPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadAicraftType" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Aircraft Type" OnClientClose="AicraftTypePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/CrewRosterAircraftTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadAicraftTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Aircraft Type" OnClientClose="SingleAicraftTypePopupClose" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadChecklistPopUp" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Title="Select Checklist Settings & Crew Members"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterChecklistSettings.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" AutoSize="true" CssClass="ExportWindow"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printImage();return false;" />
                                    </div>
                                    <asp:Image ID="ImgPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <div id="divExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlFilterForm" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_5">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="head-sub-menu" id="table2" runat="server">
                <tr>
                    <td class="tdLabel100">
                        <asp:CheckBox ID="chkActiveOnly" runat="server" Checked="false" Text="Active Only"
                            AutoPostBack="true" OnCheckedChanged="ActiveOnly_CheckedChanged" />
                    </td>
                    <td class="tdLabel120">
                        <asp:CheckBox ID="chkHomeBaseOnly" runat="server" Checked="false" Text="Home Base Only"
                            AutoPostBack="true" OnCheckedChanged="HomeBaseOnly_CheckedChanged" />
                    </td>
                    <td class="tdLabel90">
                        <asp:CheckBox ID="chkFixedWingOnly" runat="server" Checked="false" Text="Fixed Wing"
                            AutoPostBack="true" OnCheckedChanged="FixedWingOnly_CheckedChanged" />
                    </td>
                    <td class="tdLabel100">
                        <asp:CheckBox ID="chkRotaryWingOnly" runat="server" Checked="false" Text="Rotary Wing"
                            AutoPostBack="true" OnCheckedChanged="RotaryWing_CheckedChanged" />
                    </td>
                    <td>
                        <table id="Table3" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:Label runat="server" ID="lbClientCode">Client Code :</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbClientCodeFilter" runat="server" MaxLength="5" ValidationGroup="Save"
                                        OnTextChanged="ClientCodeFilter_TextChanged" AutoPostBack="true" CssClass="text50"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnClientCodeFilter" OnClientClick="javascript:openWin('RadFilterClientPopup');return false;"
                                        CssClass="browse-button" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label runat="server" ID="lbClientCodeFilter" Text="Invalid ClientCode" Visible="false"
                                        CssClass="alert-text"></asp:Label>
                                    <asp:CustomValidator ID="cvClientCodeFilter" runat="server" ControlToValidate="tbClientCodeFilter"
                                        ErrorMessage="Invalid ClientCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table id="Exception" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:Label runat="server" ID="Label2">Crew Group :</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbCrewGroupFilter" CssClass="text50" runat="server" OnTextChanged="CrewGroupFilter_TextChanged"
                                        AutoPostBack="true" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCrewGroupFilter" OnClientClick="javascript:openWin('RadCrewGroupPopup');return false;"
                                        CssClass="browse-button" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label runat="server" ID="lbCrewGroupCodeFilter" Text="Invalid Crew Group" Visible="false"
                                        CssClass="alert-text"></asp:Label>
                                    <asp:CustomValidator ID="cvCrewGroupCodeFilter" runat="server" ControlToValidate="tbCrewGroupFilter"
                                        ErrorMessage="Invalid Crew Group" Display="Dynamic" CssClass="alert-text"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_5">
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <telerik:RadGrid ID="dgCrewRoster" runat="server" AllowSorting="true" CssClass="CrewRoster_table" OnNeedDataSource="CrewRoster_BindData"
                Height="341px" OnItemCreated="CrewRoster_ItemCreated" OnItemCommand="CrewRoster_ItemCommand"
                OnPreRender="CrewRoster_PreRender" OnUpdateCommand="CrewRoster_UpdateCommand"
                OnInsertCommand="CrewRoster_InsertCommand" OnPageIndexChanged="CrewRoster_PageIndexChanged"
                OnDeleteCommand="CrewRoster_DeleteCommand" OnSelectedIndexChanged="CrewRoster_SelectedIndexChanged"
                OnItemDataBound="CrewRoster_ItemDataBound" AutoGenerateColumns="false" PageSize="10"
                AllowPaging="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"> 
                <MasterTableView DataKeyNames="CustomerID,CrewID,CrewCD,ClientCD,HomeBaseID,HomeBaseCD,LastUpdUID,LastUpdTS"  ClientDataKeyNames ="CrewID"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="CrewCD" HeaderText="Crew Code" CurrentFilterFunction="StartsWith"
                            FilterControlWidth="40px" HeaderStyle-Width="60px" AllowSorting="true" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CrewName" HeaderText="Crew Name" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="135px" FilterControlWidth="115px" AllowSorting="true" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BusinessPhone" HeaderText="Business Phone" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="100px" FilterControlWidth="80px" AutoPostBackOnFilter="false" FilterDelay="500"
                            ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CellPhoneNum" HeaderText="Business Cell Phone"
                            HeaderStyle-Width="100px" FilterControlWidth="80px" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="EmailAddress" HeaderText="Business<br />E-mail"
                            CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px" FilterControlWidth="80px"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BusinessFax" HeaderText="Business Fax" HeaderStyle-Width="80px"
                            FilterControlWidth="60px" CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="60px" AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="40px"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsStatus" HeaderText="Active" AutoPostBackOnFilter="true"
                            AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                            HeaderStyle-Width="50px">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn DataField="CrewID" HeaderText="CrewID" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CustomerID" HeaderText="CustomerID" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="LastUpdUID" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="LastUpdTS" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CrewCD" HeaderText="CrewCD" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ClientCD" HeaderText="ClientCD" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <%--<telerik:GridBoundColumn DataField="NationalityCD" HeaderText="NationalityCD" CurrentFilterFunction="StartsWith"
                            Visible="false">
                        </telerik:GridBoundColumn>--%>
                        <%--<telerik:GridBoundColumn DataField="ResidenceCountryCD" HeaderText="ResidenceCountryCD"
                            CurrentFilterFunction="StartsWith" Visible="false">
                        </telerik:GridBoundColumn>--%>
                        <%--<telerik:GridBoundColumn DataField="CitizenshipCD" HeaderText="CitizenshipCD" CurrentFilterFunction="StartsWith"
                            Visible="false">
                        </telerik:GridBoundColumn>--%>
                        <%--<telerik:GridBoundColumn DataField="LicenseCountry1" HeaderText="LicenseCountry1"
                            CurrentFilterFunction="StartsWith" Visible="false">
                        </telerik:GridBoundColumn>--%>
                        <%--<telerik:GridBoundColumn DataField="LicenseCountry2" HeaderText="LicenseCountry2"
                            CurrentFilterFunction="StartsWith" Visible="false">
                        </telerik:GridBoundColumn>--%>
                        <%--<telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="DepartmentCD" CurrentFilterFunction="StartsWith"
                            Visible="false">
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false" DataField="CheckList"
                            HeaderStyle-Font-Bold="true" HeaderStyle-Width="65px" ShowFilterIcon="false" FilterDelay="500"
                            HeaderText="Checklist" HeaderStyle-HorizontalAlign="Center" AllowFiltering="false">
                            <ItemTemplate>
                                <center>
                                    <asp:Label ID="lbCheckList" runat="server" Text='<%# Eval("CheckList") %>'></asp:Label>
                                </center>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddCrewRoster)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                Visible='<%# IsAuthorized("Permission.Database.EditCrewRoster")%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                                Visible='<%# IsAuthorized("Permission.Database.DeleteCrewRoster")%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.
                        </div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field
                        </div>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0" class="tblButtonArea">
                            <tr>
                                 <td>
                                    <asp:Button ID="btnAdditionalInformation" runat="server" CssClass="ui_nav" Text="Additional Information"
                                        OnClientClick="javascript:OnClientClick('AdditionalInformation');return false;" />
                                </td>
                                    <td>
                                    <asp:Button ID="btnImage" runat="server" CssClass="ui_nav" Text="Attachments" OnClientClick="javascript:OnClientClick('Image');return false;" />
                                </td>
                               
                                <td>
                                    <asp:Button ID="btnCurrency" runat="server" CssClass="ui_nav" Text="Currency" OnClientClick="javascript:OnClientClick('Currency');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnAdditionalNotes" runat="server" CssClass="ui_nav" Text="Additional Notes"
                                        OnClientClick="javascript:OnClientClick('AdditionalNotes');return false;" />
                                </td>
                            
                                <%--<td>
                                    <asp:Button ID="btnSaveChangesTop" runat="server" CssClass="button" Text="Save" OnClick="SaveChangesTop_Click"
                                        OnClientClick="javascript:CheckPassportChoice();" ValidationGroup="Save" />
                                </td>--%>
                                <td class="custom_radbutton">
                                    <telerik:RadButton ID="btnSaveChangesTop" runat="server" Text="Save" ValidationGroup="Save"
                                        OnClientClicking="CheckPassportChoice" OnClick="SaveChangesTop_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                        OnClick="CancelTop_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadTabStrip ID="tabCrewRoster" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                            MultiPageID="RadMultiPage1" CausesValidation="false" SelectedIndex="0" Align="Justify">
                            <Tabs>
                                <telerik:RadTab Text="Main Information">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Type Ratings">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Checklist">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Aircraft Assigned">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Passport/Visa">
                                </telerik:RadTab>
                                <telerik:RadTab Text="E-mail Preferences">
                                </telerik:RadTab>
                            </Tabs>
                        </telerik:RadTabStrip>
                    </td>
                </tr>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" Width="100%">
                            <telerik:RadPageView ID="MainInfo" runat="server">
                                <telerik:RadPanelBar ID="pnlMaintenance" Width="100%" ExpandAnimation-Type="None"
                                    CollapseAnimation-Type="None" runat="server" OnClientItemExpand="pnlMaintenanceExpand"
                                    OnClientItemCollapse="pnlMaintenanceCollapse">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text="&nbsp">
                                            <Items>
                                                <telerik:RadPanelItem>
                                                    <ContentTemplate>
                                                        <table width="100%" class="box1">
                                                            <tr>
                                                                <td align="left">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel100">
                                                                                <asp:CheckBox ID="chkActiveCrew" runat="server" Checked="true" />Active Crew
                                                                            </td>
                                                                            <td class="tdLabel110">
                                                                                <asp:CheckBox ID="chkFixedWing" runat="server" Checked="true" />Fixed Wing
                                                                            </td>
                                                                            <td class="tdLabel110">
                                                                                <asp:CheckBox ID="chkRoteryWing" runat="server" />Rotary Wing
                                                                            </td>
                                                                            <td class="tdLabel150">
                                                                                <asp:CheckBox ID="chkNoCalenderDisplay" runat="server" Checked="true" />No Calendar
                                                                                Display
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="tblspace_10">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <span class="mnd_text">Crew Code</span>
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbCrewCode" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                    Enabled="false" ValidationGroup="Save" CssClass="text60" AutoPostBack="true" OnTextChanged="tbCrewCode_TextChanged"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Home Base
                                                                            </td>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="tdLabel180">
                                                                                            <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" ValidationGroup="Save"
                                                                                                OnTextChanged="HomeBase_TextChanged" onBlur="return RemoveSpecialChars(this)"
                                                                                                AutoPostBack="true" CssClass="text60"></asp:TextBox>
                                                                                            <asp:Button ID="btnHomeBase" OnClientClick="javascript:openWin('radAirportPopup');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel250"></td>
                                                                            <td class="tdLabel140"></td>
                                                                            <td>
                                                                                <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                                    ErrorMessage="Invalid Home Base" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td class="tdLabel140">Client Code
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbClientCode" runat="server" MaxLength="5" ValidationGroup="Save"
                                                                                    OnTextChanged="ClientCode_TextChanged" onBlur="return RemoveSpecialChars(this)"
                                                                                    AutoPostBack="true" CssClass="text60"></asp:TextBox>
                                                                                <asp:Button ID="btnClientCode" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                                                    CssClass="browse-button" runat="server" />
                                                                            </td>
                                                                            <td class="tdLabel140">Department
                                                                            </td>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="tdLabel180">
                                                                                            <asp:TextBox ID="tbDepartment" MaxLength="8" AutoPostBack="true" OnTextChanged="Department_TextChanged"
                                                                                                runat="server" CssClass="text60"></asp:TextBox>
                                                                                            <asp:Button ID="btnDepartment" OnClientClick="javascript:openWin('RadDeptPopup');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel250">
                                                                                <asp:CustomValidator ID="cvClientCode" runat="server" ControlToValidate="tbClientCode"
                                                                                    ErrorMessage="Invalid ClientCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                            </td>
                                                                            <td class="tdLabel140"></td>
                                                                            <td>
                                                                                <asp:CustomValidator ID="cvDepartmentCode" runat="server" ControlToValidate="tbDepartment"
                                                                                    ErrorMessage="Invalid Department" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="tblspace_10">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <span>
                                                                                    <asp:Label ID="lbFirstName" Text="First Name" runat="server"></asp:Label></span>
                                                                            </td>
                                                                            <td class="tdLabel150">
                                                                                <asp:TextBox ID="tbFirstName" runat="server" MaxLength="20" CssClass="text120"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel100">
                                                                                <span>
                                                                                    <asp:Label ID="lbMiddleName" Text="Middle Name" runat="server"></asp:Label></span>
                                                                            </td>
                                                                            <td class="tdLabel140">
                                                                                <asp:TextBox ID="tbMiddleName" runat="server" MaxLength="20" CssClass="text100"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel80">
                                                                                <span>
                                                                                    <asp:Label ID="lbLastName" Text="Last Name" runat="server"></asp:Label></span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbLastName" OnTextChanged="LastName_TextChanged" AutoPostBack="true"
                                                                                    MaxLength="30" runat="server" CssClass="text120"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel150">
                                                                                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="tbFirstName"
                                                                                    ValidationGroup="Save" ErrorMessage="First Name is Required" Display="Dynamic"
                                                                                    CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td class="tdLabel100"></td>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel80"></td>
                                                                            <td>
                                                                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="tbLastName"
                                                                                    ValidationGroup="Save" ErrorMessage="Last Name is Required" Display="Dynamic"
                                                                                    CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel150">
                                                                                <asp:RegularExpressionValidator ID="revFirstName"  runat="server" ErrorMessage="Enter alphanumeric and [@'&#.{space}]."  Display="Dynamic" CssClass="alert-text" ControlToValidate="tbFirstName" ValidationGroup="Save" ValidationExpression="^[a-zA-Z0-9'@&#.\s]{1,20}$"  />
                                                                            </td>
                                                                            <td class="tdLabel100"></td>
                                                                            <td class="tdLabel140">
                                                                                <asp:RegularExpressionValidator ID="revMiddleName"  runat="server" ErrorMessage="Enter alphanumeric and [@'&#.{space}]." Display="Dynamic" CssClass="alert-text" ControlToValidate="tbMiddleName" ValidationGroup="Save" ValidationExpression="^[a-zA-Z0-9'@&#.\s]{0,20}$"  />
                                                                            </td>
                                                                            <td class="tdLabel80"></td>
                                                                            <td>
                                                                                <asp:RegularExpressionValidator ID="revLastName" runat="server" ErrorMessage="Enter alphanumeric and [@'&#.{space}]." Display="Dynamic" CssClass="alert-text" ControlToValidate="tbLastName" ValidationGroup="Save"  ValidationExpression="^[a-zA-Z0-9'@&#.\s]{1,20}$"  />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <asp:Button ID="btnCountry" Text="Crew Dupe" runat="server" OnClientClick="javascript:openDuplicates();return false;"></asp:Button>
                                                                            </td>
                                                                            <td class="tdLabel400">
                                                                                <asp:Label ID="lbWarningMessage" Visible="false" Text="Duplicate Crew Record May Exist."
                                                                                    runat="server" CssClass="alert-text"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel150"></td>
                                                                            <td class="tdLabel100"></td>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel80"></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbAddress1" Text="Address 1" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbAddress1" runat="server" MaxLength="40" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Address 2
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbAddress2" runat="server" MaxLength="40" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td class="tdLabel140">Address 3
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbAddress3" runat="server" MaxLength="40" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbCity" Text="City" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbCity" runat="server" MaxLength="25" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbState" Text="State/Province" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbStateProvince" runat="server" MaxLength="10" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbCountry" Text="Country" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbCountry" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                    OnTextChanged="Country_TextChanged" AutoPostBack="true" CssClass="text60"></asp:TextBox>
                                                                                <asp:Button ID="btnCountryPopup" OnClientClick="javascript:openWin('RadCountryPopup');return false;"
                                                                                    CssClass="browse-button" runat="server" />
                                                                            </td>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbPostal" Text="Postal" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbStateProvincePostal" runat="server" MaxLength="15" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140"></td>
                                                                            <td>
                                                                                <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbCountry"
                                                                                    ErrorMessage="Invalid Country" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbGender" Text="Gender" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:RadioButton ID="rbMale" runat="server" Text="Male" Checked="true" GroupName="gender" />
                                                                                <asp:RadioButton ID="rbFemale" runat="server" Text="Female" GroupName="gender" />
                                                                            </td>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbCitizenship" Text="Citizenship" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140">
                                                                                            <asp:TextBox ID="tbCitizenCode" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                OnTextChanged="CitizenCode_TextChanged" AutoPostBack="true" runat="server" CssClass="text60"></asp:TextBox>
                                                                                            <asp:Button ID="btnCitizenship" OnClientClick="javascript:openWin('RadCitizenPopup');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel250"></td>
                                                                            <td class="tdLabel140"></td>
                                                                            <td>
                                                                                <asp:CustomValidator ID="cvCitizenCode" runat="server" ControlToValidate="tbCitizenCode"
                                                                                    ErrorMessage="Invalid CitizenCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                <asp:Label ID="lbCitizenDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbDateOfBirth" Text="Date of Birth" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <%--<uc:DatePicker ID="ucDateOfBirth" runat="server"></uc:DatePicker>--%>
                                                                                <asp:TextBox ID="tbDateOfBirth" runat="server" AutoPostBack="true" OnTextChanged="Birthday_TextChanged"
                                                                                    CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                    onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbCityOfBirth" Text="City of Birth" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbCityBirth" MaxLength="30" runat="server" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbStateOfBirth" Text="State/Prov. of Birth" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbStateBirth" runat="server" MaxLength="10" CssClass="text80"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="lbCountryOfBirth" Text="Country of Birth" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140">
                                                                                            <asp:TextBox ID="tbCountryBirth" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                OnTextChanged="CountryBirth_TextChanged" AutoPostBack="true" CssClass="text60"></asp:TextBox>
                                                                                            <asp:Button ID="btnCountryOfBirth" OnClientClick="javascript:openWin('RadCountryOfBirthPopup');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel250"></td>
                                                                            <td class="tdLabel140"></td>
                                                                            <td>
                                                                                <asp:CustomValidator ID="cvCountryBirth" runat="server" ControlToValidate="tbCountryBirth"
                                                                                    ErrorMessage="Invalid Country" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="tblspace_10">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">Business Phone
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbBusinessPhone" runat="server" MaxLength="25" CssClass="text200"
                                                                                    onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Home Phone
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbHomePhone" runat="server" MaxLength="25" CssClass="text200" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td class="tdLabel140">Other Phone
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbOtherPhone" runat="server" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                    MaxLength="25" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Pager No.
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbPager" runat="server" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                    MaxLength="25" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td class="tdLabel140">Primary Mobile/Cell
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbMobilePhone" runat="server" MaxLength="25" CssClass="text200"
                                                                                    onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Secondary Mobile/Cell
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbSecondaryMobile" runat="server" MaxLength="25" CssClass="text200"
                                                                                    onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">Business E-mail
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbEmail" runat="server" MaxLength="200" ValidationGroup="Save" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Personal E-mail
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbPersonalEmail" runat="server" ValidationGroup="Save" MaxLength="250"
                                                                                    CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140"></td>
                                                                            <td class="tdLabel250">
                                                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="tbEmail"
                                                                                    ValidationGroup="Save" ErrorMessage="Invalid E-Mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                            <td class="tdLabel140"></td>
                                                                            <td>
                                                                                <asp:RegularExpressionValidator ID="revPersonalEmail" runat="server" ControlToValidate="tbPersonalEmail"
                                                                                    ValidationGroup="Save" ErrorMessage="Invalid E-Mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">Other E-mail
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbOtherEmail" runat="server" MaxLength="250" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td colspan="2">
                                                                                <asp:RegularExpressionValidator ID="revOtherEmail" runat="server" ControlToValidate="tbOtherEmail"
                                                                                    ValidationGroup="Save" ErrorMessage="Invalid E-Mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">Business Fax
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbBusinessFax" runat="server" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                    CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Home Fax
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbFax" runat="server" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                    CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="tblspace_10">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">SSN
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbSSN" runat="server" MaxLength="11" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Crew Type
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbCrewType" runat="server" MaxLength="25" CssClass="text200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">Hire Date
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <%-- <uc:DatePicker ID="ucHireDT" runat="server"></uc:DatePicker>--%>
                                                                                <asp:TextBox ID="tbHireDT" runat="server" CssClass="text200" AutoPostBack="true"
                                                                                    OnTextChanged="HireDT_TextChanged" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                    onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Termination Date
                                                                            </td>
                                                                            <td>
                                                                                <%--<uc:DatePicker ID="ucTermDT" runat="server" />--%>
                                                                                <asp:TextBox ID="tbTermDT" runat="server" AutoPostBack="true" CssClass="text200"
                                                                                    OnTextChanged="TermDT_TextChanged" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" onblur="parseDate(this, event); ValidateDate(this);"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">Emergency Contacts
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbEmergencyContact" runat="server" CssClass="text200" MaxLength="100"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Credit Card Info
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbCreditCardInfo" runat="server" CssClass="text200" MaxLength="100"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">Catering Preferences
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbCateringPreferences" CssClass="text200" runat="server" MaxLength="100"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">Hotel Preferences
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbHotelPreferences" CssClass="text200" runat="server" MaxLength="100"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="Anniversaies" Text="Anniversary" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbAnnivesaries" runat="server" CssClass="text200" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                    onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel140">
                                                                                <asp:Label ID="Birthday" Text="Date Of Birth" Style="display: none" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td class="tdLabel250">
                                                                                <asp:TextBox ID="tbBirthday" runat="server" CssClass="text200" Style="display: none"
                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onclick="showPopup(this, event);"
                                                                                    onfocus="showPopup(this, event);" MaxLength="10" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="tblspace_10">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td valign="top">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel99">
                                                                                                            <asp:Label CssClass="important-text" ID="lbLicNo" Text="Lic. No." runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td class="tdLabel150">
                                                                                                            <asp:TextBox ID="tbLicNo" runat="server" CssClass="text130" MaxLength="25"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td class="tdLabel90">Lic. Type
                                                                                                        </td>
                                                                                                        <td class="tdLabel150">
                                                                                                            <asp:TextBox ID="tbLicType" runat="server" MaxLength="30" CssClass="text130"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td class="tdLabel85">Exp. Date
                                                                                                        </td>
                                                                                                        <td class="tdLabel150">
                                                                                                            <asp:TextBox ID="tbLicExpDate" runat="server" CssClass="text100" onKeyPress="return fnAllowNumericAndChar(this, event"
                                                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                                                        </td>
                                                                                                        
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel99">City
                                                                                                        </td>
                                                                                                        <td class="tdLabel390">
                                                                                                            <asp:TextBox ID="tbLicCity" runat="server" MaxLength="30" CssClass="text370"></asp:TextBox>
                                                                                                        </td>
                                                                                            <td valign="top">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel93">
                                                                                                            <asp:Label CssClass="important-text" ID="lbLicCountry" Text="Country" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td class="tdLabel147">
                                                                                                            <asp:TextBox runat="server" CssClass="airCraftNationalityAutoComplete" ClientIDMode="Static" Style="width: 150px" ID="tbLicCountry" />
                                                                                                            <asp:HiddenField runat="server" ID="hdntbLicCountryId" ClientIDMode="Static" Value="0" />
                                                                                                            <asp:HiddenField runat="server" ID="hdntbLicCountryName"  ClientIDMode="Static"/>
<%--                                                                                                            <asp:TextBox ID="tbLicCountry" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                                OnTextChanged="LicCountry_TextChanged" AutoPostBack="true" CssClass="text100"></asp:TextBox>
                                                                                                            <asp:Button ID="btnLicCountry" OnClientClick="javascript:openWin('RadLicCountryPopup');return false;"
                                                                                                                CssClass="browse-button" runat="server" />--%>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="3">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                                </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel99">Add'l Lic.
                                                                                                        </td>
                                                                                                        <td class="tdLabel150">
                                                                                                            <asp:TextBox ID="tbAddLic" runat="server" CssClass="text130" MaxLength="25"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td class="tdLabel90">Lic. Type
                                                                                                        </td>
                                                                                                        <td class="tdLabel150">
                                                                                                            <asp:TextBox ID="tbAddLicType" MaxLength="30" runat="server" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                                 AutoPostBack="true" CssClass="text130"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td class="tdLabel85">Exp. Date
                                                                                                        </td>
                                                                                                        <td class="tdLabel150">
                                                                                                            <asp:TextBox ID="tbAddLicExpDate" runat="server" CssClass="text100" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                                                        </td>
                                                                                                        
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel99">City
                                                                                                        </td>
                                                                                                        <td class="tdLabel390">
                                                                                                            <asp:TextBox ID="tbAddLicCity" MaxLength="30" runat="server" CssClass="text370"></asp:TextBox>
                                                                                                        </td>
                                                                                            <td valign="top">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel93">Country
                                                                                                        </td>
                                                                                                        <td class="tdLabel147">
                                                                                                                <asp:TextBox runat="server" CssClass="airCraftNationalityAutoComplete" ClientIDMode="Static" Style="width: 150px" ID="tbAddLicCountry" />
                                                                                                                <asp:HiddenField runat="server" ID="hdntbAddLicCountryId" ClientIDMode="Static" Value="0" />
                                                                                                                <asp:HiddenField runat="server" ID="hdntbAddLicCountryName"  ClientIDMode="Static"/>
<%--                                                                                                            <asp:TextBox ID="tbAddLicCountry" MaxLength="3" runat="server" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                                OnTextChanged="AddLicCountry_TextChanged" AutoPostBack="true" CssClass="text100"></asp:TextBox>
                                                                                                            <asp:Button ID="btnAddLicCountry" OnClientClick="javascript:openWin('RadAddLicCountryPopup');return false;"
                                                                                                                CssClass="browse-button" runat="server" />--%>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="3" valign="top">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                                </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>Notes</legend>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea-710x40"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>
                                                                                        <asp:Label ID="lbAlienRegistrationCard" runat="server" Text="Permanent Residence Card"
                                                                                            ForeColor="Black"></asp:Label></legend>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="tdLabel140">INS A#
                                                                                            </td>
                                                                                            <td class="tdLabel240">
                                                                                                <asp:TextBox ID="tbINSANumber" runat="server" MaxLength="60"></asp:TextBox>
                                                                                            </td>
                                                                                            <td class="tdLabel140">Category
                                                                                            </td>
                                                                                            <td class="tdLabel240">
                                                                                                <asp:TextBox ID="tbCategory" runat="server" MaxLength="30"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdLabel140">Card Expiration
                                                                                            </td>
                                                                                            <td class="tdLabel240">
                                                                                                <%--<uc:DatePicker ID="ucCardExpires" runat="server"></uc:DatePicker>--%>
                                                                                                <asp:TextBox ID="tbCardExpires" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                                    onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                                            </td>
                                                                                            <td class="tdLabel140">Resident Since Year
                                                                                            </td>
                                                                                            <td class="tdLabel240">
                                                                                                <asp:TextBox ID="tbResidentSinceYear" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                                    onBlur="return RemoveSpecialChars(this);" MaxLength="4"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdLabel140"></td>
                                                                                            <td class="tdLabel240"></td>
                                                                                            <td class="tdLabel140"></td>
                                                                                            <td class="tdLabel240">
                                                                                                <asp:RangeValidator ID="rvYear" runat="server" ControlToValidate="tbResidentSinceYear"
                                                                                                    Type="Double" MinimumValue="1900" MaximumValue="2100" ValidationGroup="Save"
                                                                                                    CssClass="alert-text" ErrorMessage="Year should be 1900 to 2100" Display="Dynamic"></asp:RangeValidator>
                                                                                                <asp:CustomValidator ID="cvyear" runat="server" ErrorMessage="Resident Since Year should not be future year."
                                                                                                    ClientValidationFunction="fncResidentSinceYear" ValidationGroup="Save" CssClass="alert-text"></asp:CustomValidator>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <%-- <td class="tdLabel140" valign="top">
                                                                            Gender
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <asp:RadioButton ID="rbPaxMale" runat="server" Text="Male" GroupName="PaxGender"
                                                                                Checked="true" />
                                                                            <asp:RadioButton ID="rbPaxFemale" runat="server" Text="Female" GroupName="PaxGender" />
                                                                        </td>--%>
                                                                                            <td class="tdLabel140">Country of Birth
                                                                                            </td>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="tbGreenNationality" CssClass="text170" runat="server" MaxLength="3"
                                                                                                                onKeyPress="return fnAllowAlphaNumeric(this, event)" OnTextChanged="tbGreenNationality_OnTextChanged"
                                                                                                                AutoPostBack="true"></asp:TextBox>
                                                                                                            <asp:HiddenField ID="hdnGreenNationality" runat="server" />
                                                                                                            <asp:Button ID="btnGreenNationality" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadGreenNationalityMasterPopup');return false;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:CustomValidator ID="cvGreenNationality" runat="server" ControlToValidate="tbGreenNationality"
                                                                                                                ErrorMessage="Invalid Country Code" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                                                SetFocusOnError="true"></asp:CustomValidator>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                            <%--    <tr>
                                                                <td colspan="5">
                                                                    <fieldset>
                                                                        <legend>Photo</legend>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr valign="top">
                                                                                <td>
                                                                                    <asp:TextBox ID="tbPhoto" runat="server" CssClass="text150"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <asp:Button ID="btnBrowse" runat="server" CssClass="button" Text="Browse" />
                                                                                </td>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Image ID="imgLogo" runat="server" ImageUrl="~/App_Themes/Default/images/no-image.jpg"
                                                                                                    Width="50px" Height="50px" />
                                                                                            </td>
                                                                                            <td valign="top">
                                                                                                <asp:Button ID="btnLogoDelete" runat="server" CssClass="delete-button" Text="" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>--%>
                                                        </table>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="TypeRatings" runat="server" ValidationGroup="Save">
                                
                                 <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                     <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td align="right">
                                            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                                <tr>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnFlightExp" runat="server" Text="Display Current Flight Experience"
                                                            OnClick="FlightExp_Click" />
                                                    </td>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnUpdateFltExperience" runat="server" Text="Update Flight Experience"
                                                            ValidationGroup="Save" OnClick="UpdateFltExperience_Click" />
                                                    </td>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnUpdateAllFltExperience" runat="server" Text="Update All Flight Experience"
                                                            ValidationGroup="Save" OnClick="UpdateAllFltExperience_Click" />
                                                    </td>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnAddRating" runat="server" Text="Add Rating" ValidationGroup="Save"
                                                            OnClientClicking="ShowAircraftTypePopup" />
                                                    </td>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnDeleteRating" runat="server" Text="Delete Rating" ValidationGroup="Save"
                                                            OnClientClicking="ClickingDeleteTypeRating" OnClick="DeleteRating_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                                <table id="tblTypeRating" runat="server" width="100%" class="border-box">
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="tdLabel70">Type Code :
                                                    </td>
                                                    <td class="tdLabel65">
                                                        <asp:Label ID="lbTypeCode" runat="server" CssClass="input_no_bg"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel70">Description :
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbDescription" runat="server" CssClass="input_no_bg"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:CheckBox runat="server" ID="chkTRInactive" Text="Inactive" OnCheckedChanged="chkTRInactive_CheckedChanged"
                                                            AutoPostBack="true" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td width="18%">Qualified In Type As
                                                    </td>
                                                    <td width="82%">
                                                        <table class="border-box" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td class="tdLabel70">
                                                                                <asp:CheckBox ID="chkTRPIC91" runat="server" Text="PIC-91" AutoPostBack="true" OnCheckedChanged="TRPIC91_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel70">
                                                                                <asp:CheckBox ID="chkTRSIC91" runat="server" Text="SIC-91" AutoPostBack="true" OnCheckedChanged="TRSIC91_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel80">
                                                                                <asp:CheckBox ID="chkTREngineer" runat="server" Text="Engineer" AutoPostBack="true"
                                                                                    OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel100">
                                                                                <asp:CheckBox ID="chkTRAttendant91" runat="server" Text="Attendant-91" AutoPostBack="true"
                                                                                    OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkTRAirman" runat="server" Text="Check Airman" AutoPostBack="true"
                                                                                    OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td class="tdLabel70">
                                                                                <asp:CheckBox ID="chkTRPIC135" runat="server" Text="PIC-135" AutoPostBack="true"
                                                                                    OnCheckedChanged="TRPIC135_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel70">
                                                                                <asp:CheckBox ID="chkTRSIC135" runat="server" Text="SIC-135" AutoPostBack="true"
                                                                                    OnCheckedChanged="TRSIC135_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel80">
                                                                                <asp:CheckBox ID="chkTRInstructor" runat="server" Text="Instructor" AutoPostBack="true"
                                                                                    OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel100">
                                                                                <asp:CheckBox ID="chkTRAttendant135" runat="server" Text="Attendant-135" AutoPostBack="true"
                                                                                    OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkTRAttendant" runat="server" Text="Check Attendant" AutoPostBack="true"
                                                                                    OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tdLabel70">
                                                                                <asp:CheckBox ID="chkPIC121" runat="server" Text="PIC-121" AutoPostBack="true" OnCheckedChanged="TRPIC121_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel70">
                                                                                <asp:CheckBox ID="chkSIC121" runat="server" Text="SIC-121" AutoPostBack="true" OnCheckedChanged="TRSIC121_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel80">
                                                                                <asp:CheckBox ID="chkPIC125" runat="server" Text="PIC-125" AutoPostBack="true" OnCheckedChanged="TRPIC125_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel100">
                                                                                <asp:CheckBox ID="chkSIC125" runat="server" Text="SIC-125" AutoPostBack="true" OnCheckedChanged="TRSIC125_CheckedChanged" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkAttendent121" runat="server" Text="Attendant-121" AutoPostBack="true"
                                                                                    OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkAttendent125" runat="server" Text="Attendant-125" AutoPostBack="true"
                                                                                    OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="divModify" runat="server">
                                                <table class="border-box" width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="4" align="center">
                                                            <asp:Panel ID="pnlBeginning" runat="server" ValidationGroup="Save">
                                                                <table>
                                                                    <tr>
                                                                        <td>Beginning Flight Experience: As Of Date:
                                                                        </td>
                                                                        <td>
                                                                            <%--<uc:datepicker id="ucAsOfDt" runat="server"></uc:datepicker>--%>
                                                                            <asp:TextBox ID="tbAsOfDt" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" OnTextChanged="Typerating_TextChanged"
                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                onblur="parseDate(this, event); ValidateDate(this);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Total Hrs.</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>In Type
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTotalHrsInType" runat="server" CssClass="tdLabel70" onfocus="this.select();"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvTotalHrsInType" runat="server" ControlToValidate="tbTotalHrsInType"
                                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Day
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTotalHrsDay" runat="server" onfocus="this.select();" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revTotalHrsDay" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbTotalHrsDay" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Night
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTotalHrsNight" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revTotalHrsNight" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbTotalHrsNight" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Instrument
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTotalHrsInstrument" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revTotalHrsInstrument" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbTotalHrsInstrument" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>PIC Hrs.</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>In Type
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPICHrsInType" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%-- <asp:RegularExpressionValidator ID="revPICHrsInType" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbPICHrsInType" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Day
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPICHrsDay" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revPICHrsDay" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbPICHrsDay" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Night
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPICHrsNight" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%-- <asp:RegularExpressionValidator ID="revPICHrsNight" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbPICHrsNight" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Instrument
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPICHrsInstrument" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revPICHrsInstrument" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbPICHrsInstrument" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>SIC Hrs.</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>In Type
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSICInTypeHrs" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revInTypeSICHrs" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbSICInTypeHrs" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Day
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSICHrsDay" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revSICHrsDay" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbSICHrsDay" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Night
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSICHrsNight" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revSICHrsNight" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbSICHrsNight" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Instrument
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSICHrsInstrument" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revSICHrsInstrument" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbSICHrsInstrument" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Other Hrs.</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>Simulator
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherHrsSimulator" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revOtherHrsSimulator" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbOtherHrsSimulator" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Flt. Engineer
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherHrsFltEngineer" onfocus="this.select();" runat="server" CssClass="tdLabel70"
                                                                                            OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revOtherHrsFltEngineer" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbOtherHrsFltEngineer" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Flt. Instructor
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherHrsFltInstructor" onfocus="this.select();" runat="server"
                                                                                            CssClass="tdLabel70" OnTextChanged="Typerating_TextChanged" AutoPostBack="true"
                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revOtherHrsFltInstructor" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbOtherHrsFltInstructor" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Flt. Attendant
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherHrsFltAttendant" onfocus="this.select();" CssClass="tdLabel70"
                                                                                            runat="server" OnTextChanged="Typerating_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RegularExpressionValidator ID="revOtherHrsFltAttendant" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbOtherHrsFltAttendant" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="divDisplay" visible="false" runat="server">
                                                <table class="border-box" width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="4" align="center">
                                                            <asp:Panel ID="pnlCurrent" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td>Current Flight Experience:
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Total Hrs.</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>In Type
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbTotalHrsInTypedisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Day
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbTotalHrsDayDisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Night
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbTotalHrsNightDisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Instrument
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbTotalHrsInstrumentDisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>PIC Hrs.</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>In Type
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbPICHrsInTypedisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Day
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbPICHrsDaydisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Night
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbPICHrsNightdisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Instrument
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbPICHrsInstrumentdiaplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>SIC Hrs.</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>In Type
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbSICInTypeHrsdisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Day
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbSICHrsDaydisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Night
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbSICHrsNightdisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Instrument
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbSICHrsInstrumentdisplay" runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Other Hrs.</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>Simulator
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbOtherHrsSimulatordisplay" runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Flt. Engineer
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbOtherHrsFltEngineerdisplay" runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Flt. Instructor
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbOtherHrsFltInstructordisplay" runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Flt. Attendant
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ReadOnly="true" CssClass="tdLabel70 charter_readonly_textbox" ID="tbOtherHrsFltAttendantdisplay"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <telerik:RadGrid ID="dgTypeRating" runat="server" EnableAJAX="True" AutoGenerateColumns="false"
                                                ShowFooter="false" PagerStyle-AlwaysVisible="true" AllowPaging="false" OnSelectedIndexChanged="TypeRating_SelectedIndexChanged"
                                                OnItemDataBound="TypeRating_ItemDataBound" Width="768px" Height="341px">
                                                <MasterTableView ShowFooter="false" AllowPaging="false" DataKeyNames="CustomerID,CrewID,CrewRatingID,AircraftTypeCD,AircraftTypeID,CrewRatingDescription,IsPilotinCommand,IsSecondInCommand,IsQualInType135PIC
                                        ,IsQualInType135SIC,IsEngineer,IsInstructor,IsCheckAttendant,IsAttendantFAR91,IsAttendantFAR135,IsCheckAirman,IsInActive
                                        ,AsOfDT,TimeInType,Day1,Night1,Instrument,Simulator,FlightEngineer,FlightInstructor,FlightAttendant,ClientCD,ClientID
                                        ,SecondInCommandDay,SecondInCommandNight,SecondInCommandInstr,TotalDayHrs,TotalNightHrs,TotalINSTHrs
                                        ,PilotInCommandTypeHrs,TPilotinCommandDayHrs,TPilotInCommandNightHrs,TPilotInCommandINSTHrs,SecondInCommandTypeHrs
                                        ,SecondInCommandDayHrs,SecondInCommandNightHrs,SecondInCommandINSTHrs,OtherSimHrs,OtherFlightEngHrs,OtherFlightInstrutorHrs,OtherFlightAttdHrs,IsPIC121,IsSIC121,IsPIC125,IsSIC125,IsAttendant121,IsAttendant125,IsPIC91,IsSIC91"
                                                    CommandItemDisplay="None" AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                                    <Columns>
                                                        <telerik:GridTemplateColumn HeaderText="Code" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                            ShowFilterIcon="false" UniqueName="AircraftTypeCD" AllowFiltering="false" DataField="AircraftTypeCD"
                                                            FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbAircraftTypeCD" runat="server" Text='<%# Eval("AircraftTypeCD") %>'
                                                                    CssClass="tdtext100"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Description" CurrentFilterFunction="Contains"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="CrewRatingDescription"
                                                            AllowFiltering="false" DataField="CrewRatingDescription" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbCrewRatingDescription" runat="server" Text='<%# Eval("CrewRatingDescription") %>'
                                                                    CssClass="tdtext100"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="PIC" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                            ShowFilterIcon="false" AllowFiltering="false" UniqueName="IsPilotinCommand" DataField="IsPilotinCommand">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkIsPilotinCommand" runat="server" Enabled="false" Checked='<%# Eval("IsPilotinCommand") != null ? Eval("IsPilotinCommand") : false %>' />
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="SIC" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                            ShowFilterIcon="false" AllowFiltering="false" UniqueName="IsSecondInCommand" DataField="IsSecondInCommand">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkIsSecondInCommand" runat="server" Enabled="false" Checked='<%# Eval("IsSecondInCommand") != null ? Eval("IsSecondInCommand") : false %>' />
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Updt. As Of" CurrentFilterFunction="Contains"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="TotalUpdateAsOfDT" DataField="TotalUpdateAsOfDT"
                                                            AllowFiltering="false" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbTotalUpdateAsOfDT" runat="server" Text='<%# Eval("TotalUpdateAsOfDT") %>'
                                                                    CssClass="tdtext100"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Tot. Time In" CurrentFilterFunction="Contains"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="TotalTimeInTypeHrs" DataField="TotalTimeInTypeHrs"
                                                            AllowFiltering="false" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbTotalTimeInTypeHrs" Text='<%# Eval("TotalTimeInTypeHrs") %>' runat="server"
                                                                    CssClass="tdtext60"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Tot. Day" CurrentFilterFunction="Contains" DataField="TotalDayHrs"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="TotalDayHrs" AllowFiltering="false"
                                                            FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbTotalDayHrs" Text='<%# Eval("TotalDayHrs") %>' runat="server" CssClass="tdtext60"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Tot. Night" CurrentFilterFunction="Contains" DataField="TotalNightHrs"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="TotalNightHrs" FilterDelay="500"
                                                            AllowFiltering="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbTotalNightHrs" Text='<%# Eval("TotalNightHrs") %>' runat="server"
                                                                    CssClass="tdtext60"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Tot. Instr" CurrentFilterFunction="Contains" DataField="TotalINSTHrs"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="TotalINSTHrs" FilterDelay="500"
                                                            AllowFiltering="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbTotalINSTHrs" Text='<%# Eval("TotalINSTHrs") %>' runat="server"
                                                                    CssClass="tdtext60"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Tot Req Hrs" CurrentFilterFunction="Contains"
                                                            Display="false" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="HiddenCheckboxValues">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkTRPIC91" runat="server" Checked='<%# Eval("IsPIC91") != null ? Eval("IsPIC91") : false %>' />
                                                                <asp:CheckBox ID="chkTRSIC91" runat="server" Checked='<%# Eval("IsSIC91") != null ? Eval("IsSIC91") : false %>' />
                                                                <asp:CheckBox ID="chkTRPIC135" runat="server" Checked='<%# Eval("IsQualInType135PIC") != null ? Eval("IsQualInType135PIC") : false %>' />
                                                                <asp:CheckBox ID="chkTRSIC135" runat="server" Checked='<%# Eval("IsQualInType135SIC") != null ? Eval("IsQualInType135SIC") : false %>' />
                                                                <asp:CheckBox ID="chkTREngineer" runat="server" Checked='<%# Eval("IsEngineer") != null ? Eval("IsEngineer") : false %>' />
                                                                <asp:CheckBox ID="chkTRInstructor" runat="server" Checked='<%# Eval("IsInstructor") != null ? Eval("IsInstructor") : false %>' />
                                                                <asp:CheckBox ID="chkTRAttendant" runat="server" Checked='<%# Eval("IsCheckAttendant") != null ? Eval("IsCheckAttendant") : false %>' />
                                                                <asp:CheckBox ID="chkTRAttendant91" runat="server" Checked='<%# Eval("IsAttendantFAR91") != null ? Eval("IsAttendantFAR91") : false %>' />
                                                                <asp:CheckBox ID="chkTRAttendant135" runat="server" Checked='<%# Eval("IsAttendantFAR135") != null ? Eval("IsAttendantFAR135") : false %>' />
                                                                <asp:CheckBox ID="chkTRAirman" runat="server" Checked='<%# Eval("IsCheckAirman") != null ? Eval("IsCheckAirman") : false %>' />
                                                                <asp:CheckBox ID="chkTRInactive" runat="server" Checked='<%# Eval("IsInActive") != null ? Eval("IsInActive") : false %>' />
                                                                <asp:CheckBox ID="chkPIC121" runat="server" Checked='<%# Eval("IsPIC121") != null ? Eval("IsPIC121") : false %>' />
                                                                <asp:CheckBox ID="chkSIC121" runat="server" Checked='<%# Eval("IsSIC121") != null ? Eval("IsSIC121") : false %>' />
                                                                <asp:CheckBox ID="chkPIC125" runat="server" Checked='<%# Eval("IsPIC125") != null ? Eval("IsPIC125") : false %>' />
                                                                <asp:CheckBox ID="chkSIC125" runat="server" Checked='<%# Eval("IsSIC125") != null ? Eval("IsSIC125") : false %>' />
                                                                <asp:CheckBox ID="chkAttendent121" runat="server" Checked='<%# Eval("IsAttendant121") != null ? Eval("IsAttendant121") : false %>' />
                                                                <asp:CheckBox ID="chkAttendent125" runat="server" Checked='<%# Eval("IsAttendant125") != null ? Eval("IsAttendant125") : false %>' />
                                                                <asp:Label ID="lblTotalTimeInTypeHrs" runat="server" Text='<%# Eval("TimeInType") %>'></asp:Label>
                                                                <asp:Label ID="lblTotalDayHrs" runat="server" Text='<%# Eval("Day1") %>'></asp:Label>
                                                                <asp:Label ID="lblTotalNightHrs" runat="server" Text='<%# Eval("Night1") %>'></asp:Label>
                                                                <asp:Label ID="lblTotalINSTHrs" runat="server" Text='<%# Eval("Instrument") %>'></asp:Label>
                                                                <asp:Label ID="lbPICHrsInType" runat="server" Text='<%# Eval("PilotInCommandTypeHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbPICHrsDay" runat="server" Text='<%# Eval("TPilotinCommandDayHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbPICHrsNight" runat="server" Text='<%# Eval("TPilotInCommandNightHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbPICHrsInstrument" runat="server" Text='<%# Eval("TPilotInCommandINSTHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbSICInTypeHrs" runat="server" Text='<%# Eval("SecondInCommandTypeHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbSICHrsDay" runat="server" Text='<%# Eval("SecondInCommandDayHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbSICHrsNight" runat="server" Text='<%# Eval("SecondInCommandNightHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbSICHrsInstrument" runat="server" Text='<%# Eval("SecondInCommandINSTHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbOtherHrsSimulator" runat="server" Text='<%# Eval("OtherSimHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbOtherHrsFltEngineer" runat="server" Text='<%# Eval("OtherFlightEngHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbOtherHrsFltInstructor" runat="server" Text='<%# Eval("OtherFlightInstrutorHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbOtherHrsFltAttendant" runat="server" Text='<%# Eval("OtherFlightAttdHrs") %>'></asp:Label>
                                                                <asp:Label ID="lbAsOfDt" runat="server" Text='<%# Eval("asofdt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                                <GroupingSettings CaseSensitive="false" />
                                            </telerik:RadGrid>
                                        </td>
                                    </tr>
                                </table>
                           
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="CheckList" runat="server">
                                <table id="tblCrewCheckList" runat="server" class="border-box" width="100%">
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="tdLabel60">Code :
                                                    </td>
                                                    <td class="tdLabel70">
                                                        <asp:Label ID="lbCheckListCode" runat="server" CssClass="text60"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel700">Description :
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbCheckListDescription" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:CheckBox ID="chkDisplayInactiveChecklist" AutoPostBack="true" OnCheckedChanged="chkDisplayInactiveChecklist_CheckedChanged"
                                                            runat="server" Text="Active Only Checklist" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" class="border-box">
                                                <tr>
                                                    <td valign="top">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel90">Original Date
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbOriginalDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                    OnTextChanged="OriginalDate_TextChanged" AutoPostBack="true" onclick="showPopup(this, event);"
                                                                                    onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel90">
                                                                                <asp:Button ID="btnChgBaseDate" runat="server" Text="Chg. Base Date" CssClass="button_small_none"
                                                                                    OnClick="ChgBaseDate_Click" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbChgBaseDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" OnTextChanged="ChgBaseDate_TextChanged"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" AutoPostBack="true"
                                                                                    onblur="parseDate(this, event); ValidateDate(this);" MaxLength="10"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel90">Frequency
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="radFreq" runat="server" OnSelectedIndexChanged="radFreq_SelectedIndexChanged"
                                                                                    RepeatDirection="Horizontal" AutoPostBack="true">
                                                                                    <asp:ListItem Selected="True" Value="1" Text="Months"></asp:ListItem>
                                                                                    <asp:ListItem Value="2" Text="Days"></asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tdLabel90"></td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbFreq" runat="server" CssClass="text70" MaxLength="3" AutoPostBack="true"
                                                                                    onKeyPress="return fnAllowNumeric(this, event)" OnTextChanged="Freq_TextChanged"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel90">Previous
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbPrevious" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" onclick="showPopup(this, event);"
                                                                                    onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                    OnTextChanged="Previous_TextChanged" MaxLength="10"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel90">Due Next
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbDueNext" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                    AutoPostBack="true" OnTextChanged="DueNext_TextChanged" onclick="showPopup(this, event);"
                                                                                    onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td valign="top">
                                                        <table>
                                                            <tr>
                                                                <td>Alert Date
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbAlertDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                        AutoPostBack="true" OnTextChanged="AlertDate_TextChanged" onclick="showPopup(this, event);"
                                                                        onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                        onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Alert Days
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbAlertDays" runat="server" CssClass="text70" AutoPostBack="true"
                                                                        onBlur="return allnumeric(this, event)" onKeyPress="return fnAllowNumericAndChar(this,event,'-')"
                                                                        MaxLength="3" OnTextChanged="AlertDays_TextChanged">
                                                                    </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Grace Date
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbGraceDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                        AutoPostBack="true" OnTextChanged="GraceDate_TextChanged" onclick="showPopup(this, event);"
                                                                        onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                        onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Grace Days
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbGraceDays" runat="server" CssClass="text70" AutoPostBack="true"
                                                                        onBlur="return allnumeric(this, event)" onKeyPress="return fnAllowNumericAndChar(this,event,'-')"
                                                                        MaxLength="3" OnTextChanged="GraceDays_TextChanged">
                                                                    </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Aircraft Type
                                                                </td>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="tbAircraftType" runat="server" AutoPostBack="true" CssClass="text70"
                                                                                    MaxLength="3" OnTextChanged="AircraftType_TextChanged">
                                                                                </asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="btnAircraftType" OnClientClick="javascript:openWin('RadAicraftTypePopup');return false;"
                                                                                    CssClass="browse-button" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:CustomValidator ID="cvAssociateTypeCode" runat="server" ControlToValidate="tbAircraftType"
                                                                                    ErrorMessage="Invalid Aircraft Type Code." Display="Dynamic" CssClass="alert-text"
                                                                                    SetFocusOnError="true" ValidationGroup="Save"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Req. Flight Hrs.
                                                                </td>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="tbTotalReqFlightHrs" runat="server" MaxLength="6" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"
                                                                                    onfocus="this.select();" AutoPostBack="true" OnTextChanged="TotalReqFlightHrs_TextChanged"
                                                                                    CssClass="text70"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <%--<asp:RegularExpressionValidator ID="revTotalReqFlightHrs" runat="server" Display="Dynamic"
                                                                                    ErrorMessage="Invalid Format" ControlToValidate="tbTotalReqFlightHrs" CssClass="alert-text"
                                                                                    ValidationExpression="^[0-9]{0,5}(\.[0-9]{1})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td valign="top">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkPIC91" runat="server" Text="PIC-91" AutoPostBack="true" OnCheckedChanged="ChecklistchkPIC91_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkPIC135" runat="server" Text="PIC-135" AutoPostBack="true" OnCheckedChanged="ChecklistchkPIC135_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkSIC91" runat="server" Text="SIC-91" AutoPostBack="true" OnCheckedChanged="ChecklistchkSIC91_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkSIC135" runat="server" Text="SIC-135" AutoPostBack="true" OnCheckedChanged="ChecklistchkSIC135_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkInactive" runat="server" Text="Inactive" AutoPostBack="true"
                                                                        OnCheckedChanged="ChecklistchkInactive_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkSetToEndOfMonth" runat="server" Text="Set To End Of Month" AutoPostBack="true"
                                                                        OnCheckedChanged="SetToEndOfMonth_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkSetToNextOfMonth" runat="server" Text="Set To 1st Of Next Month"
                                                                        AutoPostBack="true" OnCheckedChanged="SetToNextOfMonth_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkSetToEndOfCalenderYear" runat="server" Text="Set To End Of calendar Year"
                                                                        AutoPostBack="true" OnCheckedChanged="SetToEndOfCalenderYear_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="ckhDisableDateCalculation" runat="server" Text="Disable Date Calculation"
                                                                        AutoPostBack="true" OnCheckedChanged="ckhDisableDateCalculation_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                <asp:CheckBox ID="chkOneTimeEvent" runat="server" Text="One Time Event" OnCheckedChanged="OneTimeEvent_CheckChanged"
                                                                                    AutoPostBack="true" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkCompleted" runat="server" Enabled="false" Text="Completed" OnCheckedChanged="ChecklistchkCompleted_CheckChanged"
                                                                                    AutoPostBack="true" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkNonConflictedEvent" runat="server" Text="Non-Conflicted Event"
                                                                        AutoPostBack="true" OnCheckedChanged="ChecklistchkNonConflictedEvent_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkRemoveFromCrewRosterRept" runat="server" Text="Remove From Crew Roster Rept."
                                                                        AutoPostBack="true" OnCheckedChanged="ChecklistchkRemoveFromCrewRosterRept_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkRemoveFromChecklistRept" runat="server" Text="Remove From Checklist Rept."
                                                                        AutoPostBack="true" OnCheckedChanged="ChecklistchkRemoveFromChecklistRept_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkDoNotPrintPassDueAlert" runat="server" Text="Do Not Print Pass Due Alert"
                                                                        AutoPostBack="true" OnCheckedChanged="ChecklistchkDoNotPrintPassDueAlert_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkPrintOneTimeEventCompleted" runat="server" Enabled="false" Text="Print One Time Event/Completed"
                                                                        AutoPostBack="true" OnCheckedChanged="ChecklistchkPrintOneTimeEventCompleted_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="tblCrewCheckList" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="left">
                                            <telerik:RadGrid ID="dgCrewCheckList" runat="server" EnableAJAX="True" AllowFilteringByColumn="false" AllowPaging="true" PageSize="999" AutoGenerateColumns="false" OnSelectedIndexChanged="CrewCheckList_SelectedIndexChanged"
                                            OnItemDataBound="CrewCheckList_ItemDataBound" Width="768px" Height="341px" CellSpacing="0" GridLines="None" ShowFooter="false">
                                                <MasterTableView CommandItemDisplay="Bottom"
                                                    AllowFilteringByColumn="false" AllowSorting="false" ItemStyle-HorizontalAlign="Left"
                                                    DataKeyNames="CustomerID,CrewID,CheckListID,CheckListCD,OriginalDT,PreviousCheckDT,DueDT,AlertDT,GraceDT,BaseMonthDT,IsScheduleCheck,IsEndCalendarYear,IsNextMonth,Frequency,IsPassedDueAlert,IsPrintStatus,TotalREQFlightHrs,IsCompleted,IsSecondInCommandFAR135,IsSecondInCommandFAR91,IsPilotInCommandFAR91,Specific,IsInActive,IsNoChecklistREPT,IsNoCrewCheckListREPTt,IsNoConflictEvent,IsOneTimeEvent,IsStopCALC,IsMonthEnd,FlightLogHours">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="CheckListCD" HeaderText="Code" CurrentFilterFunction="Contains"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="60px"
                                                            FilterDelay="500">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Description" CurrentFilterFunction="Contains" DataField="CrewChecklistDescription"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="CrewChecklistDescription"
                                                            HeaderStyle-Width="200px" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbCrewChecklistDescription" runat="server" CssClass="text200" Text='<%# Eval("CrewChecklistDescription") %>'
                                                                    MaxLength="40"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Previous" CurrentFilterFunction="Contains" DataField="PreviousCheckDT"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="PreviousCheckDT" FilterDelay="500"
                                                            HeaderStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbPreviousCheckDT" runat="server" CssClass="text60" MaxLength="40"
                                                                    Text='<%# Eval("PreviousCheckDT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Due Next" CurrentFilterFunction="Contains" DataField="DueDT"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="DueDT" HeaderStyle-Width="100px" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbDueDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("DueDT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Alert" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                            ShowFilterIcon="false" UniqueName="AlertDT" HeaderStyle-Width="100px" DataField="AlertDT" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbAlertDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("AlertDT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Grace" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                            ShowFilterIcon="false" UniqueName="GraceDT" HeaderStyle-Width="100px" DataField="GraceDT" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbGraceDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("GraceDT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Alert" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                            ShowFilterIcon="false" UniqueName="AlertDays" HeaderStyle-Width="60px" DataField="AlertDays" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbAlertDays" runat="server" CssClass="text60" Text='<%# Eval("AlertDays") %>'
                                                                    MaxLength="40"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Grace" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                            ShowFilterIcon="false" UniqueName="GraceDays" HeaderStyle-Width="60px" DataField="GraceDays" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbGraceDays" runat="server" CssClass="text60" Text='<%# Eval("GraceDays") %>'
                                                                    MaxLength="40"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Freq" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                            ShowFilterIcon="false" UniqueName="FrequencyMonth" HeaderStyle-Width="60px" DataField="FrequencyMonth" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbFrequencyMonth" runat="server" CssClass="text60" Text='<%# Eval("FrequencyMonth") %>'
                                                                    MaxLength="40"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Base Date" CurrentFilterFunction="Contains" DataField="BaseMonthDT"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="BaseMonthDT" HeaderStyle-Width="100px" FilterDelay="500">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbBaseMonthDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("BaseMonthDT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Original Date" CurrentFilterFunction="Contains" DataField="OriginalDT"
                                                            Display="false" AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="OriginalDT" FilterDelay="500"
                                                            HeaderStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbOriginalDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("OriginalDT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Aircraft Type" CurrentFilterFunction="Contains" DataField="AircraftTypeCD"
                                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="AircraftTypeCD" FilterDelay="500"
                                                            HeaderStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbAircraftTypeCD" runat="server" CssClass="text60" Text='<%# Eval("AircraftTypeCD") %>'
                                                                    MaxLength="40"></asp:Label>
                                                                <asp:HiddenField ID="hdnAircraftTypeCD" runat="server" Value='<%# Eval("AircraftID") %>' />
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Tot Req Hrs" Display="false" ShowFilterIcon="false"
                                                            UniqueName="HiddenCheckboxValues" HeaderStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="RadFreq" runat="server" Text='<%# Eval("Frequency") %>' />
                                                                <asp:CheckBox ID="chkPassedDueAlert" runat="server" Checked='<%# Eval("IsPassedDueAlert") != null ? Eval("IsPassedDueAlert") : false %>' />
                                                                <asp:CheckBox ID="chkPilotInCommandFAR91" runat="server" Checked='<%# Eval("IsPilotInCommandFAR91") != null ? Eval("IsPilotInCommandFAR91") : false %>' />
                                                                <asp:CheckBox ID="chkPilotInCommandFAR135" runat="server" Checked='<%# Eval("IsPilotInCommandFAR135") != null ? Eval("IsPilotInCommandFAR135") : false %>' />
                                                                <asp:CheckBox ID="chkSecondInCommandFAR91" runat="server" Checked='<%# Eval("IsSecondInCommandFAR91") != null ? Eval("IsSecondInCommandFAR91") : false %>' />
                                                                <asp:CheckBox ID="chkSecondInCommandFAR135" runat="server" Checked='<%# Eval("IsSecondInCommandFAR135") != null ? Eval("IsSecondInCommandFAR135") : false %>' />
                                                                <asp:CheckBox ID="chkInActive" runat="server" Checked='<%# Eval("IsInActive") != null ? Eval("IsInActive") : false %>' />
                                                                <asp:CheckBox ID="chkMonthEnd" runat="server" Checked='<%# Eval("IsMonthEnd") != null ? Eval("IsMonthEnd") : false %>' />
                                                                <asp:CheckBox ID="chkStopCALC" runat="server" Checked='<%# Eval("IsStopCALC") != null ? Eval("IsStopCALC") : false %>' />
                                                                <asp:CheckBox ID="chkOneTimeEvent" runat="server" Checked='<%# Eval("IsOneTimeEvent") != null ? Eval("IsOneTimeEvent") : false %>' />
                                                                <asp:CheckBox ID="chkCompleted" runat="server" Checked='<%# Eval("IsCompleted") != null ? Eval("IsCompleted") : false %>' />
                                                                <asp:CheckBox ID="chkNoConflictEvent" runat="server" Checked='<%# Eval("IsNoConflictEvent") != null ? Eval("IsNoConflictEvent") : false %>' />
                                                                <asp:CheckBox ID="chkNoCrewCheckListREPTt" runat="server" Checked='<%# Eval("IsNoCrewCheckListREPTt") != null ? Eval("IsNoCrewCheckListREPTt") : false %>' />
                                                                <asp:CheckBox ID="chkNoChecklistREPT" runat="server" Checked='<%# Eval("IsNoChecklistREPT") != null ? Eval("IsNoChecklistREPT") : false %>' />
                                                                <asp:CheckBox ID="chkPrintStatus" runat="server" Checked='<%# Eval("IsPrintStatus") != null ? Eval("IsPrintStatus") : false %>' />
                                                                <asp:CheckBox ID="chkNextMonth" runat="server" Checked='<%# Eval("IsNextMonth") != null ? Eval("IsNextMonth") : false %>' />
                                                                <asp:CheckBox ID="chkEndCalendarYear" runat="server" Checked='<%# Eval("IsEndCalendarYear") != null ? Eval("IsEndCalendarYear") : false %>' />
                                                                <asp:Label ID="lblTotalREQFlightHrs" runat="server" CssClass="text60" Text='<%# Eval("TotalREQFlightHrs") %>'></asp:Label>
                                                                <asp:Label ID="lblFlightLogHours" runat="server" CssClass="text60" Text='<%# Eval("FlightLogHours") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Tot Req Hrs" CurrentFilterFunction="Contains"
                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="TotalREQFlightHrs"
                                                            HeaderStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbTotalREQFlightHrs" runat="server" CssClass="text60" MaxLength="40"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Flt Log Hrs" CurrentFilterFunction="Contains"
                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="FlightLogHours"
                                                            HeaderStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbFlightLogHours" runat="server" CssClass="text60" MaxLength="40"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns> 
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    <PagerTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" style="height: 20px;" width="100%">
                                                            <tr>
                                                                <td style="text-align: right !important;padding-right: 8px !important;">
                                                                   <%# Container.OwnerTableView.Items.Count %> records
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true" EnableRowHoverStyle="false">
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                               
                                                <GroupingSettings CaseSensitive="false" />
                                            </telerik:RadGrid>
                                        </td>
                                    </tr>
                                </table>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td align="right">
                                            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                                <tr>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnApplyToAdditionalCrew" runat="server" Text="Apply To Additional Crew"
                                                            CausesValidation="false" OnClientClicking="ShowChecklistSettingsPopup" />
                                                    </td>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnAddChecklist" runat="server" Text="Add Checklist" CausesValidation="false"
                                                            OnClientClicking="ShowCrewCheckListPopup" />
                                                    </td>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnDeleteChecklist" runat="server" Text="Delete Checklist"
                                                            ValidationGroup="Save" OnClientClicking="CLickingChecklistDelete" OnClick="DeleteChecklist_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="AircraftAssigned" runat="server">
                                <table width="100%" class="border-box">
                                    <tr>
                                        <td>Crew Aircraft Assign Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <telerik:RadGrid ID="dgAircraftAssigned" runat="server" AllowSorting="true" AllowMultiRowSelection="true"
                                                            HeaderStyle-HorizontalAlign="left" OnNeedDataSource="AircraftAssigned_BindData"
                                                            OnItemDataBound="AircraftAssigned_ItemDataBound" Visible="true" AutoGenerateColumns="false"
                                                            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Width="750px" Height="341px">
                                                            <MasterTableView AllowPaging="false" HorizontalAlign="left" DataKeyNames="CrewAircraftAssignedID,FleetID,TailNum,AircraftCD,TypeDescription"
                                                                CommandItemDisplay="None">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." CurrentFilterFunction="Contains"
                                                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" UniqueName="TailNum" FilterDelay="500">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Assigned" CurrentFilterFunction="Contains"
                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" AllowFiltering="false" UniqueName="AircraftAssigned">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkAircraftAssigned" runat="server" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Type Code" CurrentFilterFunction="Contains"
                                                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterDelay="500">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="TypeDescription" HeaderText="Type Description" FilterDelay="500"
                                                                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="false" ShowFilterIcon="false">
                                                                    </telerik:GridBoundColumn>
                                                                </Columns>
                                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                            <GroupingSettings CaseSensitive="false" />
                                                        </telerik:RadGrid>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="Passports" runat="server">
                                <table width="100%" class="border-box" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>Passport Information:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <telerik:RadGrid ID="dgPassport" runat="server" OnNeedDataSource="Passport_BindData"
                                                            AllowPaging="false" OnItemDataBound="Passport_ItemDataBound" EnableAJAX="True"
                                                            Height="140px">
                                                            <MasterTableView AllowPaging="false" PagerStyle-AlwaysVisible="true" AutoGenerateColumns="False"
                                                                TableLayout="Fixed" EditMode="InPlace" DataKeyNames="PassportID,CrewID,PassengerRequestorID,CustomerID,Choice,IssueCity,PassportNum,PassportExpiryDT,CountryID,IsDefaultPassport,
                                                                    PilotLicenseNum,LastUpdUID,LastUpdTS,IssueDT,IsDeleted,CountryCD,CountryName"
                                                                CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false">
                                                                <Columns>
                                                                    <telerik:GridTemplateColumn HeaderText="Passport No." CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="120px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                        UniqueName="PassportNum">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbPassportNum" runat="server" CssClass="text90" Text='<%# Eval("PassportNum") %>'
                                                                                onblur="javascript:return CheckPassportChanged();" MaxLength="25"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Choice" CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="60px" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="Choice">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkChoice" Checked='<%# Eval("Choice") != null ? Eval("Choice") : false %>'
                                                                                runat="server" onchange="javascript:CheckPassportChanged();" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Expiration Date" CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="100px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                        UniqueName="PassportExpiryDT">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbPassportExpiryDT" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);CheckPassportChanged();"
                                                                                MaxLength="10" onkeydown="return tbDate_OnKeyDown(this, event);">
                                                                            </asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Issuing City/Authority" CurrentFilterFunction="Contains"
                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="240px"
                                                                        UniqueName="IssueCity">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbIssueCity" runat="server" CssClass="text220" Text='<%# Eval("IssueCity") %>'
                                                                                MaxLength="40" onblur="CheckPassportChanged();"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Issue Date" CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="100px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                        UniqueName="IssueDT">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbIssueDT" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);CheckPassportChanged();"
                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10">
                                                                            </asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Country" CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="110px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                        UniqueName="PassportCountry">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbPassportCountry" runat="server" CssClass="text60" Text='<%# Eval("CountryCD") %>'
                                                                                AutoPostBack="true" OnTextChanged="Nation_TextChanged" MaxLength="3" onblur="CheckPassportChanged();"></asp:TextBox>
                                                                            <asp:HiddenField ID="hdnPassportCountry" runat="server" Value='<%# Eval("CountryID") %>' />
                                                                            <asp:Button ID="btnPassportCountry" runat="server" CssClass="browse-button" OnClientClick="javascript:CheckPassportChanged();openWinGrd('RadVisaCountryMasterPopup',this);return false;" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                            </MasterTableView>
                                                            <ClientSettings>
                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                            <GroupingSettings CaseSensitive="false" />
                                                        </telerik:RadGrid>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnAddPassport" runat="server" Text="Add Passport" OnClientClicking="CheckPassportChanged"
                                                            OnClick="AddPassport_Click" />
                                                    </td>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnDeletePassport" runat="server" Text="Delete Passport" ValidationGroup="Save"
                                                            OnClientClicking="Clicking" OnClick="DeletePassport_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Visa Information:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <telerik:RadGrid ID="dgVisa" runat="server" OnNeedDataSource="Visa_BindData" EnableAJAX="True"
                                                            AllowPaging="false" OnItemDataBound="Visa_ItemDataBound" Height="140px" Width="748px">
                                                            <MasterTableView AllowPaging="false" PagerStyle-AlwaysVisible="true" AutoGenerateColumns="False"
                                                                TableLayout="Auto" EditMode="InPlace" DataKeyNames="VisaID,CrewID,PassengerRequestorID,CustomerID,VisaTYPE,VisaNum,LastUpdUID,LastUpdTS,CountryID,ExpiryDT,Notes,IssuePlace,IssueDate,IsDeleted,CountryCD,CountryName,TypeOfVisa,EntriesAllowedInPassport,VisaExpireInDays"
                                                                CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false">
                                                                <Columns>
                                                                    <telerik:GridTemplateColumn HeaderText="Country" CurrentFilterFunction="Contains"
                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="CountryCD" HeaderStyle-Width="110px">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbCountryCD" runat="server" CssClass="text60" AutoPostBack="true"
                                                                                Text='<%# Eval("CountryCD") %>' OnTextChanged="CountryCD_TextChanged" MaxLength="3"></asp:TextBox>
                                                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                                                            <asp:Button ID="btnCountryCD" runat="server" CssClass="browse-button" OnClientClick="javascript:CheckPassportChanged();openWinGrd('RadVisaCountryMasterPopup',this);return false;" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Visa No." CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="120px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                        UniqueName="VisaNum">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbVisaNum" runat="server" CssClass="text90" Text='<%# Eval("VisaNum") %>'
                                                                                onblur="javascript:return CheckVisaDuplicate(this);" MaxLength="40"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Expiration Date" CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="100px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                        UniqueName="ExpiryDT">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbExpiryDT" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10" DataFormatString="{0:MM/dd/yyyy}">
                                                                            </asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Issuing City/Authority" CurrentFilterFunction="Contains"
                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IssuePlace" HeaderStyle-Width="240px">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbIssuePlace" runat="server" CssClass="text220" Text='<%# Eval("IssuePlace") %>'
                                                                                MaxLength="40"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Issue Date" CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="100px" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                        UniqueName="IssueDate">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbIssueDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="10" DataFormatString="{0:MM/dd/yyyy}">
                                                                            </asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn FilterControlAltText="Choose Visa Type" HeaderText="Type Of Visa"
                                                                        HeaderStyle-Width="140px" UniqueName="TypeOfVisa">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlTypeOfVisa" runat="server" CssClass="dropdown120" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Number of Entries Allowed" ShowFilterIcon="false"
                                                                        UniqueName="EntriesAllowedInPassport" HeaderStyle-Width="110px">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbEntriesAllowedInPassport" runat="server" CssClass="text70" onKeyPress="return fnAllowNumeric(this, event,'')"
                                                                                onblur="parseInt(this, event);" MaxLength="5" Text='<%# Eval("EntriesAllowedInPassport") %>'>
                                                                            </asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Visa Activation Days" ShowFilterIcon="false"
                                                                        UniqueName="VisaExpireInDays" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbVisaExpireInDays" runat="server" CssClass="text70" onKeyPress="return fnAllowNumeric(this, event,'')"
                                                                                onblur="parseInt(this, event);" MaxLength="5" Text='<%# Eval("VisaExpireInDays") %>'
                                                                                ToolTip="Will expire if not used within x days from issue date">
                                                                            </asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Notes" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                        HeaderStyle-Width="140px" ShowFilterIcon="false" UniqueName="Notes">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbNotes" runat="server" CssClass="text120" Text='<%# Eval("Notes") %>'
                                                                                MaxLength="40"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                            </MasterTableView>
                                                            <ClientSettings>
                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                            <GroupingSettings CaseSensitive="false" />
                                                        </telerik:RadGrid>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnAddVisa" runat="server" Text="Add Visa" OnClick="AddVisa_Click" />
                                                    </td>
                                                    <td class="custom_radbutton">
                                                        <telerik:RadButton ID="btnDeleteVisa" runat="server" Text="Delete Visa" ValidationGroup="Save"
                                                            OnClientClicking="ClickingDelete" OnClick="DeleteVisa_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="EmailPref" runat="server">
                                <table width="100%" class="border-box" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="2">
                                            <%--<fieldset>
                                                <legend>E-mail</legend>--%>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkIsTripEmail" onclick="Deselectthis(this);" runat="server" Font-Bold="true" Text="Trip E-mail Notify" />
                                                                        <span style="font-weight: bold; color: #f00">(Required to apply E-mail Preferences)</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            <%--</fieldset>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" colspan="2">
                                            <fieldset>
                                                <legend>E-mail Preference</legend>
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" Text="Select All"
                                                                            onclick="SelectAll(this);" />
                                                                    </td>
                                                                    <td>
                                                                        <fieldset>
                                                                            <legend>Home</legend>
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel50">Date
                                                                                                </td>
                                                                                                <td class="tdLabel60">
                                                                                                    <asp:CheckBox ID="chkHomeArrival" runat="server" Text="Arrival" onclick="Deselectthis(this);" />
                                                                                                </td>
                                                                                                <td class="tdLabel80">
                                                                                                    <asp:CheckBox ID="chkHomeDeparture" runat="server" Text="Departure" onclick="Deselectthis(this);" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="left">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel50">Time
                                                                                                </td>
                                                                                                <td class="tdLabel60">
                                                                                                    <asp:RadioButton ID="radZULU" runat="server" Text="UTC" GroupName="HomeDT" AutoPostBack="true"
                                                                                                        OnCheckedChanged="ZULU_checkchanged" />
                                                                                                </td>
                                                                                                <td class="tdLabel80">
                                                                                                    <asp:RadioButton ID="radLocal" runat="server" Text="Local" GroupName="HomeDT" AutoPostBack="true"
                                                                                                        OnCheckedChanged="ZULU_checkchanged" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_5">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>General</legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td class="tdLabel180">
                                                                            <asp:CheckBox ID="chkDepartmentAuthorization" runat="server" onclick="Deselectthis(this);"
                                                                                Text="Department/Authorization" />
                                                                        </td>
                                                                        <td class="tdLabel150">
                                                                            <asp:CheckBox ID="chkRequestorPhone" runat="server" onclick="Deselectthis(this);"
                                                                                Text="Requestor/Phone" />
                                                                        </td>
                                                                        <td class="tdLabel120">
                                                                            <asp:CheckBox ID="chkAccountNo" runat="server" onclick="Deselectthis(this);" Text="Account No." />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCancellationDesc" runat="server" onclick="Deselectthis(this);"
                                                                                Text="Cancellation Description" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdLabel180">
                                                                            <asp:CheckBox ID="chkStatus" runat="server" onclick="Deselectthis(this);" Text="Status" />
                                                                        </td>
                                                                        <td class="tdLabel150">
                                                                            <asp:CheckBox ID="chkAirport" runat="server" onclick="Deselectthis(this);" Text="Airport" />
                                                                        </td>
                                                                        <td class="tdLabel120">
                                                                            <asp:CheckBox ID="chkChecklist" runat="server" onclick="Deselectthis(this);" Text="Checklist" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkRunway" runat="server" onclick="Deselectthis(this);" Text="Runway" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_5">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Crew Duty Details</legend>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="tdLabel180">
                                                                                        <asp:CheckBox ID="chkEndDuty" onclick="Deselectthis(this);" runat="server" Text="End Duty" />
                                                                                    </td>
                                                                                    <td class="tdLabel150">
                                                                                        <asp:CheckBox ID="chkOverride" onclick="Deselectthis(this);" runat="server" Text="Override" />
                                                                                    </td>
                                                                                    <td class="tdLabel120">
                                                                                        <asp:CheckBox ID="chkCrewRules" onclick="Deselectthis(this);" runat="server" Text="Crew Rules" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkFAR" onclick="Deselectthis(this);" runat="server" Text="FAR" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="tdLabel180">
                                                                                        <asp:CheckBox ID="chkDutyHours" onclick="Deselectthis(this);" runat="server" Text="Duty Hours" />
                                                                                    </td>
                                                                                    <td class="tdLabel150">
                                                                                        <asp:CheckBox ID="chkFlightHours" onclick="Deselectthis(this);" runat="server" Text="Flight Hours" />
                                                                                    </td>
                                                                                    <td class="tdLabel120">
                                                                                        <asp:CheckBox ID="chkRestHours" onclick="Deselectthis(this);" runat="server" Text="Rest Hours" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkAssociatedCrew" onclick="Deselectthis(this);" runat="server"
                                                                                            Text="Associated Crew" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_5">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Logistics Details</legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <fieldset>
                                                                                <legend>Crew</legend>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <fieldset>
                                                                                                <legend>Transport</legend>
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkCrewTransport" onclick="Deselectthis(this);" runat="server"
                                                                                                                Text="Departure" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkCrewArrival" onclick="Deselectthis(this);" runat="server" Text="Arrival" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </fieldset>
                                                                                        </td>
                                                                                        <td valign="top" align="right">
                                                                                            <asp:CheckBox ID="chkHotel" runat="server" onclick="Deselectthis(this);" Text="Hotel" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                        <td>
                                                                            <fieldset>
                                                                                <legend>Passenger</legend>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <fieldset>
                                                                                                <legend>Transport</legend>
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkPassengerTransport" onclick="Deselectthis(this);" runat="server"
                                                                                                                Text="Departure" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkPassengerArrival" onclick="Deselectthis(this);" runat="server"
                                                                                                                Text="Arrival" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </fieldset>
                                                                                        </td>
                                                                                        <td valign="top" align="right">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkPassengerHotel" onclick="Deselectthis(this);" runat="server"
                                                                                                            Text="Hotel" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkPassengerDetails" onclick="Deselectthis(this);" runat="server"
                                                                                                            Text="PAX Details" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkPassengerPaxPhone" onclick="Deselectthis(this);" runat="server"
                                                                                                            Text="PAX Phone" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <fieldset>
                                                                                <legend>FBO Handler</legend>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td class="tdLabel180">
                                                                                            <asp:CheckBox ID="chkDepartureInformation" onclick="Deselectthis(this);" runat="server"
                                                                                                Text="Departure Information" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkArrivalInformation" onclick="Deselectthis(this);" runat="server"
                                                                                                Text="Arrival Information" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                        <td>
                                                                            <fieldset>
                                                                                <legend>Catering</legend>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td class="tdLabel180">
                                                                                            <asp:CheckBox ID="chkDepartureCatering" onclick="Deselectthis(this);" runat="server"
                                                                                                Text="Departure Catering" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkArrivalCatering" onclick="Deselectthis(this);" runat="server"
                                                                                                Text="Arrival Catering" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_5">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Outbound Instructions</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkNEWSPAPER" onclick="Deselectthis(this);" runat="server" Text="Newspaper" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkCOFFEE" onclick="Deselectthis(this);" runat="server" Text="Coffee" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkJUICE" onclick="Deselectthis(this);" runat="server" Text="Juice" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tblspace_5">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <fieldset>
                                                <legend>Trip Changes E-mail</legend>
                                                <table width="100%" height="360px">
                                                    <tr>
                                                        <td valign="top">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <fieldset>
                                                                            <legend>General</legend>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkArrivalDepartureTime" onclick="Deselectthis(this);" runat="server"
                                                                                            Text="Arrival/Departure Time" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkArrivalDepartureICAO" onclick="Deselectthis(this);" runat="server"
                                                                                            Text="Arrival/Departure ICAO" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkAircraft" runat="server" onclick="Deselectthis(this);" Text="Aircraft" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkGeneralCrew" runat="server" onclick="Deselectthis(this);" Text="Crew" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkGeneralPassenger" runat="server" onclick="Deselectthis(this);"
                                                                                            Text="PAX" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkOutBoundInstructions" runat="server" onclick="Deselectthis(this);"
                                                                                            Text="Outbound Instructions" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkGeneralChecklist" runat="server" onclick="Deselectthis(this);"
                                                                                            Text="Checklist" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <fieldset>
                                                                            <legend>Logistics</legend>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkLogisticsFBO" runat="server" onclick="Deselectthis(this);" Text="FBO" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkLogisticsHotel" runat="server" onclick="Deselectthis(this);"
                                                                                            Text="Hotel" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkLogisticsTransportation" runat="server" onclick="Deselectthis(this);"
                                                                                            Text="Transportation" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkLogisticsCatering" runat="server" onclick="Deselectthis(this);"
                                                                                            Text="Catering" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Additional Info</legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkCrewHotelConfirm" runat="server" Text="Crew Hotel Confirm" onclick="chkcontrol(this),Deselectthis(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkDeptAirportNotes" runat="server" Text="Departure Airport Notes"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkCrewHotelComments" runat="server" Text="Crew Hotel Comments"
                                                                                            onclick="chkcontrol(this),Deselectthis(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkArrAirportNotes" runat="server" Text="Arrival Airport Notes"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkCrewDeptTransConfirm" runat="server" Text="Crew Departure Transport Confirm"
                                                                                            onclick="chkcontrol(this),Deselectthis(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkDeptAirportAlerts" runat="server" Text="Departure Airport Alerts"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkCrewDeptTransComments" runat="server" Text="Crew Departure Transport Comments"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkArrAirportAlerts" runat="server" Text="Arrival Airport Alerts"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkCrewArrTransConfirm" runat="server" Text="Crew Arrival Transport Confirm"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkArrFBOConfirm" runat="server" Text="Arrival FBO Confirm" onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkCrewArrTransComments" runat="server" Text="Crew Arrival Transport Comments"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkArrFBOComments" runat="server" Text="Arrival FBO Comments" onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkCrewNotes" runat="server" Text="Crew Notes" onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkDeptFBOConfirm" runat="server" Text="Departure FBO Confirm"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkPAXHotelConfirm" runat="server" Text="PAX Hotel Confirmation"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkDeptFBOComments" runat="server" Text="Departure FBO Comments"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkPAXHotelComments" runat="server" Text="PAX Hotel Comments" onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkDeptCaterConfirm" runat="server" Text="Departure Catering Confirm"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkPAXDeptTransConfirm" runat="server" Text="PAX Departure Transport Confirm"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkDeptCaterComments" runat="server" Text="Departure Catering Comments"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkPAXDeptTransComments" runat="server" Text="PAX Departure Transport Comments"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkArrCaterConfirm" runat="server" Text="Arrival Catering Confirm"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkPAXArrTransConfirm" runat="server" Text="PAX Arrival Transport Confirm"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkArrCaterComments" runat="server" Text="Arrival Catering Comments"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkPAXArrTransComments" runat="server" Text="PAX Arrival Transport Comments"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkTripAlerts" runat="server" Text="Trip Alerts" onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkPAXNotes" runat="server" Text="PAX Notes" onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkTripNotes" runat="server" Text="Trip Notes" onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkOutboundInstComments" runat="server" Text="Outbound Instruction Comments"
                                                                                            onclick="chkcontrol(this);" />
                                                                                    </td>
                                                                                    <td></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlAdditionalInfo" Width="100%" ExpandAnimation-Type="None"
                            CollapseAnimation-Type="None" runat="server" OnClientItemExpand="pnlAdditionalInfoExpand"
                            OnClientItemCollapse="pnlAdditionalInfoCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="false" Text="Additional Information">
                                    <Items>
                                        <telerik:RadPanelItem>
                                            <ContentTemplate>
                                                <table width="100%" class="box1">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="dgCrewAddlInfo" runat="server" EnableAJAX="True" AllowMultiRowSelection="false"
                                                                AllowPaging="false" PageSize="999" OnItemDataBound="CrewAddlInfo_ItemDataBound">
                                                                <MasterTableView ShowFooter="false" AllowPaging="false" AutoGenerateColumns="False"
                                                                    EditMode="InPlace" TableLayout="Fixed" DataKeyNames="CrewInfoXRefID,CrewID,CrewInfoID,InformationDESC,CrewInfoCD,InformationValue,LastUpdUID,LastUpdTS,IsReptFilter"
                                                                    CommandItemDisplay="None" AllowFilteringByColumn="false">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn DataField="CrewInfoCD" UniqueName="CrewInfoCD" HeaderText="Code"
                                                                            CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                            AllowFiltering="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="InformationDESC" UniqueName="CrewInformationDescription"
                                                                            HeaderText="Description" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                            ShowFilterIcon="false" AllowFiltering="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Additional Information" CurrentFilterFunction="Contains"
                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="AddlInfo" AllowFiltering="false">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbAddlInfo" runat="server" CssClass="text100" Text='<%# Eval("InformationValue") %>'
                                                                                    MaxLength="40"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Report Filter" CurrentFilterFunction="Contains"
                                                                            DataField="IsReptFilter" AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IsReptFilter"
                                                                            AllowFiltering="false">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkIsReptFilter" Checked='<%# Eval("IsReptFilter") %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                                <GroupingSettings CaseSensitive="false" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnAddlInfo" runat="server" Text="Add Info." CausesValidation="false"
                                                                            OnClientClicking="ShowAddlInfoPopup" />
                                                                    </td>
                                                                    <%--<td>
                                                                        <asp:Button ID="btnDeleteInfo" runat="server" CssClass="ui_nav" Text="Delete Info."
                                                                            CausesValidation="false" OnClick="DeleteInfo_Click" OnClientClick="if(!confirm('Are you sure you want to delete this ADDITIONAL INFO record?')) return false;" />
                                                                    </td>--%>
                                                                    <td class="custom_radbutton">
                                                                        <telerik:RadButton ID="btnDeleteInfo" runat="server" Text="Delete Info." ValidationGroup="Save"
                                                                            OnClientClicking="ClickingAdditionalInfoDelete" OnClick="DeleteInfo_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                runat="server" OnClientItemExpand="pnlImageExpand" OnClientItemCollapse="pnlImageCollapse">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Attachments" CssClass="PanelHeaderStyle">
                        <ContentTemplate>
                            <table width="100%" class="box1">
                                <tr style="display: none;">
                                    <td class="tdLabel100">Document Name
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:TextBox ID="tbImgName" MaxLength="24" runat="server" CssClass="text210" OnBlur="CheckName();"
                                            ClientIDMode="AutoID"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="tdLabel270">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:FileUpload ID="fileUL" runat="server" Enabled="false" ClientIDMode="AutoID"
                                                        onchange="javascript:return File_onchange();" />
                                                    <asp:Label ID="lblError" CssClass="alert-text" runat="server" Style="float: left;">All file types are allowed.</asp:Label>
                                                    <asp:HiddenField ID="hdnUrl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" class="tdLabel110">
                                        <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                            OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text200"
                                            ClientIDMode="AutoID">
                                        </asp:DropDownList>
                                    </td>
                                    <td valign="top" class="tdLabel80">
                                        <asp:Image ID="imgFile" Width="50px" Height="50px" runat="server" OnClick="OpenRadWindow();"
                                            ClientIDMode="AutoID" />
                                        <asp:HyperLink ID="lnkFileName" runat="server" ClientIDMode="Static">Download File</asp:HyperLink>
                                    </td>
                                    <td valign="top" align="right" class="custom_radbutton">
                                        <telerik:RadButton ID="btndeleteImage" runat="server" Text="Delete" Enabled="false"
                                            OnClientClicking="ImageDeleteConfirm" OnClick="DeleteImage_Click" ClientIDMode="Static" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlCurrency" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            OnClientItemExpand="pnlCurrencyExpand" OnClientItemCollapse="pnlCurrencyCollapse"
                            runat="server">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="false" Text="Currency">
                                    <Items>
                                        <telerik:RadPanelItem>
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0" class="box1" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table align="left">
                                                                <tr>
                                                                    <td>Today
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lbToday" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="right" width="70%">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="30%" align="right">
                                                                        <telerik:RadTabStrip Visible="false" ID="tabSwitchViews" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                                                                            CausesValidation="false" MultiPageID="RadMultiPage2" SelectedIndex="1" Align="Justify"
                                                                            Width="200px">
                                                                            <Tabs>
                                                                                <telerik:RadTab Text="Currency">
                                                                                </telerik:RadTab>
                                                                                <telerik:RadTab Text="Checklist">
                                                                                </telerik:RadTab>
                                                                            </Tabs>
                                                                        </telerik:RadTabStrip>
                                                                        <asp:Button ID="btnRefreshView" CssClass="ui_nav rightmarginclass" runat="server" Text="Refresh View"
                                                                                        OnClick="RefreshView_Click" ClientIDMode="Static" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <telerik:RadMultiPage ID="RadMultiPage2" runat="server" SelectedIndex="0" Width="100%">
                                                                <telerik:RadPageView ID="RadPageView1" runat="server">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <telerik:RadGrid ID="dgCurrency" runat="server" EnableAJAX="True" AllowFilteringByColumn="false"
                                                                                    OnItemDataBound="Currency_ItemDataBound" AutoGenerateColumns="false" Width="750px">
                                                                                    <MasterTableView ShowFooter="false" AllowPaging="false" CommandItemDisplay="None"
                                                                                        AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                                                                        <Columns>
                                                                                            <telerik:GridBoundColumn CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="65px" AllowSorting="false" DataField="AircraftCD" ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" DataField="LandingDay" ShowFilterIcon="false" AllowSorting="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" DataField="L_N90" ShowFilterIcon="false" AllowSorting="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" DataField="T_D90" ShowFilterIcon="false" AllowSorting="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" DataField="T_N90" ShowFilterIcon="false" AllowSorting="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Appr" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="55px" DataField="Appr90" ShowFilterIcon="false" AllowSorting="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Instr" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="55px" DataField="Instr90" ShowFilterIcon="false" AllowSorting="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Days" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" DataField="days7" ShowFilterIcon="false" AllowSorting="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Days Rest" CurrentFilterFunction="Contains"
                                                                                                HeaderStyle-Width="50px" AutoPostBackOnFilter="false" DataField="dayrest7" ShowFilterIcon="false"
                                                                                                AllowSorting="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Cal Mon" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" AllowSorting="false" DataField="CalMon" ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Days" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" AllowSorting="false" DataField="days90" ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Cal. Qtr" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" AllowSorting="false" DataField="CalQtr" ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Cal Qtr Rest" CurrentFilterFunction="Contains"
                                                                                                HeaderStyle-Width="65px" AllowSorting="false" AutoPostBackOnFilter="false" DataField="CalQtrRest"
                                                                                                ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Mon" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="60px" AllowSorting="false" DataField="Mon6" ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Prev 2 Qtrs" CurrentFilterFunction="Contains"
                                                                                                HeaderStyle-Width="50px" AllowSorting="false" AutoPostBackOnFilter="false" DataField="Prev2Qtrs"
                                                                                                ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Mon" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="65px" DataField="Mon12" AllowSorting="false" ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="Cal yr" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" DataField="Calyr" AllowSorting="false" ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="365 Days" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="55px" DataField="Days365" AllowSorting="false" ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn HeaderText="30 days" CurrentFilterFunction="Contains" AutoPostBackOnFilter="false"
                                                                                                HeaderStyle-Width="50px" DataField="days30" AllowSorting="false" ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                        </Columns>
                                                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                    </MasterTableView>
                                                                                    <ClientSettings>
                                                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                        <Selecting AllowRowSelect="true" />
                                                                                    </ClientSettings>
                                                                                    <GroupingSettings CaseSensitive="false" />
                                                                                </telerik:RadGrid>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </telerik:RadPageView>
                                                                <telerik:RadPageView ID="RadPageView2" runat="server">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <asp:CheckBox ID="chkDisplayInactiveChecks" Text="Display Inactive Checklist Only"
                                                                                AutoPostBack="true" OnCheckedChanged="chkDisplayInactiveChecks_CheckedChanged"
                                                                                runat="server" />
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <telerik:RadGrid ID="dgCurrencyChecklist" runat="server" OnNeedDataSource="CurrencyChecklist_BindData"
                                                                                    OnItemDataBound="CurrencyChecklist_ItemDataBound" EnableAJAX="True" AllowFilteringByColumn="false"
                                                                                    AutoGenerateColumns="false" Width="750px" PageSize="999">
                                                                                    <MasterTableView ShowFooter="false" AllowPaging="false" CommandItemDisplay="None"
                                                                                        AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                                                                        <Columns>
                                                                                            <telerik:GridTemplateColumn HeaderText="Description" CurrentFilterFunction="Contains"
                                                                                                AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="CrewChecklistDescription"
                                                                                                HeaderStyle-Width="220px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbCrewChecklistDescription" runat="server" Text='<%# Eval("CrewChecklistDescription") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Due" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                                                ShowFilterIcon="false" UniqueName="DueDT" HeaderStyle-Width="80px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbDueDT" runat="server" CssClass="text60" Text='<%# Eval("DueDT") %>'
                                                                                                        MaxLength="40"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Alert" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                                                ShowFilterIcon="false" UniqueName="AlertDT" HeaderStyle-Width="80px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbAlertDT" runat="server" CssClass="text60" Text='<%# Eval("AlertDT") %>'
                                                                                                        MaxLength="40"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Grace" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                                                ShowFilterIcon="false" UniqueName="GraceDT" HeaderStyle-Width="80px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbGraceDT" runat="server" CssClass="text60" Text='<%# Eval("GraceDT") %>'
                                                                                                        MaxLength="40"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Aircraft Type" CurrentFilterFunction="Contains"
                                                                                                AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="AircraftTypeCD"
                                                                                                HeaderStyle-Width="100px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbAircraftTypeCD" runat="server" Text='<%# Eval("AircraftTypeCD") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Aircraft Description" CurrentFilterFunction="Contains"
                                                                                                AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="AircraftDescription"
                                                                                                HeaderStyle-Width="200px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbAircraftTypeDesc" runat="server" Text='<%# Eval("AircraftDescription") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Current Flt. Hours" CurrentFilterFunction="Contains"
                                                                                                UniqueName="CurrentFltHrs" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                                                HeaderStyle-Width="70px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbCurrentFltHrs" runat="server" CssClass="text60" MaxLength="40"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Total Reqd. Flt. Hours" CurrentFilterFunction="Contains"
                                                                                                AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="TotalREQFlightHrs"
                                                                                                HeaderStyle-Width="70px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbTotalReqdFltHrs" runat="server" CssClass="text60" Text='<%# Eval("TotalREQFlightHrs") %>'
                                                                                                        MaxLength="40"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                        </Columns>
                                                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                    </MasterTableView>
                                                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                        <Selecting AllowRowSelect="true" />
                                                                                    </ClientSettings>
                                                                                    <GroupingSettings CaseSensitive="false" />
                                                                                </telerik:RadGrid>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </telerik:RadPageView>
                                                            </telerik:RadMultiPage>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlAdditionalNotes" Width="100%" ExpandAnimation-Type="None"
                            CollapseAnimation-Type="None" runat="server" OnClientItemExpand="pnlAdditionalNotesExpand"
                            OnClientItemCollapse="pnlAdditionalNotesCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="false" Text="Additional Notes" CssClass="PanelHeaderCrewRoster">
                                    <Items>
                                        <telerik:RadPanelItem>
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0" class="note-box">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="tbAdditionalNotes" TextMode="MultiLine" runat="server" CssClass="textarea-db"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                            <tr align="right">
                                <td>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="button" OnClick="Edit_Click" />
                                </td>
                                <td class="custom_radbutton">
                                    <telerik:RadButton ID="btnSaveChanges" runat="server" Text="Save" ValidationGroup="Save"
                                        OnClientClicking="CheckPassportChoice" OnClick="SaveChanges_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="false"
                                        CssClass="button" OnClick="Cancel_Click" />
                                </td>
                                <asp:HiddenField ID="hdnFirstOrLast" runat="server" />
                                <asp:HiddenField ID="hdnSave" runat="server" />
                                <asp:HiddenField ID="hdnNationalityID" runat="server" />
                                <asp:HiddenField ID="hdnClientID" runat="server" />
                                <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                                <asp:HiddenField ID="hdnDepartmentID" runat="server" />
                                <asp:HiddenField ID="hdnResidenceCountryID" runat="server" />
                                <asp:HiddenField ID="hdnLicCountryID" runat="server" />
                                <asp:HiddenField ID="hdnAddLicCountryID" runat="server" />
                                <asp:HiddenField ID="hdnCitizenshipID" runat="server" />
                                <asp:HiddenField ID="hdnCountryID" runat="server" />
                                <asp:HiddenField ID="hdnWarning" runat="server" />
                                <asp:HiddenField ID="hdnCrewID" runat="server" />
                                <asp:HiddenField ID="hdnCrewGroupID" runat="server" />
                                <asp:HiddenField ID="hdnTemp" runat="server" />
                                <asp:HiddenField ID="hdnIsPassportChoice" runat="server" value="No"/>
                                <asp:HiddenField ID="hdnIsPassportChanged" runat="server" />
                                <asp:HiddenField ID="hdnDelete" runat="server" />
                                <asp:HiddenField ID="hdnTempHoursTxtbox" runat="server" />
                                <asp:HiddenField ID="hdnBrowserName" runat="server" />
                                <asp:HiddenField ID="hdnIsPassportChoiceChanged" runat="server" />
                                <asp:HiddenField ID="hdnIsPassportChoiceDeletedCount" runat="server" />
                                <asp:HiddenField ID="hdnIsPassportChoiceDeleted" runat="server" />
                                <asp:HiddenField ID="hdnClientFilterID" runat="server" />
                                <asp:HiddenField ID="hdnSaveConfirm" runat="server" />
                                <asp:HiddenField ID="hdnRedirect" runat="server" />
                                <asp:Label ID="lblSaveValue" runat="server" Visible="false"></asp:Label>
                            </tr>
                        </table>
                        <table id="tblHidden" style="display: none;">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSaveChangesdummy" runat="server" Text="Save" OnClick="Save_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
