﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class PassengerChecklistPop : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            lblMessage.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckListDetail);
                }
            }
        }
        /// <summary>
        /// Method to Bind Grid Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewCheckList_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objPassengerAddInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objPassengerAddInfoVal = objPassengerAddInfoService.GetPassengerAdditionalInfoList();
                            if (objPassengerAddInfoVal.ReturnFlag == true)
                            {
                                dgCrewCheckList.DataSource = objPassengerAddInfoVal.EntityList.Where(x => x.IsDeleted == false && x.IsCheckList == true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckListDetail);
                }
            }
        }
        /// <summary>
        /// Method to trigger Grid Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewCheckList_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckListDetail);
                }
            }
        }
        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewCheckList_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckListDetail);
                }
            }
        }
        /// <summary>
        /// Method to Bind into "CrewNewAddlInfo" session, and 
        /// Call "CloseAndRebind" javascript function to Close and
        /// Rebind the selected record into parent page.
        /// </summary>
        private void InsertSelectedRow()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewCheckList.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];

                        if (CheckIfExists(Item.GetDataKeyValue("PassengerInfoCD").ToString()))
                        {
                            lblMessage.Text = "Warning, Checklist Code Already Exists.";
                            lblMessage.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            bool IsExist = false;
                            if (Session["PaxCheckList"] != null)
                            {
                                List<FlightPakMasterService.GetPaxChecklistDate> GetCrewCheckListDateInfo = (List<FlightPakMasterService.GetPaxChecklistDate>)Session["PaxCheckList"];

                                for (int Index = 0; Index < GetCrewCheckListDateInfo.Count; Index++)
                                {
                                    if (GetCrewCheckListDateInfo[Index].PassengerAdditionalInfoID.ToString() == Item.GetDataKeyValue("PassengerInformationID").ToString())
                                    {
                                        GetCrewCheckListDateInfo[Index].PreviousCheckDT = null;
                                        GetCrewCheckListDateInfo[Index].DueDT = null;
                                        GetCrewCheckListDateInfo[Index].AlertDT = null;
                                        GetCrewCheckListDateInfo[Index].GraceDT = null;
                                        GetCrewCheckListDateInfo[Index].AlertDays = 0;
                                        GetCrewCheckListDateInfo[Index].AlertDays = Convert.ToInt32("0");
                                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays != null)
                                        {
                                            GetCrewCheckListDateInfo[Index].AlertDays = Convert.ToInt32(UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays);
                                        }
                                        GetCrewCheckListDateInfo[Index].GraceDays = 0;
                                        GetCrewCheckListDateInfo[Index].FrequencyMonth = 0;
                                        GetCrewCheckListDateInfo[Index].IsMonthEnd = false;
                                        GetCrewCheckListDateInfo[Index].IsStopCALC = false;
                                        GetCrewCheckListDateInfo[Index].IsOneTimeEvent = false;
                                        GetCrewCheckListDateInfo[Index].IsNoConflictEvent = false;
                                        GetCrewCheckListDateInfo[Index].IsNoChecklistREPT = false;
                                        GetCrewCheckListDateInfo[Index].IsInActive = false;
                                        GetCrewCheckListDateInfo[Index].Specific = 0;
                                        GetCrewCheckListDateInfo[Index].IsPassedDueAlert = false;
                                        GetCrewCheckListDateInfo[Index].IsCompleted = false;
                                        GetCrewCheckListDateInfo[Index].OriginalDT = null;
                                        GetCrewCheckListDateInfo[Index].IsPrintStatus = false;
                                        GetCrewCheckListDateInfo[Index].Frequency = 1;
                                        GetCrewCheckListDateInfo[Index].IsNextMonth = false;
                                        GetCrewCheckListDateInfo[Index].IsEndCalendarYear = false;
                                        GetCrewCheckListDateInfo[Index].IsScheduleCheck = false;
                                        GetCrewCheckListDateInfo[Index].IsDeleted = false;



                                        IsExist = true;
                                    }
                                }

                                Session["PaxCheckList"] = GetCrewCheckListDateInfo;
                            }

                            if (IsExist == false)
                            {
                                List<GetPaxChecklistDate> GetCrewCheckListDate = new List<GetPaxChecklistDate>();
                                GetPaxChecklistDate GetCrewCheckListDateDef = new GetPaxChecklistDate();
                                //GetCrewCheckListDateDef.CheckListID = Convert.ToInt64(Item.GetDataKeyValue("CrewCheckID").ToString());
                                GetCrewCheckListDateDef.PassengerAdditionalInfoID = Convert.ToInt64(Item.GetDataKeyValue("PassengerInformationID").ToString());
                                GetCrewCheckListDateDef.PassengerChecklistCD = Item.GetDataKeyValue("PassengerInfoCD").ToString();
                                GetCrewCheckListDateDef.PassengerChecklistDescription = Item.GetDataKeyValue("PassengerDescription").ToString();
                                GetCrewCheckListDateDef.PreviousCheckDT = null;
                                GetCrewCheckListDateDef.DueDT = null;
                                GetCrewCheckListDateDef.AlertDT = null;
                                GetCrewCheckListDateDef.GraceDT = null;
                                GetCrewCheckListDateDef.AlertDays = 0;
                                GetCrewCheckListDateDef.AlertDays = 0;
                                if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays != null)
                                {
                                    GetCrewCheckListDateDef.AlertDays = Convert.ToInt32(UserPrincipal.Identity._fpSettings._CrewChecklistAlertDays);
                                }
                                GetCrewCheckListDateDef.GraceDays = 0;
                                GetCrewCheckListDateDef.FrequencyMonth = 0;
                               
                                GetCrewCheckListDateDef.IsMonthEnd = false;
                                GetCrewCheckListDateDef.IsStopCALC = false;
                                GetCrewCheckListDateDef.IsOneTimeEvent = false;
                                GetCrewCheckListDateDef.IsNoConflictEvent = false;
                              
                                GetCrewCheckListDateDef.IsNoChecklistREPT = false;
                                GetCrewCheckListDateDef.IsInActive = false;
                                GetCrewCheckListDateDef.Specific = 0;
                                GetCrewCheckListDateDef.IsPassedDueAlert = false;
                                
                                GetCrewCheckListDateDef.IsCompleted = false;
                                GetCrewCheckListDateDef.OriginalDT = null;
                                GetCrewCheckListDateDef.IsPrintStatus = false;
                                GetCrewCheckListDateDef.Frequency = 1;
                                GetCrewCheckListDateDef.IsNextMonth = false;
                                GetCrewCheckListDateDef.IsEndCalendarYear = false;
                                GetCrewCheckListDateDef.IsScheduleCheck = false;
                                GetCrewCheckListDateDef.IsDeleted = false;
                                GetCrewCheckListDate.Add(GetCrewCheckListDateDef);
                                if (Session["PaxCheckList"] == null)
                                {
                                    Session["PaxCheckList"] = GetCrewCheckListDate;
                                }
                                else
                                {
                                    Session["PaxNewCheckList"] = GetCrewCheckListDate;
                                }
                            }
                            //if (Item.GetDataKeyValue("CrewCheckCD") != null)
                            //{
                            //   // Session["CrewCheckListCode"] = Item.GetDataKeyValue("CrewCheckCD").ToString();
                            //}
                            InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                        }
                    }
                    else
                    {
                        RadAjaxManager1.ResponseScripts.Add(@"showMessageBox('Please select an item.','Universal Weather And Aviation')");
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }
        /// <summary>
        /// Function to Check if Crew Information Code alerady exists or not, by passing Crew Code
        /// </summary>
        /// <returns>True / False</returns>
        private bool CheckIfExists(string infoCode)
        {
            bool returnVal = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(infoCode))
            {
                if (Session["PaxCheckList"] != null)
                {
                    List<FlightPakMasterService.GetPaxChecklistDate> PassengerAdditionalInfoDef = (List<FlightPakMasterService.GetPaxChecklistDate>)Session["PaxCheckList"];
                    var result = (from PassengerAdditionalInf in PassengerAdditionalInfoDef
                                  where PassengerAdditionalInf.PassengerChecklistCD.ToUpper().Trim() == infoCode.ToUpper().Trim() &&
                                        PassengerAdditionalInf.IsDeleted == false
                                  select PassengerAdditionalInf);
                    if (result.Count() > 0)
                    { returnVal = true; }
                }
            }
            return returnVal;
        }

        protected void dgCrewCheckList_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewCheckList, Page.Session);
        }
    }
}