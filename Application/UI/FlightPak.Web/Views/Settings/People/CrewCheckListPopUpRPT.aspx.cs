﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewCheckListPopUpRPT : BaseSecuredPage
    { 
        private string HomeBase;
         private ExceptionManager exManager;

         protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["CrewCheckCD"] != null)
            {
                if (Request.QueryString["CrewCheckCD"].ToString() == "1")
                {
                    dgCrewCheckList.AllowMultiRowSelection = true;
                }
                else
                {
                    dgCrewCheckList.AllowMultiRowSelection = false;
                }
            }
            else
            {
                dgCrewCheckList.AllowMultiRowSelection = false;
            }
                  using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["CrewCheckCD"]))
                        {
                            HomeBase = Request.QueryString["CrewCheckCD"].ToUpper().Trim();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }

        

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
      protected void CrewCheckList_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewCheckListService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CheckListValue = CrewCheckListService.GetCrewChecklistList();
                            if (CheckListValue.ReturnFlag == true)
                            {
                                dgCrewCheckList.DataSource = CheckListValue.EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCrewCheckList;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }

        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(HomeBase))
                {
                    foreach (GridDataItem Item in dgCrewCheckList.MasterTableView.Items)
                    {
                        if (Item["CrewCheckCD"].Text.Trim().ToUpper() == HomeBase)
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgCrewCheckList.MasterTableView.Items.Count > 0)
                    {
                        dgCrewCheckList.SelectedIndexes.Add(0);

                    }
                }

            }

        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewCheckList_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["CrewCheckCD"] != null)
                {
                    if (Request.QueryString["CrewCheckCD"].ToString() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }

        protected void dgCrewCheckList_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgCrewCheckList_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewCheckList, Page.Session);
        }

    }
}