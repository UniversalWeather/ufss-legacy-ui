﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using FlightPak.Common;
using FlightPak.Common.Constants;
using System.Text.RegularExpressions;

//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CustomerAddressCatalog : BaseSecuredPage
    {
        // private bool IsEmptyCheck = true;
        public List<string> lstCustomerAddressCodes = new List<string>();
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private bool CustomerAddressPageNavigated = false;
        private string strCustomerAddressID = "";
        private string strCustomerAddressCD = "";       
        private bool _selectLastModified = false;

        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCustomerAddress, dgCustomerAddress, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCustomerAddress.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewCustomerAddressCatalog);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgCustomerAddress.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                EnableForm(false);
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                                {
                                    chkSearchHomebaseOnly.Checked = true;
                                }
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                                CreateDictionayForImgUpload();
                            }
                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                            else if (eventArgument == "DeleteImage_Click")
                            {
                                DeleteImage_Click(sender, e);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];

                        int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            dicImg.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = dicImg;

                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                            {
                                dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            }
                            Session["DicImgDelete"] = dicImgDelete;

                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in dicImg)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            imgFile.ImageUrl = "../../../App_Themes/Default/images/noimage.jpg";
                            ImgPopup.ImageUrl = null;
                            lnkFileName.NavigateUrl = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }


        }

        //veracode fix for 2527
        //public static byte[] ImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int)fileStream.Length);
        //        fileStream.Close();
        //        return buffer;
        //    }
        //}


        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                System.IO.Stream myStream;
                Int32 fileLen;
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        //if (ddlImg.Items.Count <= 0)
                        if (ddlImg.Items.Count >= 0)
                        {
                            string FileName = fileUL.FileName;
                            fileLen = fileUL.PostedFile.ContentLength;
                            Byte[] Input = new Byte[fileLen];

                            myStream = fileUL.FileContent;
                            myStream.Read(Input, 0, fileLen);

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);
                            
                            ////start of modification for image issue
                            //if (hdnBrowserName.Value == "Chrome")
                            //{
                            //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                            //}
                            //else
                            //{
                            //    Session["Base64"] = Input;
                            //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                            //}
                            ////end of modification for image issue                            
                            Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                            //Commented for removing document name
                            //if (tbImgName.Text.Trim() != "")
                            //    FileName = tbImgName.Text.Trim();

                            int count = dicImg.Count(D => D.Key.Equals(FileName));
                            if (count > 0)
                            {
                                dicImg[FileName] = Input;
                            }
                            else
                            {
                                DeleteOtherImage();
                                dicImg.Add(FileName, Input);
                                string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FileName);
                                ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(encodedFileName, encodedFileName));

                                //Ramesh: Set the URL for image file or link based on the file type
                                SetURL(FileName, Input);

                                ////start of modification for image issue
                                //if (hdnBrowserName.Value == "Chrome")
                                //{
                                //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                                //}
                                //else
                                //{
                                //    Session["Base64"] = Input;
                                //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                //}
                                ////end of modification for image issue
                            }
                            tbImgName.Text = "";
                            btndeleteImage.Enabled = true;
                        }
                    }
                }
            }
        }

        private void DeleteOtherImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];

                int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                if (count > 0)
                {
                    dicImg.Remove(ddlImg.Text.Trim());
                    Session["DicImg"] = dicImg;

                    Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                    dicImgDelete.Add(ddlImg.Text.Trim(), "");
                    Session["DicImgDelete"] = dicImgDelete;

                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbImgName.Text = "";
                    int i = 0;
                    foreach (var DicItem in dicImg)
                    {
                        ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                        i = i + 1;
                    }
                    imgFile.ImageUrl = "";
                    ImgPopup.ImageUrl = null;
                    lnkFileName.NavigateUrl = "";
                }
            }
        }

        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool imgFound = false;
                            Session["Base64"] = null;   //added for image issue
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ObjRetImg = ObjImgService.GetFileWarehouseList("CustomAddress", Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());

                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                {
                                    imgFound = true;
                                    byte[] picture = fwh.UWAFilePath;

                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(fwh.UWAFileName, picture);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = picture;
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                                //tbImgName.Text = ddlImg.Text.Trim();//Commented for removing document name
                                if (!imgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (count > 0)
                                    {
                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }

                                }
                                //else
                                //{
                                //    imgFile.ImageUrl = "Default/images/noimage.jpg";
                                //}
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }

        }

        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                Session["DicImg"] = dicImg;

                Dictionary<string, string> dicImgDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = dicImgDelete;
            }
        }
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgCustomerAddress.Rebind();
                    }
                    if (dgCustomerAddress.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedCustomerAddressID"] = null;
                        //}
                        if (Session["SelectedCustomerAddressID"] == null)
                        {
                            dgCustomerAddress.SelectedIndexes.Add(0);

                            Session["SelectedCustomerAddressID"] = dgCustomerAddress.Items[0].GetDataKeyValue("CustomAddressID").ToString();
                        }

                        if (dgCustomerAddress.SelectedIndexes.Count == 0)
                            dgCustomerAddress.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void SelectItem()
        {
            if (Session["SelectedCustomerAddressID"] != null)
            {
                string ID = Session["SelectedCustomerAddressID"].ToString();
                foreach (GridDataItem Item in dgCustomerAddress.MasterTableView.Items)
                {
                    if (Item["CustomAddressID"].Text.Trim() == ID)
                    {
                        Item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                DefaultSelection(false);
            }
        }

        protected void CustomerAddress_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CustomerAddressPageNavigated)
                        {
                            //SelectItem();
                        }
                        else if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCustomerAddress, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCustomerAddress;
                        }
                        else if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCustomerAddress;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCustomerAddress_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                    IsEmptyCheck = false;
                }

            }
        }
        /// <summary>
        /// Bind Customer Address Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCustomerAddress_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCustomerAddressService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objCustomerAddress = objCustomerAddressService.GetCustomerAddress();
                            if (objCustomerAddress.ReturnFlag == true)
                            {
                                dgCustomerAddress.DataSource = objCustomerAddress.EntityList;
                            }
                            Session.Remove("CustomAddressCD");
                            
                            lstCustomerAddressCodes = new List<string>();
                            foreach (GetAllCustomerAddress CustomerAddressCatalogEntity in objCustomerAddress.EntityList)
                            {
                                lstCustomerAddressCodes.Add(CustomerAddressCatalogEntity.CustomAddressCD.Trim());
                            }
                            Session["CustomAddressCD"] = lstCustomerAddressCodes;
                            
                            DoSearchFilter();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }

        }

        /// <summary>
        /// Item Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCustomerAddress_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedCustomerAddressID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CustomAddress, Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CustomAddress);
                                            SelectItem();
                                            return;
                                        }
                                        chkSearchHomebaseOnly.Enabled = false;
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        ddlImg.Enabled = true;
                                        if (imgFile.ImageUrl.Trim() != "" || lnkFileName.NavigateUrl.Trim() != "")
                                        {
                                            btndeleteImage.Enabled = true;
                                        }
                                        tbImgName.Enabled = true;
                                        //fileUL.Enabled = false; //Commented for removing document name
                                        RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                                        SelectItem();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgCustomerAddress.SelectedIndexes.Clear();
                                //tbCode.ReadOnly = false;
                                //tbCode.BackColor = System.Drawing.Color.White;
                                chkSearchHomebaseOnly.Enabled = false;
                                DisplayInsertForm();
                                if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportICAOCd != string.Empty)
                                {
                                    tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd;
                                    if (UserPrincipal != null && UserPrincipal.Identity._homeBaseId != 0)
                                    {
                                        hdnHomeBaseID.Value = Convert.ToString(UserPrincipal.Identity._homeBaseId);
                                    }
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;

                                }
                                GridEnable(true, false, false);
                                tbImgName.Enabled = true;
                                ddlImg.Enabled = true;
                                btndeleteImage.Enabled = true;
                                //fileUL.Enabled = false;//Commented for removing document name
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                DisableLinks();
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        /// <summary>
        /// Update Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCustomerAddress_UpdateCommand(object source, GridCommandEventArgs e)
        {
            e.Canceled = true;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedCustomerAddressID"] != null)
                        {
                            e.Canceled = true;
                            bool IsValidate = true;
                            if (CheckAlreadyHomeBaseExist())
                            {
                                cvHomeBase.IsValid = false;
                                RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                                IsValidate = false;

                            }
                            if (CheckAlreadyClosestIcaoExist())
                            {
                                cvClosestIcao.IsValid = false;
                                RadAjaxManager1.FocusControl(tbClosestIcao.ClientID); //tbClosestIcao.Focus();
                                IsValidate = false;
                            }
                            if (CheckAlreadyBillCountryExist())
                            {
                                cvCountry.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCountry.ClientID); //tbCountry.Focus();
                                IsValidate = false;
                            }
                            if (CheckAlreadyMetroCodeExist())
                            {
                                cvMetro.IsValid = false;
                                RadAjaxManager1.FocusControl(tbMetro.ClientID); //tbMetro.Focus();
                                IsValidate = false;
                            }
                            if (IsValidate)
                            {
                                e.Canceled = true;
                                using (FlightPakMasterService.MasterCatalogServiceClient objCustomerAddressService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objCustomerAddressService.UpdateCustomerAddress(GetItems());
                                    //For Data Anotation
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        #region Image Upload in Edit Mode
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        foreach (var DicItem in dicImg)
                                        {

                                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "CustomAddress";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString());
                                                Service.UWAWebpageName = "CustomerAddressCatalog.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                objService.AddFWHType(Service);
                                            }
                                        }
                                        #endregion

                                        #region Image Upload in Delete
                                        using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                            foreach (var DicItem in dicImgDelete)
                                            {
                                                objFWHType.UWAFileName = DicItem.Key;
                                                objFWHType.RecordID = Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString());
                                                objFWHType.RecordType = "CustomAddress";
                                                objFWHType.IsDeleted = true;
                                                objFWHType.IsDeleted = true;
                                                objFWHTypeService.DeleteFWHType(objFWHType);
                                            }
                                        }
                                        #endregion

                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.CustomAddress, Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString().Trim()));
                                            LoadControlData();
                                            GridEnable(true, true, true);
                                            SelectItem();
                                            ReadOnlyForm();
                                        }

                                        ShowSuccessMessage();
                                        EnableLinks();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CustomAddress);
                                    }
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        protected void CustomerAddress_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCustomerAddress.ClientSettings.Scrolling.ScrollTop = "0";
                        CustomerAddressPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        //private bool checkAllReadyExist()
        //{
        //    bool returnVal = false;
        //    lstCustomerAddressCodes = (List<string>)Session["CustomAddressCD"];

        //    if (lstCustomerAddressCodes != null && lstCustomerAddressCodes.Equals(tbCode.Text.ToString().Trim()))
        //        return true;
        //    return returnVal;

        //}

        /// <summary>
        /// Update Command for Customer Address Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCustomerAddress_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAlreadyExist())
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyHomeBaseExist())
                        {
                            cvHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                            IsValidate = false;

                        }
                        if (CheckAlreadyClosestIcaoExist())
                        {
                            cvClosestIcao.IsValid = false;
                            RadAjaxManager1.FocusControl(tbClosestIcao.ClientID); //tbClosestIcao.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyBillCountryExist())
                        {
                            cvCountry.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCountry.ClientID); //tbCountry.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyMetroCodeExist())
                        {
                            cvMetro.IsValid = false;
                            RadAjaxManager1.FocusControl(tbMetro.ClientID); //tbMetro.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objCustomerAddressService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objCustomerAddressService.AddCustomerAddress(GetItems());
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    #region Image Upload in New Mode

                                    var InsertedCustomAddressID = objCustomerAddressService.GetCustomerAddressID();
                                    
                                    if (InsertedCustomAddressID != null)
                                    {
                                        foreach (FlightPakMasterService.GetCustomerID FltId in InsertedCustomAddressID.EntityList)
                                        {
                                            hdnCustomAddressID.Value = FltId.CustomAddressID.ToString();
                                        }
                                    }



                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    foreach (var DicItem in dicImg)
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                            Service.FileWarehouseID = 0;
                                            Service.RecordType = "CustomAddress";
                                            Service.UWAFileName = DicItem.Key;
                                            Service.RecordID = Convert.ToInt64(hdnCustomAddressID.Value);
                                            Service.UWAWebpageName = "CustomerAddressCatalog.aspx";
                                            Service.UWAFilePath = DicItem.Value;
                                            Service.IsDeleted = false;
                                            Service.FileWarehouseID = 0;
                                            objService.AddFWHType(Service);
                                        }
                                    }
                                    #endregion

                                    dgCustomerAddress.Rebind();
                                    DefaultSelection(false);
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CustomAddress);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCustomerAddress_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedCustomerAddressID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CustomAddressService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.CustomAddress CustomAddress = new FlightPakMasterService.CustomAddress();
                                string Code = Session["SelectedCustomerAddressID"].ToString();
                                strCustomerAddressCD = "";
                                strCustomerAddressID = "";
                                foreach (GridDataItem Item in dgCustomerAddress.MasterTableView.Items)
                                {
                                    if (Item["CustomAddressID"].Text.Trim() == Code.Trim())
                                    {
                                        strCustomerAddressCD = Item["CustomAddressCD"].Text.Trim();
                                        strCustomerAddressID = Item["CustomAddressID"].Text.Trim();
                                        CustomAddress.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                        break;
                                    }
                                }
                                CustomAddress.CustomAddressCD = strCustomerAddressCD;
                                CustomAddress.CustomAddressID = Convert.ToInt64(strCustomerAddressID);
                                CustomAddress.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.CustomAddress, Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CustomAddress);
                                        return;
                                    }
                                }
                                CustomAddressService.DeleteCustomerAddress(CustomAddress);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                dgCustomerAddress.Rebind();
                                DefaultSelection(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.CustomAddress, Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCustomerAddress_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem Item = dgCustomerAddress.SelectedItems[0] as GridDataItem;
                                chkSearchHomebaseOnly.Enabled = true;
                                Session["SelectedCustomerAddressID"] = Item["CustomAddressID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                    }
                }
            }
        }


        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }


        }

        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();

                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Delay Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgCustomerAddress.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgCustomerAddress.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            chkSearchHomebaseOnly.Enabled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }


        /// <summary>
        /// To check unique Code  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            CheckAlreadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }

        }

        /// <summary>
        /// To check unique code already exists
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyExist()
        {
            bool returnVal = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objservice.GetCustomerAddress().EntityList.Where(x => x.CustomAddressCD.Trim().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                        if (objRetVal.Count() > 0 && objRetVal != null)
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            returnVal = true;
                        }
                        else
                        {                            
                            RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            return returnVal;
        }

        /// <summary>
        /// To check unique Home Base  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbHomeBase.Text != null)
                        {
                            CheckAlreadyHomeBaseExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }

        }

        private bool CheckAlreadyHomeBaseExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbHomeBase.Text != string.Empty) && (tbHomeBase.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.Trim().ToUpper().Equals(tbHomeBase.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvHomeBase.IsValid = false;
                                RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                                RetVal = true;
                            }
                            else
                            {
                                foreach (FlightPakMasterService.GetAllCompanyMaster cm in RetValue)
                                {
                                    hdnHomeBaseID.Value = cm.HomebaseID.ToString();
                                    tbHomeBase.Text = cm.HomebaseCD;
                                }
                                RadAjaxManager1.FocusControl(tbName.ClientID); //tbName.Focus();
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClosestIcao_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbClosestIcao.Text != null)
                        {
                            CheckAlreadyClosestIcaoExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        private bool CheckAlreadyClosestIcaoExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClosestIcao.Text.Trim() != string.Empty) && (tbClosestIcao.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetAirportByAirportICaoID(Convert.ToString(tbClosestIcao.Text)).EntityList.Where(x => x.IcaoID.Trim().ToUpper().Equals(tbClosestIcao.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvClosestIcao.IsValid = false;
                                RadAjaxManager1.FocusControl(tbClosestIcao.ClientID); //tbClosestIcao.Focus();
                                RetVal = true;
                            }

                            else
                            {
                                foreach (FlightPakMasterService.GetAllAirport cm in RetValue)
                                {
                                    hdnAirportID.Value = cm.AirportID.ToString();
                                    tbClosestIcao.Text = cm.IcaoID;
                                }
                                RadAjaxManager1.FocusControl(tbAddr1.ClientID); //tbAddr1.Focus();
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Country_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCountry.Text != null)
                        {
                            CheckAlreadyBillCountryExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        private bool CheckAlreadyBillCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbCountry.Text != string.Empty) && (tbCountry.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper().Equals(tbCountry.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvCountry.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCountry.ClientID); //tbCountry.Focus();

                                RetVal = true;
                            }
                            else
                            {
                                foreach (FlightPakMasterService.Country cm in RetValue)
                                {
                                    hdnCountryID.Value = cm.CountryID.ToString();
                                    tbCountry.Text = cm.CountryCD;
                                }
                                RadAjaxManager1.FocusControl(tbPostal.ClientID); //tbPostal.Focus();
                                RetVal = false;
                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Metro_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbMetro.Text != null)
                        {
                            CheckAlreadyMetroCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        private bool CheckAlreadyMetroCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbMetro.Text != string.Empty) && (tbMetro.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetMetroCityList().EntityList.Where(x => x.MetroCD.Trim().ToUpper().Equals(tbMetro.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvMetro.IsValid = false;
                                RadAjaxManager1.FocusControl(tbMetro.ClientID); //tbMetro.Focus();
                                RetVal = true;
                            }
                            else
                            {
                                foreach (FlightPakMasterService.Metro cm in RetValue)
                                {
                                    hdnMetroID.Value = cm.MetroID.ToString();
                                    tbMetro.Text = cm.MetroCD;
                                }
                                RadAjaxManager1.FocusControl(tbCity.ClientID); //tbCity.Focus();
                                RetVal = false;
                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        /// <summary>
        /// Cancel Delay Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedCustomerAddressID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.CustomAddress, Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString().Trim()));
                                    DefaultSelection(false);
                                }
                            }
                        }
                        chkSearchHomebaseOnly.Enabled = true;
                        GridEnable(true, true, true);
                        DefaultSelection(false);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        private CustomAddress GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.CustomAddress CustomAddress = new FlightPakMasterService.CustomAddress();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    CustomAddress.CustomAddressCD = tbCode.Text.Trim();
                    CustomAddress.Name = tbName.Text;
                    if (hdnSave.Value == "Update")
                    {
                        CustomAddress.CustomAddressID = Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString());
                    }
                    if (!string.IsNullOrEmpty(tbHomeBase.Text))
                    {
                        CustomAddress.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                    }
                    if (!string.IsNullOrEmpty(tbClosestIcao.Text))
                    {
                        CustomAddress.AirportID = Convert.ToInt64(hdnAirportID.Value);
                    }
                    if (!string.IsNullOrEmpty(tbName.Text))
                    {
                        CustomAddress.Name = tbName.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbAddr1.Text))
                    {
                        CustomAddress.Address1 = tbAddr1.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbAddr2.Text))
                    {
                        CustomAddress.Address2 = tbAddr2.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbAddr3.Text))
                    {
                        CustomAddress.Address3 = tbAddr3.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbCity.Text))
                    {
                        CustomAddress.CustomCity = tbCity.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbState.Text))
                    {
                        CustomAddress.CustomState = tbState.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbPostal.Text))
                    {
                        CustomAddress.PostalZipCD = tbPostal.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbFax.Text))
                    {
                        CustomAddress.FaxNum = tbFax.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbPhone.Text))
                    {
                        CustomAddress.PhoneNum = tbPhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbEmail.Text))
                    {
                        CustomAddress.ContactEmailID = tbEmail.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbWebsite.Text))
                    {
                        CustomAddress.Website = tbWebsite.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbMetro.Text))
                    {
                        CustomAddress.MetropolitanArea = tbMetro.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbCountry.Text))
                    {
                        CustomAddress.CountryID = Convert.ToInt64(hdnCountryID.Value);
                    }
                    if (!string.IsNullOrEmpty(tbNotes.Text))
                    {
                        CustomAddress.Remarks = tbNotes.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbContact.Text))
                    {
                        CustomAddress.Contact = tbContact.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbCustom.Text))
                    {
                        CustomAddress.Symbol = tbCustom.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbLatitudeDeg.Text))
                    {
                        CustomAddress.LatitudeDegree = Convert.ToDecimal(tbLatitudeDeg.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(tbLatitudeMin.Text))
                    {
                        CustomAddress.LatitudeMinutes = Convert.ToDecimal(tbLatitudeMin.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(tbLatitudeNS.Text))
                    {
                        CustomAddress.LatitudeNorthSouth = tbLatitudeNS.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbLongitudeDeg.Text))
                    {
                        CustomAddress.LongitudeDegrees = Convert.ToDecimal(tbLongitudeDeg.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(tbLongitudeMin.Text))
                    {
                        CustomAddress.LongitudeMinutes = Convert.ToDecimal(tbLongitudeMin.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(tbLongitudeEW.Text))
                    {
                        CustomAddress.LongitudeEastWest = tbLongitudeEW.Text.Trim();
                    }
                    CustomAddress.IsDeleted = false;
                    if (!string.IsNullOrEmpty(tbBusinessPhone.Text))
                    {
                        CustomAddress.BusinessPhone = tbBusinessPhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbCellPhoneNum.Text))
                    {
                        CustomAddress.CellPhoneNum = tbCellPhoneNum.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbHomeFax.Text))
                    {
                        CustomAddress.HomeFax = tbHomeFax.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbCellPhoneNum2.Text))
                    {
                        CustomAddress.CellPhoneNum2 = tbCellPhoneNum2.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbOtherPhone.Text))
                    {
                        CustomAddress.OtherPhone = tbOtherPhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBusinessEmail.Text))
                    {
                        CustomAddress.BusinessEmail = tbBusinessEmail.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbPersonalEmail.Text))
                    {
                        CustomAddress.PersonalEmail = tbPersonalEmail.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbOtherEmail.Text))
                    {
                        CustomAddress.OtherEmail = tbOtherEmail.Text.Trim();
                    }
                    CustomAddress.IsInActive = chkInactive.Checked;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return CustomAddress;
            }
        }

        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    //tbCode.ReadOnly = true;
                    //tbCode.BackColor = System.Drawing.Color.LightGray;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    tbCode.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCustomerAddressID"] != null)
                    {
                        strCustomerAddressID = "";
                        foreach (GridDataItem Item in dgCustomerAddress.MasterTableView.Items)
                        {
                            if (Item["CustomAddressID"].Text.Trim() == Session["SelectedCustomerAddressID"].ToString().Trim())
                            {
                                tbCode.Text = Item.GetDataKeyValue("CustomAddressCD").ToString().Trim();
                                if (Item.GetDataKeyValue("Name") != null)
                                {
                                    tbName.Text = Item.GetDataKeyValue("Name").ToString().Trim();
                                }
                                else
                                {
                                    tbName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Address1") != null)
                                {
                                    tbAddr1.Text = Item.GetDataKeyValue("Address1").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr1.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Address2") != null)
                                {
                                    tbAddr2.Text = Item.GetDataKeyValue("Address2").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr2.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Address3") != null)
                                {
                                    tbAddr3.Text = Item.GetDataKeyValue("Address3").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr3.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HomeBaseCD") != null)
                                {
                                    tbHomeBase.Text = Item.GetDataKeyValue("HomeBaseCD").ToString();
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ClosestICAO") != null)
                                {
                                    tbClosestIcao.Text = Item.GetDataKeyValue("ClosestICAO").ToString();
                                }
                                else
                                {
                                    tbClosestIcao.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("CustomCity") != null)
                                {
                                    tbCity.Text = Item.GetDataKeyValue("CustomCity").ToString();
                                }
                                else
                                {
                                    tbCity.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CustomState") != null)
                                {
                                    tbState.Text = Item.GetDataKeyValue("CustomState").ToString().Trim();
                                }
                                else
                                {
                                    tbState.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PostalZipCD") != null)
                                {
                                    tbPostal.Text = Item.GetDataKeyValue("PostalZipCD").ToString().Trim();
                                }
                                else
                                {
                                    tbPostal.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("NationalityCD") != null)
                                {
                                    tbCountry.Text = Item.GetDataKeyValue("NationalityCD").ToString();
                                }
                                else
                                {
                                    tbCountry.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("MetropolitanArea") != null)
                                {
                                    tbMetro.Text = Item.GetDataKeyValue("MetropolitanArea").ToString();
                                }
                                else
                                {
                                    tbMetro.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Contact") != null)
                                {
                                    tbContact.Text = Item.GetDataKeyValue("Contact").ToString();
                                }
                                else
                                {
                                    tbContact.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PhoneNum") != null)
                                {
                                    tbPhone.Text = Item.GetDataKeyValue("PhoneNum").ToString();
                                }
                                else
                                {
                                    tbPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("FaxNum") != null)
                                {
                                    tbFax.Text = Item.GetDataKeyValue("FaxNum").ToString();
                                }
                                else
                                {
                                    tbFax.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ContactEmailID") != null)
                                {
                                    tbEmail.Text = Item.GetDataKeyValue("ContactEmailID").ToString();
                                }
                                else
                                {
                                    tbEmail.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Website") != null)
                                {
                                    tbWebsite.Text = Item.GetDataKeyValue("Website").ToString();
                                }
                                else
                                {
                                    tbWebsite.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("Remarks") != null)
                                {
                                    tbNotes.Text = Item.GetDataKeyValue("Remarks").ToString();
                                }
                                else
                                {
                                    tbNotes.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Symbol") != null)
                                {
                                    tbCustom.Text = Item.GetDataKeyValue("Symbol").ToString();
                                }
                                else
                                {
                                    tbCustom.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("LatitudeDegree") != null)
                                {
                                    tbLatitudeDeg.Text = Item.GetDataKeyValue("LatitudeDegree").ToString();
                                }
                                else
                                {
                                    tbLatitudeDeg.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LatitudeMinutes") != null)
                                {
                                    tbLatitudeMin.Text = Item.GetDataKeyValue("LatitudeMinutes").ToString();
                                }
                                else
                                {
                                    tbLatitudeMin.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LatitudeNorthSouth") != null)
                                {
                                    tbLatitudeNS.Text = Item.GetDataKeyValue("LatitudeNorthSouth").ToString();
                                }
                                else
                                {
                                    tbLatitudeNS.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LongitudeDegrees") != null)
                                {
                                    tbLongitudeDeg.Text = Item.GetDataKeyValue("LongitudeDegrees").ToString();
                                }
                                else
                                {
                                    tbLongitudeDeg.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LongitudeMinutes") != null)
                                {
                                    tbLongitudeMin.Text = Item.GetDataKeyValue("LongitudeMinutes").ToString();
                                }
                                else
                                {
                                    tbLongitudeMin.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LongitudeEastWest") != null)
                                {
                                    tbLongitudeEW.Text = Item.GetDataKeyValue("LongitudeEastWest").ToString();
                                }
                                else
                                {
                                    tbLongitudeEW.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("BusinessPhone") != null)
                                {
                                    tbBusinessPhone.Text = Item.GetDataKeyValue("BusinessPhone").ToString();
                                }
                                else
                                {
                                    tbBusinessPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CellPhoneNum") != null)
                                {
                                    tbCellPhoneNum.Text = Item.GetDataKeyValue("CellPhoneNum").ToString();
                                }
                                else
                                {
                                    tbCellPhoneNum.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HomeFax") != null)
                                {
                                    tbHomeFax.Text = Item.GetDataKeyValue("HomeFax").ToString();
                                }
                                else
                                {
                                    tbHomeFax.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CellPhoneNum2") != null)
                                {
                                    tbCellPhoneNum2.Text = Item.GetDataKeyValue("CellPhoneNum2").ToString();
                                }
                                else
                                {
                                    tbCellPhoneNum2.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("OtherPhone") != null)
                                {
                                    tbOtherPhone.Text = Item.GetDataKeyValue("OtherPhone").ToString();
                                }
                                else
                                {
                                    tbOtherPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BusinessEmail") != null)
                                {
                                    tbBusinessEmail.Text = Item.GetDataKeyValue("BusinessEmail").ToString();
                                }
                                else
                                {
                                    tbBusinessEmail.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PersonalEmail") != null)
                                {
                                    tbPersonalEmail.Text = Item.GetDataKeyValue("PersonalEmail").ToString();
                                }
                                else
                                {
                                    tbPersonalEmail.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("OtherEmail") != null)
                                {
                                    tbOtherEmail.Text = Item.GetDataKeyValue("OtherEmail").ToString();
                                }
                                else
                                {
                                    tbOtherEmail.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                #region Get Image from Database
                                CreateDictionayForImgUpload();
                                imgFile.ImageUrl = "";
                                ImgPopup.ImageUrl = null;
                                lnkFileName.NavigateUrl = "";
                                using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var ObjRetImg = ObjImgService.GetFileWarehouseList("CustomAddress", Convert.ToInt64(Session["SelectedCustomerAddressID"].ToString())).EntityList;
                                    ddlImg.Items.Clear();
                                    ddlImg.Text = "";
                                    int i = 0;
                                    foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                    {
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.UWAFileName);
                                        ddlImg.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                                        dicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                                        if (i == 0)
                                        {
                                            byte[] picture = fwh.UWAFilePath;

                                            //Ramesh: Set the URL for image file or link based on the file type
                                            SetURL(fwh.UWAFileName, picture);

                                            ////start of modification for image issue
                                            //if (hdnBrowserName.Value == "Chrome")
                                            //{
                                            //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                            //}
                                            //else
                                            //{
                                            //    Session["Base64"] = picture;
                                            //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                            //}
                                            ////end of modification for image issue
                                        }
                                        i = i + 1;
                                    }
                                    if (ddlImg.Items.Count > 0)
                                    {
                                        ddlImg.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                        lnkFileName.NavigateUrl = "";
                                    }
                                }
                                #endregion
                                lbColumnName1.Text = "Custom Address Code";
                                lbColumnName2.Text = "Name";
                                lbColumnValue1.Text = Item["CustomAddressCD"].Text;
                                lbColumnValue2.Text = Item["Name"].Text;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCustomerAddress.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = dgCustomerAddress.SelectedItems[0] as GridDataItem;
                        Label lbLastUpdatedUser = (Label)dgCustomerAddress.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                        }
                        LoadControlData();
                        EnableForm(false);
                    }
                    else
                    {
                        DefaultSelection(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        //protected void ReadOnlyForm()
        //{
        //    Session.Remove("SelectedItem");
        //    Session["SelectedItem"] = (GridDataItem)dgCustomerAddress.SelectedItems[0];
        //    GridDataItem item = (GridDataItem)Session["SelectedItem"];
        //    tbCode.Text = item.GetDataKeyValue("CustomAddressCD").ToString().Trim();
        //    if (item.GetDataKeyValue("Name") != null)
        //    {
        //        tbName.Text = item.GetDataKeyValue("Name").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbName.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Address1") != null)
        //    {
        //        tbAddr1.Text = item.GetDataKeyValue("Address1").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbAddr1.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Address2") != null)
        //    {
        //        tbAddr2.Text = item.GetDataKeyValue("Address2").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbAddr2.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Address3") != null)
        //    {
        //        tbAddr3.Text = item.GetDataKeyValue("Address3").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbAddr3.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("HomeBaseCD") != null)
        //    {
        //        tbHomeBase.Text = item.GetDataKeyValue("HomeBaseCD").ToString();
        //    }
        //    else
        //    {
        //        tbHomeBase.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("ClosestICAO") != null)
        //    {
        //        tbClosestIcao.Text = item.GetDataKeyValue("ClosestICAO").ToString();
        //    }
        //    else
        //    {
        //        tbClosestIcao.Text = string.Empty;
        //    }

        //    if (item.GetDataKeyValue("CustomCity") != null)
        //    {
        //        tbCity.Text = item.GetDataKeyValue("CustomCity").ToString();
        //    }
        //    else
        //    {
        //        tbCity.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("CustomState") != null)
        //    {
        //        tbState.Text = item.GetDataKeyValue("CustomState").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbState.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("PostalZipCD") != null)
        //    {
        //        tbPostal.Text = item.GetDataKeyValue("PostalZipCD").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbPostal.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("CountryCD") != null)
        //    {
        //        tbCountry.Text = item.GetDataKeyValue("CountryCD").ToString();
        //    }
        //    else
        //    {
        //        tbCountry.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("MetropolitanArea") != null)
        //    {
        //        tbMetro.Text = item.GetDataKeyValue("MetropolitanArea").ToString();
        //    }
        //    else
        //    {
        //        tbMetro.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Contact") != null)
        //    {
        //        tbContact.Text = item.GetDataKeyValue("Contact").ToString();
        //    }
        //    else
        //    {
        //        tbContact.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("PhoneNum") != null)
        //    {
        //        tbPhone.Text = item.GetDataKeyValue("PhoneNum").ToString();
        //    }
        //    else
        //    {
        //        tbPhone.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("FaxNum") != null)
        //    {
        //        tbFax.Text = item.GetDataKeyValue("FaxNum").ToString();
        //    }
        //    else
        //    {
        //        tbFax.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("ContactEmailID") != null)
        //    {
        //        tbEmail.Text = item.GetDataKeyValue("ContactEmailID").ToString();
        //    }
        //    else
        //    {
        //        tbEmail.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Website") != null)
        //    {
        //        tbWebsite.Text = item.GetDataKeyValue("Website").ToString();
        //    }
        //    else
        //    {
        //        tbWebsite.Text = string.Empty;
        //    }

        //    if (item.GetDataKeyValue("Remarks") != null)
        //    {
        //        tbNotes.Text = item.GetDataKeyValue("Remarks").ToString();
        //    }
        //    else
        //    {
        //        tbNotes.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Symbol") != null)
        //    {
        //        tbCustom.Text = item.GetDataKeyValue("Symbol").ToString();
        //    }
        //    else
        //    {
        //        tbCustom.Text = string.Empty;
        //    }

        //    if (item.GetDataKeyValue("LatitudeDegree") != null)
        //    {
        //        tbLatitudeDeg.Text = item.GetDataKeyValue("LatitudeDegree").ToString();
        //    }
        //    else
        //    {
        //        tbLatitudeDeg.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LatitudeMinutes") != null)
        //    {
        //        tbLatitudeMin.Text = item.GetDataKeyValue("LatitudeMinutes").ToString();
        //    }
        //    else
        //    {
        //        tbLatitudeMin.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LatitudeNorthSouth") != null)
        //    {
        //        tbLatitudeNS.Text = item.GetDataKeyValue("LatitudeNorthSouth").ToString();
        //    }
        //    else
        //    {
        //        tbLatitudeNS.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LongitudeDegrees") != null)
        //    {
        //        tbLongitudeDeg.Text = item.GetDataKeyValue("LongitudeDegrees").ToString();
        //    }
        //    else
        //    {
        //        tbLongitudeDeg.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LongitudeMinutes") != null)
        //    {
        //        tbLongitudeMin.Text = item.GetDataKeyValue("LongitudeMinutes").ToString();
        //    }
        //    else
        //    {
        //        tbLongitudeMin.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LongitudeEastWest") != null)
        //    {
        //        tbLongitudeEW.Text = item.GetDataKeyValue("LongitudeEastWest").ToString();
        //    }
        //    else
        //    {
        //        tbLongitudeEW.Text = string.Empty;
        //    }


        //    tbCode.ReadOnly = true;
        //    tbCode.BackColor = System.Drawing.Color.LightGray;
        //    EnableForm(false);
        //}

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbName.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    tbClosestIcao.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbAddr3.Text = string.Empty;

                    tbCustom.Text = string.Empty;
                    tbContact.Text = string.Empty;
                    tbLatitudeDeg.Text = string.Empty;
                    tbLatitudeMin.Text = string.Empty;
                    tbLatitudeNS.Text = string.Empty;
                    tbLongitudeDeg.Text = string.Empty;
                    tbLongitudeMin.Text = string.Empty;
                    tbLongitudeEW.Text = string.Empty;

                    #region Image clear
                    CreateDictionayForImgUpload();
                    fileUL.Enabled = false;
                    ddlImg.Enabled = false;
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    ImgPopup.ImageUrl = null;
                    lnkFileName.NavigateUrl = "";
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbNotes.Text = "";
                    tbImgName.Text = "";
                    #endregion

                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbCountry.Text = string.Empty;
                    tbMetro.Text = string.Empty;
                    tbPostal.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbNotes.Text = string.Empty;

                    tbBusinessPhone.Text = string.Empty;
                    tbCellPhoneNum.Text = string.Empty;
                    tbHomeFax.Text = string.Empty;
                    tbCellPhoneNum2.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;

                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbName.Enabled = enable;
                    tbHomeBase.Enabled = enable;
                    tbClosestIcao.Enabled = enable;
                    tbAddr1.Enabled = enable;
                    tbAddr2.Enabled = enable;
                    tbAddr3.Enabled = enable;

                    tbCustom.Enabled = enable;
                    tbContact.Enabled = enable;
                    tbLatitudeDeg.Enabled = enable;
                    tbLatitudeMin.Enabled = enable;
                    tbLatitudeNS.Enabled = enable;
                    tbLongitudeDeg.Enabled = enable;
                    tbLongitudeMin.Enabled = enable;
                    tbLongitudeEW.Enabled = enable;

                    ddlImg.Enabled = enable;
                    tbImgName.Enabled = enable;
                    btndeleteImage.Enabled = enable;
                    btndeleteImage.Visible = enable;
                    fileUL.Enabled = enable;

                    tbCity.Enabled = enable;
                    tbState.Enabled = enable;
                    tbCountry.Enabled = enable;
                    tbMetro.Enabled = enable;
                    tbPostal.Enabled = enable;
                    tbPhone.Enabled = enable;
                    tbFax.Enabled = enable;
                    tbWebsite.Enabled = enable;
                    tbEmail.Enabled = enable;
                    tbNotes.Enabled = enable;

                    btnCountry.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnCancelTop.Visible = enable;
                    btnClosestIcao.Enabled = enable;
                    btnHomeBase.Enabled = enable;
                    btnMetro.Enabled = enable;
                    btnCustomSymbol.Enabled = enable;
                    btnSaveChanges.Visible = enable;
                    btnSaveChangesTop.Visible = enable;

                    tbBusinessPhone.Enabled = enable;
                    tbCellPhoneNum.Enabled = enable;
                    tbHomeFax.Enabled = enable;
                    tbCellPhoneNum2.Enabled = enable;
                    tbOtherPhone.Enabled = enable;
                    tbBusinessEmail.Enabled = enable;
                    tbPersonalEmail.Enabled = enable;
                    tbOtherEmail.Enabled = enable;

                    chkInactive.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void GridEnable(bool Add, bool Edit, bool Delete)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgCustomerAddress.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDelCtl = (LinkButton)dgCustomerAddress.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgCustomerAddress.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddCustomerAddressCatalog))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteCustomerAddressCatalog))
                    {
                        lbtnDelCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDelCtl.Enabled = true;
                            lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtnDelCtl.Enabled = false;
                            lbtnDelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditCustomerAddressCatalog))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchFilter();
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        protected void DoSearchFilter()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objCustomerAddressService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objCustomerAddress = objCustomerAddressService.GetCustomerAddress();

                List<FlightPakMasterService.GetAllCustomerAddress> lstCustomer = new List<FlightPakMasterService.GetAllCustomerAddress>();
                lstCustomer = objCustomerAddress.EntityList.ToList<GetAllCustomerAddress>();
                
                if (lstCustomer.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstCustomer = lstCustomer.Where(x => x.IsInActive == false).ToList<GetAllCustomerAddress>(); }
                    
                    if (chkSearchHomebaseOnly.Checked == true) { lstCustomer = lstCustomer.Where(x => (x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd) && (x.IsDeleted == false)).ToList<GetAllCustomerAddress>(); }
                    dgCustomerAddress.DataSource = lstCustomer;
                    //dgCustomerAddress.DataBind();
                }

            }
        }
        
        #endregion
        //private void GridEnable(bool add, bool edit, bool delete)
        //{

        //    LinkButton insertCtl, editCtl, delCtl;
        //    insertCtl = (LinkButton)dgCustomerAddress.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
        //    delCtl = (LinkButton)dgCustomerAddress.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
        //    editCtl = (LinkButton)dgCustomerAddress.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
        //    if (add)
        //    {
        //        insertCtl.Enabled = true;
        //    }
        //    else
        //        insertCtl.Enabled = false;
        //    if (delete)
        //    {
        //        delCtl.Enabled = true;
        //        delCtl.OnClientClick = "javascript:return ProcessDelete();";

        //    }
        //    else
        //    {
        //        delCtl.Enabled = false;
        //        delCtl.OnClientClick = string.Empty;
        //    }
        //    if (edit)
        //    {
        //        editCtl.Enabled = true;
        //        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
        //    }
        //    else
        //    {
        //        editCtl.Enabled = false;
        //        editCtl.OnClientClick = string.Empty;
        //    }
        //}

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {            
            chkSearchActiveOnly.Enabled = false;
            chkSearchHomebaseOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
            chkSearchHomebaseOnly.Enabled = true;
        }
        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {

                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCustomerAddress.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    chkInactive.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }

        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var CustomAddressValue = FPKMstService.GetCustomerAddress();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CustomAddressValue);
            List<FlightPakMasterService.GetAllCustomerAddress> filteredList = GetFilteredList(CustomAddressValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CustomAddressID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCustomerAddress.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCustomerAddress.CurrentPageIndex = PageNumber;
            dgCustomerAddress.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllCustomerAddress CustomAddressValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedCustomerAddressID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CustomAddressValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().CustomAddressID;
                Session["SelectedCustomerAddressID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllCustomerAddress> GetFilteredList(ReturnValueOfGetAllCustomerAddress CustomAddressValue)
        {
            List<FlightPakMasterService.GetAllCustomerAddress> filteredList = new List<FlightPakMasterService.GetAllCustomerAddress>();

            if (CustomAddressValue.ReturnFlag == true)
            {
                filteredList = CustomAddressValue.EntityList.ToList();
            }

            if (((chkSearchActiveOnly.Checked == true) || (chkSearchHomebaseOnly.Checked == true)))
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetAllCustomerAddress>(); }
                    if (chkSearchHomebaseOnly.Checked == true) { filteredList = filteredList.Where(x => (x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd) && (x.IsDeleted == false)).ToList<GetAllCustomerAddress>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCustomerAddress.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgCustomerAddress.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            //Ramesh: Code to allow any file type for upload control.
            int iIndex = filename.Trim().IndexOf('.');
            //string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);  
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            if (regMatch.Success)
            {
                if (Request.UserAgent.Contains("Chrome"))
                {
                    hdnBrowserName.Value = "Chrome";
                }
                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                else
                {
                    Session["Base64"] = filedata;
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid();
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgFile.Visible = false;
                lnkFileName.Visible = true;
                Session["Base64"] = filedata;
                lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();
            }
        }
    }
}