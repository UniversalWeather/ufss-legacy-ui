﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewRosterAddInfoPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                      //  RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrewRosterAddInfo, dgCrewRosterAddInfo, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCrewRosterAddInfo.ClientID));
                     //   RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            lbMessage.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAdditionalInfo);
                }
            }

        }

        /// <summary>
        /// Method to Bind Grid Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewRosterAddInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CrewRosterAddInfoValue = CrewRosterAddInfoService.GetCrewRosterAddInfoList();
                            if (CrewRosterAddInfoValue.ReturnFlag == true)
                            {
                                dgCrewRosterAddInfo.DataSource = CrewRosterAddInfoValue.EntityList.Where(x => x.IsDeleted == false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAdditionalInfo);
                }
            }

        }

        /// <summary>
        /// Method to trigger Grid Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAdditionalInfo);
                }
            }

        }

        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAdditionalInfo);
                }
            }

        }

        /// <summary>
        /// Method to Bind into "CrewNewAddlInfo" session, and 
        /// Call "CloseAndRebind" javascript function to Close and
        /// Rebind the selected record into parent page.
        /// </summary>
        private void InsertSelectedRow()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewRosterAddInfo.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgCrewRosterAddInfo.SelectedItems[0];

                        if (CheckIfExists(Item.GetDataKeyValue("CrewInfoCD").ToString()))
                        {
                            lbMessage.Text = "Warning, Additional Info. Code Already Exists.";
                            lbMessage.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            bool IsExist = false;
                            List<FlightPakMasterService.GetCrewDefinition> CrewDefinitionInfo = (List<FlightPakMasterService.GetCrewDefinition>)Session["CrewAddInfo"];
                            for (int Index = 0; Index < CrewDefinitionInfo.Count; Index++)
                            {
                                if (CrewDefinitionInfo[Index].CrewInfoCD == Item.GetDataKeyValue("CrewInfoCD").ToString())
                                {
                                    CrewDefinitionInfo[Index].InformationValue = string.Empty;
                                    CrewDefinitionInfo[Index].IsDeleted = false;
                                    IsExist = true;
                                }
                            }

                            Session["CrewAddInfo"] = CrewDefinitionInfo;

                            if (IsExist == false)
                            {
                                List<GetCrewDefinition> CrewList = new List<GetCrewDefinition>();
                                GetCrewDefinition crewDef = new GetCrewDefinition();
                                crewDef.CrewInfoID = Convert.ToInt64(Item.GetDataKeyValue("CrewInfoID").ToString());
                                crewDef.CrewInfoCD = Item.GetDataKeyValue("CrewInfoCD").ToString();
                                crewDef.InformationDESC = Item.GetDataKeyValue("CrewInformationDescription").ToString();
                                crewDef.InformationValue = string.Empty;
                                crewDef.IsReptFilter = false;
                                crewDef.IsDeleted = false;
                                CrewList.Add(crewDef);
                                Session["CrewNewAddlInfo"] = CrewList;
                            }
                           // InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow();oWnd.close(); ", true);
                            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "CloseAndRebind('navigateToInserted');", true);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }

        /// <summary>
        /// Function to Check if Crew Information Code alerady exists or not, by passing Crew Code
        /// </summary>
        /// <returns>True / False</returns>
        private bool CheckIfExists(string InfoCode)
        {
            bool ReturnValue = false;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InfoCode))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewAddInfo"] != null)
                    {
                        List<GetCrewDefinition> CrewDefinitionList = (List<GetCrewDefinition>)Session["CrewAddInfo"];
                        var CrewAdditionalInfoValue = (from crew in CrewDefinitionList
                                                       where crew.CrewInfoCD.ToUpper().Trim() == InfoCode.ToUpper().Trim()
                                                       && crew.IsDeleted == false   // added
                                                       select crew);

                        if (CrewAdditionalInfoValue.Count() > 0)
                        { ReturnValue = true; }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

                return ReturnValue;
            }

        }

        protected void dgCrewRosterAddInfo_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                string Code = "";
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        using (FlightPakMasterService.MasterCatalogServiceClient CrewInformationService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FlightPakMasterService.CrewInformation CrewInformation = new FlightPakMasterService.CrewInformation();
                            Code = dgCrewRosterAddInfo.SelectedValues["CrewInfoID"].ToString();//Session["SelectedCrewInfoID"].ToString();
                            string strCrewAddInfoCD = "";
                            string strCrewAddInfoID = "";
                            foreach (GridDataItem Item in dgCrewRosterAddInfo.MasterTableView.Items)
                            {
                                if (Item["CrewInfoID"].Text.Trim() == Code.Trim())
                                {
                                    strCrewAddInfoCD = Item["CrewInfoCD"].Text.Trim();
                                    strCrewAddInfoID = Item["CrewInfoID"].Text.Trim();
                                    CrewInformation.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                    break;
                                }
                            }
                            CrewInformation.CrewInfoCD = strCrewAddInfoCD;
                            CrewInformation.CrewInfoID = Convert.ToInt64(strCrewAddInfoID);
                            CrewInformation.IsDeleted = true;
                            //Lock will happen from UI
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.CrewInformation, Convert.ToInt64(Code));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewInformation);
                                    return;
                                }
                                CrewInformationService.DeleteCrewRosterAddInfo(CrewInformation);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                dgCrewRosterAddInfo.Rebind();
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    if (Code != "")
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            //Unlock should happen from Service Layer 
                            var returnValue1 = CommonService.UnLock(EntitySet.Database.CrewInformation, Convert.ToInt64(Code));
                        }
                    }
                }
            }
        }

        protected void dgCrewRosterAddInfo_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewInformation);
                }
            }
        }

    }
}
