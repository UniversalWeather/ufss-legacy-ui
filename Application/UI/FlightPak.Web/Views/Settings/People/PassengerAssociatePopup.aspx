﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PassengerAssociatePopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.PassengerAssociatePopup" ClientIDMode="AutoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select Associated with PAX</title>
    
</head>
<body>
    <%=Styles.Render("~/bundles/jqgrid") %>
    <%=Scripts.Render("~/bundles/jquery") %>
    <%=Scripts.Render("~/bundles/sitecommon") %>
    <%=Scripts.Render("~/bundles/jqgridjs") %>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var jqGridPassengers = "#gridPassenger";
            var selectedRowData = "";
            var isopenlookup = false;
            var ismultiSelect = false;

            $(document).ready(function () {

                $.extend(jQuery.jgrid.defaults, {
                    prmNames: {
                        page: "page", rows: "size", order: "dir", sort: "sort"
                    }
                });


                selectedRowData =$.trim(decodeURI( getQuerystring("PaxCode", "")));
                if (getQuerystring("cliendid", "") != "") {
                    $("#clientId").val(getQuerystring("cliendid", ""));
                }


                if (selectedRowData != "") {
                    isopenlookup = true;
                }

                $("#btnSubmit").click(function () {
                    var selectedRowId = jQuery(jqGridPassengers).jqGrid('getGridParam', 'selrow');
                    if (IsNullOrEmpty(selectedRowId)) {
                        jAlert("Please select a Passenger.");
                        return;
                    }
                    returnToParent();
                });

                jQuery(jqGridPassengers).jqGrid({
                    url: '/Views/Utilities/ApiCallerwithFilter.aspx',
                    mtype: 'GET',
                    datatype: "json",
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                    serializeGridData: function (postData) {
                        $(jqGridPassengers).jqGrid("clearGridData");
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }                        
                        postData.apiType = 'fss';
                        postData.method = 'associatepassengerslist';
                        
                        postData.passengerId = 0;
                        postData.passengerCD = function () { if (ismultiSelect && selectedRowData.length > 0) { return passengerCD[0]; } else { return selectedRowData; } };                        
                        
                        postData.clientId = $("#clientId").val();
                        postData.markSelectedRecord = isopenlookup;
                        isopenlookup = false;
                        return postData;
                    },
                    height: 400,
                    width: 700,
                    autowidth: false,
                    shrinkToFit: false,
                    rowNum: $("#rowNum").val(),                    
                    viewrecords:true,                    
                    pager: "pg_gridPager",
                    enableclear: true,
                    emptyrecords: "No records to view.",
                    colNames: ['PassengerRequestorID', 'Code', "PAX Name", "Dept", "Dept.Desc", "Phone", "Home Base",'type'],
                    colModel: [
                                { name: 'PassengerRequestorID', index: 'PassengerRequestorID', key: true, hidden: true, sortable: false },
                                { name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', width: 80, sortable: true},
                                { name: 'PassengerName', index: 'PassengerName', width: 150, sortable: true, search: true },
                                { name: 'DepartmentCD', index: 'DepartmentCD', width: 150, sortable: true, search: true },
                                { name: 'DepartmentName', index: 'DepartmentName', width: 150, sortable: true, search: true },                                
                                { name: 'AdditionalPhoneNum', index: 'AdditionalPhoneNum', width: 150, sortable: true, search: true},
                                { name: 'HomeBaseCD', index: 'HomeBaseCD', width: 150, sortable: true, search: true },
                                { name: 'IsEmployeeType', index: 'IsEmployeeType', width: 150, sortable: true, search: true }
                    ],
                    afterInsertRow: function (rowid, rowObject) {
                        if (!IsNullOrEmpty(rowObject.PassengerRequestorCD) && rowObject.PassengerRequestorCD == selectedRowData) {
                            $(this).find(".selected").removeClass('selected');
                            $(this).find('.ui-state-highlight').addClass('selected');
                            $(jqGridPassengers).setSelection(rowid, true);
                        }
                    },
                    ondblClickRow: function (rowId) {
                        returnToParent();
                    },                                      
                    loadComplete: function (rowData) {
                        
                    }
                });
                $("#pagesizebox").insertBefore('.ui-paging-info');
                $(jqGridPassengers).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

            });
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();   
                var selectedRowId = jQuery(jqGridPassengers).jqGrid('getGridParam', 'selrow');

                if (IsNullOrEmpty(selectedRowId)) {
                    jAlert("Please select a Passenger.");
                    return;
                }
                var rowData = jQuery(jqGridPassengers).getRowData(selectedRowId);

                if (!IsNullOrEmpty(selectedRowId)) {                    
                    oArg.PassengerRequestorCD = GetValidatedValue(rowData, "PassengerRequestorCD");
                    oArg.PassengerRequestorID = GetValidatedValue(rowData, "PassengerRequestorID");
                    oArg.PassengerName = GetValidatedValue(rowData, "PassengerName");

                }                
                else {
                    oArg.PassengerRequestorCD = "";
                    oArg.PassengerRequestorID = "";
                    oArg.PassengerName = "";
                }

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="jqgrid">
        <input type="hidden" value="" id="clientId" />
        <table class="box1">
            <tr>
                <td>
                    <table id="gridPassenger" style="width: 981px !important;" class="table table-striped table-hover table-bordered"></table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="grid_icon">
                        <div role="group" id="pg_gridPager"></div>
                        <div id="pagesizebox">
                            <span>Page Size:</span>
                            <input class="psize" id="rowNum" type="text" value="50" maxlength="5" />
                            <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqGridPassengers, $('#rowNum')); return false;" />
                        </div>
                    </div>
                    <div style="padding: 5px 5px; text-align: right;">
                        <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                    </div>
                </td>
            </tr>
        </table>

    </div>
    </form>
</body>
</html>
