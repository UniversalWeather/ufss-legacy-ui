﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewDutyTypePopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.CrewDutyTypePopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="FlightPak.Common.Constants" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Crew DutyType</title>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            //this function is used to get the selected code
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCrewDutyType.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "DutyTypeCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "DutyTypesDescription")
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "ValuePTS")
                        var cell4 = MasterTable.getCellByColumnUniqueName(row, "IsCrewDuty")
                        var cell5 = MasterTable.getCellByColumnUniqueName(row, "DutyTypeID")
                        var cell6 = MasterTable.getCellByColumnUniqueName(row, "DutyStartTM")
                        var cell7 = MasterTable.getCellByColumnUniqueName(row, "DutyEndTM")
                        if (i == 0) {
                            oArg.DutyTypeCD = cell1.innerHTML + ",";
                            oArg.DutyTypesDescription = cell2.innerHTML;
                            oArg.ValuePTS = cell3.innerHTML;
                            oArg.IsCrewDuty = cell4.innerHTML;
                            oArg.DutyTypeID = cell5.innerHTML;
                            oArg.DutyStartTM = cell6.innerHTML;
                            oArg.DutyEndTM = cell7.innerHTML;
                        }
                        else {
                            oArg.DutyTypeCD += cell1.innerHTML + ",";
                            oArg.DutyTypesDescription = cell2.innerHTML;
                            oArg.ValuePTS = cell3.innerHTML;
                            oArg.IsCrewDuty = cell4.innerHTML;
                            oArg.DutyTypeID = cell5.innerHTML;
                            oArg.DutyStartTM = cell6.innerHTML;
                            oArg.DutyEndTM = cell7.innerHTML;
                        }
                    }
                }

                else {
                    oArg.DutyTypeCD = "";
                    oArg.DutyTypesDescription = "";
                    oArg.ValuePTS = "";
                    oArg.IsCrewDuty = "";
                    oArg.DutyTypeID = "";
                    oArg.DutyStartTM = "";
                    oArg.DutyEndTM = "";
                }
                if (oArg.DutyTypeCD != "")
                    oArg.DutyTypeCD = oArg.DutyTypeCD.substring(0, oArg.DutyTypeCD.length - 1)
                else
                    oArg.DutyTypeCD = "";
                oArg.Arg1 = oArg.DutyTypeCD;
                oArg.CallingButton = "CrewDutyTypeCD";
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }

            }
            function rebindgrid() {
                var masterTable = $find("<%= dgCrewDutyType.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            function GetGridId() {
                return $find("<%= dgCrewDutyType.ClientID %>");
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        </telerik:RadWindowManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgCrewDutyType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewDutyType" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewDutyType" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadGrid ID="dgCrewDutyType" runat="server" AllowMultiRowSelection="true"
            AllowSorting="true" OnNeedDataSource="dgCrewDutyType_BindData" AutoGenerateColumns="false"
            OnItemDataBound="dgCrewDutyType_ItemDataBound" Height="341px" PageSize="10" AllowPaging="true"
            Width="580px" PagerStyle-AlwaysVisible="true" OnItemCommand="dgCrewDutyType_ItemCommand" OnItemCreated="dgCrewDutyType_ItemCreated"
            OnDeleteCommand="dgCrewDutyType_DeleteCommand" OnPreRender="dgCrewDutyType_PreRender" >
            <MasterTableView DataKeyNames="DutyTypeCD,DutyTypesDescription,ValuePTS,IsCrewDuty,BackgroundCustomColor,ForeGrndCustomColor,DutyTypeID,DutyStartTM,DutyEndTM"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="DutyTypeCD" HeaderText="Code" CurrentFilterFunction="StartsWith"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="80px"
                        FilterControlWidth="60px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DutyTypesDescription" HeaderText="Crew Duty Type"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith" ShowFilterIcon="false"
                        HeaderStyle-Width="260px" FilterControlWidth="240px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ValuePTS" HeaderText="Value Pts" CurrentFilterFunction="EqualTo"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="80px"
                        FilterControlWidth="60px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsCrewDuty" HeaderText="Crew Duty" CurrentFilterFunction="EqualTo"
                        AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="60px">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridBoundColumn DataField="DutyTypeID" HeaderText="DutyTypeID" CurrentFilterFunction="EqualTo"
                        ShowFilterIcon="false" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DutyStartTM" HeaderText="DutyStartTM" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DutyEndTM" HeaderText="DutyEndTM" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" Display="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div class="grid_icon">
                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                            CommandName="InitInsert" Visible='<%# ShowCRUD && IsUIReports && IsAuthorized(Permission.Database.AddCrewDutyTypes)%>'></asp:LinkButton>
                        <%-- --%>
                        <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                            CommandName="Edit" Visible='<%# ShowCRUD && IsUIReports && IsAuthorized(Permission.Database.EditCrewDutyTypes)%>'
                            OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                        <%-- --%>
                        <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgCrewDutyType', 'radCrewDutyTypePopup');"
                            CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# ShowCRUD && IsUIReports && IsAuthorized(Permission.Database.DeleteCrewDutyTypes)%>'></asp:LinkButton>
                        <%-- CommandName="DeleteSelected" --%>
                    </div>
                    <div class="grd_ok">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button okButton">
                            OK</button>
                    </div>
                     <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                    visible="false">
                                    Use CTRL key to multi select</div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick="returnToParent"  />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
