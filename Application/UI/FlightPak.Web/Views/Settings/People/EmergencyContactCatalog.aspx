﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="EmergencyContactCatalog.aspx.cs" ClientIDMode="AutoID"
    Inherits="FlightPak.Web.Views.Settings.People.EmergencyContactCatalog" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function CheckTxtBox(control) {
            var txtCode = document.getElementById('<%=tbCode.ClientID%>').value;
            var txtLastName = document.getElementById('<%=tbLastName.ClientID%>').value;
            var txtFirstName = document.getElementById('<%=tbFirstName.ClientID%>').value;
            var txtMiddleName = document.getElementById('<%=tbMiddleName.ClientID%>').value;
            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);

                } return false;
            }
        }
        
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Aircraft Emergency Contact</span> <span class="tab-nav-icons">
                        <!--<a
                        href="#" class="search-icon"></a><a href="#" class="save-icon"></a><a href="#" class="print-icon">
                        </a>-->
                        <a href="../../Help/ViewHelp.aspx?Screen=EmergencyContactHelp" target="_blank" title="Help"
                            class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div class="divGridPanel">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">

                function openWin() {
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    var oWnd = oManager.Open("../Company/CountryMasterPopup.aspx?CountryCD=" + document.getElementById("<%=tbCountry.ClientID%>").value, "RadWindow1");
                }

                function ConfirmClose(WinName) {
                    var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                    var oWnd = oManager.GetWindowByName(WinName);
                    //Find the Close button on the page and attach to the 
                    //onclick event
                    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                    CloseButton.onclick = function () {
                        CurrentWinName = oWnd.Id;
                        //radconfirm is non-blocking, so you will need to provide a callback function
                        oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                    }
                }


                function OnClientClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbCountry.ClientID%>").value = arg.CountryCD;
                            document.getElementById("<%=hdnCountry.ClientID%>").value = arg.CountryID;
                            document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                        }
                        else
                            document.getElementById("<%=tbCountry.ClientID%>").value = "";
                        document.getElementById("<%=hdnCountry.ClientID%>").value = "";
                    }
                }

                function GetDimensions(sender, args) {
                    var bounds = sender.getWindowBounds();
                    return;
                }
                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }

                
                $(document).ready(function UpdateConfirm() {
                    $("a").click(function (event) {
                        var redirectUrl = $(this).attr("href");
                        if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                            var msg = "Do you want to save changes to the current record?";
                            document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                            var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                            oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                            //for blocking redirection
                            return false;
                        }
                    });
                });
                
                function UpdateConfirmCallBackFn(arg) {
                    if (arg === 1) {
                        document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                    }
                    else if (arg === 2) {
                        document.getElementById('<%=btnCancel.ClientID%>').click();
                    }
                    //for blocking redirection
                    return false;
                }
                function prepareSearchInput(input) { input.value = input.value; }
            </script>
        </telerik:RadCodeBlock>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
            ClientIDMode="AutoID">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgEmergencyContact">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgEmergencyContact" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgEmergencyContact" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgEmergencyContact" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgEmergencyContact">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgEmergencyContact" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgEmergencyContact" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbCode">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbCode" />
                        <telerik:AjaxUpdatedControl ControlID="cvEmergencyContactCode" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbCountry">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        
        <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                        <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
        </telerik:RadWindowManager>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                    <tr>
                        <td>
                            <div class="status-list">
                                <span>
                                    <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                        AutoPostBack="true" /></span>
                            </div>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="dgEmergencyContact" runat="server" AllowSorting="true" OnItemCreated="dgEmergencyContact_ItemCreated"
                    OnNeedDataSource="dgEmergencyContact_BindData" OnItemCommand="dgEmergencyContact_ItemCommand"
                    OnUpdateCommand="dgEmergencyContact_UpdateCommand" OnInsertCommand="dgEmergencyContact_InsertCommand"
                    OnDeleteCommand="dgEmergencyContact_DeleteCommand" OnPreRender="dgEmergencyContact_PreRender"
                    OnPageIndexChanged="dgEmergencyContact_PageIndexChanged" AutoGenerateColumns="false"
                    OnItemDataBound="dgEmergencyContact_ItemDataBound" PageSize="10" AllowPaging="true"
                    OnSelectedIndexChanged="dgEmergencyContact_SelectedIndexChanged" AllowFilteringByColumn="true"
                    Height="341px">
                    <MasterTableView DataKeyNames="EmergencyContactID,EmergencyContactCD,CustomerID,LastName,FirstName,MiddleName,Addr1,Addr2,CityName,
            StateName,PostalZipCD,CountryID,PhoneNum,FaxNum,CellPhoneNum,EmailAddress,LastUpdUID,LastUpdTS,IsDeleted,CountryCD,CountryName,BusinessPhone,BusinessFax,CellPhoneNum2,OtherPhone,PersonalEmail,OtherEmail,Addr3,IsInActive"
                        CommandItemDisplay="Bottom">
                        <Columns>
                            <telerik:GridBoundColumn DataField="EmergencyContactID" HeaderText="EmergencyContactID"
                                AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterDelay="500"
                                CurrentFilterFunction="EqualTo" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EmergencyContactCD" HeaderText="Emergency Contact Code"
                                HeaderStyle-Width="160px" FilterControlWidth="140px" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FirstName" HeaderText="Name" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="StartsWith" UniqueName="FirstName"
                                HeaderStyle-Width="300px" FilterControlWidth="280px" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="BusinessPhone" HeaderText="Business Phone" AutoPostBackOnFilter="false"
                                HeaderStyle-Width="110px" FilterControlWidth="90px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                UniqueName="BusinessPhone" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="BusinessFax" HeaderText="Business Fax" AutoPostBackOnFilter="false"
                                HeaderStyle-Width="110px" FilterControlWidth="90px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                UniqueName="BusinessFax" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                                HeaderStyle-Width="80px" ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                            </telerik:GridCheckBoxColumn>
                        </Columns>
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px; float: left; clear: both;">
                                <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                    Visible='<%# IsAuthorized(Permission.Database.AddEmergencyContactCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                    ToolTip="Edit" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditEmergencyContactCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                    runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeleteEmergencyContactCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                            </div>
                            <div>
                                <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                            </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings CaseSensitive="false" />
                </telerik:RadGrid>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="tdLabel160">
                            <div id="tdSuccessMessage" class="success_msg">
                                Record saved successfully.</div>
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table class="border-box">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        <span class="mnd_text">Emergency Contact Code</span>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbCode" AutoPostBack="true" OnTextChanged="Code_TextChanged" runat="server"
                                                        MaxLength="5" CssClass="text40"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnECID" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="tbCode"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true">Emergency Contact Code is Required.</asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvEmergencyContactCode" runat="server" ControlToValidate="tbCode"
                                                        ErrorMessage="Unique Emergency Contact Code is Required" Display="Dynamic" CssClass="alert-text"
                                                        ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        <span class="tdLabel150">
                                            <asp:Label ID="lbFirstName" Text="First Name" runat="server"></asp:Label></span>
                                    </td>
                                    <td class="tdLabel150" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbFirstName" runat="server" ValidationGroup="Save" CssClass="text130"
                                                        MaxLength="20"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvFirstname" runat="server" ControlToValidate="tbFirstName"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true">First Name is Required.</asp:RequiredFieldValidator>
                                                </td>
                                        </table>
                                    </td>
                                    <td valign="top" class="tdLabel80">
                                        <span class="tdLabel150">
                                            <asp:Label ID="lbMiddleName" Text="Middle Name" runat="server"></asp:Label></span>
                                    </td>
                                    <td class="tdLabel150" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbMiddleName" runat="server" CssClass="text130" MaxLength="20"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%--<asp:RequiredFieldValidator ID="rfvMiddlename" runat="server" ControlToValidate="tbMiddleName"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true">Middle Name is Required.</asp:RequiredFieldValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel70" valign="top">
                                        <span class="tdLabel150">
                                            <asp:Label ID="lbLastName" Text="Last Name" runat="server"></asp:Label></span>
                                    </td>
                                    <td class="tdLabel150" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbLastName" runat="server" ValidationGroup="Save" CssClass="text130"
                                                        MaxLength="20"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvLastname" runat="server" ControlToValidate="tbLastName"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"
                                                        ErrorMessage="Last Name is Required." />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        Address Line 1
                                    </td>
                                    <td class="tdLabel240" valign="top">
                                        <asp:TextBox ID="tbAddress1" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel140" valign="top">
                                        Address Line 2
                                    </td>
                                    <td class="tdLabel220" valign="top">
                                        <asp:TextBox ID="tbAddress2" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        Address Line 3
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbAddress3" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        City
                                    </td>
                                    <td class="tdLabel240" valign="top">
                                        <asp:TextBox ID="tbCity" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel140" valign="top">
                                        State
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbState" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        Country
                                    </td>
                                    <td class="tdLabel240" valign="top">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbCountry" runat="server" CssClass="text50" MaxLength="3" AutoPostBack="true"
                                                        OnTextChanged="tbCountry_TextChanged"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnCountry" runat="server" />
                                                    <asp:Button ID="btnCountry" runat="server" OnClientClick="javascript:openWin();return false;"
                                                        CssClass="browse-button" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="cvCountry" runat="server" ErrorMessage="Invalid Country Code"
                                                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbCountry" CssClass="alert-text"
                                                        ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel140" valign="top">
                                        Postal
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbPostal" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        <asp:Label ID="lbBusinessPhone" Text="Business Phone" runat="server"></asp:Label>
                                    </td>
                                    <td class="tdLabel240" valign="top">
                                        <asp:TextBox ID="tbBusinessPhone" runat="server" CssClass="text200" MaxLength="25"
                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel140" valign="top">
                                        <asp:Label ID="lbHomePhone" Text="Home Phone" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="revBusinessPhone" runat="server" ControlToValidate="tbBusinessPhone"
                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"
                                            ErrorMessage="Business Phone is Required." />
                                    </td>
                                    <td>
                                    </td>
                                    <td>                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        Other Phone
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbOtherPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        Primary Mobile/Cell
                                    </td>
                                    <td class="tdLabel240" valign="top">
                                        <asp:TextBox ID="tbMobile" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel140">
                                        Secondary Mobile/Cell
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbSecondaryMobile" runat="server" CssClass="text200" MaxLength="25"
                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        <asp:Label ID="lbBusinessEmail" Text="Business E-mail" runat="server"></asp:Label>
                                    </td>
                                    <td class="tdLabel240" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top">
                                                    <asp:TextBox ID="tbEmail" runat="server" CssClass="text200" MaxLength="100"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:RegularExpressionValidator ID="rfvemail" runat="server" Display="Dynamic" ErrorMessage="Invalid E-mail Format"
                                                        ControlToValidate="tbEmail" CssClass="alert-text" ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                    </asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="revBusinessEmail" runat="server" ControlToValidate="tbEmail"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"
                                                        ErrorMessage="Business E-mail is Required." />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel140" valign="top">
                                        Personal E-mail
                                    </td>
                                    <td valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top">
                                                    <asp:TextBox ID="tbPersonalEmail" runat="server" MaxLength="250" CssClass="text200"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="Dynamic" ErrorMessage="Invalid E-mail Format"
                                                        ControlToValidate="tbPersonalEmail" CssClass="alert-text" ValidationGroup="Save"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        Other E-mail
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbOtherEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revOtherEmail" runat="server" Display="Dynamic"
                                                        ErrorMessage="Invalid E-mail Format" ControlToValidate="tbOtherEmail" CssClass="alert-text"
                                                        ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150" valign="top">
                                        Business Fax
                                    </td>
                                    <td class="tdLabel240" valign="top">
                                        <asp:TextBox ID="tbBusinessFax" runat="server" CssClass="text200" MaxLength="25"
                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel140" valign="top">
                                        Home Fax
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbFax" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                                CssClass="button" ValidationGroup="Save" />
                            <asp:HiddenField ID="hdnRedirect" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                                OnClick="Cancel_Click" CssClass="button" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                        </td>
                    </tr>
                </table>
                <div id="toolbar" class="scr_esp_down_db">
                    <div class="floating_main_db">
                        <div class="floating_bar_db">
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                            <span class="padright_20">
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                            <span>
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
                <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
            </asp:Panel>
        </div>
        <br />
    </div>
</asp:Content>
