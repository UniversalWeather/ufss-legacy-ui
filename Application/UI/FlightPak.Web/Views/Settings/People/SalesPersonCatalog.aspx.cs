﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using FlightPak.Common;
using FlightPak.Common.Constants;

//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class SalesPersonCatalog : BaseSecuredPage
    {
        // private bool IsEmptyCheck = true;
        public List<string> lstSalesPersonCodes = new List<string>();
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private bool SalesPersonPageNavigated = false;
        private string strSalesPersonID = "";
        private string strSalesPersonCD = "";
        private bool _selectLastModified = false;
        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgSalesPerson, dgSalesPerson, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgSalesPerson.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewSalesPersonCatalog);
                            base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);  
                            //To check the page level access.
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {                               
                                dgSalesPerson.Rebind();
                                ReadOnlyForm();
                                chkSearchActiveOnly.Checked = false;                                                           
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                                CreateDictionayForImgUpload();
                            }

                            litCQLogoMsg.Text = DataValidation.ReportLogoMessage;
                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                            else if (eventArgument == "DeleteImage_Click")
                            {
                                DeleteImage_Click(sender, e);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgSalesPerson.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var SalesPersonValue = FPKMstService.GetSalesPersonList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, SalesPersonValue);
            List<FlightPakMasterService.GetSalesPerson> filteredList = GetFilteredList(SalesPersonValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.SalesPersonID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgSalesPerson.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgSalesPerson.CurrentPageIndex = PageNumber;
            dgSalesPerson.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetSalesPerson SalesPersonValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedSalesPersonID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = SalesPersonValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().SalesPersonID;
                Session["SelectedSalesPersonID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetSalesPerson> GetFilteredList(ReturnValueOfGetSalesPerson SalesPersonValue)
        {
            List<FlightPakMasterService.GetSalesPerson> filteredList = new List<FlightPakMasterService.GetSalesPerson>();

            if (SalesPersonValue.ReturnFlag)
            {
                filteredList = SalesPersonValue.EntityList;
            }

            if (filteredList.Count > 0)
            {
                if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetSalesPerson>(); }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgSalesPerson.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgSalesPerson.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbHomeBase.Text.Trim() != string.Empty)
                        {
                            CheckHomeBaseExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        private bool CheckHomeBaseExist()
        {
            if ((tbHomeBase.Text != string.Empty) && (tbHomeBase.Text != null))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient HomeBaseService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var HomeBaseValue = HomeBaseService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.ToString().ToUpper().Trim() == tbHomeBase.Text.ToUpper().Trim()).ToList();
                    if (HomeBaseValue.Count() == 0 || HomeBaseValue == null)
                    {
                        cvHomeBase.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbHomeBase.ClientID + "');", true);
                        return true;
                    }
                    else
                    {
                        foreach (FlightPakMasterService.GetAllCompanyMaster cm in HomeBaseValue)
                        {
                            hdnHomeBaseID.Value = cm.HomebaseID.ToString();
                            tbHomeBase.Text = cm.HomebaseCD;
                        }
                        return false;
                    }
                }
            }
            return false;
        }
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> dicImage = (Dictionary<string, byte[]>)Session["DicImg"];

                        int count = dicImage.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            dicImage.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = dicImage;

                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                            {
                                dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            }
                            Session["DicImgDelete"] = dicImgDelete;

                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in dicImage)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            if (ddlImg.Items.Count > 0)
                            {
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                Session["Base64"] = null;   //added for image issue
                                if (Count > 0)
                                {
                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                            }
                            else
                            {
                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");                                
                            }                           
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }


        }

        //Veracode Issue fix 2516
        //public static byte[] ImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int)fileStream.Length);
        //        fileStream.Close();
        //        return buffer;
        //    }
        //}


        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                System.IO.Stream myStream;
                Int32 fileLen;
                Session["Base64"] = null;   //added for image issue
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        //if (ddlImg.Items.Count <= 0)
                        if (ddlImg.Items.Count >= 0)
                        {
                            string FileName = fileUL.FileName;
                            fileLen = fileUL.PostedFile.ContentLength;
                            Byte[] Input = new Byte[fileLen];

                            myStream = fileUL.FileContent;
                            myStream.Read(Input, 0, fileLen);

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);

                            ////start of modification for image issue
                            //if (hdnBrowserName.Value == "Chrome")
                            //{
                            //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                            //}
                            //else
                            //{
                            //    Session["Base64"] = Input;
                            //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                            //}
                            ////end of modification for image issue                            
                            Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                            //Commented for removing document name
                            //if (tbImgName.Text.Trim() != "")
                            //    FileName = tbImgName.Text.Trim();

                            int count = dicImg.Count(D => D.Key.Equals(FileName));
                            if (count > 0)
                            {
                                dicImg[FileName] = Input;
                            }
                            else
                            {
                                DeleteOtherImage();
                                dicImg.Add(FileName, Input);

                                string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FileName);
                                ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(encodedFileName, encodedFileName));

                                //Ramesh: Set the URL for image file or link based on the file type
                                SetURL(FileName, Input);

                                ////start of modification for image issue
                                //if (hdnBrowserName.Value == "Chrome")
                                //{
                                //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                                //}
                                //else
                                //{
                                //    Session["Base64"] = Input;
                                //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                //}
                                ////end of modification for image issue
                            }
                            tbImgName.Text = "";
                            btndeleteImage.Enabled = true;
                        }
                    }
                }
            }
        }

        private void DeleteOtherImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];

                int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                if (count > 0)
                {
                    dicImg.Remove(ddlImg.Text.Trim());
                    Session["DicImg"] = dicImg;

                    Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                    dicImgDelete.Add(ddlImg.Text.Trim(), "");
                    Session["DicImgDelete"] = dicImgDelete;

                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbImgName.Text = "";
                    int i = 0;
                    foreach (var DicItem in dicImg)
                    {
                        ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                        i = i + 1;
                    }
                    imgFile.ImageUrl = "";
                    ImgPopup.ImageUrl = null;
                }
            }
        }

        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool imgFound = false;
                            Session["Base64"] = null;   //added for image issue
                            imgFile.ImageUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ObjRetImg = ObjImgService.GetFileWarehouseList("SalesPerson", Convert.ToInt64(Session["SelectedSalesPersonID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());

                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                {
                                    imgFound = true;
                                    byte[] picture = fwh.UWAFilePath;

                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(fwh.UWAFileName, picture);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = picture;
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                                //tbImgName.Text = ddlImg.Text.Trim();//Commented for removing document name
                                if (!imgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (count > 0)
                                    {
                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }

                                }
                                //else
                                //{
                                //    imgFile.ImageUrl = "Default/images/noimage.jpg";
                                //}
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                Session["DicImg"] = dicImg;

                Dictionary<string, string> dicImgDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = dicImgDelete;
            }
        }
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgSalesPerson.Rebind();
                    }
                    if (dgSalesPerson.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedSalesPersonID"] = null;
                        //}
                        if (Session["SelectedSalesPersonID"] == null)
                        {
                            dgSalesPerson.SelectedIndexes.Add(0);

                            Session["SelectedSalesPersonID"] = dgSalesPerson.Items[0].GetDataKeyValue("SalesPersonID").ToString();
                        }

                        if (dgSalesPerson.SelectedIndexes.Count == 0)
                            dgSalesPerson.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void SelectItem()
        {
            if (Session["SelectedSalesPersonID"] != null)
            {
                if (dgSalesPerson.Items.Count > 0)
                {
                    string ID = Session["SelectedSalesPersonID"].ToString();
                    foreach (GridDataItem Item in dgSalesPerson.MasterTableView.Items)
                    {
                        if (Item["SalesPersonID"].Text.Trim() == ID)
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                    ////if (dgSalesPerson.Items.Count > 0 && dgSalesPerson.SelectedItems.Count == 0)
                    ////{
                    ////    dgSalesPerson.SelectedIndexes.Add(0);
                    ////}
                }
                else
                {
                    DefaultSelection(false);
                }
            }
            else
            {
                DefaultSelection(false);
            }
        }

        protected void dgSalesPerson_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (SalesPersonPageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgSalesPerson, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgSalesPerson;
                        }
                        else if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgSalesPerson;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }

        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSalesPerson_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + target + "');", true);
                    IsEmptyCheck = false;
                }

            }
        }
        /// <summary>
        /// Bind Customer Address Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSalesPerson_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objSalesPersonService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objSalesPerson = objSalesPersonService.GetSalesPersonList();
                            if (objSalesPerson.ReturnFlag == true)
                            {
                                dgSalesPerson.DataSource = objSalesPerson.EntityList;
                            }
                            Session.Remove("SalesPersonCD");
                            
                            lstSalesPersonCodes = new List<string>();
                            foreach (GetSalesPerson SalesPersonCatalogEntity in objSalesPerson.EntityList)
                            {
                                lstSalesPersonCodes.Add(SalesPersonCatalogEntity.SalesPersonCD.Trim());
                            }
                            Session["SalesPersonCD"] = lstSalesPersonCodes;
                            DoSearchFilter();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }

        }

        /// <summary>
        /// Item Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSalesPerson_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedSalesPersonID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.SalesPerson, Convert.ToInt64(Session["SelectedSalesPersonID"].ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.SalesPerson);
                                            SelectItem();
                                            return;
                                        }                                        
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        ddlImg.Enabled = true;
                                        if (imgFile.ImageUrl.Trim() != "")
                                        {
                                            btndeleteImage.Enabled = true;
                                        }
                                        tbImgName.Enabled = true;
                                        //fileUL.Enabled = false; //Commented for removing document name
                                        //RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                                        SelectItem();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgSalesPerson.SelectedIndexes.Clear();
                                //tbCode.ReadOnly = false;
                                //tbCode.BackColor = System.Drawing.Color.White;
                               
                                DisplayInsertForm();
                               
                                GridEnable(true, false, false);
                                tbImgName.Enabled = true;
                                ddlImg.Enabled = true;
                                btndeleteImage.Enabled = true;
                                //fileUL.Enabled = false;//Commented for removing document name
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                                DisableLinks();
                                break;
                            case "Filter":
                                foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }

        /// <summary>
        /// Update Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgSalesPerson_UpdateCommand(object source, GridCommandEventArgs e)
        {
            e.Canceled = true;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedSalesPersonID"] != null)
                        {
                            e.Canceled = true;
                            bool IsValidate = true;                                                    
                            if (CheckAlreadyBillCountryExist())
                            {
                                cvCountry.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCountry.ClientID); //tbCountry.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCountry.ClientID + "');", true);
                                IsValidate = false;
                            }
                            if (CheckHomeBaseExist())
                            {
                                cvHomeBase.IsValid = false;
                                RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbCountry.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbHomeBase.ClientID + "');", true);
                                IsValidate = false;
                            }
                            if (IsValidate)
                            {
                                e.Canceled = true;
                                using (FlightPakMasterService.MasterCatalogServiceClient objSalesPersonService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objSalesPersonService.UpdateSalesPerson(GetItems());
                                    //For Data Anotation
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        #region Image Upload in Edit Mode
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        foreach (var DicItem in dicImg)
                                        {

                                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "SalesPerson";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(Session["SelectedSalesPersonID"].ToString());
                                                Service.UWAWebpageName = "SalesPersonCatalog.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                objService.AddFWHType(Service);
                                            }
                                        }
                                        #endregion

                                        #region Image Upload in Delete
                                        using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                            foreach (var DicItem in dicImgDelete)
                                            {
                                                objFWHType.UWAFileName = DicItem.Key;
                                                objFWHType.RecordID = Convert.ToInt64(Session["SelectedSalesPersonID"].ToString());
                                                objFWHType.RecordType = "SalesPerson";
                                                objFWHType.IsDeleted = true;
                                                objFWHType.IsDeleted = true;
                                                objFWHTypeService.DeleteFWHType(objFWHType);
                                            }
                                        }
                                        #endregion

                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.SalesPerson, Convert.ToInt64(Session["SelectedSalesPersonID"].ToString().Trim()));
                                            GridEnable(true, true, true);
                                            SelectItem();
                                            ReadOnlyForm();
                                        }

                                        ShowSuccessMessage();
                                        EnableLinks();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.SalesPerson);
                                    }
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }

        protected void dgSalesPerson_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgSalesPerson.ClientSettings.Scrolling.ScrollTop = "0";
                        SalesPersonPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }

        //private bool checkAllReadyExist()
        //{
        //    bool returnVal = false;
        //    lstSalesPersonCodes = (List<string>)Session["SalesPersonCD"];

        //    if (lstSalesPersonCodes != null && lstSalesPersonCodes.Equals(tbCode.Text.ToString().Trim()))
        //        return true;
        //    return returnVal;

        //}

        /// <summary>
        /// Update Command for Customer Address Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgSalesPerson_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAlreadyExist())
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            IsValidate = false;
                        }                                             
                        if (CheckAlreadyBillCountryExist())
                        {
                            cvCountry.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCountry.ClientID); //tbCountry.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCountry.ClientID + "');", true);
                            IsValidate = false;
                        }
                        if (CheckHomeBaseExist())
                        {
                            cvHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbCountry.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbHomeBase.ClientID + "');", true);
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objSalesPersonService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objSalesPersonService.AddSalesPerson(GetItems());
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    #region Image Upload in New Mode

                                    var InsertedSalesPersonID = objSalesPersonService.GetSalesPersonID();
                                    //Int64 NewCrewID = 0;
                                    if (InsertedSalesPersonID != null)
                                    {
                                        foreach (FlightPakMasterService.GetSalesPersonID salesPersonId in InsertedSalesPersonID.EntityList)
                                        {
                                            hdnSalesPersonID.Value = salesPersonId.SalesPersonID.ToString();
                                        }
                                    }



                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    foreach (var DicItem in dicImg)
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                            Service.FileWarehouseID = 0;
                                            Service.RecordType = "SalesPerson";
                                            Service.UWAFileName = DicItem.Key;
                                            Service.RecordID = Convert.ToInt64(hdnSalesPersonID.Value);
                                            Service.UWAWebpageName = "SalesPersonCatalog.aspx";
                                            Service.UWAFilePath = DicItem.Value;
                                            Service.IsDeleted = false;
                                            Service.FileWarehouseID = 0;
                                            objService.AddFWHType(Service);
                                        }
                                    }
                                    #endregion

                                    dgSalesPerson.Rebind();
                                    DefaultSelection(false);
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.SalesPerson);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgSalesPerson_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedSalesPersonID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient SalesPersonService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.SalesPerson SalesPerson = new FlightPakMasterService.SalesPerson();
                                string Code = Session["SelectedSalesPersonID"].ToString();
                                strSalesPersonCD = "";
                                strSalesPersonID = "";
                                foreach (GridDataItem Item in dgSalesPerson.MasterTableView.Items)
                                {
                                    if (Item["SalesPersonID"].Text.Trim() == Code.Trim())
                                    {
                                        strSalesPersonCD = Item["SalesPersonCD"].Text.Trim();
                                        strSalesPersonID = Item["SalesPersonID"].Text.Trim();
                                        SalesPerson.SalesPersonID = Convert.ToInt64(Item.GetDataKeyValue("SalesPersonID").ToString());
                                        break;
                                    }
                                }
                                SalesPerson.SalesPersonCD = strSalesPersonCD;
                                SalesPerson.SalesPersonID = Convert.ToInt64(strSalesPersonID);
                                SalesPerson.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.SalesPerson, Convert.ToInt64(Session["SelectedSalesPersonID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.SalesPerson);
                                        return;
                                    }
                                }
                                SalesPersonService.DeleteSalesPerson(SalesPerson);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                dgSalesPerson.Rebind();
                                DefaultSelection(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.SalesPerson, Convert.ToInt64(Session["SelectedSalesPersonID"].ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSalesPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem Item = dgSalesPerson.SelectedItems[0] as GridDataItem;

                                Session["SelectedSalesPersonID"] = Item["SalesPersonID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                    }
                }
            }
        }


        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }


        }

        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    radlstLogoPosition.SelectedIndex = 0;
                    ClearForm();

                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Delay Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgSalesPerson.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgSalesPerson.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }                            
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }


        /// <summary>
        /// To check unique Code  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            CheckAlreadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }

        }

        /// <summary>
        /// To check unique code already exists
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyExist()
        {
            bool returnVal = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objservice.GetSalesPersonList().EntityList.Where(x => x.SalesPersonCD.Trim().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                        if (objRetVal.Count() > 0 && objRetVal != null)
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            returnVal = true;
                        }
                        else
                        {
                            RadAjaxManager1.FocusControl(tbFirstName.ClientID);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbFirstName.ClientID + "');", true);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            return returnVal;
        }
                       
        private bool CheckAlreadyBillCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbCountry.Text != string.Empty) && (tbCountry.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper().Equals(tbCountry.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvCountry.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCountry.ClientID); //tbCountry.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCountry.ClientID + "');", true);
                                RetVal = true;
                            }
                            else
                            {
                                foreach (FlightPakMasterService.Country cm in RetValue)
                                {
                                    hdnCountryID.Value = cm.CountryID.ToString();
                                    tbCountry.Text = cm.CountryCD;
                                }
                                RadAjaxManager1.FocusControl(tbPostal.ClientID); //tbPostal.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbPostal.ClientID + "');", true);
                                RetVal = false;
                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
       
        /// <summary>
        /// Cancel Sales Person Catalog Form Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedSalesPersonID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.SalesPerson, Convert.ToInt64(Session["SelectedSalesPersonID"].ToString().Trim()));
                                    DefaultSelection(false);
                                }
                            }
                        }
                       
                        GridEnable(true, true, true);
                        DefaultSelection(false);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        private SalesPerson GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.SalesPerson salesPerson = new FlightPakMasterService.SalesPerson();
                
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    salesPerson.SalesPersonCD = tbCode.Text.Trim();
                    //salesPerson.Name = tbName.Text;
                    if (hdnSave.Value == "Update")
                    {
                        salesPerson.SalesPersonID = Convert.ToInt64(Session["SelectedSalesPersonID"].ToString());
                    }                                    
                    if (!string.IsNullOrEmpty(tbFirstName.Text))
                    {
                        salesPerson.FirstName = tbFirstName.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbMiddleName.Text))
                    {
                        salesPerson.MiddleName = tbMiddleName.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbLastName.Text))
                    {
                        salesPerson.LastName = tbLastName.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbAddr1.Text))
                    {
                        salesPerson.Address1 = tbAddr1.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbAddr2.Text))
                    {
                        salesPerson.Address2 = tbAddr2.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbAddr3.Text))
                    {
                        salesPerson.Address3 = tbAddr3.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbCity.Text))
                    {
                        salesPerson.CityName = tbCity.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbState.Text))
                    {
                        salesPerson.StateName = tbState.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbPostal.Text))
                    {
                        salesPerson.PostalZipCD = tbPostal.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbFax.Text))
                    {
                        salesPerson.FaxNUM = tbFax.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbPersonalFaxNum.Text))
                    {
                        salesPerson.PersonalFaxNum = tbPersonalFaxNum.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbPhone.Text))
                    {
                        salesPerson.PhoneNUM = tbPhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbCellPhoneNum.Text))
                    {
                        salesPerson.CellPhoneNUM = tbCellPhoneNum.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbEmail.Text))
                    {
                        salesPerson.EmailAddress = tbEmail.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbPersonalEmailID.Text))
                    {
                        salesPerson.PersonalEmailID = tbPersonalEmailID.Text.Trim();
                    }                   
                    if (!string.IsNullOrEmpty(tbCountry.Text))
                    {
                        salesPerson.CountryID = Convert.ToInt64(hdnCountryID.Value);
                    }
                    salesPerson.IsDeleted = false;
                    if (!string.IsNullOrEmpty(tbNotes.Text))
                    {
                        salesPerson.Notes = tbNotes.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(hdnHomeBaseID.Value))
                    {
                        salesPerson.HomeBaseId = Convert.ToInt64(hdnHomeBaseID.Value);
                    }
                    if (radlstLogoPosition.SelectedItem != null)
                    {
                        if (!string.IsNullOrEmpty(radlstLogoPosition.SelectedItem.Value))
                        {
                            string Trips = radlstLogoPosition.SelectedItem.Value.ToString();
                            salesPerson.LogoPosition = Convert.ToInt32(Trips);
                        }
                    }
                    salesPerson.IsInActive = chkInactive.Checked;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return salesPerson;
            }
        }

        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    //tbCode.ReadOnly = true;
                    //tbCode.BackColor = System.Drawing.Color.LightGray;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    tbCode.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedSalesPersonID"] != null)
                    {
                        strSalesPersonID = "";
                        foreach (GridDataItem Item in dgSalesPerson.MasterTableView.Items)
                        {
                            if (Item["SalesPersonID"].Text.Trim() == Session["SelectedSalesPersonID"].ToString().Trim())
                            {
                                tbCode.Text = Item.GetDataKeyValue("SalesPersonCD").ToString().Trim();
                                if (Item.GetDataKeyValue("FirstName") != null)
                                {
                                    tbFirstName.Text = Item.GetDataKeyValue("FirstName").ToString().Trim();
                                }
                                else
                                {
                                    tbFirstName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastName") != null)
                                {
                                    tbLastName.Text = Item.GetDataKeyValue("LastName").ToString().Trim();
                                }
                                else
                                {
                                    tbLastName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("MiddleName") != null)
                                {
                                    tbMiddleName.Text = Item.GetDataKeyValue("MiddleName").ToString().Trim();
                                }
                                else
                                {
                                    tbMiddleName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Address1") != null)
                                {
                                    tbAddr1.Text = Item.GetDataKeyValue("Address1").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr1.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Address2") != null)
                                {
                                    tbAddr2.Text = Item.GetDataKeyValue("Address2").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr2.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Address3") != null)
                                {
                                    tbAddr3.Text = Item.GetDataKeyValue("Address3").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr3.Text = string.Empty;
                                }                               
                                if (Item.GetDataKeyValue("CityName") != null)
                                {
                                    tbCity.Text = Item.GetDataKeyValue("CityName").ToString();
                                }
                                else
                                {
                                    tbCity.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("StateName") != null)
                                {
                                    tbState.Text = Item.GetDataKeyValue("StateName").ToString().Trim();
                                }
                                else
                                {
                                    tbState.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PostalZipCD") != null)
                                {
                                    tbPostal.Text = Item.GetDataKeyValue("PostalZipCD").ToString().Trim();
                                }
                                else
                                {
                                    tbPostal.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("NationalityCD") != null)
                                {
                                    tbCountry.Text = Item.GetDataKeyValue("NationalityCD").ToString();
                                }
                                else
                                {
                                    tbCountry.Text = string.Empty;
                                }                                                                
                                if (Item.GetDataKeyValue("PhoneNum") != null)
                                {
                                    tbPhone.Text = Item.GetDataKeyValue("PhoneNum").ToString();
                                }
                                else
                                {
                                    tbPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("FaxNum") != null)
                                {
                                    tbFax.Text = Item.GetDataKeyValue("FaxNum").ToString();
                                }
                                else
                                {
                                    tbFax.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("EmailAddress") != null)
                                {
                                    tbEmail.Text = Item.GetDataKeyValue("EmailAddress").ToString();
                                }
                                else
                                {
                                    tbEmail.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PersonalEmailID") != null)
                                {
                                    tbPersonalEmailID.Text = Item.GetDataKeyValue("PersonalEmailID").ToString();
                                }
                                else
                                {
                                    tbPersonalEmailID.Text = string.Empty;
                                } 
                                if (Item.GetDataKeyValue("Notes") != null)
                                {
                                    tbNotes.Text = Item.GetDataKeyValue("Notes").ToString();
                                }
                                else
                                {
                                    tbNotes.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PersonalFaxNum") != null)
                                {
                                    tbPersonalFaxNum.Text = Item.GetDataKeyValue("PersonalFaxNum").ToString();
                                }
                                else
                                {
                                    tbPersonalFaxNum.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CellPhoneNum") != null)
                                {
                                    tbCellPhoneNum.Text = Item.GetDataKeyValue("CellPhoneNum").ToString();
                                }
                                else
                                {
                                    tbCellPhoneNum.Text = string.Empty;
                                }                                                  
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                if (Item.GetDataKeyValue("LogoPosition") != null)
                                {
                                    radlstLogoPosition.SelectedIndex = Convert.ToInt32(Item.GetDataKeyValue("LogoPosition"));
                                }
                                if (Item.GetDataKeyValue("IcaoID") != null)
                                {
                                    tbHomeBase.Text = Item.GetDataKeyValue("IcaoID").ToString();
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;
                                }
                                #region Get Image from Database
                                CreateDictionayForImgUpload();
                                imgFile.ImageUrl = "";
                                ImgPopup.ImageUrl = null;
                                using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var ObjRetImg = ObjImgService.GetFileWarehouseList("SalesPerson", Convert.ToInt64(Session["SelectedSalesPersonID"].ToString())).EntityList;
                                    ddlImg.Items.Clear();
                                    ddlImg.Text = "";
                                    int i = 0;
                                    Session["Base64"] = null;   //added for image issue
                                    foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                    {
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.UWAFileName);
                                        ddlImg.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                                        dicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                                        if (i == 0)
                                        {
                                            byte[] picture = fwh.UWAFilePath;

                                            //Ramesh: Set the URL for image file or link based on the file type
                                            SetURL(fwh.UWAFileName, picture);

                                            ////start of modification for image issue
                                            //if (hdnBrowserName.Value == "Chrome")
                                            //{
                                            //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                            //}
                                            //else
                                            //{
                                            //    Session["Base64"] = picture;
                                            //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                            //}
                                            ////end of modification for image issue
                                        }
                                        i = i + 1;
                                    }
                                    if (ddlImg.Items.Count > 0)
                                    {
                                        ddlImg.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                    }
                                }
                                #endregion
                                lbColumnName1.Text = "Salesperson Code";
                                lbColumnName2.Text = "Name";
                                lbColumnValue1.Text = Item["SalesPersonCD"].Text;
                                lbColumnValue2.Text = Item["Name"].Text;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgSalesPerson.Items.Count > 0 && dgSalesPerson.SelectedItems.Count == 0)
                    {
                        dgSalesPerson.SelectedIndexes.Add(0);
                    }
                    if (dgSalesPerson.Items.Count > 0 && dgSalesPerson.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = dgSalesPerson.SelectedItems[0] as GridDataItem;
                        Label lbLastUpdatedUser = (Label)dgSalesPerson.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                        }
                        LoadControlData();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        //protected void ReadOnlyForm()
        //{
        //    Session.Remove("SelectedItem");
        //    Session["SelectedItem"] = (GridDataItem)dgSalesPerson.SelectedItems[0];
        //    GridDataItem item = (GridDataItem)Session["SelectedItem"];
        //    tbCode.Text = item.GetDataKeyValue("SalesPersonCD").ToString().Trim();
        //    if (item.GetDataKeyValue("Name") != null)
        //    {
        //        tbName.Text = item.GetDataKeyValue("Name").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbName.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Address1") != null)
        //    {
        //        tbAddr1.Text = item.GetDataKeyValue("Address1").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbAddr1.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Address2") != null)
        //    {
        //        tbAddr2.Text = item.GetDataKeyValue("Address2").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbAddr2.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Address3") != null)
        //    {
        //        tbAddr3.Text = item.GetDataKeyValue("Address3").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbAddr3.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("HomeBaseCD") != null)
        //    {
        //        tbHomeBase.Text = item.GetDataKeyValue("HomeBaseCD").ToString();
        //    }
        //    else
        //    {
        //        tbHomeBase.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("ClosestICAO") != null)
        //    {
        //        tbClosestIcao.Text = item.GetDataKeyValue("ClosestICAO").ToString();
        //    }
        //    else
        //    {
        //        tbClosestIcao.Text = string.Empty;
        //    }

        //    if (item.GetDataKeyValue("CustomCity") != null)
        //    {
        //        tbCity.Text = item.GetDataKeyValue("CustomCity").ToString();
        //    }
        //    else
        //    {
        //        tbCity.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("CustomState") != null)
        //    {
        //        tbState.Text = item.GetDataKeyValue("CustomState").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbState.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("PostalZipCD") != null)
        //    {
        //        tbPostal.Text = item.GetDataKeyValue("PostalZipCD").ToString().Trim();
        //    }
        //    else
        //    {
        //        tbPostal.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("CountryCD") != null)
        //    {
        //        tbCountry.Text = item.GetDataKeyValue("CountryCD").ToString();
        //    }
        //    else
        //    {
        //        tbCountry.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("MetropolitanArea") != null)
        //    {
        //        tbMetro.Text = item.GetDataKeyValue("MetropolitanArea").ToString();
        //    }
        //    else
        //    {
        //        tbMetro.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Contact") != null)
        //    {
        //        tbContact.Text = item.GetDataKeyValue("Contact").ToString();
        //    }
        //    else
        //    {
        //        tbContact.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("PhoneNum") != null)
        //    {
        //        tbPhone.Text = item.GetDataKeyValue("PhoneNum").ToString();
        //    }
        //    else
        //    {
        //        tbPhone.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("FaxNum") != null)
        //    {
        //        tbFax.Text = item.GetDataKeyValue("FaxNum").ToString();
        //    }
        //    else
        //    {
        //        tbFax.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("ContactEmailID") != null)
        //    {
        //        tbEmail.Text = item.GetDataKeyValue("ContactEmailID").ToString();
        //    }
        //    else
        //    {
        //        tbEmail.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Website") != null)
        //    {
        //        tbWebsite.Text = item.GetDataKeyValue("Website").ToString();
        //    }
        //    else
        //    {
        //        tbWebsite.Text = string.Empty;
        //    }

        //    if (item.GetDataKeyValue("Remarks") != null)
        //    {
        //        tbNotes.Text = item.GetDataKeyValue("Remarks").ToString();
        //    }
        //    else
        //    {
        //        tbNotes.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("Symbol") != null)
        //    {
        //        tbCustom.Text = item.GetDataKeyValue("Symbol").ToString();
        //    }
        //    else
        //    {
        //        tbCustom.Text = string.Empty;
        //    }

        //    if (item.GetDataKeyValue("LatitudeDegree") != null)
        //    {
        //        tbLatitudeDeg.Text = item.GetDataKeyValue("LatitudeDegree").ToString();
        //    }
        //    else
        //    {
        //        tbLatitudeDeg.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LatitudeMinutes") != null)
        //    {
        //        tbLatitudeMin.Text = item.GetDataKeyValue("LatitudeMinutes").ToString();
        //    }
        //    else
        //    {
        //        tbLatitudeMin.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LatitudeNorthSouth") != null)
        //    {
        //        tbLatitudeNS.Text = item.GetDataKeyValue("LatitudeNorthSouth").ToString();
        //    }
        //    else
        //    {
        //        tbLatitudeNS.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LongitudeDegrees") != null)
        //    {
        //        tbLongitudeDeg.Text = item.GetDataKeyValue("LongitudeDegrees").ToString();
        //    }
        //    else
        //    {
        //        tbLongitudeDeg.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LongitudeMinutes") != null)
        //    {
        //        tbLongitudeMin.Text = item.GetDataKeyValue("LongitudeMinutes").ToString();
        //    }
        //    else
        //    {
        //        tbLongitudeMin.Text = string.Empty;
        //    }
        //    if (item.GetDataKeyValue("LongitudeEastWest") != null)
        //    {
        //        tbLongitudeEW.Text = item.GetDataKeyValue("LongitudeEastWest").ToString();
        //    }
        //    else
        //    {
        //        tbLongitudeEW.Text = string.Empty;
        //    }


        //    tbCode.ReadOnly = true;
        //    tbCode.BackColor = System.Drawing.Color.LightGray;
        //    EnableForm(false);
        //}

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbFirstName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    hdnHomeBaseID.Value = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    
                    #region Image clear
                    CreateDictionayForImgUpload();
                    fileUL.Enabled = false;
                    ddlImg.Enabled = false;
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    ImgPopup.ImageUrl = null;
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";                    
                    tbImgName.Text = "";
                    #endregion

                    radlstLogoPosition.SelectedIndex = 0;

                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbCountry.Text = string.Empty;
                    
                    tbPostal.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    
                    tbEmail.Text = string.Empty;
                    tbPersonalEmailID.Text = string.Empty;
                    tbNotes.Text = string.Empty;
                                        
                    tbCellPhoneNum.Text = string.Empty;
                    tbPersonalFaxNum.Text = string.Empty;
                    
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbFirstName.Enabled = enable;
                    tbMiddleName.Enabled = enable;
                    tbLastName.Enabled = enable;
                                       
                    tbAddr1.Enabled = enable;
                    tbAddr2.Enabled = enable;
                    tbAddr3.Enabled = enable;
                                        
                    ddlImg.Enabled = enable;
                    tbImgName.Enabled = enable;
                    btndeleteImage.Enabled = enable;
                    btndeleteImage.Visible = enable;
                    fileUL.Enabled = enable;

                    tbCity.Enabled = enable;
                    tbState.Enabled = enable;
                    tbCountry.Enabled = enable;
                    
                    tbPostal.Enabled = enable;
                    tbPhone.Enabled = enable;
                    tbFax.Enabled = enable;
                    tbPersonalFaxNum.Enabled = enable;
                    tbEmail.Enabled = enable;
                    tbNotes.Enabled = enable;

                    btnCountry.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnCancelTop.Visible = enable;
                   
                    //btnCustomSymbol.Enabled = enable;
                    btnSaveChanges.Visible = enable;
                    btnSaveChangesTop.Visible = enable;
                                        
                    tbCellPhoneNum.Enabled = enable;
                    tbPersonalEmailID.Enabled = enable;
                    
                    chkInactive.Enabled = enable;
                    radlstLogoPosition.Enabled = enable;

                    tbHomeBase.Enabled = enable;
                    btnHomeBase.Enabled = enable;

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Country_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCountry.Text != null)
                        {
                            CheckAlreadyBillCountryExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

       
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgSalesPerson.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDelCtl = (LinkButton)dgSalesPerson.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgSalesPerson.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddSalesPersonCatalog))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteSalesPersonCatalog))
                    {
                        lbtnDelCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDelCtl.Enabled = true;
                            lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtnDelCtl.Enabled = false;
                            lbtnDelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditSalesPersonCatalog))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchFilter();
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.SalesPerson);
                }
            }
        }

        protected void DoSearchFilter()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objSalesPersonService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objSalesPerson = objSalesPersonService.GetSalesPersonList();

                List<FlightPakMasterService.GetSalesPerson> lstSalesPerson = new List<FlightPakMasterService.GetSalesPerson>();
                lstSalesPerson = objSalesPerson.EntityList.ToList<GetSalesPerson>();
                
                if (lstSalesPerson.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstSalesPerson = lstSalesPerson.Where(x => x.IsInActive == false).ToList<GetSalesPerson>(); }                                        
                    dgSalesPerson.DataSource = lstSalesPerson;
                    //dgSalesPerson.DataBind();
                }

            }
        }
        
        #endregion
        //private void GridEnable(bool add, bool edit, bool delete)
        //{

        //    LinkButton insertCtl, editCtl, delCtl;
        //    insertCtl = (LinkButton)dgSalesPerson.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
        //    delCtl = (LinkButton)dgSalesPerson.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
        //    editCtl = (LinkButton)dgSalesPerson.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
        //    if (add)
        //    {
        //        insertCtl.Enabled = true;
        //    }
        //    else
        //        insertCtl.Enabled = false;
        //    if (delete)
        //    {
        //        delCtl.Enabled = true;
        //        delCtl.OnClientClick = "javascript:return ProcessDelete();";

        //    }
        //    else
        //    {
        //        delCtl.Enabled = false;
        //        delCtl.OnClientClick = string.Empty;
        //    }
        //    if (edit)
        //    {
        //        editCtl.Enabled = true;
        //        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
        //    }
        //    else
        //    {
        //        editCtl.Enabled = false;
        //        editCtl.OnClientClick = string.Empty;
        //    }
        //}

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {            
            chkSearchActiveOnly.Enabled = false;            
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;           
        }

        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// 
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            //Ramesh: Code to allow any file type for upload control.
            int iIndex = filename.Trim().IndexOf('.');
          //  string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);  
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            if (regMatch.Success)
            {
                if (Request.UserAgent.Contains("Chrome"))
                {
                    hdnBrowserName.Value = "Chrome";
                }
                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    //lnkFileName.NavigateUrl = "";
                    //lnkFileName.Visible = false;
                }
                else
                {
                    Session["Base64"] = filedata;
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid();
                    //lnkFileName.NavigateUrl = "";
                    //lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgFile.Visible = false;
                //lnkFileName.Visible = true;
                Session["Base64"] = filedata;
                //lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();
            }
        }
    }
}