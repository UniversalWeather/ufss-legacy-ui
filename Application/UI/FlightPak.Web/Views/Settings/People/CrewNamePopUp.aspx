﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewNamePopUp.aspx.cs"
    ClientIDMode="AutoID" Inherits="FlightPak.Web.Views.Settings.People.CrewNamePopUp" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Select Multiple Crew</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:radcodeblock id="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgCrewRoster.ClientID %>");

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function RowDblClick() {
                var masterTable = $find("<%= dgCrewRoster.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadClientCodePopup") {
                    url = "../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCodeFilter.ClientID%>").value;
                }
                var oWnd = radopen(url, radWin);
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function ClientCodeFilterPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=cvClientCodeFilter.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = "";
                        document.getElementById("<%=cvClientCodeFilter.ClientID%>").innerHTML = "";
                    }
                }
            }


            function Close() {
                GetRadWindow().Close();
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgCrewRoster.ClientID %>").get_masterTableView();
                masterTable.rebind();
                masterTable.clearSelectedItems();
                masterTable.selectItem(masterTable.get_dataItems()[0].get_element());
            }
            function GetGridId() {
                return $find("<%= dgCrewRoster.ClientID %>");
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:radcodeblock>
    <asp:ScriptManager ID="scr1" runat="server">
    </asp:ScriptManager>
    <telerik:radajaxmanager id="RadAjaxManager1" runat="server" onajaxsettingcreating="RadAjaxManager1_AjaxSettingCreating"
        onajaxrequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="divExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCrewRoster">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbClientCodeFilter">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:radajaxmanager>
    <telerik:radwindowmanager id="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCodeFilterPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>
    <telerik:radajaxloadingpanel id="RadAjaxLoadingPanel1" runat="server" skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlSearchPanel" runat="server" Visible="true">
            <div class="divGridPanel">
                <table class="box1">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="tdLabel100" valign="top">
                                        <asp:CheckBox ID="chkShowAllCrew" runat="server" Checked="false" Text="Show All Crew" />
                                    </td>
                                    <td class="tdLabel10" valign="top">
                                    </td>
                                    <td valign="top">
                                        <table id="Table1" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    <asp:Label runat="server" ID="lbClientCode">Client Code :</asp:Label>
                                                </td>
                                                <td class="tdLabel100">
                                                    <asp:TextBox ID="tbClientCodeFilter" runat="server" MaxLength="5" ValidationGroup="Save"
                                                        OnTextChanged="ClientCodeFilter_TextChanged" AutoPostBack="true" CssClass="text50"></asp:TextBox>
                                                    <asp:Button ID="btnClientCodeFilter" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:CustomValidator ID="cvClientCodeFilter" runat="server" ControlToValidate="tbClientCodeFilter"
                                                        ErrorMessage="Invalid ClientCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel10" valign="top">
                                    </td>
                                    <td class="tdLabel100" valign="top">
                                        <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                            ValidationGroup="Save" OnClick="Search_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:radgrid id="dgCrewRoster" runat="server" allowmultirowselection="true" allowsorting="true"
                                oninsertcommand="CrewRoster_InsertCommand" onitemdatabound="dgCrewRoster_ItemDataBound"
                                onneeddatasource="dgCrewRoster_BindData" autogeneratecolumns="false" height="330px"
                                onitemcommand="dgCrewRoster_ItemCommand" onpageindexchanged="dgCrewRoster_PageIndexChanged"
                                pagerstyle-alwaysvisible="true" allowpaging="false" width="850px" allowfilteringbycolumn="true"
                                onitemcreated="dgCrewRoster_ItemCreated" OnPreRender="dgCrewRoster_PreRender">
                                <MasterTableView DataKeyNames="CrewID,CrewCD,PhoneNum,HomeBaseID,IsStatus,HomeBaseCD,LastName,FirstName,MiddleInitial"
                                    CommandItemDisplay="Bottom">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" CurrentFilterFunction="StartsWith"
                                            AllowSorting="true" AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="380px"
                                            FilterControlWidth="360px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" CurrentFilterFunction="StartsWith"
                                            AllowSorting="true" AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="380px"
                                            FilterControlWidth="360px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="MiddleInitial" HeaderText="Middle Intial" CurrentFilterFunction="StartsWith"
                                            AllowSorting="true" AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="380px"
                                            FilterControlWidth="360px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CrewCD" HeaderText="Code" AutoPostBackOnFilter="false"
                                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                            FilterControlWidth="80px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="Department" AutoPostBackOnFilter="false"
                                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                            FilterControlWidth="80px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                            FilterControlWidth="80px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridCheckBoxColumn DataField="IsStatus" HeaderText="Active" AutoPostBackOnFilter="true"
                                            AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                            HeaderStyle-Width="65px">
                                        </telerik:GridCheckBoxColumn>
                                        <telerik:GridBoundColumn DataField="PilotLicense1" HeaderText="Pilot License" AutoPostBackOnFilter="false"
                                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px"
                                            FilterControlWidth="100px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Phone" AutoPostBackOnFilter="false"
                                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px"
                                            FilterControlWidth="100px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="EmailAddress" HeaderText="Business<br />E-Mail"
                                            CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px" FilterControlWidth="80px"
                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ClientCD" HeaderText="ClientCD" CurrentFilterFunction="StartsWith"
                                            Display="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CrewID" HeaderText="CrewID" AutoPostBackOnFilter="false"
                                            Display="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                        <div class="grd_ok">
                                            <asp:LinkButton ID="lbtnInitInsert" runat="server"  CommandName="InitInsert"
                                                CausesValidation="false" CssClass="button" Text="Switch"></asp:LinkButton>
                                            <button id="btnCancel" onclick="Close();" class="button">
                                                Cancel</button>
                                        </div>
                                        <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                            visible="false">
                                            Use CTRL key to multi select</div>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings>
                                    <ClientEvents OnRowDblClick="RowDblClick" />
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                                <GroupingSettings CaseSensitive="false"></GroupingSettings>
                            </telerik:radgrid>
                            <asp:Label ID="InjectScript" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnCrewGroupID" runat="server" />
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
