﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MaintainUser.aspx.cs" MasterPageFile="~/Framework/Masters/Settings.master"
    Inherits="FlightPak.Web.Admin.MaintainUser" ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
        <script type="text/javascript" language="javascript">

            function rfvlstSelected_ClientValidate(source, arguments) {
                if (!document.getElementById("<%=chkSysAdmin.ClientID%>").checked || document.getElementById("<%=lstSelected.ClientID%>").options.length == 0)
                { arguments.IsValid = false; }
                else
                { arguments.IsValid = true; }
            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }

            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function openWin(popupid) {
                var url = '';
                if (popupid == "RadWindow1") {
                    url = "../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCode.ClientID%>").value;
                }
                else if (popupid == "radCompanyMasterPopup") {
                    url = '../Company/CompanyMasterPopup.aspx?HomeBase=', document.getElementById("<%=tbHomebase.ClientID%>").value;
                }
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, popupid);
            }

            function OnClientCloseAirportMasterPopup(oWnd, args) {
                var combo = $find("<%= tbHomebase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomebase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=rfvHomebaseInvalid.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomebaseID.ClientID%>").value = arg.HomebaseID;
                    }
                    else {
                        document.getElementById("<%=tbHomebase.ClientID%>").value = "";
                        document.getElementById("<%=rfvHomebaseInvalid.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomebaseID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientClose(oWnd, args) {
                var combo = $find("<%= tbClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = htmlDecode(arg.ClientCD);
                        document.getElementById("<%=tbClientCodeInvalid.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnClientID.ClientID%>").value = arg.ClientID;

                    }
                    else {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                        document.getElementById("<%=tbClientCodeInvalid.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnClientID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }


            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgMaintainUser" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgMaintainUser" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgMaintainUser">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgMaintainUser" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnResetPwd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbHomebase">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbClientCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbTravelCoordinator">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkSearchActiveOnly">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgMaintainUser" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbEmail">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">User Administration</span> <span class="tab-nav-icons"><a
                        href="../../Help/ViewHelp.aspx?Screen=UserManagement" class="help-icon" target="_blank"
                        title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0" class="head-sub-menu">
            <tr>
                <td>
                    <div class="status-list">
                        <span>
                            <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                AutoPostBack="true" /></span>
                        <span>
                            <asp:CheckBox ID="chkSearchAC" runat="server" Text="A/C Only" OnCheckedChanged="FilterByCheckboxAC_OnCheckedChanged"
                                AutoPostBack="true" /></span>
                    </div>
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="dgMaintainUser" runat="server" AllowSorting="true" OnItemCreated="dgMaintainUser_ItemCreated"
            OnPageIndexChanged="dgMaintainGroupAccess_PageIndexChanged" Visible="true" OnNeedDataSource="dgMaintainUser_BindData"
            OnItemCommand="dgMaintainUser_ItemCommand" OnUpdateCommand="dgMaintainUser_UpdateCommand"
            OnInsertCommand="dgMaintainUser_InsertCommand" OnDeleteCommand="dgMaintainUser_DeleteCommand"
            AutoGenerateColumns="false" Height="341px" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgMaintainUser_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPreRender="dgMaintainUser_PreRender">
            <MasterTableView DataKeyNames="UserName,CustomerID,HomebaseID,ClientID,FirstName,MiddleName,LastName,HomeBase,TravelCoordID,IsActive
                                        ,IsUserLock,IsTripPrivacy,EmailID,PhoneNum,PassengerRequestorID,UVTripLinkID,UVTripLinkPassword,IsPrimaryContact
                                        ,IsSecondaryContact,IsSystemAdmin,IsDispatcher,IsOverrideCorp,IsCorpRequestApproval,LastUpdUID,LastUpdTS,IsDeleted,UserMasterID,IcaoID,ClientCD,TravelCoordCD"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="UserName" HeaderText="Display Name" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EmailId" HeaderText="E-mail" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="240px"
                        FilterControlWidth="220px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="220px"
                        FilterControlWidth="200px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="220px"
                        FilterControlWidth="200px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="Active" AutoPostBackOnFilter="true"
                        AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        HeaderStyle-Width="80px">
                        <%-- <FilterTemplate>
                        <telerik:RadComboBox ID="ImportedDeleteFilter" runat="server" OnClientSelectedIndexChanged="ImportedFilterDeleteSelectedIndexChanged"
                            SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("IsActive").CurrentFilterValue %>'
                            Width="70px" AppendDataBoundItems="true" OnItemDataBound="ImportedFilter_ItemDataBound">
                            <Items>
                                <telerik:RadComboBoxItem Text="NoFilter" Value="" />
                                <telerik:RadComboBoxItem Text="No" Value="0" />
                                <telerik:RadComboBoxItem Text="Yes" Value="1" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadScriptBlock ID="RadScriptBlock12" runat="server">
                            <script type="text/javascript">
                                function ImportedFilterDeleteSelectedIndexChanged(sender, args) {
                                    var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    var filterVal = args.get_item().get_value();
                                    if (filterVal == "") {
                                        tableView.filter("IsActive", filterVal, "NoFilter");
                                    }
                                    else if (filterVal == "1") {
                                        tableView.filter("IsActive", "1", "EqualTo");
                                    }
                                    else if (filterVal == "0") {
                                        tableView.filter("IsActive", "0", "EqualTo");
                                    }
                                } 
                            </script>
                        </telerik:RadScriptBlock>
                    </FilterTemplate>--%>
                    </telerik:GridCheckBoxColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                            Visible="false" runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                    <div style="padding: 5px 5px; float: right;">
                        <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td class="tdLabel100" style="display: none">
                                    <asp:CheckBox runat="server" ID="chkUserLock" Text="User Lock" Enabled="false" />
                                </td>
                                <td class="tdLabel150">
                                    <asp:CheckBox runat="server" ID="chkTrip" Text="Trip Privacy Setting" />
                                </td>
                                <td class="tdLabel210">
                                    <asp:CheckBox ID="chkSysAdmin" runat="server" Text="Sys. Adm. - Unrestricted Access" />
                                </td>
                                <td class="tdLabel100">
                                    <asp:CheckBox ID="chkDispatcher" runat="server" Text="Dispatcher" />
                                </td>
                                <td align="left">
                                    <asp:CheckBox runat="server" ID="chkActive" Text="Active" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <div class="tblspace_5">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel150" valign="top">
                                    <span class=" mnd_text">
                                        <asp:Label ID="lbUserName" runat="server" Text="E-mail Address/Login ID"></asp:Label></span>
                                </td>
                                <td class="tdLabel150">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbEmail" runat="server" ValidationGroup="save" CssClass="text560"
                                                    OnTextChanged="tbEmail_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="save" Display="Dynamic"
                                                    ControlToValidate="tbEmail" CssClass="alert-text" runat="server" ErrorMessage="E-mail is Required"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="tbEmailInvalid" runat="server" ControlToValidate="tbEmail"
                                                    ErrorMessage="Unique E-mail id required" Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="save" SetFocusOnError="true" OnServerValidate="tbEmail_ServerValidate"></asp:CustomValidator>
                                                <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbEmail"
                                                    CssClass="alert-text" runat="server" ID="RegularExpressionValidator2" ErrorMessage="Please Enter valid E-mail"
                                                    ValidationExpression="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel150" valign="top">
                                    <span class="mnd_text">
                                        <asp:Label ID="lbEmail" runat="server" Text="Display Name"></asp:Label></span>
                                </td>
                                <td class="tdLabel360">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbUserName" runat="server" ValidationGroup="save" CssClass="text310"></asp:TextBox>
                                                <asp:HiddenField ID="hdnUserid" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="save" Display="Dynamic"
                                                    ControlToValidate="tbUserName" CssClass="alert-text" runat="server" ErrorMessage="Display Name is Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" class="tdLabel145">
                                    <asp:Button ID="btnResetPwd" Text="Reset User Password" CssClass="button_small_none"
                                        runat="server" OnClick="ResetPassword_Click" />
                                </td>
                                <td valign="top">
                                    <asp:Button ID="btnUnlockUser" Text="Unlock User" CssClass="button_small_none" runat="server"
                                        OnClick="UnlockUser_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel150" valign="top">
                                    <span class=" mnd_text">
                                        <asp:Label ID="lbFirstName" runat="server" Text="First Name"></asp:Label></span>
                                </td>
                                <td class="tdLabel150">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbFirstName" runat="server" CssClass="text100" ValidationGroup="save"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="save" Display="Dynamic"
                                                    ControlToValidate="tbFirstName" CssClass="alert-text" runat="server" ErrorMessage="First Name is Required"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbFirstName"
                                                    CssClass="alert-text" runat="server" ID="reFirstName" ErrorMessage="Please Enter valid First Name"
                                                    ValidationExpression="([a-zA-Z]{3,30}\s*)+"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel80" valign="top">
                                    <asp:Label ID="lbMiddleName" runat="server" Text="Middle Name"></asp:Label>
                                </td>
                                <td class="tdLabel150" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbMiddleName" runat="server" ValidationGroup="save" CssClass="text100"
                                                    MaxLength="60"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="save" Display="Dynamic"
                                                    ControlToValidate="tbMiddleName" CssClass="alert-text" runat="server" ErrorMessage="Middle Name is Required"></asp:RequiredFieldValidator>--%>
                                                <%--<asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbMiddleName"
                                                    CssClass="alert-text" runat="server" ID="RegularExpressionValidator1" ErrorMessage="Please Enter valid Middle Name"
                                                    ValidationExpression="([a-zA-Z]{3,30}\s*)+"></asp:RegularExpressionValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel80" valign="top">
                                    <span class="mnd_text">
                                        <asp:Label ID="lbLastName" runat="server" Text="Last Name"></asp:Label></span>
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbLastName" runat="server" ValidationGroup="save" CssClass="text100"
                                                    MaxLength="60"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="save" Display="Dynamic"
                                                    ControlToValidate="tbLastName" CssClass="alert-text" runat="server" ErrorMessage="Last Name is Required"></asp:RequiredFieldValidator>
                                                <%-- <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbLastName"
                                                    CssClass="alert-text" runat="server" ID="RegularExpressionValidator4" ErrorMessage="Enter valid Last Name"
                                                    ValidationExpression="([a-zA-Z]{3,30}\s*)+"></asp:RegularExpressionValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel150" valign="top">
                                    <span class="mnd_text">
                                        <asp:Label ID="lbHome" runat="server" Text="Home Base"></asp:Label></span>
                                </td>
                                <td class="tdLabel150" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbHomebase" runat="server" ValidationGroup="save" CssClass="text100"
                                                    AutoPostBack="true" OnTextChanged="tbHomeBase_TextChanged"></asp:TextBox>
                                                <asp:Button ID="Button1" OnClientClick="javascript:openWin('radCompanyMasterPopup');return false;"
                                                    CssClass="browse-button" runat="server" />
                                                <asp:HiddenField ID="hdnHomebaseID" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvHomebase" runat="server" ControlToValidate="tbHomebase"
                                                    ErrorMessage="Home Base is Required" Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="rfvHomebaseInvalid" runat="server" ControlToValidate="tbHomebase"
                                                    ErrorMessage="Home Base does not exist" Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="save" SetFocusOnError="true" OnServerValidate="rfvHomebaseInvalid_ServerValidate"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel80" valign="top">
                                    <asp:Label ID="lbClientCode" runat="server" Text="Client Code"></asp:Label>
                                </td>
                                <td class="tdLabel150" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbClientCode" runat="server" CssClass="text100" ValidationGroup="save"
                                                    onKeyPress="return fnAllowAlphaNumeric(this, event)" AutoPostBack="true" OnTextChanged="tbClientCode_TextChanged"></asp:TextBox>
                                                <asp:Button ID="btnClientCode" OnClientClick="javascript:openWin('RadWindow1');return false;"
                                                    CssClass="browse-button" runat="server" />
                                                <%--<asp:Button ID="btnClientCode" runat="server" OnClientClick="javascript:openWin('../Company/ClientCodePopup.aspx?ClientCD=',document.getElementById('<%=tbClientCode.ClientID%>').value,'RadWindow1');return false;"
                                                    CssClass="browse-button" />--%>
                                                <asp:HiddenField ID="hdnClientID" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="tbClientCodeInvalid" runat="server" ControlToValidate="tbClientCode"
                                                    ErrorMessage="Client does not exist" Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="save" SetFocusOnError="true" OnServerValidate="tbClientCode_ServerValidate"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel150" valign="top">
                                    <asp:Label ID="lbPhone" runat="server" Text="Phone"></asp:Label>
                                </td>
                                <td class="tdLabel150" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbPhone" CssClass="text100" runat="server" ValidationGroup="save"
                                                    onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                <%-- <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbPhone"
                                            CssClass="alert-text" runat="server" ID="RegularExpressionValidator3" ErrorMessage="Enter valid Phone"
                                            ValidationExpression="((?:0[1-46-9]\d{3})|(?:[1-357-9]\d{4})|(?:[4][0-24-9]\d{3})|(?:[6][013-9]\d{3}))"></asp:RegularExpressionValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <div class="tblspace_5">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel150">
                                    <asp:Label ID="lbUVtriplink" runat="server" Text="Universal Username"></asp:Label>
                                </td>
                                <td class="tdLabel150">
                                    <asp:TextBox ID="tbUVtriplink" runat="server" ValidationGroup="save" CssClass="text100"></asp:TextBox>
                                </td>
                                <td class="tdLabel160">
                                    <asp:RadioButton runat="server" ID="radPrimary" Text="Primary Contact" GroupName="Contact" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel150">
                                    <asp:Label ID="lbUVtriplinkPassword" runat="server" Text="Universal Password"></asp:Label>
                                </td>
                                <td class="tdLabel150">
                                    <asp:TextBox ID="tbUVtriplinkPassword" runat="server" TextMode="Password" CssClass="text100"></asp:TextBox>
                                </td>
                                <td class="tdLabel160">
                                    <asp:RadioButton runat="server" ID="radSecondary" Text="Secondary Contact" GroupName="Contact" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <div class="tblspace_10">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel230">
                                    <span class="mnd_text">
                                        <asp:Label runat="server" ID="lbUserGroups" Text="Available User Group"></asp:Label>
                                    </span>
                                </td>
                                <td>
                                    <span class="mnd_text">Assigned User Group </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstAvailable" runat="server" Height="200px" SelectionMode="Multiple"
                                        Width="200px"></asp:ListBox>
                                </td>
                                <td valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnNext" runat="server" CssClass="icon-next" OnClick="btnNext_click"
                                                    ToolTip="Move selected items from Available List" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnLast" runat="server" CssClass="icon-last" OnClick="btnLast_click"
                                                    ToolTip="Move all items from Available List" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFirst" runat="server" CssClass="icon-first" OnClick="btnFirst_click"
                                                    ToolTip="Move all items from Selected List" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnPrevious" runat="server" CssClass="icon-prev" OnClick="btnPrevious_click"
                                                    ToolTip="Move selected items from Selected List" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:ListBox ID="lstSelected" runat="server" Width="200px" Height="200px" SelectionMode="Multiple">
                                    </asp:ListBox>
                                    <asp:ListBox ID="lstHidden" runat="server" SelectionMode="Multiple" Visible="false">
                                    </asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td align="left">
                                    <%--<asp:RequiredFieldValidator ID="rfvlstSelected" runat="server" ControlToValidate="lstSelected"
                                                    ValidationGroup="save" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">User Group Code is required</asp:RequiredFieldValidator>--%>
                                    <asp:CustomValidator ID="rfvlstSelected" runat="server" Display="Dynamic" CssClass="alert-text"
                                        OnServerValidate="rfvlstSelected_ServerValidate" ValidationGroup="save" SetFocusOnError="true">A user must be marked as Sys Admin - Unrestricted access or he/she should be mapped to at least one user group</asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" CssClass="button" ValidationGroup="save"
                            OnClick="SaveChanges_Click" CausesValidation="true" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                            CausesValidation="false" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
