﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.AdminService;
using System.Collections;
using System.Web.Security;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Data;
using AjaxControlToolkit;


namespace FlightPak.Web.Admin
{
    public partial class MaintainUser : BaseSecuredPage
    {
        private List<string> listUserCodes = new List<string>();
        ArrayList userGroupArray = new ArrayList();
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!UserPrincipal.Identity._isSysAdmin)
                        {
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        }
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgMaintainUser, dgMaintainUser, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgMaintainUser.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                        // testUpdate();
                        if (!IsPostBack)
                        {
                            chkSearchActiveOnly.Checked = true;
                            chkSearchAC.Checked = false;
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }

        protected void dgMaintainGroupAccess_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgMaintainUser.ClientSettings.Scrolling.ScrollTop = "0";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }

        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (BindDataSwitch)
                {
                    dgMaintainUser.Rebind();
                }
                
                if (dgMaintainUser.MasterTableView.Items.Count > 0)
                {
                    //if (!IsPostBack)
                    //{
                    //    Session["UserSelectedId"] = null;
                    //}
                    if (Session["UserSelectedId"] == null)
                    {
                        dgMaintainUser.SelectedIndexes.Add(0);
                        Session["UserSelectedItem"] = dgMaintainUser.Items[0].GetDataKeyValue("UserName").ToString();
                        Session["UserSelectedId"] = dgMaintainUser.Items[0].GetDataKeyValue("UserMasterID").ToString();
                    }

                    if (dgMaintainUser.SelectedIndexes.Count == 0)
                        dgMaintainUser.SelectedIndexes.Add(0);

                    ReadOnlyForm();
                    tbUserName.Enabled = false;
                    GridEnable(true, true, true);
                    LoadSelectedList();
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgMaintainUser;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainUser_ItemCreated(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }

        /// <summary>
        /// To validate Home base
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnHomebaseID.Value = "";
                        if (!string.IsNullOrEmpty(tbHomebase.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetCompanyMasterListInfo();
                                List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();

                                if (objRetVal.ReturnFlag)
                                {

                                    CompanyList = objRetVal.EntityList.Where(x => x.HomebaseCD.ToString().ToUpper().Trim().Equals(tbHomebase.Text.ToUpper().Trim())).ToList();
                                    if (CompanyList != null && CompanyList.Count > 0)
                                    {
                                        hdnHomebaseID.Value = CompanyList[0].HomebaseID.ToString();
                                        tbHomebase.Text = CompanyList[0].HomebaseCD;
                                        rfvHomebaseInvalid.Text = "";
                                        tbClientCode.Focus();
                                    }
                                    else
                                    {
                                        hdnHomebaseID.Value = "";
                                        rfvHomebaseInvalid.IsValid = false;
                                        tbHomebase.Focus();
                                    }
                                }
                                else
                                {
                                    hdnHomebaseID.Value = "";
                                    rfvHomebaseInvalid.IsValid = false;
                                    tbHomebase.Focus();
                                }
                            }
                        }
                        else
                        {
                            hdnHomebaseID.Value = "";
                            rfvHomebaseInvalid.IsValid = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }

        /// <summary>
        /// To validate Client Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbClientCode_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnClientID.Value = "";
                        if (!string.IsNullOrEmpty(tbClientCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetClientCodeList();
                                List<FlightPakMasterService.Client> Client = new List<FlightPakMasterService.Client>();

                                if (objRetVal.ReturnFlag)
                                {

                                    Client = objRetVal.EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbClientCode.Text.ToUpper().Trim())).ToList();
                                    if (Client != null && Client.Count > 0)
                                    {
                                        hdnClientID.Value = Client[0].ClientID.ToString();
                                        tbClientCode.Text = Client[0].ClientCD;
                                        tbClientCodeInvalid.Text = "";
                                    }
                                    else
                                    {
                                        hdnClientID.Value = "";
                                        tbClientCodeInvalid.IsValid = false;
                                        tbClientCode.Focus();
                                    }
                                }
                                else
                                {
                                    hdnClientID.Value = "";
                                    tbClientCodeInvalid.IsValid = false;
                                    tbClientCode.Focus();
                                }
                            }
                        }
                        else
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient client = new FlightPakMasterService.MasterCatalogServiceClient())
                            {

                            }
                            hdnClientID.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgMaintainUser_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                        {
                            //objService.TryService<Type, AdminService.AdminServiceClient>(objService.GetUserMasterList(), true);

                            var objUserVal = objService.GetUserMasterList();
                            List<AdminService.GetAllUserMasterResult> lstUserMaster = new List<AdminService.GetAllUserMasterResult>();
                            if (objUserVal.ReturnFlag == true)
                            {
                                lstUserMaster = objUserVal.EntityList.ToList();
                                dgMaintainUser.DataSource = lstUserMaster;
                                Session["MaintainUser"] = (List<GetAllUserMasterResult>)objUserVal.EntityList.ToList();
                            }
                            Session.Remove("UserName");
                            listUserCodes = new List<string>();
                            foreach (AdminService.GetAllUserMasterResult userEntity in objUserVal.EntityList)
                            {
                                listUserCodes.Add(Convert.ToString(userEntity.UserName).ToLower().Trim());
                            }
                            Session["UserName"] = lstUserMaster;

                            if (chkSearchActiveOnly != null)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }
        /// <summary>
        /// Item Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainUser_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                DisplayEditForm();
                                SelectItem();
                                GridEnable(false, true, false);
                                tbFirstName.Focus();
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.UserMaster, Convert.ToInt64(Session["UserSelectedId"].ToString().Trim()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.UserMaster);
                                        DefaultSelection(true);
                                        return;
                                    }
                                }
                                chkSearchActiveOnly.Enabled = false;
                                chkSearchAC.Enabled = false;
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgMaintainUser.SelectedIndexes.Clear();
                                tbFirstName.Enabled = true;
                                //tbFirstName.BackColor = System.Drawing.Color.White;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                lstSelected.Items.Clear();
                                tbUserName.Focus();
                                chkSearchActiveOnly.Enabled = false;
                                chkSearchAC.Enabled = false;
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }
        /// <summary>
        /// Update Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgMaintainUser_UpdateCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (!CheckCanEdit())
                        {
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Maximum user count reached. Please inactivate users to add new users', 360, 50, 'Maintain User');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        else if (Session["UserSelectedItem"] != null)
                        {
                            SaveUserMasterAvailableList();
                            SaveUserMasterSelectedList();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    e.Canceled = true;
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
                finally
                {

                }
            }

        }
        /// <summary>
        /// Insert Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgMaintainUser_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Unique UserName is Required', 360, 50, 'Maintain User');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        else if (!CheckCanAdd())
                        {
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Maximum user count reached. Please inactivate users to add new users', 360, 50, 'Maintain User');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        else
                        {
                            using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                            {
                                AdminService.UserMaster userMaster = new AdminService.UserMaster();
                                bool isClient = false;
                                //userMaster.UserID = Guid.Parse(hdnUserid.Value); // Commented as UserID does not exist
                                userMaster.UserName = tbUserName.Text;
                                userMaster.CustomerID = base.UserPrincipal.Identity._customerID;
                                userMaster.FirstName = tbFirstName.Text;
                                userMaster.MiddleName = tbMiddleName.Text;
                                userMaster.LastName = tbLastName.Text;
                                if (!String.IsNullOrEmpty(tbHomebase.Text))
                                {
                                    userMaster.HomebaseID = Convert.ToInt64(hdnHomebaseID.Value);
                                }
                                if (!String.IsNullOrEmpty(tbClientCode.Text))
                                {
                                    userMaster.ClientID = Convert.ToInt64(hdnClientID.Value);
                                }  // Converted to Int32
                                userMaster.IsActive = chkActive.Checked;
                                userMaster.IsUserLock = chkUserLock.Checked;
                                userMaster.IsTripPrivacy = chkTrip.Checked;
                                userMaster.EmailID = tbEmail.Text;
                                userMaster.PhoneNum = tbPhone.Text;
                                //userMaster.PassengerRequestorCD=Not needed;
                                userMaster.UVTripLinkID = tbUVtriplink.Text.Trim();
                                userMaster.UVTripLinkPassword = tbUVtriplinkPassword.Text;
                                userMaster.IsPrimaryContact = radPrimary.Checked;
                                userMaster.IsSecondaryContact = radSecondary.Checked;
                                userMaster.IsSystemAdmin = chkSysAdmin.Checked;
                                userMaster.IsDispatcher = chkDispatcher.Checked;
                                userMaster.IsDeleted = false;
                                // bulk Update  by single stuff
                                AdminService.UserMaster ObjuserGroup = new AdminService.UserMaster();
                                ObjuserGroup.CustomerID = base.UserPrincipal.Identity._customerID; // Guid.Parse(hdnUserid.Value); // Commented as Customer Id is int
                                userMaster.UserGroupMapping = new List<AdminService.UserGroupMapping>();
                                string selValue = string.Empty;
                                int ItemCount = 0;
                                if (tbClientCode.Enabled && btnClientCode.Enabled && !string.IsNullOrEmpty(tbClientCode.Text) && !string.IsNullOrEmpty(hdnClientID.Value))
                                {
                                    if (userMaster.ClientCodeMapping == null) userMaster.ClientCodeMapping = new List<ClientCodeMapping>();
                                    userMaster.ClientCodeMapping.Add(new ClientCodeMapping
                                    {
                                        ClientID = Convert.ToInt64(hdnClientID.Value),
                                        CustomerID = UserPrincipal.Identity._customerID,
                                        IsDeleted = false,
                                        LastUpdTS = DateTime.UtcNow,
                                        LastUpdUID = UserPrincipal.Identity._name,
                                        UserName = tbUserName.Text.Trim()
                                    });
                                    isClient = true;
                                }
                                for (int iVal = 0; iVal < lstSelected.Items.Count; iVal++)
                                {
                                    ItemCount++;
                                    AdminService.UserGroupMapping objgroup = new AdminService.UserGroupMapping();
                                    ArrayList paxCode = new ArrayList(lstSelected.Items[iVal].Text.Split(new Char[] { '-' }));
                                    ArrayList paxCodeID = new ArrayList(lstSelected.Items[iVal].Value.Split(new Char[] { '-' }));
                                    selValue = paxCodeID[0].ToString().Replace("(", "").Replace(")", "").Trim();
                                    objgroup.CustomerID = base.UserPrincipal.Identity._customerID;
                                    objgroup.UserName = tbUserName.Text;  //objgroup.UserID = Guid.Parse(hdnUserid.Value);
                                    objgroup.UserGroupID = Convert.ToInt64(selValue);    //objgroup.UserGroupCD = selValue;
                                    objgroup.UserGroupMappingID = ItemCount;
                                    objgroup.IsDeleted = false;
                                    objgroup.LastUpdUID = UserPrincipal.Identity._name;
                                    objgroup.LastUpdTS = System.DateTime.UtcNow;
                                    userMaster.UserGroupMapping.Add(objgroup);
                                }
                                for (int iVal = 0; iVal < lstAvailable.Items.Count; iVal++)
                                {
                                    ItemCount++;
                                    AdminService.UserGroupMapping objgroup = new AdminService.UserGroupMapping();
                                    ArrayList paxCode = new ArrayList(lstAvailable.Items[iVal].Text.Split(new Char[] { '-' }));
                                    ArrayList paxCodeID = new ArrayList(lstAvailable.Items[iVal].Value.Split(new Char[] { '-' }));
                                    selValue = paxCodeID[0].ToString().Replace("(", "").Replace(")", "").Trim();
                                    objgroup.CustomerID = base.UserPrincipal.Identity._customerID;
                                    objgroup.UserName = tbUserName.Text;  //objgroup.UserID = Guid.Parse(hdnUserid.Value);
                                    objgroup.UserGroupID = Convert.ToInt64(selValue);    //objgroup.UserGroupCD = selValue;
                                    objgroup.UserGroupMappingID = ItemCount;
                                    objgroup.IsDeleted = true;
                                    //objgroup.LastUpdUID = "UC";
                                    objgroup.LastUpdTS = System.DateTime.UtcNow;
                                    userMaster.UserGroupMapping.Add(objgroup);
                                }
                                var returnValue = objService.AddUserMaster(userMaster, Membership.Provider.GetPassword(userMaster.UserName, "Add"), isClient);
                                if (returnValue.ReturnFlag)
                                {
                                    ShowSuccessMessage();
                                    GridEnable(true, true, true);
                                    DefaultSelection(false);
                                    _selectLastModified = true;

                                    chkSearchActiveOnly.Enabled = true;
                                    chkSearchAC.Enabled = true;
                                    refreshUserPrincipal();
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(returnValue.ErrorMessage, ModuleNameConstants.Database.UserMaster);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
                finally
                {
                    e.Canceled = true;
                    e.Item.OwnerTableView.IsItemInserted = false;
                    dgMaintainUser.Rebind();
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgMaintainUser_DeleteCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["UserSelectedItem"] != null)
                        {
                            using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                            {
                                AdminService.UserMaster userMaster = new AdminService.UserMaster();
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.UserMaster, Convert.ToInt64(Session["UserSelectedId"].ToString().Trim()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.UserMaster);
                                        return;
                                    }

                                    string Code = Session["UserSelectedItem"].ToString();
                                    foreach (GridDataItem Item in dgMaintainUser.MasterTableView.Items)
                                    {
                                        if (Item["UserName"].Text.Trim() == Code.Trim())
                                        {
                                            userMaster.UserName = Item.GetDataKeyValue("UserName").ToString();    //userMaster.UserID = Guid.Parse(item.GetDataKeyValue("UserID").ToString());
                                            userMaster.CustomerID = Convert.ToInt32(Item.GetDataKeyValue("CustomerID").ToString());

                                            userMaster.FirstName = Item.GetDataKeyValue("FirstName").ToString();
                                            userMaster.MiddleName = Item.GetDataKeyValue("MiddleName").ToString();
                                            userMaster.LastName = Item.GetDataKeyValue("LastName").ToString();
                                            break;
                                        }

                                    }

                                    userMaster.LastUpdUID = UserPrincipal.Identity._name;
                                    userMaster.LastUpdTS = System.DateTime.UtcNow;
                                    userMaster.IsDeleted = true;
                                    objService.DeleteUserMaster(userMaster);

                                    // Update Session when there is a change in UserPrincipal
                                    RefreshSession();

                                    DefaultSelection(true);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.UserMaster, Convert.ToInt64(Session["UserSelectedId"].ToString().Trim()));
                    }
                }
            }

        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                //Session["UserSelectedItem"] = (GridDataItem)dgMaintainUser.SelectedItems[0];
                                GridDataItem item = dgMaintainUser.SelectedItems[0] as GridDataItem;
                                Session["UserSelectedItem"] = item["UserName"].Text;
                                Session["UserSelectedId"] = item.GetDataKeyValue("UserMasterID").ToString();
                                ReadOnlyForm();
                                tbUserName.Enabled = false;
                                LoadSelectedList();
                                GridEnable(true, true, true);
                            }
                            else
                            {
                                SelectItem();
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                    }
                }
            }
        }
        /// <summary>
        /// Load Selected List Items in the Listbox
        /// </summary>
        protected void LoadSelectedList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                foreach (GridDataItem item in dgMaintainUser.MasterTableView.Items)
                {
                    if (item.Selected)
                    {
                        if (item.GetDataKeyValue("UserName") != null)
                        {
                            Session["SelectedUserName"] = item.GetDataKeyValue("UserName").ToString();
                        }
                        if (item.GetDataKeyValue("CustomerID") != null)
                        {
                            Session["customerID"] = item.GetDataKeyValue("CustomerID").ToString();
                        }
                        item.Selected = true;
                    }
                }
                //Guid UserID = Guid.Parse(Convert.ToString(Session["UserID"]));
                string UserName = Convert.ToString(Session["SelectedUserName"]);
                int CustomerID = Convert.ToInt32(Session["customerID"]);
                List<AdminService.UserGroup> userGroupAvailableList = new List<AdminService.UserGroup>();
                List<AdminService.UserGroup> userGroupSelectedList = new List<AdminService.UserGroup>();
                Hashtable tempAvlList = new Hashtable();
                Hashtable tempSelList = new Hashtable();
                userGroupAvailableList = GetAvailableListByCode(CustomerID, UserName);
                //foreach (AdminService.UserGroup uAvlList in userGroupAvailableList)
                //{
                //    userGroupArray.Add(uAvlList.UserGroupCD);
                //tempAvlList.Add(uAvlList.UserGroupDescription, uAvlList.UserGroupID);
                //}
                //Session["UserGroupArray"] = userGroupArray;
                userGroupSelectedList = GetSelectedListByCode(CustomerID, UserName);
                //foreach (AdminService.UserGroup upSelList in userGroupSelectedList)
                //{
                //tempSelList.Add(upSelList.UserGroupDescription, upSelList.UserGroupID);
                //}
                //lstAvailable.DataTextField = "Key";
                //lstAvailable.DataValueField = "Value";
                //lstAvailable.DataSource = tempAvlList;
                //lstAvailable.DataBind();
                //lstSelected.DataTextField = "Key";
                //lstSelected.DataValueField = "Value";
                //lstSelected.DataSource = tempSelList;
                //lstSelected.DataBind();
                lstAvailable.DataTextField = "UserGroupDescription";
                lstAvailable.DataValueField = "UserGroupID";
                lstAvailable.DataSource = userGroupAvailableList;
                lstAvailable.DataBind();
                Session["userGroupAvailableList"] = userGroupAvailableList;
                lstSelected.DataTextField = "UserGroupDescription";
                lstSelected.DataValueField = "UserGroupID";
                lstSelected.DataSource = userGroupSelectedList;
                lstSelected.DataBind();
                Session["userGroupSelectedList"] = userGroupAvailableList;
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSave.Value = "Save";
                hdnUserid.Value = System.Guid.NewGuid().ToString();
                tbUserName.Text = string.Empty;
                //tbUserName.BackColor = System.Drawing.Color.White;
                tbFirstName.Text = string.Empty;
                tbMiddleName.Text = string.Empty;
                tbLastName.Text = string.Empty;
                tbHomebase.Text = string.Empty;
                hdnHomebaseID.Value = string.Empty;
                tbClientCode.Text = string.Empty;
                hdnClientID.Value = string.Empty;
                chkUserLock.Checked = false;
                chkTrip.Checked = false;
                tbEmail.Text = string.Empty;
                tbPhone.Text = string.Empty;
                tbUVtriplink.Text = string.Empty;
                tbUVtriplinkPassword.Text = string.Empty;
                radPrimary.Checked = false;
                radSecondary.Checked = false;
                chkSysAdmin.Checked = false;
                chkDispatcher.Checked = false;
                ClearForm();
                EnableForm(true);
                Hashtable tempAvlList = new Hashtable();
                List<AdminService.UserGroup> userGroupAvailableList = new List<AdminService.UserGroup>();
                userGroupAvailableList = GetAvailableListByCode(base.UserPrincipal.Identity._customerID, tbUserName.Text);
                //foreach (AdminService.UserGroup uAvlList in userGroupAvailableList)
                //{
                //    userGroupArray.Add(uAvlList.UserGroupCD);
                //    tempAvlList.Add(uAvlList.UserGroupCD, uAvlList.CustomerID);
                //}
                //lstAvailable.DataTextField = "Key";
                //lstAvailable.DataValueField = "Value";
                //lstAvailable.DataSource = tempAvlList;
                //lstAvailable.DataBind();
                lstAvailable.DataTextField = "UserGroupDescription";
                lstAvailable.DataValueField = "UserGroupID";
                lstAvailable.DataSource = userGroupAvailableList;
                lstAvailable.DataBind();
            }
        }
        /// <summary>
        /// Display Edit form, when click on Edit Button
        /// </summary>
        ///  /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LoadControlData();
                //tbUserName.ReadOnly = true;
                //tbUserName.BackColor = System.Drawing.Color.LightGray;                
                hdnSave.Value = "Update";
                hdnRedirect.Value = "";
                dgMaintainUser.Rebind();
                EnableForm(true);
                if (!UserPrincipal.Identity._isSysAdmin)
                {
                    tbUserName.Enabled = false;
                }
                else
                {
                    tbUserName.Enabled = true;
                }
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Passenger Group Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Page.IsValid)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgMaintainUser.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgMaintainUser.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }

        protected void rfvHomebaseInvalid_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnHomebaseID.Value = "";
                        if (!string.IsNullOrEmpty(tbHomebase.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetCompanyMasterListInfo();
                                List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();

                                if (objRetVal.ReturnFlag)
                                {

                                    CompanyList = objRetVal.EntityList.Where(x => x.HomebaseCD.ToString().ToUpper().Trim().Equals(tbHomebase.Text.ToUpper().Trim())).ToList();
                                    if (CompanyList != null && CompanyList.Count > 0)
                                    {
                                        hdnHomebaseID.Value = CompanyList[0].HomebaseID.ToString();
                                        tbHomebase.Text = CompanyList[0].HomebaseCD;
                                    }
                                    else
                                    {
                                        hdnHomebaseID.Value = "";
                                        e.IsValid = false;
                                        tbHomebase.Focus();
                                    }
                                }
                                else
                                {
                                    hdnHomebaseID.Value = "";
                                    e.IsValid = false;
                                    tbHomebase.Focus();
                                }
                            }
                        }
                        else
                        {
                            hdnHomebaseID.Value = "";
                            e.IsValid = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }

        protected void tbClientCode_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnClientID.Value = "";
                        if (!string.IsNullOrEmpty(tbClientCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetClientCodeList();
                                List<FlightPakMasterService.Client> Client = new List<FlightPakMasterService.Client>();

                                if (objRetVal.ReturnFlag)
                                {

                                    Client = objRetVal.EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbClientCode.Text.ToUpper().Trim())).ToList();
                                    if (Client != null && Client.Count > 0)
                                    {
                                        hdnClientID.Value = Client[0].ClientID.ToString();
                                        tbClientCode.Text = Client[0].ClientCD;
                                    }
                                    else
                                    {
                                        hdnClientID.Value = "";
                                        e.IsValid = false;
                                        tbClientCode.Focus();
                                    }
                                }
                                else
                                {
                                    hdnClientID.Value = "";
                                    e.IsValid = false;
                                    tbClientCode.Focus();
                                }
                            }
                        }
                        else
                        {
                            hdnClientID.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }

        protected void rfvlstSelected_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkSysAdmin.Checked || !(lstSelected.Items.Count == 0))
                            e.IsValid = true;
                        else
                            e.IsValid = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }
        protected void UnlockUser_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Vishwa;
                        string userName = string.Empty;
                        // GridDataItem item = (GridDataItem)Session["UserSelectedItem"];

                        string Code = Session["UserSelectedItem"].ToString();
                        foreach (GridDataItem Item in dgMaintainUser.MasterTableView.Items)
                        {
                            if (Item["UserName"].Text.Trim() == Code.Trim())
                            {
                                if (Item.GetDataKeyValue("UserName") != null)
                                {
                                    userName = Item.GetDataKeyValue("UserName").ToString().Trim();
                                }
                                else
                                {
                                    userName = string.Empty;
                                }
                                break;
                            }

                        }

                        using (AdminServiceClient client = new AdminServiceClient())
                        {
                            client.LockOrUnlockUser(userName, !chkUserLock.Checked);
                        }
                        dgMaintainUser.Rebind();
                        LoadControlData();
                        EnableForm(true);
                        if (!chkUserLock.Checked)
                            ShowAlert("Unlocked Successfully", "Unlock User");
                        else
                            ShowAlert("Locked Successfully", "Lock User");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }
        protected void ResetPassword_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Vishwa;
                        string userName = string.Empty;
                        // GridDataItem item = (GridDataItem)Session["UserSelectedItem"];

                        string Code = Session["UserSelectedItem"].ToString();
                        foreach (GridDataItem Item in dgMaintainUser.MasterTableView.Items)
                        {
                            if (Item["UserName"].Text.Trim() == Code.Trim())
                            {
                                if (Item.GetDataKeyValue("UserName") != null)
                                {
                                    userName = Item.GetDataKeyValue("UserName").ToString().Trim();
                                }
                                else
                                {
                                    userName = string.Empty;
                                }
                                break;
                            }

                        }

                        Membership.Provider.ResetPassword(userName, string.Empty);
                        ShowAlert("Information has been sent to registered email id", "Reset Password");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }
        /// <summary>
        /// Cancel Passenger Group Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["UserSelectedItem"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.UserMaster, Convert.ToInt64(Session["UserSelectedId"].ToString().Trim()));
                            }
                        }
                        //Session.Remove("UserSelectedItem");
                        //Session.Remove("UserSelectedId");
                        GridEnable(true, true, true);
                        DefaultSelection(false);
                        chkSearchActiveOnly.Enabled = true;
                        chkSearchAC.Enabled = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// Function to Check if Passenger Group Code Alerady Exists
        /// </summary>
        /// <returns></returns>
        private Boolean CheckAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //bool returnVal = false;
                //listUserCodes = (List<string>)Session["UserName"];
                //if (listUserCodes != null && listUserCodes.ToString().ToUpper().Trim() == (tbUserName.Text.ToString().ToUpper().Trim()))
                //{
                //    returnVal = true;
                //}
                //return returnVal;
                //GridDataItem item = (GridDataItem)Session["SelectedItem"];
                //string custnum = item.GetDataKeyValue("UserName").ToString();
                //List<AdminService.GetAllUserMasterResult> lstUserName = (List<AdminService.GetAllUserMasterResult>)Session["UserName"];
                //AdminService.UserMaster airportObject = new AdminService.UserMaster();
                //var results = (from a in lstUserName
                //               where a.UserName.Contains(tbUserName.Text.ToString().Trim()) && (a.CustomerID.ToString().ToUpper().Equals(base.UserPrincipal.Identity._customerID.ToString().ToUpper()))
                //               select a);
                //if (results.Count() > 0 && results != null)
                //    return true;
                //else
                //    return false;

                using (AdminServiceClient adminService = new AdminServiceClient())
                {
                    var returnValue = adminService.GetUserByUserName(tbUserName.Text.ToString());
                    if (returnValue != null && returnValue.EntityInfo != null && returnValue.EntityInfo.UserName.Length > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LinkButton insertCtl, delCtl, editCtl;

                insertCtl = (LinkButton)dgMaintainUser.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                delCtl = (LinkButton)dgMaintainUser.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                editCtl = (LinkButton)dgMaintainUser.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                if (add && CheckCanAdd())
                    insertCtl.Enabled = true;
                else
                    insertCtl.Enabled = false;
                if (delete)
                {
                    delCtl.Enabled = true;
                    delCtl.OnClientClick = "javascript:return ProcessDelete();";
                }
                else
                {
                    delCtl.Enabled = false;
                    delCtl.OnClientClick = string.Empty;
                }
                if (edit)
                {
                    editCtl.Enabled = true;
                    editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                }
                else
                {
                    editCtl.Enabled = false;
                    editCtl.OnClientClick = string.Empty;
                }
            }
        }

        private bool CheckCanAdd()
        {
            using (AdminServiceClient client = new AdminServiceClient())
            {
                var ActualUserCount = client.GetActualUserCount(UserPrincipal.Identity._customerID);
                var UsedUserCount = client.GetUsedUserCount(UserPrincipal.Identity._customerID);
                return ActualUserCount == null || UsedUserCount < ActualUserCount;
            }
        }

        private bool CheckCanEdit()
        {
            using (AdminServiceClient client = new AdminServiceClient())
            {
                var ActualUserCount = client.GetActualUserCount(UserPrincipal.Identity._customerID);
                var UsedUserCount = client.GetUsedUserCount(UserPrincipal.Identity._customerID);
                var ActualUser = client.GetUserByUserName(tbUserName.Text.Trim()).EntityInfo;
                //true scenario
                if (ActualUserCount == null || UsedUserCount < ActualUserCount || ActualUser.IsActive == chkActive.Checked || chkActive.Checked == false)
                    return true;
                return false;
                //return (ActualUserCount == null || UsedUserCount <= ActualUserCount) && (ActualUser != null && ActualUser.IsActive != chkActive.Checked && chkActive.Checked == true);
            }
        }
        /// <summary>
        /// Function to Enable / Diable the form fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (hdnSave.Value != "Update" && enable == true)
                { //tbUserName.ReadOnly = false; 
                    tbUserName.Enabled = true;
                }
                else
                {
                    if (UserPrincipal.Identity._isSysAdmin)
                    {
                        tbUserName.Enabled = true;
                    }
                    else
                    {
                        tbUserName.Enabled = false;
                    }
                }
                tbFirstName.Enabled = enable;
                tbMiddleName.Enabled = enable;
                tbLastName.Enabled = enable;
                tbHomebase.Enabled = enable;

                Button1.Enabled = enable;
                tbClientCode.Enabled = enable;
                btnClientCode.Enabled = enable;
                //}
                chkActive.Enabled = enable;
                //chkUserLock.Enabled = enable;
                chkTrip.Enabled = enable;
                tbEmail.Enabled = enable;
                tbPhone.Enabled = enable;
                tbUVtriplink.Enabled = enable;
                tbUVtriplinkPassword.Enabled = enable;
                radPrimary.Enabled = enable;
                radSecondary.Enabled = enable;
                chkSysAdmin.Enabled = enable;
                chkDispatcher.Enabled = enable;
                btnNext.Enabled = enable;
                btnPrevious.Enabled = enable;
                btnFirst.Enabled = enable;
                btnLast.Enabled = enable;
                btnSaveChanges.Visible = enable;
                btnCancel.Visible = enable;
                lstSelected.Enabled = enable;
                lstAvailable.Enabled = enable;
                btnResetPwd.Enabled = enable;
                btnUnlockUser.Enabled = enable;
                if (enable)
                {
                    btnResetPwd.CssClass = "button";
                    btnUnlockUser.CssClass = "button";
                }
                else
                {
                    btnResetPwd.CssClass = "button-disable";
                    btnUnlockUser.CssClass = "button-disable";
                }
                if (chkUserLock.Checked)
                    btnUnlockUser.Text = "Unlock User";
                else
                    btnUnlockUser.Text = "Lock User";
                if (hdnSave.Value != "Update")
                {
                    btnResetPwd.Enabled = false;
                    btnUnlockUser.Enabled = false;
                    
                }
                //Button1.Visible = enable;

                //btnClientCode.Visible = enable;
                //if (enable)
                //{
                //    btnClientCode.Attributes.Add("onclick", "javascript:alert(document.getElementById(" + tbClientCode.ClientID + "));openWin('../Company/ClientCodePopup.aspx?ClientCD=',document.getElementById(" + tbClientCode.ClientID + ").value,'RadWindow1');return false;");
                //}
                //Button2.Visible = enable;
            }
        }
        /// <summary>
        /// Function to Get Form Itesm
        /// </summary>
        /// <returns></returns>
        private void SaveUserMasterAvailableList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                AdminService.UserMaster userMaster = new AdminService.UserMaster();
                using (AdminService.AdminServiceClient adminservice = new AdminServiceClient())
                {
                    //userMaster.UserID = Guid.Parse(hdnUserid.Value);
                    userMaster.UserName = tbUserName.Text;
                    userMaster.CustomerID = base.UserPrincipal.Identity._customerID;
                    userMaster.FirstName = tbFirstName.Text;
                    userMaster.MiddleName = tbMiddleName.Text;
                    userMaster.LastName = tbLastName.Text;
                    if (!String.IsNullOrEmpty(tbHomebase.Text))
                    {
                        userMaster.HomeBase = tbHomebase.Text;
                        userMaster.HomebaseID = Convert.ToInt64(hdnHomebaseID.Value);
                    }
                    if (!String.IsNullOrEmpty(tbClientCode.Text))
                    {
                        userMaster.ClientID = Convert.ToInt64(hdnClientID.Value);
                    }  // Converted to Int32
                    userMaster.IsActive = chkActive.Checked;
                    userMaster.IsUserLock = chkUserLock.Checked;
                    userMaster.IsTripPrivacy = chkTrip.Checked;
                    userMaster.EmailID = tbEmail.Text;
                    userMaster.PhoneNum = tbPhone.Text;
                    //userMaster.PassengerRequestorCD=Not needed;
                    userMaster.UVTripLinkID = tbUVtriplink.Text.Trim();
                    userMaster.UVTripLinkPassword = tbUVtriplinkPassword.Text;
                    userMaster.IsPrimaryContact = radPrimary.Checked;
                    userMaster.IsSecondaryContact = radSecondary.Checked;
                    userMaster.IsSystemAdmin = chkSysAdmin.Checked;
                    userMaster.IsDispatcher = chkDispatcher.Checked;
                    userMaster.IsDeleted = false;
                    //userMaster.LastUpdUID = "UC";
                    userMaster.LastUpdTS = System.DateTime.UtcNow;
                    userMaster.IsDeleted = false;
                    // bulk Update  by single stuff
                    AdminService.UserMaster ObjuserGroup = new AdminService.UserMaster();
                    ObjuserGroup.CustomerID = base.UserPrincipal.Identity._customerID; //ObjuserGroup.CustomerID = Guid.Parse(hdnUserid.Value);
                    userMaster.UserGroupMapping = new List<AdminService.UserGroupMapping>();
                    string selText = string.Empty;
                    string selValue = string.Empty;
                    List<AdminService.UserGroup> userGroupAvailableList = new List<AdminService.UserGroup>();
                    Session["userGroupAvailableList"] = userGroupAvailableList;

                    for (int iVal = 0; iVal < lstAvailable.Items.Count; iVal++)
                    {
                        AdminService.UserGroupMapping objgroup = new AdminService.UserGroupMapping();
                        ArrayList UserCode = new ArrayList(lstAvailable.Items[iVal].Text.Split(new Char[] { '-' }));
                        ArrayList UserCodeID = new ArrayList(lstAvailable.Items[iVal].Value.Split(new Char[] { '-' }));
                        selText = UserCodeID[0].ToString().Replace("(", "").Replace(")", "").Trim();
                        objgroup.CustomerID = base.UserPrincipal.Identity._customerID;
                        objgroup.UserGroupMappingID = iVal + 1;
                        objgroup.UserName = tbUserName.Text; //Guid.Parse(hdnUserid.Value);
                        objgroup.UserGroupID = Convert.ToInt64(selText);
                        objgroup.IsDeleted = true;
                        ArrayList CustomerIDs = new ArrayList(lstAvailable.Items[iVal].Value.Split(new Char[] { '*' }));
                        selValue = CustomerIDs[0].ToString().Replace("(", "").Replace(")", "").Trim();
                        if (!selValue.ToUpper().Contains("0"))
                        {
                            objgroup.LastUpdTS = System.DateTime.UtcNow;
                        }
                        userMaster.UserGroupMapping.Add(objgroup);
                    }
                    adminservice.UpdateUserMaster(userMaster, lstAvailable.Items.Count, false);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void SaveUserMasterSelectedList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                AdminService.UserMaster userMaster = new AdminService.UserMaster();
                using (AdminService.AdminServiceClient adminservice = new AdminServiceClient())
                {
                    bool isClientCode = false;
                    //userMaster.UserID = Guid.Parse(hdnUserid.Value);
                    userMaster.UserName = tbUserName.Text;
                    userMaster.CustomerID = base.UserPrincipal.Identity._customerID;
                    userMaster.FirstName = tbFirstName.Text;
                    userMaster.MiddleName = tbMiddleName.Text;
                    userMaster.LastName = tbLastName.Text;
                    if (!String.IsNullOrEmpty(tbHomebase.Text))
                    {
                        userMaster.HomeBase = tbHomebase.Text;
                        userMaster.HomebaseID = Convert.ToInt64(hdnHomebaseID.Value);
                    }
                    if (!String.IsNullOrEmpty(tbClientCode.Text))
                    {
                        userMaster.ClientID = Convert.ToInt64(hdnClientID.Value);
                    }  // Converted to Int32
                    userMaster.IsActive = chkActive.Checked;
                    userMaster.IsUserLock = chkUserLock.Checked;
                    userMaster.IsTripPrivacy = chkTrip.Checked;
                    userMaster.EmailID = tbEmail.Text;
                    userMaster.PhoneNum = tbPhone.Text;
                    //userMaster.PassengerRequestorCD=Not needed;
                    userMaster.UVTripLinkID = tbUVtriplink.Text.Trim();
                    userMaster.UVTripLinkPassword = tbUVtriplinkPassword.Text;
                    userMaster.IsPrimaryContact = radPrimary.Checked;
                    userMaster.IsSecondaryContact = radSecondary.Checked;
                    userMaster.IsSystemAdmin = chkSysAdmin.Checked;
                    userMaster.IsDispatcher = chkDispatcher.Checked;
                    userMaster.IsDeleted = false;
                    //userMaster.LastUpdUID = "UC";
                    userMaster.LastUpdTS = System.DateTime.UtcNow;
                    userMaster.IsDeleted = false;
                    // bulk Update  by single stuff
                    AdminService.UserMaster ObjuserGroup = new AdminService.UserMaster();
                    ObjuserGroup.CustomerID = base.UserPrincipal.Identity._customerID; //Guid.Parse(hdnUserid.Value);
                    userMaster.UserGroupMapping = new List<AdminService.UserGroupMapping>();
                    string selText = string.Empty;
                    string selValue = string.Empty;
                    // If the user doesn't have client code mapping, has the possibility to add it here.
                    //if (UserPrincipal != null && !UserPrincipal.Identity._clientId.HasValue && !string.IsNullOrEmpty(tbClientCode.Text) && !string.IsNullOrEmpty(hdnClientID.Value))
                    if (tbClientCode.Enabled && btnClientCode.Enabled && !string.IsNullOrEmpty(tbClientCode.Text) && !string.IsNullOrEmpty(hdnClientID.Value))
                    {
                        if (userMaster.ClientCodeMapping == null) userMaster.ClientCodeMapping = new List<ClientCodeMapping>();
                        userMaster.ClientCodeMapping.Add(new ClientCodeMapping
                        {
                            ClientID = Convert.ToInt64(hdnClientID.Value),
                            CustomerID = UserPrincipal.Identity._customerID,
                            IsDeleted = false,
                            LastUpdTS = DateTime.UtcNow,
                            LastUpdUID = UserPrincipal.Identity._name,
                            UserName = tbUserName.Text.Trim()
                        });
                        isClientCode = true;
                    }
                    for (int iVal = 0; iVal < lstSelected.Items.Count; iVal++)
                    {
                        AdminService.UserGroupMapping objgroup = new AdminService.UserGroupMapping();
                        ArrayList UserCode = new ArrayList(lstSelected.Items[iVal].Text.Split(new Char[] { '-' }));
                        ArrayList UserCodeId = new ArrayList(lstSelected.Items[iVal].Value.Split(new Char[] { '-' }));
                        selText = UserCodeId[0].ToString().Replace("(", "").Replace(")", "").Trim();
                        objgroup.UserGroupMappingID = iVal + 1;
                        objgroup.CustomerID = base.UserPrincipal.Identity._customerID;
                        objgroup.UserName = tbUserName.Text;//Guid.Parse(hdnUserid.Value);
                        objgroup.UserGroupID = Convert.ToInt64(selText);
                        objgroup.IsDeleted = false;
                        ArrayList CustomerIDs = new ArrayList(lstSelected.Items[iVal].Value.Split(new Char[] { '*' }));
                        selValue = CustomerIDs[0].ToString().Replace("(", "").Replace(")", "").Trim();
                        if (!selValue.ToUpper().Contains("0"))
                        {
                            objgroup.LastUpdTS = System.DateTime.UtcNow;
                        }
                        userMaster.UserGroupMapping.Add(objgroup);
                    }
                    var returnValue = adminservice.UpdateUserMaster(userMaster, lstSelected.Items.Count, isClientCode);
                    if (returnValue.ReturnFlag)
                    {
                        /////Update Method UnLock
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            CommonService.UnLock(EntitySet.Database.UserMaster, Convert.ToInt64(Session["UserSelectedId"].ToString().Trim()));
                        }
                        dgMaintainUser.Rebind();
                        ShowSuccessMessage();

                        // Update Session when there is a change in UserPrincipal
                        RefreshSession();

                        //UserService.UpdateUserMaster(GetFormItemsSel());                       
                        GridEnable(true, true, true);
                        SaveUserGroups(Session["UserName"].ToString().Trim());

                        DefaultSelection(false);
                        chkSearchActiveOnly.Enabled = true;
                        chkSearchAC.Enabled = true;
                        _selectLastModified = true;

                        refreshUserPrincipal();
                    }
                    else
                    {
                        //For Data Anotation
                        ProcessErrorMessage(returnValue.ErrorMessage, ModuleNameConstants.Database.UserMaster);
                    }
                }
            }
        }
        /// <summary>
        /// Delete from [UserMasterOrder] Table, based on PaxGroup Code
        /// </summary>
        /// <param name="paxCode">Pass Passenger Code</param>
        /// <param name="paxGroupCode">Pass Passenger Group Code</param>
        protected void DeleteSeletedItem(string paxCode, string paxGroupCode)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerID"></param>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public List<AdminService.UserGroup> GetAvailableListByCode(long customerID, string UserName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerID, UserName))
            {
                using (AdminService.AdminServiceClient PaxService = new AdminService.AdminServiceClient())
                {
                    var ObjRetVal = PaxService.GetUserGroupAvailableList(UserName, customerID);
                    return ObjRetVal.EntityList.OrderBy(x => x.UserGroupDescription).ToList();
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<AdminService.UserGroup> GetSelectedListByCode(int customerID, string UserName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerID, UserName))
            {
                using (AdminService.AdminServiceClient PaxService = new AdminService.AdminServiceClient())
                {
                    var ObjRetVal = PaxService.GetUserGroupSelectedList(UserName, customerID);
                    return ObjRetVal.EntityList.OrderBy(x => x.UserGroupDescription).ToList();
                }
            }
        }
        /// <summary>
        /// Save Selected Passenger Orders into Table
        /// </summary>
        /// <param name="paxGroupCode">Pass Passenger Group Code</param>
        protected void SaveUserGroups(string paxGroupCode)
        {

        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LoadControlData();
                EnableForm(false);
            }
        }
        /// <summary>
        /// Load Selected Item Data into the Form Fields
        /// </summary>
        private void LoadControlData()
        {
            //GridDataItem item = (GridDataItem)Session["UserSelectedItem"];
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["UserSelectedItem"] != null)
                {
                    string Code = Session["UserSelectedItem"].ToString();

                    foreach (GridDataItem Item in dgMaintainUser.MasterTableView.Items)
                    {
                        if (Item["UserName"].Text.Trim() == Code.Trim())
                        {
                            string LastUpdUID = string.Empty, LastUpdTS = string.Empty;
                            if (Item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                LastUpdUID = Item.GetDataKeyValue("LastUpdUID").ToString().Trim().Replace("&nbsp;", "");
                            }
                            else
                            {
                                LastUpdUID = string.Empty;
                            }
                            if (Item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                LastUpdTS = Item.GetDataKeyValue("LastUpdTS").ToString().Trim().Replace("&nbsp;", "");
                            }
                            else
                            {
                                LastUpdTS = string.Empty;
                            }
                            if (Item.GetDataKeyValue("UserName") != null)
                            {
                                hdnUserid.Value = Item.GetDataKeyValue("UserName").ToString().Trim();
                            }
                            else
                            {
                                hdnUserid.Value = string.Empty;
                            }
                            if (Item.GetDataKeyValue("FirstName") != null)
                            {
                                tbFirstName.Text = Item.GetDataKeyValue("FirstName").ToString().Trim();
                            }
                            else
                            {
                                tbFirstName.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("IsActive") != null)
                            {
                                chkActive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsActive").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsActive").ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                chkActive.Checked = false;
                            }
                            if (Item.GetDataKeyValue("IsUserLock") != null)
                            {
                                chkUserLock.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsUserLock").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsUserLock").ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                chkUserLock.Checked = false;
                            }
                            if (Item.GetDataKeyValue("IsTripPrivacy") != null)
                            {
                                chkTrip.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsTripPrivacy").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsTripPrivacy").ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                chkTrip.Checked = false;
                            }
                            if (Item.GetDataKeyValue("MiddleName") != null)
                            {
                                tbMiddleName.Text = Item.GetDataKeyValue("MiddleName").ToString().Trim();
                            }
                            else
                            {
                                tbMiddleName.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("EmailID") != null)
                            {
                                tbEmail.Text = Item.GetDataKeyValue("EmailID").ToString().Trim();
                            }
                            else
                            {
                                tbEmail.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("LastName") != null)
                            {
                                tbLastName.Text = Item.GetDataKeyValue("LastName").ToString().Trim();
                            }
                            else
                            {
                                tbLastName.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("PhoneNum") != null)
                            {
                                tbPhone.Text = Item.GetDataKeyValue("PhoneNum").ToString().Trim();
                            }
                            else
                            {
                                tbPhone.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("UserName") != null)
                            {
                                tbUserName.Text = Item.GetDataKeyValue("UserName").ToString().Trim();
                            }
                            else
                            {
                                tbUserName.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ClientID") != null)
                            {
                                hdnClientID.Value = Item.GetDataKeyValue("ClientID").ToString().Trim();
                            }
                            else
                            {
                                hdnClientID.Value = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ClientCD") != null)
                            {
                                tbClientCode.Text = Item.GetDataKeyValue("ClientCD").ToString().Trim();
                            }
                            else
                            {
                                tbClientCode.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("HomebaseID") != null)
                            {
                                hdnHomebaseID.Value = Item.GetDataKeyValue("HomebaseID").ToString().Trim();
                            }
                            else
                            {
                                hdnHomebaseID.Value = string.Empty;
                            }
                            if (Item.GetDataKeyValue("IcaoID") != null)
                            {
                                tbHomebase.Text = Item.GetDataKeyValue("IcaoID").ToString().Trim();
                            }
                            else
                            {
                                tbHomebase.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("UVTripLinkID") != null)
                            {
                                tbUVtriplink.Text = Item.GetDataKeyValue("UVTripLinkID").ToString().Trim();
                            }
                            else
                            {
                                tbUVtriplink.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("UVTripLinkPassword") != null)
                            {
                                tbUVtriplinkPassword.Attributes.Add("value", Item.GetDataKeyValue("UVTripLinkPassword").ToString().Trim());
                            }
                            else
                            {
                                tbUVtriplinkPassword.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("IsSecondaryContact") != null)
                            {
                                radSecondary.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsSecondaryContact").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsSecondaryContact").ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                radSecondary.Checked = false;
                            }
                            if (Item.GetDataKeyValue("IsPrimaryContact") != null)
                            {
                                radPrimary.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsPrimaryContact").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsPrimaryContact").ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                radPrimary.Checked = false;
                            }
                            if (Item.GetDataKeyValue("IsSystemAdmin") != null)
                            {
                                chkSysAdmin.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsSystemAdmin").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsSystemAdmin").ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                chkSysAdmin.Checked = false;
                            }
                            if (Item.GetDataKeyValue("IsDispatcher") != null)
                            {
                                chkDispatcher.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsDispatcher").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsDispatcher").ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                chkDispatcher.Checked = false;
                            }
                            Label lbUser = (Label)dgMaintainUser.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            //Label lbUser = (Label)Item.FindControl("lbLastUpdatedUser");

                            if (!string.IsNullOrEmpty(LastUpdUID))
                            {
                                lbUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + LastUpdUID);
                            }
                            else
                            {
                                lbUser.Text = string.Empty;
                            }

                            if (!string.IsNullOrEmpty(LastUpdTS))
                            {
                                //lbUser.Text = lbUser.Text + " Date: " + GetDate(Convert.ToDateTime(LastUpdTS));
                                lbUser.Text = System.Web.HttpUtility.HtmlEncode(lbUser.Text + " Date: " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " hh:mm tt}", GetDate(Convert.ToDateTime(LastUpdTS))));

                            }
                            lbColumnName1.Text = "User Name";
                            lbColumnName2.Text = "E-mail";
                            lbColumnValue1.Text = Item["UserName"].Text;
                            lbColumnValue2.Text = Item.GetDataKeyValue("EmailID").ToString().Trim();
                            break;
                        }
                    }


                }
            }


        }
        /// <summary>
        /// To Clear the Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            //tbCode.Text = string.Empty;
            //tbDescription.Text = string.Empty;
            //tbHomeBase.Text = string.Empty;
            //btnHomeBase.Visible = false;
            //lstAvailable.Items.Clear();
            //lstSelected.Items.Clear();
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["UserSelectedId"] != null)
                    {
                        string ID = Session["UserSelectedId"].ToString();
                        foreach (GridDataItem Item in dgMaintainUser.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("UserMasterID") != null)
                            {
                                if (Item.GetDataKeyValue("UserMasterID").ToString().Trim() == ID.Trim())
                                {
                                    Item.Selected = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbClientCode.Text != null && tbClientCode.Text.Trim() != string.Empty)
                        {
                            FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient();
                            var objRetVal = objService.GetClientMasterList().EntityList.Where(x => x.ClientCD.Trim().ToUpper() == (tbClientCode.Text.ToString().ToUpper().Trim()));
                            if (objRetVal.Count() > 0 && objRetVal != null)
                            {
                                //cvCode.IsValid = false;
                                tbClientCode.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }
        /// <summary>
        /// To check Valid Home Base
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {

        }
        #region "Move Listbox items from one to another"
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btnNext_click(object sender, EventArgs e)
        //{
        //    if (lstAvailable.SelectedItem != null)
        //    {
        //        string listSelected = string.Empty;
        //        ArrayList selectedList = new ArrayList();
        //        for (int i = lstAvailable.Items.Count - 1; i >= 0; i--)
        //        {
        //            if (lstAvailable.Items[i].Selected)
        //            {
        //                listSelected = lstAvailable.Items[i].ToString();
        //                ArrayList tempSelectedList = new ArrayList(listSelected.Split(new Char[] { '-' }));
        //                selectedList.Add(tempSelectedList[0].ToString());
        //                lstSelected.Items.Add(lstAvailable.Items[i]);
        //                lstAvailable.Items.RemoveAt(i);
        //            }
        //        }
        //    }
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btnPrevious_click(object sender, EventArgs e)
        //{
        //    if (lstSelected.SelectedItem != null)
        //    {
        //        string listSelected = string.Empty;
        //        ArrayList selectedList = new ArrayList();
        //        for (int i = lstSelected.Items.Count - 1; i >= 0; i--)
        //        {
        //            if (lstSelected.Items[i].Selected)
        //            {
        //                listSelected = lstSelected.Items[i].ToString();
        //                ArrayList tempSelectedList = new ArrayList(listSelected.Split(new Char[] { '-' }));
        //                selectedList.Add(tempSelectedList[0].ToString());
        //                lstAvailable.Items.Add(lstSelected.Items[i]);
        //                lstSelected.Items.RemoveAt(i);
        //            }
        //        }
        //    }
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btnLast_click(object sender, EventArgs e)
        //{
        //    ArrayList selectedList = new ArrayList();
        //    for (int i = lstAvailable.Items.Count - 1; i >= 0; i--)
        //    {
        //        string listSelected;
        //        listSelected = lstAvailable.Items[i].ToString();
        //        ArrayList tempSelectedList = new ArrayList(listSelected.Split(new Char[] { '-' }));
        //        selectedList.Add(tempSelectedList[0].ToString());
        //        lstSelected.Items.Add(lstAvailable.Items[i]);
        //        lstAvailable.Items.RemoveAt(i);
        //    }
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btnFirst_click(object sender, EventArgs e)
        //{
        //    ArrayList selectedList = new ArrayList();
        //    for (int i = lstSelected.Items.Count - 1; i >= 0; i--)
        //    {
        //        string listSelected;
        //        listSelected = lstSelected.Items[0].ToString();
        //        ArrayList tempSelectedList = new ArrayList(listSelected.Split(new Char[] { '-' }));
        //        selectedList.Add(tempSelectedList[0].ToString());
        //        lstAvailable.Items.Add(lstSelected.Items[i]);
        //        lstSelected.Items.RemoveAt(i);
        //    }
        //}
        private void MoveOneItem(ListBox ListBoxFrom, ListBox ListBoxTo)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ListBoxFrom, ListBoxTo))
            {
                for (int index = ListBoxFrom.Items.Count - 1; index >= 0; index--)
                {
                    if (ListBoxFrom.Items[index].Selected == true)
                    {
                        ListBoxTo.Items.Add(ListBoxFrom.Items[index]);
                        ListBoxFrom.Items.Remove(ListBoxFrom.Items[index]);
                    }
                }
            }
        }
        private void MoveAllItem(ListBox ListBoxFrom, ListBox ListBoxTo)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ListBoxFrom, ListBoxTo))
            {
                for (int index = ListBoxFrom.Items.Count - 1; index >= 0; index--)
                {
                    ListBoxTo.Items.Add(ListBoxFrom.Items[index]);
                    ListBoxFrom.Items.Remove(ListBoxFrom.Items[index]);
                }
            }
        }
        protected void btnNext_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstAvailable, lstSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }
        protected void btnLast_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstAvailable, lstSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }
        protected void btnPrevious_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstSelected, lstAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }
        protected void btnFirst_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstSelected, lstAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }
        #endregion
        /// <summary>
        /// To filter Inactive records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #region "Search and Filter"
        protected void FilterByCheckboxAC_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSearchActiveOnly.Checked = false;
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSearchAC.Checked = false;
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Session["MaintainUser"] = (List<GetAllUserMasterResult>)objUserVal.EntityList.ToList();              
                List<GetAllUserMasterResult> lstMaintainUser = new List<GetAllUserMasterResult>();
                if (Session["MaintainUser"] != null)
                {
                    lstMaintainUser = (List<GetAllUserMasterResult>)Session["MaintainUser"];
                }
                if (lstMaintainUser.Count != 0)
                {
                    if (chkSearchAC.Checked == true)
                    {
                        lstMaintainUser = lstMaintainUser.Where(x => x.IsActive == false && x.LastUpdTS <= DateTime.Today.AddDays(-180)).ToList<GetAllUserMasterResult>();
                        foreach (var userDisable in lstMaintainUser.Where(x => x.IsUserLock == false || x.IsUserLock == null))
                        {
                            using (AdminServiceClient client = new AdminServiceClient())
                            {
                                client.LockOrUnlockUser(userDisable.UserName, true);
                            }
                        }
                    }
                    else
                    {
                        if (chkSearchActiveOnly.Checked == true) { lstMaintainUser = lstMaintainUser.Where(x => x.IsActive == true).ToList<GetAllUserMasterResult>(); }
                    }
                    dgMaintainUser.DataSource = lstMaintainUser;
                    if (IsDataBind)
                    {
                        dgMaintainUser.DataBind();
                        SelectFirstItem();
                    }

                }
                return false;
            }
        }
        private void SelectFirstItem()
        {
            if (dgMaintainUser.SelectedItems.Count <= 0)
            {
                dgMaintainUser.SelectedIndexes.Add(0);
            }
            if (dgMaintainUser.MasterTableView.Items.Count > 0)
            {
                Session["UserSelectedItem"] = dgMaintainUser.Items[0].GetDataKeyValue("UserName").ToString();
                Session["UserSelectedId"] = dgMaintainUser.Items[0].GetDataKeyValue("UserMasterID").ToString();
                ReadOnlyForm();
                tbUserName.Enabled = false;
                GridEnable(true, true, true);
                LoadSelectedList();
            }

        }
        #endregion
        //protected void ImportedFilter_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        //{
        //    DataRowView rowView = (DataRowView)e.Item.DataItem;
        //    if (rowView["IsActive"] is DBNull)
        //    {
        //        e.Item.Text = "No";
        //        e.Item.Value = "0";
        //    }
        //    else if ((bool)rowView["IsActive"])
        //    {
        //        e.Item.Text = "Yes";
        //        e.Item.Value = "1";
        //    }
        //}
        /// <summary>
        /// To validate Email Id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbEmail_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AdminService.AdminServiceClient client = new AdminServiceClient())
                        {
                            var user = client.GetUserByEMailId(tbEmail.Text).EntityList;
                            if (user != null && user.Where(x => x.UserName.Trim().ToUpper() != tbUserName.Text.Trim().ToUpper()).Count() > 0)
                            {
                                tbEmailInvalid.IsValid = false;
                                tbEmail.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }
        protected void tbEmail_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AdminService.AdminServiceClient client = new AdminServiceClient())
                        {
                            var user = client.GetUserByEMailId(tbEmail.Text).EntityList;
                            if (user != null && user.Where(x => x.UserName.Trim().ToUpper() != tbUserName.Text.Trim().ToUpper()).Count() > 0)
                            {
                                e.IsValid = false;
                                tbEmail.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (AdminService.AdminServiceClient AdmnService = new AdminService.AdminServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgMaintainUser.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    chkSearchAC.Checked = false;
                    PreSelectItem(AdmnService, true);
                }
            }
        }
        
        private void PreSelectItem(AdminService.AdminServiceClient AdmnService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var UserMasterValue = AdmnService.GetUserMasterList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, UserMasterValue);
            List<AdminService.GetAllUserMasterResult> filteredList = GetFilteredList(UserMasterValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.UserMasterID.Value)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgMaintainUser.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgMaintainUser.CurrentPageIndex = PageNumber;
            dgMaintainUser.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllUserMasterResult UserMasterValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["UserSelectedId"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = UserMasterValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().UserMasterID.Value;
                Session["UserSelectedId"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<AdminService.GetAllUserMasterResult> GetFilteredList(ReturnValueOfGetAllUserMasterResult UserMasterValue)
        {
            List<AdminService.GetAllUserMasterResult> filteredList = new List<AdminService.GetAllUserMasterResult>();

            if (UserMasterValue.ReturnFlag)
            {
                filteredList = UserMasterValue.EntityList;
            }

            if (filteredList.Count > 0)
            {
                if (chkSearchAC.Checked == true)
                {
                    filteredList = filteredList.Where(x => x.IsActive == false && x.LastUpdTS <= DateTime.Today.AddDays(-180)).ToList<GetAllUserMasterResult>();
                }
                else
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsActive == true).ToList<GetAllUserMasterResult>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (AdminService.AdminServiceClient AdmnService = new AdminService.AdminServiceClient())
            {
                if (_selectLastModified)
                {
                    dgMaintainUser.SelectedIndexes.Clear();
                    PreSelectItem(AdmnService, false);
                    dgMaintainUser.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        protected void dgMaintainUser_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgMaintainUser, Page.Session);
        }
    }
}
