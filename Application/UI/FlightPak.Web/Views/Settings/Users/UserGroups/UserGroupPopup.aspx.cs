﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Users.UserGroups
{
    public partial class UserGroupPopup : BaseSecuredPage
    {
        List<string> crewRoaster = new List<string>();
        private ExceptionManager exManager;
        private string VendorCD;
        public bool showCommandItems = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["UserGroupCD"] != null)
            {
                if (Request.QueryString["UserGroupCD"].ToString() == "1")
                {
                    dgMaintainGroupAccess.AllowMultiRowSelection = true;
                }
                else
                {
                    dgMaintainGroupAccess.AllowMultiRowSelection = false;
                }
            }
            else
            {
                dgMaintainGroupAccess.AllowMultiRowSelection = false;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        showCommandItems = true;
                        if (Request.QueryString["UserGroupCD"] != null)
                        {
                            //VendorCD = Request.QueryString["UserGroupCD"];

                            //if (!string.IsNullOrEmpty(VendorCD))
                            //{
                            //    dgMaintainGroupAccess.Rebind();

                            //    foreach (GridDataItem item in dgMaintainGroupAccess.MasterTableView.Items)
                            //    {
                            //        if (item["UserGroupCD"].Text.Trim().ToUpper() == VendorCD)
                            //        {
                            //            item.Selected = true;
                            //            break;
                            //        }
                            //    }
                            //}
                        }
                        else
                        {
                            //if (dgMaintainGroupAccess.MasterTableView.Items.Count > 0)
                            //{
                            //    dgMaintainGroupAccess.SelectedIndexes.Add(0);

                            //}
                        }

                        if (!string.IsNullOrEmpty(Request.QueryString["fromPage"]) && Request.QueryString["fromPage"] == "SystemTools")
                        {
                            showCommandItems = false;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

            

        }

        /// <summary>
        /// Bound Row styles, based on Checklist information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Bind Crew Roaster Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgMaintainGroupAccess_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                        {
                            if (objService.GetUserGroupList(base.UserPrincipal.Identity._customerID).ReturnFlag == true)
                            {
                                dgMaintainGroupAccess.DataSource = objService.GetUserGroupList(base.UserPrincipal.Identity._customerID).EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgMaintainGroupAccess;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    if (dgMaintainGroupAccess.Items.Count > 0)
            //    {
            //        dgMaintainGroupAccess.SelectedIndexes.Add(0);
            //        GridDataItem Item = (GridDataItem)dgMaintainGroupAccess.SelectedItems[0];
            //        Item.Selected = true;

            //    }
            //}

            if (!IsPostBack)
            {
                if (dgMaintainGroupAccess.Items.Count > 0)
                {
                    SelectItem();
                }
                else
                {
                    dgMaintainGroupAccess.SelectedIndexes.Add(0);
                }
            }
        }

        protected void SelectItem()
        {
            if (!string.IsNullOrEmpty(VendorCD))
            {

                foreach (GridDataItem item in dgMaintainGroupAccess.MasterTableView.Items)
                {
                    //if (item["UserGroupCD"].Text.Trim().ToUpper() == VendorCD)
                    //{
                    //    item.Selected = true;
                    //    break;
                    //}
                }
            }
            else
            {
                if (dgMaintainGroupAccess.MasterTableView.Items.Count > 0)
                {
                    dgMaintainGroupAccess.SelectedIndexes.Add(0);

                }
            }
        }
        
        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_ItemCreated(object sender, GridItemEventArgs e)
        {
            
        }

        protected void dgMaintainGroupAccess_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgMaintainGroupAccess_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgMaintainGroupAccess, Page.Session);
        }


    }
}