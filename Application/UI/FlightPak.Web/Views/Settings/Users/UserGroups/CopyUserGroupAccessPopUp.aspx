﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CopyUserGroupAccessPopUp.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Users.UserGroups.CopyUserGroupAccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Copy User Group Access</title>
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }
            var oArg = new Object();
//           

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();


                //                var MasterTable = grid.get_masterTableView();
                //                var selectedRows = MasterTable.get_selectedItems();
                //                var SelectedMain = false;
                //                for (var i = 0; i < selectedRows.length; i++) {
                //                    var row = selectedRows[i];
                var cell1 = document.getElementById("<%=txtUserGroup.ClientID%>").value;

                var cell2 = document.getElementById("<%=txtDescription.ClientID%>").value;
               
                if ((cell1.length > 0) && (cell2.length > 0)) {
                    oArg.UserGroup = cell1;                   
                    oArg.Desc = cell2;


                }
                else {
                    oArg.UserGroup = "";
                    oArg.Desc = "";

                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
              
            function Close() {
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <div>
        <table style="width: 100%">
            <tr>
                <td>
                    <b>Enter New User Group Information:</b>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <span class="mnd_text">User Group:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUserGroup" runat="server" CssClass="text40"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="mnd_text">Description:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="text100"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            <td colspan="2">
                            </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                         
                                                <td>
                                                    <asp:Button ID="btnSaveChanges" runat="server" Text="Save" CssClass="button" ValidationGroup="Save"  OnClick="btnSaveChanges_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
                                                </td>
                                            
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
              <asp:Label ID="InjectScript" runat="server"></asp:Label>
        </table>
    </div>
    </form>
</body>
</html>
