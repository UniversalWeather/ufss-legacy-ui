﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

using System.Data;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Users.UserGroups
{
    public partial class CopyUserGroupAccess : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserPrincipal.Identity._isSysAdmin)
            {
                Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
            }
        }

        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            List<string> lstCopyUser = new List<string>();
            lstCopyUser.Add(txtUserGroup.Text);
            lstCopyUser.Add(txtDescription.Text);
            Session["CopyUser"] = lstCopyUser;

            InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtUserGroup.Text = string.Empty;
            txtDescription.Text = string.Empty;
            InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
        }
    }
}