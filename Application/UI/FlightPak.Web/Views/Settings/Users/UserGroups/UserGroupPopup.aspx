﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserGroupPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Users.UserGroups.UserGroupPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Group</title>
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
    <link href="../../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgMaintainGroupAccess.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgMaintainGroupAccess.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "UserGroupCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "UserGroupDescription");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "UserGroupID");
                        if (i == 0) {
                            oArg.UserGroupCD = cell1.innerHTML + ",";
                            oArg.UserGroupDescription = cell2.innerHTML;
                            oArg.UserGroupID = cell3.innerHTML;
                        }
                        else {
                            oArg.UserGroupCD += cell1.innerHTML + ",";
                            oArg.UserGroupDescription += cell2.innerHTML + ",";
                            oArg.UserGroupID += cell3.innerHTML;
                        }
                    }
                }
                else {
                    oArg.UserGroupCD = "";
                    oArg.UserGroupDescription = "";
                    oArg.UserGroupID = "";
                }
                if (oArg.UserGroupCD != "")
                    oArg.UserGroupCD = oArg.UserGroupCD.substring(0, oArg.UserGroupCD.length - 1)
                else
                    oArg.UserGroupCD = "";

                oArg.Arg1 = oArg.UserGroupCD;
                oArg.CallingButton = "UserGroupCD"; // ( the name TypeCode should be same as popup tag)

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgMaintainGroupAccess.ClientID %>").get_masterTableView();
                masterTable.rebind();
                masterTable.clearSelectedItems();
                masterTable.selectItem(masterTable.get_dataItems()[0].get_element());
            }
            function GetGridId() {
                return $find("<%= dgMaintainGroupAccess.ClientID %>");
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <asp:ScriptManager ID="scr1" runat="server">
    </asp:ScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlSearchPanel" runat="server" Visible="true">
            <div class="divGridPanel">
                <table class="box1">
                    <tr>
                        <td>
                            <telerik:RadGrid ID="dgMaintainGroupAccess" runat="server" AllowSorting="true"
                                OnItemCreated="dgMaintainGroupAccess_ItemCreated"
                                Visible="true" OnNeedDataSource="dgMaintainGroupAccess_BindData" AutoGenerateColumns="false"
                                Height="341px" PageSize="10" AllowPaging="true" AllowFilteringByColumn="true"
                                PagerStyle-AlwaysVisible="true" OnItemCommand="dgMaintainGroupAccess_ItemCommand" OnPreRender="dgMaintainGroupAccess_PreRender" >
                                <MasterTableView DataKeyNames="CustomerID,UserGroupCD,UserGroupDescription,LastUpdUID,LastUpdTS,UserGroupID"
                                    CommandItemDisplay="Bottom">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="UserGroupCD" HeaderText="Code" AutoPostBackOnFilter="false"
                                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px"
                                            FilterControlWidth="100px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="UserGroupDescription" HeaderText="Description"
                                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                            HeaderStyle-Width="640px" FilterControlWidth="620px" FilterDelay="500">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="UserGroupID" HeaderText="UserGroupID" CurrentFilterFunction="EqualTo"
                                            FilterDelay="500" ShowFilterIcon="false" Display="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                        <div class="grd_ok">
                                            <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                                Ok</button>
                                        </div>
                                        <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                            >
                                            Use CTRL key to multi select</div>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings>
                                    <ClientEvents OnRowDblClick="returnToParent" />
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                                <GroupingSettings CaseSensitive="false"></GroupingSettings>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnCrewGroupID" runat="server" />
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
