﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.AdminService;
using System.Data;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Users.UserGroups
{
    public partial class MaintainGroupsAndAccess : BaseSecuredPage
    {
        private List<string> listUserGroups = new List<string>();
        System.Globalization.CultureInfo enGB = new System.Globalization.CultureInfo("en-GB");
        private bool rebindFlag = false;
        //string itemIndex = "0";
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgGroupAndAccess.AllowPaging = false;

                        if (!UserPrincipal.Identity._isSysAdmin)
                        {
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        }
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgMaintainGroupAccess, dgGroupAndAccess, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgMaintainGroupAccess.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            Session["UserGroupCurrentItemIndex"] = "0";
                            DefaultSelectGrid(true);
                            //Ramesh: To fix defect# 7043, commented code below to enable grid collapse for open group                            
                            //this.CollapseAllButOne();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        private void DefaultSelectGrid(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (BindDataSwitch)
                {
                    dgMaintainGroupAccess.Rebind();
                }
                
                if (dgMaintainGroupAccess.MasterTableView.Items.Count > 0)
                {
                    //if (!IsPostBack)
                    //{
                    //    Session["UserGroupSelectedItem"] = null;
                    //}
                    if (Session["UserGroupSelectedItem"] == null)
                    {
                        dgMaintainGroupAccess.SelectedIndexes.Add(0);
                        Session["UserGroupSelectedItem"] = dgMaintainGroupAccess.Items[0].GetDataKeyValue("UserGroupID").ToString();
                        Session["UserGroupAccessCode"] = dgMaintainGroupAccess.Items[0].GetDataKeyValue("UserGroupCD").ToString();
                    }

                    if (dgMaintainGroupAccess.SelectedIndexes.Count == 0)
                        dgMaintainGroupAccess.SelectedIndexes.Add(0);

                    ReadOnlyForm();
                    GridEnable(true, true, true);
                }
                else
                {
                    GridEnable(true, false, false);
                    EnableForm(false);
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lnkDelete = (LinkButton)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                        if (e.Initiator.ID.IndexOf("btnSaveChanges") > -1)
                        {
                            e.Updated = dgMaintainGroupAccess;
                        }
                        if (e.Initiator.ID.IndexOf("lnkDelete", StringComparison.Ordinal) > -1)
                        {

                            e.Updated = dgMaintainGroupAccess;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }


        }


        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_ItemCreated(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }


        protected void dgMaintainGroupAccess_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgGroupAndAccess.ClientSettings.Scrolling.ScrollTop = "0";
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                        {
                            if (objService.GetUserGroupList(base.UserPrincipal.Identity._customerID).ReturnFlag == true)
                            {
                                dgMaintainGroupAccess.DataSource = objService.GetUserGroupList(base.UserPrincipal.Identity._customerID).EntityList;
                            }

                            Session.Remove("UserGroups");
                            listUserGroups = new List<string>();
                            foreach (AdminService.UserGroup objEntity in objService.GetUserGroupList(base.UserPrincipal.Identity._customerID).EntityList)
                            {
                                listUserGroups.Add(objEntity.UserGroupCD.Trim().ToLower());
                                Session["UserGroups"] = listUserGroups;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        protected void BindModuleGridValue()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                dgGroupAndAccess.AllowPaging = false;
                dgGroupAndAccess.Rebind();
            }
        }
        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgGroupAndAccess_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                        {
                            Int64 UserGroupID = 0;
                            if (Session["UserGroupSelectedItem"] != null)
                            {
                                // GridDataItem item = (GridDataItem)Session["UserGroupSelectedItem"];
                                UserGroupID = Convert.ToInt64(Session["UserGroupSelectedItem"].ToString().Trim());
                            }

                                //Changes for FSS-340
                                var resultSet = objService.GetUserGroupPermission((tbusergroup.Text.Length > 0) ? UserGroupID : 0, base.UserPrincipal.Identity._customerID);
                                if (resultSet.ReturnFlag == true)
                                {
                                    var resultSetList = resultSet.EntityList.Where(x => x.UMPermissionName.Trim() != "Flight Planning" && x.UMPermissionName.ToString().Trim() != "Weather" && x.ModuleName.Trim() != "Corporate Request" && x.ModuleName.Trim() != "Corporate Request Reports").ToList();                                    
                                    dgGroupAndAccess.DataSource = resultSetList;
                                    Session["UserGroupActualItems"] = resultSetList;
                                }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }


        /// <summary>
        /// Datagrid Item Command for Aircraft Type Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                rebindFlag = true;
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                DisplayEditForm();
                                GridEnable(false, true, false);
                                BindModuleGridValue();
                                EnableForm(true);
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.UserGroupPermission, Convert.ToInt64(Session["UserGroupSelectedItem"].ToString().Trim()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.UserGroupPermission);
                                        DefaultSelectGrid(true);
                                        return;
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgMaintainGroupAccess.SelectedIndexes.Clear();
                                GridEnable(true, false, false);
                                DisplayInsertForm();
                                dgMaintainGroupAccess.Rebind();
                                BindModuleGridValue();
                                RadAjaxManager1.FocusControl(tbusergroup.ClientID); //tbusergroup.Focus();
                                // Set Current Item to null when in add mode
                                Session.Remove("UserGroupSelectedItem");
                                Session.Remove("UserGroupAccessCode");
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        /// <summary>
        /// Update Command for Aircraft Duty Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_UpdateCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;


                        if (Session["UserGroupSelectedItem"] != null)
                        {
                            //GridDataItem Item = (GridDataItem)Session["UserGroupSelectedItem"];
                            using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                            {
                                AdminService.UserGroup objUserGroup = new AdminService.UserGroup();
                                objUserGroup.CustomerID = base.UserPrincipal.Identity._customerID;
                                objUserGroup.UserGroupID = Convert.ToInt64(Session["UserGroupSelectedItem"].ToString().Trim());
                                objUserGroup.UserGroupDescription = tbdescription.Text.Trim();
                                objUserGroup.UserGroupCD = tbusergroup.Text.Trim();
                                objUserGroup.IsDeleted = false;
                                List<AdminService.UserGroupPermission> umPermissionList = new List<AdminService.UserGroupPermission>();
                                int ival = 1;

                                //var updateTable = CreateTableMapping();

                                //Get original row count of rad grid
                                foreach (GridDataItem item in dgGroupAndAccess.Items)
                                {

                                    AdminService.UserGroupPermission objUserPermission = new UserGroupPermission();
                                    objUserPermission.LastUpdTS = DateTime.UtcNow;
                                    objUserPermission.IsDeleted = false;

                                    //objUserPermission.IsDeleted = false;
                                    //if (item.GetDataKeyValue("CustomerID") != null)
                                    //{
                                    //    if ((Int64)(item.GetDataKeyValue("CustomerID")) != 0)
                                    //    {
                                    //        objUserPermission.LastUpdTS = DateTime.UtcNow;
                                    //    }
                                    //}

                                    CheckBox chkview = ((CheckBox)item["CanView"].FindControl("chkview")); //(CheckBox)item.FindControl("chkview");
                                    objUserPermission.CanView = chkview.Checked;
                                    CheckBox chkadd = ((CheckBox)item["CanAdd"].FindControl("chkadd")); //(CheckBox)item.FindControl("chkadd");
                                    objUserPermission.CanAdd = chkadd.Checked;
                                    CheckBox chkedit = ((CheckBox)item["CanEdit"].FindControl("chkedit")); //(CheckBox)item.FindControl("chkedit");
                                    objUserPermission.CanEdit = chkedit.Checked;
                                    CheckBox chkDelete = ((CheckBox)item["CanDelete"].FindControl("chkDelete")); //(CheckBox)item.FindControl("chkDelete");
                                    objUserPermission.CanDelete = chkDelete.Checked;
                                    GridDataItem itemvalue = item;
                                    objUserPermission.CustomerID = base.UserPrincipal.Identity._customerID;
                                    if (item.GetDataKeyValue("UMPermissionID") != null)
                                    {
                                        //objUserPermission.UMPermissionID = (Int64)(item.GetDataKeyValue("UMPermissionID"));
                                        objUserPermission.UMPermissionID = (Int64)(item.GetDataKeyValue("UMPermissionID"));
                                    }
                                    if (Session["UserGroupID"] != null)
                                    {
                                        //objUserPermission.UserGroupID = (Int64)Session["UserGroupID"];
                                        objUserPermission.UserGroupID = (Int64)Session["UserGroupID"];
                                    }
                                    //objUserPermission.CustomerID = (int)item.GetDataKeyValue("CustomerID");
                                    objUserPermission.CustomerID = base.UserPrincipal.Identity._customerID;
                                    //objUserPermission.CustomerID = base.UserPrincipal.Identity._customerID;
                                    //ival is the Dummy value not used in Db change 
                                    //objUserPermission.UserGroupPermissionID = ival;
                                    if ((Int64)item.GetDataKeyValue("UserGroupPermissionID") == 0)
                                        objUserPermission.UserGroupPermissionID = ival - 10000;
                                    else
                                        objUserPermission.UserGroupPermissionID = (Int64)item.GetDataKeyValue("UserGroupPermissionID");

                                    //doubt
                                    // objUse = tbusergroup.Text;

                                    //objUserGroup.UserGroupPermission.Add(objUserPermission);
                                    umPermissionList.Add(objUserPermission);
                                    ival++;
                                }
                                var objResult = objService.UpdateUserGroup(objUserGroup, umPermissionList, dgGroupAndAccess.Items.Count);
                                if (objResult.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                    //Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.UserGroupPermission, Convert.ToInt64(Session["UserGroupSelectedItem"].ToString().Trim()));
                                    }
                                    //string alertMsg = "radalert('All user's right attached to updated group have been adjusted.', 360, 50, 'Maintain User');";
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);

                                    // Update Session when there is a change in UserPrincipal
                                    RefreshSession();
                                    DefaultSelectGrid(true);
                                    ShowAlert(System.Web.HttpUtility.HtmlEncode("Rights of all users attached to this group have been updated."), "UserGroup");
                                    refreshUserPrincipal();
                                    return;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objResult.ErrorMessage, ModuleNameConstants.Database.UserGroupPermission);
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    e.Canceled = true;
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }

            }


        }

        private DataTable CreateTableMapping()
        {
            DataTable dtUpdate = new DataTable("dtUpdate");
            DataColumn dcPermissionId = new DataColumn("UserGroupPermissionID");
            dtUpdate.Columns.Add(dcPermissionId);
            DataColumn dcCustomerId = new DataColumn("CustomerID");
            dtUpdate.Columns.Add(dcCustomerId);
            DataColumn dcUserGroupId = new DataColumn("UserGroupID");
            dtUpdate.Columns.Add(dcUserGroupId);
            DataColumn dcUmPermissionId = new DataColumn("UMPermissionID");
            dtUpdate.Columns.Add(dcUmPermissionId);
            DataColumn dcView = new DataColumn("CanView");
            dtUpdate.Columns.Add(dcView);
            DataColumn dcAdd = new DataColumn("CanAdd");
            dtUpdate.Columns.Add(dcAdd);
            DataColumn dcEdit = new DataColumn("CanEdit");
            dtUpdate.Columns.Add(dcEdit);
            DataColumn dcDelete = new DataColumn("CanDelete");
            dtUpdate.Columns.Add(dcDelete);
            DataColumn dcLastUpdatedUID = new DataColumn("LastUpdUID");
            dtUpdate.Columns.Add(dcLastUpdatedUID);
            DataColumn dcLastUpdatedTS = new DataColumn("LastUpdTS");
            dtUpdate.Columns.Add(dcLastUpdatedTS);
            DataColumn dcIsDeleted = new DataColumn("IsDeleted");
            dtUpdate.Columns.Add(dcIsDeleted);
            DataColumn dcIsInActive = new DataColumn("IsInActive");
            dtUpdate.Columns.Add(dcIsInActive);
            return dtUpdate;
        }

        /// <summary>
        /// Insert Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAlreadyExist())
                        {
                            InsertGroup();
                        }
                        else
                        {
                            e.Item.OwnerTableView.IsItemInserted = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
                finally
                {
                    e.Canceled = true;
                    e.Item.OwnerTableView.IsItemInserted = false;
                }
            }
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_DeleteCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["UserGroupSelectedItem"] != null)
                        {

                            using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                            {
                                AdminService.UserGroup objUserGroup = new AdminService.UserGroup();
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.UserGroupMapping, Convert.ToInt64(Session["UserGroupSelectedItem"].ToString().Trim()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.UserGroupMapping);
                                        return;
                                    }
                                    // GridDataItem item = (GridDataItem)Session["UserGroupSelectedItem"];
                                    string Code = Session["UserGroupSelectedItem"].ToString();
                                    foreach (GridDataItem Item in dgMaintainGroupAccess.MasterTableView.Items)
                                    {
                                        if (Item["UserGroupID"].Text.Trim() == Code)
                                        {
                                            objUserGroup.CustomerID = (Int64)Item.GetDataKeyValue("CustomerID");
                                            objUserGroup.UserGroupDescription = Item.GetDataKeyValue("UserGroupDescription").ToString();
                                            objUserGroup.UserGroupCD = Item.GetDataKeyValue("UserGroupCD").ToString();
                                            objUserGroup.UserGroupID = (Int64)Item.GetDataKeyValue("UserGroupID");
                                            objUserGroup.LastUpdTS = System.DateTime.UtcNow;
                                            objUserGroup.LastUpdUID = "UC";
                                            objUserGroup.IsDeleted = true;
                                            objService.DeleteUserGroup(objUserGroup);

                                            DefaultSelectGrid(true);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.UserGroupPermission, Convert.ToInt64(Session["UserGroupSelectedItem"].ToString().Trim()));
                    }
                }
            }


        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                // GridDataItem item = (GridDataItem)dgMaintainGroupAccess.SelectedItems[0];
                                GridDataItem Item = dgMaintainGroupAccess.SelectedItems[0] as GridDataItem;
                                Session["UserGroupSelectedItem"] = Item["UserGroupID"].Text;
                                Session["UserGroupAccessCode"] = Item["UserGroupCD"].Text;
                                dgMaintainGroupAccess.Rebind();
                                BindModuleGridValue();
                                Item.Selected = true;
                                Label lbLastUpdatedUser;

                                lbLastUpdatedUser = (Label)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUserID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUserID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUptTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUptTS").ToString().Trim())));
                                }
                                ReadOnlyForm();
                                GridEnable(true, true, true);

                                Item.Selected = true;
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                    }
                }
            }
        }

        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSave.Value = "Save";
                ClearForm();
                EnableForm(true);
                BindModuleGridValue();
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Aircraft Duty Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }


        }


        /// <summary>
        /// Cancel Aircraft Duty Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //pnlExternalForm.Visible = false;
                        if (Session["UserGroupSelectedItem"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.UserGroupPermission, Convert.ToInt64(Session["UserGroupSelectedItem"].ToString().Trim()));
                            }
                        }
                        //Session.Remove("UserGroupSelectedItem");
                        //Session.Remove("UserGroupAccessCode");
                        //dgMaintainGroupAccess.Rebind();
                        GridEnable(true, true, true);
                        BindModuleGridValue();
                        ClearForm();
                        //DefaultSelectGrid(true);
                        DefaultSelectGrid(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        protected void dgGroupAndAccess_OnItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridHeaderItem)
                        {
                            //Set visibility
                            CheckBox chkview = (CheckBox)e.Item.FindControl("chkViewAll");
                            RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkview, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            CheckBox chkadd = (CheckBox)e.Item.FindControl("chkAddAll");
                            RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkadd, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            CheckBox chkedit = (CheckBox)e.Item.FindControl("chkEditAll");
                            RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkedit, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            CheckBox chkDelete = (CheckBox)e.Item.FindControl("chkDeleteAll");
                            RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkDelete, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            if (Session["UserGroupActualItems"] != null)
                            {
                                var checkModule = (List<FlightPak.Web.AdminService.GetUserGroupPermission>)Session["UserGroupActualItems"];
                                chkview.Checked = checkModule.Where(x => x.CanView == false).Count() == 0;
                                chkadd.Checked = checkModule.Where(x => x.CanAdd == false).Count() == 0;
                                chkedit.Checked = checkModule.Where(x => x.CanEdit == false).Count() == 0;
                                chkDelete.Checked = checkModule.Where(x => x.CanDelete == false).Count() == 0;
                            }

                        }
                        else if ((e.Item is GridGroupHeaderItem))
                        { //Set visibility

                            CheckBox chkview = (CheckBox)e.Item.FindControl("chkViewModule");
                            chkview.Checked = true;
                            RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkview, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            CheckBox chkadd = (CheckBox)e.Item.FindControl("chkAddModule");
                            RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkadd, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            CheckBox chkedit = (CheckBox)e.Item.FindControl("chkEditModule");
                            RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkedit, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            CheckBox chkDelete = (CheckBox)e.Item.FindControl("chkDeleteModule");
                            RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkDelete, dgGroupAndAccess, RadAjaxLoadingPanel1);


                        }
                        else if (e.Item is GridDataItem)
                        {
                            //Set visibility
                            CheckBox chkview = (CheckBox)e.Item.FindControl("chkview");
                            //RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkview, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            CheckBox chkadd = (CheckBox)e.Item.FindControl("chkadd");
                            //RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkadd, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            CheckBox chkedit = (CheckBox)e.Item.FindControl("chkedit");
                            //RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkedit, dgGroupAndAccess, RadAjaxLoadingPanel1);
                            CheckBox chkDelete = (CheckBox)e.Item.FindControl("chkDelete");
                            //RadAjaxManager1.AjaxSettings.AddAjaxSetting(chkDelete, dgGroupAndAccess, RadAjaxLoadingPanel1);

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }
        }
        protected void dgGroupAndAccess_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridHeaderItem)
                        {
                            //Set visibility
                            CheckBox chkview = (CheckBox)e.Item.FindControl("chkViewAll");
                            chkview.Enabled = tbdescription.Enabled;
                            CheckBox chkadd = (CheckBox)e.Item.FindControl("chkAddAll");
                            chkadd.Enabled = tbdescription.Enabled;
                            CheckBox chkedit = (CheckBox)e.Item.FindControl("chkEditAll");
                            chkedit.Enabled = tbdescription.Enabled;
                            CheckBox chkDelete = (CheckBox)e.Item.FindControl("chkDeleteAll");
                            chkDelete.Enabled = tbdescription.Enabled;
                        }
                        else if ((e.Item is GridGroupHeaderItem))
                        { //Set visibility
                            CheckBox chkview = (CheckBox)e.Item.FindControl("chkViewModule");
                            chkview.Enabled = tbdescription.Enabled;
                            CheckBox chkadd = (CheckBox)e.Item.FindControl("chkAddModule");
                            chkadd.Enabled = tbdescription.Enabled;
                            CheckBox chkedit = (CheckBox)e.Item.FindControl("chkEditModule");
                            chkedit.Enabled = tbdescription.Enabled;
                            CheckBox chkDelete = (CheckBox)e.Item.FindControl("chkDeleteModule");
                            chkDelete.Enabled = tbdescription.Enabled;
                            Label lblModuleName = (Label)e.Item.FindControl("lblModuleName");
                            if (Session["UserGroupActualItems"] != null)
                            {
                                var checkModule = (List<FlightPak.Web.AdminService.GetUserGroupPermission>)Session["UserGroupActualItems"];
                                chkview.Checked = checkModule.Where(x => x.CanView == false && x.ModuleName.Trim().ToUpper() == lblModuleName.Text.Trim().ToUpper()).Count() == 0;
                                chkadd.Checked = checkModule.Where(x => x.CanAdd == false && x.ModuleName.Trim().ToUpper() == lblModuleName.Text.Trim().ToUpper()).Count() == 0;
                                chkedit.Checked = checkModule.Where(x => x.CanEdit == false && x.ModuleName.Trim().ToUpper() == lblModuleName.Text.Trim().ToUpper()).Count() == 0;
                                chkDelete.Checked = checkModule.Where(x => x.CanDelete == false && x.ModuleName.Trim().ToUpper() == lblModuleName.Text.Trim().ToUpper()).Count() == 0;
                            }
                        }
                        else if (e.Item is GridDataItem)
                        {
                            GridDataItem item = e.Item as GridDataItem;
                            //Set visibility
                            CheckBox chkview = (CheckBox)e.Item.FindControl("chkview");
                            chkview.Enabled = tbdescription.Enabled;
                            CheckBox chkadd = (CheckBox)e.Item.FindControl("chkadd");
                            chkadd.Enabled = tbdescription.Enabled;
                            CheckBox chkedit = (CheckBox)e.Item.FindControl("chkedit");
                            chkedit.Enabled = tbdescription.Enabled;
                            CheckBox chkDelete = (CheckBox)e.Item.FindControl("chkDelete");
                            chkDelete.Enabled = tbdescription.Enabled;
                            //Preserve ToggleState of the check box

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }
        /// <summary>
        /// Binding the colors for grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_ItemDataBound(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="add"></param>
        /// <param name="edit"></param>
        /// <param name="delete"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, delCtl, editCtl;
                insertCtl = (LinkButton)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                delCtl = (LinkButton)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                editCtl = (LinkButton)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                if (add)
                {
                    insertCtl.Enabled = true;
                }
                else
                {
                    insertCtl.Enabled = false;
                }
                if (delete)
                {
                    delCtl.Enabled = true;
                    delCtl.OnClientClick = "javascript:return ProcessDelete();";
                }
                else
                {
                    delCtl.Enabled = false;
                    delCtl.OnClientClick = string.Empty;
                }


                if (edit)
                {
                    editCtl.Enabled = true;
                    editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                }
                else
                {
                    editCtl.Enabled = false;
                    editCtl.OnClientClick = string.Empty;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                PopulateData();
                //hdnSave.Value = "Update";
                //dgMaintainGroupAccess.Rebind();
                BindModuleGridValue();
                EnableForm(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void PopulateData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //  GridDataItem item = (GridDataItem)Session["UserGroupSelectedItem"];
                if (Session["UserGroupSelectedItem"] != null)
                {
                    string Code = Session["UserGroupSelectedItem"].ToString();
                    foreach (GridDataItem Item in dgMaintainGroupAccess.MasterTableView.Items)
                    {
                        if (Item["UserGroupID"].Text.Trim() == Code)
                        {
                            string LastUpdUID = string.Empty, LastUpdTS = string.Empty;
                            if (Item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                LastUpdUID = Item.GetDataKeyValue("LastUpdUID").ToString().Trim().Replace("&nbsp;", "");
                            }
                            else
                            {
                                LastUpdUID = string.Empty;
                            }
                            if (Item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                LastUpdTS = Item.GetDataKeyValue("LastUpdTS").ToString().Trim().Replace("&nbsp;", "");
                            }
                            else
                            {
                                LastUpdTS = string.Empty;
                            }
                            tbusergroup.Text = Item.GetDataKeyValue("UserGroupCD").ToString();
                            if (Item.GetDataKeyValue("UserGroupDescription") != null)
                            {
                                tbdescription.Text = Item.GetDataKeyValue("UserGroupDescription").ToString();
                            }
                            if (Item.GetDataKeyValue("UserGroupID") != null)
                            {
                                Session["UserGroupID"] = Item.GetDataKeyValue("UserGroupID");
                            }
                            Label lbUser = (Label)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            //Label lbUser = (Label)Item.FindControl("lbLastUpdatedUser");

                            if (!string.IsNullOrEmpty(LastUpdUID))
                            {
                                lbUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + LastUpdUID);
                            }
                            else
                            {
                                lbUser.Text = string.Empty;
                            }

                            if (!string.IsNullOrEmpty(LastUpdTS))
                            {
                                lbUser.Text = System.Web.HttpUtility.HtmlEncode(lbUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(LastUpdTS)));
                            }
                            lbColumnName1.Text = "User Group";
                            lbColumnName2.Text = "Description";
                            lbColumnValue1.Text = Item["UserGroupCD"].Text;
                            lbColumnValue2.Text = Item["UserGroupDescription"].Text;
                            break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                GridHeaderItem headerItem = dgGroupAndAccess.MasterTableView.GetItems(GridItemType.Header) != null && dgGroupAndAccess.MasterTableView.GetItems(GridItemType.Header).Count() > 0 ? (GridHeaderItem)dgGroupAndAccess.MasterTableView.GetItems(GridItemType.Header)[0] : null;



                GridItem[] groupHeaderItem = (GridItem[])dgGroupAndAccess.MasterTableView.GetItems(GridItemType.GroupHeader);

                //dgGroupAndAccess.Enabled = enable;
                if (headerItem != null && headerItem.Cells != null)
                {
                    foreach (TableCell item in headerItem.Cells)
                    {
                        CheckBox chkview = (CheckBox)item.FindControl("chkViewAll");
                        chkview.Enabled = enable;
                        CheckBox chkadd = (CheckBox)item.FindControl("chkAddAll");
                        chkadd.Enabled = enable;
                        CheckBox chkedit = (CheckBox)item.FindControl("chkEditAll");
                        chkedit.Enabled = enable;
                        CheckBox chkDelete = (CheckBox)item.FindControl("chkDeleteAll");
                        chkDelete.Enabled = enable;
                    }
                }
                foreach (GridGroupHeaderItem item in groupHeaderItem)
                {
                    CheckBox chkview = (CheckBox)item.FindControl("chkViewModule");
                    chkview.Enabled = enable;
                    CheckBox chkadd = (CheckBox)item.FindControl("chkAddModule");
                    chkadd.Enabled = enable;
                    CheckBox chkedit = (CheckBox)item.FindControl("chkEditModule");
                    chkedit.Enabled = enable;
                    CheckBox chkDelete = (CheckBox)item.FindControl("chkDeleteModule");
                    chkDelete.Enabled = enable;

                }
                foreach (GridDataItem item in dgGroupAndAccess.Items)
                {
                    CheckBox chkview = (CheckBox)item.FindControl("chkview");
                    chkview.Enabled = enable;
                    CheckBox chkadd = (CheckBox)item.FindControl("chkadd");
                    chkadd.Enabled = enable;
                    CheckBox chkedit = (CheckBox)item.FindControl("chkedit");
                    chkedit.Enabled = enable;
                    CheckBox chkDelete = (CheckBox)item.FindControl("chkDelete");
                    chkDelete.Enabled = enable;
                }
                tbdescription.Enabled = enable;
                tbusergroup.Enabled = enable;
                btnSaveChanges.Visible = enable;
                btnCancel.Visible = enable;
                btnCancelTop.Visible = enable;
                btnSaveTop.Visible = enable;
                if (hdnSave.Value == "Save" && enable == true)
                {
                    tbusergroup.Enabled = true;
                }
                else
                {
                    tbusergroup.Enabled = false;
                }


            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                PopulateData();
            }
        }
        /// <summary>
        /// To Clear the Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbusergroup.Text = string.Empty;
                tbdescription.Text = string.Empty;
                BindModuleGridValue();
            }
        }
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                // Copy Info
                                if (Session["CopyUser"] != null)
                                {
                                    BindModuleGridValue();
                                    List<string> lstCopyUser = new List<string>();
                                    lstCopyUser = (List<string>)Session["CopyUser"];
                                    hdnCode.Value = lstCopyUser[0];
                                    hdnDescription.Value = lstCopyUser[1];
                                    using (AdminService.AdminServiceClient Service = new AdminService.AdminServiceClient())
                                    {
                                        var ReturnValue = Service.GetUserGroupByGroupCode(hdnCode.Value).EntityInfo;
                                        if (ReturnValue != null && ReturnValue.UserGroupCD.Length > 0)
                                        {

                                            ShowAlert("The User Group entered already exists. Please enter a unique value", ModuleNameConstants.Database.UserGroup);
                                            return;
                                        }
                                        InsertGroup();
                                        Session.Remove("CopyUser");
                                    }
                                }
                                break;
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroup);
                }
            }

        }

        private void InsertGroup()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                btnCancel.Visible = true;


                using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                {
                    AdminService.UserGroup objUserGroup = new AdminService.UserGroup();
                    objUserGroup.CustomerID = base.UserPrincipal.Identity._customerID;
                    if ((hdnDescription.Value != null) && (hdnDescription.Value != string.Empty))
                    {
                        objUserGroup.UserGroupDescription = hdnDescription.Value;
                    }
                    else
                    {
                        objUserGroup.UserGroupDescription = tbdescription.Text;
                    }
                    if ((hdnCode.Value != null) && (hdnCode.Value != string.Empty))
                    {
                        objUserGroup.UserGroupCD = hdnCode.Value;
                    }
                    else
                    {
                        objUserGroup.UserGroupCD = tbusergroup.Text;
                    }

                    objUserGroup.IsDeleted = false;
                    objUserGroup.UserGroupID = 0;
                    // bulk Update  by single stuff


                    objUserGroup.UserGroupPermission = new List<AdminService.UserGroupPermission>();
                    int i = 0;
                    foreach (GridDataItem item in dgGroupAndAccess.Items)
                    {
                        i = --i;
                        AdminService.UserGroupPermission objUserPermission = new UserGroupPermission();
                        objUserPermission.IsDeleted = false;
                        objUserPermission.LastUpdTS = DateTime.UtcNow;
                        CheckBox chkview = (CheckBox)item.FindControl("chkview");
                        objUserPermission.CanView = chkview.Checked;
                        CheckBox chkadd = (CheckBox)item.FindControl("chkadd");
                        objUserPermission.CanAdd = chkadd.Checked;
                        CheckBox chkedit = (CheckBox)item.FindControl("chkedit");
                        objUserPermission.CanEdit = chkedit.Checked;
                        CheckBox chkDelete = (CheckBox)item.FindControl("chkDelete");
                        objUserPermission.CanDelete = chkDelete.Checked;
                        GridDataItem itemvalue = item;
                        objUserPermission.CustomerID = base.UserPrincipal.Identity._customerID;
                        //doubt objUserPermission.UMPermissionName= tbusergroup.Text;
                        if (item.GetDataKeyValue("UMPermissionID") != null)
                        {
                            objUserPermission.UMPermissionID = (Int64)(item.GetDataKeyValue("UMPermissionID"));
                        }
                        objUserPermission.UserGroupID = 0;
                        objUserPermission.UserGroupPermissionID = i;
                        objUserGroup.UserGroupPermission.Add(objUserPermission);
                    }
                    var objResult = objService.AddUserGroup(objUserGroup);
                    if (objResult.ReturnFlag == true)
                    {
                        ShowSuccessMessage();
                        dgGroupAndAccess.Rebind();
                        DefaultSelectGrid(true);
                        hdnCode.Value = string.Empty;
                        hdnDescription.Value = string.Empty;
                        _selectLastModified = true;
                        refreshUserPrincipal();
                    }
                    else
                    {
                        //For Data Anotation
                        ProcessErrorMessage(objResult.ErrorMessage, ModuleNameConstants.Database.UserGroup);
                    }

                }

            }

        }

        private bool CheckAlreadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnFlag = false;


                using (AdminService.AdminServiceClient Service = new AdminService.AdminServiceClient())
                {
                    var ReturnValue = Service.GetUserGroupByGroupCode(tbusergroup.Text).EntityInfo;

                    if (ReturnValue != null && ReturnValue.UserGroupCD.Length > 0 && ReturnValue.UserGroupCD.ToUpper().Trim().Equals(tbusergroup.Text.Trim().ToUpper()))
                    {
                        cvCode.IsValid = false;
                        RadAjaxManager1.FocusControl(tbusergroup.ClientID); //tbusergroup.Focus();
                        return ReturnFlag;
                    }
                    else
                    {
                        RadAjaxManager1.FocusControl(tbdescription.ClientID); //tbdescription.Focus();
                    }
                }
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbusergroup.Text != null)
                        {
                            CheckAlreadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroup);
                }
            }
        }


        protected void chkViewAll_CheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridHeaderItem headerItem = (GridHeaderItem)dgGroupAndAccess.MasterTableView.GetItems(GridItemType.Header)[0];
                        GridItem[] groupHeaderItem = (GridItem[])dgGroupAndAccess.MasterTableView.GetItems(GridItemType.GroupHeader);
                        CheckBox chkview = null;
                        //dgGroupAndAccess.Enabled = enable;
                        foreach (TableCell item in headerItem.Cells)
                        {
                            chkview = (CheckBox)item.FindControl("chkViewAll");
                        }
                        foreach (GridDataItem item in dgGroupAndAccess.MasterTableView.Items)
                        {
                            CheckBox chkbx = (CheckBox)item["CanView"].FindControl("chkview");
                            chkbx.Checked = chkview.Checked;
                        }
                        foreach (GridGroupHeaderItem item in groupHeaderItem)
                        {
                            CheckBox chkViewModule = (CheckBox)item.FindControl("chkViewModule");
                            chkViewModule.Checked = chkview.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        protected void chkAddAll_CheckedChanged(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridHeaderItem headerItem = (GridHeaderItem)dgGroupAndAccess.MasterTableView.GetItems(GridItemType.Header)[0];
                        GridItem[] groupHeaderItem = (GridItem[])dgGroupAndAccess.MasterTableView.GetItems(GridItemType.GroupHeader);
                        CheckBox chkadd = null;
                        //dgGroupAndAccess.Enabled = enable;
                        foreach (TableCell item in headerItem.Cells)
                        {
                            chkadd = (CheckBox)item.FindControl("chkAddAll");
                        }
                        foreach (GridDataItem item in dgGroupAndAccess.MasterTableView.Items)
                        {
                            CheckBox chkbx = (CheckBox)item["CanAdd"].FindControl("chkadd");
                            chkbx.Checked = chkadd.Checked;
                        }
                        foreach (GridGroupHeaderItem item in groupHeaderItem)
                        {
                            CheckBox chkAddModule = (CheckBox)item.FindControl("chkAddModule");
                            chkAddModule.Checked = chkadd.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        protected void chkEditAll_CheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridHeaderItem headerItem = (GridHeaderItem)dgGroupAndAccess.MasterTableView.GetItems(GridItemType.Header)[0];
                        GridItem[] groupHeaderItem = (GridItem[])dgGroupAndAccess.MasterTableView.GetItems(GridItemType.GroupHeader);
                        CheckBox chkedit = null;
                        //dgGroupAndAccess.Enabled = enable;
                        foreach (TableCell item in headerItem.Cells)
                        {
                            chkedit = (CheckBox)item.FindControl("chkEditAll");
                        }
                        foreach (GridDataItem item in dgGroupAndAccess.MasterTableView.Items)
                        {
                            CheckBox chkbx = (CheckBox)item["CanEdit"].FindControl("chkedit");
                            chkbx.Checked = chkedit.Checked;
                        }
                        foreach (GridGroupHeaderItem item in groupHeaderItem)
                        {
                            CheckBox chkEditModule = (CheckBox)item.FindControl("chkEditModule");
                            chkEditModule.Checked = chkedit.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        protected void chkDeleteAll_CheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridHeaderItem headerItem = (GridHeaderItem)dgGroupAndAccess.MasterTableView.GetItems(GridItemType.Header)[0];
                        GridItem[] groupHeaderItem = (GridItem[])dgGroupAndAccess.MasterTableView.GetItems(GridItemType.GroupHeader);
                        CheckBox chkdelete = null;
                        //dgGroupAndAccess.Enabled = enable;
                        foreach (TableCell item in headerItem.Cells)
                        {
                            chkdelete = (CheckBox)item.FindControl("chkDeleteAll");
                        }
                        foreach (GridDataItem item in dgGroupAndAccess.MasterTableView.Items)
                        {
                            CheckBox chkbx = (CheckBox)item["CanDelete"].FindControl("chkDelete");
                            chkbx.Checked = chkdelete.Checked;
                        }
                        foreach (GridGroupHeaderItem item in groupHeaderItem)
                        {
                            CheckBox chkDeleteModule = (CheckBox)item.FindControl("chkDeleteModule");
                            chkDeleteModule.Checked = chkdelete.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        //Ramesh: Removed property to call this method, to fix defect# 7043
        //protected void dgGroupAndAccess_PreRender(object sender, EventArgs e)
        //{

        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                this.CollapseAllButOne();
        //            }, FlightPak.Common.Constants.Policy.UILayer);

        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
        //        }
        //    }

        //}
        protected void dgGroupAndAccess_OnItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.CommandName == RadGrid.ExpandCollapseCommandName)
                        {
                            if (e.Item is GridGroupHeaderItem)
                            {
                                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                                Session["UserGroupCurrentItemIndex"] = item.GroupIndex;
                            }
                        }
                        //add more commands if necessary
                        if (e.CommandName == RadGrid.SortCommandName | e.CommandName == RadGrid.PageCommandName | e.CommandName == RadGrid.ExpandCollapseCommandName)
                        {
                            e.ExecuteCommand(e.Item);
                            e.Canceled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }




        }

        //Ramesh: All instance to call this method commented to fix Defect# 7403
        //private void CollapseAllButOne()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        if (dgGroupAndAccess.MasterTableView.Controls != null && dgGroupAndAccess.MasterTableView.Controls.Count > 0)
        //        {
        //            foreach (GridItem gridItem in dgGroupAndAccess.MasterTableView.Controls[0].Controls)
        //            {
        //                if ((gridItem) is GridGroupHeaderItem)
        //                {
        //                    GridGroupHeaderItem groupHeaderItem = (GridGroupHeaderItem)gridItem;
        //                    if (groupHeaderItem.GroupIndex == Session["UserGroupCurrentItemIndex"].ToString())
        //                    {
        //                        groupHeaderItem.Expanded = true;
        //                    }
        //                    else
        //                    {
        //                        groupHeaderItem.Expanded = false;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        protected void chkViewModule_OnCheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckBox check = (CheckBox)sender;
                        GridGroupHeaderItem groupHeader = (GridGroupHeaderItem)check.NamingContainer;
                        GridItem[] children = groupHeader.GetChildItems();

                        foreach (GridItem child in children)
                        {
                            GridDataItem newChild = (GridDataItem)child;
                            CheckBox chkbx = (CheckBox)newChild["CanView"].FindControl("chkview");
                            chkbx.Checked = check.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        protected void chkAddModule_OnCheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckBox check = (CheckBox)sender;
                        GridGroupHeaderItem groupHeader = (GridGroupHeaderItem)check.NamingContainer;
                        GridItem[] children = groupHeader.GetChildItems();

                        foreach (GridItem child in children)
                        {
                            GridDataItem newChild = (GridDataItem)child;
                            CheckBox chkbx = (CheckBox)newChild["CanAdd"].FindControl("chkadd");
                            chkbx.Checked = check.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        protected void chkEditModule_OnCheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckBox check = (CheckBox)sender;
                        GridGroupHeaderItem groupHeader = (GridGroupHeaderItem)check.NamingContainer;
                        GridItem[] children = groupHeader.GetChildItems();

                        foreach (GridItem child in children)
                        {
                            GridDataItem newChild = (GridDataItem)child;
                            CheckBox chkbx = (CheckBox)newChild["CanEdit"].FindControl("chkedit");
                            chkbx.Checked = check.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }

        protected void chkDeleteModule_OnCheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckBox check = (CheckBox)sender;
                        GridGroupHeaderItem groupHeader = (GridGroupHeaderItem)check.NamingContainer;
                        GridItem[] children = groupHeader.GetChildItems();

                        foreach (GridItem child in children)
                        {
                            GridDataItem newChild = (GridDataItem)child;
                            CheckBox chkbx = (CheckBox)newChild["CanDelete"].FindControl("chkDelete");
                            chkbx.Checked = check.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroupPermission);
                }
            }

        }
        #region "Reports
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "Db";
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserGroupCD=" + Session["UserGroupAccessCode"].ToString();


                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        #endregion


        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (AdminService.AdminServiceClient AdmnService = new AdminService.AdminServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgMaintainGroupAccess.SelectedIndexes.Clear();
                    PreSelectItem(AdmnService, true);
                }
            }
        }
        
        private void PreSelectItem(AdminService.AdminServiceClient AdmnService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            if (AdmnService.GetUserGroupList(base.UserPrincipal.Identity._customerID).ReturnFlag == true)
            {
                var UserGroupValue = AdmnService.GetUserGroupList(base.UserPrincipal.Identity._customerID);
                Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, UserGroupValue);
                List<AdminService.UserGroup> filteredList = GetFilteredList(UserGroupValue);

                foreach (var Item in filteredList)
                {
                    if (PrimaryKeyValue == Item.UserGroupID)
                    {
                        FullItemIndex = iIndex;
                        break;
                    }

                    iIndex++;
                }

                int PageSize = dgMaintainGroupAccess.PageSize;
                int PageNumber = FullItemIndex / PageSize;
                int ItemIndex = FullItemIndex % PageSize;

                dgMaintainGroupAccess.CurrentPageIndex = PageNumber;
                dgMaintainGroupAccess.SelectedIndexes.Add(ItemIndex);
            }
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfUserGroup UserGroupValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["UserGroupSelectedItem"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = UserGroupValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().UserGroupID;
                Session["UserGroupSelectedItem"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<AdminService.UserGroup> GetFilteredList(ReturnValueOfUserGroup UserGroupValue)
        {
            List<AdminService.UserGroup> filteredList = new List<AdminService.UserGroup>();

            if (UserGroupValue.ReturnFlag)
            {
                filteredList = UserGroupValue.EntityList;
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (AdminService.AdminServiceClient AdmnService = new AdminService.AdminServiceClient())
            {
                if (_selectLastModified)
                {
                    dgMaintainGroupAccess.SelectedIndexes.Clear();
                    PreSelectItem(AdmnService, false);
                    dgMaintainGroupAccess.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                    dgGroupAndAccess_BindData(sender, null);
                }
                else if(hdnSave.Value == "Update")
                {
                    SelectItem();
                }
            }
        }
        
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["UserGroupSelectedItem"] != null)
                    {
                        string ID = Session["UserGroupSelectedItem"].ToString();
                        foreach (GridDataItem Item in dgMaintainGroupAccess.MasterTableView.Items)
                        {
                            if (Item["UserGroupID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelectGrid(false);
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void dgMaintainGroupAccess_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgMaintainGroupAccess, Page.Session);
        }
    }
}
