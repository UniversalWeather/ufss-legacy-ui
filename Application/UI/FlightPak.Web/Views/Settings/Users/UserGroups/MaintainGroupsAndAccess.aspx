﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="MaintainGroupsAndAccess.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Users.UserGroups.MaintainGroupsAndAccess"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function CheckTxtBox(control) {


            var txtCode = document.getElementById("<%=tbusergroup.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbdescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvusergroup.ClientID%>'));
                    document.getElementById("<%=tbusergroup.ClientID%>").focus();
                    setTimeout("document.getElementById('<%=tbusergroup.ClientID%>').focus()", 0);
                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvdescription.ClientID%>'));
                    document.getElementById("<%=tbdescription.ClientID%>").focus();
                    setTimeout("document.getElementById('<%=tbdescription.ClientID%>').focus()", 0);
                } return false;
            }
        }
        function ShowReports(radWin, ReportFormat, ReportName) {
            document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
            document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
            if (ReportFormat == "EXPORT") {
                url = "../../Reports/ExportReportInformation.aspx";
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, 'RadExportData');
                return true;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Group Administration</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBUserGroupAcess');"
                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <%-- <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBUserGroupAcessExport');return false;"
                            OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>--%>
                        <asp:Button ID="btnShowReports" runat="server" CssClass="button-disable" Style="display: none;"
                            OnClick="btnShowReports_OnClick" />
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                        <a href="../../../Help/Administration/UserManagement.aspx?Screen=UserManagement"
                            class="help-icon" target="_blank" title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <div id="divSaveCancel" runat="server">
    </div>
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="dgMaintainGroupAccess">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgMaintainGroupAccess" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadCopyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    Title="Export Report Information" KeepInScreenBounds="true" AutoSize="false"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="CopyUserGroupAccessPopUp.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
    </telerik:RadAjaxLoadingPanel>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            // this function is used to close the pop up
            function confirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }

            //this function is used to display the value of selected country code from popup
            function onClientCloseCountryPopup(oWnd, args) {

                var grid = $find("<%=dgMaintainGroupAccess.ClientID %>");
                var linkButton1 = $telerik.findControl(grid.get_element(), "lnkInitInsert");

                linkButton1.click();

                //                footerLabelID.Click();               

                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%=tbusergroup.ClientID%>").value = arg.UserGroup;
                        document.getElementById("<%=tbdescription.ClientID%>").value = arg.Desc;

                    }
                    else {
                        document.getElementById("<%=tbusergroup.ClientID%>").value = "";
                        document.getElementById("<%=tbdescription.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {


                if (radWin == "RadCopyMasterPopup") {

                    var url = "./CopyUserGroupAccessPopUp.aspx";

                }
                //var oWnd = radopen(url, radWin, 'width=300, height=100, menubar=yes, resizable=no');
                if (url != "") {
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    var oWnd = oManager.open(url, radWin);
                }
            }

            // this function is used to the refresh the currency grid
            function refreshGrid(arg) {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest('Rebind');
            }
            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            //this function is used to get radwindow frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) {
                    oWindow = window.radWindow;
                }
                else if (window.frameElement.radWindow) {
                    oWindow = window.frameElement.radWindow;
                }
                return oWindow;
            }

            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
 
            //To display plane image at right place

            $(document).ready(function showPlaneImage() {

                $('#ctl00_ctl00_MainContent_SettingBodyContent_dgMaintainGroupAccess_ctl00_ctl03_ctl01_lnkInitEdit,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_dgMaintainGroupAccess_ctl00_ctl03_ctl01_lnkInitInsert,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_dgMaintainGroupAccess_ctl00_ctl03_ctl01_lnkDelete,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnSaveTop,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnCancelTop,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnSaveChanges,'
                    + '#ctl00_ctl00_MainContent_SettingBodyContent_btnCancel,'
                    + '.rgExpand, .rgCollapse').on('click', setPlaneImage);
            });

            var initialAccessGridHeight = 0;

            function setPlaneImage() {

                var topMargin = 0;
                var scrollTop = $(window).scrollTop();
                var thisTop = $(this).position().top;

                var accessGridHeight = $('#ctl00_ctl00_MainContent_SettingBodyContent_dgGroupAndAccess_ctl00').height();
                var accessGridTop = $('#ctl00_ctl00_MainContent_SettingBodyContent_dgGroupAndAccess_ctl00').position().top;

                if (initialAccessGridHeight == 0) {
                    initialAccessGridHeight = accessGridHeight;
                }

                if (scrollTop > 300) {
                    topMargin = accessGridHeight - initialAccessGridHeight + thisTop - accessGridTop - (accessGridHeight - initialAccessGridHeight) - Math.floor((accessGridHeight - initialAccessGridHeight) / 2) - 30;
                }
                else {
                    topMargin = scrollTop - 300;
                }

                //console.info(initialAccessGridHeight + ', ' + accessGridHeight + ', ' + accessGridTop + ', '
                //     + scrollTop + ', ' + thisTop + ', ' + topMargin);

                $('div.raDiv').attr('style', 'margin-top: ' + topMargin + 'px !important');
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <telerik:RadGrid ID="dgMaintainGroupAccess" runat="server" AllowSorting="true" OnItemCreated="dgMaintainGroupAccess_ItemCreated"
            OnPageIndexChanged="dgMaintainGroupAccess_PageIndexChanged" Visible="true" OnNeedDataSource="dgMaintainGroupAccess_BindData"
            OnItemCommand="dgMaintainGroupAccess_ItemCommand" OnUpdateCommand="dgMaintainGroupAccess_UpdateCommand"
            OnInsertCommand="dgMaintainGroupAccess_InsertCommand" OnDeleteCommand="dgMaintainGroupAccess_DeleteCommand"
            AutoGenerateColumns="false" Height="341px" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgMaintainGroupAccess_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPreRender="dgMaintainGroupAccess_PreRender">
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView DataKeyNames="CustomerID,UserGroupCD,UserGroupDescription,LastUpdUID,LastUpdTS,UserGroupID"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="UserGroupCD" HeaderText="Code" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px"
                        FilterControlWidth="100px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="UserGroupDescription" HeaderText="Description" FilterDelay="500"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="640px" FilterControlWidth="620px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="UserGroupID" HeaderText="UserGroupID" CurrentFilterFunction="EqualTo"
                        FilterDelay="500" ShowFilterIcon="false" Display="false" AutoPostBackOnFilter="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert">
                    <img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            ToolTip="Edit" CommandName="Edit">
                    <img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lnkCopy" OnClientClick="javascript:openWin('RadCopyMasterPopup');return false;"
                            runat="server" CommandName="CopySelected" ToolTip="Copy">
                    <img style="border:0px;vertical-align:middle;" alt="Copy" src="<%=ResolveClientUrl("~/App_Themes/Default/images/copy.png") %>"/></asp:LinkButton>
                        <asp:LinkButton ID="lnkDelete" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                            OnClientClick="javascript:return ProcessDelete();">
                    <img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>"  /></asp:LinkButton>
                    </div>
                    <div style="padding: 5px 5px; float: right;">
                        <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" class="tdLabel160">
                    <div id="tdSuccessMessage" class="success_msg">
                        Record saved successfully.</div>
                </td>
                <td align="right">
                    <div class="mandatory">
                        <span>Bold</span> Indicates required field</div>
                </td>
            </tr>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0" class="tblButtonArea">
            <tr>
                <td align="right">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:Button ID="btnSaveTop" CssClass="button" Text="Save" OnClick="btnSaveChanges_Click"
                                    runat="server" ValidationGroup="Save" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelTop" CssClass="button" OnClick="btnCancel_Click" Text="Cancel"
                                    runat="server" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table style="width: 100%">
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td valign="top" class="tdLabel130 mnd_text">
                                    User Group
                                </td>
                                <td class="tdLabel150">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:TextBox ID="tbusergroup" runat="server" CssClass="text122" MaxLength="5" AutoPostBack="true"
                                                    OnTextChanged="Code_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                    onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbusergroup" ErrorMessage="The User Group entered already exists. Please enter a unique value"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="rfvusergroup" runat="server" ValidationGroup="Save"
                                                    ControlToValidate="tbusergroup" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">User Group Required.</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" colspan="2">
                                    <table style="width: 100%">
                                        <tr>
                                            <td class="tdLabel65">
                                            </td>
                                            <td>
                                                <%-- <asp:CheckBox ID="chkapplytripprivacysettings" runat="server" Text="Apply Trip Privacy Settings" />--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="mnd_text">
                                    Description
                                </td>
                                <td colspan="3">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbdescription" runat="server" CssClass="text455"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvdescription" runat="server" ValidationGroup="Save"
                                                    ControlToValidate="tbdescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Description Required.</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0" class="admin-grid">
                            <tr>
                                <td>
                                    <telerik:RadGrid ID="dgGroupAndAccess" runat="server" AllowSorting="false" AllowPaging="false"
                                        ShowGroupPanel="false" AutoGenerateColumns="False" OnNeedDataSource="dgGroupAndAccess_BindData"
                                        GridLines="None" OnItemDataBound="dgGroupAndAccess_ItemDataBound" ViewStateMode="Inherit"
                                        EnableLinqExpressions="true" OnItemCommand="dgGroupAndAccess_OnItemCommand" OnItemCreated="dgGroupAndAccess_OnItemCreated">
                                        <%--<PagerStyle Mode="NumericPages"></PagerStyle>--%>
                                        <GroupingSettings CaseSensitive="false" />
                                        <MasterTableView DataKeyNames="UMPermissionID,CustomerID,UMPermissionName,UserGroupPermissionID"
                                            Width="100%" GroupLoadMode="Client" TableLayout="Fixed" EnableColumnsViewState="true"
                                            GroupsDefaultExpanded="false">
                                            <GroupByExpressions>
                                                <telerik:GridGroupByExpression>
                                                    <SelectFields>
                                                        <telerik:GridGroupByField FieldAlias="ModuleName" FieldName="ModuleName"></telerik:GridGroupByField>
                                                    </SelectFields>
                                                    <GroupByFields>
                                                        <telerik:GridGroupByField FieldName="SortOrder"></telerik:GridGroupByField>
                                                    </GroupByFields>
                                                </telerik:GridGroupByExpression>
                                            </GroupByExpressions>
                                            <GroupHeaderTemplate>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="tdLabel289">
                                                            <asp:Label runat="server" ID="Label2" Text='<%#"Module Name: " + Eval("ModuleName") %>'>
                                                            </asp:Label>
                                                            <asp:Label runat="server" ID="lblModuleName" Text='<%#Eval("ModuleName") %>' Visible="false">
                                                            </asp:Label>
                                                        </td>
                                                        <td class="tdLabel99">
                                                            <asp:CheckBox ID="chkViewModule" runat="server" AutoPostBack="true" ToolTip="Apply to All"
                                                                TextAlign="Left" OnCheckedChanged="chkViewModule_OnCheckedChanged" />
                                                        </td>
                                                        <td class="tdLabel100">
                                                            <asp:CheckBox ID="chkAddModule" ToolTip="Apply to All" runat="server" AutoPostBack="true"
                                                                OnCheckedChanged="chkAddModule_OnCheckedChanged" />
                                                        </td>
                                                        <td class="tdLabel99">
                                                            <asp:CheckBox ID="chkEditModule" runat="server" AutoPostBack="true" ToolTip="Apply to All"
                                                                OnCheckedChanged="chkEditModule_OnCheckedChanged" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkDeleteModule" runat="server" AutoPostBack="true" ToolTip="Apply to All"
                                                                OnCheckedChanged="chkDeleteModule_OnCheckedChanged" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </GroupHeaderTemplate>
                                            <Columns>
                                                <telerik:GridBoundColumn SortExpression="UMPermissionName" HeaderText="Permission Name"
                                                    DataField="UMPermissionName" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                    AllowFiltering="true" AutoPostBackOnFilter="false" HeaderStyle-Width="300px" FilterControlWidth="280px"
                                                    ItemStyle-Width="300px" FilterDelay="500">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn SortExpression="UMPermissionID" HeaderText="Permission Id"
                                                    HeaderButtonType="TextButton" DataField="UMPermissionID" ShowFilterIcon="false"
                                                    CurrentFilterFunction="EqualTo" Display="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn UniqueName="CanView" AllowFiltering="false" ShowFilterIcon="false">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkViewAll" AutoPostBack="true" runat="server" OnCheckedChanged="chkViewAll_CheckedChanged"
                                                            Text="View" TextAlign="Right" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkview" runat="server" Checked='<%# Convert.ToBoolean(Eval("CanView")) %>'>
                                                        </asp:CheckBox>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="CanAdd" AllowFiltering="false" ShowFilterIcon="false">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAddAll" AutoPostBack="true" runat="server" OnCheckedChanged="chkAddAll_CheckedChanged"
                                                            Text="Add" TextAlign="Right" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkadd" runat="server" Checked='<%# Convert.ToBoolean(Eval("CanAdd")) %>'>
                                                        </asp:CheckBox>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="CanEdit" AllowFiltering="false" ShowFilterIcon="false">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkEditAll" AutoPostBack="true" runat="server" OnCheckedChanged="chkEditAll_CheckedChanged"
                                                            Text="Edit" TextAlign="Right" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkedit" runat="server" Checked='<%# Convert.ToBoolean(Eval("CanEdit")) %>'>
                                                        </asp:CheckBox>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="CanDelete" ShowFilterIcon="false" SortExpression="CanDelete"
                                                    AllowFiltering="false">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkDeleteAll" AutoPostBack="true" runat="server" OnCheckedChanged="chkDeleteAll_CheckedChanged"
                                                            Text="Delete" TextAlign="Right" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkDelete" runat="server" Checked='<%# Convert.ToBoolean(Eval("CanDelete")) %>'>
                                                        </asp:CheckBox>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                        </MasterTableView>
                                        <ClientSettings AllowGroupExpandCollapse="true" ReorderColumnsOnClient="false" AllowDragToGroup="True"
                                            AllowColumnsReorder="True">
                                        </ClientSettings>
                                        <ClientSettings AllowDragToGroup="false" AllowColumnsReorder="false" AllowRowsDragDrop="false"
                                            Animation-AllowColumnReorderAnimation="false">
                                        </ClientSettings>
                                        <GroupingSettings ShowUnGroupButton="true" />
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSaveChanges" CssClass="button" Text="Save" OnClick="btnSaveChanges_Click"
                                        runat="server" ValidationGroup="Save" />
                                    <asp:Button ID="btnCancel" CssClass="button" OnClick="btnCancel_Click" Text="Cancel"
                                        runat="server" CausesValidation="false" />
                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                    <asp:HiddenField ID="hdnDescription" runat="server" />
                                    <asp:HiddenField ID="hdnCode" runat="server" />
                                    <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>

