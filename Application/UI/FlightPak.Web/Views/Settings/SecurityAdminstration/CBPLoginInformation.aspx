﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Settings.master"
    CodeBehind="CBPLoginInformation.aspx.cs" Inherits="FlightPak.Web.Views.Settings.SecurityAdminstration.CBPLoginInformation" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
<telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">CBP Login</span> <span class="tab-nav-icons"><a href="#"
                        class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <fieldset>
            
                <table>
                    
                </table>
                <table align="center">
                    <tr>
                        <td>
                            <asp:Label ID="lbCBPinfo" runat="server" Text="CBP Info is applicable to All Main Bases in Company Profile"
                                CssClass="grn-text"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" CssClass="button" />
                        <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                            CssClass="button" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
