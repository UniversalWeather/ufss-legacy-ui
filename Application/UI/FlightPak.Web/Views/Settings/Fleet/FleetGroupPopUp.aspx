﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FleetGroupPopUp.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Fleet.FleetGroupPopUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fleet Group</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgFleetGroup.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgFleetGroup.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "FleetGroupCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "FleetGroupDescription");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "HomeBaseCD")
                        if (i == 0)
                            oArg.FleetGroupCD = cell1.innerHTML + ",";
                        else
                            oArg.FleetGroupCD += cell1.innerHTML + ",";
                        oArg.FleetGroupDescription += cell2.innerHTML + ",";
                        oArg.HomeBaseCD += cell3.innerHTML + ",";
                    }
                }
                else {
                    oArg.FleetGroupCD = "";
                    oArg.FleetGroupDescription = "";
                    oArg.HomeBaseCD = "";
                }
                if (oArg.FleetGroupCD != "")
                    oArg.FleetGroupCD = oArg.FleetGroupCD.substring(0, oArg.FleetGroupCD.length - 1)
                else
                    oArg.FleetGroupCD = "";

                oArg.Arg1 = oArg.FleetGroupCD;
                oArg.CallingButton = "FleetGroupCD";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgFleetGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetGroup" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" /></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                    OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgFleetGroup" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
                        OnItemCommand="dgFleetGroup_ItemCommand" OnInsertCommand="dgFleetGroup_InsertCommand"
                        OnNeedDataSource="dgFleetGroup_BindData" AutoGenerateColumns="false" Height="341px" OnItemCreated="dgFleetGroup_ItemCreated"
                        AllowPaging="false" Width="500px" OnPreRender="dgFleetGroup_PreRender">
                        <MasterTableView DataKeyNames="FleetGroupCD,FleetGroupDescription,HomeBaseCD,FleetGroupID"
                            CommandItemDisplay="None" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn DataField="FleetGroupID" HeaderText="Code" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="EqualTo" Display="false" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FleetGroupCD" HeaderText="Code" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FleetGroupDescription" HeaderText="Description"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="300px" FilterControlWidth="280px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>                                
                                <div class="grd_ok">
                                    <asp:Button ID="btnSubmit" OnClientClick="returnToParent(); return false;" Text="Ok"
                                        runat="server" CssClass="button"></asp:Button>
                                </div>
                                <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                    visible="false">
                                    Use CTRL key to multi select</div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent"  />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <asp:Label ID="InjectScript" runat="server"></asp:Label>
                    <asp:Label ID="lbMessage" runat="server" CssClass="alert-text" Font-Bold="true"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
