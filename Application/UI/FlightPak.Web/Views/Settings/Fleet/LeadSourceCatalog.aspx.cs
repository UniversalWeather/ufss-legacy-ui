﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class LeadSourceCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private bool LeadSourcePageNavigated = false;
        private List<string> lstLeadSourceCodes = new List<string>();
        private string strLeadSourceID = "";
        private string strLeadSourceCD = "";
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgLeadSource, dgLeadSource, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgLeadSource.ClientID));
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewLeadSourceCatalogReport);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgLeadSource.Rebind();
                                ReadOnlyForm();                                
                                DisableLinks();                                
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                CheckAutorization(Permission.Database.ViewLeadSourceCatalog);
                                base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                        if (IsPopUp)
                        {
                            dgLeadSource.AllowPaging = false;
                            chkSearchActiveOnly.Visible = false;
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }

        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgLeadSource.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var LeadSourceValue = FPKMstService.GetLeadSourceList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, LeadSourceValue);
            List<FlightPakMasterService.LeadSource> filteredList = GetFilteredList(LeadSourceValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.LeadSourceID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgLeadSource.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgLeadSource.CurrentPageIndex = PageNumber;
            dgLeadSource.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfLeadSource LeadSourceValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedLeadSourceID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = LeadSourceValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().LeadSourceID;
                Session["SelectedLeadSourceID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.LeadSource> GetFilteredList(ReturnValueOfLeadSource LeadSourceValue)
        {
            List<FlightPakMasterService.LeadSource> filteredList = new List<FlightPakMasterService.LeadSource>();

            if (LeadSourceValue.ReturnFlag)
            {
                filteredList = LeadSourceValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<LeadSource>(); }
                }
            }

            return filteredList;
        }
        /// <summary>
        /// To select the first record as default in read only mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgLeadSource.Rebind();
                    }
                    if (dgLeadSource.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedLeadSourceID"] = null;
                        //}
                        if (Session["SelectedLeadSourceID"] == null)
                        {
                            dgLeadSource.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                            {
                                Session["SelectedLeadSourceID"] = dgLeadSource.Items[0].GetDataKeyValue("LeadSourceID").ToString();
                            }
                        }

                        if (dgLeadSource.SelectedIndexes.Count == 0)
                            dgLeadSource.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the selected item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedLeadSourceID"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                        //string code = items.GetDataKeyValue("LeadSourceCD").ToString();
                        string ID = Session["SelectedLeadSourceID"].ToString();
                        foreach (GridDataItem item in dgLeadSource.MasterTableView.Items)
                        {
                            if (item["LeadSourceID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton deleteButton = (e.Item as GridCommandItem).FindControl("lnkDelete") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(deleteButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            //target.Focus();
                            RadAjaxManager1.FocusControl(target); //tbCode.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + target + "');", true);
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Bind Lead Source Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objLeadSourceService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objDelVal = objLeadSourceService.GetLeadSourceList();
                            if (objDelVal.ReturnFlag == true)
                            {
                                dgLeadSource.DataSource = objDelVal.EntityList;
                            }
                            Session["LeadSourceCodes"] = objDelVal.EntityList.ToList();
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        /// <summary>
        /// Item Command for Lead Source Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                hdnRedirect.Value = "";
                                if (Session["SelectedLeadSourceID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.LeadSource, Convert.ToInt64(Session["SelectedLeadSourceID"].ToString().Trim()));
                                        Session["IsLeadSourceEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.LeadSource);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.LeadSource);

                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        //tbDescription.Focus();
                                        RadAjaxManager1.FocusControl(tbDescription.ClientID); 
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                                        SelectItem();
                                        DisableLinks();
                                        tbCode.Enabled = false;
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgLeadSource.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                //tbCode.Focus();
                                RadAjaxManager1.FocusControl(tbCode.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                                break;
                            case "UpdateEdited":
                                dgLeadSource_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        /// <summary>
        /// Update Command for Lead Source Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedLeadSourceID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objLeadSourceService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objLeadSourceService.UpdateLeadSource(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    ///////Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.LeadSource, Convert.ToInt64(Session["SelectedLeadSourceID"].ToString().Trim()));
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        DefaultSelection(true);

                                    }
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.LeadSource);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            // Session["SelectedLeadSourceID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radLeadSourcePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        /// <summary>
        /// Checking New Lead Source already exists in the Lead Source Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>

        private bool CheckAllReadyExist()
        {
            bool ReturnFlag = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    List<LeadSource> LeadSourceList = new List<LeadSource>();
                    LeadSourceList = ((List<LeadSource>)Session["LeadSourceCodes"]).Where(x => x.LeadSourceCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<LeadSource>();
                    if (LeadSourceList.Count != 0)
                    {
                        cvLeadSourceCode.IsValid = false;
                        //tbCode.Focus();
                        RadAjaxManager1.FocusControl(tbCode.ClientID);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                        ReturnFlag = true;
                    }
                    return ReturnFlag;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnFlag;
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgLeadSource.ClientSettings.Scrolling.ScrollTop = "0";
                        LeadSourcePageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        /// <summary>
        /// Insert Command for Lead Source Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvLeadSourceCode.IsValid = false;
                            //tbCode.Focus();
                            
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objLeadSourceService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objLeadSourceService.AddLeadSource(GetItems());
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    dgLeadSource.Rebind();
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.LeadSource);
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radLeadSourcePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedLeadSourceID"] != null)
                        {
                            FlightPakMasterService.MasterCatalogServiceClient LeadSourceService = new FlightPakMasterService.MasterCatalogServiceClient();
                            FlightPakMasterService.LeadSource LeadSource = new FlightPakMasterService.LeadSource();
                            string Code = Session["SelectedLeadSourceID"].ToString();
                            strLeadSourceCD = "";
                            strLeadSourceID = "";
                            foreach (GridDataItem Item in dgLeadSource.MasterTableView.Items)
                            {
                                if (Item["LeadSourceID"].Text.Trim() == Code.Trim())
                                {
                                    strLeadSourceCD = Item["LeadSourceCD"].Text.Trim();
                                    strLeadSourceID = Item["LeadSourceID"].Text.Trim();
                                    break;
                                }
                            }
                            LeadSource.LeadSourceCD = strLeadSourceCD;
                            LeadSource.LeadSourceID = Convert.ToInt64(strLeadSourceID);
                            LeadSource.IsDeleted = true;
                            //Lock will happen from UI
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.LeadSource, Convert.ToInt64(strLeadSourceID));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                    if (IsPopUp)
                                        ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.LeadSource);
                                    else
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.LeadSource);
                                    return;
                                }
                                LeadSourceService.DeleteLeadSource(LeadSource);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.LeadSource, Convert.ToInt64(strLeadSourceID));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgLeadSource.SelectedItems[0] as GridDataItem;
                                Session["SelectedLeadSourceID"] = item["LeadSourceID"].Text;
                                if (btnSaveChanges.Visible == false)
                                {
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);
                                }
                            }

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                    }
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Lead Source on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            if (CheckAllReadyExist())
                            {
                                cvLeadSourceCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Lead Source Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgLeadSource.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgLeadSource.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        /// <summary>
        /// Cancel Lead Source Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedLeadSourceID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.LeadSource, Convert.ToInt64(Session["SelectedLeadSourceID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        EnableLinks();
                        //Session.Remove("SelectedLeadSourceID");
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radLeadSourcePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgLeadSource;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private LeadSource GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.LeadSource LeadSourceService = null;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LeadSourceService = new FlightPakMasterService.LeadSource();
                    LeadSourceService.LeadSourceCD = tbCode.Text;
                    LeadSourceService.LeadSourceDescription = tbDescription.Text;
                    //need to get data
                    if (hdnSave.Value == "Update")
                    {
                        //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                        if (Session["SelectedLeadSourceID"] != null)
                        {
                            LeadSourceService.LeadSourceID = Convert.ToInt64(Session["SelectedLeadSourceID"].ToString().Trim());
                        }
                    }
                    else
                    {
                        LeadSourceService.LeadSourceID = 0;
                    }
                    LeadSourceService.IsInActive = chkInactive.Checked;
                    LeadSourceService.IsDeleted = false;
                    return LeadSourceService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return LeadSourceService;
            }
        }

        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedLeadSourceID"] != null)
                    {

                        strLeadSourceID = "";

                        foreach (GridDataItem Item in dgLeadSource.MasterTableView.Items)
                        {
                            if (Item["LeadSourceID"].Text.Trim() == Session["SelectedLeadSourceID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("LeadSourceCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("LeadSourceCD").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("LeadSourceID") != null)
                                {
                                    strLeadSourceID = Item.GetDataKeyValue("LeadSourceID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("LeadSourceDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("LeadSourceDescription").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                break;
                            }
                        }
                        EnableForm(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtninsertCtl, lbtndelCtl, lbtneditCtl;
                    lbtninsertCtl = (LinkButton)dgLeadSource.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    lbtndelCtl = (LinkButton)dgLeadSource.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    lbtneditCtl = (LinkButton)dgLeadSource.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddLeadSourceCatalog))
                    {
                        lbtninsertCtl.Visible = true;
                        if (add)
                        {
                            lbtninsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtninsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtninsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteLeadSourceCatalog))
                    {
                        lbtndelCtl.Visible = true;
                        if (delete)
                        {
                            lbtndelCtl.Enabled = true;
                            lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtndelCtl.Enabled = false;
                            lbtndelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtndelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditLeadSourceCatalog))
                    {
                        lbtneditCtl.Visible = true;
                        if (edit)
                        {
                            lbtneditCtl.Enabled = true;
                            lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtneditCtl.Enabled = false;
                            lbtneditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtneditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (LeadSourcePageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgLeadSource, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }

        /// <summary>
        /// To display the value in read only format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear  the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbDescription.Enabled = enable;
                    btnSaveChanges.Visible = enable;
                    btnCancel.Visible = enable;
                    chkInactive.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                tableLeadSource.Visible = false;
                                dgLeadSource.Visible = false;
                                if (IsAdd)
                                {
                                    (dgLeadSource.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgLeadSource.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
            
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgLeadSource.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgLeadSource.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();


                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedLeadSourceID"] != null)
                    {
                        //TimeDisplay();
                        foreach (GridDataItem Item in dgLeadSource.MasterTableView.Items)
                        {
                            if (Item["LeadSourceID"].Text.Trim() == Session["SelectedLeadSourceID"].ToString().Trim())
                            {
                                //GridDataItem Item = dgLeadSource.SelectedItems[0] as GridDataItem;
                                if (Item.GetDataKeyValue("LeadSourceCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("LeadSourceCD").ToString();
                                }
                                if (Item.GetDataKeyValue("LeadSourceDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("LeadSourceDescription").ToString();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }

                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgLeadSource.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Lead Source Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["LeadSourceCD"].Text;
                                lbColumnValue2.Text = Item["LeadSourceDescription"].Text;

                                //Item.Selected = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.LeadSource);
                }
            }
        }
        #endregion

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.LeadSource> lstLeadSource = new List<FlightPakMasterService.LeadSource>();
                if (Session["LeadSourceCodes"] != null)
                {
                    lstLeadSource = (List<FlightPakMasterService.LeadSource>)Session["LeadSourceCodes"];
                }
                if (lstLeadSource.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstLeadSource = lstLeadSource.Where(x => x.IsInActive == false).ToList<LeadSource>(); }
                    dgLeadSource.DataSource = lstLeadSource;
                    if (IsDataBind)
                    {
                        dgLeadSource.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;

                //Check the formatting condition
                if (dataBoundItem["LeadSourceDescription"].Text.Count() > 40)
                {
                    string strDesc = dataBoundItem["LeadSourceeDescription"].Text;
                    dataBoundItem["LeadSourceDescription"].Text = strDesc.Substring(0, 65) + "...";
                    dataBoundItem["LeadSourceDescription"].ToolTip = strDesc;
                }
            }
        }
    }
}