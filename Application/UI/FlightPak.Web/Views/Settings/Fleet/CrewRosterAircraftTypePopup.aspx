﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewRosterAircraftTypePopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Fleet.CrewRosterAircraftTypePopup" ClientIDMode="AutoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Aircraft Type</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            //this function is used to get the selected code
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgAircraftType.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "AircraftCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "AircraftDescription")
                }

                if (selectedRows.length > 0) {
                    oArg.AircraftCD = cell1.innerHTML;
                    oArg.AircraftDescription = cell2.innerHTML;
                }
                else {
                    oArg.AircraftCD = "";
                    oArg.AircraftDescription = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            function RowDblClick() {
                var masterTable = $find("<%= dgAircraftType.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgAircraftType" runat="server" AllowMultiRowSelection="true"
            AllowSorting="true" OnNeedDataSource="AircraftType_BindData" OnItemCommand="AircraftType_ItemCommand"
            OnInsertCommand="AircraftType_InsertCommand" AutoGenerateColumns="false" Height="330px"
            AllowPaging="false" Width="500px" PagerStyle-AlwaysVisible="true" OnPreRender="dgAircraftType_PreRender"
            AllowFilteringByColumn="true" OnItemCreated="AircraftType_ItemCreated" >
            <MasterTableView DataKeyNames="AircraftID,AircraftCD,AircraftDescription" CommandItemDisplay="Bottom" AllowPaging="false">
                <Columns>
                    <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Type Code" CurrentFilterFunction="StartsWith"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="100px" FilterDelay="500"
                        FilterControlWidth="80px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AircraftDescription" HeaderText="Aircraft Type"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith" ShowFilterIcon="false" FilterDelay="500"
                        HeaderStyle-Width="380px" FilterControlWidth="360px">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;">
                        &nbsp;&nbsp;
                        <asp:Button id="btnSubmit" OnClientClick="returnToParent(); return false;" runat="server" Text="OK" CssClass="button" />
                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick = "RowDblClick" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Label ID="InjectScript" runat="server"></asp:Label>
        <br />
        <asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    </form>
</body>
</html>
