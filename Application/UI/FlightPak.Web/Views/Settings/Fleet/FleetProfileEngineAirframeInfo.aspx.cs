﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using System.Text.RegularExpressions;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetProfileEngineAirframeInfo : BaseSecuredPage
    {
        private ExceptionManager exManager;
        string DateFormat = string.Empty;
        private bool IsValid = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // To Assign Date Format from User Control, based on Company Profile
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }
                        ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;


                        if (!IsPostBack)
                        {
                            TimeDisplay();  //SetMask();
                            btnsavechanges.Visible = false;
                            hdnPkeyId.Value = "0";

                            ViewState["FleetID"] = "";
                            if (Request.QueryString["FleetID"] != null)
                            {
                                hdnFleetID.Value = Request.QueryString["FleetID"].ToString().Trim();
                                if (Request.QueryString["FleetID"] != null)
                                {
                                    ViewState["FleetID"] = Request.QueryString["FleetID"].ToString();
                                }
                                if (Request.QueryString["TailNum"] != null && Request.QueryString["TailNum"].ToString().Trim() != "")
                                {
                                    tbTailNumber.Text = Request.QueryString["TailNum"].ToString();
                                }
                                tbTailNumber.Enabled = false;
                                if (ViewState["FleetID"] != null)
                                {
                                    LoadData(Convert.ToInt64(ViewState["FleetID"].ToString()));
                                }
                                ContentPlaceHolder cph = (ContentPlaceHolder)Master.Master.FindControl("MainContent");
                                if (cph != null)
                                {
                                    RadPanelBar rpb = (RadPanelBar)cph.FindControl("pnlNaviGation");

                                }
                                btnsavechanges.Visible = true;
                                btncancel.Visible = true;                                
                                CheckAutorization(Permission.Database.ViewFleetProfileEngineAirframeInfo);
                                if (hdnMode.Value.ToLower() == "edit")
                                {

                                    if (IsAuthorized(Permission.Database.EditFleetProfileEngineAirframeInfo))
                                    {
                                        btnsavechanges.Visible = true;
                                        btncancel.Visible = true;
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.Lock(EntitySet.Database.FleetProfile, Convert.ToInt64(Request.QueryString["FleetID"].ToString().Trim()));
                                            if (!returnValue.ReturnFlag)
                                            {
                                                //ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetProfile);
                                                string radPageLoadAlertScript = "<script language='javascript'>function PageLoad_Alert(){ jAlert('" + returnValue.LockMessage + "', '" + ModuleNameConstants.Database.FleetProfile + "'); Sys.Application.remove_load(PageLoad_Alert);}; Sys.Application.add_load(PageLoad_Alert);</script>";
                                                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radPageLoadAlertScript);
                                                btnsavechanges.Enabled = false;
                                                btnsavechanges.CssClass = "button-disable";
                                            }
                                        }
                                    }
                                }                                
                            }
                            else
                            {
                                btnsavechanges.Visible = false;
                                btncancel.Visible = false;
                            }


                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

        }
        /// <summary>
        /// Method to Set Time Format, based on Company Profile
        /// </summary>
        public void TimeDisplay()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UserPrincipal.Identity != null)
                {

                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                    if (UserPrincipal.Identity != null)
                    {
                        // Display Time Fields based on Tenths / Minutes - Company Profile
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            string OnPress = "javascript:return fnAllowNumericAndChar(this, event,'.')";
                            SetMask("0000000.0", 9, OnPress);
                            SetMaskToThree("0000000.00", 10, OnPress);
                        }
                        else
                        {
                            string OnPress = "javascript:return fnAllowNumericAndChar(this, event,':')";
                            SetMask("0:00", 10, OnPress);
                            SetMaskToThree("0000000.00", 10, OnPress);
                        }
                    }
                    else
                    {
                        string OnPress = "javascript:return fnAllowNumericAndChar(this, event,':')";
                        SetMask("0:00", 4, OnPress);
                    }
                }
            }
        }

        private void SetMask(string display, int maxlength,string  OnPress)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(display, maxlength, OnPress))
            {
                tbHoursAirFrame.Attributes.Add("onkeypress", OnPress);
                tbHoursAirFrame.Text = display;            
                tbHoursAirFrame.MaxLength = maxlength;
                tbHoursEngine1.Attributes.Add("onkeypress", OnPress);
                tbHoursEngine1.Text = display;
                tbHoursEngine1.MaxLength = maxlength;
                tbHoursEngine2.Attributes.Add("onkeypress", OnPress);
                tbHoursEngine2.Text = display;
                tbHoursEngine2.MaxLength = maxlength;
                tbHoursEngine3.Attributes.Add("onkeypress", OnPress);
                tbHoursEngine3.Text = display;
                tbHoursEngine3.MaxLength = maxlength;
                tbHoursEngine4.Attributes.Add("onkeypress", OnPress);
                tbHoursEngine4.Text = display;
                tbHoursEngine4.MaxLength = maxlength;
                tbReserver1hours.Attributes.Add("onkeypress", OnPress);
                tbReserver1hours.Text = display;
                tbReserver1hours.MaxLength = maxlength;
                tbReserver2hours.Attributes.Add("onkeypress", OnPress);
                tbReserver2hours.Text = display;
                tbReserver2hours.MaxLength = maxlength;
                tbReserver3hours.Attributes.Add("onkeypress", OnPress);
                tbReserver3hours.Text = display;
                tbReserver3hours.MaxLength = maxlength;
                tbReserver4hours.Attributes.Add("onkeypress", OnPress);
                tbReserver4hours.Text = display;
                tbReserver4hours.MaxLength = maxlength;
            }

        }

        private void SetMaskToThree(string display, int maxlength, string OnPress)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(display, maxlength, OnPress))
            {
                tbReserver1hours.Attributes.Add("onkeypress", OnPress);
                tbReserver1hours.Text = display;
                tbReserver1hours.MaxLength = maxlength;
                tbReserver2hours.Attributes.Add("onkeypress", OnPress);
                tbReserver2hours.Text = display;
                tbReserver2hours.MaxLength = maxlength;
                tbReserver3hours.Attributes.Add("onkeypress", OnPress);
                tbReserver3hours.Text = display;
                tbReserver3hours.MaxLength = maxlength;
                tbReserver4hours.Attributes.Add("onkeypress", OnPress);
                tbReserver4hours.Text = display;
                tbReserver4hours.MaxLength = maxlength;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        private void ValidateText(string text, string expression)
        {
            bool IsCheck = true;
            int minute = 0;
            if (UserPrincipal.Identity != null)
            {
                Regex regex = new Regex(expression);
                Regex regextwodecimal = new Regex("^[0-9]{0,7}(\\.[0-9]{0,2})?$");
                // Display Time Fields based on Tenths / Minutes - Company Profile
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {

                    if (!string.IsNullOrEmpty(tbHoursAirFrame.Text))
                    {
                        if (!regex.IsMatch(tbHoursAirFrame.Text))
                        {
                            cvHoursAirFrame.IsValid = false;
                            cvHoursAirFrame.ErrorMessage = text;
                            tbHoursAirFrame.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbHoursEngine1.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbHoursEngine1.Text))
                        {
                            cvHoursEngine1.IsValid = false;
                            cvHoursEngine1.ErrorMessage = text;
                            tbHoursEngine1.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbHoursEngine2.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbHoursEngine2.Text))
                        {
                            cvHoursEngine2.IsValid = false;
                            cvHoursEngine2.ErrorMessage = text;
                            tbHoursEngine2.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbHoursEngine3.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbHoursEngine3.Text))
                        {
                            cvHoursEngine3.IsValid = false;
                            cvHoursEngine3.ErrorMessage = text;
                            tbHoursEngine3.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbHoursEngine4.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbHoursEngine4.Text))
                        {
                            cvHoursEngine4.IsValid = false;
                            cvHoursEngine4.ErrorMessage = text;
                            tbHoursEngine4.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbReserver1hours.Text)) && (IsCheck))
                    {
                        if (!regextwodecimal.IsMatch(tbReserver1hours.Text))
                        {
                            cvReserver1hours.IsValid = false;
                            cvReserver1hours.ErrorMessage = "NNNNNNN.NN";
                            tbReserver1hours.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbReserver2hours.Text)) && (IsCheck))
                    {
                        if (!regextwodecimal.IsMatch(tbReserver2hours.Text))
                        {
                            cvReserver2hours.IsValid = false;
                            cvReserver2hours.ErrorMessage = "NNNNNNN.NN";
                            tbReserver2hours.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbReserver3hours.Text)) && (IsCheck))
                    {
                        if (!regextwodecimal.IsMatch(tbReserver3hours.Text))
                        {
                            cvReserver3hours.IsValid = false;
                            cvReserver3hours.ErrorMessage = "NNNNNNN.NN";
                            tbReserver3hours.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbReserver4hours.Text)) && (IsCheck))
                    {
                        if (!regextwodecimal.IsMatch(tbReserver4hours.Text))
                        {
                            cvReserver4hours.IsValid = false;
                            cvReserver4hours.ErrorMessage = "NNNNNNN.NN";
                            IsValid = false;
                            tbReserver4hours.Focus();
                        }
                    }
                }
                else
                {

                    if (!string.IsNullOrEmpty(tbHoursAirFrame.Text))
                    {
                        if (!regex.IsMatch(tbHoursAirFrame.Text))
                        {
                            cvHoursAirFrame.IsValid = false;
                            if (tbHoursAirFrame.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbHoursAirFrame.Text.Split(':');
                                if (timeArray[1].Length < 2)
                                {
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        minute = Convert.ToInt16(timeArray[1]);
                                    }
                                    if (minute > 59)
                                    {
                                        cvHoursAirFrame.ErrorMessage = "Minute entry must be between 0 and 59";
                                    }
                                    else
                                    {
                                        cvHoursAirFrame.ErrorMessage = text;
                                    }
                                }
                                {
                                    cvHoursAirFrame.ErrorMessage = text;
                                }
                            }
                            else
                            {
                                cvHoursAirFrame.ErrorMessage = text;
                            }
                            tbHoursAirFrame.Focus();
                            IsValid = false;
                            IsCheck = false;

                        }
                    }
                    if ((!string.IsNullOrEmpty(tbHoursEngine1.Text)) && (IsCheck))
                    {

                        if (!regex.IsMatch(tbHoursEngine1.Text))
                        {
                            cvHoursEngine1.IsValid = false;
                            if (tbHoursEngine1.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbHoursEngine1.Text.Split(':');
                                if (timeArray[1].Length < 2)
                                {
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        minute = Convert.ToInt16(timeArray[1]);
                                    }
                                    if (minute > 59)
                                    {

                                        cvHoursEngine1.ErrorMessage = "Minute entry must be between 0 and 59";
                                    }
                                    else
                                    {
                                        cvHoursEngine1.ErrorMessage = text;
                                    }
                                }
                                else
                                {
                                    cvHoursEngine1.ErrorMessage = text;
                                }
                            }
                            else
                            {
                                cvHoursEngine1.ErrorMessage = text;
                            }
                            tbHoursEngine1.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbHoursEngine2.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbHoursEngine2.Text))
                        {
                            cvHoursEngine2.IsValid = false;
                            if (tbHoursEngine2.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbHoursEngine2.Text.Split(':');
                                if (timeArray[1].Length < 2)
                                {
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        minute = Convert.ToInt16(timeArray[1]);
                                    }
                                    if (minute > 59)
                                    {
                                        cvHoursEngine2.ErrorMessage = "Minute entry must be between 0 and 59";
                                    }
                                    else
                                    {
                                        cvHoursEngine2.ErrorMessage = text;
                                    }
                                }
                                else
                                {
                                    cvHoursEngine2.ErrorMessage = text;
                                }

                            }
                            else
                            {
                                cvHoursEngine2.ErrorMessage = text;
                            }
                            tbHoursEngine2.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbHoursEngine3.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbHoursEngine3.Text))
                        {
                            cvHoursEngine3.IsValid = false;
                            if (tbHoursEngine3.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbHoursEngine3.Text.Split(':');
                                if (timeArray[1].Length < 2)
                                {
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        minute = Convert.ToInt16(timeArray[1]);
                                    }
                                    if (minute > 59)
                                    {
                                        cvHoursEngine3.ErrorMessage = "Minute entry must be between 0 and 59";
                                    }
                                    else
                                    {
                                        cvHoursEngine3.ErrorMessage = text;
                                    }
                                }
                                else
                                {
                                    cvHoursEngine3.ErrorMessage = text;
                                }

                            }
                            else
                            {
                                cvHoursEngine3.ErrorMessage = text;
                            }
                            tbHoursEngine3.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbHoursEngine4.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbHoursEngine4.Text))
                        {
                            cvHoursEngine4.IsValid = false;
                            if (tbHoursEngine4.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbHoursEngine4.Text.Split(':');
                                if (timeArray[1].Length < 2)
                                {
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        minute = Convert.ToInt16(timeArray[1]);
                                    }
                                    if (minute > 59)
                                    {
                                        cvHoursEngine4.ErrorMessage = "Minute entry must be between 0 and 59";
                                    }
                                    else
                                    {
                                        cvHoursEngine4.ErrorMessage = text;
                                    }
                                }
                                else
                                {
                                    cvHoursEngine4.ErrorMessage = text;
                                }

                            }
                            else
                            {
                                cvHoursEngine4.ErrorMessage = text;
                            }
                            tbHoursEngine4.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbReserver1hours.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbReserver1hours.Text))
                        {
                            cvReserver1hours.IsValid = false;
                            if (tbReserver1cycles.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbReserver1hours.Text.Split(':');
                                if (timeArray[1].Length < 2)
                                {
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        minute = Convert.ToInt16(timeArray[1]);
                                    }
                                    if (minute > 59)
                                    {
                                        cvReserver1hours.ErrorMessage = "Minute entry must be between 0 and 59";
                                    }
                                    else
                                    {
                                        cvReserver1hours.ErrorMessage = text;
                                    }
                                }
                                else
                                {
                                    cvReserver1hours.ErrorMessage = text;
                                }
                            }
                            else
                            {
                                cvReserver1hours.ErrorMessage = text;
                            }
                            tbReserver1hours.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbReserver2hours.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbReserver2hours.Text))
                        {
                            cvReserver2hours.IsValid = false;
                            if (tbReserver2hours.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbReserver2hours.Text.Split(':');
                                if (timeArray[1].Length < 2)
                                {
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        minute = Convert.ToInt16(timeArray[1]);
                                    }
                                    if (minute > 59)
                                    {
                                        cvReserver2hours.ErrorMessage = "Minute entry must be between 0 and 59";
                                    }
                                    else
                                    {
                                        cvReserver2hours.ErrorMessage = text;
                                    }
                                }
                                else
                                {
                                    cvReserver2hours.ErrorMessage = text;
                                }
                            }
                            else
                            {
                                cvReserver2hours.ErrorMessage = text;
                            }
                            tbReserver2hours.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbReserver3hours.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbReserver3hours.Text))
                        {
                            cvReserver3hours.IsValid = false;
                            if (tbReserver3hours.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbReserver3hours.Text.Split(':');
                                if (timeArray[1].Length < 2)
                                {
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        minute = Convert.ToInt16(timeArray[1]);
                                    }
                                    if (minute > 59)
                                    {
                                        cvReserver3hours.ErrorMessage = "Minute entry must be between 0 and 59";
                                    }
                                    else
                                    {
                                        cvReserver3hours.ErrorMessage = text;
                                    }
                                }
                                else
                                {
                                    cvReserver3hours.ErrorMessage = text;
                                }
                            }
                            else
                            {
                                cvReserver3hours.ErrorMessage = text;
                            }
                            tbReserver3hours.Focus();
                            IsValid = false;
                            IsCheck = false;

                        }
                    }
                    if ((!string.IsNullOrEmpty(tbReserver4hours.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbReserver4hours.Text))
                        {
                            cvReserver4hours.IsValid = false;
                            if (tbReserver4hours.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbReserver4hours.Text.Split(':');
                                if (timeArray[1].Length < 2)
                                {
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        minute = Convert.ToInt16(timeArray[1]);
                                    }

                                    if (minute > 59)
                                    {
                                        cvReserver4hours.ErrorMessage = "Minute entry must be between 0 and 59";
                                    }
                                    else
                                    {
                                        cvReserver4hours.ErrorMessage = text;
                                    }
                                }
                                {
                                    cvReserver4hours.ErrorMessage = text;
                                }
                            }
                            else
                            {
                                cvReserver4hours.ErrorMessage = text;
                            }
                            IsValid = false;
                            tbReserver4hours.Focus();
                        }
                    }
                }
            }

        }
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FlightPakMasterService.FleetInformation Service = new FlightPakMasterService.FleetInformation();

                            if (UserPrincipal.Identity != null)
                            {
                                // Display Time Fields based on Tenths / Minutes - Company Profile
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                                {
                                    string text = "NNNNNNN.N";
                                    string expression = "^[0-9]{0,7}(\\.[0-9]{0,1})?$";
                                    ValidateText(text, expression);
                                }
                                else
                                {
                                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    {
                                        string text = "HHHHHHH:MM";
                                        string expression = "^[0-9]{0,7}(:([0-5]{0,1}[0-9]{0,1}))?$";                                        
                                        ValidateText(text, expression);
                                    }
                                }
                            }
                            if (IsValid)
                            {
                                TextBox tbairframeasof = (TextBox)ucairframeasof.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(tbairframeasof.Text.Trim().ToString()))
                                {
                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (DateFormat != null)
                                    {
                                        Service.AIrframeAsOfDT = Convert.ToDateTime(FormatDate(((TextBox)tbairframeasof.FindControl("tbDate")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        Service.AIrframeAsOfDT = Convert.ToDateTime(((TextBox)tbairframeasof.FindControl("tbDate")).Text);
                                    }

                                    // Service.AIrframeAsOfDT = DateTime.ParseExact(tbairframeasof.Text, DateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    Service.AIrframeAsOfDT = null;

                                if (!string.IsNullOrEmpty(tbcyclesairframe.Text))
                                    Service.AirframeCycle = Convert.ToDecimal(tbcyclesairframe.Text);
                                else
                                    Service.AirframeCycle = 0;

                                int Msk = Convert.ToInt32(UserPrincipal.Identity._fpSettings._TimeDisplayTenMin);
                                if (Msk == 2)
                                {
                                    if (!string.IsNullOrEmpty(tbHoursAirFrame.Text))
                                    {
                                        var charExists = (tbHoursAirFrame.Text.IndexOf(':') >= 0) ? true : false;
                                        if (!charExists)
                                            tbHoursAirFrame.Text = tbHoursAirFrame.Text + ":00";
                                        Service.AirframeHrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbHoursAirFrame.Text, true, "GENERAL")), 3);
                                    }

                                    if (!string.IsNullOrEmpty(tbHoursEngine1.Text))
                                    {
                                        var charExists = (tbHoursEngine1.Text.IndexOf(':') >= 0) ? true : false;
                                        if (!charExists)
                                            tbHoursEngine1.Text = tbHoursEngine1.Text + ":00";
                                        Service.Engine1Hrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbHoursEngine1.Text, true, "GENERAL")), 3);
                                    }
                                    if (!string.IsNullOrEmpty(tbHoursEngine2.Text))
                                    {
                                        var charExists = (tbHoursEngine2.Text.IndexOf(':') >= 0) ? true : false;
                                        if (!charExists)
                                            tbHoursEngine2.Text = tbHoursEngine2.Text + ":00";
                                        Service.Engine2Hrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbHoursEngine2.Text, true, "GENERAL")), 3);
                                    }
                                    if (!string.IsNullOrEmpty(tbHoursEngine3.Text))
                                    {
                                        var charExists = (tbHoursEngine3.Text.IndexOf(':') >= 0) ? true : false;
                                        if (!charExists)
                                            tbHoursEngine3.Text = tbHoursEngine3.Text + ":00";
                                        Service.Engine3Hrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbHoursEngine3.Text, true, "GENERAL")), 3);
                                    }
                                    if (!string.IsNullOrEmpty(tbHoursEngine4.Text))
                                    {
                                        var charExists = (tbHoursEngine4.Text.IndexOf(':') >= 0) ? true : false;
                                        if (!charExists)
                                            tbHoursEngine4.Text = tbHoursEngine4.Text + ":00";
                                        Service.Engine4Hrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbHoursEngine4.Text, true, "GENERAL")), 3);
                                    }

                                    if (!string.IsNullOrEmpty(tbReserver1hours.Text))
                                    {
                                        var charExists = (tbReserver1hours.Text.IndexOf(':') >= 0) ? true : false;
                                        if (!charExists)
                                            tbReserver1hours.Text = tbReserver1hours.Text + ":00";
                                        Service.Reverse1Hrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbReserver1hours.Text, true, "GENERAL")), 3);
                                    }
                                    if (!string.IsNullOrEmpty(tbReserver2hours.Text))
                                    {
                                        var charExists = (tbReserver2hours.Text.IndexOf(':') >= 0) ? true : false;
                                        if (!charExists)
                                            tbReserver2hours.Text = tbReserver2hours.Text + ":00";
                                        Service.Reverse2Hrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbReserver2hours.Text, true, "GENERAL")), 3);
                                    }
                                    if (!string.IsNullOrEmpty(tbReserver3hours.Text))
                                    {
                                        var charExists = (tbReserver3hours.Text.IndexOf(':') >= 0) ? true : false;
                                        if (!charExists)
                                            tbReserver3hours.Text = tbReserver3hours.Text + ":00";
                                        Service.Reverse3Hrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbReserver3hours.Text, true, "GENERAL")), 3);
                                    }
                                    if (!string.IsNullOrEmpty(tbReserver4hours.Text))
                                    {
                                        var charExists = (tbReserver4hours.Text.IndexOf(':') >= 0) ? true : false;
                                        if (!charExists)
                                            tbReserver4hours.Text = tbReserver4hours.Text + ":00";
                                        Service.Reverse4Hrs = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbReserver4hours.Text, true, "GENERAL")), 3);
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(tbHoursAirFrame.Text))
                                        Service.AirframeHrs = (Convert.ToDecimal(tbHoursAirFrame.Text));
                                    if (!string.IsNullOrEmpty(tbHoursEngine1.Text))
                                        Service.Engine1Hrs = Convert.ToDecimal(tbHoursEngine1.Text);
                                    if (!string.IsNullOrEmpty(tbHoursEngine2.Text))
                                        Service.Engine2Hrs = Convert.ToDecimal(tbHoursEngine2.Text);
                                    if (!string.IsNullOrEmpty(tbHoursEngine3.Text))
                                        Service.Engine3Hrs = Convert.ToDecimal(tbHoursEngine3.Text);
                                    if (!string.IsNullOrEmpty(tbHoursEngine4.Text))
                                        Service.Engine4Hrs = Convert.ToDecimal(tbHoursEngine4.Text);

                                    if (!string.IsNullOrEmpty(tbReserver1hours.Text))
                                        Service.Reverse1Hrs = Convert.ToDecimal(tbReserver1hours.Text);
                                    if (!string.IsNullOrEmpty(tbReserver2hours.Text))
                                        Service.Reverse2Hrs = Convert.ToDecimal(tbReserver2hours.Text);
                                    if (!string.IsNullOrEmpty(tbReserver3hours.Text))
                                        Service.Reverse3Hrs = Convert.ToDecimal(tbReserver3hours.Text);
                                    if (!string.IsNullOrEmpty(tbReserver4hours.Text))
                                        Service.Reverse4Hrs = Convert.ToDecimal(tbReserver4hours.Text);
                                }


                                TextBox tbasofengine1 = (TextBox)ucasofengine1.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(tbasofengine1.Text.Trim().ToString()))
                                {
                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (DateFormat != null)
                                    {
                                        Service.Engine1AsOfDT = Convert.ToDateTime(FormatDate(((TextBox)ucasofengine1.FindControl("tbDate")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        Service.Engine1AsOfDT = Convert.ToDateTime(((TextBox)ucasofengine1.FindControl("tbDate")).Text);
                                    }
                                    // Service.Engine1AsOfDT = DateTime.ParseExact(tbasofengine1.Text, DateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    Service.Engine1AsOfDT = null;

                                if (!string.IsNullOrEmpty(tbcyclesengine1.Text))
                                    Service.Engine1Cycle = Convert.ToDecimal(tbcyclesengine1.Text);
                                else
                                    Service.Engine1Cycle = 0;




                                TextBox tbasofengine2 = (TextBox)ucasofengine2.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(tbasofengine2.Text.Trim().ToString()))
                                {
                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (DateFormat != null)
                                    {
                                        Service.Engine2AsOfDT = Convert.ToDateTime(FormatDate(((TextBox)tbasofengine2.FindControl("tbDate")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        Service.Engine2AsOfDT = Convert.ToDateTime(((TextBox)tbasofengine2.FindControl("tbDate")).Text);
                                    }
                                    // Service.Engine2AsOfDT = DateTime.ParseExact(tbasofengine2.Text, DateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    Service.Engine2AsOfDT = null;


                                if (!string.IsNullOrEmpty(tbcyclesengine2.Text))
                                    Service.Engine2Cycle = Convert.ToDecimal(tbcyclesengine2.Text);



                                TextBox tbasofengine3 = (TextBox)ucasofengine3.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(tbasofengine3.Text.Trim().ToString()))
                                {
                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (DateFormat != null)
                                    {
                                        Service.Engine3AsOfDT =Convert.ToDateTime(FormatDate(((TextBox)tbasofengine3.FindControl("tbDate")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        Service.Engine3AsOfDT = Convert.ToDateTime(((TextBox)tbasofengine3.FindControl("tbDate")).Text);
                                    }
                                    //Service.Engine3AsOfDT = DateTime.ParseExact(tbasofengine3.Text, DateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    Service.Engine3AsOfDT = null;


                                if (!string.IsNullOrEmpty(tbcyclesengine3.Text))
                                    Service.Engine3Cycle = Convert.ToDecimal(tbcyclesengine3.Text);
                                else
                                    Service.Engine3Cycle = 0;



                                TextBox tbasofengine4 = (TextBox)ucasofengine4.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(tbasofengine4.Text.Trim().ToString()))
                                {
                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (DateFormat != null)
                                    {
                                        Service.Engine4AsOfDT = Convert.ToDateTime(FormatDate(((TextBox)tbasofengine4.FindControl("tbDate")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        Service.Engine4AsOfDT = Convert.ToDateTime(((TextBox)tbasofengine4.FindControl("tbDate")).Text);
                                    }
                                    // Service.Engine4AsOfDT = DateTime.ParseExact(tbasofengine4.Text, DateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    Service.Engine4AsOfDT = null;

                                if (!string.IsNullOrEmpty(tbcyclesengine4.Text))
                                    Service.Engine4Cycle = Convert.ToDecimal(tbcyclesengine4.Text);
                                else
                                    Service.Engine4Cycle = 0;



                                if (!string.IsNullOrEmpty(tb1stthhr.Text))
                                    Service.Fuel1Hrs = Convert.ToDecimal(tb1stthhr.Text);
                                else
                                    Service.Fuel1Hrs = 0;

                                if (!string.IsNullOrEmpty(tb2ndhhr.Text))
                                    Service.Fuel2Hrs = Convert.ToDecimal(tb2ndhhr.Text);
                                else
                                    Service.Fuel2Hrs = 0;

                                if (!string.IsNullOrEmpty(tb3rdhr.Text))
                                    Service.Fuel3Hrs = Convert.ToDecimal(tb3rdhr.Text);
                                else
                                    Service.Fuel3Hrs = 0;

                                if (!string.IsNullOrEmpty(tb3rdhr.Text))
                                    Service.Fuel4Hrs = Convert.ToDecimal(tb4thhr.Text);
                                else
                                    Service.Fuel4Hrs = 0;

                                if (!string.IsNullOrEmpty(tb5thhr.Text))
                                    Service.Fuel5Hrs = Convert.ToDecimal(tb5thhr.Text);
                                else
                                    Service.Fuel5Hrs = 0;


                                #region High Speed Fuel

                                if (!string.IsNullOrEmpty(tbhsc1sthr.Text))
                                    Service.HighSpeedFuel1Hrs = Convert.ToDecimal(tbhsc1sthr.Text);
                                else
                                    Service.HighSpeedFuel1Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc2ndhr.Text))
                                    Service.HighSpeedFuel2Hrs = Convert.ToDecimal(tbhsc2ndhr.Text);
                                else
                                    Service.HighSpeedFuel2Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc3rdhr.Text))
                                    Service.HighSpeedFuel3Hrs = Convert.ToDecimal(tbhsc3rdhr.Text);
                                else
                                    Service.HighSpeedFuel3Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc4thhr.Text))
                                    Service.HighSpeedFuel4Hrs = Convert.ToDecimal(tbhsc4thhr.Text);
                                else
                                    Service.HighSpeedFuel4Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc5thhr.Text))
                                    Service.HighSpeedFuel5Hrs = Convert.ToDecimal(tbhsc5thhr.Text);
                                else
                                    Service.HighSpeedFuel5Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc6thhr.Text))
                                    Service.HighSpeedFuel6Hrs = Convert.ToDecimal(tbhsc6thhr.Text);
                                else
                                    Service.HighSpeedFuel6Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc7thhr.Text))
                                    Service.HighSpeedFuel7Hrs = Convert.ToDecimal(tbhsc7thhr.Text);
                                else
                                    Service.HighSpeedFuel7Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc8thhr.Text))
                                    Service.HighSpeedFuel8Hrs = Convert.ToDecimal(tbhsc8thhr.Text);
                                else
                                    Service.HighSpeedFuel8Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc9thhr.Text))
                                    Service.HighSpeedFuel9Hrs = Convert.ToDecimal(tbhsc9thhr.Text);
                                else
                                    Service.HighSpeedFuel9Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc10thhr.Text))
                                    Service.HighSpeedFuel10Hrs = Convert.ToDecimal(tbhsc10thhr.Text);
                                else
                                    Service.HighSpeedFuel10Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc11thhr.Text))
                                    Service.HighSpeedFuel11Hrs = Convert.ToDecimal(tbhsc11thhr.Text);
                                else
                                    Service.HighSpeedFuel11Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc12thhr.Text))
                                    Service.HighSpeedFuel12Hrs = Convert.ToDecimal(tbhsc12thhr.Text);
                                else
                                    Service.HighSpeedFuel12Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc13thhr.Text))
                                    Service.HighSpeedFuel13Hrs = Convert.ToDecimal(tbhsc13thhr.Text);
                                else
                                    Service.HighSpeedFuel13Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc14thhr.Text))
                                    Service.HighSpeedFuel14Hrs = Convert.ToDecimal(tbhsc14thhr.Text);
                                else
                                    Service.HighSpeedFuel14Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc15thhr.Text))
                                    Service.HighSpeedFuel51Hrs = Convert.ToDecimal(tbhsc15thhr.Text);
                                else
                                    Service.HighSpeedFuel51Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc16thhr.Text))
                                    Service.HighSpeedFuel16Hrs = Convert.ToDecimal(tbhsc16thhr.Text);
                                else
                                    Service.HighSpeedFuel16Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc17thhr.Text))
                                    Service.HighSpeedFuel17Hrs = Convert.ToDecimal(tbhsc17thhr.Text);
                                else
                                    Service.HighSpeedFuel17Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc18thhr.Text))
                                    Service.HighSpeedFuel18Hrs = Convert.ToDecimal(tbhsc18thhr.Text);
                                else
                                    Service.HighSpeedFuel18Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc19thhr.Text))
                                    Service.HighSpeedFuel19Hrs = Convert.ToDecimal(tbhsc19thhr.Text);
                                else
                                    Service.HighSpeedFuel19Hrs = 0;

                                if (!string.IsNullOrEmpty(tbhsc20thhr.Text))
                                    Service.HighSpeedFuel20Hrs = Convert.ToDecimal(tbhsc20thhr.Text);
                                else
                                    Service.HighSpeedFuel20Hrs = 0;
                                
                                #endregion High Speed Fuel

                                #region Long Range Fuel
                                if (!string.IsNullOrEmpty(tblrc1sthr.Text))
                                    Service.LongRangeFuel1Hrs = Convert.ToDecimal(tblrc1sthr.Text);
                                else
                                    Service.LongRangeFuel1Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc2ndhr.Text))
                                    Service.LongRangeFuel2Hrs = Convert.ToDecimal(tblrc2ndhr.Text);
                                else
                                    Service.LongRangeFuel2Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc3rdhr.Text))
                                    Service.LongRangeFuel3Hrs = Convert.ToDecimal(tblrc3rdhr.Text);
                                else
                                    Service.LongRangeFuel3Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc4thr.Text))
                                    Service.LongRangeFuel4Hrs = Convert.ToDecimal(tblrc4thr.Text);
                                else
                                    Service.LongRangeFuel4Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc5thr.Text))
                                    Service.LongRangeFuel5Hrs = Convert.ToDecimal(tblrc5thr.Text);
                                else
                                    Service.LongRangeFuel5Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc6thr.Text))
                                    Service.LongRangeFuel6Hrs = Convert.ToDecimal(tblrc6thr.Text);
                                else
                                    Service.LongRangeFuel6Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc7thr.Text))
                                    Service.LongRangeFuel7Hrs = Convert.ToDecimal(tblrc7thr.Text);
                                else
                                    Service.LongRangeFuel7Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc8thr.Text))
                                    Service.LongRangeFuel8Hrs = Convert.ToDecimal(tblrc8thr.Text);
                                else
                                    Service.LongRangeFuel8Hrs = 0;
                                if (!string.IsNullOrEmpty(tblrc9thr.Text))
                                    Service.LongRangeFuel9Hrs = Convert.ToDecimal(tblrc9thr.Text);
                                else
                                    Service.LongRangeFuel9Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc10thr.Text))
                                    Service.LongRangeFuel10Hrs = Convert.ToDecimal(tblrc10thr.Text);
                                else
                                    Service.LongRangeFuel10Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc11thr.Text))
                                    Service.LongRangeFuel11Hrs = Convert.ToDecimal(tblrc11thr.Text);
                                else
                                    Service.LongRangeFuel11Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc12thr.Text))
                                    Service.LongRangeFuel12Hrs = Convert.ToDecimal(tblrc12thr.Text);
                                else
                                    Service.LongRangeFuel12Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc13thr.Text))
                                    Service.LongRangeFuel13Hrs = Convert.ToDecimal(tblrc13thr.Text);
                                else
                                    Service.LongRangeFuel13Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc14thr.Text))
                                    Service.LongRangeFuel14Hrs = Convert.ToDecimal(tblrc14thr.Text);
                                else
                                    Service.LongRangeFuel14Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc15thr.Text))
                                    Service.LongRangeFuel15Hrs = Convert.ToDecimal(tblrc15thr.Text);
                                else
                                    Service.LongRangeFuel15Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc16thr.Text))
                                    Service.LongRangeFuel16Hrs = Convert.ToDecimal(tblrc16thr.Text);
                                else
                                    Service.LongRangeFuel16Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc17thr.Text))
                                    Service.LongRangeFuel17Hrs = Convert.ToDecimal(tblrc17thr.Text);
                                else
                                    Service.LongRangeFuel17Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc18thr.Text))
                                    Service.LongRangeFuel18Hrs = Convert.ToDecimal(tblrc18thr.Text);
                                else
                                    Service.LongRangeFuel18Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc19thr.Text))
                                    Service.LongRangeFuel19Hrs = Convert.ToDecimal(tblrc19thr.Text);
                                else
                                    Service.LongRangeFuel19Hrs = 0;

                                if (!string.IsNullOrEmpty(tblrc20thr.Text))
                                    Service.LongRangeFuel20Hrs = Convert.ToDecimal(tblrc20thr.Text);
                                else
                                    Service.LongRangeFuel20Hrs = 0;

                                #endregion Long Range Fuel

                                #region MACH Fuel
                                if (!string.IsNullOrEmpty(tb1stthhr.Text))
                                    Service.MACHFuel1Hrs = Convert.ToDecimal(tb1stthhr.Text);
                                else
                                    Service.MACHFuel1Hrs = 0;

                                if (!string.IsNullOrEmpty(tb2ndhhr.Text))
                                    Service.MACHFuel2Hrs = Convert.ToDecimal(tb2ndhhr.Text);
                                else
                                    Service.MACHFuel2Hrs = 0;

                                if (!string.IsNullOrEmpty(tb3rdhr.Text))
                                    Service.MACHFuel3Hrs = Convert.ToDecimal(tb3rdhr.Text);
                                else
                                    Service.MACHFuel3Hrs = 0;

                                if (!string.IsNullOrEmpty(tb4thhr.Text))
                                    Service.MACHFuel4Hrs = Convert.ToDecimal(tb4thhr.Text);
                                else
                                    Service.MACHFuel4Hrs = 0;

                                if (!string.IsNullOrEmpty(tb5thhr.Text))
                                    Service.MACHFuel5Hrs = Convert.ToDecimal(tb5thhr.Text);
                                else
                                    Service.MACHFuel5Hrs = 0;

                                if (!string.IsNullOrEmpty(tb6thhr.Text))
                                    Service.MACHFuel6Hrs = Convert.ToDecimal(tb6thhr.Text);
                                else
                                    Service.MACHFuel6Hrs = 0;

                                if (!string.IsNullOrEmpty(tb7thhr.Text))
                                    Service.MACHFuel7Hrs = Convert.ToDecimal(tb7thhr.Text);
                                else
                                    Service.MACHFuel7Hrs = 0;

                                if (!string.IsNullOrEmpty(tb8thhr.Text))
                                    Service.MACHFuel8Hrs = Convert.ToDecimal(tb8thhr.Text);
                                else
                                    Service.MACHFuel8Hrs = 0;

                                if (!string.IsNullOrEmpty(tb8thhr.Text))
                                    Service.MACHFuel9Hrs = Convert.ToDecimal(tb9thhr.Text);
                                else
                                    Service.MACHFuel9Hrs = 0;

                                if (!string.IsNullOrEmpty(tb10thhr.Text))
                                    Service.MACHFuel10Hrs = Convert.ToDecimal(tb10thhr.Text);
                                else
                                    Service.MACHFuel10Hrs = 0;

                                if (!string.IsNullOrEmpty(tb11thhr.Text))
                                    Service.MACHFuel11Hrs = Convert.ToDecimal(tb11thhr.Text);
                                else
                                    Service.MACHFuel11Hrs = 0;

                                if (!string.IsNullOrEmpty(tb12thhr.Text))
                                    Service.MACHFuel12Hrs = Convert.ToDecimal(tb12thhr.Text);
                                else
                                    Service.MACHFuel12Hrs = 0;

                                if (!string.IsNullOrEmpty(tb13thhr.Text))
                                    Service.MACHFuel13Hrs = Convert.ToDecimal(tb13thhr.Text);
                                else
                                    Service.MACHFuel13Hrs = 0;

                                if (!string.IsNullOrEmpty(tb14thhr.Text))
                                    Service.MACHFuel14Hrs = Convert.ToDecimal(tb14thhr.Text);
                                else
                                    Service.MACHFuel14Hrs = 0;

                                if (!string.IsNullOrEmpty(tb15thhr.Text))
                                    Service.MACHFuel15Hrs = Convert.ToDecimal(tb15thhr.Text);
                                else
                                    Service.MACHFuel15Hrs = 0;

                                if (!string.IsNullOrEmpty(tb16thhr.Text))
                                    Service.MACHFuel16Hrs = Convert.ToDecimal(tb16thhr.Text);
                                else
                                    Service.MACHFuel16Hrs = 0;

                                if (!string.IsNullOrEmpty(tb17thhr.Text))
                                    Service.MACHFuel17Hrs = Convert.ToDecimal(tb17thhr.Text);
                                else
                                    Service.MACHFuel17Hrs = 0;

                                if (!string.IsNullOrEmpty(tb18thhr.Text))
                                    Service.MACHFuel18Hrs = Convert.ToDecimal(tb18thhr.Text);
                                else
                                    Service.MACHFuel18Hrs = 0;

                                if (!string.IsNullOrEmpty(tb19thhr.Text))
                                    Service.MACHFuel19Hrs = Convert.ToDecimal(tb19thhr.Text);
                                else
                                    Service.MACHFuel19Hrs = 0;

                                if (!string.IsNullOrEmpty(tb20thhr.Text))
                                    Service.MACHFuel20Hrs = Convert.ToDecimal(tb20thhr.Text);
                                else
                                    Service.MACHFuel20Hrs = 0;

                                #endregion MACH Fuel

                                Service.IsDeleted = false;

                                TextBox tbReserver1asof = (TextBox)ucreverser1asof.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(tbReserver1asof.Text.Trim().ToString()))
                                {
                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (DateFormat != null)
                                    {
                                        Service.Reverser1AsOfDT = Convert.ToDateTime(FormatDate(((TextBox)tbReserver1asof.FindControl("tbDate")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        Service.Reverser1AsOfDT = Convert.ToDateTime(((TextBox)tbReserver1asof.FindControl("tbDate")).Text);
                                    }
                                    //Service.Reverser1AsOfDT = DateTime.ParseExact(tbReserver1asof.Text, DateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    Service.Reverser1AsOfDT = null;


                                if (!string.IsNullOrEmpty(tbReserver1cycles.Text))
                                    Service.Reverser1Cycle = Convert.ToDecimal(tbReserver1cycles.Text);
                                else
                                    Service.Reverser1Cycle = 0;

                                TextBox tbReserver2asof = (TextBox)ucreverser2asof.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(tbReserver2asof.Text.Trim().ToString()))
                                {
                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (DateFormat != null)
                                    {
                                        Service.Reverser2AsOfDT = Convert.ToDateTime(FormatDate(((TextBox)tbReserver2asof.FindControl("tbDate")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        Service.Reverser2AsOfDT = Convert.ToDateTime(((TextBox)tbReserver2asof.FindControl("tbDate")).Text);
                                    }
                                    // Service.Reverser2AsOfDT = DateTime.ParseExact(tbReserver2asof.Text, DateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    Service.Reverser2AsOfDT = null;

                                if (!string.IsNullOrEmpty(tbReserver2cycles.Text))
                                    Service.Reverser2Cycle = Convert.ToDecimal(tbReserver2cycles.Text);
                                else
                                    Service.Reverser2Cycle = 0;

                                TextBox tbReserver3asof = (TextBox)ucreverser3asof.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(tbReserver3asof.Text.Trim().ToString()))
                                {
                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (DateFormat != null)
                                    {
                                        Service.Reverser3AsOfDT = Convert.ToDateTime(FormatDate(((TextBox)tbReserver3asof.FindControl("tbDate")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        Service.Reverser3AsOfDT = Convert.ToDateTime(((TextBox)tbReserver3asof.FindControl("tbDate")).Text);
                                    }
                                    // Service.Reverser3AsOfDT = DateTime.ParseExact(tbReserver3asof.Text, DateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    Service.Reverser3AsOfDT = null;

                                if (!string.IsNullOrEmpty(tbReserver3cycles.Text))
                                    Service.Reverser3Cycle = Convert.ToDecimal(tbReserver3cycles.Text);
                                else
                                    Service.Reverser3Cycle = 0;

                                TextBox tbReserver4asof = (TextBox)ucreverser4asof.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(tbReserver4asof.Text.Trim().ToString()))
                                {
                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (DateFormat != null)
                                    {
                                        Service.Reverser4AsOfDT = Convert.ToDateTime(FormatDate(((TextBox)tbReserver4asof.FindControl("tbDate")).Text, DateFormat));
                                    }
                                    else
                                    {
                                        Service.Reverser4AsOfDT = Convert.ToDateTime(((TextBox)tbReserver4asof.FindControl("tbDate")).Text);
                                    }
                                    // Service.Reverser4AsOfDT = DateTime.ParseExact(tbReserver4asof.Text, DateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    Service.Reverser4AsOfDT = null;

                                if (!string.IsNullOrEmpty(tbReserver4cycles.Text))
                                    Service.Reverser4Cycle = Convert.ToDecimal(tbReserver4cycles.Text);
                                else
                                    Service.Reverser4Cycle = 0;

                                Service.FleetID = Convert.ToInt64(ViewState["FleetID"].ToString());
                                ReturnValueOfFleetInformation Result;
                                if (hdnMode.Value.ToLower() == "edit")
                                {
                                    Service.FleetInformationID = Convert.ToInt64(hdnPkeyId.Value);
                                    Result = objService.UpdateFleetInformation(Service);
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.FleetProfile, Convert.ToInt64(Request.QueryString["FleetID"].ToString().Trim()));
                                    }
                                }
                                else
                                {
                                    Result = objService.AddFleetInformationType(Service);
                                }

                                btnsavechanges.Enabled = false;
                                btnsavechanges.CssClass = "button-disable";
                                EnableForm(false);
                                if (Result.ReturnFlag == true)
                                {
                                    //ShowSuccessMessage();
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowSuccessMessage", "ShowSuccessMessage();", true);
                                }
                                if (ViewState["FleetID"] != null)
                                {
                                    LoadData(Convert.ToInt64(ViewState["FleetID"].ToString()));
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

        }


        private void LoadData(Int64 FleetInformationID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetInformationID))
            {

                hdnMode.Value = "";
                hdnPkeyId.Value = "0";
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ReturnValue = Service.GetFleetInformation(FleetInformationID).EntityList.FirstOrDefault();
                    if (ReturnValue != null)
                    {

                        TextBox tbairframeasof = (TextBox)ucairframeasof.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(ReturnValue.AIrframeAsOfDT.ToString()))
                        {
                            ((TextBox)tbairframeasof.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(ReturnValue.AIrframeAsOfDT));
                            //  tbairframeasof.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ReturnValue.AIrframeAsOfDT);
                        }
                        else
                        {
                            tbairframeasof.Text = string.Empty;
                        }
                        // tbairframeasof.Text = ReturnValue.AIrframeAsOfDT.ToString();


                        tbcyclesairframe.Text = ReturnValue.AirframeCycle.ToString();


                        TextBox tbasofengine1 = (TextBox)ucasofengine1.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(ReturnValue.Engine1AsOfDT.ToString()))
                        {
                            ((TextBox)tbasofengine1.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(ReturnValue.Engine1AsOfDT));
                            // tbasofengine1.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ReturnValue.Engine1AsOfDT);
                        }
                        else
                        {
                            tbasofengine1.Text = string.Empty;
                        }
                        // tbasofengine1.Text = ReturnValue.Engine1AsOfDT.ToString();

                        tbcyclesengine1.Text = ReturnValue.Engine1Cycle.ToString();


                        TextBox tbasofengine2 = (TextBox)ucasofengine2.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(ReturnValue.Engine2AsOfDT.ToString()))
                        {
                            ((TextBox)tbasofengine2.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(ReturnValue.Engine2AsOfDT));
                            // tbasofengine2.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ReturnValue.Engine2AsOfDT);
                        }
                        else
                        {
                            tbasofengine2.Text = string.Empty;
                        }
                        // tbasofengine2.Text = ReturnValue.Engine2AsOfDT.ToString();

                        tbcyclesengine2.Text = ReturnValue.Engine2Cycle.ToString();

                        TextBox tbasofengine3 = (TextBox)ucasofengine3.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(ReturnValue.Engine3AsOfDT.ToString()))
                        {
                            //  tbasofengine3.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ReturnValue.Engine3AsOfDT);
                            ((TextBox)tbasofengine3.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(ReturnValue.Engine3AsOfDT));
                        }
                        else
                        {
                            tbasofengine3.Text = string.Empty;
                        }
                        //tbasofengine3.Text = ReturnValue.Engine3AsOfDT.ToString();

                        tbcyclesengine3.Text = ReturnValue.Engine3Cycle.ToString();


                        TextBox tbasofengine4 = (TextBox)ucasofengine4.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(ReturnValue.Engine4AsOfDT.ToString()))
                        {
                            ((TextBox)tbasofengine4.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(ReturnValue.Engine4AsOfDT));
                            // tbasofengine4.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ReturnValue.Engine4AsOfDT);
                        }
                        else
                        {
                            tbasofengine4.Text = string.Empty;
                        }
                        //tbasofengine4.Text = ReturnValue.Engine4AsOfDT.ToString();

                        tbcyclesengine4.Text = ReturnValue.Engine4Cycle.ToString();

                        #region Fuel Burn
                        //tb1stthhr.Text = ReturnValue.Fuel1Hrs.ToString();

                        //tb2ndhhr.Text = ReturnValue.Fuel2Hrs.ToString();

                        //tb3rdhr.Text = ReturnValue.Fuel3Hrs.ToString();

                        //tb4thhr.Text = ReturnValue.Fuel4Hrs.ToString();
                        #region High Speed Fuel

                        tbhsc1sthr.Text = ReturnValue.HighSpeedFuel1Hrs.ToString();

                        tbhsc2ndhr.Text = ReturnValue.HighSpeedFuel2Hrs.ToString();

                        tbhsc3rdhr.Text = ReturnValue.HighSpeedFuel3Hrs.ToString();

                        tbhsc4thhr.Text = ReturnValue.HighSpeedFuel4Hrs.ToString();

                        tbhsc5thhr.Text = ReturnValue.HighSpeedFuel5Hrs.ToString();

                        tbhsc6thhr.Text = ReturnValue.HighSpeedFuel6Hrs.ToString();

                        tbhsc7thhr.Text = ReturnValue.HighSpeedFuel7Hrs.ToString();

                        tbhsc8thhr.Text = ReturnValue.HighSpeedFuel8Hrs.ToString();

                        tbhsc9thhr.Text = ReturnValue.HighSpeedFuel9Hrs.ToString();
                        
                        tbhsc10thhr.Text = ReturnValue.HighSpeedFuel10Hrs.ToString();

                        tbhsc11thhr.Text = ReturnValue.HighSpeedFuel11Hrs.ToString();

                        tbhsc12thhr.Text = ReturnValue.HighSpeedFuel12Hrs.ToString();

                        tbhsc13thhr.Text = ReturnValue.HighSpeedFuel13Hrs.ToString();

                        tbhsc14thhr.Text = ReturnValue.HighSpeedFuel14Hrs.ToString();

                        tbhsc15thhr.Text = ReturnValue.HighSpeedFuel51Hrs.ToString();

                        tbhsc16thhr.Text = ReturnValue.HighSpeedFuel16Hrs.ToString();

                        tbhsc17thhr.Text = ReturnValue.HighSpeedFuel17Hrs.ToString();

                        tbhsc18thhr.Text = ReturnValue.HighSpeedFuel18Hrs.ToString();

                        tbhsc19thhr.Text = ReturnValue.HighSpeedFuel19Hrs.ToString();

                        tbhsc20thhr.Text = ReturnValue.HighSpeedFuel20Hrs.ToString();
                        #endregion High Speed Fuel

                        #region Long Range Fuel

                        tblrc1sthr.Text = ReturnValue.LongRangeFuel1Hrs.ToString();

                        tblrc2ndhr.Text = ReturnValue.LongRangeFuel2Hrs.ToString();

                        tblrc3rdhr.Text = ReturnValue.LongRangeFuel3Hrs.ToString();

                        tblrc4thr.Text = ReturnValue.LongRangeFuel4Hrs.ToString();

                        tblrc5thr.Text = ReturnValue.LongRangeFuel5Hrs.ToString();

                        tblrc6thr.Text = ReturnValue.LongRangeFuel6Hrs.ToString();

                        tblrc7thr.Text = ReturnValue.LongRangeFuel7Hrs.ToString();

                        tblrc8thr.Text = ReturnValue.LongRangeFuel8Hrs.ToString();

                        tblrc9thr.Text = ReturnValue.LongRangeFuel9Hrs.ToString();

                        tblrc10thr.Text = ReturnValue.LongRangeFuel10Hrs.ToString();

                        tblrc11thr.Text = ReturnValue.LongRangeFuel11Hrs.ToString();

                        tblrc12thr.Text = ReturnValue.LongRangeFuel12Hrs.ToString();

                        tblrc13thr.Text = ReturnValue.LongRangeFuel13Hrs.ToString();

                        tblrc14thr.Text = ReturnValue.LongRangeFuel14Hrs.ToString();

                        tblrc15thr.Text = ReturnValue.LongRangeFuel15Hrs.ToString();

                        tblrc16thr.Text = ReturnValue.LongRangeFuel16Hrs.ToString();

                        tblrc17thr.Text = ReturnValue.LongRangeFuel17Hrs.ToString();

                        tblrc18thr.Text = ReturnValue.LongRangeFuel18Hrs.ToString();

                        tblrc19thr.Text = ReturnValue.LongRangeFuel19Hrs.ToString();

                        tblrc20thr.Text = ReturnValue.LongRangeFuel20Hrs.ToString();

                        #endregion Long Range Fuel

                        #region Mach Fuel

                        tb1stthhr.Text = ReturnValue.MACHFuel1Hrs.ToString();

                        tb2ndhhr.Text = ReturnValue.MACHFuel2Hrs.ToString();

                        tb3rdhr.Text = ReturnValue.MACHFuel3Hrs.ToString();

                        tb4thhr.Text = ReturnValue.MACHFuel4Hrs.ToString();

                        tb5thhr.Text = ReturnValue.MACHFuel5Hrs.ToString();

                        tb6thhr.Text = ReturnValue.MACHFuel6Hrs.ToString();

                        tb7thhr.Text = ReturnValue.MACHFuel7Hrs.ToString();

                        tb8thhr.Text = ReturnValue.MACHFuel8Hrs.ToString();

                        tb9thhr.Text = ReturnValue.MACHFuel9Hrs.ToString();

                        tb10thhr.Text = ReturnValue.MACHFuel10Hrs.ToString();

                        tb11thhr.Text = ReturnValue.MACHFuel11Hrs.ToString();

                        tb12thhr.Text = ReturnValue.MACHFuel12Hrs.ToString();

                        tb13thhr.Text = ReturnValue.MACHFuel13Hrs.ToString();

                        tb14thhr.Text = ReturnValue.MACHFuel14Hrs.ToString();

                        tb15thhr.Text = ReturnValue.MACHFuel15Hrs.ToString();

                        tb16thhr.Text = ReturnValue.MACHFuel16Hrs.ToString();

                        tb17thhr.Text = ReturnValue.MACHFuel17Hrs.ToString();

                        tb18thhr.Text = ReturnValue.MACHFuel18Hrs.ToString();

                        tb19thhr.Text = ReturnValue.MACHFuel19Hrs.ToString();

                        tb20thhr.Text = ReturnValue.MACHFuel20Hrs.ToString();

                        #endregion Mach Fuel

                        #endregion Fuel Burn

                        TextBox tbReserver1asof = (TextBox)ucreverser1asof.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(ReturnValue.Reverser1AsOfDT.ToString()))
                        {
                            ((TextBox)tbReserver1asof.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(ReturnValue.Reverser1AsOfDT));
                            // tbReserver1asof.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ReturnValue.Reverser1AsOfDT);
                        }
                        else
                        {
                            tbReserver1asof.Text = string.Empty;
                        }
                        // tbReserver1asof.Text = ReturnValue.Reverser1AsOfDT.ToString();

                        tbReserver1cycles.Text = ReturnValue.Reverser1Cycle.ToString();


                        TextBox tbReserver2asof = (TextBox)ucreverser2asof.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(ReturnValue.Reverser2AsOfDT.ToString()))
                        {
                            ((TextBox)tbReserver2asof.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(ReturnValue.Reverser2AsOfDT));
                            //    tbReserver2asof.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ReturnValue.Reverser2AsOfDT);
                        }
                        else
                        {
                            tbReserver2asof.Text = string.Empty;
                        }
                        //tbReserver2asof.Text = ReturnValue.Reverser2AsOfDT.ToString();

                        tbReserver2cycles.Text = ReturnValue.Reverser2Cycle.ToString();

                        TextBox tbReserver3asof = (TextBox)ucreverser3asof.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(ReturnValue.Reverser3AsOfDT.ToString()))
                        {
                            ((TextBox)tbReserver3asof.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(ReturnValue.Reverser3AsOfDT));
                            //  tbReserver3asof.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ReturnValue.Reverser3AsOfDT);
                        }
                        else
                        {
                            tbReserver3asof.Text = string.Empty;
                        }
                        //tbReserver3asof.Text = ReturnValue.Reverser3AsOfDT.ToString();

                        tbReserver3cycles.Text = ReturnValue.Reverser3Cycle.ToString();

                        TextBox tbReserver4asof = (TextBox)ucreverser4asof.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(ReturnValue.Reverser4AsOfDT.ToString()))
                        {
                            ((TextBox)tbReserver4asof.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(ReturnValue.Reverser4AsOfDT));
                            //  tbReserver4asof.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ReturnValue.Reverser4AsOfDT);
                        }
                        else
                        {
                            tbReserver4asof.Text = string.Empty;
                        }
                        //tbReserver4asof.Text = ReturnValue.Reverser4AsOfDT.ToString();

                        tbReserver4cycles.Text = ReturnValue.Reverser4Cycle.ToString();

                        int Msk = Convert.ToInt32(UserPrincipal.Identity._fpSettings._TimeDisplayTenMin);
                        TimeDisplay();
                        if (Msk == 2)
                        {
                            if (ReturnValue.AirframeHrs != null && Convert.ToDecimal(ReturnValue.AirframeHrs) > 0)
                            {
                                tbHoursAirFrame.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(ReturnValue.AirframeHrs), 3).ToString());
                            }
                            else
                            {
                                tbHoursAirFrame.Text = "0:00";
                            }
                            if (ReturnValue.Engine1Hrs != null && Convert.ToDecimal(ReturnValue.Engine1Hrs) > 0)
                            {
                                tbHoursEngine1.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(ReturnValue.Engine1Hrs), 3).ToString());
                            }
                            else
                            {
                                tbHoursEngine1.Text = "0:00";
                            }
                            if (ReturnValue.Engine2Hrs != null && Convert.ToDecimal(ReturnValue.Engine2Hrs) > 0)
                            {
                                tbHoursEngine2.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(ReturnValue.Engine2Hrs), 3).ToString());
                            }
                            else
                            {
                                tbHoursEngine2.Text = "0:00";
                            }
                            if (ReturnValue.Engine3Hrs != null && Convert.ToDecimal(ReturnValue.Engine3Hrs) > 0)
                            {
                                tbHoursEngine3.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(ReturnValue.Engine3Hrs), 3).ToString());
                            }
                            else
                            {
                                tbHoursEngine3.Text = "0:00";
                            }
                            if (ReturnValue.Engine4Hrs != null && Convert.ToDecimal(ReturnValue.Engine4Hrs) > 0)
                            {
                                tbHoursEngine4.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(ReturnValue.Engine4Hrs), 3).ToString());
                            }
                            else
                            {
                                tbHoursEngine4.Text = "0:00";
                            }
                            if (ReturnValue.Reverse1Hrs != null && Convert.ToDecimal(ReturnValue.Reverse1Hrs) > 0)
                            {
                                tbReserver1hours.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(ReturnValue.Reverse1Hrs), 3).ToString());
                            }
                            else
                            {
                                tbReserver1hours.Text = "0:00";
                            }
                            if (ReturnValue.Reverse2Hrs != null && Convert.ToDecimal(ReturnValue.Reverse2Hrs) > 0)
                            {
                                tbReserver2hours.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(ReturnValue.Reverse2Hrs), 3).ToString());
                            }
                            else
                            {
                                tbReserver2hours.Text = "0:00";
                            }
                            if (ReturnValue.Reverse3Hrs != null && Convert.ToDecimal(ReturnValue.Reverse3Hrs) > 0)
                            {
                                tbReserver3hours.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(ReturnValue.Reverse3Hrs), 3).ToString());
                            }
                            else
                            {
                                tbReserver3hours.Text = "0:00";
                            }
                            if (ReturnValue.Reverse4Hrs != null && Convert.ToDecimal(ReturnValue.Reverse4Hrs) > 0)
                            {
                                tbReserver4hours.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(ReturnValue.Reverse4Hrs), 3).ToString());
                            }
                            else
                            {
                                tbReserver4hours.Text = "0:00";
                            }
                        }
                        else
                        {

                            if (ReturnValue.AirframeHrs != null && Convert.ToDecimal(ReturnValue.AirframeHrs) > 0)
                            {
                                tbHoursAirFrame.Text = AddPadding(ReturnValue.AirframeHrs.ToString());
                            }
                            if (ReturnValue.Engine1Hrs != null && Convert.ToDecimal(ReturnValue.Engine1Hrs) > 0)
                            {
                                tbHoursEngine1.Text = AddPadding(ReturnValue.Engine1Hrs.ToString());
                            }
                            if (ReturnValue.Engine2Hrs != null && Convert.ToDecimal(ReturnValue.Engine2Hrs) > 0)
                            {
                                tbHoursEngine2.Text = AddPadding(ReturnValue.Engine2Hrs.ToString());
                            }
                            if (ReturnValue.Engine3Hrs != null && Convert.ToDecimal(ReturnValue.Engine3Hrs) > 0)
                            {
                                tbHoursEngine3.Text = AddPadding(ReturnValue.Engine3Hrs.ToString());
                            }
                            if (ReturnValue.Engine4Hrs != null && Convert.ToDecimal(ReturnValue.Engine4Hrs) > 0)
                            {
                                tbHoursEngine4.Text = AddPadding(ReturnValue.Engine4Hrs.ToString());
                            }
                            if (ReturnValue.Reverse1Hrs != null && Convert.ToDecimal(ReturnValue.Reverse1Hrs) > 0)
                            {
                                tbReserver1hours.Text = AddPaddingToThreeDecimal(ReturnValue.Reverse1Hrs.ToString());
                            }
                            if (ReturnValue.Reverse2Hrs != null && Convert.ToDecimal(ReturnValue.Reverse2Hrs) > 0)
                            {
                                tbReserver2hours.Text = AddPaddingToThreeDecimal(ReturnValue.Reverse2Hrs.ToString());
                            }
                            if (ReturnValue.Reverse3Hrs != null && Convert.ToDecimal(ReturnValue.Reverse3Hrs) > 0)
                            {
                                tbReserver3hours.Text = AddPaddingToThreeDecimal(ReturnValue.Reverse3Hrs.ToString());
                            }
                            if (ReturnValue.Reverse4Hrs != null && Convert.ToDecimal(ReturnValue.Reverse4Hrs) > 0)
                            {
                                tbReserver4hours.Text = AddPaddingToThreeDecimal(ReturnValue.Reverse4Hrs.ToString());
                            }
                        }
                        hdnPkeyId.Value = ReturnValue.FleetInformationID.ToString();
                        hdnMode.Value = "Edit";
                    }
                    else
                    {
                        TimeDisplay();
                    }
                }
            }
        }
        private string AddPadding(string Value)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string[] strArr = new string[2];
                string ReturnValue = string.Empty;
                strArr = Value.Split('.');
                if (strArr[1].Length == 1)
                {
                    ReturnValue = strArr[1].ToString();
                }
                else if (strArr[1].Length == 2)
                {
                    ReturnValue = strArr[1].ToString().Substring(0, 1);
                }
                else if (strArr[1].Length == 3)
                {
                    ReturnValue = strArr[1].ToString().Substring(0, 1);
                }
                return strArr[0] + "." + ReturnValue;
            }
        }

        private string AddPaddingToThreeDecimal(string Value)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string[] strArr = new string[2];
                string ReturnValue = string.Empty;
                strArr = Value.Split('.');
                if (strArr[1].Length == 1)
                {
                    ReturnValue = strArr[1].ToString();
                }
                else if (strArr[1].Length == 2)
                {
                    ReturnValue = strArr[1].ToString().Substring(0, 2);
                }
                else if (strArr[1].Length == 3)
                {
                    ReturnValue = strArr[1].ToString().Substring(0, 2);
                }
                return strArr[0] + "." + ReturnValue;
            }
        }

        
        protected void Cancel_Click(object sender, EventArgs e)
        {
            //ClearForm();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnMode.Value.ToLower() == "edit")
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.FleetProfile, Convert.ToInt64(Request.QueryString["FleetID"].ToString().Trim()));
                            }
                        }
                        Response.Redirect("FleetProfileCatalog.aspx?FleetID=" + ViewState["FleetID"].ToString().Trim());
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

        }

        private void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                TextBox tbairframeasof = (TextBox)ucairframeasof.FindControl("tbDate");
                tbairframeasof.Text = string.Empty;
                tbcyclesairframe.Text = string.Empty;
                tbHoursAirFrame.Text = string.Empty;

                TextBox tbasofengine1 = (TextBox)ucasofengine1.FindControl("tbDate");
                tbasofengine1.Text = string.Empty;
                tbcyclesengine1.Text = string.Empty;
                tbHoursEngine1.Text = string.Empty;
                
                TextBox tbasofengine2 = (TextBox)ucasofengine2.FindControl("tbDate");
                tbasofengine2.Text = string.Empty;
                tbcyclesengine2.Text = string.Empty;
                tbHoursEngine2.Text = string.Empty;
                
                TextBox tbasofengine3 = (TextBox)ucasofengine3.FindControl("tbDate");
                tbasofengine3.Text = string.Empty;
                tbcyclesengine3.Text = string.Empty;
                tbHoursEngine3.Text = string.Empty;
                
                TextBox tbasofengine4 = (TextBox)ucasofengine4.FindControl("tbDate");
                tbasofengine4.Text = string.Empty;
                tbcyclesengine4.Text = string.Empty;
                tbHoursEngine4.Text = string.Empty;
                
                //tb1stthhr.Text = string.Empty;
                //tb2ndhhr.Text = string.Empty;
                //tb3rdhr.Text = string.Empty;
                //tb4thhr.Text = string.Empty;

                tbhsc1sthr.Text = string.Empty;
                tbhsc2ndhr.Text = string.Empty;
                tbhsc3rdhr.Text = string.Empty;
                tbhsc4thhr.Text = string.Empty;
                tbhsc5thhr.Text = string.Empty;
                tbhsc6thhr.Text = string.Empty;
                tbhsc7thhr.Text = string.Empty;
                tbhsc8thhr.Text = string.Empty;
                tbhsc9thhr.Text = string.Empty;
                tbhsc10thhr.Text = string.Empty;
                tbhsc11thhr.Text = string.Empty;
                tbhsc12thhr.Text = string.Empty;
                tbhsc13thhr.Text = string.Empty;
                tbhsc14thhr.Text = string.Empty;
                tbhsc15thhr.Text = string.Empty;
                tbhsc16thhr.Text = string.Empty;
                tbhsc17thhr.Text = string.Empty;
                tbhsc18thhr.Text = string.Empty;
                tbhsc19thhr.Text = string.Empty;
                tbhsc20thhr.Text = string.Empty;

                tblrc1sthr.Text = string.Empty;
                tblrc2ndhr.Text = string.Empty;
                tblrc3rdhr.Text = string.Empty;
                tblrc4thr.Text = string.Empty;
                tblrc5thr.Text = string.Empty;
                tblrc6thr.Text = string.Empty;
                tblrc7thr.Text = string.Empty;
                tblrc8thr.Text = string.Empty;
                tblrc9thr.Text = string.Empty;
                tblrc10thr.Text = string.Empty;
                tblrc11thr.Text = string.Empty;
                tblrc12thr.Text = string.Empty;
                tblrc13thr.Text = string.Empty;
                tblrc14thr.Text = string.Empty;
                tblrc15thr.Text = string.Empty;
                tblrc16thr.Text = string.Empty;
                tblrc17thr.Text = string.Empty;
                tblrc18thr.Text = string.Empty;
                tblrc19thr.Text = string.Empty;
                tblrc20thr.Text = string.Empty;
                
                tb1stthhr.Text = string.Empty;
                tb2ndhhr.Text = string.Empty;
                tb3rdhr.Text = string.Empty;
                tb4thhr.Text = string.Empty;
                tb5thhr.Text = string.Empty;
                tb6thhr.Text = string.Empty;
                tb7thhr.Text = string.Empty;
                tb8thhr.Text = string.Empty;
                tb9thhr.Text = string.Empty;
                tb10thhr.Text = string.Empty;
                tb11thhr.Text = string.Empty;
                tb12thhr.Text = string.Empty;
                tb13thhr.Text = string.Empty;
                tb14thhr.Text = string.Empty;
                tb15thhr.Text = string.Empty;
                tb16thhr.Text = string.Empty;
                tb17thhr.Text = string.Empty;
                tb18thhr.Text = string.Empty;
                tb19thhr.Text = string.Empty;
                tb20thhr.Text = string.Empty;
                
                tbReserver1hours.Text = string.Empty;
                tbReserver2hours.Text = string.Empty;
                tbReserver3hours.Text = string.Empty;
                tbReserver4hours.Text = string.Empty;
                
                TextBox tbReserver1asof = (TextBox)ucreverser1asof.FindControl("tbDate");                
                tbReserver1asof.Text = string.Empty;
                tbReserver1cycles.Text = string.Empty;

                TextBox tbReserver2asof = (TextBox)ucreverser2asof.FindControl("tbDate");
                tbReserver2asof.Text = string.Empty;
                tbReserver2cycles.Text = string.Empty;

                TextBox tbReserver3asof = (TextBox)ucreverser3asof.FindControl("tbDate");
                tbReserver3asof.Text = string.Empty;
                tbReserver3cycles.Text = string.Empty;

                TextBox tbReserver4asof = (TextBox)ucreverser4asof.FindControl("tbDate");
                tbReserver4asof.Text = string.Empty;
                tbReserver4cycles.Text = string.Empty;
                hdnMode.Value = "";
            }
        }

        private void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                TextBox tbairframeasof = (TextBox)ucairframeasof.FindControl("tbDate");
                tbairframeasof.Enabled = enable;
                tbcyclesairframe.Enabled = enable;
                tbHoursAirFrame.Enabled = enable;

                TextBox tbasofengine1 = (TextBox)ucasofengine1.FindControl("tbDate");
                tbasofengine1.Enabled = enable;
                tbcyclesengine1.Enabled = enable;
                tbHoursEngine1.Enabled = enable;

                TextBox tbasofengine2 = (TextBox)ucasofengine2.FindControl("tbDate");
                tbasofengine2.Enabled = enable;
                tbcyclesengine2.Enabled = enable;
                tbHoursEngine2.Enabled = enable;

                TextBox tbasofengine3 = (TextBox)ucasofengine3.FindControl("tbDate");
                tbasofengine3.Enabled = enable;
                tbcyclesengine3.Enabled = enable;
                tbHoursEngine3.Enabled = enable;

                TextBox tbasofengine4 = (TextBox)ucasofengine4.FindControl("tbDate");
                tbasofengine4.Enabled = enable;
                tbcyclesengine4.Enabled = enable;
                tbHoursEngine4.Enabled = enable;

                //tb1stthhr.Enabled = enable;
                //tb2ndhhr.Enabled = enable;
                //tb3rdhr.Enabled = enable;
                //tb4thhr.Enabled = enable;

                tbhsc1sthr.Enabled = enable;
                tbhsc2ndhr.Enabled = enable;
                tbhsc3rdhr.Enabled = enable;
                tbhsc4thhr.Enabled = enable;
                tbhsc5thhr.Enabled = enable;
                tbhsc6thhr.Enabled = enable;
                tbhsc7thhr.Enabled = enable;
                tbhsc8thhr.Enabled = enable;
                tbhsc9thhr.Enabled = enable;
                tbhsc10thhr.Enabled = enable;
                tbhsc11thhr.Enabled = enable;
                tbhsc12thhr.Enabled = enable;
                tbhsc13thhr.Enabled = enable;
                tbhsc14thhr.Enabled = enable;
                tbhsc15thhr.Enabled = enable;
                tbhsc16thhr.Enabled = enable;
                tbhsc17thhr.Enabled = enable;
                tbhsc18thhr.Enabled = enable;
                tbhsc19thhr.Enabled = enable;
                tbhsc20thhr.Enabled = enable;

                tblrc1sthr.Enabled = enable;
                tblrc2ndhr.Enabled = enable;
                tblrc3rdhr.Enabled = enable;
                tblrc4thr.Enabled = enable;
                tblrc5thr.Enabled = enable;
                tblrc6thr.Enabled = enable;
                tblrc7thr.Enabled = enable;
                tblrc8thr.Enabled = enable;
                tblrc9thr.Enabled = enable;
                tblrc10thr.Enabled = enable;
                tblrc11thr.Enabled = enable;
                tblrc12thr.Enabled = enable;
                tblrc13thr.Enabled = enable;
                tblrc14thr.Enabled = enable;
                tblrc15thr.Enabled = enable;
                tblrc16thr.Enabled = enable;
                tblrc17thr.Enabled = enable;
                tblrc18thr.Enabled = enable;
                tblrc19thr.Enabled = enable;
                tblrc20thr.Enabled = enable;

                tb1stthhr.Enabled = enable;
                tb2ndhhr.Enabled = enable;
                tb3rdhr.Enabled = enable;
                tb4thhr.Enabled = enable;
                tb5thhr.Enabled = enable;
                tb6thhr.Enabled = enable;
                tb7thhr.Enabled = enable;
                tb8thhr.Enabled = enable;
                tb9thhr.Enabled = enable;
                tb10thhr.Enabled = enable;
                tb11thhr.Enabled = enable;
                tb12thhr.Enabled = enable;
                tb13thhr.Enabled = enable;
                tb14thhr.Enabled = enable;
                tb15thhr.Enabled = enable;
                tb16thhr.Enabled = enable;
                tb17thhr.Enabled = enable;
                tb18thhr.Enabled = enable;
                tb19thhr.Enabled = enable;
                tb20thhr.Enabled = enable;

                tbReserver1hours.Enabled = enable;
                tbReserver2hours.Enabled = enable;
                tbReserver3hours.Enabled = enable;
                tbReserver4hours.Enabled = enable;

                TextBox tbReserver1asof = (TextBox)ucreverser1asof.FindControl("tbDate");
                tbReserver1asof.Enabled = enable;
                tbReserver1cycles.Enabled = enable;

                TextBox tbReserver2asof = (TextBox)ucreverser2asof.FindControl("tbDate");
                tbReserver2asof.Enabled = enable;
                tbReserver2cycles.Enabled = enable;

                TextBox tbReserver3asof = (TextBox)ucreverser3asof.FindControl("tbDate");
                tbReserver3asof.Enabled = enable;
                tbReserver3cycles.Enabled = enable;

                TextBox tbReserver4asof = (TextBox)ucreverser4asof.FindControl("tbDate");
                tbReserver4asof.Enabled = enable;
                tbReserver4cycles.Enabled = enable;
            }
        }

        
        /// <summary>
        /// Method to Get Tenths Conversion Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <param name="isTenths">Pass Boolean Value</param>
        /// <param name="conversionType">Pass Conversion Type</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertMinToTenths(String time, Boolean isTenths, String conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time, isTenths, conversionType))
            {
                string result = "00.0";
                decimal minute; decimal hour;
                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    if (!string.IsNullOrEmpty(timeArray[0]))
                        hour = Convert.ToDecimal(timeArray[0]);
                    else
                        hour = 00;
                    if (!string.IsNullOrEmpty(timeArray[1]))
                        minute = Convert.ToDecimal(timeArray[1]);
                    else
                        minute = 00;
                    string minutes = Convert.ToString(minute);
                    decimal decimalOfMin = 0;
                    if (minute > 0) // Added conditon to avoid divide by zero error.
                        decimalOfMin = minute / 60;
                    result = Convert.ToString(hour + decimalOfMin);
                }
                return result;
            }
        }
        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "0:00";
                int val;
                if (time.IndexOf(".") != -1)
                {
                    string[] timeArray = time.Split('.');
                    decimal hour = Convert.ToInt64(timeArray[0]);
                    string minute = Convert.ToString(timeArray[1]);
                    decimal decimal_min = Convert.ToDecimal(String.Concat("0.", minute.ToString()));
                    decimal decimalOfMin = 0;
                    if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                        decimalOfMin = (decimal_min * 60 / 100);
                    else
                        decimalOfMin = 00;
                    decimal finalval = Math.Round(hour + decimalOfMin, 2);
                    result = Convert.ToString(finalval).Replace(".", ":");
                    string[] strArr = new string[2];
                    string ReturnValue = string.Empty;
                    strArr = result.Split(':');
                    var charExists = (result.IndexOf(':') >= 0) ? true : false;
                    if (charExists == false)
                    {
                        ReturnValue = "00";
                        result = strArr[0] + ":" + ReturnValue;
                    }

                }
                else if (int.TryParse(time, out val))
                {
                    result = time;
                }
                return result;
            }
        }
    }
}