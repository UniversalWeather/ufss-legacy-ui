﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FleetProfilePopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Fleet.Popup.FleetProfilePopup" ClientIDMode="AutoID" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="format-detection" content="telephone=no">
    
    <title>Fleet Profile</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:radcodeblock id="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgFleetPopup.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {

                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgFleetPopup.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;

                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var FleetId = MasterTable.getCellByColumnUniqueName(row, "FleetID");
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "TailNum");
                        var aircraftTypeCD = MasterTable.getCellByColumnUniqueName(row, "AircraftCD"); // Added for Postflight
                        var AirCraftTypeCode = MasterTable.getCellByColumnUniqueName(row, "AirCraft_AircraftCD");   //added for utilities-itineraryplan
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "FleetID"); // Added for CharterQuote
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "AirCraft_AircraftCD"); // Added for CharterQuote
                        if (i == 0) {
                            oArg.TailNum = cell1.innerHTML + ",";
                            oArg.FleetId = FleetId.innerHTML;

                            oArg.AircraftTypeCD = aircraftTypeCD.innerHTML;
                            oArg.AirCraftTypeCode = AirCraftTypeCode.innerHTML;   //added for utilities-itineraryplan
                            oArg.FleetIds = cell2.innerHTML + ","; // Added for CharterQuote
                            oArg.AirCraftTypeCodes = cell3.innerHTML + ","; // Added for CharterQuote
                        }
                        else {
                            oArg.TailNum += cell1.innerHTML + ",";
                            oArg.FleetId += FleetId.innerHTML;
                            oArg.AircraftTypeCD += aircraftTypeCD.innerHTML;  // Added for Postflight
                            oArg.AirCraftTypeCode += AirCraftTypeCode.innerHTML;   //added for utilities-itineraryplan
                            oArg.FleetIds += cell2.innerHTML + ","; // Added for CharterQuote
                            oArg.AirCraftTypeCodes += cell3.innerHTML + ","; // Added for CharterQuote
                        }
                    }
                }
                else {
                    oArg.FleetId = "0";
                    oArg.TailNum = "";
                    oArg.AircraftTypeCD = "";
                    oArg.AirCraftTypeCode = "";
                    oArg.FleetIds = ""; // Added for CharterQuote
                    oArg.AirCraftTypeCodes = ""; // Added for CharterQuote
                }
                if (oArg.TailNum != "") {
                    oArg.TailNum = oArg.TailNum.substring(0, oArg.TailNum.length - 1)
                }
                else {
                    oArg.TailNum = "";
                }
                oArg.Arg1 = oArg.TailNum;
                oArg.CallingButton = "TailNum";
                // Added for CharterQuote
                if (oArg.FleetIds != "") {
                    oArg.FleetIds = oArg.FleetIds.substring(0, oArg.FleetIds.length - 1)
                }
                else {
                    oArg.FleetIds = "";
                }
                if (oArg.FleetIds != null) {
                    oArg.Arg2 = oArg.FleetIds;
                    oArg.CallingButton1 = "FleetID";
                }

                // Added for CharterQuote
                if (oArg.AirCraftTypeCodes != "") {
                    oArg.AirCraftTypeCodes = oArg.AirCraftTypeCodes.substring(0, oArg.AirCraftTypeCodes.length - 1)
                }
                else {
                    oArg.AirCraftTypeCodes = "";
                }
                if (oArg.AirCraftTypeCodes) {
                    oArg.Arg3 = oArg.AirCraftTypeCodes;
                    oArg.CallingButton2 = "AirCraft_AircraftCD";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function GetGridId() {
                return $find("<%= dgFleetPopup.ClientID %>");
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgFleetPopup.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
        </script>
    </telerik:radcodeblock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:radajaxmanager id="RadAjaxManager1" runat="server" onajaxsettingcreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgFleetPopup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetPopup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetPopup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:radajaxmanager>
        <telerik:radwindowmanager id="RadWindowManager1" runat="server" />
        <telerik:radajaxloadingpanel id="RadAjaxLoadingPanel1" runat="server" skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkActiveOnly" runat="server" Checked="true" Text="Active Only"
                                    />
                            </td>
                            <td>
                                <asp:CheckBox ID="chkHomeBaseOnly" runat="server" Checked="false" Text="Home Base Only"
                                    />
                            </td>
                            <td>
                                <asp:CheckBox ID="chkRotary" runat="server" Checked="true" Text="Rotary Only" />
                            </td>
                            <td>
                                <asp:CheckBox ID="chkFixedWing" runat="server" Checked="true" Text="Fixed Wing Only"
                                    />
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                    ValidationGroup="Save" OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:radgrid id="dgFleetPopup" runat="server" allowmultirowselection="true" allowsorting="true"
                        onneeddatasource="dgFleetPopup_BindData" 
                        onitemdatabound="dgFleetPopup_ItemDatabound" autogeneratecolumns="false" height="330px"
                        allowpaging="false" width="960px" onitemcommand="dgFleetPopup_ItemCommand" ondeletecommand="dgFleetPopup_DeleteCommand"
                        onitemcreated="dgFleetPopup_ItemCreated">
                        <MasterTableView DataKeyNames="FleetID,AircraftCD,TailNum,AirCraft_AircraftCD,IsInActive,LastUpdUID,LastUpdTS,IcaoID,PowerSettings1TrueAirSpeed,PowerSettings2TrueAirSpeed,PowerSettings3TrueAirSpeed"
                            CommandItemDisplay="None" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="FleetID" HeaderText="FleetID" DataField="FleetID"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Fleet Code" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" HeaderStyle-Width="60px" FilterControlWidth="40px" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" HeaderStyle-Width="80px" FilterControlWidth="60px" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AirCraft_AircraftCD" HeaderText="Type Code" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TypeDescription" HeaderText="Type Description"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                    HeaderStyle-Width="160px" FilterControlWidth="140px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PowerSettings1TrueAirSpeed" HeaderText="Constant Mach"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="80px" CurrentFilterFunction="Contains"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PowerSettings2TrueAirSpeed" HeaderText="Long Range Cruise"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="80px" CurrentFilterFunction="Contains"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PowerSettings3TrueAirSpeed" HeaderText="High Speed Cruise"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="80px" CurrentFilterFunction="Contains"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="MaximumPassenger" HeaderText="Maximum PAX" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Vendor Code" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IcaoID" HeaderText="Home Base" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomebaseID" HeaderText="Home Base ID" Display="false"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn UniqueName="BoolField" DataField="IsInActive" HeaderText="Active"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="60px" AllowFiltering="false"
                                    CurrentFilterFunction="EqualTo">
                                </telerik:GridCheckBoxColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grid_icon">
                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                        CommandName="InitInsert" Visible='<%# showCommandItems && IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.AddFleetProfile)%>'></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                        CommandName="Edit" Visible='<%# showCommandItems && IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.EditFleetProfile)%>'
                                        OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgFleetPopup', 'radFleetProfilePopup');"
                                        CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# showCommandItems && IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.DeleteFleetProfile)%>'></asp:LinkButton>
                                    <%-- CommandName="DeleteSelected" --%>
                                </div>
                                <div class="grd_ok">
                                    <asp:Button ID="btnSubmit" OnClientClick="returnToParent(); return false;" Text="Ok"
                                        runat="server" CssClass="button"></asp:Button>
                                    <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                                </div>
                                <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                    visible="false">
                                    Use CTRL key to multi select</div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:radgrid>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdnVendorID" runat="server" />
    </div>
    </form>
</body>
</html>
