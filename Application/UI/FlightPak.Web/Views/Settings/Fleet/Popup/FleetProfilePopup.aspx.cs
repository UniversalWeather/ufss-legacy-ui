﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;

namespace FlightPak.Web.Views.Settings.Fleet.Popup
{
    public partial class FleetProfilePopup : BaseSecuredPage
    {
        private string TailNum;
        private ExceptionManager exManager;
        public bool showCommandItems = true;
        public bool CRMain = false;
        private string VendorID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string i = Request.QueryString["TailNumber"];

                if (Request.QueryString["TailNumber"] != null || Request.QueryString["FromPage"] != null)
                {
                    if ((Request.QueryString["TailNumber"] != null && Request.QueryString["TailNumber"].ToString() == "1") || (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "CompareCharterRates"))
                    {
                        dgFleetPopup.AllowMultiRowSelection = true;
                    }
                    else
                    {
                        dgFleetPopup.AllowMultiRowSelection = false;
                    }
                }
                else
                {
                    dgFleetPopup.AllowMultiRowSelection = false;
                }
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        showCommandItems = true;
                        if (!IsPostBack)
                        {
                            //Added for Reassign the Selected Value and highlight the specified row in the Grid             
                            if (Request.QueryString["TailNumber"] != null)
                            {
                                TailNum = Request.QueryString["TailNumber"].ToUpper().Trim();
                            }

                            if (Request.QueryString["Vendor"] != null)
                            {
                                VendorID = Request.QueryString["Vendor"].Trim();
                            }
                        }
                        //Ramesh: Defect# 7146, Commented code to display CRUD operation in System Tools
                        //if (!string.IsNullOrEmpty(Request.QueryString["fromPage"]) && Request.QueryString["fromPage"] == "SystemTools")
                        //{
                        //    showCommandItems = false;
                        //}
                        if (!string.IsNullOrEmpty(Request.QueryString["fromPage1"]) && Request.QueryString["fromPage1"] == "CRMain")
                        {
                            CRMain = true;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }



        }


        protected void dgFleetPopup_Prerender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["fromPage1"] == null && Request.QueryString["fromPage1"] != "CRMain")
                {
                    if ((Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToLower() == "preflight") || (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToLower() == "postflight"))
                    {
                        using (PreflightService.PreflightServiceClient ObjService = new PreflightService.PreflightServiceClient())
                        {
                            string IsFixedRotary = "B";
                            bool IsInActive = false;
                            string HomeBaseCD = string.Empty;
                            if (chkRotary.Checked && !chkFixedWing.Checked)
                            {
                                IsFixedRotary = "R";
                            }
                            else if (!chkRotary.Checked && chkFixedWing.Checked)
                            {
                                IsFixedRotary = "F";
                            }
                            if (chkActiveOnly.Checked)
                            {
                                IsInActive = true;
                            }
                            if (chkHomeBaseOnly.Checked)
                            {
                                if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null)
                                {
                                    HomeBaseCD = UserPrincipal.Identity._airportICAOCd;
                                }
                            }

                            var ObjRetVal = ObjService.GetAllFleetForPopup(IsInActive, IsFixedRotary, HomeBaseCD, 0);
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgFleetPopup.DataSource = ObjRetVal.EntityList;
                                //if (chkActiveOnly.Checked)
                                //{
                                //    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.IsDeleted == false && x.IsInactive == "True").ToList();
                                //}
                                //else
                                //{
                                //    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.IsDeleted == false).ToList();
                                //}
                                dgFleetPopup.Rebind();
                            }
                        }
                    }
                }
                //else
                //{
                //    if ((Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToLower() == "crmain"))
                //    {
                //        if (UserPrincipal.Identity._isSysAdmin == false || UserPrincipal.Identity._travelCoordID != null)
                //        {
                //            using (CorporateRequestService.CorporateRequestServiceClient ObjService = new CorporateRequestService.CorporateRequestServiceClient())
                //            {
                //                var ObjRetVal = ObjService.GetCRFleet(Convert.ToInt64(UserPrincipal.Identity._travelCoordID));
                //                if (ObjRetVal.ReturnFlag == true)
                //                {
                //                    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.IsInActive == false).ToList();
                //                    dgFleetPopup.Rebind();
                //                }
                //            }
                //        }
                //        else
                //        {
                //            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                //            {
                //                var ObjRetVal = ObjService.GetAllFleetForPopupList();
                //                if (ObjRetVal.ReturnFlag == true)
                //                {
                //                    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.IsInActive == false).ToList();
                //                    dgFleetPopup.Rebind();
                //                }
                //            }
                //        }
                //    }
                //}
            }

        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgFleetPopup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        string IsFixedRotary = "B";
                                bool IsInActive = false;
                                string HomeBaseCD = string.Empty;
                                if (chkRotary.Checked && !chkFixedWing.Checked)
                                {
                                    IsFixedRotary = "R";
                                }
                                else if (!chkRotary.Checked && chkFixedWing.Checked)
                                {
                                    IsFixedRotary = "F";
                                }
                                if (chkActiveOnly.Checked)
                                {
                                    IsInActive = true;
                                }
                                if (chkHomeBaseOnly.Checked)
                                {
                                    if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null)
                                    {
                                        HomeBaseCD = UserPrincipal.Identity._airportICAOCd;
                                    }
                                }


                        if (!CRMain)
                        {
                            using (PreflightService.PreflightServiceClient ObjService = new PreflightService.PreflightServiceClient())
                            {
                                


                                if ((Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToLower() == "expressquote"))
                                {
                                    long _VendorID = 0;
                                    if (!string.IsNullOrEmpty(VendorID) && long.TryParse(VendorID, out _VendorID))
                                    {
                                        var ObjRetVal = ObjService.GetAllFleetForPopup(IsInActive, IsFixedRotary, HomeBaseCD, _VendorID);

                                        if (ObjRetVal.ReturnFlag == true)
                                        {
                                            dgFleetPopup.DataSource = ObjRetVal.EntityList;
                                        }

                                        //if (chkActiveOnly.Checked)
                                        //{
                                        //    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.VendorID == Convert.ToInt64(VendorID) && x.IsDeleted == false && Convert.ToBoolean(x.IsInactive) == true).ToList();
                                        //}
                                        //else
                                        //{
                                        //    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.VendorID == Convert.ToInt64(VendorID) && x.IsDeleted == false).ToList();
                                        //}
                                    }
                                }
                                else
                                {

                                    var ObjRetVal = ObjService.GetAllFleetForPopup(IsInActive, IsFixedRotary, HomeBaseCD, 0);

                                    if (ObjRetVal.ReturnFlag == true)
                                    {
                                        dgFleetPopup.DataSource = ObjRetVal.EntityList;
                                    }
                                    //if (chkActiveOnly.Checked)
                                    //{
                                    //    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.IsInactive == "True").ToList();
                                    //}
                                    //else
                                    //{
                                    //    dgFleetPopup.DataSource = ObjRetVal.EntityList.ToList();
                                    //}
                                }
                            }

                        }
                        else
                        {
                            
                            if (UserPrincipal.Identity._isSysAdmin == false && UserPrincipal.Identity._travelCoordID != null)
                            {
                                #region "CR Main"
                                using (CorporateRequestService.CorporateRequestServiceClient ObjService = new CorporateRequestService.CorporateRequestServiceClient())
                                {
                                    var ObjRetVal = ObjService.GetCRFleet(Convert.ToInt64(UserPrincipal.Identity._travelCoordID));
                                    if (ObjRetVal.ReturnFlag == true)
                                    {
                                        if (chkActiveOnly.Checked)
                                        {
                                            dgFleetPopup.DataSource = dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.IsDeleted == false && Convert.ToBoolean(x.IsInactive) == true).ToList();
                                        }
                                        else
                                        {
                                            dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.IsDeleted == false).ToList();
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {

                               

                                using (PreflightService.PreflightServiceClient ObjService = new PreflightService.PreflightServiceClient())
                                {
                                   
                                        if ((Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToLower() == "expressquote"))
                                        {
                                            long _VendorID = 0;
                                            if (!string.IsNullOrEmpty(VendorID) && long.TryParse(VendorID,out _VendorID) )
                                            {

                                                var ObjRetVal = ObjService.GetAllFleetForPopup(IsInActive, IsFixedRotary, HomeBaseCD, _VendorID);

                                                if (ObjRetVal.ReturnFlag == true)
                                                {
                                                    dgFleetPopup.DataSource = ObjRetVal.EntityList;
                                                }

                                                //if (chkActiveOnly.Checked)
                                                //{
                                                //    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.VendorID == Convert.ToInt64(VendorID) && x.IsDeleted == false && Convert.ToBoolean(x.IsInactive) == true).ToList();
                                                //}
                                                //else
                                                //{
                                                //    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => x.VendorID == Convert.ToInt64(VendorID) && x.IsDeleted == false).ToList();
                                                //}
                                            }
                                        }
                                        else
                                        {
                                            var ObjRetVal = ObjService.GetAllFleetForPopup(IsInActive, IsFixedRotary, HomeBaseCD, 0);

                                            if (ObjRetVal.ReturnFlag == true)
                                            {
                                                dgFleetPopup.DataSource = ObjRetVal.EntityList;
                                            }

                                            //if (chkActiveOnly.Checked)
                                            //{
                                            //    dgFleetPopup.DataSource = ObjRetVal.EntityList.Where(x => Convert.ToBoolean(x.IsInactive) == true).ToList();
                                            //}
                                            //else
                                            //{
                                            //    dgFleetPopup.DataSource = ObjRetVal.EntityList.ToList();
                                            //}
                                        }
                                    
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetPopup_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["TailNumber"] != null)
                {
                    if (Request.QueryString["TailNumber"].ToString() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgFleetPopup;
                        //if (!IsPostBack)
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

        }

        protected void dgFleetPopup_ItemDatabound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            //SelectItem();

                            if (!string.IsNullOrEmpty(TailNum))
                            {
                                GridDataItem Item = e.Item as GridDataItem;
                                if (Item["TailNum"].Text.Trim().ToUpper() == TailNum)
                                {
                                    Item.Selected = true;
                                    //break;
                                }
                            }
                            else
                            {
                                if (dgFleetPopup.MasterTableView.Items.Count > 0)
                                {
                                    if (Session["FPSelectedItem"] != null)
                                    {
                                        string ID = Session["FPSelectedItem"].ToString();
                                        foreach (GridDataItem Item in dgFleetPopup.MasterTableView.Items)
                                        {
                                            if (Item.GetDataKeyValue("FleetID").ToString().Trim() == ID)
                                            {
                                                Item.Selected = true;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dgFleetPopup.SelectedIndexes.Add(0);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }



        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(TailNum))
                {

                    foreach (GridDataItem item in dgFleetPopup.MasterTableView.Items)
                    {
                        if (item["TailNum"].Text.Trim().ToUpper() == TailNum)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgFleetPopup.MasterTableView.Items.Count > 0)
                    {
                        if (Session["FPSelectedItem"] != null)
                        {
                            string ID = Session["FPSelectedItem"].ToString();
                            foreach (GridDataItem Item in dgFleetPopup.MasterTableView.Items)
                            {
                                if (Item.GetDataKeyValue("FleetID").ToString().Trim() == ID)
                                {
                                    Item.Selected = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            dgFleetPopup.SelectedIndexes.Add(0);
                        }
                    }
                }

            }

        }

        private void BindData()
        {
            string IsFixedRotary = "B";
            bool IsInActive = false;
            string HomeBaseCD = string.Empty;
            if (chkRotary.Checked && !chkFixedWing.Checked)
            {
                IsFixedRotary = "R";
            }
            else if (!chkRotary.Checked && chkFixedWing.Checked)
            {
                IsFixedRotary = "F";
            }
            if (chkActiveOnly.Checked)
            {
                IsInActive = true;
            }
            if (chkHomeBaseOnly.Checked)
            {
                if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null)
                {
                    HomeBaseCD = UserPrincipal.Identity._airportICAOCd;
                }
            }


            if (!CRMain)
            {
                

                using (PreflightService.PreflightServiceClient Service = new PreflightService.PreflightServiceClient())
                {

                    var ObjRetVal = Service.GetAllFleetForPopup(IsInActive, IsFixedRotary, HomeBaseCD, 0);

                    if (ObjRetVal.ReturnFlag == true)
                    {
                        dgFleetPopup.DataSource = ObjRetVal.EntityList;
                        dgFleetPopup.Rebind();
                    }
                    //dgFleetPopup.DataSource = Service.GetFleetSearch(IsInActive, IsFixedRotary, HomeBaseID).EntityList.ToList();
                    //dgFleetPopup.Rebind();
                }
            }
            else
            {

                if (UserPrincipal.Identity._isSysAdmin == false && UserPrincipal.Identity._travelCoordID != null)
                {

                    #region CR Request
                    List<CorporateRequestService.GetAllFleetForCR> FleetList = new List<CorporateRequestService.GetAllFleetForCR>();
                    using (CorporateRequestService.CorporateRequestServiceClient Service = new CorporateRequestService.CorporateRequestServiceClient())
                    {
                        FleetList = Service.GetCRFleet(Convert.ToInt64(UserPrincipal.Identity._travelCoordID)).EntityList.ToList();
                    }

                    
                    Int64 HomeBaseID = 0;
                    if (chkRotary.Checked && !chkFixedWing.Checked)
                    {
                        IsFixedRotary = "R";
                    }
                    else if (!chkRotary.Checked && chkFixedWing.Checked)
                    {
                        IsFixedRotary = "F";
                    }
                    if (chkActiveOnly.Checked)
                    {
                        IsInActive = true;
                    }
                    if (chkHomeBaseOnly.Checked)
                    {
                        if (UserPrincipal != null && UserPrincipal.Identity._homeBaseId != null)
                        {
                            HomeBaseID = Convert.ToInt64(UserPrincipal.Identity._homeBaseId);
                            FleetList = FleetList.Where(x => x.HomebaseID == HomeBaseID).ToList();
                        }
                    }
                    if (IsFixedRotary == "B")
                    {
                        FleetList = FleetList.Where(x => Convert.ToBoolean(x.IsInactive) == IsInActive).ToList();
                    }
                    else
                    {
                        FleetList = FleetList.Where(x => Convert.ToBoolean(x.IsInactive) == IsInActive && x.IsFixedRotary == IsFixedRotary).ToList();
                    }
                    dgFleetPopup.DataSource = FleetList;
                    dgFleetPopup.Rebind();

                    #endregion
                }
                else
                {

                    using (PreflightService.PreflightServiceClient Service = new PreflightService.PreflightServiceClient())
                    {

                        var ObjRetVal = Service.GetAllFleetForPopup(IsInActive, IsFixedRotary, HomeBaseCD, 0);

                        if (ObjRetVal.ReturnFlag == true)
                        {
                            dgFleetPopup.DataSource = ObjRetVal.EntityList;
                            dgFleetPopup.Rebind();
                        }
                    }

                    //using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    //{
                    //    dgFleetPopup.DataSource = Service.GetFleetSearch(IsInActive, IsFixedRotary, HomeBaseID).EntityList.ToList();
                    //    dgFleetPopup.Rebind();
                    //}
                }
            }
        }
        protected void chkActiveOnly_CheckedChanged(object sender, EventArgs e)
        {

        }
        protected void chkRotary_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }
        protected void chkFixedWing_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }
        protected void chkHomeBaseOnly_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            BindData();
        }
        protected void dgFleetPopup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    string resolvedurl = string.Empty;
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                //e.Item.Selected = true;
                                GridDataItem item = dgFleetPopup.SelectedItems[0] as GridDataItem;
                                Session["FPSelectedItem"] = item["FleetID"].Text;
                                TryResolveUrl("/Views/Settings/Fleet/FleetProfileCatalog.aspx?IsPopup=", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radFleetProfileCRUDPopup');", true);
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/Fleet/FleetProfileCatalog.aspx?IsPopup=Add", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radFleetProfileCRUDPopup');", true);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Account);
                }
            }
        }

        protected void dgFleetPopup_DeleteCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                GridDataItem item = dgFleetPopup.SelectedItems[0] as GridDataItem;
                Int64 FleetId = Convert.ToInt64(item["FleetId"].Text);
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        using (FlightPakMasterService.MasterCatalogServiceClient FleetTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FlightPakMasterService.Fleet FleetType = new FlightPakMasterService.Fleet();
                            FleetType.FleetID = Convert.ToInt64(FleetId);
                            FleetType.IsDeleted = true;
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var ReturnValue = CommonService.Lock(EntitySet.Database.FleetProfile, FleetId);
                                if (!ReturnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    ShowLockAlertPopup(ReturnValue.LockMessage, ModuleNameConstants.Database.FleetProfile);
                                    return;
                                }
                                FleetTypeService.DeleteFleetProfileType(FleetType);
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.FleetProfile);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FleetProfile, FleetId);
                    }
                }


            }
        }
    }
}
