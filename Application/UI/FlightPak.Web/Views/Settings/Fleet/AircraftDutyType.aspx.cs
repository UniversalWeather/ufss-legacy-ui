﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class AircraftDutyType : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private List<string> listAircraftCodes = new List<string>();
        private bool AircraftDutyTypePageNavigated = false;
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewAircraftDutyTypeReport);
                        if (!IsPostBack)
                        {
                            // Grid Control could be ajaxified when the page is initially loaded.
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAircraftDutyTypes, dgAircraftDutyTypes, RadAjaxLoadingPanel1);
                            // Store the clientID of the grid to reference it later on the client
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgAircraftDutyTypes.ClientID));
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewAircraftDutyType);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgAircraftDutyTypes.Rebind();
                                ReadOnlyForm();
                                EnableForm(false);
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }

                            if (IsPopUp)
                            {
                                //Hide Controls
                                dgAircraftDutyTypes.Visible = false;
                                chkSearchActiveOnly.Visible = false;
                                lbtnReports.Visible = false;
                                lbtnSaveReports.Visible = false;
                                btnShowReports.Visible = false;
                                // Show Insert Form
                                if (IsAdd)
                                {
                                    (dgAircraftDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgAircraftDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (IsPopUp)
                        dgAircraftDutyTypes.AllowPaging = false;
                    if (BindDataSwitch)
                    {
                        dgAircraftDutyTypes.Rebind();
                    }
                    if (dgAircraftDutyTypes.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["AircraftDutyID"] = null;
                        //}
                        if (Session["AircraftDutyID"] == null)
                        {
                            dgAircraftDutyTypes.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                                Session["AircraftDutyID"] = dgAircraftDutyTypes.Items[0].GetDataKeyValue("AircraftDutyID").ToString();
                        }

                        if (dgAircraftDutyTypes.SelectedIndexes.Count == 0)
                            dgAircraftDutyTypes.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["AircraftDutyID"] != null)
                    {
                        string ID = Session["AircraftDutyID"].ToString();
                        foreach (GridDataItem Item in dgAircraftDutyTypes.MasterTableView.Items)
                        {
                            if (Item["AircraftDutyID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                dgAircraftDutyTypes.SelectedIndexes.Add(Item.ItemIndex);
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, delCtl, editCtl;
                    insertCtl = (LinkButton)dgAircraftDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    delCtl = (LinkButton)dgAircraftDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    editCtl = (LinkButton)dgAircraftDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddAircraftDutyType))
                    {
                        insertCtl.Visible = true;
                        if (add)
                        {
                            insertCtl.Enabled = true;
                        }
                        else
                        {
                            insertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteAircraftDutyType))
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditAircraftDutyType))
                    {
                        editCtl.Visible = true;
                        if (edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.ReadOnly = false;
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    
                    LoadControlData();
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    //dgAircraftDutyTypes.Rebind();
                    EnableForm(true);
                    RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                    LoadDate();

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Function to Enable / Diable the form fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (hdnSave.Value == "Update" && enable == true)
                    {
                        tbCode.Enabled = false;
                    }
                    else
                    {
                        tbCode.Enabled = enable;
                    }
                    if (hdnSave.Value == "Update" && enable == true && ((tbCode.Text.Trim().ToUpper() == "F" || tbCode.Text.Trim().ToUpper() == "G" || tbCode.Text.Trim().ToUpper() == "R")))
                    {
                        tbDescription.Enabled = false;
                    }
                    else
                    {
                        tbDescription.Enabled = enable;
                    }

                    tbStart.Enabled = enable;
                    tbEnd.Enabled = enable;
                    chkQuickCalendarEntry.Enabled = enable;
                    chkAircraftStandBy.Enabled = enable;
                    chkInactive.Enabled = enable;
                    tbForeColor.Enabled = enable;
                    tbBackColor.Enabled = enable;
                    rcpForeColor.Enabled = enable;
                    rcpBackColor.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear the Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    hdnAircraftDutyID.Value = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbStart.Text = "00:00";
                    tbEnd.Text = "00:00";
                    chkQuickCalendarEntry.Checked = false;
                    chkAircraftStandBy.Checked = false;
                    chkInactive.Checked = false;
                    tbForeColor.Text = string.Empty;
                    hdnForeColor.Value = string.Empty;
                    tbBackColor.Text = string.Empty;
                    hdnBackColor.Value = string.Empty;
                    rcpBackColor.SelectedColor = System.Drawing.Color.White;
                    rcpForeColor.SelectedColor = System.Drawing.Color.White;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadDate();
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Load Selected Item Data into the Form Fields
        /// </summary>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["AircraftDutyID"] != null)
                    {
                        foreach (GridDataItem item in dgAircraftDutyTypes.MasterTableView.Items)
                        {
                            if (item["AircraftDutyID"].Text.Trim() == Session["AircraftDutyID"].ToString().Trim())
                            {
                                hdnAircraftDutyID.Value = item.GetDataKeyValue("AircraftDutyID").ToString();
                                tbCode.Text = item.GetDataKeyValue("AircraftDutyCD").ToString();
                                if (item.GetDataKeyValue("AircraftDutyDescription") != null)
                                {
                                    tbDescription.Text = item.GetDataKeyValue("AircraftDutyDescription").ToString();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (item.GetDataKeyValue("ForeGrndCustomColor") != null)
                                {
                                    System.Drawing.Color foreColor;
                                    tbForeColor.Text = item.GetDataKeyValue("ForeGrndCustomColor").ToString();
                                    hdnForeColor.Value = item.GetDataKeyValue("ForeGrndCustomColor").ToString();
                                    foreColor = System.Drawing.Color.FromName(Convert.ToString(tbForeColor.Text, CultureInfo.CurrentCulture));
                                    rcpForeColor.SelectedColor = foreColor;
                                }
                                else
                                {
                                    tbForeColor.Text = string.Empty;
                                }
                                if (item.GetDataKeyValue("BackgroundCustomColor") != null)
                                {
                                    System.Drawing.Color backColor;
                                    tbBackColor.Text = item.GetDataKeyValue("BackgroundCustomColor").ToString();
                                    hdnBackColor.Value = item.GetDataKeyValue("BackgroundCustomColor").ToString();
                                    backColor = System.Drawing.Color.FromName(Convert.ToString(tbBackColor.Text, CultureInfo.CurrentCulture));
                                    rcpBackColor.SelectedColor = backColor;
                                }
                                else
                                {
                                    tbBackColor.Text = string.Empty;
                                }
                                if (item.GetDataKeyValue("IsCalendarEntry") != null)
                                {
                                    chkQuickCalendarEntry.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsCalendarEntry").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsCalendarEntry").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkQuickCalendarEntry.Checked = false;
                                }
                                if (item.GetDataKeyValue("IsAircraftStandby") != null)
                                {
                                    chkAircraftStandBy.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsAircraftStandby").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsAircraftStandby").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkAircraftStandBy.Checked = false;
                                }
                                if (item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsInActive").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                if (item.GetDataKeyValue("DefaultStartTM") != null)
                                {
                                    tbStart.Text = item.GetDataKeyValue("DefaultStartTM").ToString().Trim();
                                }
                                else
                                {
                                    tbStart.Text = "00:00";
                                }
                                if (item.GetDataKeyValue("DefualtEndTM") != null)
                                {
                                    tbEnd.Text = item.GetDataKeyValue("DefualtEndTM").ToString().Trim();
                                }
                                else
                                {
                                    tbEnd.Text = "00:00";
                                }

                                //item.Selected = true;

                                lbColumnName1.Text = "Aircraft Duty Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = item["AircraftDutyCD"].Text;
                                lbColumnValue2.Text = item["AircraftDutyDescription"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private FlightPakMasterService.AircraftDuty GetItems(FlightPakMasterService.AircraftDuty objAirDutyType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objAirDutyType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (hdnSave.Value == "Update")
                    {
                        objAirDutyType.AircraftDutyID = Convert.ToInt64(hdnAircraftDutyID.Value); //Convert.ToInt64(Item.GetDataKeyValue("AircraftDutyID"));
                    }
                    objAirDutyType.AircraftDutyCD = tbCode.Text;
                    objAirDutyType.AircraftDutyDescription = tbDescription.Text;
                    objAirDutyType.BackgroundCustomColor = hdnBackColor.Value;
                    objAirDutyType.ForeGrndCustomColor = hdnForeColor.Value;
                    objAirDutyType.DefaultStartTM = tbStart.TextWithLiterals;
                    objAirDutyType.DefualtEndTM = tbEnd.TextWithLiterals;
                    objAirDutyType.IsAircraftStandby = chkAircraftStandBy.Checked;
                    objAirDutyType.IsCalendarEntry = chkQuickCalendarEntry.Checked;
                    objAirDutyType.IsInActive = chkInactive.Checked;
                    objAirDutyType.IsDeleted = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            return objAirDutyType;
        }
        #region "Grid Events"
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftDutyTypes_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftDutyTypes_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objAircraftDuty = objService.GetAircraftDuty();
                            List<FlightPakMasterService.AircraftDuty> AircraftDutyList = new List<AircraftDuty>();
                            if (objAircraftDuty.ReturnFlag == true)
                            {
                                AircraftDutyList = objAircraftDuty.EntityList;
                            }
                            dgAircraftDutyTypes.DataSource = AircraftDutyList;
                            Session["DutyCodes"] = AircraftDutyList;
                            if (((chkSearchActiveOnly.Checked == true)) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Command for Aircraft Type Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftDutyTypes_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                // Lock the record
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.AircraftDuty, Convert.ToInt64(Session["AircraftDutyID"]));
                                    Session["IsEditLockAircraftDuty"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.AircraftDuty);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.AircraftDuty);
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                    DisableLinks();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgAircraftDutyTypes.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                //dgAircraftDutyTypes.Rebind();
                                DisableLinks();
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            //case "RowClick":
                            //   // dgAircraftDutyTypes.Rebind();
                            //    GridDataItem item = dgAircraftDutyTypes.SelectedItems[0] as GridDataItem;
                            //    Session["AircraftDutyID"] = item["AircraftDutyID"].Text;
                            //    var key = item.ItemIndex;
                            //    dgAircraftDutyTypes.Rebind();
                            //    dgAircraftDutyTypes.SelectedIndexes.Add(key);                               
                            //    GridEnable(true, true, true);                            

                            //    break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        /// <summary>
        /// Update Command for Aircraft Duty Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAircraftDutyTypes_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["AircraftDutyID"] != null)
                        {
                            if (Convert.ToDouble(tbStart.Text, CultureInfo.CurrentCulture) > Convert.ToDouble(tbEnd.Text, CultureInfo.CurrentCulture))
                            {
                                
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Start time should be lesser than end time', 360, 50, 'Aircraftduty Type Catalog', FocusCallBackFn);";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                RadAjaxManager1.FocusControl(tbStart.ClientID);
                            }
                            else
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.AircraftDuty objAirDutyType = new FlightPakMasterService.AircraftDuty();
                                    objAirDutyType = GetItems(objAirDutyType);
                                    var ReturnValue = objService.UpdateAircraftDuty(objAirDutyType);
                                    if (ReturnValue.ReturnFlag == true)
                                    {
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.AircraftDuty, Convert.ToInt64(Session["AircraftDutyID"]));
                                        }
                                        Session["IsEditLockAircraftDuty"] = "False";
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        dgAircraftDutyTypes.Rebind();
                                        SelectItem();
                                        ReadOnlyForm();

                                        ShowSuccessMessage();
                                        EnableLinks();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.AircraftDuty);
                                    }

                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            Session.Remove("AircraftDutyID");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('rdDutyCodePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        /// <summary>
        /// Insert Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAircraftDutyTypes_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //try
                        //{
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Unique Code is required', 380, 50, 'Aircraft Duty Type');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        else if (CheckForReservedCode())
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('R, F and G Codes Are Reserved For Internal Use by FlightPak.', 400, 50, 'Aircraft Duty Type');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }

                        else if (Convert.ToDouble(tbStart.Text, CultureInfo.CurrentCulture) > Convert.ToDouble(tbEnd.Text, CultureInfo.CurrentCulture))
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Start time should be lesser than end time', 360, 50, 'Aircraftduty Type Catalog', FocusCallBackFn);";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            RadAjaxManager1.FocusControl(tbStart.ClientID);
                        }
                        else
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.AircraftDuty objAirDutyType = new FlightPakMasterService.AircraftDuty();
                                objAirDutyType = GetItems(objAirDutyType);
                                var RetVal = objService.AddAircraftDuty(objAirDutyType);
                                if (RetVal.ReturnFlag == true)
                                {
                                    GridEnable(true, true, true);
                                    DefaultSelection(true);
                                    hdnGridEnable.Value = "Save";

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(RetVal.ErrorMessage, ModuleNameConstants.Database.AircraftDuty);
                                }
                            }
                        }

                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('rdDutyCodePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAircraftDutyTypes_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    Int64 AircraftDutyID = 0;
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["AircraftDutyID"] != null)
                            {
                                GridDataItem item = dgAircraftDutyTypes.SelectedItems[0] as GridDataItem;
                                string dutyCode = item.GetDataKeyValue("AircraftDutyCD").ToString();
                                if (dutyCode.Trim() == "F" || dutyCode.Trim() == "G" || dutyCode.Trim() == "R")
                                {
                                    
                                    string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                        + @" oManager.radalert('R, F and G Codes Are Reserved For Internal Use by FlightPak.', 380, 50, 'Aircraft Duty Type');";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                }
                                else
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        FlightPakMasterService.AircraftDuty objAirDutyType = new FlightPakMasterService.AircraftDuty();
                                        objAirDutyType.AircraftDutyID = Convert.ToInt64(item.GetDataKeyValue("AircraftDutyID").ToString());
                                        AircraftDutyID = Convert.ToInt64(item.GetDataKeyValue("AircraftDutyID").ToString());
                                        objAirDutyType.AircraftDutyCD = item.GetDataKeyValue("AircraftDutyCD").ToString();
                                        objAirDutyType.IsDeleted = true;
                                        //Lock the record
                                        var returnValue = CommonService.Lock(EntitySet.Database.AircraftDuty, AircraftDutyID);
                                        if (!returnValue.ReturnFlag)
                                        {
                                            e.Item.Selected = true;
                                            DefaultSelection(false);
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.AircraftDuty);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.AircraftDuty);
                                            return;
                                        }
                                        Int32 aircraftDutyExists = objService.GetPreflightLegAircraftDuty(item.GetDataKeyValue("AircraftDutyCD").ToString());
                                        if (aircraftDutyExists > 0)
                                        {
                                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                        + @" oManager.radalert('This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.', 380, 50, 'Aircraft Duty Type');";
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                            return;
                                        }
                                        objService.DeleteAircraftDuty(objAirDutyType);
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        DefaultSelection(true);
                                        hdnSave.Value = "Delete";
                                    }
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.AircraftDuty, AircraftDutyID);
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftDutyTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgAircraftDutyTypes.SelectedItems[0] as GridDataItem;
                                Session["AircraftDutyID"] = item["AircraftDutyID"].Text;
                                var key = item.ItemIndex;
                                dgAircraftDutyTypes.Rebind();
                                dgAircraftDutyTypes.SelectedIndexes.Add(key);
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                    }
                }
            }
        }
        /// <summary>
        /// Binding the colors for grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftDutyTypes_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {

                            GridDataItem dataItem = e.Item as GridDataItem;
                            dataItem.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ForeGrndCustomColor"), CultureInfo.CurrentCulture));
                            dataItem.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackgroundCustomColor"), CultureInfo.CurrentCulture));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        protected void dgAircraftDutyTypes_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAircraftDutyTypes.ClientSettings.Scrolling.ScrollTop = "0";
                        AircraftDutyTypePageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        protected void dgAircraftDutyTypes_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (AircraftDutyTypePageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgAircraftDutyTypes, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        #endregion
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges") > -1)
                        {

                            e.Updated = dgAircraftDutyTypes;
                            if (hdnSave.Value == "Delete" || hdnGridEnable.Value == "Save")
                            {
                                GridEnable(true, true, true);
                            }
                            //LoadDate();


                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        /// <summary>
        /// To Load Date
        /// </summary>
        private void LoadDate()
        {
            if (dgAircraftDutyTypes.SelectedItems.Count > 0)
            {
                GridDataItem item = dgAircraftDutyTypes.SelectedItems[0] as GridDataItem;
                Label lbLastUpdatedUser = (Label)dgAircraftDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                lbLastUpdatedUser = (Label)dgAircraftDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                if (item.GetDataKeyValue("LastUpdUID") != null)
                {
                    lbLastUpdatedUser.Text = HttpUtility.HtmlEncode("Last Updated User: " + item.GetDataKeyValue("LastUpdUID").ToString());
                }
                else
                {
                    lbLastUpdatedUser.Text = string.Empty;
                }
                if (item.GetDataKeyValue("LastUpdTS") != null)
                {
                    lbLastUpdatedUser.Text = HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                }
            }

        }
        /// <summary>
        /// Command Event Trigger for Save or Update Aircraft Duty Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (IsEmptyCheck)
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgAircraftDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgAircraftDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

                hdnSave.Value = "";
                if (!string.IsNullOrEmpty(hdnRedirect.Value))
                {
                    Response.Redirect(hdnRedirect.Value, false);
                }
            }
        }
        /// <summary>
        /// Cancel Aircraft Duty Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue = CommonService.UnLock(EntitySet.Database.AircraftDuty, Convert.ToInt64(Session["AircraftDutyID"]));
                        //Session.Remove("AircraftDutyID");
                        GridEnable(true, true, true);
                        DefaultSelection(true);
                        EnableLinks();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('rdDutyCodePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text.Trim() == string.Empty)
                        {
                            rfvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                        }
                        else if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
                        {
                            if (CheckAllReadyExist())
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                            }
                            if (CheckForReservedCode())
                            {
                                
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('R, F and G Codes Are Reserved For Internal Use by FlightPak.', 400, 50, 'Aircraft Duty Type');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        /// <summary>
        /// Function to Check if Aircraft Duty Type Code Alerady Exists
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.AircraftDuty> AircraftDutyList = new List<AircraftDuty>();
                    AircraftDutyList = ((List<FlightPakMasterService.AircraftDuty>)Session["DutyCodes"]).Where(x => x.AircraftDutyCD.Trim().ToUpper().ToString() == tbCode.Text.Trim().ToUpper().ToString()).ToList();
                    if (AircraftDutyList.Count != 0)
                    {
                        returnVal = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }
        /// <summary>
        /// CheckForReservedCode
        /// </summary>
        /// <returns></returns>
        private bool CheckForReservedCode()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    string dutyCode = tbCode.Text.ToUpper().Trim();
                    if (dutyCode.Trim() == "F" || dutyCode.Trim() == "G" || dutyCode.Trim() == "R")
                    {

                        returnVal = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }

        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.AircraftDuty);
                }
            }
        }
        #endregion

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.AircraftDuty> lstAircraftDuty = new List<FlightPakMasterService.AircraftDuty>();
                if (Session["DutyCodes"] != null)
                {
                    lstAircraftDuty = (List<FlightPakMasterService.AircraftDuty>)Session["DutyCodes"];
                }
                if (lstAircraftDuty.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstAircraftDuty = lstAircraftDuty.Where(x => x.IsInActive == false).ToList<AircraftDuty>(); }
                    dgAircraftDutyTypes.DataSource = lstAircraftDuty;
                    if (IsDataBind)
                    {
                        dgAircraftDutyTypes.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgAircraftDutyTypes.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var AircraftDutyValue = FPKMstService.GetAircraftDuty();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, AircraftDutyValue);
            List<FlightPakMasterService.AircraftDuty> filteredList = GetFilteredList(AircraftDutyValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.AircraftDutyID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgAircraftDutyTypes.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgAircraftDutyTypes.CurrentPageIndex = PageNumber;
            dgAircraftDutyTypes.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfAircraftDuty AircraftDutyValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["AircraftDutyID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = AircraftDutyValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().AircraftDutyID;
                Session["AircraftDutyID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.AircraftDuty> GetFilteredList(ReturnValueOfAircraftDuty AircraftDutyValue)
        {
            List<FlightPakMasterService.AircraftDuty> filteredList = new List<FlightPakMasterService.AircraftDuty>();

            if (AircraftDutyValue.ReturnFlag)
            {
                filteredList = AircraftDutyValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<AircraftDuty>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgAircraftDutyTypes.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgAircraftDutyTypes.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}