﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Settings.master"
    CodeBehind="FleetProfileEngineAirframeInfo.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.FleetProfileEngineAirframeInfo"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script runat="server">
       
        void QueryStringButton_Click(object sender, EventArgs e)
        {
            string URL = "../Fleet/FleetProfileCatalog.aspx?FleetID=" + hdnFleetID.Value + "&Screen=FleetProfile";
            Response.Redirect(URL);
        }

      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">
                        <asp:LinkButton ID="LinkButton1" Text="Fleet Profile " runat="server" OnClick="QueryStringButton_Click"></asp:LinkButton>&nbsp;>
                        Fleet Profile Engine Airframe Info</span> <span class="tab-nav-icons">
                            <%--<a href="#"
                            class="search-icon"></a><a href="#" class="save-icon"></a><a href="#" class="print-icon">
                            </a>--%><a href="#" title="Help" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" class="sticky">
        <tr style="display: none;">
            <td>
                <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
            </td>
        </tr>
        <tr>
            <td class="tdLabel100">
                Tail No.
            </td>
            <td>
                <asp:TextBox ID="tbTailNumber" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" id="tdSuccessMessage" class="success_msg" style="width: 50%;">
                Record saved successfully.
            </td>
            <td style="width: 50%;">
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="tblspace_10">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" class="border-box">
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td width="50%">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                </td>
                                                <td class="tdLabel60">
                                                    <span class="mnd_text">Hours</span>
                                                </td>
                                                <td class="tdLabel60">
                                                    <span class="mnd_text">Cycles/<br />Landings</span>
                                                </td>
                                                <td>
                                                    <span class="mnd_text">As Of</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Airframe:
                                                </td>
                                                <td class="tdLabel70">
                                                    <asp:TextBox ID="tbHoursAirFrame" runat="server" MaxLength="5" CssClass="text60"
                                                        onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td class="tdLabel60">
                                                    <telerik:RadNumericTextBox ID="tbcyclesairframe" runat="server" MinValue="0" MaxLength="6"
                                                        EmptyMessage="0" SelectionOnFocus="SelectAll">
                                                        <NumberFormat DecimalDigits="0" AllowRounding="false" GroupSeparator=""></NumberFormat>
                                                        <EnabledStyle HorizontalAlign="Right" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <uc:DatePicker ID="ucairframeasof" runat="server"></uc:DatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:RequiredFieldValidator ID="rfvHoursAirFrame" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbHoursAirFrame" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Airframe Hours  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvHoursAirFrame" runat="server" ControlToValidate="tbHoursAirFrame"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    Engine 1:
                                                </td>
                                                <td class="tdLabel70">
                                                    <asp:TextBox ID="tbHoursEngine1" runat="server" MaxLength="5" CssClass="text60" onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td class="tdLabel60">
                                                    <telerik:RadNumericTextBox ID="tbcyclesengine1" runat="server" MinValue="0" MaxLength="6"
                                                        EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                        <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                        <EnabledStyle HorizontalAlign="Right" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <uc:DatePicker ID="ucasofengine1" runat="server"></uc:DatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:RequiredFieldValidator ID="rfvHoursEngine1" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbHoursEngine1" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Engine1 Hours  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvHoursEngine1" runat="server" ControlToValidate="tbHoursEngine1"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    Engine 2:
                                                </td>
                                                <td class="tdLabel70">
                                                    <asp:TextBox ID="tbHoursEngine2" runat="server" MaxLength="5" CssClass="text60" onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td class="tdLabel60">
                                                    <telerik:RadNumericTextBox ID="tbcyclesengine2" runat="server" MinValue="0" MaxLength="6"
                                                        EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                        <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                        <EnabledStyle HorizontalAlign="Right" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <uc:DatePicker ID="ucasofengine2" runat="server"></uc:DatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:RequiredFieldValidator ID="rfvHoursEngine2" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbHoursEngine2" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Engine2 Hours  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvHoursEngine2" runat="server" ControlToValidate="tbHoursEngine2"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    Engine 3:
                                                </td>
                                                <td class="tdLabel70">
                                                    <asp:TextBox ID="tbHoursEngine3" runat="server" MaxLength="5" CssClass="text60" onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td class="tdLabel60">
                                                    <telerik:RadNumericTextBox ID="tbcyclesengine3" runat="server" MinValue="0" MaxLength="6"
                                                        EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                        <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                        <EnabledStyle HorizontalAlign="Right" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <uc:DatePicker ID="ucasofengine3" runat="server"></uc:DatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:RequiredFieldValidator ID="rfvHoursEngine3" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbHoursEngine3" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Engine3 Hours  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvHoursEngine3" runat="server" ControlToValidate="tbHoursEngine3"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    Engine 4:
                                                </td>
                                                <td class="tdLabel70">
                                                    <asp:TextBox ID="tbHoursEngine4" runat="server" MaxLength="5" CssClass="text60" onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td class="tdLabel60">
                                                    <telerik:RadNumericTextBox ID="tbcyclesengine4" runat="server" MinValue="0" MaxLength="6"
                                                        EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                        <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                        <EnabledStyle HorizontalAlign="Right" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <uc:DatePicker ID="ucasofengine4" runat="server"></uc:DatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:RequiredFieldValidator ID="rfvHoursEngine4" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbHoursEngine4" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Engine4 Hours  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvHoursEngine4" runat="server" ControlToValidate="tbHoursEngine4"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="50%" valign="top">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div class="nav-20">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                </td>
                                                <td class="tdLabel60">
                                                    <span class="mnd_text">Hours</span>
                                                </td>
                                                <td class="tdLabel60">
                                                    <span class="mnd_text">Cycles/<br /> Landings</span>
                                                </td>
                                                <td>
                                                    <span class="mnd_text">As Of</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Reverser 1:
                                                </td>
                                                <td class="tdLabel60">
                                                    <asp:TextBox ID="tbReserver1hours" runat="server" MaxLength="5" CssClass="text60"
                                                        onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td class="tdLabel60">
                                                    <telerik:RadNumericTextBox ID="tbReserver1cycles" runat="server" MinValue="0" MaxLength="6"
                                                        EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                        <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                        <EnabledStyle HorizontalAlign="Right" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <uc:DatePicker ID="ucreverser1asof" runat="server"></uc:DatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:RequiredFieldValidator ID="rfvReserver1hours" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbReserver1hours" Display="Dynamic" CssClass="alert-text"
                                                        SetFocusOnError="true" ErrorMessage="Reverser1 Hours  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvReserver1hours" runat="server" ControlToValidate="tbReserver1hours"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    Reverser 2:
                                                </td>
                                                <td class="tdLabel60">
                                                    <asp:TextBox ID="tbReserver2hours" runat="server" MaxLength="5" CssClass="text60"
                                                        onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td class="tdLabel60">
                                                    <telerik:RadNumericTextBox ID="tbReserver2cycles" runat="server" MinValue="0" MaxLength="6"
                                                        EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                        <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                        <EnabledStyle HorizontalAlign="Right" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <uc:DatePicker ID="ucreverser2asof" runat="server"></uc:DatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:RequiredFieldValidator ID="rfvReserver2hours" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbReserver2hours" Display="Dynamic" CssClass="alert-text"
                                                        SetFocusOnError="true" ErrorMessage="Reverser2 Hours  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvReserver2hours" runat="server" ControlToValidate="tbReserver2hours"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    Reverser 3:
                                                </td>
                                                <td class="tdLabel60">
                                                    <asp:TextBox ID="tbReserver3hours" runat="server" MaxLength="5" CssClass="text60"
                                                        onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td class="tdLabel60">
                                                    <telerik:RadNumericTextBox ID="tbReserver3cycles" runat="server" MinValue="0" MaxLength="6"
                                                        EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                        <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                        <EnabledStyle HorizontalAlign="Right" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <uc:DatePicker ID="ucreverser3asof" runat="server"></uc:DatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:RequiredFieldValidator ID="rfvReserver3hours" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbReserver3hours" Display="Dynamic" CssClass="alert-text"
                                                        SetFocusOnError="true" ErrorMessage="Reverser3 Hours  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvReserver3hours" runat="server" ControlToValidate="tbReserver3hours"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel80">
                                                    Reverser 4:
                                                </td>
                                                <td class="tdLabel60">
                                                    <asp:TextBox ID="tbReserver4hours" runat="server" MaxLength="5" CssClass="text60"
                                                        onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td class="tdLabel60">
                                                    <telerik:RadNumericTextBox ID="tbReserver4cycles" runat="server" MinValue="0" MaxLength="6"
                                                        EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                        <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                        <EnabledStyle HorizontalAlign="Right" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <uc:DatePicker ID="ucreverser4asof" runat="server"></uc:DatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:RequiredFieldValidator ID="rfvReserver4hours" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbReserver4hours" Display="Dynamic" CssClass="alert-text"
                                                        SetFocusOnError="true" ErrorMessage="Reverser4 Hours  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvReserver4hours" runat="server" ControlToValidate="tbReserver4hours"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Fuel Burn</legend>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <telerik:RadPanelBar ID="pnlFleetProfileCatalog" Width="100%" ExpandAnimation-Type="none"
                                                CollapseAnimation-Type="none" runat="server">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Constant Mach">
                                                        <ContentTemplate>
                                                            <table width="100%" class="box1">
                                                                <tr>
                                                                    <td>
                                                                        1st Hr.
                                                                    </td>
                                                                    <td>
                                                                        2nd Hr.
                                                                    </td>
                                                                    <td>
                                                                        3rd Hr.
                                                                    </td>
                                                                    <td>
                                                                        4th Hr.
                                                                    </td>
                                                                    <td>
                                                                        5th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb1stthhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb2ndhhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb3rdhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb4thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb5thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        6th Hr.
                                                                    </td>
                                                                    <td>
                                                                        7th Hr.
                                                                    </td>
                                                                    <td>
                                                                        8th Hr.
                                                                    </td>
                                                                    <td>
                                                                        9th Hr.
                                                                    </td>
                                                                    <td>
                                                                        10th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb6thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb7thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb8thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb9thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb10thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        11th Hr.
                                                                    </td>
                                                                    <td>
                                                                        12th Hr.
                                                                    </td>
                                                                    <td>
                                                                        13th Hr.
                                                                    </td>
                                                                    <td>
                                                                        14th Hr.
                                                                    </td>
                                                                    <td>
                                                                        15th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb11thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb12thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb13thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb14thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb15thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        16th Hr.
                                                                    </td>
                                                                    <td>
                                                                        17th Hr.
                                                                    </td>
                                                                    <td>
                                                                        18th Hr.
                                                                    </td>
                                                                    <td>
                                                                        19th Hr.
                                                                    </td>
                                                                    <td>
                                                                        20th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb16thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb17thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb18thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb19thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tb20thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                            <telerik:RadPanelBar ID="RadPanelBar1" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                                                runat="server">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Long Range Cruise">
                                                        <ContentTemplate>
                                                            <table width="100%" class="box1">
                                                                <tr>
                                                                    <td>
                                                                        1st Hr.
                                                                    </td>
                                                                    <td>
                                                                        2nd Hr.
                                                                    </td>
                                                                    <td>
                                                                        3rd Hr.
                                                                    </td>
                                                                    <td>
                                                                        4th Hr.
                                                                    </td>
                                                                    <td>
                                                                        5th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc1sthr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc2ndhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc3rdhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc4thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc5thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        6th Hr.
                                                                    </td>
                                                                    <td>
                                                                        7th Hr.
                                                                    </td>
                                                                    <td>
                                                                        8th Hr.
                                                                    </td>
                                                                    <td>
                                                                        9th Hr.
                                                                    </td>
                                                                    <td>
                                                                        10th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc6thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc7thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc8thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc9thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc10thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        11th Hr.
                                                                    </td>
                                                                    <td>
                                                                        12th Hr.
                                                                    </td>
                                                                    <td>
                                                                        13th Hr.
                                                                    </td>
                                                                    <td>
                                                                        14th Hr.
                                                                    </td>
                                                                    <td>
                                                                        15th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc11thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc12thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc13thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc14thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc15thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        16th Hr.
                                                                    </td>
                                                                    <td>
                                                                        17th Hr.
                                                                    </td>
                                                                    <td>
                                                                        18th Hr.
                                                                    </td>
                                                                    <td>
                                                                        19th Hr.
                                                                    </td>
                                                                    <td>
                                                                        20th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc16thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc17thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc18thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc19thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tblrc20thr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                            <telerik:RadPanelBar ID="RadPanelBar2" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                                                runat="server">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="High Speed Cruise">
                                                        <ContentTemplate>
                                                            <table class="box1" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        1st Hr.
                                                                    </td>
                                                                    <td>
                                                                        2nd Hr.
                                                                    </td>
                                                                    <td>
                                                                        3rd Hr.
                                                                    </td>
                                                                    <td>
                                                                        4th Hr.
                                                                    </td>
                                                                    <td>
                                                                        5th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc1sthr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc2ndhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc3rdhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc4thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc5thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        6th Hr.
                                                                    </td>
                                                                    <td>
                                                                        7th Hr.
                                                                    </td>
                                                                    <td>
                                                                        8th Hr.
                                                                    </td>
                                                                    <td>
                                                                        9th Hr.
                                                                    </td>
                                                                    <td>
                                                                        10th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc6thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc7thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc8thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc9thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc10thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        11th Hr.
                                                                    </td>
                                                                    <td>
                                                                        12th Hr.
                                                                    </td>
                                                                    <td>
                                                                        13th Hr.
                                                                    </td>
                                                                    <td>
                                                                        14th Hr.
                                                                    </td>
                                                                    <td>
                                                                        15th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc11thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc12thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc13thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc14thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc15thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        16th Hr.
                                                                    </td>
                                                                    <td>
                                                                        17th Hr.
                                                                    </td>
                                                                    <td>
                                                                        18th Hr.
                                                                    </td>
                                                                    <td>
                                                                        19th Hr.
                                                                    </td>
                                                                    <td>
                                                                        20th Hr.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc16thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc17thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc18thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc19thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadNumericTextBox ID="tbhsc20thhr" runat="server" MinValue="0" MaxLength="5"
                                                                            EmptyMessage="0" SelectionOnFocus="SelectAll" Width="50px">
                                                                            <NumberFormat DecimalDigits="0" AllowRounding="false"></NumberFormat>
                                                                            <EnabledStyle HorizontalAlign="Right" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="pad-top-btm" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="right">
                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                    <tr>
                        <td>
                            <asp:Button ID="btnsavechanges" ValidationGroup="save" OnClick="SaveChanges_Click"
                                runat="server" Text="Save" CssClass="button" />
                        </td>
                        <td>
                            <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="Cancel_Click"
                                CausesValidation="false" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                            <asp:HiddenField ID="hdnMode" runat="server" />
                            <asp:HiddenField ID="hdnPkeyId" runat="server" />
                            <asp:HiddenField ID="hdnFleetID" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
