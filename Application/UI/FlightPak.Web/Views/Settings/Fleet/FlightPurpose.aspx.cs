﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FlightPurpose : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private bool FlightPurposePageNavigated = false;
        private string strFlightPurposeCD = "";
        private string strFlightPurposeID = "";
        private List<string> lstFlightPurposeCodes = new List<string>();
        private bool _selectLastModified = false;
        #region constants
        private const string ConstIsPersonalTravel = "IsPersonalTravel";
        private const string ConstIsDefaultPurpose = "IsDefaultPurpose";
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFlightPurpose, dgFlightPurpose, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFlightPurpose.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewFlightPurposeReport);

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewFlightPurpose);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgFlightPurpose.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// To select the selected Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFlightPurposeID"] != null)
                    {
                        string ID = Session["SelectedFlightPurposeID"].ToString();
                        foreach (GridDataItem item in dgFlightPurpose.MasterTableView.Items)
                        {
                            if (item["FlightPurposeID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Datagrid Item Created for Flight Purpose Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgFlightPurpose.Rebind();
                    }
                    if (dgFlightPurpose.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedFlightPurposeID"] = null;
                        //}
                        if (Session["SelectedFlightPurposeID"] == null)
                        {
                            dgFlightPurpose.SelectedIndexes.Add(0);
                            Session["SelectedFlightPurposeID"] = dgFlightPurpose.Items[0].GetDataKeyValue("FlightPurposeID").ToString();
                        }

                        if (dgFlightPurpose.SelectedIndexes.Count == 0)
                            dgFlightPurpose.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Prerender method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightPurpose_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (FlightPurposePageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFlightPurpose, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFlightPurpose_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFlightPurpose.ClientSettings.Scrolling.ScrollTop = "0";
                        FlightPurposePageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// Datagrid Item Created for Flight Purpose Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightPurpose_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// Bind Flight Purpose Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightPurpose_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objFlightPurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objFlightPurposeVal = objFlightPurposeService.GetAllFlightPurposeList();
                            List<FlightPakMasterService.GetAllFlightPurposes> FlightPurposeList = new List<FlightPakMasterService.GetAllFlightPurposes>();
                            if (objFlightPurposeVal.ReturnFlag == true)
                            {
                                FlightPurposeList = objFlightPurposeVal.EntityList;
                            }
                            dgFlightPurpose.DataSource = FlightPurposeList;
                            Session["flightPurposeCodes"] = FlightPurposeList;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// To check unique Code on Tab out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            CheckAllReadyCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// To check unique client Code on Tab out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbClientCode.Text != null)
                        {
                            CheckAllReadyClientCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Command for Flight Purpose Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightPurpose_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedFlightPurposeID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.FlightPurpose, Convert.ToInt64(Session["SelectedFlightPurposeID"].ToString().Trim()));
                                        Session["IsFlightPurposeEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FlightPurpose);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        DisableLinks();
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDescription);
                                       // tbDescription.Focus();
                                        SelectItem();
                                        tbCode.Enabled = false;
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFlightPurpose.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCode);
                               // tbCode.Focus();
                                EnableForm(true);
                                DisableLinks();
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    dgFlightPurpose.Rebind();
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// Update Command for updating the values of Flight Purpose in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFlightPurpose_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedFlightPurposeID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objFlightPurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objFlightPurposeService.UpdateFlightPurpose(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    ///////Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.FlightPurpose, Convert.ToInt64(Session["SelectedFlightPurposeID"].ToString().Trim()));

                                        GridEnable(true, true, true);
                                        dgFlightPurpose.Rebind();
                                        DefaultSelection(false);

                                        ShowSuccessMessage();
                                        EnableLinks();
                                    }
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FlightPurpose);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// Save and Update Flight Purpose Form Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgFlightPurpose.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgFlightPurpose.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    
                    //This if block is implemented because LastUpdTS is not updating when a new Flight Purpose is added
                    //It is updating when a Flight Purpose is modified
                    if (!_selectLastModified)
                        hdnSave.Value = "";

                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// Cancel Flight Purpose Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedFlightPurposeID"] != null)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.FlightPurpose, Convert.ToInt64(Session["SelectedFlightPurposeID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        Session.Remove("SelectedItem");
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// Flight Purpose Insert Command for inserting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFlightPurpose_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAllReadyCodeExist())
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCode);
                           // tbCode.Focus();
                            IsValidate = false;
                        }
                        if (CheckAllReadyClientCodeExist())
                        {
                            cvClientCode.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                           // tbClientCode.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objFlightPurposeService = new MasterCatalogServiceClient())
                            {
                                var ReturnValue = objFlightPurposeService.AddFlightPurpose(GetItems());
                                if (ReturnValue.ReturnFlag == true)
                                {
                                    dgFlightPurpose.Rebind();
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.FlightPurpose);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        ///  Flight Purpose Delete Command for deleting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFlightPurpose_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedFlightPurposeID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient FlightPurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FlightPurpose FlightPurpose = new FlightPakMasterService.FlightPurpose();
                                string Code = Session["SelectedFlightPurposeID"].ToString();
                                foreach (GridDataItem Item in dgFlightPurpose.MasterTableView.Items)
                                {
                                    if (Item["FlightPurposeID"].Text.Trim() == Code.Trim())
                                    {
                                        if (Item.GetDataKeyValue("FlightPurposeCD") != null)
                                        {
                                            FlightPurpose.FlightPurposeCD = Item.GetDataKeyValue("FlightPurposeCD").ToString().Trim();
                                        }
                                        if (Item["FlightPurposeID"] != null)
                                        {
                                            FlightPurpose.FlightPurposeID = Convert.ToInt64(Item.GetDataKeyValue("FlightPurposeID").ToString().Trim());
                                            strFlightPurposeID = Item.GetDataKeyValue("FlightPurposeID").ToString().Trim();
                                        }
                                        break;
                                    }
                                }
                                FlightPurpose.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.FlightPurpose, Convert.ToInt64(strFlightPurposeID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FlightPurpose);
                                        return;
                                    }
                                    FlightPurposeService.DeleteFlightPurpose(FlightPurpose);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        if (Session["SelectedFlightPurposeID"] != null)
                        { 
                            var returnValue1 = CommonService.UnLock(EntitySet.Database.FlightPurpose, Convert.ToInt64(Session["SelectedFlightPurposeID"])); 
                        }
                        
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightPurpose_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgFlightPurpose.SelectedItems[0] as GridDataItem;
                                Session["SelectedFlightPurposeID"] = item["FlightPurposeID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                    }
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private FlightPakMasterService.FlightPurpose GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.FlightPurpose FlightPurpose = new FlightPakMasterService.FlightPurpose();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    FlightPurpose.FlightPurposeCD = tbCode.Text;
                    FlightPurpose.FlightPurposeDescription = tbDescription.Text;
                    FlightPurpose.IsPersonalTravel = chkPersonalTravel.Checked;
                    FlightPurpose.IsDefaultPurpose = chkDefaultPurpose.Checked;
                    FlightPurpose.IsInActive = chkInactive.Checked;
                    FlightPurpose.IsDeleted = false;
                    if (tbClientCode.Text != string.Empty)
                    {
                        FlightPurpose.ClientID = Convert.ToInt64(hdnclientCD.Value);
                    }
                    else
                    {
                        FlightPurpose.ClientID = null;
                    }
                    foreach (GridDataItem Item in dgFlightPurpose.MasterTableView.Items)
                    {
                        if (Item["FlightPurposeID"].Text.Trim() == Session["SelectedFlightPurposeID"].ToString().Trim())
                        {
                            FlightPurpose.FlightPurposeID = Convert.ToInt64(Item["FlightPurposeID"].Text.Trim());
                            break;
                        }
                    }
                    return FlightPurpose;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return FlightPurpose;
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFlightPurpose;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    dgFlightPurpose.Rebind();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    GridDataItem Item = dgFlightPurpose.SelectedItems[0] as GridDataItem;
                    Label lbLastUpdatedUser = (Label)dgFlightPurpose.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Load Selected Item Data into the Form Fields
        /// </summary>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFlightPurposeID"] != null)
                    {
                        strFlightPurposeID = "";
                        foreach (GridDataItem Item in dgFlightPurpose.MasterTableView.Items)
                        {
                            if (Item["FlightPurposeID"].Text.Trim() == Session["SelectedFlightPurposeID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("FlightPurposeCD") != null)
                                {
                                    tbCode.Text = Item["FlightPurposeCD"].Text.Trim();
                                }
                                else
                                {
                                    tbCode.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("FlightPurposeID") != null)
                                {
                                    strFlightPurposeID = Item["FlightPurposeID"].Text.Trim();
                                }
                                if (Item.GetDataKeyValue("FlightPurposeDescription") != null)
                                {
                                    tbDescription.Text = Item["FlightPurposeDescription"].Text.Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ClientID") != null)
                                {
                                    hdnclientCD.Value = Item.GetDataKeyValue("ClientID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("ClientCD") != null)
                                {
                                    tbClientCode.Text = Item.GetDataKeyValue("ClientCD").ToString().Trim();
                                }
                                else
                                {
                                    tbClientCode.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsDefaultPurpose") != null)
                                {
                                    chkDefaultPurpose.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsDefaultPurpose").ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkDefaultPurpose.Checked = false;
                                }
                                if (Item.GetDataKeyValue("IsPersonalTravel") != null)
                                {
                                    chkPersonalTravel.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsPersonalTravel").ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkPersonalTravel.Checked = false;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Flight Purpose Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["FlightPurposeCD"].Text;
                                lbColumnValue2.Text = Item["FlightPurposeDescription"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbClientCode.Text = string.Empty;
                    chkPersonalTravel.Checked = false;
                    chkDefaultPurpose.Checked = false;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Set Logged-in User Name
                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                    if (identity != null && identity.Identity._clientId != null && identity.Identity._clientId != 0)
                    {
                        tbClientCode.Enabled = false;
                        //need to change
                        tbClientCode.Text = identity.Identity._clientCd.ToString().Trim();
                        hdnclientCD.Value = identity.Identity._clientId.ToString().Trim();
                        btnHomeBase.Enabled = false;
                    }
                    else
                    {
                        tbClientCode.Enabled = enable;
                        btnHomeBase.Enabled = enable;
                    }
                    tbCode.Enabled = enable;
                    tbDescription.Enabled = enable;
                    //tbClientCode.Enabled = enable;
                    chkDefaultPurpose.Enabled = enable;
                    chkPersonalTravel.Enabled = enable;
                    btnSaveChanges.Visible = enable;
                    btnCancel.Visible = enable;
                    chkInactive.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check whether the code is unique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private bool CheckAllReadyCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                bool ReturnValue = false;
                exManager.Process(() =>
                {
                    if (Session["flightPurposeCodes"] != null)
                    {
                        List<GetAllFlightPurposes> FlightPurposeGroupList = new List<GetAllFlightPurposes>();
                        FlightPurposeGroupList = ((List<GetAllFlightPurposes>)Session["flightPurposeCodes"]).Where(x => x.FlightPurposeCD.ToString().ToUpper().Trim().Contains(tbCode.Text.ToString().ToUpper().Trim())).ToList<GetAllFlightPurposes>();
                        if (FlightPurposeGroupList.Count != 0)
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCode);
                            //tbCode.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            ReturnValue = true;
                        }
                        else
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDescription);
                            //tbDescription.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                        }
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// To Check Unique ClientCode
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyClientCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objFlightPurposesvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    bool ReturnVal = false;
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((tbClientCode.Text != string.Empty) && (tbClientCode.Text != null))
                        {
                            var objFlightPurposeVal = objFlightPurposesvc.GetClientCodeList().EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbClientCode.Text.ToString().ToUpper().Trim())).ToList();
                            if (objFlightPurposeVal.Count() == 0 || objFlightPurposeVal == null)
                            {
                                cvClientCode.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                               // tbClientCode.Focus();
                                ReturnVal = true;
                            }
                            else
                            {
                                hdnclientCD.Value = ((FlightPak.Web.FlightPakMasterService.Client)objFlightPurposeVal[0]).ClientID.ToString().Trim();
                                tbClientCode.Text = ((FlightPak.Web.FlightPakMasterService.Client)objFlightPurposeVal[0]).ClientCD;
                                ReturnVal = false;
                            }
                        }
                        else
                        {
                            hdnclientCD.Value = string.Empty;
                        }
                        return ReturnVal;

                    }, FlightPak.Common.Constants.Policy.UILayer);
                    return ReturnVal;
                }
            }
        }

        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);    
                }
            }
        }
        #endregion
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtninsertCtl, lbtndelCtl, lbtneditCtl;
                    lbtninsertCtl = (LinkButton)dgFlightPurpose.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    lbtndelCtl = (LinkButton)dgFlightPurpose.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    lbtneditCtl = (LinkButton)dgFlightPurpose.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddFlightPurpose))
                    {
                        lbtninsertCtl.Visible = true;
                        if (add)
                        {
                            lbtninsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtninsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtninsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteFlightPurpose))
                    {
                        lbtndelCtl.Visible = true;
                        if (delete)
                        {
                            lbtndelCtl.Enabled = true;
                            lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtndelCtl.Enabled = false;
                            lbtndelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtndelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditFlightPurpose))
                    {
                        lbtneditCtl.Visible = true;
                        if (edit)
                        {
                            lbtneditCtl.Enabled = true;
                            lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtneditCtl.Enabled = false;
                            lbtneditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtneditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllFlightPurposes> lstFlightPurposeCode = new List<FlightPakMasterService.GetAllFlightPurposes>();
                if (Session["flightPurposeCodes"] != null)
                {
                    lstFlightPurposeCode = (List<FlightPakMasterService.GetAllFlightPurposes>)Session["flightPurposeCodes"];
                }
                if (lstFlightPurposeCode.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstFlightPurposeCode = lstFlightPurposeCode.Where(x => x.IsInactive == false).ToList<GetAllFlightPurposes>(); }
                    if (chkIsDefaultPurpose.Checked == true) { lstFlightPurposeCode = lstFlightPurposeCode.Where(x => x.IsDefaultPurpose == true).ToList<GetAllFlightPurposes>(); }
                    if (chkIsPersonalTravel.Checked == true) { lstFlightPurposeCode = lstFlightPurposeCode.Where(x => x.IsPersonalTravel == true).ToList<GetAllFlightPurposes>(); }
                    dgFlightPurpose.DataSource = lstFlightPurposeCode;
                    if (IsDataBind)
                    {
                        dgFlightPurpose.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFlightPurpose.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var FlightPurposeValue = FPKMstService.GetAllFlightPurposeList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, FlightPurposeValue);
            List<FlightPakMasterService.GetAllFlightPurposes> filteredList = GetFilteredList(FlightPurposeValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FlightPurposeID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFlightPurpose.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFlightPurpose.CurrentPageIndex = PageNumber;
            dgFlightPurpose.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllFlightPurposes FlightPurposeValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedFlightPurposeID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                
                //This if else block and hdnSave.Value - "" are implemented because LastUpdTS is not updating
                //when a new Flight Purpose is added. It is updating when a Flight Purpose is modified
                if (hdnSave.Value == "Save")
                {
                    PrimaryKeyValue = FlightPurposeValue.EntityList.OrderByDescending(x => x.FlightPurposeID).FirstOrDefault().FlightPurposeID;
                }
                else
                {
                    PrimaryKeyValue = FlightPurposeValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FlightPurposeID;
                }

                Session["SelectedFlightPurposeID"] = PrimaryKeyValue;
                hdnSave.Value = "";
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllFlightPurposes> GetFilteredList(ReturnValueOfGetAllFlightPurposes FlightPurposeValue)
        {
            List<FlightPakMasterService.GetAllFlightPurposes> filteredList = new List<FlightPakMasterService.GetAllFlightPurposes>();

            if (FlightPurposeValue.ReturnFlag)
            {
                filteredList = FlightPurposeValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInactive == false).ToList<GetAllFlightPurposes>(); }
                    if (chkIsDefaultPurpose.Checked) { filteredList = filteredList.Where(x => x.IsDefaultPurpose == true).ToList<GetAllFlightPurposes>(); }
                    if (chkIsPersonalTravel.Checked) { filteredList = filteredList.Where(x => x.IsPersonalTravel == true).ToList<GetAllFlightPurposes>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFlightPurpose.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFlightPurpose.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}