﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Data;
using System.Text;
using System.Collections;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FeeScheduleCatalog : BaseSecuredPage
    {
        #region "VARIABLE DECLARATIONS"
        private ExceptionManager exManager;
        private string ModuleNameConstant = ModuleNameConstants.Database.FeeSchedule;
        private string EntitySetFeeSchedule = EntitySet.Database.FeeSchedule;
        private string ViewFeeSchedule = Permission.Database.ViewFBO;
        private string AddFeeSchedule = Permission.Database.AddFBO;
        private string EditFeeSchedule = Permission.Database.EditFBO;
        private string DeleteFeeSchedule = Permission.Database.DeleteFBO;
        private bool _selectLastModified = false;
        #endregion "VARIABLE DECLARATIONS"
        #region "PAGE EVENTS"
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFeeSchedule, dgFeeSchedule, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFeeSchedule.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(ViewFeeSchedule);
                            base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {                                
                                dgFeeSchedule.Rebind();
                                LoadControlData();
                                EnableForm(false);
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                PopUpSettings("Page_Load");
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFeeSchedule.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            FlightPakMasterService.FeeSchedule oGetFeeSchedule = new FlightPakMasterService.FeeSchedule();
            oGetFeeSchedule.FeeScheduleID = -1;
            oGetFeeSchedule.IsDeleted = false;

            var FeeScheduleValue = FPKMstService.GetFeeSchedule(oGetFeeSchedule);
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, FeeScheduleValue);
            List<FlightPakMasterService.GetFeeSchedule> filteredList = GetFilteredList(FeeScheduleValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FeeScheduleID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFeeSchedule.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFeeSchedule.CurrentPageIndex = PageNumber;
            dgFeeSchedule.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetFeeSchedule FeeScheduleValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["FeeScheduleID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = FeeScheduleValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FeeScheduleID;
                Session["FeeScheduleID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetFeeSchedule> GetFilteredList(ReturnValueOfGetFeeSchedule FeeScheduleValue)
        {
            List<FlightPakMasterService.GetFeeSchedule> filteredList = new List<FlightPakMasterService.GetFeeSchedule>();

            if (FeeScheduleValue.ReturnFlag)
            {
                filteredList = FeeScheduleValue.EntityList;
            }

            if (filteredList.Count > 0)
            {
                if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList(); }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFeeSchedule.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    SelectItem();
                    DefaultSelection(false);
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsPopUp)
                        {
                            // Set Master Page
                            this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                        }
                        PopUpSettings("Page_PreInit");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "PAGE EVENTS"
        #region "AJAX EVENTS"
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFeeSchedule;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "AJAX EVENTS"
        #region "USER DEFINED METHODS"
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgFeeSchedule.Rebind();
                    }
                    if (dgFeeSchedule.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["FeeScheduleID"] = null;
                        //}
                        if (Session["FeeScheduleID"] == null)
                        {
                            dgFeeSchedule.SelectedIndexes.Add(0);
                            Session["FeeScheduleID"] = dgFeeSchedule.Items[0].GetDataKeyValue("FeeScheduleID").ToString();
                        }

                        if (dgFeeSchedule.SelectedIndexes.Count == 0)
                            dgFeeSchedule.SelectedIndexes.Add(0);

                        LoadControlData();
                        EnableForm(false);
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        Session.Remove("FeeScheduleID");
                        ClearForm();
                        EnableForm(false);
                        GridEnable(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["FeeScheduleID"] != null)
                    {
                        string ID = Session["FeeScheduleID"].ToString();
                        foreach (GridDataItem Item in dgFeeSchedule.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("FeeScheduleID").ToString() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        if (dgFeeSchedule.SelectedItems.Count == 0)
                        {
                            DefaultSelection(false);
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton insertCtl, editCtl, delCtl;
                insertCtl = (LinkButton)dgFeeSchedule.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                editCtl = (LinkButton)dgFeeSchedule.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                delCtl = (LinkButton)dgFeeSchedule.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                if (IsAuthorized(AddFeeSchedule))
                {
                    insertCtl.Visible = true;
                    if (Add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(EditFeeSchedule))
                {
                    editCtl.Visible = true;
                    if (Edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
                if (IsAuthorized(DeleteFeeSchedule))
                {
                    delCtl.Visible = true;
                    if (Delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
            }
        }
        private void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {                
                chkInactive.Enabled = Enable;
                tbAccountNumber.Enabled = Enable;
                btnBrowseAccountNumber.Enabled = Enable;
                tbFeeGroup.Enabled = Enable;
                btnBrowseFeeGroup.Enabled = Enable;
                if (hdnSave.Value == "Update")
                {
                    tbAccountNumber.Enabled = false;
                    btnBrowseAccountNumber.Enabled = true;
                    tbFeeGroup.Enabled = false;
                    btnBrowseFeeGroup.Enabled = true;
                    btnBrowseAccountNumber.CssClass = "browse-button";
                    btnBrowseFeeGroup.CssClass = "browse-button";
                }
                tbAccountDescription.Enabled = false;
                tbAmount.Enabled = Enable;
                chkTaxable.Enabled = Enable;
                chkDiscount.Enabled = Enable;
                btnSaveChanges.Visible = Enable;
                btnCancel.Visible = Enable;
                chkSearchActiveOnly.Enabled = !(Enable);
                if (Enable)
                {
                    btnBrowseAccountNumber.CssClass = "browse-button";
                    btnBrowseFeeGroup.CssClass = "browse-button";
                }
                else
                {
                    btnBrowseAccountNumber.CssClass = "browse-button-disabled";
                    btnBrowseFeeGroup.CssClass = "browse-button-disabled";
                }
            }
        }
        private void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string EmptyString = string.Empty;
                hdnSave.Value = EmptyString;
                chkInactive.Checked = false;
                tbAccountNumber.Text = EmptyString;
                tbFeeGroup.Text = EmptyString;
                tbAccountDescription.Text = EmptyString;
                tbAmount.Text = "0.00";
                chkTaxable.Checked = false;
                chkDiscount.Checked = false;
            }
        }
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();
                if (dgFeeSchedule.SelectedItems.Count != 0)
                {
                    GridDataItem item = dgFeeSchedule.SelectedItems[0] as GridDataItem;
                    if (item.GetDataKeyValue("FeeScheduleID") != null)
                    {
                        hdnFeeScheduleID.Value = item.GetDataKeyValue("FeeScheduleID").ToString();
                    }
                    if (item.GetDataKeyValue("AccountNum") != null)
                    {
                        tbAccountNumber.Text = item.GetDataKeyValue("AccountNum").ToString();
                    }
                    if (item.GetDataKeyValue("AccountID") != null)
                    {
                        hdnAccountID.Value = item.GetDataKeyValue("AccountID").ToString();
                    }
                    if (item.GetDataKeyValue("AccountDescription") != null)
                    {
                        tbAccountDescription.Text = item.GetDataKeyValue("AccountDescription").ToString();
                    }
                    if (item.GetDataKeyValue("FeeGroupCD") != null)
                    {
                        tbFeeGroup.Text = item.GetDataKeyValue("FeeGroupCD").ToString();
                    }
                    if (item.GetDataKeyValue("FeeGroupID") != null)
                    {
                        hdnFeeGroupID.Value = item.GetDataKeyValue("FeeGroupID").ToString();
                    }
                    if (item.GetDataKeyValue("Amount") != null)
                    {
                        tbAmount.Text = item.GetDataKeyValue("Amount").ToString();
                    }
                    if (item.GetDataKeyValue("IsDiscount") != null)
                    {
                        chkDiscount.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsDiscount").ToString());
                    }
                    if (item.GetDataKeyValue("IsTaxable") != null)
                    {
                        chkTaxable.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsTaxable").ToString());
                    }
                    if (item.GetDataKeyValue("IsInActive") != null)
                    {
                        chkInactive.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsInActive").ToString());
                    }
                    lbColumnName1.Text = "Account No.";
                    lbColumnName2.Text = "Account Description";
                    lbColumnValue1.Text = item.GetDataKeyValue("AccountNum").ToString();
                    lbColumnValue2.Text = item.GetDataKeyValue("AccountDescription").ToString();

                    Label lbLastUpdatedUser;
                    lbLastUpdatedUser = (Label)dgFeeSchedule.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    }
                    if (item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.GetDataKeyValue("LastUpdTS").ToString())));
                    }
                }


            }
        }
        private FeeSchedule GetItems(FeeSchedule oFeeSchedule)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFeeSchedule))
            {
                if (hdnSave.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnFeeScheduleID.Value))
                    {
                        oFeeSchedule.FeeScheduleID = Convert.ToInt64(hdnFeeScheduleID.Value);
                    }
                }
                if (!string.IsNullOrEmpty(hdnAccountID.Value))
                {
                    oFeeSchedule.AccountID = Convert.ToInt64(hdnAccountID.Value);
                }
                if (!string.IsNullOrEmpty(hdnFeeGroupID.Value))
                {
                    oFeeSchedule.FeeGroupID = Convert.ToInt64(hdnFeeGroupID.Value);
                }
                if (!string.IsNullOrEmpty(tbAmount.Text))
                {
                    oFeeSchedule.Amount = Convert.ToDecimal(tbAmount.Text);
                }
                oFeeSchedule.IsInActive = chkInactive.Checked;
                oFeeSchedule.IsTaxable = chkTaxable.Checked;
                oFeeSchedule.IsDiscount = chkDiscount.Checked;
                oFeeSchedule.IsDeleted = false;
                return oFeeSchedule;
            }
        }
        private void PopUpSettings(string LoadType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LoadType))
            {
                if (Request.QueryString["IsPopupCatalog"] != null)
                {
                    if (LoadType == "Page_PreInit")
                    {
                        this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                    }
                    else if (LoadType == "Page_Load")
                    {
                        dgFeeSchedule.ClientSettings.ClientEvents.OnRowDblClick = "returnToParent";
                        divPopupButtons.Visible = true;
                        if (Request.QueryString["FeeScheduleID"] != null)
                        {
                            Session["FeeScheduleID"] = Request.QueryString["FeeScheduleID"].ToString();
                            SelectItem();
                        }
                    }
                }
            }
        }
        #endregion "USER DEFINED METHODS"
        #region "GRID EVENTS"
        private void BindData(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsDataBind))
            {
                FlightPakMasterService.FeeSchedule oGetFeeSchedule = new FlightPakMasterService.FeeSchedule();
                oGetFeeSchedule.FeeScheduleID = -1;
                oGetFeeSchedule.IsDeleted = false;
                //if (chkSearchActiveOnly.Checked == true)
                //{
                //    oGetFeeSchedule.IsInActive = chkSearchActiveOnly.Checked;
                //}
                List<FlightPakMasterService.GetFeeSchedule> GetFeeScheduleList = new List<FlightPakMasterService.GetFeeSchedule>();
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var GetFeeScheduleInfo = objService.GetFeeSchedule(oGetFeeSchedule);
                    if (GetFeeScheduleInfo.ReturnFlag == true)
                    {
                        if (chkSearchActiveOnly.Checked == true)
                        {
                            GetFeeScheduleList = GetFeeScheduleInfo.EntityList.Where( x=> x.IsInActive == false).ToList();
                        }
                        else
                        {
                            GetFeeScheduleList = GetFeeScheduleInfo.EntityList.ToList();
                        }
                    }
                    dgFeeSchedule.DataSource = GetFeeScheduleList;
                    if (IsDataBind)
                    {
                        dgFeeSchedule.DataBind();
                    }
                    Session["FeeScheduleList"] = GetFeeScheduleList;
                }
            }
        }

        protected void dgDelayType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFeeSchedule.ClientSettings.Scrolling.ScrollTop = "0";
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }

        protected void dgFeeSchedule_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void dgFeeSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgFeeSchedule.SelectedItems[0] as GridDataItem;
                                Session["FeeScheduleID"] = item.GetDataKeyValue("FeeScheduleID").ToString();
                                LoadControlData();
                                EnableForm(false);
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstant);
                    }
                }
            }
        }
        protected void dgFeeSchedule_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFeeSchedule.SelectedIndexes.Clear();
                                GridEnable(false, false, false);
                                ClearForm();
                                hdnSave.Value = "Save";
                                EnableForm(true);
                                Session.Remove("FeeScheduleID");
                                RadAjaxManager1.FocusControl(tbAccountNumber.ClientID);
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                //e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["FeeScheduleID"] != null)
                                    {
                                        var returnValue = CommonService.Lock(EntitySetFeeSchedule, Convert.ToInt64(Session["FeeScheduleID"].ToString()));
                                        Session["IsEditLockFeeSchedule"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstant);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstant);
                                            return;
                                        }
                                    }
                                }
                                GridEnable(false, false, false);
                                LoadControlData();
                                hdnSave.Value = "Update";
                                hdnRedirect.Value = "";
                                EnableForm(true);
                                RadAjaxManager1.FocusControl(tbAccountNumber.ClientID);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void dgFeeSchedule_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        IsValidateCustom = ValidateAll();
                        if (IsValidateCustom == true)
                        {
                            FlightPakMasterService.FeeSchedule oFeeSchedule = new FlightPakMasterService.FeeSchedule();
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                oFeeSchedule = GetItems(oFeeSchedule);
                                var Result = objService.AddFeeSchedule(oFeeSchedule);
                                if (Result.ReturnFlag == true)
                                {
                                    Session["FeeScheduleID"] = Result.EntityInfo.FeeScheduleID.ToString();
                                    ShowSuccessMessage();
                                    dgFeeSchedule.Rebind();
                                    EnableForm(false);
                                    SelectItem();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstant);
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
                finally
                {
                }
            }
        }
        protected void dgFeeSchedule_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        IsValidateCustom = ValidateAll();
                        if (IsValidateCustom == true)
                        {
                            FlightPakMasterService.FeeSchedule oFeeSchedule = new FlightPakMasterService.FeeSchedule();
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                oFeeSchedule = GetItems(oFeeSchedule);
                                var Result = objService.UpdateFeeSchedule(oFeeSchedule);
                                if (Result.ReturnFlag == true)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        if (Session["FeeScheduleID"] != null)
                                        {
                                            var returnValue = CommonService.UnLock(EntitySetFeeSchedule, Convert.ToInt64(Session["FeeScheduleID"].ToString()));
                                            Session["IsEditLockFeeSchedule"] = "False";
                                        }
                                    }
                                    ShowSuccessMessage();
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgFeeSchedule.Rebind();
                                    EnableForm(false);
                                    SelectItem();
                                    LoadControlData();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstant);
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
                finally
                {
                }
            }
        }
        protected void dgFeeSchedule_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["FeeScheduleID"] != null)
                        {
                            FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient();
                            FlightPakMasterService.FeeSchedule oFeeSchedule = new FlightPakMasterService.FeeSchedule();
                            oFeeSchedule.FeeScheduleID = Convert.ToInt64(Session["FeeScheduleID"].ToString());
                            oFeeSchedule.IsDeleted = true;
                            //Lock will happen from UI
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySetFeeSchedule, oFeeSchedule.FeeScheduleID);
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                    if (IsPopUp)
                                        ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstant);
                                    else
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstant);
                                    return;
                                }
                                objService.DeleteFeeSchedule(oFeeSchedule);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        if (Session["FeeScheduleID"] != null)
                        {
                            //Unlock should happen from Service Layer
                            var returnValue1 = CommonService.UnLock(EntitySetFeeSchedule, Convert.ToInt64(Session["FeeScheduleID"].ToString()));
                        }
                    }
                }
            }
        }
        protected void dgFeeSchedule_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "GRID EVENTS"
        #region "CONTROL EVENTS"
        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgFeeSchedule.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgFeeSchedule.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["FeeScheduleID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySetFeeSchedule, Convert.ToInt64(Session["FeeScheduleID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radFeeGroupPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindData(true);
                        DefaultSelection(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void tbAccountNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        return CheckAccountNumberExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void tbFeeGroup_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        return CheckFeeGroupExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "CONTROL EVENTS"
        #region "VALIDATIONS"
        private bool ValidateAll()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool IsValidateCustom = true;
                if ((CheckAccountNumberExist() == false) && (IsValidateCustom == true))
                {
                    IsValidateCustom = false;
                }
                if ((CheckFeeGroupExist() == false) && (IsValidateCustom == true))
                {
                    IsValidateCustom = false;
                }
                if (hdnSave.Value == "Save")
                {
                    if ((CheckAccountNumberFeeGroupUnique() == false) && (IsValidateCustom == true))
                    {
                        IsValidateCustom = false;
                    }
                }
                return IsValidateCustom;
            }
        }
        private bool CheckAccountNumberExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(tbAccountNumber.Text.Trim()))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objService.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper() == tbAccountNumber.Text.Trim().ToUpper()).ToList();
                        if (objRetVal.Count != 0)
                        {
                            List<FlightPakMasterService.Account> AccountList = new List<FlightPakMasterService.Account>();
                            AccountList = (List<FlightPakMasterService.Account>)objRetVal.ToList();
                            hdnAccountID.Value = AccountList[0].AccountID.ToString();
                            tbAccountDescription.Text = AccountList[0].AccountDescription;
                            RadAjaxManager1.FocusControl(tbFeeGroup.ClientID);
                            ReturnValue = true;
                        }
                        else
                        {
                            cvAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAccountNumber.ClientID);
                            ReturnValue = false;
                        }
                    }
                }
                else
                {
                    rfvAccountNumber.IsValid = false;
                    ReturnValue = false;
                }
                return ReturnValue;
            }
        }
        private bool CheckFeeGroupExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(tbFeeGroup.Text.Trim()))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objService.GetFeeGroupList().EntityList.Where(x => x.FeeGroupCD.Trim().ToUpper() == tbFeeGroup.Text.Trim().ToUpper()).ToList();
                        if (objRetVal.Count != 0)
                        {
                            List<FlightPakMasterService.FeeGroup> FeeGroupList = new List<FlightPakMasterService.FeeGroup>();
                            FeeGroupList = (List<FlightPakMasterService.FeeGroup>)objRetVal.ToList();
                            hdnFeeGroupID.Value = FeeGroupList[0].FeeGroupID.ToString();
                            tbFeeGroup.Text = FeeGroupList[0].FeeGroupCD;
                            RadAjaxManager1.FocusControl(tbAmount.ClientID);
                            ReturnValue = true;
                        }
                        else
                        {
                            cvFeeGroup.IsValid = false;
                            RadAjaxManager1.FocusControl(tbFeeGroup.ClientID);
                            ReturnValue = false;
                        }
                    }
                }
                else
                {
                    rfvFeeGroup.IsValid = false;
                    ReturnValue = false;
                }
                return ReturnValue;
            }
        }
        private bool CheckAccountNumberFeeGroupUnique()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if ((!string.IsNullOrEmpty(hdnAccountID.Value)) && (!string.IsNullOrEmpty(hdnFeeGroupID.Value)))
                {
                    FlightPakMasterService.FeeSchedule oGetFeeSchedule = new FlightPakMasterService.FeeSchedule();
                    oGetFeeSchedule.FeeScheduleID = -1;
                    oGetFeeSchedule.IsDeleted = false;
                    List<FlightPakMasterService.GetFeeSchedule> GetFeeScheduleList = new List<FlightPakMasterService.GetFeeSchedule>();
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var GetFeeScheduleInfo = objService.GetFeeSchedule(oGetFeeSchedule).EntityList.Where(x => x.AccountID == Convert.ToInt64(hdnAccountID.Value) && x.FeeGroupID == Convert.ToInt64(hdnFeeGroupID.Value)).ToList();
                        if (GetFeeScheduleInfo.Count > 0)
                        {
                            ReturnValue = false;
                            
                            string alertMsg = "var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Fee Schedule Group Code and Account Number Combination Must Be Unique.', 360, 50, '" + ModuleNameConstant + "');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                    }
                }
                return ReturnValue;
            }
        }
        #endregion "VALIDATIONS"

        protected void dgFeeSchedule_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFeeSchedule, Page.Session);
        }
    }
}