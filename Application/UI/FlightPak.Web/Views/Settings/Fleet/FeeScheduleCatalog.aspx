﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeeScheduleCatalog.aspx.cs"
    MasterPageFile="~/Framework/Masters/Settings.master" Inherits="FlightPak.Web.Views.Settings.Fleet.FeeScheduleCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">
            // For Popup Starts here
            var oArg = new Object();

            function rebindgrid() {
                var masterTable = $find("<%= dgFeeSchedule.ClientID %>").get_masterTableView();
                masterTable.rebind();
                masterTable.clearSelectedItems();
                masterTable.selectItem(masterTable.get_dataItems()[0].get_element());
            }

            //this function is used to get the selected code
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgFeeSchedule.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                if (selectedRows.length <= 0) {
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radalert("Please select the record", 360, 50, "Fee Schedule");
                    return;
                }
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "FeeScheduleID");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "AccountNum");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "FeeGroupCD");
                }

                if (selectedRows.length > 0) {
                    oArg.FeeScheduleID = cell1.innerHTML;
                    oArg.AccountNum = cell2.innerHTML;
                    oArg.FeeGroupCD = cell3.innerHTML;
                }
                else {
                    oArg.FeeScheduleID = "";
                    oArg.AccountNum = "";
                    oArg.FeeGroupCD = "";

                }

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            //this function is used to close this window
            function Close() {
                GetRadWindow().Close();
            }
            // For Popup Ends here

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function openWin(radWin) {
                var url = "";
                if (radWin == "radAccountMasterPopup") {
                    url = "../Company/AccountMasterPopup.aspx?AccountNum=" + document.getElementById('<%=tbAccountNumber.ClientID%>').value;
                }
                else if (radWin == "radFeeGroupPopup") {
                    url = "../Company/FeeScheduleGroupPopup.aspx?Code=" + document.getElementById('<%=tbFeeGroup.ClientID%>').value;
                }
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }

            function OnClientCloseAccountPopup(oWnd, args) {
                var combo = $find("<%= tbAccountNumber.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAccountNumber.ClientID%>").value = arg.AccountNum;
                        document.getElementById("<%=hdnAccountID.ClientID%>").value = arg.AccountID;
                        document.getElementById("<%=tbAccountDescription.ClientID%>").value = arg.AccountDescription;
                        document.getElementById("<%=cvAccountNumber.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbAccountNumber.ClientID%>").value = "";
                        document.getElementById("<%=hdnAccountID.ClientID%>").value = "";
                        document.getElementById("<%=tbAccountDescription.ClientID%>").value = "";
                        document.getElementById("<%=cvAccountNumber.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                var step = "tbAccountNumber_TextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function OnClientCloseFeeGroupPopup(oWnd, args) {
                var combo = $find("<%= tbFeeGroup.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbFeeGroup.ClientID%>").value = arg.FeeGroupCD;
                        document.getElementById("<%=hdnFeeGroupID.ClientID%>").value = arg.FeeGroupID;
                        document.getElementById("<%=cvFeeGroup.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbFeeGroup.ClientID%>").value = "";
                        document.getElementById("<%=hdnFeeGroupID.ClientID%>").value = "";
                        document.getElementById("<%=cvFeeGroup.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                var step = "tbFeeGroup_TextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function tbAmount_onblur(sender, e) {
                var ReturnValue = true;
                if (sender.value.indexOf(".") == -1) {
                    sender.value = sender.value + ".00";
                }
                return ReturnValue;
            }
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFeeSchedule">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgFeeSchedule" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAccountPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx"
                VisibleStatusbar="false" />
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx"
                VisibleStatusbar="false" />
            <telerik:RadWindow ID="radFeeGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFeeGroupPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" NavigateUrl="~/Views/Settings/Company/FeeScheduleGroupPopup.aspx"
                VisibleStatusbar="false" />
            <telerik:RadWindow ID="radFeeGroupCRUDPopup" runat="server" Height="275px" Width="1050px"
                OnClientClose="OnClientCloseFeeGroupPopup" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" NavigateUrl="~/Views/Settings/Company/FeeScheduleGroupCatalog.aspx"
                VisibleStatusbar="false" />
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fee Schedule</span> <span class="tab-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=FeeScheduleHelp"
                        class="help-icon" target="_blank" title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgFeeSchedule" runat="server" AllowSorting="true" OnNeedDataSource="dgFeeSchedule_BindData"
                OnItemCommand="dgFeeSchedule_ItemCommand" OnUpdateCommand="dgFeeSchedule_UpdateCommand"
                OnInsertCommand="dgFeeSchedule_InsertCommand" OnDeleteCommand="dgFeeSchedule_DeleteCommand"
                OnPageIndexChanged="dgDelayType_PageIndexChanged" OnSelectedIndexChanged="dgFeeSchedule_SelectedIndexChanged"
                AutoGenerateColumns="false" PageSize="10" Height="341px" AllowPaging="true" AllowFilteringByColumn="true"
                PagerStyle-AlwaysVisible="true" OnPreRender="dgFeeSchedule_PreRender">
                <MasterTableView DataKeyNames="FeeScheduleID,CustomerID,FeeGroupID,AccountID,Amount,IsTaxable,IsDiscount,IsInActive,
                                                LastUpdUID,LastUpdTS,IsDeleted,AccountNum,AccountDescription,FeeGroupCD,FeeGroupDescription"
                    ClientDataKeyNames="FeeScheduleID" CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="FeeScheduleID" HeaderText="FeeScheduleID" UniqueName="FeeScheduleID"
                            Display="false" />
                        <telerik:GridBoundColumn DataField="AccountNum" HeaderText="Account No." UniqueName="AccountNum"
                            AutoPostBackOnFilter="false" HeaderStyle-Width="180px" FilterControlWidth="160px"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500" />
                        <telerik:GridBoundColumn DataField="AccountDescription" HeaderText="Account Description"
                            UniqueName="AccountDescription" AutoPostBackOnFilter="false" HeaderStyle-Width="240px"
                            FilterControlWidth="220px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500" />
                        <telerik:GridBoundColumn DataField="Amount" HeaderText="Default Amount" UniqueName="Amount"
                            AutoPostBackOnFilter="false" HeaderStyle-Width="120px" FilterControlWidth="100px"
                            ShowFilterIcon="false" CurrentFilterFunction="EqualTo" FilterDelay="500" />
                        <telerik:GridCheckBoxColumn DataField="IsTaxable" HeaderText="Tax" UniqueName="IsTaxable"
                            AutoPostBackOnFilter="true" HeaderStyle-Width="50px" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                            AllowFiltering="false" />
                        <telerik:GridBoundColumn DataField="FeeGroupCD" HeaderText="Group" UniqueName="FeeGroupCD"
                            AutoPostBackOnFilter="false" HeaderStyle-Width="80px" FilterControlWidth="60px"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500" />
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" UniqueName="IsInActive"
                            AutoPostBackOnFilter="true" HeaderStyle-Width="80px" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                            AllowFiltering="false" />
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <div id="divPopupButtons" runat="server" style="text-align: right;" visible="false">
                <asp:Button ID="btnSelect" Text="Ok" runat="server" CssClass="button" OnClientClick="returnToParent(); return false;" />
            </div>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkInactive" runat="server" Text="Inactive" Font-Size="12px" ForeColor="Black" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    <span class="mnd_text">Account No.</span>
                                </td>
                                <td class="tdLabel180">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbAccountNumber" AutoPostBack="true" OnTextChanged="tbAccountNumber_TextChanged"
                                                    runat="server" MaxLength="8" CssClass="text100" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                                <asp:HiddenField ID="hdnAccountID" runat="server" />
                                                <asp:Button ID="btnBrowseAccountNumber" OnClientClick="javascript:openWin('radAccountMasterPopup');return false;"
                                                    CssClass="browse-button" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvAccountNumber" runat="server" ErrorMessage="Account No. is Required."
                                                    Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAccountNumber" ValidationGroup="Save" />
                                                <asp:CustomValidator ID="cvAccountNumber" runat="server" ControlToValidate="tbAccountNumber"
                                                    ErrorMessage="Invalid Account No." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                    SetFocusOnError="true" />
                                                <%-- <asp:RegularExpressionValidator ID="regAccountNumber" runat="server" ErrorMessage="Invalid Account No. Format"
                                                    Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAccountNumber" ValidationGroup="Save"
                                                    ValidationExpression="^[0-9]{0,4}\.?[0-9]{0,3}$" />--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel70" valign="top">
                                    <span class="mnd_text">Group</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbFeeGroup" AutoPostBack="true" OnTextChanged="tbFeeGroup_TextChanged"
                                                    runat="server" MaxLength="4" CssClass="text50"></asp:TextBox>
                                                <asp:HiddenField ID="hdnFeeGroupID" runat="server" />
                                                <asp:Button ID="btnBrowseFeeGroup" OnClientClick="javascript:openWin('radFeeGroupPopup');return false;"
                                                    CssClass="browse-button" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvFeeGroup" runat="server" ErrorMessage="Fee Schedule Group Code is Required."
                                                    Display="Dynamic" CssClass="alert-text" ControlToValidate="tbFeeGroup" ValidationGroup="Save" />
                                                <asp:CustomValidator ID="cvFeeGroup" runat="server" ControlToValidate="tbFeeGroup"
                                                    ErrorMessage="Invalid Fee Schedule Group Code." Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="Save" SetFocusOnError="true" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    <span>Account Description</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbAccountDescription" runat="server" MaxLength="40" CssClass="text300"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    <span>Default Amount</span>
                                </td>
                                <td class="tdLabel170">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="pr_radtextbox_120">
                                                <%--<asp:TextBox ID="tbAmount" runat="server" MaxLength="17" CssClass="text120" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                    onblur="javascript:return tbAmount_onblur(this, event);"></asp:TextBox>--%>
                                                <telerik:RadNumericTextBox ID="tbAmount" runat="server" Type="Currency" Culture="en-US"
                                                    MaxLength="14" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                </telerik:RadNumericTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%--<asp:RegularExpressionValidator ID="regAmount" runat="server" ErrorMessage="Invalid Format"
                                                    Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAmount" ValidationGroup="Save"
                                                    ValidationExpression="^[0-9]{0,14}\.?[0-9]{0,2}$" />--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel70">
                                    <asp:CheckBox ID="chkTaxable" runat="server" Text="Taxable" Font-Size="12px" ForeColor="Black" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkDiscount" runat="server" Text="Discount" Font-Size="12px" ForeColor="Black" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveChanges" Text="Save" ValidationGroup="Save" runat="server"
                            OnClick="btnSaveChanges_Click" CssClass="button" />
                        <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                            OnClick="btnCancel_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnFeeScheduleID" runat="server" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
