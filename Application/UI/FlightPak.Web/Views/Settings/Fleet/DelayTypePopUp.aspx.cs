﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class DelatTypePopUp : BaseSecuredPage
    {
        private string DelayCode;
        private ExceptionManager exManager;
        public bool showCommandItems = true;
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        showCommandItems = true;
                        //Added for Reassign the Selected Value and highlight the specified row in the Grid
                        if (!string.IsNullOrEmpty(Request.QueryString["DelayTypeCD"]))
                        {
                            DelayCode = Request.QueryString["DelayTypeCD"].ToUpper().Trim();
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["fromPage"]) && Request.QueryString["fromPage"] == "SystemTools")
                        {
                            showCommandItems = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }

        }

        /// <summary>
        /// Bind Delay Code Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgDelayType_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objDelayTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            //var objDelVal = objDelayTypeService.GetDelayTypeList();
                            //List<FlightPakMasterService.DelayType> lstDelayType = new List<FlightPakMasterService.DelayType>();
                            //if (objDelVal.ReturnFlag == true)
                            //{
                            //    lstDelayType = objDelVal.EntityList;
                            //}
                            //dgDelayType.DataSource = lstDelayType;
                            //Session["DelayTypeList"] = lstDelayType;
                            //if (chkSearchActiveOnly.Checked == true)
                            //{
                                SearchAndFilter(false);
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }

        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgDelayType;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }

        }
        private void SelectItem()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(DelayCode))
                {

                    foreach (GridDataItem Item in dgDelayType.MasterTableView.Items)
                    {
                        if (Item["DelayTypeCD"].Text.Trim().ToUpper() == DelayCode)
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgDelayType.MasterTableView.Items.Count > 0)
                    {
                        if (Session["SelectedDelayTypeID"] != null)
                        {
                            string ID = Session["SelectedDelayTypeID"].ToString();
                            foreach (GridDataItem Item in dgDelayType.MasterTableView.Items)
                            {
                                if (Item.GetDataKeyValue("DelayTypeID").ToString().Trim() == ID)
                                {
                                    Item.Selected = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            dgDelayType.SelectedIndexes.Add(0);
                        }
                    }
                }

            }

        }

        protected void dgDelayType_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    string resolvedurl = string.Empty;
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                //e.Item.Selected = true;

                                GridDataItem item = dgDelayType.SelectedItems[0] as GridDataItem;
                                Session["SelectedDelayTypeID"] = item["DelayTypeID"].Text;
                                TryResolveUrl("/Views/Settings/Fleet/DelayTypeCatalog.aspx?IsPopup=", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "ProcessPopupAdd('AccountsCatalog.aspx?IsPopup=', 'Add Accounts', '1000','500', '50', '50');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radDelayTypeCRUDPopup');", true);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/Fleet/DelayTypeCatalog.aspx?IsPopup=Add", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radDelayTypeCRUDPopup');", true);
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void dgDelayType_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string SelectedDelayTypeID = string.Empty;
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            GridDataItem item = dgDelayType.SelectedItems[0] as GridDataItem;
                            SelectedDelayTypeID = item["DelayTypeID"].Text;
                            DelayType DelayType = new DelayType();
                            string DelayTypeID = SelectedDelayTypeID;
                            //string CrewCD = string.Empty;
                            DelayType.DelayTypeID = Convert.ToInt64(DelayTypeID);
                            //Crew.CrewCD = item["CrewCD"].Text;                    // Doubt
                            DelayType.IsDeleted = true;
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.Crew, Convert.ToInt64(SelectedDelayTypeID));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.DelayType);
                                    return;
                                }
                            }
                            CrewRosterService.DeleteDelayType(DelayType);
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.DelayType);
                }
                finally
                {
                    if (Session["SelectedDelayTypeID"] != null)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.DelayType, Convert.ToInt64(SelectedDelayTypeID));
                        }
                    }
                }

            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            long DelayTypeID = 0;
            string DelayTypeCD = string.Empty;
            bool FetchActiveOnly = false;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //List<FlightPakMasterService.DelayType> lstDelayType = new List<FlightPakMasterService.DelayType>();
                //if (Session["DelayTypeList"] != null)
                //{
                //    lstDelayType = (List<FlightPakMasterService.DelayType>)Session["DelayTypeList"];
                //}
                //if (lstDelayType.Count != 0)
                //{
                //    if (chkSearchActiveOnly.Checked == true) { lstDelayType = lstDelayType.Where(x => x.IsInActive == false).ToList<DelayType>(); }
                //    dgDelayType.DataSource = lstDelayType;
                //    if (IsDataBind)
                //    {
                //        dgDelayType.DataBind();
                //    }
                //}
                using (FlightPakMasterService.MasterCatalogServiceClient objDelayTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    if (chkSearchActiveOnly.Checked == true)
                    {
                        FetchActiveOnly = true;
                    }

                    var objDelVal = objDelayTypeService.GetAllDelayTypeWithFilters(DelayTypeID, DelayTypeCD, FetchActiveOnly);
                    dgDelayType.DataSource = objDelVal.EntityList;
                    if (IsDataBind)
                    {
                        dgDelayType.DataBind();
                    }
                }
                return false;
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            SearchAndFilter(true);
        }
        #endregion

        protected void dgDelayType_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgDelayType, Page.Session);
        }
    }
}
