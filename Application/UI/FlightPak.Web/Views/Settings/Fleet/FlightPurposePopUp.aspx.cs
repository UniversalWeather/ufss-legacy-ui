﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FlightPurposePopUp : BaseSecuredPage
    {
        private string FlightPurposeCD;
        private string FlightDefaultPurpose;
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Added for Reassign the Selected Value and highlight the specified row in the Grid            
                        if (!string.IsNullOrEmpty(Request.QueryString["FlightPurposeCD"]))
                        {
                            FlightPurposeCD = Request.QueryString["FlightPurposeCD"].ToUpper().Trim();

                        }
                        if (Request.QueryString["FDP"] != null)
                        {
                            FlightDefaultPurpose = Request.QueryString["FDP"];
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }

        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgFlightPurpose_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            //var ObjRetVal = ObjService.GetFlightPurposeList();
                            var ObjRetVal = ObjService.GetAllFlightPurposeList();
                            List<FlightPakMasterService.GetAllFlightPurposes> lstFlightPurpose = new List<FlightPakMasterService.GetAllFlightPurposes>();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                lstFlightPurpose = ObjRetVal.EntityList;
                            }
                            dgFlightPurpose.DataSource = lstFlightPurpose;
                            Session["FlightPurposeList"] = lstFlightPurpose;
                            if (chkSearchActiveOnly.Checked == true)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }

        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgFlightPurpose;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }

        }
        private void SelectItem()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(FlightPurposeCD))
                {

                    foreach (GridDataItem item in dgFlightPurpose.MasterTableView.Items)
                    {
                        if (!string.IsNullOrEmpty(FlightDefaultPurpose))
                        {
                            if (item["IsDefaultPurpose"].Text.Trim().ToUpper() == "TRUE")
                            {
                                item.Selected = true;
                                break;
                            }

                        }
                        else
                        {

                            if (item["FlightPurposeCD"].Text.Trim().ToUpper() == FlightPurposeCD)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(FlightDefaultPurpose))
                    {
                        foreach (GridDataItem item in dgFlightPurpose.MasterTableView.Items)
                        {
                            if (item["IsDefaultPurpose"] != null)
                            {
                                if (item["IsDefaultPurpose"].Text.Trim().ToUpper() == "TRUE")
                                {
                                    item.Selected = true;
                                    break;
                                }
                            }
                        }

                    }
                    else
                    {
                        if (dgFlightPurpose.MasterTableView.Items.Count > 0)
                        {
                            dgFlightPurpose.SelectedIndexes.Add(0);

                        }
                    }
                }

            }

        }
        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllFlightPurposes> lstFlightPurpose = new List<FlightPakMasterService.GetAllFlightPurposes>();
                if (Session["FlightPurposeList"] != null)
                {
                    lstFlightPurpose = (List<FlightPakMasterService.GetAllFlightPurposes>)Session["FlightPurposeList"];
                }
                if (lstFlightPurpose.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstFlightPurpose = lstFlightPurpose.Where(x => x.IsInactive == false).ToList<GetAllFlightPurposes>(); }
                    dgFlightPurpose.DataSource = lstFlightPurpose;
                    if (IsDataBind)
                    {
                        dgFlightPurpose.DataBind();
                    }
                }
                return false;
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            SearchAndFilter(true);
        }
        #endregion

        protected void dgFlightPurpose_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgFlightPurpose_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.GridFilterSessionName,
                dgFlightPurpose, Page.Session);

        }
    }
}