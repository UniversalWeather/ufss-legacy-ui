﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FleetProfilePopupReports.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Fleet.FleetProfilePopupReports" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="format-detection" content="telephone=no" />
    <title>Fleet Profile</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgFleetPopup.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {

                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgFleetPopup.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;

                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var FleetId = MasterTable.getCellByColumnUniqueName(row, "FleetID");
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "TailNum");
                        if (i == 0) {
                            oArg.TailNum = cell1.innerHTML + ",";
                            oArg.FleetId = FleetId.innerHTML;
                        }
                        else {
                            oArg.TailNum += cell1.innerHTML + ",";
                            oArg.FleetId += FleetId.innerHTML;
                        }
                    }
                }
                else {
                    oArg.FleetId = "0";
                    oArg.TailNum = "";
                }
                if (oArg.TailNum != "") {
                    oArg.TailNum = oArg.TailNum.substring(0, oArg.TailNum.length - 1)
                }
                else {
                    oArg.TailNum = "";
                }
                oArg.Arg1 = oArg.TailNum;
                oArg.CallingButton = "TailNum";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
                return false;
            }

            function Close() {
                GetRadWindow().Close();
            }
            function GetGridId() {
                return $find("<%= dgFleetPopup.ClientID %>");
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgFleetPopup.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgFleetPopup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetPopup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkAllActive">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetPopup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkAllInactive">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetPopup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table cellpadding="2" cellspacing="2">
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkAllActive" runat="server" AutoPostBack="true" Text="All Active"
                                    OnCheckedChanged="chkAllActive_CheckedChanged" />
                            </td>
                            <td>
                                <asp:CheckBox ID="chkAllInactive" runat="server" Text="All Inactive" AutoPostBack="true"
                                    OnCheckedChanged="chkAllInactive_CheckedChanged" />
                            </td>
                            <td>
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button" OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgFleetPopup" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
                        OnNeedDataSource="dgFleetPopup_BindData" OnItemDataBound="dgFleetPopup_ItemDatabound"
                        AutoGenerateColumns="false" Height="330px" AllowPaging="false" Width="650px"
                        OnSelectedIndexChanged="CrewRoster_SelectedIndexChanged" OnItemCommand="dgFleetPopup_ItemCommand"
                        OnItemCreated="dgFleetPopup_ItemCreated" OnPreRender="dgFleetPopup_PreRender">
                        <MasterTableView DataKeyNames="FleetID,AircraftCD,TailNum,AirCraft_AircraftCD,IsInActive,LastUpdUID,LastUpdTS,IcaoID,PowerSettings1TrueAirSpeed,PowerSettings2TrueAirSpeed,PowerSettings3TrueAirSpeed"
                            CommandItemDisplay="None" AllowPaging="false">
                            <Columns>
                                 <telerik:GridClientSelectColumn HeaderStyle-Width="30px" FilterControlWidth="30px" UniqueName="IsSelect">
                                   </telerik:GridClientSelectColumn>
                                <telerik:GridBoundColumn UniqueName="FleetID" HeaderText="FleetID" DataField="FleetID"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" HeaderText="Description" HeaderStyle-HorizontalAlign="Left"
                                    HeaderStyle-Width="510px" FilterControlWidth="500px" DataField="TailNum" FilterDelay="500">
                                    <ItemTemplate>
                                        <asp:Label ID="lbLastName" runat="server" Text='<%#Eval("TailNum")%>'></asp:Label>
                                        <asp:Label ID="Label1" runat="server" Text=" -"></asp:Label>
                                        <asp:Label ID="lbFirstName" runat="server" Text='<%#Eval("AirCraft_AircraftCD")%>'></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text=" ("></asp:Label>
                                        <asp:Label ID="lbInitial" runat="server" Text='<%#Eval("TypeDescription")%>'></asp:Label>
                                        <asp:Label ID="Label3" runat="server" Text=" )"></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." AutoPostBackOnFilter="false"
                                    Display="false" ShowFilterIcon="false" HeaderStyle-Width="80px" FilterControlWidth="60px"
                                    CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn UniqueName="BoolField" DataField="IsInActive" HeaderText="Inactive"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="60px" AllowFiltering="false"
                                    CurrentFilterFunction="EqualTo">
                                </telerik:GridCheckBoxColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grd_ok">
                                    <asp:Button ID="btnSubmit" OnClientClick="returnToParent(); return false;" Text="Ok"
                                        runat="server" CssClass="button"></asp:Button>
                                </div>
                                <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                    visible="false">
                                    Use CTRL key to multi select</div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings EnablePostBackOnRowClick="false" >
                            <ClientEvents OnRowDblClick="returnToParent" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" EnableDragToSelectRows="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdnVendorID" runat="server" />
    </div>
    </form>
</body>
</html>
