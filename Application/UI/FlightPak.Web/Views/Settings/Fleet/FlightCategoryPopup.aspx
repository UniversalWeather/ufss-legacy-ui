﻿﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Flight Category</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var jqgridTableId = '#gridFlightCategory';
        var flightCategoryId = null;
        var flightCategoryCd = null;
        $(document).ready(function () {
            $("#chkSearchActiveOnly").prop('checked', true);
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });
            var scItems = getQuerystring("fromPage", "");
            if (scItems == "SystemTools") {
                showCommandItems = false;
            }

            callGridFillMethod();

            $("#recordAdd").click(function () {
                btnAddClick();
            });
            $("#recordDelete").click(function () {
                btnDeleteClick();
            });


            $("#recordEdit").click(function () {
                btnEditClick();
            });

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if (selr == null) {
                    showMessageBox("Please select a flight category.", popupTitle);
                    return false;
                } else {
                    returnToParent();
                }
                return false;
            });
        });

        function callGridFillMethod() {
            var isopenlookup = false;
            var selectedRowData = "";

            selectedRowData = decodeURI(getQuerystring("FlightCatagoryCD", ""));
            if (selectedRowData != "") {
                isopenlookup = true;
            }
            var fetchActive = $("#chkSearchActiveOnly").is(":checked");
            jQuery(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {

                        if (postData.filters === undefined) postData.filters = null;

                    }

                    postData.sendCustomerId = true;
                    postData.apiType = 'fss';
                    postData.method = 'FlightCategories';
                    postData.fetchActiveOnly = function () {

                        if ($("#chkSearchActiveOnly").is(":checked") == true)
                            return false;
                        else
                            return true;


                    };
                    postData.flightCatagoryCD = selectedRowData;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;

                    return postData;
                },
                multiselectWidth: 100,
                height: 300,
                width: 600,
                viewrecords: true,
                rowNum: 20,
                multiselect: ismultiselect,
                ignoreCase: true,
                pager: "#pager_gridFlightCategory",
                colNames: ['', 'Code', 'Description', 'Inactive', '', ''],
                colModel: [
                    { name: 'FlightCategoryID', index: 'FlightCategoryID', hidden: true },
                    {
                        name: 'FlightCatagoryCD', index: 'FlightCatagoryCD', width: 58, align: "left"
                    },
                    {
                        name: 'FlightCatagoryDescription', index: 'FlightCatagoryDescription', width: 203, align: "left"
                    },
                      {
                          name: 'IsInActive', index: 'IsInActive', width: 44, align: 'center !important', formatter: 'checkbox', editoptions: { value: '1:0' },
                          formatoptions: { disabled: true }, search: false,
                      },
                    { name: 'ForeGrndCustomColor', index: 'ForeGrndCustomColor', hidden: true },
                    { name: 'BackgroundCustomColor', index: 'BackgroundCustomColor', hidden: true }
                ],
                shrinkToFit: false,
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    jQuery(jqgridTableId).setSelection(rowId, true);
                    returnToParent();
                },
                onSelectAll: function (id, status) {
                    var tempcd = selectedFlightCD.split(",");
                    if (selectedFlightCD.lastIndexOf(",") != selectedFlightCD.length - 1) {
                        selectedFlightCD += ",";
                    }
                    for (var i = 0; i < id.length; i++) {

                        var rowData = $(jqgridTableId).jqGrid("getRowData", id[i]);

                        if (status) {
                            if (jQuery.inArray(rowData.FlightCatagoryCD, tempcd) == -1) {
                                selectedFlightCD += rowData.FlightCatagoryCD + ",";
                            }
                        } else {
                            if (jQuery.inArray(rowData.FlightCatagoryCD, tempcd) != -1) {
                                tempcd[jQuery.inArray(rowData.FlightCatagoryCD, tempcd)] = undefined;
                            }
                            tempcd = jQuery.grep(tempcd, function (value) {
                                return value != undefined && value != "";
                            });

                            selectedFlightCD = tempcd.join(",");
                        }

                    }

                    selectedFlightCD = jQuery.trim(selectedFlightCD);
                    if (selectedFlightCD.lastIndexOf(",") == selectedFlightCD.length - 1)
                        selectedFlightCD = selectedFlightCD.substr(0, selectedFlightCD.length - 1);
                    $("#hdnselectedfccd").val(selectedFlightCD);
                },
                onSelectRow: function (rowid, status, e) {
                    var rowData = $(jqgridTableId).jqGrid("getRowData", rowid);

                    if (ismultiselect) {
                        var tempcds = selectedFlightCD.split(",");
                        if (status) {
                            if (selectedFlightCD.lastIndexOf(",") != selectedFlightCD.length - 1) {
                                selectedFlightCD += ",";
                            }
                            if (jQuery.inArray(rowData.FlightCatagoryCD, tempcds) == -1) {
                                selectedFlightCD += rowData.FlightCatagoryCD;
                            }
                            selectedFlightCD.substring(0, selectedFlightCD.length - 1);
                        } else {

                            if (jQuery.inArray(rowData.FlightCatagoryCD, tempcds) != -1) {
                                tempcds[jQuery.inArray(rowData.FlightCatagoryCD, tempcds)] = undefined;
                            }
                            tempcds = jQuery.grep(tempcds, function (value) {
                                return value != undefined && value != "";
                            });

                            selectedFlightCD = tempcds.join(",");
                            selectedFlightCD.slice(0, selectedFlightCD.length - 1);
                        }
                    } else {
                        selectedFlightCD = rowData["FlightCatagoryCD"];
                        selectedFlightID = rowData["FlightCategoryID"];
                    }
                },
                afterInsertRow: function (rowid, rowObject) {
                    var trElement = jQuery("#" + rowid, $(this));
                    trElement.removeClass('ui-widget-content');
                    $(this).jqGrid('setRowData', rowid, false, { "color": rowObject.ForeGrndCustomColor, "background-color": rowObject.BackgroundCustomColor });
                    if (ismultiselect) {
                        var tempcds = selectedFlightCD.split(",");
                        if (jQuery.inArray(rowObject.FlightCatagoryCD, tempcds) != -1) {
                            jQuery(jqgridTableId).setSelection(rowid, true);
                        }
                        allRowsFlightCDs += rowObject.FlightCatagoryCD + ",";
                    }
                    if (rowObject.FlightCategoryID == selectedFlightID) {
                        jQuery(jqgridTableId).setSelection(rowid, true);
                    }
                    if (rowObject.FlightCatagoryCD == selectedFlightCD) {
                        jQuery(jqgridTableId).setSelection(rowid, true);
                    }

                },
                loadComplete: function (rowData) {
                    if (ismultiselect) {
                        var temp = selectedFlightCD.split(",");
                        var temp2 = allRowsFlightCDs.split(",");
                        for (var i = 0; i < temp.length; i++) {
                            if (jQuery.inArray(temp[i], allRowsFlightCDs) != -1) {
                                temp[i] = undefined;
                            }
                        }

                        temp = jQuery.grep(temp, function (value) {
                            return value != undefined && value != "";
                        });

                        selectedFlightCD = temp.join(",");
                        allRowsFlightCDs = "";
                        var gridid = jqgridTableId.substring(1, jqgridTableId.length);
                        $("#jqgh_" + gridid + "_cb").find("b").remove();

                        $("#jqgh_" + gridid + "_cb").append("<b> Select All</b>");
                        $("#jqgh_" + gridid + "_cb").css("height", "25px");
                        $(jqgridTableId + "_cb").css("width", "100px");
                        $(jqgridTableId + " tbody tr").children().first("td").css("width", "81px");

                    }

                    $(jqgridTableId + "_FlightCatagoryCD").css("width", "34px");
                    $(jqgridTableId + "_FlightCatagoryDescription").css("width", "126px");
                    $(jqgridTableId + "_IsInActive").css("width", "24px");
                }
            });

            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
            var pager = $(jqgridTableId).jqGrid().getGridParam('pager');
            $("#pagesizebox").insertBefore($(pager).find('table tr td[id="pager_gridFlightCategory_left"]'));
        }

        function btnEditClick() {
            var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
            var rowData = $(jqgridTableId).getRowData(selr);
             var url = "/Views/Settings/Fleet/FlightCategory.aspx?&IsPopup=&SelectedFlightCategoryID=" + rowData.FlightCategoryID;
             if (selr == undefined || rowData.FlightCategoryID == undefined) {
                 showMessageBox("Please select a flight category.", "Select Flight");
                 return false;
             }
             openWinAddEdit(url, "rdAddpopupwindow", 1100, 700, jqgridTableId);
             return false;
         }
        
        function btnAddClick() {
                var url = "/Views/Settings/Fleet/FlightCategory.aspx?IsPopup=Add";
                openWinAddEdit(url, "rdAddpopupwindow", 1100, 700, jqgridTableId);
                return false;
            }
        function btnSearchClick() {
            refreshGridData();
            return false;
        }
        function btnDeleteClick() {
            var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
            var rowData = $(jqgridTableId).getRowData(selr);
            flightCategoryId = rowData['FlightCategoryID'];
            flightCategoryCd = rowData['FlightCatagoryCD'];
            var widthDoc = $(document).width();
            if (flightCategoryId != undefined && flightCategoryId != null && flightCategoryId != '' && flightCategoryId != 0) {
               
                //changed :- added this dialog
                showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
            }
            else {
                showMessageBox('Please select a flight category.', popupTitle);
            }
            return false;
        }

        function confirmCallBackFn(arg) {
            if (arg) {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selarrrow');
                var listParameter = new Array();
                if(selr.length > 0 && ismultiselect==true){
                    for (var i = 0; i < selr.length; i++) {
                        var rowData = $(jqgridTableId).getRowData(selr[i]);
                        var newCategoryParameter = new Object();
                        newCategoryParameter.FlightCategoryID=rowData["FlightCategoryID"];
                        newCategoryParameter.FlightCatagoryCD=rowData["FlightCatagoryCD"];
                        listParameter.push(newCategoryParameter);
                    }
                } else {
                    var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');                    
                    var rowData = $(jqgridTableId).getRowData(selr);
                    var newCategoryParameter = new Object();
                    newCategoryParameter.FlightCategoryID = rowData["FlightCategoryID"];
                    newCategoryParameter.FlightCatagoryCD = rowData["FlightCatagoryCD"];
                    listParameter.push(newCategoryParameter);
                }

                $.ajax({
                    type: 'GET',
                    data:JSON.stringify(listParameter),
                    url: '/Views/Utilities/DeleteList.aspx?apiName=FSS&apiType=DELETE&method=FlightCategory',
                    contentType: "text/html",
                    success: function (data) {
                        verifyReturnedResultForJqgrid(data);
                        if (data != undefined && data != null && data != "") {
                            var dt = eval(htmlDecode(data));
                            if (dt.length > 0) {
                                refreshGridData();
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                            }
                            else {
                                refreshGridData();
                            }
                        }
                        
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var content = jqXHR.responseText;
                        if (content.indexOf('404'))
                            showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                    }
                });
            }
        }

        function refreshGridData() {
            jQuery(jqgridTableId).jqGrid('clearGridData');
            $(jqgridTableId).trigger('reloadGrid');
            $(jqgridTableId).jqGrid('setGridParam', { url: "/Views/Utilities/ApiCallerWithFilter.aspx", datatype: "json" }).trigger("reloadGrid");

            selectedFlightCD = jQuery("#hdnselectedfccd").val();

        }
    </script>
    
    <style type="text/css">
    body {
          width: auto;
         height: auto;
    }

    #gridFlightCategory tr td:first-child {text-align: center!important}

    .flightcategorypop #gridFlightCategory{width:600px!important}

    .flightcategorypop .ui-jqgrid-htable{width:600px!important}

</style>
</head>
<body>
    <form id="form1">
        <script type="text/javascript">
            var oArg = new Object();
            var showCommandItems = true;
            var jqgridTableId = "#gridFlightCategory";
            var selectedFlightCD = "";
            var selectedFlightID = "";
            var ismultiselect = false;
            var allRowsFlightCDs = "";
            selectedFlightCD = getQuerystring("FlightCatagoryCD", "");
            selectedFlightID = getQuerystring("FlightCategoryID", "");
            if (getQuerystring("IsUIReports", "") == "")
                ismultiselect = false;
            else
                ismultiselect = true;

            $("#hdnselectedfccd").val(selectedFlightCD);

            var scItems = getQuerystring("fromPage", "");
            if (scItems == "SystemTools") {
                showCommandItems = false;
            }

            function returnToParent() {
                selectedFlightCD = jQuery.trim(selectedFlightCD);
                if (selectedFlightCD.lastIndexOf(",") == selectedFlightCD.length - 1)
                    selectedFlightCD = selectedFlightCD.substr(0, selectedFlightCD.length - 1);

                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if (selr == null) {
                    showMessageBox("Please select a flight category.", popupTitle);
                    return;
                }
                var selectedRow = $(jqgridTableId).getRowData(selr);

                var cell1 = selectedRow.FlightCatagoryCD;
                var cell2 = selectedRow.FlightCatagoryDescription;
                var cell3 = selectedRow.FlightCategoryID;

                var oArg = new Object();
                if (selectedRow != null) {
                    oArg.FlightCatagoryCD = selectedFlightCD;
                    oArg.FlightCatagoryDescription = cell2;
                    oArg.FlightCategoryID = cell3;
                }
                else {
                    oArg.FlightCatagoryCD = "";
                    oArg.FlightCatagoryDescription = "";
                    oArg.FlightCategoryID = "";
                }

                var paramValue = getQuerystring("ControlName", "FlightCatagoryCD");
                if (ismultiselect) {
                    oArg.Arg1 = selectedFlightCD;
                } else {
                    oArg.Arg1 = oArg.FlightCatagoryCD;
                }

                oArg.CallingButton = paramValue;
                var oWnd = GetRadWindow();
                if (oArg) {
                oWnd.close(oArg);
                }
            }
    </script>

   <%--</telerik:RadCodeBlock>--%>

   <div  class="jqgrid flightcategorypop">
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                 <td>
                      <table>
                            <tr>
                                  <td align="left">
                                        <div>
                                             <input type="checkbox" name="chkSearchActiveOnly" id="chkSearchActiveOnly" />
                                              Display Inactive 
                                        </div>
                                  </td>
                           <td class="tdLabel100">
                               <button title="Search" id="btnSearch" onclick="return btnSearchClick();" class="button">
                                   Search</button>
                           </td>
                            </tr>
                      </table>
               </td>
           </tr>
           <tr>
               <td>
                   <table id="gridFlightCategory" class="table table-striped table-hover table-bordered"></table>
                   <div class="grid_icon">
                       <div role="group" id="pager_gridFlightCategory"></div>
                            <div id="pagesizebox">
                               <span>Page Size:</span>
                               <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                               <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                           </div>
                   </div>
                   <div style="padding: 5px 5px; text-align: right;">
                       <div id="jqgridOverflow" style="color: red; display: none;">Loading...</div>
                       <div class="grid_icon">
                           <a class="add-icon-grid" href="#" id="recordAdd"></a>
                           <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                           <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                       </div>
                       <div style="padding: 5px 5px; text-align: right;">
                           <button  value="OK" class="button" id="btnSubmit" style="min-width: 60px;" title="Ok" > OK </button>
                       </div>
                   </div>
                   <input type="hidden" id="hdnselectedfccd" value="" />
               </td>
           </tr>
       </table>
   </div>
    </form>
   </body>
</html>

