﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DelayTypePopUp.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Fleet.DelatTypePopUp" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Delay Type</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgDelayType.ClientID %>");

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function rebindgrid() {
                var masterTable = $find("<%= dgDelayType.ClientID %>").get_masterTableView();
                masterTable.rebind();
                masterTable.clearSelectedItems();
                masterTable.selectItem(masterTable.get_dataItems()[0].get_element());
            }
            function GetGridId() {
                return $find("<%= dgDelayType.ClientID %>");
            }

            //this function is used to get the selected code
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgDelayType.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "DelayTypeCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "DelayTypeDescription");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "DelayTypeID");
                }

                if (selectedRows.length > 0) {
                    oArg.DelayTypeCD = cell1.innerHTML;
                    oArg.DelayTypeDescription = cell2.innerHTML;
                    oArg.DelayTypeID = cell3.innerHTML;
                }
                else {
                    oArg.DelayTypeCD = "";
                    oArg.DelayTypeDescription = "";
                    oArg.DelayTypeID = "";

                }

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            //this function is used to close this window
            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgDelayType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDelayType" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDelayType" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" /></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                    OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgDelayType" runat="server" AllowSorting="true" OnNeedDataSource="dgDelayType_BindData"
                        OnDeleteCommand="dgDelayType_DeleteCommand" OnItemCommand="dgDelayType_ItemCommand"
                        AutoGenerateColumns="false" AllowPaging="false" AllowFilteringByColumn="true"
                        Width="660px" PagerStyle-AlwaysVisible="true" OnPreRender="dgDelayType_PreRender">
                        <MasterTableView ClientDataKeyNames="DelayTypeID,DelayTypeCD,DelayTypeDescription"
                            DataKeyNames="DelayTypeCD,DelayTypeDescription,LastUpdUID,LastUpdTS,DelayTypeID,IsInActive"
                            CommandItemDisplay="None" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn DataField="DelayTypeCD" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                                    CurrentFilterFunction="StartsWith" HeaderText="Code" FilterDelay="500">
                                    <HeaderStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DelayTypeDescription" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderText="Description"
                                    FilterDelay="500">
                                     <HeaderStyle Width="460px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="IsInActive" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                                    HeaderText="IsInActive" FilterDelay="500" AllowFiltering="false">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="DelayTypeID" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                                    CurrentFilterFunction="EqualTo" HeaderText="Description" UniqueName="DelayTypeID"
                                    Display="false" FilterDelay="500">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grid_icon">
                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                        CommandName="PerformInsert" Visible='<%# showCommandItems && ShowCRUD && IsAuthorized(Permission.Database.AddDelayTypeCatalog)%>'></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                        CommandName="Edit" Visible='<%# showCommandItems && ShowCRUD && IsAuthorized(Permission.Database.EditDelayTypeCatalog)%>'
                                        OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgDelayType', 'radDelayTypePopup');"
                                        CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# showCommandItems && ShowCRUD && IsAuthorized(Permission.Database.DeleteDelayTypeCatalog)%>'></asp:LinkButton>
                                    <%-- CommandName="DeleteSelected" --%>
                                </div>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                        Ok</button>
                                    <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent"  />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false"></GroupingSettings>
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
