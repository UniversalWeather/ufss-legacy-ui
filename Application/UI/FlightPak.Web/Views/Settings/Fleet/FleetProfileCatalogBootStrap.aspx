﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master" AutoEventWireup="true" 
    CodeBehind="FleetProfileCatalogBootStrap.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.FleetProfileCatalogBootStrap" ClientIDMode="AutoID" ValidateRequest="false" %>

<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <style>
        #art-main .art-Sheet .art-Sheet-body { border: 0 none; height: 924px !important;overflow-y: scroll;}
         @media only screen and (min-width:700px) and (max-width:900px) {
             #art-main .art-Sheet .art-Sheet-body {
                 width: 860px;
             }
         }
        @media only screen and (min-width:500px) and (max-width:699px) {
            #art-main .art-Sheet .art-Sheet-body {
                width: 658px;
            }
        }
        @media only screen and (min-width:400px) and (max-width:499px) {
            #art-main .art-Sheet .art-Sheet-body {
                width: 458px !important;
            }
        }
        @media only screen and (min-width:280px) and (max-width:400px) {
            #art-main .art-Sheet .art-Sheet-body { width: 358px;}
        }
        @media only screen and (min-width:220px) and (max-width:300px) {
            #art-main .art-Sheet .art-Sheet-body { width: 278px;}
        }
    </style>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
       <%=Styles.Render("~/bundles/BootStrapOnlyResponsiveCss") %>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            var isFormEdited = "";
            var currentLoadingPanel = null;
            var currentUpdatedControl = null;
            function ResponseEnd() {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    setTimeout(function () {
                        currentLoadingPanel.hide(currentUpdatedControl);
                        currentUpdatedControl = null;
                        currentLoadingPanel = null;
                    }, 10000);

            }
            function forecolorChange(sender, args) {

                
                $get("<%=tbForeColor.ClientID%>").value = sender.get_selectedColor();
                $get("<%=hdnForeColor.ClientID%>").value = sender.get_selectedColor();
                

            }
            function backcolorChange(sender, args) {

                
                $get("<%=tbBackColor.ClientID%>").value = sender.get_selectedColor();
                $get("<%=hdnBackColor.ClientID%>").value = sender.get_selectedColor();
                
            }

            //this function is used to display the format if the textbox is empty
            function ValidateEmptyTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0.0";
                }
            }
            function File_onchange() {
                __doPostBack('__Page', 'LoadImage');
            }
            function CheckName() {
                var nam = document.getElementById('<%=tbImgName.ClientID%>').value;

                if (nam != "") {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = false;
                }
                else {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = true;
                }
            }



            function OnClientClick(strPanelToExpand) {
                var PanelBar1 = $find("<%= pnlGeneral.ClientID %>");
                var PanelBar2 = $find("<%= pnlImage.ClientID %>");
                var PanelBar3 = $find("<%= pnlAdditionalInfo.ClientID %>");
                var PanelBar4 = $find("<%= pnlbarnotes.ClientID %>");
                PanelBar1.get_items().getItem(0).set_expanded(false);
                PanelBar2.get_items().getItem(0).set_expanded(false);
                PanelBar3.get_items().getItem(0).set_expanded(false);
                PanelBar4.get_items().getItem(0).set_expanded(false);

                if (strPanelToExpand == "General") {
                    PanelBar1.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "Image") {
                    PanelBar2.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "AddlInfo") {
                    PanelBar3.get_items().getItem(0).set_expanded(true);
                }
                else if (strPanelToExpand == "Notes") {
                    PanelBar4.get_items().getItem(0).set_expanded(true);
                }
                return false;
            }

            function OpenRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
                oWnd.show();
            }

            function CloseRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = null;
                oWnd.close();
            }
            
            function openWin(radWin) {
                var url = '';
                if (radWin == "radCompanyMasterPopup") {
                    url = '../Company/CompanyMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbhomebase.ClientID%>').value;
                }
                else if (radWin == "radAirportPopup") {
                    url = '../Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbhomebase.ClientID%>').value;
                }
                else if (radWin == "RadEmergencyPopUp") {
                    url = '../People/EmergencyContactPopup.aspx?EmergencyContactCD=' + document.getElementById('<%=tbemergency.ClientID%>').value;
                }
                else if (radWin == "RadVendorPopUp") {
                    url = '../Logistics/VendorMasterPopUp.aspx?VendorCD=' + document.getElementById('<%=tbvendor.ClientID%>').value;
                }
                else if (radWin == "RadAircraftTypePopup") {
                    url = '../Fleet/AircraftPopUp.aspx?AircraftCD=' + document.getElementById('<%=tbtypecode.ClientID%>').value;
                }
                else if (radWin == "radCrewRosterPopup") {
                    url = '../People/CrewRosterPopup.aspx?CrewCD=' + document.getElementById('<%=tbmaintcrew.ClientID%>').value;
                }
                else if (radWin == "RadClientPopUp") {
                    url = '../Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbclient.ClientID%>').value;
                }
                else if (radWin == "RadExportData") {
                    url = "../../Reports/ExportReportInformation.aspx?Report=RptDBFleetProfileExport&TailNum=" + document.getElementById('<%=tbtailnumber.ClientID%>').value;
                }
                else if (radWin == "RadAddlInfo") {
                    url = '../Fleet/FleetProfileAdditionalInfo.aspx';
                }
                else if (radWin == "RadFilterClientPopUp") {
                    url = '../Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbClientCode.ClientID%>').value;
                }

                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }



            function ShowAddlInfoPopup() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../Fleet/FleetProfileAdditionalInfo.aspx", "RadAddlInfo");
                return false;
            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); 
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }
            function OnClientCloseAircraftTypeMasterPopup(oWnd, args) {
                var combo = $find("<%= tbtypecode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnType.ClientID%>").value = arg.AircraftID;
                        document.getElementById("<%=tbtypecode.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=tbdescription.ClientID%>").value = arg.AircraftDescription;
                        document.getElementById("<%=cvTypeCode.ClientID%>").innerHTML = "";
                    }
                    else {

                        document.getElementById("<%=cvTypeCode.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnType.ClientID%>").value = "0";
                        document.getElementById("<%=tbtypecode.ClientID%>").value = "";
                        document.getElementById("<%=tbdescription.ClientID%>l").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseClientMasterPopup(oWnd, args) {
                var combo = $find("<%= tbclient.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbclient.ClientID%>").value = htmlDecode(arg.ClientCD);
                        document.getElementById("<%=hdnClient.ClientID%>").value = arg.ClientID;
                        document.getElementById("<%=cvClient.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbclient.ClientID%>").value = "";
                        document.getElementById("<%=hdnClient.ClientID%>").value = "0";
                        document.getElementById("<%=cvClient.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientFilterCloseClientMasterPopup(oWnd, args) {
                var combo = $find("<%= tbClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = htmlDecode(arg.ClientCD);
                        document.getElementById("<%=hdnClientCode.ClientID%>").value = arg.ClientID;
                        document.getElementById("<%=cvClientCode.ClientID%>").innerHTML = "";

                        var masterTable = $find("<%= dgFleetProfile.ClientID %>").get_masterTableView();
                        masterTable.filter("ClientCD", htmlDecode(arg.ClientCD), Telerik.Web.UI.GridFilterFunction.StartsWith, true);

                    }
                    else {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnClientCode.ClientID%>").value = "0";
                        document.getElementById("<%=cvClientCode.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            // this function is used to display the value of selected Airport code from popup
            function HomeBasePopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbhomebase.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbhomebase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "0";
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                    }
                }
            }

            function OnClientCloseCompanyMasterPopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbhomebase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.HomebaseID;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbhomebase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "0";
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                    }
                }
            }


            function OnClientCloseVendorMasterPopup(oWnd, args) {
                //get the transferred arguments


                var combo = $find("<%= tbvendor.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnVendorID.ClientID%>").value = arg.VendorID;
                        document.getElementById("<%=tbvendor.ClientID%>").value = arg.VendorCD;
                        document.getElementById("<%=cvVendor.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=hdnVendorID.ClientID%>").value = "0";
                        document.getElementById("<%=tbvendor.ClientID%>").value = "";
                        document.getElementById("<%=cvVendor.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseEmergencyContactPopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnContact.ClientID%>").value = arg.EmergencyContactID;
                        document.getElementById("<%=tbemergency.ClientID%>").value = arg.EmergencyContactCD;
                        document.getElementById("<%=cvEmergency.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbemergency.ClientID%>").value = "";
                        document.getElementById("<%=hdnContact.ClientID%>").value = "0";
                        document.getElementById("<%=cvEmergency.ClientID%>").innerHTML = "";
                    }
                }
            }

            function OnClientCloseCrewRosterPopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbmaintcrew.ClientID%>").value = arg.CrewCD;
                        document.getElementById("<%=hdnCrew.ClientID%>").value = arg.CrewID;
                        document.getElementById("<%=cvMaintCrew.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=hdnCrew.ClientID%>").value = "0";
                        document.getElementById("<%=tbmaintcrew.ClientID%>").value = "";
                        document.getElementById("<%=cvMaintCrew.ClientID%>").innerHTML = "";
                    }
                }
            }
            function ShowReports(radWin, ReportFormat, ReportName) {
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                if (ReportFormat == "EXPORT") {
                    url = "../../Reports/ExportReportInformation.aspx";
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    var oWnd = oManager.open(url, 'RadExportData');
                    return true;
                }
                else {
                    return true;
                }
            }
            function OnClientCloseExportReport(oWnd, args) {
                document.getElementById("<%=btnShowReports.ClientID%>").click();
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            // this function is used to the refresh the currency grid
            function refreshGrid(arg) {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest('Rebind');
            }
            function printImage() {
                var image = document.getElementById('<%=ImgPopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlImg = document.getElementById('<%=ddlImg.ClientID%>');
                if (ddlImg.length > 0) {
                    var ImageName = ddlImg.options[ddlImg.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });

                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Fleet");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }
        </script>
        <script type="text/javascript">
            function browserName() {
                var agt = navigator.userAgent.toLowerCase();
                if (agt.indexOf("msie") != -1) return 'Internet Explorer';
                if (agt.indexOf("chrome") != -1) return 'Chrome';
                if (agt.indexOf("opera") != -1) return 'Opera';
                if (agt.indexOf("firefox") != -1) return 'Firefox';
                if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
                if (agt.indexOf("netscape") != -1) return 'Netscape';
                if (agt.indexOf("safari") != -1) return 'Safari';
                if (agt.indexOf("staroffice") != -1) return 'Star Office';
                if (agt.indexOf("webtv") != -1) return 'WebTV';
                if (agt.indexOf("beonex") != -1) return 'Beonex';
                if (agt.indexOf("chimera") != -1) return 'Chimera';
                if (agt.indexOf("netpositive") != -1) return 'NetPositive';
                if (agt.indexOf("phoenix") != -1) return 'Phoenix';
                if (agt.indexOf("skipstone") != -1) return 'SkipStone';
                if (agt.indexOf('\/') != -1) {
                    if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                        return navigator.userAgent.substr(0, agt.indexOf('\/'));
                    }
                    else return 'Netscape';
                } else if (agt.indexOf(' ') != -1)
                    return navigator.userAgent.substr(0, agt.indexOf(' '));
                else return navigator.userAgent;
            }
            function GetBrowserName() {
                document.getElementById('<%=hdnBrowserName.ClientID%>').value = browserName();
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("input").bind('input', function () {
                    isFormEdited = "true";
                });
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update" && isFormEdited === "true") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                    isFormEdited = "";
                    ResponseEnd();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                    isFormEdited = "";
                    ResponseEnd();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr style="display: none;">
            <td>
                <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
            </td>
        </tr>
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fleet Profile</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBFleetProfile');"
                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBFleetProfileExport');return false;"
                            OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                        <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                            Style="display: none;" />
                        <a href="../../Help/ViewHelp.aspx?Screen=FleetProfileHelp" class="help-icon" target="_blank"
                            title="Help"></a>
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                    </span>
                </div>
            </td>
        </tr>
    </table>
    <div class="divGridPanel">
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
            OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgFleetProfile">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadAircraftTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseAircraftTypeMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftTypePopUp.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadClientPopUp" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseClientMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadFilterClientPopUp" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientFilterCloseClientMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseCompanyMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseCompanyMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadEmergencyPopUp" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseEmergencyContactPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/EmergencyContactPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadVendorPopUp" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseVendorMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/VendorMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                    Title="Export Report Information" KeepInScreenBounds="true" AutoSize="false"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCrewRosterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseCrewRosterPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCrewRosterCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                    Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                    <ContentTemplate>
                        <div>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div class="float_printicon">
                                            <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                                AlternateText="Print" OnClientClick="javascript:printImage();return false;" /></div>
                                        <asp:Image ID="ImgPopup" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadAddlInfo" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        
        <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                        <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
        </telerik:RadWindowManager>
    </div>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlGrid" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu" width="100%">
                <tr>
                    <td>
                        <div class="fleet_select">
                            <asp:LinkButton ID="lbtnIntl" CssClass="fleet_link" runat="server" OnClick="Intl_Click">International</asp:LinkButton>
                            <asp:LinkButton ID="lbtnEngineAirframe" CssClass="fleet_link" runat="server" OnClick="Engineairframe_Click">Engine/Airframe</asp:LinkButton>
                            <asp:LinkButton ID="lbtnComponent" CssClass="fleet_link" runat="server" OnClick="Component_Click">Components</asp:LinkButton>
                            <asp:LinkButton ID="lnkForecast" CssClass="fleet_link" runat="server" OnClick="Forecast_Click">Forecast</asp:LinkButton>
                            <asp:LinkButton ID="lbtn" runat="server" CssClass="fleet_link" OnClick="Chargerates_Click">Charge Rates</asp:LinkButton>
                            <asp:LinkButton ID="lbCharterRates" CssClass="fleet_link" runat="server" Visible="false">Charter Rates</asp:LinkButton>
                            <asp:LinkButton ID="lbNewCharterRates" CssClass="fleet_link" OnClick="Charterrates_Click"
                                runat="server">Charter Rates</asp:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="head-sub-menu" width="100%">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkActiveOnly" runat="server" Checked="true" Text="Active Only"
                                    AutoPostBack="true" OnCheckedChanged="chkActiveOnly_CheckedChanged" />
                            </span><span>
                                <asp:CheckBox ID="chkHomeBaseOnly" runat="server" Checked="false" Text="Home Base Only"
                                    AutoPostBack="true" OnCheckedChanged="chkHomeBaseOnly_CheckedChanged" />
                            </span><span>
                                <asp:CheckBox ID="chkRotary" runat="server" Checked="true" Text="Rotary Only" AutoPostBack="true"
                                    OnCheckedChanged="chkRotary_CheckedChanged" />
                            </span><span>
                                <asp:CheckBox ID="chkFixedWing" runat="server" Checked="true" Text="Fixed Wing Only"
                                    AutoPostBack="true" OnCheckedChanged="chkFixedWing_CheckedChanged" />
                            </span>
                            <span>Client Code</span>
                            <span>
                                <asp:HiddenField ID="hdnClientCode" runat="server" />
                                <asp:TextBox ID="tbClientCode" runat="server" CssClass="text50" MaxLength="5" AutoPostBack="true" onKeyPress="return fnAllowAlphaNumeric(this, event)" OnTextChanged="tbClientCode_TextChanged"></asp:TextBox>
                                <asp:Button ID="btnClientCode" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadFilterClientPopUp');return false;" />
                                <asp:CustomValidator ID="cvClientCode" runat="server" ControlToValidate="tbClientCode" ErrorMessage="Invalid Client Code."
                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                            </span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgFleetProfile" runat="server" AllowSorting="true" OnItemCreated="dgFleetProfile_ItemCreated"
                OnNeedDataSource="dgFleetProfile_BindData" OnItemCommand="dgFleetProfile_ItemCommand"
                OnPageIndexChanged="dgAircraftType_PageIndexChanged" OnItemDataBound="dgFleetProfile_ItemDataBound"
                OnUpdateCommand="dgFleetProfile_UpdateCommand" OnInsertCommand="dgFleetProfile_InsertCommand"
                OnDeleteCommand="dgFleetProfile_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
                Height="341px" AllowPaging="true" OnSelectedIndexChanged="dgFleetProfile_SelectedIndexChanged"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPreRender="dgFleetProfile_PreRender">
                <MasterTableView DataKeyNames="CustomerID,AircraftCD,TailNum,AircraftCD,TypeDescription,MaximumPassenger,VendorCD,FleetID,ClientCD,AirCraft_AircraftCD,LastUpdUID,LastUpdTS,ForeGrndCustomColor,BackgroundCustomColor"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="FleetID" HeaderText="FleetID" ShowFilterIcon="false"
                            Display="false" CurrentFilterFunction="EqualTo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CustomerID" HeaderText="FleetID" ShowFilterIcon="false"
                            Display="false" CurrentFilterFunction="EqualTo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Aircraft Code" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" HeaderStyle-Width="60px" FilterControlWidth="40px" CurrentFilterFunction="StartsWith"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="80px" FilterDelay="500"
                            FilterControlWidth="60px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AirCraft_AircraftCD" HeaderText="Type Code" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="80px" FilterDelay="500"
                            FilterControlWidth="60px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TypeDescription" HeaderText="Type Description" FilterDelay="500"
                            HeaderStyle-Width="180px" FilterControlWidth="160px" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MaximumPassenger" HeaderText="Max. PAX" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="EqualTo" HeaderStyle-Width="60px" FilterDelay="500"
                            FilterControlWidth="40px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Vendor Code" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="80px" FilterDelay="500"
                            FilterControlWidth="60px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IcaoID" HeaderText="Home Base" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="80px" FilterControlWidth="60px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ClientCD" HeaderText="Client Code" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="80px" FilterControlWidth="60px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Active" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="UserID" ShowFilterIcon="false"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="Date" ShowFilterIcon="false"
                            Display="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div class="grid_icon">
                            <asp:LinkButton ID="lnkInitInsert" runat="server" CssClass="add-icon-grid" ToolTip="Add"
                                CommandName="InitInsert" Visible='<%# IsAuthorized(Permission.Database.AddFleetProfile)%>'></asp:LinkButton>
                            <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                Visible='<%# IsAuthorized(Permission.Database.EditFleetProfile)%>' ToolTip="Edit"
                                CssClass="edit-icon-grid" CommandName="Edit"></asp:LinkButton>
                            <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                Visible='<%# IsAuthorized(Permission.Database.DeleteFleetProfile)%>' runat="server"
                                CommandName="DeleteSelected" ToolTip="Delete" CssClass="delete-icon-grid"></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false"></GroupingSettings>
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnGeneral" runat="server" Text="Main Information" CssClass="ui_nav"
                                        OnClientClick="javascript:OnClientClick('General'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnImage" runat="server" Text="Attachments" CssClass="ui_nav" OnClientClick="javascript:OnClientClick('Image'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnAdtlInfo" runat="server" Text="Additional Information" CssClass="ui_nav"
                                        OnClientClick="javascript:OnClientClick('AddlInfo'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnNotes" runat="server" Text="Notes" CssClass="ui_nav" OnClientClick="javascript:OnClientClick('Notes'); return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveTop" runat="server" Text="Save" CssClass="button" ValidationGroup="Save"
                                        OnClick="btnSaveTop_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancelTop_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlGeneral" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            runat="server">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                                    <ContentTemplate>
                                        <table width="100%" class="box1">
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td colspan="4" align="left">
                                                                <asp:CheckBox ID="chkinactiveairfact" runat="server" Text="Inactive Aircraft" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="tdLabel170">
                                                                <span class="mnd_text">Aircraft Code</span>
                                                            </td>
                                                            <td class="tdLabel220">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbaircraftcode" runat="server" MaxLength="4" CssClass="text60" OnTextChanged="FleetCode_TextChanged"
                                                                                AutoPostBack="true"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="rfvaircraftcode" runat="server" ValidationGroup="Save"
                                                                                ControlToValidate="tbaircraftcode" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Aircraft Code Required.</asp:RequiredFieldValidator>
                                                                            <asp:CustomValidator ID="cvAircraftCode" runat="server" ControlToValidate="tbaircraftcode"
                                                                                ErrorMessage="Unique Aircraft Code Required" Display="Dynamic" CssClass="alert-text"
                                                                                ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td valign="top" class="tdLabel160">
                                                                <span class="mnd_text">Serial No.</span>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbserialnumber" runat="server" MaxLength="20" CssClass="text150"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="rfvserialnumber" runat="server" ValidationGroup="Save"
                                                                                ControlToValidate="tbserialnumber" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Serial No. Required.</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <span class="mnd_text">Tail No.</span>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbtailnumber" runat="server" MaxLength="9" CssClass="text60" OnTextChanged="FleetTailNum_TextChanged"
                                                                                AutoPostBack="true"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="rfvtailnumber" runat="server" ValidationGroup="Save"
                                                                                ControlToValidate="tbtailnumber" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Tail No. Required.</asp:RequiredFieldValidator>
                                                                            <asp:CustomValidator ID="cvTailNum" runat="server" ControlToValidate="tbtailnumber"
                                                                                ErrorMessage="Unique Tail No. Required" Display="Dynamic" CssClass="alert-text"
                                                                                ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                Home Base
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbhomebase" runat="server" CssClass="text60" MaxLength="4" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                AutoPostBack="true" OnTextChanged="tbhomebase_TextChanged"></asp:TextBox>&nbsp
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:Button ID="btnHB" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radAirportPopup');return false;" />
                                                                            <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbhomebase"
                                                                                ErrorMessage="Invalid Home Base Code." Display="Dynamic" CssClass="alert-text"
                                                                                ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <span class="mnd_text">Type Code</span>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:TextBox ID="tbtypecode" runat="server" CssClass="text60" AutoPostBack="true"
                                                                                ValidationGroup="Save" MaxLength="10" OnTextChanged="tbtypecode_TextChanged"></asp:TextBox>&nbsp
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:HiddenField ID="hdnType" runat="server" />
                                                                            <asp:Button ID="btntypecode" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadAircraftTypePopup');return false;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:RequiredFieldValidator ID="rfvtypecode" runat="server" ValidationGroup="Save"
                                                                                ControlToValidate="tbtypecode" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Type Code Required.</asp:RequiredFieldValidator>
                                                                            <asp:CustomValidator ID="cvTypeCode" runat="server" ControlToValidate="tbtypecode"
                                                                                ErrorMessage="Invalid Aircraft Type Code." Display="Dynamic" CssClass="alert-text"
                                                                                ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                Max. PAX
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbmaxpax" runat="server" MaxLength="3" CssClass="text60" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbDescription" Text="Aircraft Type Description" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbdescription" runat="server" CssClass="text120" MaxLength="15"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                Max. Reservations
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbmaxrsvtns" runat="server" MaxLength="3" CssClass="text60" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Maintenance Crew
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbmaintcrew" runat="server" CssClass="text60" Length="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                AutoPostBack="true" OnTextChanged="tbmaintcrew_TextChanged"></asp:TextBox>&nbsp
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:HiddenField ID="hdnCrew" runat="server" />
                                                                            <asp:Button ID="btnmaintcrew" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radCrewRosterPopup');return false;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:CustomValidator ID="cvMaintCrew" runat="server" ControlToValidate="tbmaintcrew"
                                                                                ErrorMessage="Invalid Crew Code." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                Minimum Runway
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbminrunway" runat="server" MaxLength="5" CssClass="text60" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Primary Dom. Flight Phone
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbdomflightphone" runat="server" MaxLength="25" CssClass="text120"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                Primary Intl. Flight Phone
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tblntlflightphone" runat="server" MaxLength="25" CssClass="text120"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Secondary Dom. Flight Phone
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbSecondaryDomFlightPhone" runat="server" MaxLength="25" CssClass="text120"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td valign="top">
                                                                Secondary Intl. Flight Phone
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbSecondaryIntlFlightPhone" runat="server" MaxLength="25" CssClass="text120"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                Client
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbclient" runat="server" CssClass="text60" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                AutoPostBack="true" OnTextChanged="tbclient_TextChanged"></asp:TextBox>&nbsp
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:HiddenField ID="hdnClient" runat="server" />
                                                                            <asp:Button ID="btnclient" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadClientPopUp');return false;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:CustomValidator ID="cvClient" runat="server" ControlToValidate="tbclient" ErrorMessage="Invalid Client Code."
                                                                                Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td valign="top">
                                                                Vendor
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbvendor" runat="server" CssClass="text60" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                AutoPostBack="true" OnTextChanged="tbvendor_TextChanged"></asp:TextBox>&nbsp
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:HiddenField ID="hdnVendorID" runat="server" />
                                                                            <asp:Button ID="btnvendor" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadVendorPopUp');return false;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:CustomValidator ID="cvVendor" runat="server" ControlToValidate="tbvendor" ErrorMessage="Invalid Vendor Code."
                                                                                Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Aircraft Class
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList ID="drpairfactclass" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                Aircraft Type
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList ID="drpaircrafttype" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbEmergencyContact" Text="Emergency Contact" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbemergency" runat="server" CssClass="text120" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                AutoPostBack="true" OnTextChanged="tbemergency_TextChanged"></asp:TextBox>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:HiddenField ID="hdnContact" runat="server" />
                                                                            <asp:Button ID="btnemergency" runat="server" CssClass="browse-button rbrowse-button" OnClientClick="javascript:openWin('RadEmergencyPopUp');return false;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:CustomValidator ID="cvEmergency" runat="server" ControlToValidate="tbemergency"
                                                                                ErrorMessage="Invalid Emergency Contact Code." Display="Dynamic" CssClass="alert-text"
                                                                                ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td colspan="3">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkfar91" runat="server" Text="FAR 91" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkfar135" runat="server" Text="FAR 135" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Margin Percentage
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbMarginPercentage" runat="server" Text="0.0" MaxLength="10" ValidationGroup="Save"
                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                                                    CssClass="text80"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RegularExpressionValidator ID="revtbMarginPercentage" runat="server" Display="Dynamic"
                                                                    ErrorMessage="Invalid Format (NNNNNNN.NN)" ControlToValidate="tbMarginPercentage"
                                                                    CssClass="alert-text" ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                <fieldset>
                                                                    <legend>Inspection History</legend>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                Last Hourly
                                                                            </td>
                                                                            <td>
                                                                                <uc:DatePicker ID="ucLastInspectionDate" runat="server"></uc:DatePicker>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Hours To Next
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbhourstonext" runat="server" MaxLength="4" CssClass="text80" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Hours To Warning
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbhourstowarning" runat="server" MaxLength="4" CssClass="text80"
                                                                                    onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:CompareValidator ID="cvHrs" runat="server" ControlToValidate="tbhourstowarning"
                                                                                    ControlToCompare="tbhourstonext" CssClass="alert-text" ErrorMessage="Hours to Warning Cannot Be Greater Than Hours to Inspection"
                                                                                    Type="Double" Operator="LessThanEqual" ValidationGroup="Save" SetFocusOnError="true" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>Aircraft Parameters</legend>
                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                            <td>
                                                                                Max. Fuel
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbmaxfuel" runat="server" MaxLength="6" CssClass="text70" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                Max. Takeoff Wt.
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbmaxtakeoffwt" runat="server" MaxLength="6" CssClass="text70" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Min. Fuel
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbminfuel" runat="server" MaxLength="6" CssClass="text70" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                Max. Landing Wt.
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbmaxlandingwt" runat="server" MaxLength="6" CssClass="text70" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Taxi Fuel
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbtaxifuel" runat="server" MaxLength="6" CssClass="text70" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                Max. Zero Fuel Wt.
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbmaxzerofuelwt" runat="server" MaxLength="6" CssClass="text70"
                                                                                    onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                                Basic Operating Wt.
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbbasicoperatingwt" runat="server" MaxLength="6" CssClass="text70"
                                                                                    onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>SIFL Multiples</legend>
                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                            <td>
                                                                                Control
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbcontrol" runat="server" Text="0.0" MaxLength="8" ValidationGroup="Save"
                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                                                                    CssClass="text80"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:RegularExpressionValidator ID="revFare1" runat="server" Display="Dynamic" ErrorMessage="Invalid Format (NNNN.N)"
                                                                                    ControlToValidate="tbcontrol" CssClass="alert-text" ValidationExpression="^[0-9]{0,4}(\.[0-9]{1,1})?$"
                                                                                    ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Non-Control
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbnoncontrol" runat="server" Text="0.0" MaxLength="8" ValidationGroup="Save"
                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                                                                    CssClass="text80"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                                                                    ErrorMessage="Invalid Format (NNNN.N)" ControlToValidate="tbnoncontrol" CssClass="alert-text"
                                                                                    ValidationExpression="^[0-9]{0,4}(\.[0-9]{1,1})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Sec. Control
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbseccontrol" runat="server" Text="0.0" MaxLength="8" ValidationGroup="Save"
                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                                                                    CssClass="text80"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                                                                    ErrorMessage="Invalid Format (NNNN.N)" ControlToValidate="tbseccontrol" CssClass="alert-text"
                                                                                    ValidationExpression="^[0-9]{0,4}(\.[0-9]{1,1})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>Flight Plan Defaults</legend>
                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                            <td>
                                                                                Cruise Speed
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbcruisespeed" runat="server" MaxLength="10" CssClass="text70"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Max. Altitude
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbmaxflightlevel" runat="server" MaxLength="5" CssClass="text70"
                                                                                    onKeyPress="return fnAllowNumeric(this, event,'.')" ToolTip="Enter in Feet (For example - 30000)"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <div class="tblspace_20">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>Set Calendar Colors</legend>
                                                                    <table width="100%" class="fleet_colorpicker">
                                                                        <tr>
                                                                            <td>
                                                                                Foreground Color
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbForeColor" runat="server" MaxLength="8" onKeyPress="return fnNotAllowKeys(this, event)"
                                                                                    CssClass="text60" Enabled="false"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdnForeColor" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <telerik:RadColorPicker runat="server" ID="rcpForeColor" OnClientColorChange="forecolorChange"
                                                                                    ShowIcon="true" ZIndex="90000" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Background Color
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbBackColor" runat="server" MaxLength="8" onKeyPress="return fnNotAllowKeys(this, event)"
                                                                                    CssClass="text60" Enabled="false"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdnBackColor" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <telerik:RadColorPicker runat="server" ID="rcpBackColor" OnClientColorChange="backcolorChange"
                                                                                    ShowIcon="true" ZIndex="90000" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <div class="tblspace_20">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Attachments" CssClass="PanelHeaderStyle">
                        <ContentTemplate>
                            <table width="100%" class="box1">
                                <tr style="display: none;">
                                    <td class="tdLabel100">
                                        Document Name
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text210" OnBlur="CheckName();"
                                            ClientIDMode="AutoID"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="tdLabel270">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:FileUpload ID="fileUL" runat="server" Enabled="false" ClientIDMode="AutoID"
                                                        onchange="javascript:return File_onchange();" />
                                                    <asp:Label ID="lblError" CssClass="alert-text" runat="server" Style="float: left;">All file types are allowed.</asp:Label>
                                                    <asp:HiddenField ID="hdnUrl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" class="tdLabel110">
                                        <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                            OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text200"
                                            ClientIDMode="AutoID">
                                        </asp:DropDownList>
                                    </td>
                                    <td valign="top" class="tdLabel80">
                                        <asp:Image ID="imgFile" Width="50px" Height="50px" AlternateText="" runat="server"
                                            OnClick="OpenRadWindow();" ClientIDMode="AutoID" />
                                        <asp:HyperLink ID="lnkFileName" runat="server" ClientIDMode="Static">Download File</asp:HyperLink>
                                    </td>
                                    <td valign="top" align="right" class="custom_radbutton">
                                        <telerik:RadButton ID="btndeleteImage" runat="server" Text="Delete" Enabled="false"
                                            OnClientClicking="ImageDeleteConfirm" OnClick="DeleteImage_Click" ClientIDMode="Static" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlAdditionalInfo" Width="100%" ExpandAnimation-Type="None"
                            CollapseAnimation-Type="None" runat="server">
                            <Items>
                                <telerik:RadPanelItem Value="AddInfo" Text="Additional Information" runat="server">
                                    <ContentTemplate>
                                        <table width="100%" class="box1" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <telerik:RadGrid ID="dgPassengerAddlInfo" runat="server" EnableAJAX="True" AllowMultiRowSelection="false"
                                                        Height="245px">
                                                        <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                                            DataKeyNames="FleetProfileInfoXRefID,FleetProfileInformationID,CustomerID,FleetID,FleetInfoCD,FleetDescription,InformationValue,ClientID,LastUpdUID,LastUpdTS,IsDeleted"
                                                            CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false" AllowPaging="false">
                                                            <Columns>
                                                                <telerik:GridTemplateColumn HeaderText="Print on CQ Aircraft Profile Report" ShowFilterIcon="false"
                                                                    UniqueName="IsPrintable" AllowFiltering="false" HeaderStyle-Width="140px">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPrintable" ToolTip="Print on CQ Aircraft Profile Report" runat="server"
                                                                            Checked='<%# Eval("IsPrintable") != null ? Eval("IsPrintable") : false %>'></asp:CheckBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridBoundColumn DataField="FleetProfileInformationID" HeaderText="Info ID"
                                                                    CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                    AllowFiltering="false" Display="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="FleetProfileInfoXRefID" HeaderText="FleetProfileInfoXRefID"
                                                                    CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                    AllowFiltering="false" Display="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="CustomerID" HeaderText="CustomerID" CurrentFilterFunction="StartsWith"
                                                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" AllowFiltering="false" Display="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="FleetInfoCD" HeaderText="Code" AutoPostBackOnFilter="true"
                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="FleetDescription" HeaderText="Description" CurrentFilterFunction="StartsWith"
                                                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" AllowFiltering="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Additional Information" CurrentFilterFunction="StartsWith"
                                                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="InformationValue"
                                                                    HeaderStyle-Width="240px" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="tbAddlInfo" runat="server" CssClass="text210" Text='<%# Eval("InformationValue") %>'
                                                                            MaxLength="25"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                            </Columns>
                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                        </MasterTableView>
                                                        <ClientSettings>
                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                            <Selecting AllowRowSelect="true" />
                                                        </ClientSettings>
                                                        <GroupingSettings CaseSensitive="false" />
                                                    </telerik:RadGrid>
                                                </td>
                                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnAddlInfo" OnClientClick="javascript:return ShowAddlInfoPopup();"
                                                                    runat="server" CssClass="ui_nav" Text="Add Info." CausesValidation="false" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnDeleteInfo" runat="server" CssClass="ui_nav" Text="Delete Info."
                                                                    CausesValidation="false" OnClick="DeleteInfo_Click" OnClientClick="if(!confirm('Are you sure you want to delete this ADDITIONAL INFO record?')) return false;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlbarnotes" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            runat="server">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Notes" CssClass="PanelHeaderStyle">
                                    <ContentTemplate>
                                        <table width="100%" class="note-box" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbNotes" TextMode="MultiLine" runat="server" CssClass="textarea-db"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <div align="right">
                            <asp:Button ID="btnSaveChanges" OnClick="btnSaveChanges_Click" ValidationGroup="Save"
                                runat="server" Text="Save" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false"
                                OnClick="btnCancel_Click" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                            <asp:HiddenField ID="hdnFleetID" runat="server" />
                            <asp:HiddenField ID="hdnImgMode" runat="server" />
                            <asp:HiddenField ID="hdnCustomerID" runat="server" />
                            <asp:HiddenField ID="hdnImgID" runat="server" />
                            <asp:HiddenField ID="hdnBrowserName" runat="server" />
                            <asp:HiddenField ID="hdnScalendar" runat="server" />
                            <asp:HiddenField ID="hdnRedirect" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="/Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>

