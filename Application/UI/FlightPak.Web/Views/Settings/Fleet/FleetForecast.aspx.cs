﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
using System.Collections;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetForecast : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private List<string> lstFleetForeCastYear = new List<string>();
        bool flag = true;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFF, dgFF, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFF.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewFleetForecast);
                            if (Request.QueryString["FleetID"] != null && Request.QueryString["FleetID"].ToString().Trim() != "")
                            {
                                hdnFleetID.Value = Request.QueryString["FleetID"].ToString();
                            }
                            if (Request.QueryString["TailNum"] != null && Request.QueryString["TailNum"].ToString().Trim() != "")
                            {
                                tbTailNumber.Text = Request.QueryString["TailNum"].ToString();
                            }
                        }
                        tbTailNumber.Enabled = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }


        protected void dgAircraftType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFF.ClientSettings.Scrolling.ScrollTop = "0";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            defaultSelection(false);
                        }
                        else
                        {
                            if (ViewState["ËditFFID"] != null && ViewState["ËditFFID"].ToString() != "")
                            {
                                defaultSelection(false);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFF.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFF.Rebind();
                    SelectItem(string.Empty);
                    ReadOnlyForm();
                }
            }
        }

        /// <summary>
        /// Method to highlight the default item
        /// </summary>
        private void defaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (BindDataSwitch)
                {
                    dgFF.Rebind();
                }


                if (dgFF.MasterTableView.Items.Count > 0)
                {
                    if (!string.IsNullOrEmpty(hdnFleetForeCastID.Value) && Convert.ToInt64(hdnFleetForeCastID.Value) > 0)
                    {
                        SelectItem(hdnFleetForeCastID.Value);
                    }
                    else
                    {
                        dgFF.SelectedIndexes.Add(0);
                        GridDataItem item = dgFF.SelectedItems[0] as GridDataItem;
                        hdnFleetForeCastID.Value = item.GetDataKeyValue("FleetForeCastID").ToString();
                    }

                    DisplayEditForm();
                    ReadOnlyForm();
                    GridEnable(true, true, true);
                    //ViewState["ËditFFID"] = null;
                }
                else
                {

                    EnableForm(false);
                }
            }
        }


        private void SelectItem(string itm)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(itm))
            {
                string Code = hdnFleetForeCastID.Value.ToString().Trim();
                foreach (GridDataItem Item in dgFF.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("FleetForeCastID").ToString() == Code)
                    {
                        Item.Selected = true;
                        break;
                    }
                }

            }

        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFF_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //// Code to find the edit button in the grid
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, bodyForm, RadAjaxLoadingPanel1);

                            //// Code to find the insert button in the grid
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, bodyForm, RadAjaxLoadingPanel1);

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }

        /// <summary>
        /// Bind Fleet Forecast Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFF_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    string hs;

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FleetForecastHourService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FlightPakMasterService.FleetForeCast FleetForecast = new FlightPakMasterService.FleetForeCast();

                            if (hdnFleetID.Value.ToString().Trim() != "")
                            {
                                var FleetForecastValues = FleetForecastHourService.GetFleetForecastInfo(Convert.ToInt64(hdnFleetID.Value), 0).EntityList.Where(x => Convert.ToDateTime(x.YearMonthDate).Month == 12 || Convert.ToDateTime(x.YearMonthDate).Day == 12);
                                if (FleetForecastValues != null)
                                {
                                    dgFF.DataSource = FleetForecastValues;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }

        /// <summary>
        /// Method to modify the data before binding in the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFF_ItemDataBound(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (!string.IsNullOrEmpty(Item.GetDataKeyValue("YearMonthDate").ToString()))
                            {
                                // ((Label)Item["YearMonthDate"].FindControl("lbYearMonthDate")).Text = Convert.ToDateTime(Item.GetDataKeyValue("YearMon").ToString()).Year.ToString();
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }
        /// <summary>
        /// Item Command for Fleet Forecast Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void dgFF_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.FleetForeCast, Convert.ToInt64(hdnFleetForeCastID.Value.ToString().Trim()));
                                    Session["IsFleetForeCastEditLock"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetForeCast);
                                        defaultSelection(false);
                                        return;
                                    }
                                    ViewState["ËditFFID"] = null;
                                    SelectItem(string.Empty);
                                    GridEnable(false, true, false);
                                    EnableForm(true);
                                    tbforecastyear.Enabled = false;
                                    hdnSave.Value = "Update";
                                    hdnRedirect.Value = "";
                                    tbjanuary.Focus();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFF.SelectedIndexes.Clear();
                                ViewState["ËditFFID"] = null;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                EnableForm(true);
                                tbforecastyear.Focus();
                                btnSaveChanges.Visible = true;
                                btnCancel.Visible = true;
                                tbforecastyear.Enabled = true;
                                hdnSave.Value = "Insert";
                                tbforecastyear.Text = DateTime.Now.Year.ToString();
                                tbforecastyear.Focus();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }
        /// <summary>
        /// Update Command for Fleet Forecast Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFF_UpdateCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        e.Canceled = true;
                        List<string> lstFleet = new List<string>();
                        lstFleet.Add("01/01/");
                        lstFleet.Add("01/02/");
                        lstFleet.Add("01/03/");
                        lstFleet.Add("01/04/");
                        lstFleet.Add("01/05/");
                        lstFleet.Add("01/06/");
                        lstFleet.Add("01/07/");
                        lstFleet.Add("01/08/");
                        lstFleet.Add("01/09/");
                        lstFleet.Add("01/10/");
                        lstFleet.Add("01/11/");
                        lstFleet.Add("01/12/");

                        if (tbjanuary.Text.Trim() == "")
                        {
                            tbjanuary.Text = "0";
                        }
                        if (tbfebruary.Text.Trim() == "")
                        {
                            tbfebruary.Text = "0";
                        }
                        if (tbmarch.Text.Trim() == "")
                        {
                            tbmarch.Text = "0";
                        }
                        if (tbapril.Text.Trim() == "")
                        {
                            tbapril.Text = "0";
                        }
                        if (tbmay.Text.Trim() == "")
                        {
                            tbmay.Text = "0";
                        }
                        if (tbjune.Text.Trim() == "")
                        {
                            tbjune.Text = "0";
                        }
                        if (tbjuly.Text.Trim() == "")
                        {
                            tbjuly.Text = "0";
                        }
                        if (tbaugust.Text.Trim() == "")
                        {
                            tbaugust.Text = "0";
                        }
                        if (tbseptember.Text.Trim() == "")
                        {
                            tbseptember.Text = "0";
                        }
                        if (tboctober.Text.Trim() == "")
                        {
                            tboctober.Text = "0";
                        }
                        if (tbnovember.Text.Trim() == "")
                        {
                            tbnovember.Text = "0";
                        }
                        if (tbdecember.Text.Trim() == "")
                        {
                            tbdecember.Text = "0";
                        }

                        List<string> forecastHours = new List<string>();
                        forecastHours.Add(tbjanuary.Text.Trim());
                        forecastHours.Add(tbfebruary.Text.Trim());
                        forecastHours.Add(tbmarch.Text.Trim());
                        forecastHours.Add(tbapril.Text.Trim());
                        forecastHours.Add(tbmay.Text.Trim());
                        forecastHours.Add(tbjune.Text.Trim());
                        forecastHours.Add(tbjuly.Text.Trim());
                        forecastHours.Add(tbaugust.Text.Trim());
                        forecastHours.Add(tbseptember.Text.Trim());
                        forecastHours.Add(tboctober.Text.Trim());
                        forecastHours.Add(tbnovember.Text.Trim());
                        forecastHours.Add(tbdecember.Text.Trim());

                        using (FlightPakMasterService.MasterCatalogServiceClient FleetForecastHourService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var FleetForecastValues = FleetForecastHourService.GetFleetForecastInfo(Convert.ToInt64(hdnFleetID.Value), Convert.ToInt32(tbforecastyear.Text));

                            DateTime DateSource = DateTime.MinValue;
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);

                            for (int i = 0; i < lstFleet.Count; i++)
                            {
                                FlightPakMasterService.FleetForeCast FleetForecast = new FlightPakMasterService.FleetForeCast();

                                FleetForecast.FleetForeCastID = Convert.ToInt64(FleetForecastValues.EntityList[i].FleetForeCastID.ToString());

                                if (hdnFleetID.Value.ToString().Trim() != "")
                                    FleetForecast.FleetID = Convert.ToInt64(hdnFleetID.Value);
                                DateSource = DateTime.Parse((lstFleet[i] + tbforecastyear.Text.Trim()).ToString(yyyymmddFormat), System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal);
                                FleetForecast.YearMonthDate = DateSource;
                                FleetForecast.IsDeleted = false;

                                FleetForecast.ForecastHrs = Convert.ToDecimal(forecastHours[i]);
                                using (MasterCatalogServiceClient FleetForecastService = new MasterCatalogServiceClient())
                                {
                                    FleetForecastService.UpdateFleetForecastType(FleetForecast);
                                    // Unlock the Record
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.FleetForeCast, Convert.ToInt64(hdnFleetForeCastID.Value.ToString().Trim()));
                                    }
                                    Session["IsFleetForeCastEditLock"] = "False";
                                }
                            
                            }

                            ViewState["ËditFFID"] = hdnFleetForeCastID.Value.ToString().Trim();
                            ShowSuccessMessage();
                            _selectLastModified = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }



        /// <summary>
        /// Insert Command for Fleet Forecast Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFF_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (tbforecastyear.Text.Trim() == "")
                        {
                            cvYear.IsValid = false;
                            tbforecastyear.Text = "";
                            tbforecastyear.Focus();
                            IsValidate = false;
                            btnSaveChanges.Visible = true;
                            btnCancel.Visible = true;
                            flag = false;
                            return;
                        }
                        if (CheckAlreadyExist())
                        {
                            cvYear.IsValid = false;
                            tbforecastyear.Focus();
                            IsValidate = false;
                            btnSaveChanges.Visible = true;
                            btnCancel.Visible = true;
                            flag = false;
                            return;
                        }
                        if (IsValidate)
                        {
                            List<string> lstFleet = new List<string>();
                            lstFleet.Add("01/01/");
                            lstFleet.Add("01/02/");
                            lstFleet.Add("01/03/");
                            lstFleet.Add("01/04/");
                            lstFleet.Add("01/05/");
                            lstFleet.Add("01/06/");
                            lstFleet.Add("01/07/");
                            lstFleet.Add("01/08/");
                            lstFleet.Add("01/09/");
                            lstFleet.Add("01/10/");
                            lstFleet.Add("01/11/");
                            lstFleet.Add("01/12/");

                            if (tbjanuary.Text.Trim() == "")
                            {
                                tbjanuary.Text = "0";
                            }
                            if (tbfebruary.Text.Trim() == "")
                            {
                                tbfebruary.Text = "0";
                            }
                            if (tbmarch.Text.Trim() == "")
                            {
                                tbmarch.Text = "0";
                            }
                            if (tbapril.Text.Trim() == "")
                            {
                                tbapril.Text = "0";
                            }
                            if (tbmay.Text.Trim() == "")
                            {
                                tbmay.Text = "0";
                            }
                            if (tbjune.Text.Trim() == "")
                            {
                                tbjune.Text = "0";
                            }
                            if (tbjuly.Text.Trim() == "")
                            {
                                tbjuly.Text = "0";
                            }
                            if (tbaugust.Text.Trim() == "")
                            {
                                tbaugust.Text = "0";
                            }
                            if (tbseptember.Text.Trim() == "")
                            {
                                tbseptember.Text = "0";
                            }
                            if (tboctober.Text.Trim() == "")
                            {
                                tboctober.Text = "0";
                            }
                            if (tbnovember.Text.Trim() == "")
                            {
                                tbnovember.Text = "0";
                            }
                            if (tbdecember.Text.Trim() == "")
                            {
                                tbdecember.Text = "0";
                            }

                            List<string> forecastHours = new List<string>();
                            forecastHours.Add(tbjanuary.Text.Trim());
                            forecastHours.Add(tbfebruary.Text.Trim());
                            forecastHours.Add(tbmarch.Text.Trim());
                            forecastHours.Add(tbapril.Text.Trim());
                            forecastHours.Add(tbmay.Text.Trim());
                            forecastHours.Add(tbjune.Text.Trim());
                            forecastHours.Add(tbjuly.Text.Trim());
                            forecastHours.Add(tbaugust.Text.Trim());
                            forecastHours.Add(tbseptember.Text.Trim());
                            forecastHours.Add(tboctober.Text.Trim());
                            forecastHours.Add(tbnovember.Text.Trim());
                            forecastHours.Add(tbdecember.Text.Trim());

                            DateTime DateSource = DateTime.MinValue;
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);

                            List<FlightPakMasterService.FleetForeCast> lstFleetForecast = new List<FlightPakMasterService.FleetForeCast>();
                            for (int i = 0; i < lstFleet.Count; i++)
                            {
                                FlightPakMasterService.FleetForeCast FleetForecast = new FlightPakMasterService.FleetForeCast();
                                FleetForecast.FleetForeCastID = 0;
                                if (hdnFleetID.Value.ToString().Trim() != "")
                                    FleetForecast.FleetID = Convert.ToInt64(hdnFleetID.Value);

                                DateSource = DateTime.Parse((lstFleet[i] + tbforecastyear.Text.Trim()).ToString(yyyymmddFormat), System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal);
                                FleetForecast.YearMonthDate = DateSource;

                                FleetForecast.IsDeleted = false;

                                FleetForecast.ForecastHrs = Convert.ToDecimal(forecastHours[i]);
                                using (MasterCatalogServiceClient FleetForecastService = new MasterCatalogServiceClient())
                                {
                                    FleetForecastService.AddFleetForecastType(FleetForecast);
                                
                                }
                            }
                            ViewState["ËditFFID"] = null;
                            defaultSelection(true);
                            flag = true;

                            ShowSuccessMessage();
                            _selectLastModified = true;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }
        /// <summary>
        /// S
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFF_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (hdnFleetForeCastID.Value.ToString().Trim() != "" && Convert.ToInt64(hdnFleetForeCastID.Value.ToString()) > 0)
                        {
                            ////
                            string Code = hdnFleetForeCastID.Value.ToString();

                            foreach (GridDataItem Item in dgFF.MasterTableView.Items)
                            {
                                if (Item.GetDataKeyValue("FleetForeCastID").ToString() == Code.Trim())
                                {
                                    tbforecastyear.Text = Convert.ToDateTime(Item.GetDataKeyValue("YearMonthDate").ToString()).Year.ToString();
                                    Item.Selected = true;
                                    break;
                                }
                            }
                            /////

                            List<string> lstFleet = new List<string>();
                            lstFleet.Add("01/01/");
                            lstFleet.Add("01/02/");
                            lstFleet.Add("01/03/");
                            lstFleet.Add("01/04/");
                            lstFleet.Add("01/05/");
                            lstFleet.Add("01/06/");
                            lstFleet.Add("01/07/");
                            lstFleet.Add("01/08/");
                            lstFleet.Add("01/09/");
                            lstFleet.Add("01/10/");
                            lstFleet.Add("01/11/");
                            lstFleet.Add("01/12/");

                            List<string> forecastHours = new List<string>();
                            forecastHours.Add(tbjanuary.Text.Trim());
                            forecastHours.Add(tbfebruary.Text.Trim());
                            forecastHours.Add(tbmarch.Text.Trim());
                            forecastHours.Add(tbapril.Text.Trim());
                            forecastHours.Add(tbmay.Text.Trim());
                            forecastHours.Add(tbjune.Text.Trim());
                            forecastHours.Add(tbjuly.Text.Trim());
                            forecastHours.Add(tbaugust.Text.Trim());
                            forecastHours.Add(tbseptember.Text.Trim());
                            forecastHours.Add(tboctober.Text.Trim());
                            forecastHours.Add(tbnovember.Text.Trim());
                            forecastHours.Add(tbdecember.Text.Trim());

                            // here no loop required delete based on fleetid and year (fleetid and year isunique)
                            using (FlightPakMasterService.MasterCatalogServiceClient FleetForecastHourService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var FleetForecastValues = FleetForecastHourService.GetFleetForecastInfo(Convert.ToInt64(hdnFleetID.Value), Convert.ToInt32(tbforecastyear.Text));

                                // for (int i = 0; i < lstFleet.Count; i++)
                                // {
                                FlightPakMasterService.FleetForeCast FleetForecast = new FlightPakMasterService.FleetForeCast();

                                FleetForecast.FleetForeCastID = Convert.ToInt64(FleetForecastValues.EntityList[0].FleetForeCastID.ToString());
                                FleetForecast.FleetID = Convert.ToInt64(hdnFleetID.Value);
                                FleetForecast.YearMonthDate = Convert.ToDateTime(FleetForecastValues.EntityList[0].YearMonthDate);
                                FleetForecast.IsDeleted = true;

                                using (MasterCatalogServiceClient FleetForecastService = new MasterCatalogServiceClient())
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.FleetForeCast, Convert.ToInt64(hdnFleetForeCastID.Value.ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            e.Item.Selected = true;
                                            defaultSelection(false);
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetForeCast);
                                            return;
                                        }
                                        FleetForecastService.DeleteFleetForecastType(FleetForecast);
                                    }
                                }
                                //}
                                ClearForm();
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FleetForeCast, Convert.ToInt64(hdnFleetForeCastID.Value.ToString()));
                    }
                }
            }

        }

        /// <summary>
        /// Bind Selected Item in the Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void dgFF_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem(string.Empty);

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {

                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {

                            GridDataItem item = dgFF.SelectedItems[0] as GridDataItem;
                            hdnFleetForeCastID.Value = item.GetDataKeyValue("FleetForeCastID").ToString();
                            ViewState["ËditFFID"] = item.GetDataKeyValue("FleetForeCastID").ToString();

                            Label lblUser;

                            lblUser = (Label)dgFF.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + item.GetDataKeyValue("LastUpdUID").ToString());
                            }
                            else
                            {
                                lblUser.Text = string.Empty;
                            }
                            if (item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.GetDataKeyValue("LastUpdTS").ToString())));
                            }

                            DisplayEditForm();

                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                    }
                }
            }
        }
        /// <summary>
        /// Method to display the Insert form
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbforecastyear.Text = string.Empty;
                tbjanuary.Text = "0";
                tbfebruary.Text = "0";
                tbmarch.Text = "0";
                tbapril.Text = "0";
                tbmay.Text = "0";
                tbjune.Text = "0";
                tbjuly.Text = "0";
                tbaugust.Text = "0";
                tbseptember.Text = "0";
                tboctober.Text = "0";
                tbnovember.Text = "0";
                tbdecember.Text = "0";

                EnableForm(true);
                btnCancel.Visible = true;
                btnSaveChanges.Visible = true;
            }
        }

        /// <summary>
        /// Event to save the Save the Changes 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        Page.Validate();
                        if (!Page.IsValid)
                        {
                            flag = false;
                        }

                        if (hdnSave.Value.ToString().ToUpper() == "UPDATE")
                        {
                            (dgFF.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            flag = true;
                        }
                        else
                        {
                            (dgFF.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                        if (flag)
                        {
                            GridEnable(true, true, true);
                            EnableForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }

        /// <summary>
        /// Cancel Fleet Forecast Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            if (hdnFleetForeCastID.Value.ToString().Trim() != "")
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.FleetForeCast, Convert.ToInt64(hdnFleetForeCastID.Value.ToString().Trim()));
                            }
                            //hdnFleetForeCastID.Value = null; 
                            GridEnable(true, true, true);
                            EnableForm(false);
                            ClearForm();
                            defaultSelection(true);
                            SelectItem(string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        private void ClearForm()
        {
            tbjanuary.Text = string.Empty;
            tbfebruary.Text = string.Empty;
            tbmarch.Text = string.Empty;
            tbapril.Text = string.Empty;
            tbmay.Text = string.Empty;
            tbjune.Text = string.Empty;
            tbjuly.Text = string.Empty;
            tbaugust.Text = string.Empty;
            tbseptember.Text = string.Empty;
            tboctober.Text = string.Empty;
            tbnovember.Text = string.Empty;
            tbdecember.Text = string.Empty;
            tbforecastyear.Text = string.Empty;
            //hdnFleetForeCastID.Value = "0"; 
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFF;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }

        /// <summary>
        /// Method to display the Edit Form
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string Code = hdnFleetForeCastID.Value.ToString().Trim();
                string YearMonthDate = "";

                tbjanuary.Text = "0";
                tbfebruary.Text = "0";
                tbmarch.Text = "0";
                tbapril.Text = "0";
                tbmay.Text = "0";
                tbjune.Text = "0";
                tbjuly.Text = "0";
                tbaugust.Text = "0";
                tbseptember.Text = "0";
                tboctober.Text = "0";
                tbnovember.Text = "0";
                tbdecember.Text = "0";

                foreach (GridDataItem Item in dgFF.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("FleetForeCastID").ToString() == Code.Trim())
                    {
                        tbforecastyear.Text = Convert.ToDateTime(Item.GetDataKeyValue("YearMonthDate").ToString()).Year.ToString();
                        YearMonthDate = Item.GetDataKeyValue("YearMonthDate").ToString();
                        Item.Selected = true;


                        Label lblUser;
                        lblUser = (Label)dgFF.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lblUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));
                        }

                        lbColumnName1.Text = "Forecast Year";
                        lbColumnValue1.Text = Convert.ToDateTime(Item.GetDataKeyValue("YearMonthDate").ToString()).Year.ToString();
                        
                        break;
                    }
                }



                //  if (!string.IsNullOrEmpty(item.GetDataKeyValue("YearMonthDate").ToString()))
                if (!string.IsNullOrEmpty(YearMonthDate))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient FleetForecastHourService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        FlightPakMasterService.FleetForeCast FleetForecast = new FlightPakMasterService.FleetForeCast();
                        var FleetForecastValues = FleetForecastHourService.GetFleetForecastInfo(Convert.ToInt64(hdnFleetID.Value), Convert.ToInt32(tbforecastyear.Text));

                        if (FleetForecastValues.ReturnFlag == true)
                        {

                            tbjanuary.Text = FleetForecastValues.EntityList[0].ForecastHrs.ToString();
                            tbfebruary.Text = FleetForecastValues.EntityList[1].ForecastHrs.ToString();
                            tbmarch.Text = FleetForecastValues.EntityList[2].ForecastHrs.ToString();
                            tbapril.Text = FleetForecastValues.EntityList[3].ForecastHrs.ToString();
                            tbmay.Text = FleetForecastValues.EntityList[4].ForecastHrs.ToString();
                            tbjune.Text = FleetForecastValues.EntityList[5].ForecastHrs.ToString();
                            tbjuly.Text = FleetForecastValues.EntityList[6].ForecastHrs.ToString();
                            tbaugust.Text = FleetForecastValues.EntityList[7].ForecastHrs.ToString();
                            tbseptember.Text = FleetForecastValues.EntityList[8].ForecastHrs.ToString();
                            tboctober.Text = FleetForecastValues.EntityList[9].ForecastHrs.ToString();
                            tbnovember.Text = FleetForecastValues.EntityList[10].ForecastHrs.ToString();
                            tbdecember.Text = FleetForecastValues.EntityList[11].ForecastHrs.ToString();
                        }
                        else
                        {
                            tbjanuary.Text = string.Empty;
                            tbfebruary.Text = string.Empty;
                            tbmarch.Text = string.Empty;
                            tbapril.Text = string.Empty;
                            tbmay.Text = string.Empty;
                            tbjune.Text = string.Empty;
                            tbjuly.Text = string.Empty;
                            tbaugust.Text = string.Empty;
                            tbseptember.Text = string.Empty;
                            tboctober.Text = string.Empty;
                            tbnovember.Text = string.Empty;
                            tbdecember.Text = string.Empty;
                        }
                    }
                }

                bodyForm.Visible = true;
                EnableForm(true);
            }
        }

        /// <summary>
        /// Method to enable or disable the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, delCtl, editCtl;
                insertCtl = (LinkButton)dgFF.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                delCtl = (LinkButton)dgFF.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                editCtl = (LinkButton)dgFF.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                if (IsAuthorized(Permission.Database.AddFleetForecast))
                {
                    insertCtl.Visible = true;
                    if (add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteFleetForecast))
                {
                    delCtl.Visible = true;
                    if (delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = "";
                    }
                }

                else
                {
                    delCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditFleetForecast))
                {
                    editCtl.Visible = true;
                    if (edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = "";
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
            }
        }

        /// <summary>
        /// Method to display the Read Only Form
        /// </summary>
        protected void ReadOnlyForm()
        {


            EnableForm(false);
        }

        /// <summary>
        /// Method to Enable or Disable the form
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                tbforecastyear.Enabled = enable;
                tbjanuary.Enabled = enable;
                tbfebruary.Enabled = enable;
                tbmarch.Enabled = enable;
                tbapril.Enabled = enable;
                tbmay.Enabled = enable;
                tbjune.Enabled = enable;
                tbjuly.Enabled = enable;
                tbaugust.Enabled = enable;
                tbseptember.Enabled = enable;
                tboctober.Enabled = enable;
                tbnovember.Enabled = enable;
                tbdecember.Enabled = enable;
                btnCancel.Visible = enable;
                btnSaveChanges.Visible = enable;
            }
        }


        /// <summary>
        /// Event fires when the Text of Forecast Year is Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ForecastYear_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbforecastyear.Text.Trim() != "")
                        {
                            CheckAlreadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }

        }

        /// <summary>
        /// Method to check whether the Year is Unique
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;

                if (tbforecastyear.Text.ToString().Trim() != "")
                {
                    Int32 FcYear = Convert.ToInt32(tbforecastyear.Text.ToString());
                    if (FcYear < 1900 || FcYear > 2100)
                    {
                        cvYear.IsValid = false;
                        cvYear.ErrorMessage = "Year should be 1900 to 2100";
                        tbforecastyear.Focus();
                        return true;
                    }
                }

                using (FlightPakMasterService.MasterCatalogServiceClient FleetForecastHourService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.FleetForeCast FleetForecast = new FlightPakMasterService.FleetForeCast();
                    if (hdnFleetID.Value.ToString().Trim() != "")
                    {
                        var FleetForecastValues = FleetForecastHourService.GetFleetForecastInfo(Convert.ToInt64(hdnFleetID.Value), Convert.ToInt32(tbforecastyear.Text));

                        if (FleetForecastValues.ReturnFlag == true)
                        {
                            if (FleetForecastValues.EntityList.Count > 0)
                            {
                                cvYear.IsValid = false;
                                cvYear.ErrorMessage = "Unique Forecast Year is Required";
                                tbforecastyear.Focus();
                                return true;
                            }
                        }
                    }
                }
                return returnVal;
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFF.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var ForecastValue = FPKMstService.GetFleetForecastInfo(Convert.ToInt64(hdnFleetID.Value), 0);
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, ForecastValue);
            List<FlightPakMasterService.GetAllFleetForecast> filteredList = GetFilteredList(ForecastValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FleetForeCastID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFF.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFF.CurrentPageIndex = PageNumber;
            dgFF.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllFleetForecast ForecastValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    ViewState["ËditFFID"] = Session["SearchItemPrimaryKeyValue"];
                    hdnFleetForeCastID.Value = PrimaryKeyValue.ToString();
                }
            }
            else
            {
                PrimaryKeyValue = ForecastValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FleetForeCastID;
                ViewState["ËditFFID"] = PrimaryKeyValue;
                hdnFleetForeCastID.Value = PrimaryKeyValue.ToString();
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllFleetForecast> GetFilteredList(ReturnValueOfGetAllFleetForecast ForecastValue)
        {
            List<FlightPakMasterService.GetAllFleetForecast> filteredList = new List<FlightPakMasterService.GetAllFleetForecast>();

            if (ForecastValue.ReturnFlag)
            {
                filteredList = ForecastValue.EntityList.Where(x => Convert.ToDateTime(x.YearMonthDate).Month == 12 || Convert.ToDateTime(x.YearMonthDate).Day == 12).ToList();
            }

            return filteredList;
        }

        protected void dgFF_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFF, Page.Session);
        }
    }
}