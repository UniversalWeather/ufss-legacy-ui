﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AircraftDutyTypePopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Fleet.AircraftDutyTypePopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="FlightPak.Common.Constants" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Aircraft Duty Types</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">

            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }
            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            //this function is used to select the value of selected Metro code 
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgAircraftDutyTypes.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "AircraftDutyCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "AircraftDutyDescription");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "AircraftDutyID");
                        var cell4 = MasterTable.getCellByColumnUniqueName(row, "DefaultStartTM");
                        var cell5 = MasterTable.getCellByColumnUniqueName(row, "DefualtEndTM");
                        if (i == 0) {

                            oArg.AircraftDutyCD = cell1.innerHTML + ",";
                            oArg.AircraftDutyDescription = cell2.innerHTML;
                            oArg.AircraftDutyID = cell3.innerHTML;
                            oArg.DefaultStartTM = cell4.innerHTML;
                            oArg.DefualtEndTM = cell5.innerHTML;
                        }

                        else {
                            oArg.AircraftDutyCD += cell1.innerHTML + ",";
                            oArg.AircraftDutyDescription += cell2.innerHTML;
                            oArg.AircraftDutyID += cell3.innerHTML;
                            oArg.DefaultStartTM += cell4.innerHTML;
                            oArg.DefualtEndTM += cell5.innerHTML;
                        }
                    }
                }
                if (oArg.AircraftDutyCD != "")
                    oArg.AircraftDutyCD = oArg.AircraftDutyCD.substring(0, oArg.AircraftDutyCD.length - 1)
                else
                    oArg.AircraftDutyCD = "";
                oArg.Arg1 = oArg.AircraftDutyCD;
                oArg.CallingButton = "AircraftDutyCD";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            //this function is used to close the window 
            function Close() {
                GetRadWindow().Close();
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgAircraftDutyTypes.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            function GetGridId() {
                return $find("<%= dgAircraftDutyTypes.ClientID %>");
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgAircraftDutyTypes">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%-- <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
                <telerik:AjaxSetting AjaxControlID="btnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" /></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                    OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
                    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
                    <telerik:RadGrid ID="dgAircraftDutyTypes" runat="server" AllowMultiRowSelection="true"
                        OnItemCreated="dgAircraftDutyTypes_ItemCreated" OnItemDataBound="dgAircraftDutyTypes_ItemDataBound"
                        OnItemCommand="dgAircraftDutyTypes_ItemCommand" OnDeleteCommand="dgAircraftDutyTypes_DeleteCommand"
                        AllowSorting="true" OnNeedDataSource="dgAircraftDutyTypes_BindData" AutoGenerateColumns="false"
                        Height="330px" AllowPaging="false" Width="450px" OnPreRender="dgAircraftDutyTypes_PreRender" >
                        <MasterTableView DataKeyNames="AircraftDutyID,AircraftDutyCD,AircraftDutyDescription,ClientID,ForeGrndCustomColor,BackgroundCustomColor,IsCalendarEntry,IsAircraftStandby,IsDeleted,DefaultStartTM,DefualtEndTM,LastUpdUID,LastUpdTS"
                            CommandItemDisplay="Bottom" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn DataField="AircraftDutyCD" HeaderText="Aircraft Duty Code"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="100px" FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AircraftDutyDescription" HeaderText="Description"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="330px" FilterControlWidth="310px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AircraftDutyID" HeaderText="AircraftDutyID" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" Display="false" CurrentFilterFunction="EqualTo" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DefaultStartTM" HeaderText="DefaultStartTM" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DefualtEndTM" HeaderText="DefualtEndTM" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" Display="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grid_icon">
                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                        CommandName="InitInsert" Visible='<%# showCommandItems && IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.AddAircraftDutyType)%>'></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                        CommandName="Edit" Visible='<%# showCommandItems && IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.EditAircraftDutyType)%>'
                                        OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgAircraftDutyTypes', 'rdDutyCodePopup');"
                                        CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# showCommandItems && IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.DeleteAircraftDutyType)%>'></asp:LinkButton>
                                    <%-- CommandName="DeleteSelected" --%>
                                </div>
                                <div class="grd_ok">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                        Ok</button>
                                </div>
                                <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                    visible="false">
                                    Use CTRL key to multi select</div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent"  />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
