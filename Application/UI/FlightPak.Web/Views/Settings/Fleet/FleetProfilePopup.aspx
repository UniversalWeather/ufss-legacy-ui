﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <title>Fleet Profile</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var selectedRowData = null;
        var jqgridTableId = '#gridfleetProfile';
        var deleteFleetId = null;
        var deleteTailNum = null;
        var selectedTailNum = "";
        var isopenlookup = false;
        $(document).ready(function () {
            selectedTailNum = $.trim(unescape(getQuerystring("TailNumber", "")));
            if (selectedTailNum) {
                isopenlookup = true;
            }
            $("#btnSubmit").click(function () {
                var selr = $('#gridfleetProfile').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridfleetProfile').getRowData(selr);
                
                if (rowData["FleetID"] == undefined) {
                    showMessageBox('Please select a tail no.', popupTitle);
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                openWin2("/Views/Settings/Fleet/FleetProfileCatalog.aspx?IsPopup=Add", "radFleetProfileCRUDPopup");
                
                return false;
            });

            $("#btnClientCodeFilter").click(function () {
                openWinForFilterGrid("/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("tbClientCodeFilter").value, "rdAddpopupwindow", popupTitle, 650, 495, jqgridTableId, "tbClientCodeFilter", "ClientCD", "hdnClientId", "ClientID");
                return false;
            });
            $("#tbClientCodeFilter").change(function () {
                $("#hdnClientId").val("");
                return false;
            });
            $(document).ready(function () {
                $("#tbClientCodeFilter").blur(function () {
                    var clientCd = $.trim($("#tbClientCodeFilter").val());
                    if (!IsNullOrEmpty(clientCd)) {
                        CheckClientCodeExistance(clientCd, hdnClientId, errorMsg);
                    }
                    else {
                        $("#hdnClientId").val(0);
                        $("#errorMsg").removeClass().addClass('hideDiv');
                    }
                });
            });

            $("#recordEdit").click(function () {
                var selr = $('#gridfleetProfile').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridfleetProfile').getRowData(selr);
                var fleetID = rowData['FleetID'];
                var widthDoc = $(document).width();
                if (fleetID != undefined && fleetID != null && fleetID != '' && fleetID != 0) {
                    openWin2("/Views/Settings/Fleet/FleetProfileCatalog.aspx?IsPopup=&FleetId=" + fleetID, "radFleetProfileCRUDPopup");
                }
                else {
                    showMessageBox('Please select a tail no.', popupTitle);
                }
                return false;
            });
            $("#recordDelete").click(function () {
                var selr = $('#gridfleetProfile').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridfleetProfile').getRowData(selr);
                deleteFleetId = rowData['FleetID'];
                deleteTailNum = rowData['TailNum'];
                var widthDoc = $(document).width();
                if (deleteFleetId != undefined && deleteFleetId != null && deleteFleetId != '' && deleteFleetId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a tail no.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=FleetProfile&fleetID=' + deleteFleetId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (content.indexOf('404'))
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                        }
                    });
                }
            }

            function chkformat(cellvalue, options, rowObject) {
                if (cellvalue == "" || cellvalue == "false")
                    return "<input type=checkbox checked=checked value=true offval=no disabled=disabled/>";
                else
                    return "<input type=checkbox value=false offval=no disabled=disabled>";
            }

            jQuery("#gridfleetProfile").jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }

                    var isFixedRotary;
                    var chkRotary = $('#chkRotary').is(':checked') ? true : false;
                    var chkFixed = $('#chkFixedWing').is(':checked') ? true : false;
                    if (chkRotary == true && chkFixed == false) {
                        isFixedRotary = "R";
                    } else if (chkRotary == false && chkFixed == true) {
                        isFixedRotary = "F";
                    } else {
                        isFixedRotary = "B";
                    }
                    var model = {
                        ClientID: $("#hdnClientId").val(),
                        FetchActiveOnly: $('#chkActiveOnly').is(':checked') ? true : false,
                        IsFixedRotary: isFixedRotary,
                        VendorID: 0
                    };
                    postData.Model = model;
                    postData.sendCustomerId = false;
                    postData.sendIcaoId = $('#chkHomeBaseOnly').is(':checked') ? true : false;
                    postData.apiType = 'fss';
                    postData.method = 'fleetprofile';
                    postData.markSelectedRecord = isopenlookup;
                    postData.tailNum = selectedTailNum;
                    isopenlookup = false;
                    return postData;
                },
                height: 300,
                width: 1035,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: false,
                pager: "#pg_gridPager",
                colNames: ['FleetID', 'Fleet Code', 'Tail No.', 'Type Code', 'Type Description', 'Constant Mach', 'Long Range Cruise', 'High Speed Cruise', 'Maximum Pax', 'Vendor Code', 'Home Base', 'Active', 'IcaoID'],
                colModel: [
                     { name: 'FleetID', index: 'FleetID', key: true, hidden: true, },
                     { name: 'AircraftCD', index: 'f.AircraftCD', width: 110 },
                     { name: 'TailNum', index: 'TailNum', width: 100 },
                     { name: 'AirCraft_AircraftCD', index: 'a.AircraftCD', width: 110 },
                     { name: 'TypeDescription', index: 'TypeDescription', width: 160 },
                     { name: 'PowerSettings1TrueAirSpeed', index: 'PowerSettings1TrueAirSpeed', width: 100 },
                     { name: 'PowerSettings2TrueAirSpeed', index: 'PowerSettings2TrueAirSpeed', width: 100 },
                     { name: 'PowerSettings3TrueAirSpeed', index: 'PowerSettings3TrueAirSpeed', width: 100 },
                     { name: 'MaximumPassenger', index: 'MaximumPassenger', width: 80 },
                     {
                         name: 'VendorCD', index: 'VendorCD', width: 80,
                         searchoptions: {
                             dataInit: function (el) {
                                 var VendorCode = getQuerystring("VendorCD", "");
                                 if (VendorCode != null && VendorCode != "" && VendorCode != undefined) {
                                     var intervalId = setInterval(function () {
                                         var rowCount = $("#gridfleetProfile").getGridParam("reccount");
                                         if (rowCount != undefined && rowCount != null && rowCount != "" && rowCount > 0) {
                                             $(el).val(VendorCode);
                                             $(el).focus().trigger({ type: 'keypress', charCode: 13 });
                                             clearInterval(intervalId);
                                         }
                                     }, 1000);
                                 }
                             }
                         }
                     },
                     { name: 'IcaoID', index: 'ap.IcaoID', width: 75 },
                     { name: 'IsInactive', index: 'f.IsInactive', width: 55, formatter: 'checkbox', align: 'center', search: false, formatter: chkformat },
                    { name: 'HomebaseID', index: 'HomebaseID', hidden: true }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);

                    returnToParent(rowData);
                },
                onSelectRow: function (id) {

                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['AircraftCD'];//replace name with any column
                    selectedTailNum = $.trim(rowData['AircraftCD']);

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                loadComplete: function () {
                    var i, count, $grid = $(jqgridTableId);
                    var rowArray = $(jqgridTableId).jqGrid('getDataIDs');
                    for (i = 0, count = rowArray.length; i < count; i += 1) {
                        var rowData = $(jqgridTableId).getRowData(rowArray[i]);
                        if ($.trim(rowData["TailNum"]) == $.trim(selectedTailNum)) {
                            $grid.jqGrid('setSelection', rowArray[i], true);
                            break;
                        }
                    }
                },
                afterInsertRow: function (rowid, rowdata, rowelem) {
                    var lastSel = rowdata['TailNum'];//replace name with any column

                    if ($.trim(selectedTailNum) == $.trim(lastSel)) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        $(jqgridTableId).jqGrid('setSelection', rowid);
                    }
                }
            });
            $("#gridfleetProfile").jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
        });
        function reloadFleetProfile() {
            $('#gridfleetProfile').jqGrid().trigger('reloadGrid');

        }
    </script>
    <script type="text/javascript">
        function returnToParent(rowData) {
            var oArg = new Object();
            oArg.FleetID = rowData["FleetID"];
            oArg.FleetIDs = oArg.FleetID;
            oArg.TailNum = rowData["TailNum"];
            oArg.AircraftTypeCD = rowData["AircraftCD"];
            oArg.AirCraftTypeCode = rowData["AirCraft_AircraftCD"];
            oArg.AirCraftTypeCodes = oArg.AirCraftTypeCode;
            oArg.Arg1 = oArg.TailNum;
            oArg.CallingButton = "TailNum";
            oArg.Arg2 = oArg.FleetID;
            oArg.CallingButton1 = "FleetID";
            oArg.Arg3 = oArg.AirCraftTypeCode;
            oArg.CallingButton2 = "AirCraft_AircraftCD";
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }
    </script>

    <style type="text/css">
        body {
            height: 463px;
        }

        #pg_gridPager_center {
            width: 530px !important;
        }

            #pg_gridPager_center > table {
                margin-left: 250px;
            }

        .showDiv {
            text-align: right;
            margin-right: 77px;
            color: red;
            visibility: visible !important;
        }

        .hideDiv {
            visibility: hidden;
        }
    </style>

</head>

<body>
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div { height: 30px; }
        .ui-jqgrid .ui-th-div-ie { height: 30px; }
    </style>
    <form id="form1" runat="server">
         <div class="bootstrap_pop_minwrapper fleetpro_popup">
             <div class="jqgrid row">

            <div class="col-md-12">
                <table class="box1">
                    <tr>
                        <td>
                            <table class="client_headingtop">
                                <tr>
                                    <td>
                                        <label>
                                            <input type="checkbox" id="chkActiveOnly" checked="checked" />
                                            Active Only</label>


                                    </td>
                                    <td>
                                        <label>
                                            <input type="checkbox" id="chkHomeBaseOnly" />Home Base Only</label>

                                    </td>
                                    <td>
                                        <label>
                                            <input type="checkbox" id="chkRotary" checked="checked" />Rotary Only</label>
                                    </td>
                                    <td>
                                        <label>
                                            <input type="checkbox" id="chkFixedWing" checked="checked" />Fixed Wing Only</label>
                                    </td>
                                    <td>
                                        <label> &nbsp;&nbsp;&nbsp;Client Code:</label>
                                    </td>
                                    <td>
                                        <span>
                                            <input style="width: 48px;" type="text" id="tbClientCodeFilter" maxlength="5" class="text50" />
                                        </span>
                                    </td>
                                    <td>
                                        <input type="button" class="browse-button" id="btnClientCodeFilter" />
                                        <input id="hdnClientId" type="hidden" value="" />
                                    </td>
                                    <td>
                                        <input type="button" id="btnSearch" class="button" title="Search" onclick="reloadFleetProfile()" value="Search" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">
                                        <div id="errorMsg" class="hideDiv">Invalid client code.!</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridfleetProfile" style="width: 981px !important;" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <span class="Span">Page Size:</span>
                                <input class="PageSize" id="rowNum" type="text" value="20" maxlength="5" />
                                <input id="btnChange" class="btnChange" value="Change" type="submit" name="btnChange" onclick="reloadPageSize($(jqgridTableId), $('#rowNum')); return false;" />
                            </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />

                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        </div>
    </form>
</body>
</html>

