﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetComponentHistory : BaseSecuredPage
    {

        string DateFormat = string.Empty;
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // To Assign Date Format from User Control, based on Fleet Component

                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFComp, dgFComp, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client

                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }
                        ((RadDatePicker)ucDate.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;

                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFComp.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewFleetComponentHistory);
                            if (Request.QueryString["FleetID"] != null && Request.QueryString["FleetID"].ToString().Trim() != "")
                            {
                                hdnFleetID.Value = Request.QueryString["FleetID"].ToString();
                            }
                            if (Request.QueryString["TailNum"] != null && Request.QueryString["TailNum"].ToString().Trim() != "")
                            {
                                tbTailNumber.Text = Request.QueryString["TailNum"].ToString();
                            }
                            tbTailNumber.Enabled = false;
                            DefaultSelection();
                            btnCancel.Visible = false;
                            btnSaveChanges.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }


        }

        protected void dgFComp_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFComp.ClientSettings.Scrolling.ScrollTop = "0";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }


        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFComp_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }

        }
        /// <summary>
        /// Bind Delay Type Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFComp_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnFleetID.Value.ToString().Trim() == "")
                            hdnFleetID.Value = "0";

                        using (FlightPakMasterService.MasterCatalogServiceClient objDelayTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objVal = objDelayTypeService.GetFleetComponentList().EntityList.Where(x => x.FleetID == Convert.ToInt64(hdnFleetID.Value));

                            if (objVal != null)
                            {
                                dgFComp.DataSource = objVal;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }




        }
        /// <summary>
        /// Item Command for Fleet Component Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFComp_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    TextBox tbplastinspection = (TextBox)ucDate.FindControl("tbDate");
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.FleetComponent, Convert.ToInt64(hdnFleetComponentID.Value));

                                    Session["IsFleetComponentEditLock"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetComponent);
                                        DefaultSelection();
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    SelectItem();
                                    tbcomponent.Enabled = true;
                                    tbwarning.Enabled = true;
                                    tbserialnumber.Enabled = true;
                                    tbnextinspection.Enabled = true;
                                    tbplastinspection.Enabled = true;
                                    hdnSave.Value = "Update";
                                    hdnRedirect.Value = "";
                                    tbcomponent.Focus();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFComp.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                btnhours.Checked = true;
                                btncycles.Checked = false;
                                btndays.Checked = false;
                                tbplastinspection.Enabled = true;
                                tbcomponent.Enabled = true;
                                tbwarning.Enabled = true;
                                tbserialnumber.Enabled = true;
                                tbnextinspection.Enabled = true;
                                hdnFleetComponentID.Value = "0";
                                hdnSave.Value = "Insert";
                                break;
                            case "UpdateEdited":
                                dgFComp_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }

        }
        /// <summary>
        /// Update Command for Fleet Component  Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFComp_UpdateCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string componentcd = string.Empty;
                        e.Canceled = true;
                        if (hdnFleetComponentID.Value.ToString().Trim() != "" && Convert.ToInt64(hdnFleetComponentID.Value) > 0)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objFleetTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objFleetTypeService.UpdateFleetComponentType(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    // Unlock the Record
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.FleetComponent, Convert.ToInt64(hdnFleetComponentID.Value));
                                    }
                                    Session["IsFleetComponentEditLock"] = "False";
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;

                                    ShowSuccessMessage();

                                    GridEnable(true, true, true);
                                    btnSaveChanges.Visible = false;
                                    btnCancel.Visible = false;
                                    ClearForm();
                                    DefaultSelection();
                                    EnableForm(false);
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FleetComponent);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }


        }

        /// <summary>
        /// Update Command for Fleet Component Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFComp_InsertCommand1(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (MasterCatalogServiceClient objFleetComponentTypeService = new MasterCatalogServiceClient())
                        {
                            var objRetVal = objFleetComponentTypeService.AddFleetComponentType(GetItems());
                            if (objRetVal.ReturnFlag == true)
                            {
                                dgFComp.Rebind();
                                DefaultSelection();

                                ShowSuccessMessage();

                                GridEnable(true, true, true);
                                btnSaveChanges.Visible = false;
                                btnCancel.Visible = false;
                                ClearForm();
                                DefaultSelection();
                                EnableForm(false);
                                _selectLastModified = true;
                            }
                            else
                            {
                                //For Data Anotation
                                ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FleetComponent);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }

        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFComp_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (hdnFleetComponentID.Value.ToString().Trim() != null && Convert.ToInt64(hdnFleetComponentID.Value.ToString()) > 0)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objFleetComponentTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FleetComponent objFleetComponentType = new FlightPakMasterService.FleetComponent();
                                objFleetComponentType.FleetComponentID = Convert.ToInt64(hdnFleetComponentID.Value);
                                objFleetComponentType.IsDeleted = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.FleetComponent, Convert.ToInt64(Convert.ToInt64(hdnFleetComponentID.Value)));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection();
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetComponent);
                                        return;
                                    }
                                }
                                objFleetComponentTypeService.DeleteFleetComponentType(objFleetComponentType);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                ClearForm();
                                DefaultSelection();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FleetComponent, Convert.ToInt64(hdnFleetComponentID.Value));
                    }
                }
            }

        }
        /// <summary>
        /// Bind Selected Item in the  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFComp_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {

                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            GridDataItem item = dgFComp.SelectedItems[0] as GridDataItem;
                            hdnFleetComponentID.Value = item["FleetComponentID"].Text;

                            Label lblUser;
                            lblUser = (Label)dgFComp.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + item.GetDataKeyValue("LastUpdUID").ToString());
                            }
                            else
                            {
                                lblUser.Text = string.Empty;
                            }
                            if (item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.GetDataKeyValue("LastUpdTS").ToString())));
                            }

                            ReadOnlyForm();
                            GridEnable(true, true, true);
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                    }
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();

                btnCancel.Visible = true;
                btnSaveChanges.Visible = true;
                btncycles.Checked = false;
                btndays.Checked = false;
                btnhours.Checked = false;
                EnableForm(true);
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Delay Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (hdnSave.Value.ToString().ToUpper() == "UPDATE")
                        {
                            if (checkAllReadyExistforUpdate())
                            {
                                //cvSerialNumber.IsValid = false;
                                tbcomponent.Focus();
                            }
                            else
                            {
                                (dgFComp.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);                               
                            }

                        }
                        else
                        {
                            if (checkAllReadyExist())
                            {
                                //cvSerialNumber.IsValid = false;
                                tbcomponent.Focus();
                            }
                            else
                            {
                                (dgFComp.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);                                
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }

            //tbCode.ReadOnly = true;
        }

        protected void DefaultSelection()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                dgFComp.Rebind();

                if (dgFComp.MasterTableView.Items.Count > 0)
                {
                    //if (!IsPostBack)
                    //{
                    //    hdnFleetComponentID.Value = "0";
                    //}
                    if (string.IsNullOrEmpty(hdnFleetComponentID.Value) || hdnFleetComponentID.Value == "0")
                    {
                        dgFComp.SelectedIndexes.Add(0);
                        GridDataItem item = (GridDataItem)dgFComp.SelectedItems[0];
                        hdnFleetComponentID.Value = item["FleetComponentID"].Text;
                        //dgFComp.Items[0].Selected = true;
                    }

                    if (dgFComp.SelectedIndexes.Count == 0)
                        dgFComp.SelectedIndexes.Add(0);

                    ReadOnlyForm();
                }
                else
                {
                    EnableForm(false);
                }

                GridEnable(true, true, true);
            }
        }

        /// <summary>
        /// Cancel Delay Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //pnlExternalForm.Visible = false;
                        // Unlock the Record
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.FleetComponent, Convert.ToInt64(hdnFleetComponentID.Value));
                        }
                        GridEnable(true, true, true);
                        btnSaveChanges.Visible = false;
                        btnCancel.Visible = false;
                        ClearForm();
                        DefaultSelection();
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }

            //tbCode.ReadOnly = true;
            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFComp;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }

        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private FleetComponent GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.FleetComponent objFleetComponentType = new FlightPakMasterService.FleetComponent();

                if (hdnFleetComponentID.Value.ToString().Trim() != "")
                    objFleetComponentType.FleetComponentID = Convert.ToInt64(hdnFleetComponentID.Value);

                if (hdnFleetID.Value.ToString().Trim() != "")
                    objFleetComponentType.FleetID = Convert.ToInt64(hdnFleetID.Value);

                objFleetComponentType.FleetComponentDescription = tbcomponent.Text.ToUpper();
                if (tbcomponent.Text.Trim() != "")
                    objFleetComponentType.ComponentCD = tbcomponent.Text.ToUpper();
                else
                    objFleetComponentType.ComponentCD = null;
                if (!string.IsNullOrEmpty(tbnextinspection.Text))
                    objFleetComponentType.InspectionHrs = Convert.ToDecimal(tbnextinspection.Text);
                else
                    objFleetComponentType.InspectionHrs = null;
                if (!string.IsNullOrEmpty(tbwarning.Text))
                    objFleetComponentType.WarningHrs = Convert.ToDecimal(tbwarning.Text);
                else
                    objFleetComponentType.WarningHrs = null;

                if (!string.IsNullOrEmpty(tbserialnumber.Text))
                    objFleetComponentType.SericalNum = tbserialnumber.Text.ToUpper();
                else
                    objFleetComponentType.SericalNum = null;


                TextBox ucInsDate = (TextBox)ucDate.FindControl("tbDate");
                if (!string.IsNullOrEmpty(ucInsDate.Text.Trim().ToString()))
                {

                    objFleetComponentType.LastInspectionDT = DateTime.ParseExact(ucInsDate.Text.Trim(), DateFormat, CultureInfo.InvariantCulture);
                }
                else
                {
                    objFleetComponentType.LastInspectionDT = null;
                }


                if (btncycles.Checked)
                    objFleetComponentType.IsHrsDaysCycles = "C";
                if (btnhours.Checked)
                    objFleetComponentType.IsHrsDaysCycles = "H";
                if (btndays.Checked)
                    objFleetComponentType.IsHrsDaysCycles = "D";
                objFleetComponentType.IsDeleted = false;
                return objFleetComponentType;
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                btnhours.Checked = false;
                btndays.Checked = false;
                btncycles.Checked = false;

                using (FlightPakMasterService.MasterCatalogServiceClient objDelayTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objVal = objDelayTypeService.GetFleetComponentList().EntityList.Where(x => x.FleetComponentID == Convert.ToInt64(hdnFleetComponentID.Value));
                    if (objVal != null)
                    {

                        foreach (FlightPakMasterService.FleetComponent Fleet in objVal)
                        {
                            if (Fleet.FleetID != null && Fleet.FleetID.ToString().Trim() != "")
                            {
                                if (Convert.ToInt64(Fleet.FleetID) > 0)
                                {
                                    hdnFleetID.Value = Fleet.FleetID.ToString();
                                }
                                else
                                {
                                    hdnFleetID.Value = "0";
                                }
                            }
                            else
                            {
                                hdnFleetID.Value = "0";
                            }
                            if (Fleet.FleetComponentDescription != null && Fleet.FleetComponentDescription.ToString().Trim() != "")
                            {
                                tbcomponent.Text = Fleet.FleetComponentDescription.ToString().Trim();
                            }
                            else
                            {
                                tbcomponent.Text = "";
                            }
                            if (Fleet.InspectionHrs != null && Fleet.InspectionHrs.ToString().Trim() != "")
                            {
                                if (Convert.ToInt64(Fleet.InspectionHrs) > 0)
                                {
                                    tbnextinspection.Text = Fleet.InspectionHrs.ToString();
                                }
                                else
                                {
                                    tbnextinspection.Text = "0";
                                }
                            }
                            else
                            {
                                tbnextinspection.Text = "0";
                            }
                            if (Fleet.WarningHrs != null && Fleet.WarningHrs.ToString().Trim() != "")
                            {
                                if (Convert.ToInt64(Fleet.WarningHrs) > 0)
                                {
                                    tbwarning.Text = Fleet.WarningHrs.ToString();
                                }
                                else
                                {
                                    tbwarning.Text = "0";
                                }
                            }
                            else
                            {
                                tbwarning.Text = "0";
                            }
                            if (Fleet.SericalNum != null && Fleet.SericalNum.ToString().Trim() != "")
                            {
                                tbserialnumber.Text = Fleet.SericalNum.ToString().Trim();
                            }
                            else
                            {
                                tbserialnumber.Text = "";
                            }
                            btncycles.Checked = false;
                            btndays.Checked = false;
                            btnhours.Checked = false;
                            if (Fleet.IsHrsDaysCycles != null && Fleet.IsHrsDaysCycles.ToString().Trim() != "")
                            {
                                if (Fleet.IsHrsDaysCycles.ToString().Trim().ToUpper() == "H")
                                {
                                    btnhours.Checked = true;
                                }
                                else if (Fleet.IsHrsDaysCycles.ToString().Trim().ToUpper() == "D")
                                {
                                    btndays.Checked = true;
                                }
                                else if (Fleet.IsHrsDaysCycles.ToString().Trim().ToUpper() == "C")
                                {
                                    btncycles.Checked = true;
                                }
                            }


                            if (!string.IsNullOrEmpty(DateFormat))
                            {
                                ((TextBox)ucDate.FindControl("tbDate")).Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Fleet.LastInspectionDT);
                            }
                            else
                            {
                                ((TextBox)ucDate.FindControl("tbDate")).Text = string.Empty;
                            }
                            lbColumnName1.Text = "Component Description";
                            lbColumnName2.Text = "Serial No.";
                            lbColumnValue1.Text = System.Web.HttpUtility.HtmlEncode(Fleet.FleetComponentDescription.ToString());
                            lbColumnValue2.Text = System.Web.HttpUtility.HtmlEncode(Fleet.SericalNum.ToString());
                            break;
                        }

                        btnCancel.Visible = true;
                        btnSaveChanges.Visible = true;
                        EnableForm(true);
                    }
                }
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, delCtl, editCtl;

                insertCtl = (LinkButton)dgFComp.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                delCtl = (LinkButton)dgFComp.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                editCtl = (LinkButton)dgFComp.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                if (IsAuthorized(Permission.Database.AddFleetComponentHistory))
                {
                    insertCtl.Visible = true;
                    if (add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteFleetComponentHistory))
                {
                    delCtl.Visible = true;
                    if (delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = "";
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditFleetComponentHistory))
                {
                    editCtl.Visible = true;
                    if (edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = "";
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
            }
        }
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DisplayEditForm();
                TextBox tbplastinspection = (TextBox)ucDate.FindControl("tbDate");
                tbcomponent.Enabled = false;
                tbwarning.Enabled = false;
                tbserialnumber.Enabled = false;
                tbplastinspection.Enabled = false;
                tbnextinspection.Enabled = false;

                btnCancel.Visible = false;
                btnSaveChanges.Visible = false;
                EnableForm(false);
            }
        }
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbcomponent.Text = "";
                TextBox tblastinspection = (TextBox)ucDate.FindControl("tbDate");
                tblastinspection.Text = "";
                tbnextinspection.Text = "";
                tbwarning.Text = "";
                tbserialnumber.Text = "";
                //hdnFleetComponentID.Value = "0"; 
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                tbcomponent.Enabled = enable;
                tbnextinspection.Enabled = enable;
                TextBox tbplastinspection = (TextBox)ucDate.FindControl("tbDate");
                tbplastinspection.Enabled = enable;
                tbwarning.Enabled = enable;
                tbserialnumber.Enabled = enable;
                btncycles.Enabled = enable;
                btnhours.Enabled = enable;
                btndays.Enabled = enable;



            }
        }

        /// <summary>
        /// for setting format for date in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFComp_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem DataItem = e.Item as GridDataItem;
                            GridColumn Column = dgFComp.MasterTableView.GetColumn("LastInspectionDT");
                            string UwaValue = DataItem["LastInspectionDT"].Text.Trim();
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["LastInspectionDT"];
                            if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                            {
                                cell.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(cell.Text)));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {

                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }

        protected void SerialNumber_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbserialnumber.Text.Trim() != string.Empty)
                        {
                            checkAllReadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetComponent);
                }
            }

            //else
            //{

            //}
        }

        private bool checkAllReadyExist()
        {
            bool returnVal = false;
            using (FlightPakMasterService.MasterCatalogServiceClient objCrewRostersvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal1 = objCrewRostersvc.GetFleetComponentList().EntityList.Where(x => (x.SericalNum != null) && (x.FleetComponentDescription != null) && (x.FleetID == Convert.ToInt64(hdnFleetID.Value)));
                var objRetVal = objRetVal1.Where(x => (x.SericalNum.ToString().ToUpper().Trim().Equals(tbserialnumber.Text.ToString().ToUpper().Trim())) && (x.FleetComponentDescription.ToString().ToUpper().Trim().Equals(tbcomponent.Text.ToString().ToUpper().Trim())));
                if (objRetVal.Count() > 0 && objRetVal != null)
                {
                    string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                        + " oManager.radalert('The Combination of the Component and Serial Number must be Unique.', 360, 50, 'Fleet Component Catalog');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                    tbcomponent.Focus();
                    return true;
                }
            }
            return returnVal;
        }

        private bool checkAllReadyExistforUpdate()
        {
            bool returnVal = false;
            using (FlightPakMasterService.MasterCatalogServiceClient objCrewRostersvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (hdnFleetComponentID.Value.Trim() != "")
                {
                    var objRetVal1 = objCrewRostersvc.GetFleetComponentList().EntityList.Where(x => (x.SericalNum != null) && (x.FleetComponentDescription != null) && (x.FleetComponentID != Convert.ToInt64(hdnFleetComponentID.Value)) && (x.FleetID == Convert.ToInt64(hdnFleetID.Value)));
                    var objRetVal = objRetVal1.Where(x => (x.SericalNum.ToString().ToUpper().Trim().Equals(tbserialnumber.Text.ToString().ToUpper().Trim())) && (x.FleetComponentDescription.ToString().ToUpper().Trim().Equals(tbcomponent.Text.ToString().ToUpper().Trim())));
                    if (objRetVal.Count() > 0 && objRetVal != null)
                    {
                        string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                            + " oManager.radalert('The Combination of the Component and Serial Number must be Unique.', 360, 50, 'Fleet Component Catalog');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        tbcomponent.Focus();
                        return true;
                    }
                }
            }
            return returnVal;
        }
        
        
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!string.IsNullOrEmpty(hdnFleetComponentID.Value) && Convert.ToInt64(hdnFleetComponentID.Value) > 0)
                    {
                        string ID = hdnFleetComponentID.Value;
                        foreach (GridDataItem Item in dgFComp.MasterTableView.Items)
                        {
                            if (Item["FleetComponentID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection();
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFComp.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var ComponentValue = FPKMstService.GetFleetComponentList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, ComponentValue);
            List<FlightPakMasterService.FleetComponent> filteredList = GetFilteredList(ComponentValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FleetComponentID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFComp.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFComp.CurrentPageIndex = PageNumber;
            dgFComp.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfFleetComponent ComponentValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    hdnFleetComponentID.Value = Session["SearchItemPrimaryKeyValue"].ToString();
                }
            }
            else
            {
                PrimaryKeyValue = ComponentValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FleetComponentID;
                hdnFleetComponentID.Value = PrimaryKeyValue.ToString();
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.FleetComponent> GetFilteredList(ReturnValueOfFleetComponent ComponentValue)
        {
            List<FlightPakMasterService.FleetComponent> filteredList = new List<FlightPakMasterService.FleetComponent>();

            if (ComponentValue.ReturnFlag)
            {
                filteredList = ComponentValue.EntityList.Where(x => x.FleetID == Convert.ToInt64(hdnFleetID.Value)).ToList();
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFComp.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFComp.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        protected void dgFComp_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFComp, Page.Session);
        }
    }
}