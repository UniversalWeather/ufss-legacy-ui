﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="AutoID" CodeBehind="FleetProfileInternationalData.aspx.cs"
    MasterPageFile="~/Framework/Masters/Settings.master" Inherits="FlightPak.Web.Views.Settings.Fleet.FleetProfileInternationalData"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function HideOwner() {

                var pnlData = document.getElementById("trownedlnfnmn");
                pnlData.style.display = 'none';
                var lnfnmn = document.getElementById("trownedcompanyname");
                lnfnmn.style.visibility = 'visible';
            }

            function HideOperator() {

                var pnlData = document.getElementById("troperatorlnfnmn");
                pnlData.style.display = 'none';
                var lnfnmn = document.getElementById("troperatorcompanyname");
                lnfnmn.style.visibility = 'visible';
            }

            function openWin(radWin) {
                var url = '';
                if (radWin == "RadCountryMasterPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbownercountry.ClientID%>').value;
                }
                else if (radWin == "RadCountryMasterPup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tboperatorcountry.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }
            function onClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbownercountry.ClientID %>");
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbownercountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountry1.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbownercountry.ClientID%>").value = "";
                        document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountry1.ClientID%>").value = "0";
                    }
                }
            }
            function onClientCloseCountryPup(oWnd, args) {
                var combo = $find("<%= tboperatorcountry.ClientID %>");
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tboperatorcountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=cvOCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountry2.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tboperatorcountry.ClientID%>").value = "";
                        document.getElementById("<%=cvOCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountry2.ClientID%>").value = "0";
                    }
                }
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

        </script>
        <script runat="server">
       
            void QueryStringButton_Click(object sender, EventArgs e)
            {
                string URL = "../Fleet/FleetProfileCatalog.aspx?FleetID=" + hdnFleetID.Value + "&Screen=FleetProfile";
                Response.Redirect(URL);
            }

      
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCountryMasterPup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseCountryPup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">
                        <asp:LinkButton ID="LinkButton1" Text="Fleet Profile " runat="server" OnClick="QueryStringButton_Click"></asp:LinkButton>&nbsp;>
                        Fleet Profile International Data</span> <span class="tab-nav-icons">
                            <%--<a href="#" class="search-icon">
                        </a><a href="#" class="save-icon"></a><a href="#" class="print-icon"></a>--%><a href="#"
                            title="Help" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" class="sticky">
        <tr>
            <td class="tdLabel100">
                Tail No.
            </td>
            <td>
                <asp:TextBox ID="tbTailNumber" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" id="tdSuccessMessage" class="success_msg" style="width: 50%;">
                Record saved successfully.
            </td>
            <td style="width: 50%;">
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" class="border-box">
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150">
                                        <asp:Label ID="lbDecalNo" Text="Decal No." runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbdecalnumber" runat="server" CssClass="text60" MaxLength="8"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                         <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150">
                                        <asp:Label ID="lbYear1" Text="Year" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbYear1" runat="server" CssClass="text60" MaxLength="8"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150">
                                        <asp:Label ID="lbDecalNo2" Text="Decal No." runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbdecalnumber2" runat="server" CssClass="text60" MaxLength="8"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                         <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150">
                                        <asp:Label ID="lbYear2" Text="Year" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbYear2" runat="server" CssClass="text60" MaxLength="8"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150">
                                        Aircraft Make
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbaircraftmake" runat="server" CssClass="text310" MaxLength="30"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150">
                                        Aircraft Model
                                    </td>
                                    <td class="tdLabel220">
                                        <asp:TextBox ID="tbaircraftmodel" runat="server" CssClass="text200" MaxLength="20"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel40">
                                        Year
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbyear" runat="server" CssClass="text50" MaxLength="4" OnTextChanged="Year_TextChanged"
                                            AutoPostBack="true" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150">
                                        <asp:Label ID="lbColors" Text="Colors" runat="server"></asp:Label>
                                    </td>
                                    <td class="tdLabel220">
                                        <asp:TextBox ID="tbcolors" runat="server" MaxLength="15"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel40">
                                    </td>
                                    <td>
                                    </td>
                                    <td align="right">
                                        <asp:CustomValidator ID="cvYear" runat="server" ControlToValidate="tbyear" ErrorMessage="Year should be 1900 to 2100"
                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150">
                                        <asp:Label ID="lbColors1" Text="Colors" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbcolors1" runat="server" MaxLength="15"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel150">
                                        <asp:Label ID="lbTrim" Text="Trim" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbtrim" runat="server" MaxLength="15"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Owner/Lessee Details </legend>
                                <table id="tblowner" width="100%">
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr id="trownedbycompany">
                                                    <td align="left" nowrap="nowrap">
                                                        <asp:CheckBox ID="chkownerownedbycompany" Text="Owned by Company" AutoPostBack="true"
                                                            OnCheckedChanged="ownedbycompany_checkedchanged" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr id="trownedcompanyname" runat="server" visible="false">
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbownercompanyname" Text="Company Name" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel140">
                                                        <asp:TextBox ID="tbownercompanyname" runat="server" MaxLength="100" Width="575px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr id="TrAdd1" runat="server">
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbLastName" Text="Last Name" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel150">
                                                        <asp:TextBox ID="tblastname" CssClass="text100" runat="server" MaxLength="20"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel80">
                                                        <asp:Label ID="lbFirstName" Text="First Name" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel140">
                                                        <asp:TextBox ID="tbfirstname" runat="server" CssClass="text100" MaxLength="20"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel100">
                                                        <asp:Label ID="lbMiddleName" Text="Middle Name" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbmiddlename" runat="server" CssClass="text100" MaxLength="20"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbAddressLine1" Text="Address Line 1" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel240">
                                                        <asp:TextBox ID="tbownerAddr1" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel130">
                                                        <asp:Label ID="lbAddressLine2" Text="Address Line 2" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbownerAddr2" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbAddressLine3" Text="Address Line 3" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbownerAddr3" CssClass="text200" runat="server" MaxLength="40"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbCity" Text="City" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel240">
                                                        <asp:TextBox ID="tbownercity1" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel130">
                                                        <asp:Label ID="lbStateProv" Text="State/Prov" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbownerstate" CssClass="text200" runat="server" MaxLength="10"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbCountry" Text="Country" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel240">
                                                        <asp:HiddenField ID="hdnCountry1" runat="server" />
                                                        <asp:TextBox ID="tbownercountry" CssClass="text170" runat="server" AutoPostBack="true"
                                                            OnTextChanged="tbownercountry_TextChanged" MaxLength="3"></asp:TextBox>
                                                        <asp:Button ID="btnCountry1" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadCountryMasterPopup');return false;" />
                                                    </td>
                                                    <td class="tdLabel130">
                                                        <asp:Label ID="lbPostal" Text="Postal" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbownerpostal1" CssClass="text200" runat="server" MaxLength="15"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbownercountry"
                                                            ErrorMessage="Invalid Country Code." Display="Dynamic" CssClass="alert-text"
                                                            ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbBusinessPhone" Text="Business Phone" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel240">
                                                        <asp:TextBox ID="tbOwnerBusinessphone" CssClass="text200" runat="server" MaxLength="25"
                                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel130">
                                                        <asp:Label ID="lbHomePhone" Text="Home Phone" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbownerphone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbOtherPhone" Text="Other Phone" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbOwnerOtherphone" CssClass="text200" runat="server" MaxLength="25"
                                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbBusinessFax" Text="Business Fax" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel240">
                                                        <asp:TextBox ID="tbownerfax1" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel130">
                                                        <asp:Label ID="lbHomeFax" Text="Home Fax" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbOwnerHomeFax" CssClass="text200" runat="server" MaxLength="25"
                                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        Primary Mobile/Cell
                                                    </td>
                                                    <td class="tdLabel240">
                                                        <asp:TextBox ID="tbOwnerPrimaryMobile" CssClass="text200" runat="server" MaxLength="25"
                                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel130">
                                                        Secondary Mobile/Cell
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbOwnerSecondaryMobile" CssClass="text200" runat="server" MaxLength="25"
                                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbBusinessEmail" Text="Business E-mail" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel240">
                                                        <asp:TextBox ID="tbowneremail1" CssClass="text200" runat="server" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel130">
                                                        <asp:Label ID="lbPersonalEmail" Text="Personal E-mail" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbOwnerPersonalEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:RegularExpressionValidator ID="revowneremail1" runat="server" ControlToValidate="tbowneremail1"
                                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:RegularExpressionValidator ID="revOwnerPersonalEmail" runat="server" ControlToValidate="tbOwnerPersonalEmail"
                                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        Other E-mail
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbOwnerOthermail" runat="server" CssClass="text200" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:RegularExpressionValidator ID="revOwnerOthermail" runat="server" ControlToValidate="tbOwnerOthermail"
                                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Operator Details</legend>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="left" nowrap="nowrap">
                                                        <asp:CheckBox ID="chkoperatorOwnedbyCompany" Text="Owned by Company" AutoPostBack="true"
                                                            OnCheckedChanged="operatorownedbycompany_checkchanged" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr id="troperatorcompanyname" runat="server" visible="false">
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lboperatorcompanyname" Text="Company Name" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel140">
                                                        <asp:TextBox ID="tboperatorcompanyname" runat="server" MaxLength="100" Width="575px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr id="TrAdd2" runat="server">
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbLastName1" Text="Last Name" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel150">
                                                        <asp:TextBox ID="tboperatorlastname" CssClass="text100" runat="server" MaxLength="20"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel80">
                                                        <asp:Label ID="lbFirstName1" Text="First Name" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel140">
                                                        <asp:TextBox ID="tboperatorfirstname" CssClass="text100" runat="server" MaxLength="20"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel100">
                                                        <asp:Label ID="lbMiddleName1" Text="Middle Name" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tboperatormiddlename" CssClass="text100" runat="server" MaxLength="20"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbAddress1Line1" Text="Address Line 1" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel240">
                                                        <asp:TextBox ID="tboperatoraddr1" CssClass="text200" runat="server" MaxLength="25"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel130">
                                                        <asp:Label ID="lbAddress1Line2" Text="Address Line 2" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tboperatoraddr2" CssClass="text200" runat="server" MaxLength="25"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="tdLabel140">
                                                        <asp:Label ID="lbAddress1Line3" Text="Address Line 3" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tboperatoraddr3" CssClass="text200" runat="server" MaxLength="40"></asp:TextBox>
                                                    </td>
                                                </tr>
                                    </tr>
                                </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbCity1" Text="City" runat="server"></asp:Label>
                                    </td>
                                    <td class="tdLabel240">
                                        <asp:TextBox ID="tboperatorcity1" CssClass="text200" runat="server" MaxLength="25"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel130">
                                        <asp:Label ID="lbState11" Text="State/Prov" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tboperatorstateprov" CssClass="text200" runat="server" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbCountry1" Text="Country" runat="server"></asp:Label>
                                    </td>
                                    <td class="tdLabel240">
                                        <asp:TextBox ID="tboperatorcountry" CssClass="text170" runat="server" MaxLength="3"
                                            AutoPostBack="true" OnTextChanged="tboperatorcountry_TextChanged"> </asp:TextBox>
                                        <asp:HiddenField ID="hdnCountry2" runat="server" />
                                        <asp:Button runat="server" ID="btnOpCountry" CssClass="browse-button" OnClientClick="javascript:openWin('RadCountryMasterPup');return false;" />
                                    </td>
                                    <td class="tdLabel130">
                                        <asp:Label ID="lbPostal1" Text="Postal" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tboperatorpostal" CssClass="text200" runat="server" MaxLength="15"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:CustomValidator ID="cvOCountry" runat="server" ControlToValidate="tboperatorcountry"
                                            ErrorMessage="Invalid Country Code." Display="Dynamic" CssClass="alert-text"
                                            ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbBusinessPhone1" Text="Business Phone" runat="server"></asp:Label>
                                    </td>
                                    <td class="tdLabel240">
                                        <asp:TextBox ID="tbOperatorBusinessPhone" CssClass="text200" runat="server" MaxLength="25"
                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                    </td>
                                    <td class="tdLabel130">
                                        <asp:Label ID="lbHomePhone1" Text="Home Phone" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tboperatorphone" CssClass="text200" runat="server" MaxLength="25"
                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbOtherPhone1" Text="Other Phone" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbOperatorOtherPhone" CssClass="text200" runat="server" MaxLength="25"
                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbBusinessFax1" Text="Business Fax" runat="server"></asp:Label>
                                    </td>
                                    <td class="tdLabel240">
                                        <asp:TextBox ID="tboperatorfax1" CssClass="text200" runat="server" MaxLength="25"
                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                    </td>
                                    <td class="tdLabel130">
                                        <asp:Label ID="lbHomeFax1" Text="Home Fax" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbOperatorHomeFax" CssClass="text200" runat="server" MaxLength="25"
                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        Primary Mobile/Cell
                                    </td>
                                    <td class="tdLabel240">
                                        <asp:TextBox ID="tbOperatorPrimaryMobile" CssClass="text200" runat="server" MaxLength="25"
                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                    </td>
                                    <td class="tdLabel130">
                                        Secondary Mobile/Cell
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbOperatorSecondaryMobile" CssClass="text200" runat="server" MaxLength="25"
                                            onKeyPress="return fnAllowAlphaNumericAndChar(this,event,'-,+,(,),#, ,,') "></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbBusinessEmail1" Text="Business E-mail" runat="server"></asp:Label>
                                    </td>
                                    <td class="tdLabel240">
                                        <asp:TextBox ID="tboperatoremail1" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel130">
                                        <asp:Label ID="lbPersonalEmail1" Text="Personal E-mail" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbOperatorPersonalEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel140">
                                    </td>
                                    <td class="tdLabel240">
                                        <asp:RegularExpressionValidator ID="revoperatoremail1" runat="server" ControlToValidate="tboperatoremail1"
                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                    </td>
                                    <td class="tdLabel130">
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="revOperatorPersonalEmail" runat="server" ControlToValidate="tbOperatorPersonalEmail"
                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        Other E-mail
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbOperatorOtherEmail" runat="server" CssClass="text200" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td class="tdLabel140">
                                        <asp:RegularExpressionValidator ID="revOperatorOtherEmail" runat="server" ControlToValidate="tbOperatorOtherEmail"
                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </td> </tr> </table>
    <table class="pad-top-btm" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="right">
                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                    <tr>
                        <td align="center">
                            <asp:HiddenField ID="hdnMode" runat="server" />
                            <asp:HiddenField ID="hdnFPIId" runat="server" />
                            <asp:HiddenField ID="hdnFleetID" runat="server" />
                            <asp:Button ID="btnsave" CssClass="button" OnClick="SaveChanges_Click" ValidationGroup="Save"
                                Text="Save" runat="server" />
                            <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                                OnClick="Cancel_Click" CssClass="button" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
