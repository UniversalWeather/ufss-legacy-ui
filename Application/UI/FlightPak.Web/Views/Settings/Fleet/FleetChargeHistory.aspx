﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Settings.master"
    ClientIDMode="AutoID" CodeBehind="FleetChargeHistory.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.FleetChargeHistory"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script runat="server">       
            void QueryStringButton_Click(object sender, EventArgs e)
            {
                string URL = "../Fleet/FleetProfileCatalog.aspx?FleetID=" + hdnFleetID.Value + "&Screen=FleetProfile";
                Response.Redirect(URL);
            }        
        </script>
        <script type="text/javascript">
            function btnSaveChanges_OnClientClick(sender, args) {
                var ReturnValue = true;
                var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                    if (shouldSubmit) {
                        document.getElementById('<%=hdnCalculateFleetChargeRate.ClientID %>').value = "YES";
                        ReturnValue = true;
                    }
                    else {
                        document.getElementById('<%=hdnCalculateFleetChargeRate.ClientID %>').value = "NO";
                        ReturnValue = false;
                    }
                    sender.click();
                });
                if (document.getElementById('<%=hdnSave.ClientID%>').value == "Insert") {
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm('Do you want to Update the Flight Costs of the Existing Trips and Flight Logs', callBackFunction, 400, 100, null, "Fleet Charge Rate");
                    args.set_cancel(true);
                }
                if (document.getElementById('<%=hdnSave.ClientID%>').value == "Update") {
                    var grid = $find("<%=dgFleetChargeRate.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var length = MasterTable.get_dataItems().length;
                    var FleetChargeRateID = document.getElementById('<%=hdnFleetChargeRateID.ClientID %>').value;
                    var BeginDate = document.forms[0].ctl00$ctl00$MainContent$SettingBodyContent$ucBegDate$tbDate.value;
                    var EndDate = document.forms[0].ctl00$ctl00$MainContent$SettingBodyContent$uctbEnd$tbDate.value;
                    var ChargeRate = document.getElementById('<%=tbchgrate.ClientID %>').value;
                    var ChargeUnit;
                    if (document.getElementById('<%=btnhours.ClientID %>').checked == true) {
                        ChargeUnit = "H";
                    }
                    else if (document.getElementById('<%=btndays.ClientID %>').checked == true) {
                        ChargeUnit = "N";
                    }
                    else if (document.getElementById('<%=btncycles.ClientID %>').checked == true) {
                        ChargeUnit = "S";
                    }
                    for (var i = 0; i < length; i++) {
                        if (MasterTable.get_dataItems()[i].getDataKeyValue('FleetChargeRateID') == FleetChargeRateID) {
                            if ((BeginDate != MasterTable.get_dataItems()[i].getDataKeyValue('BeginRateDT')) ||
                                (EndDate != MasterTable.get_dataItems()[i].getDataKeyValue('EndRateDT')) ||
                                (ChargeRate != MasterTable.get_dataItems()[i].getDataKeyValue('ChargeRate')) ||
                                (ChargeUnit != MasterTable.get_dataItems()[i].getDataKeyValue('ChargeUnit'))) {
                                
                                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                                oManager.radconfirm('Do you want to Update the Flight Costs of the Existing Trips and Flight Logs', callBackFunction, 400, 100, null, "Fleet Charge Rate");
                                args.set_cancel(true);
                                break;
                            }
                            else {
                                document.getElementById('<%=hdnCalculateFleetChargeRate.ClientID %>').value = "NO";
                                ReturnValue = false;
                                break;
                            }
                        }
                    }
                }
                return false;
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div id="TBLink" runat="server" class="tab-nav-top">
                    <span class="head-title">
                        <asp:LinkButton ID="LinkButton1" Text="Fleet Profile " runat="server" OnClick="QueryStringButton_Click"></asp:LinkButton>&nbsp;>
                        Fleet Charge Rate History</span> <span class="tab-nav-icons">
                            <%--<a href="#" class="search-icon">
                        </a><a href="#" class="save-icon"></a><a href="#" class="print-icon"></a>--%><a href="#"
                            title="Help" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" class="sticky">
        <tr style="display: none;">
            <td colspan="2">
                <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
            </td>
        </tr>
        <tr>
            <td class="tdLabel100">
                Tail No.
            </td>
            <td>
                <asp:TextBox ID="tbTailNumber" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgFleetChargeRate" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgFleetChargeRate" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgFleetChargeRate" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFleetChargeRate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgFleetChargeRate" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>

    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <telerik:RadGrid ID="dgFleetChargeRate" runat="server" AllowSorting="true" OnItemCreated="dgFleetChargeRate_ItemCreated"
        OnNeedDataSource="dgFleetChargeRate_BindData" OnItemCommand="dgFleetChargeRate_ItemCommand"
        OnPageIndexChanged="dgFleetChargeRate_PageIndexChanged" OnInsertCommand="dgFleetChargeRate_InsertCommand"
        OnUpdateCommand="dgFleetChargeRate_UpdateCommand" OnDeleteCommand="dgFleetChargeRate_DeleteCommand"
        OnItemDataBound="dgFleetChargeRate_ItemDataBound" AutoGenerateColumns="false"
        Height="341px" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgFleetChargeRate_SelectedIndexChanged"
        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPreRender="dgFleetChargeRate_PreRender">
        <MasterTableView DataKeyNames="FleetChargeRateID,FleetID,AircraftCD,BeginRateDT,EndRateDT,ChargeRate,ChargeUnit,LastUpdUID,LastUpdTS"
            ClientDataKeyNames="FleetChargeRateID,FleetID,AircraftCD,BeginRateDT,EndRateDT,ChargeRate,ChargeUnit,LastUpdUID,LastUpdTS"
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="BeginRateDT" HeaderText="Begin" AllowFiltering="false"
                    ShowFilterIcon="false" HeaderStyle-Width="80px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EndRateDT" HeaderText="End" AllowFiltering="false"
                    ShowFilterIcon="false" HeaderStyle-Width="80px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ChargeRate" HeaderText="Charge Rate" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="EqualTo" HeaderStyle-Width="300px" FilterDelay="500"
                    FilterControlWidth="280px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ChargeUnit" HeaderText="Charge Unit" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="300px" FilterDelay="500"
                    FilterControlWidth="280px">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                        Visible='<%# IsAuthorized(Permission.Database.AddFleetChargeHistory)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        Visible='<%# IsAuthorized(Permission.Database.EditFleetChargeHistory)%>' ToolTip="Edit"
                        CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                        Visible='<%# IsAuthorized(Permission.Database.DeleteFleetChargeHistory)%>' runat="server"
                        CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                </div>
                <div style="padding: 5px 5px; float: right;">
                    <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false"></GroupingSettings>
    </telerik:RadGrid>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="PnlBody" runat="server">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" class="border-box">
                <tr>
                    <td width="30%">
                        <fieldset>
                            <legend>Date Range</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel90">
                                        <span class="mnd_text">
                                            <asp:Label ID="lblBeginDate" runat="server" Text=" Begin Date"></asp:Label></span>
                                    </td>
                                    <td>
                                        <uc:DatePicker ID="ucBegDate" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="mnd_text">
                                            <asp:Label ID="lblEndDate" runat="server" onblur="return checktextbox()" Text="End Date"></asp:Label>
                                        </span>
                                    </td>
                                    <td>
                                        <uc:DatePicker ID="uctbEnd" runat="server"></uc:DatePicker>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td align="left" valign="top">
                        <table>
                            <tr>
                                <td>
                                    Charge Rate:
                                </td>
                                <td class="pr_radtextbox_150">
                                    <%--<asp:TextBox ID="tbchgrate" runat="server" MaxLength="18" ValidationGroup="Save"
                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" CssClass="tdtext165"></asp:TextBox>--%>
                                    <telerik:RadNumericTextBox ID="tbchgrate" runat="server" Type="Currency" Culture="en-US"
                                        MaxLength="15" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <%--<asp:CustomValidator ID="cvRate" runat="server" ControlToValidate="tbchgrate" ErrorMessage="Invalid Format (NNNNNNNNNNNNNNN.NN)"
                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Charge Unit:
                                </td>
                                <td>
                                    <asp:RadioButton ID="btnhours" GroupName="Radio" Text="Hours" Checked="true" runat="server" />
                                    <asp:RadioButton ID="btndays" Text="Nautical" GroupName="Radio" runat="server" />
                                    <asp:RadioButton ID="btncycles" Text="Statute" GroupName="Radio" runat="server" />
                                    <asp:RadioButton ID="btnKM" Text="Kilometers" GroupName="Radio" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea" width="100%">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="right" class="custom_radbutton">
                                    <telerik:RadButton ID="btnSaveChanges" runat="server" Text="Save" ValidationGroup="Save"
                                        CausesValidation="false" OnClick="SaveChanges_Click" OnClientClicking="btnSaveChanges_OnClientClick" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" Text="Cancel" OnClick="Cancel_Click" CausesValidation="false"
                                        runat="server" CssClass="button" />
                                    <asp:HiddenField ID="hdnFleetID" runat="server" />
                                    <asp:HiddenField ID="hdnFleetChargeRateID" runat="server" />
                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                    <asp:HiddenField ID="hdnCalculateFleetChargeRate" runat="server" />
                                    <asp:HiddenField ID="hdnRedirect" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
