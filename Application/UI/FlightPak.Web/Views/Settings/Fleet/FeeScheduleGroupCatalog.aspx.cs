﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FeeScheduleGroupCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private bool FeeGroupPageNavigated = false;
        private List<string> lstFeeGroupCodes = new List<string>();
        private string strFeeGroupID = "";
        private string strFeeGroupCD = "";
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFeeGroup, dgFeeGroup, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFeeGroup.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewFeeGroupReport);

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewFeeGroup);
                            base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                chkSearchActiveOnly.Checked = false;
                                dgFeeGroup.Rebind();
                                ReadOnlyForm();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                        if (IsPopUp)
                        {
                            dgFeeGroup.AllowPaging = false;
                            chkSearchActiveOnly.Visible = false;


                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }

        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFeeGroup.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var FeeGroupValue = FPKMstService.GetFeeGroupList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, FeeGroupValue);
            List<FlightPakMasterService.FeeGroup> filteredList = GetFilteredList(FeeGroupValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FeeGroupID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFeeGroup.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFeeGroup.CurrentPageIndex = PageNumber;
            dgFeeGroup.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfFeeGroup FeeGroupValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedFeeGroupID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = FeeGroupValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FeeGroupID;
                Session["SelectedFeeGroupID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.FeeGroup> GetFilteredList(ReturnValueOfFeeGroup FeeGroupValue)
        {
            List<FlightPakMasterService.FeeGroup> filteredList = new List<FlightPakMasterService.FeeGroup>();

            if (FeeGroupValue.ReturnFlag)
            {
                filteredList = FeeGroupValue.EntityList.Where(x => x.IsDeleted == false).ToList();
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<FeeGroup>(); }
                }
            }

            return filteredList;
        }

        /// <summary>
        /// To select the first record as default in read only mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgFeeGroup.Rebind();
                    }
                    if (dgFeeGroup.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedFeeGroupID"] = null;
                        //}
                        if (Session["SelectedFeeGroupID"] == null)
                        {
                            dgFeeGroup.SelectedIndexes.Add(0);

                            if (!IsPopUp)
                            {
                                Session["SelectedFeeGroupID"] = dgFeeGroup.Items[0].GetDataKeyValue("FeeGroupID").ToString();
                            }
                        }

                        if (dgFeeGroup.SelectedIndexes.Count == 0)
                            dgFeeGroup.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the selected item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFeeGroupID"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                        //string code = items.GetDataKeyValue("FeeGroupCD").ToString();
                        string ID = Session["SelectedFeeGroupID"].ToString();
                        foreach (GridDataItem item in dgFeeGroup.MasterTableView.Items)
                        {
                            if (item["FeeGroupID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton deleteButton = (e.Item as GridCommandItem).FindControl("lnkDelete") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(deleteButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Bind Fee Schedule Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objFeeGroupService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objDelVal = objFeeGroupService.GetFeeGroupList();
                            if (objDelVal.ReturnFlag == true)
                            {
                                dgFeeGroup.DataSource = objDelVal.EntityList;
                            }
                            Session["FeeGroupCodes"] = objDelVal.EntityList.ToList();
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        /// <summary>
        /// Item Command for Fee Schedule Group Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                hdnRedirect.Value = "";
                                if (Session["SelectedFeeGroupID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.FeeGroup, Convert.ToInt64(Session["SelectedFeeGroupID"].ToString().Trim()));
                                        Session["IsFeeGroupEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FeeGroup);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FeeGroup);

                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        SelectItem();
                                        DisableLinks();
                                        tbCode.Enabled = false;
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFeeGroup.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                tbCode.Focus();
                                break;
                            case "UpdateEdited":
                                dgFeeGroup_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        /// <summary>
        /// Update Command for Fee Schedule Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedFeeGroupID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objFeeGroupService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objFeeGroupService.UpdateFeeGroup(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    ///////Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.FeeGroup, Convert.ToInt64(Session["SelectedFeeGroupID"].ToString().Trim()));
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        DefaultSelection(true);

                                    }
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FeeGroup);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            // Session["SelectedFeeGroupID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radFeeGroupPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        /// <summary>
        /// Checking New Fee Schedule Group already exists in the Fee Schedule Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>

        private bool CheckAllReadyExist()
        {
            bool ReturnFlag = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    List<FeeGroup> FeeGroupList = new List<FeeGroup>();
                    FeeGroupList = ((List<FeeGroup>)Session["FeeGroupCodes"]).Where(x => x.FeeGroupCD.ToString().ToUpper().Trim().Contains(tbCode.Text.ToString().ToUpper().Trim())).ToList<FeeGroup>();
                    if (FeeGroupList.Count != 0)
                    {
                        cvFeeGroupCode.IsValid = false;
                        tbCode.Focus();
                        ReturnFlag = true;
                    }
                    return ReturnFlag;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnFlag;
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFeeGroup.ClientSettings.Scrolling.ScrollTop = "0";
                        FeeGroupPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        /// <summary>
        /// Insert Command for Fee Schedule Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvFeeGroupCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objFeeGroupService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objFeeGroupService.AddFeeGroup(GetItems());
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    dgFeeGroup.Rebind();
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FeeGroup);
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radFeeGroupPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedFeeGroupID"] != null)
                        {
                            FlightPakMasterService.MasterCatalogServiceClient FeeGroupService = new FlightPakMasterService.MasterCatalogServiceClient();
                            FlightPakMasterService.FeeGroup FeeGroup = new FlightPakMasterService.FeeGroup();
                            string Code = Session["SelectedFeeGroupID"].ToString();
                            strFeeGroupCD = "";
                            strFeeGroupID = "";
                            foreach (GridDataItem Item in dgFeeGroup.MasterTableView.Items)
                            {
                                if (Item["FeeGroupID"].Text.Trim() == Code.Trim())
                                {
                                    strFeeGroupCD = Item["FeeGroupCD"].Text.Trim();
                                    strFeeGroupID = Item["FeeGroupID"].Text.Trim();
                                    break;
                                }
                            }
                            FeeGroup.FeeGroupCD = strFeeGroupCD;
                            FeeGroup.FeeGroupID = Convert.ToInt64(strFeeGroupID);
                            FeeGroup.IsDeleted = true;
                            //Lock will happen from UI
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.FeeGroup, Convert.ToInt64(strFeeGroupID));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                    if (IsPopUp)
                                        ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FeeGroup);
                                    else
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FeeGroup);
                                    return;
                                }
                                FeeGroupService.DeleteFeeGroup(FeeGroup);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FeeGroup, Convert.ToInt64(strFeeGroupID));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgFeeGroup.SelectedItems[0] as GridDataItem;
                                Session["SelectedFeeGroupID"] = item["FeeGroupID"].Text;
                                if (btnSaveChanges.Visible == false)
                                {
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);
                                }
                            }

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                    }
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Fee Schedule Group on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            if (CheckAllReadyExist())
                            {
                                cvFeeGroupCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Fee Schedule Group Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgFeeGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgFeeGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        /// <summary>
        /// Cancel Fee Schedule Group Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedFeeGroupID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.FeeGroup, Convert.ToInt64(Session["SelectedFeeGroupID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        EnableLinks();
                        //Session.Remove("SelectedFeeGroupID");
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radFeeGroupPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFeeGroup;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private FeeGroup GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.FeeGroup FeeGroupService = null;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    FeeGroupService = new FlightPakMasterService.FeeGroup();
                    FeeGroupService.FeeGroupCD = tbCode.Text;
                    FeeGroupService.FeeGroupDescription = tbDescription.Text;
                    //need to get data
                    if (hdnSave.Value == "Update")
                    {
                        //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                        if (Session["SelectedFeeGroupID"] != null)
                        {
                            FeeGroupService.FeeGroupID = Convert.ToInt64(Session["SelectedFeeGroupID"].ToString().Trim());
                        }
                    }
                    else
                    {
                        FeeGroupService.FeeGroupID = 0;
                    }
                    FeeGroupService.IsInActive = chkInactive.Checked;
                    FeeGroupService.IsDeleted = false;
                    return FeeGroupService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return FeeGroupService;
            }
        }

        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFeeGroupID"] != null)
                    {

                        strFeeGroupID = "";

                        foreach (GridDataItem Item in dgFeeGroup.MasterTableView.Items)
                        {
                            if (Item["FeeGroupID"].Text.Trim() == Session["SelectedFeeGroupID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("FeeGroupCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("FeeGroupCD").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("FeeGroupID") != null)
                                {
                                    strFeeGroupID = Item.GetDataKeyValue("FeeGroupID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("FeeGroupDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("FeeGroupDescription").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                break;
                            }
                        }
                        if (Request.QueryString["IsPopup"] != "Add")
                        {
                            lbColumnName1.Text = "Fee Schedule Group Code";
                            lbColumnName2.Text = "Description";
                            lbColumnValue1.Text = Microsoft.Security.Application.Encoder.HtmlEncode(tbCode.Text);
                            lbColumnValue2.Text = Microsoft.Security.Application.Encoder.HtmlEncode(tbDescription.Text);
                        }
                        EnableForm(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtninsertCtl, lbtndelCtl, lbtneditCtl;
                    lbtninsertCtl = (LinkButton)dgFeeGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    lbtndelCtl = (LinkButton)dgFeeGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    lbtneditCtl = (LinkButton)dgFeeGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddFeeGroup))
                    {
                        lbtninsertCtl.Visible = true;
                        if (add)
                        {
                            lbtninsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtninsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtninsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteFeeGroup))
                    {
                        lbtndelCtl.Visible = true;
                        if (delete)
                        {
                            lbtndelCtl.Enabled = true;
                            lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtndelCtl.Enabled = false;
                            lbtndelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtndelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditFeeGroup))
                    {
                        lbtneditCtl.Visible = true;
                        if (edit)
                        {
                            lbtneditCtl.Enabled = true;
                            lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtneditCtl.Enabled = false;
                            lbtneditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtneditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (FeeGroupPageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFeeGroup, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }

        /// <summary>
        /// To display the value in read only format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();

                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear  the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbDescription.Enabled = enable;
                    btnSaveChanges.Visible = enable;
                    btnCancel.Visible = enable;
                    chkInactive.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                tableFeeGroup.Visible = false;
                                dgFeeGroup.Visible = false;
                                if (IsAdd)
                                {
                                    (dgFeeGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgFeeGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFeeGroup.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFeeGroup.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();


                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFeeGroupID"] != null)
                    {
                        //TimeDisplay();
                        foreach (GridDataItem Item in dgFeeGroup.MasterTableView.Items)
                        {
                            if (Item["FeeGroupID"].Text.Trim() == Session["SelectedFeeGroupID"].ToString().Trim())
                            {
                                //GridDataItem Item = dgFeeGroup.SelectedItems[0] as GridDataItem;
                                if (Item.GetDataKeyValue("FeeGroupCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("FeeGroupCD").ToString();
                                }
                                if (Item.GetDataKeyValue("FeeGroupDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("FeeGroupDescription").ToString();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }

                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgFeeGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Fee Schedule Group Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["FeeGroupCD"].Text;
                                lbColumnValue2.Text = Item["FeeGroupDescription"].Text;

                                Item.Selected = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }
        #endregion

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.FeeGroup> lstFeeGroup = new List<FlightPakMasterService.FeeGroup>();
                if (Session["FeeGroupCodes"] != null)
                {
                    lstFeeGroup = (List<FlightPakMasterService.FeeGroup>)Session["FeeGroupCodes"];
                }
                if (lstFeeGroup.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstFeeGroup = lstFeeGroup.Where(x => x.IsInActive == false).ToList<FeeGroup>(); }
                    dgFeeGroup.DataSource = lstFeeGroup;
                    if (IsDataBind)
                    {
                        dgFeeGroup.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFeeGroup_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;

                //Check the formatting condition
                if (dataBoundItem["FeeGroupDescription"].Text.Count() > 40)
                {
                    string strDesc = dataBoundItem["FeeGroupeDescription"].Text;
                    dataBoundItem["FeeGroupDescription"].Text = strDesc.Substring(0, 65) + "...";
                    dataBoundItem["FeeGroupDescription"].ToolTip = strDesc;
                }
            }
        }
    }
}