﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Data;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetProfileInternationalData : BaseSecuredPage
    {
        public List<string> AircraftCodes = new List<string>();
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgEmergencyContact, dgEmergencyContact, this.Page.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                        //store the clientID of the grid to reference it later on the client
                        // RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgEmergencyContact.ClientID));
                        Table t = (Table)Form.FindControl("tblowner");
                        TableRow tr = (TableRow)Form.FindControl("trownedcompanyname");
                        if (!IsPostBack)
                        {
                            btnsave.Visible = false;
                            btnCancel.Visible = false;
                            hdnFPIId.Value = "0";
                            if (UserPrincipal.Identity._fpSettings._IsAPISSupport != null)
                            {
                                if (UserPrincipal.Identity._fpSettings._IsAPISSupport == true)
                                {
                                    lbDecalNo.CssClass = "important-text";
                                    lbColors.CssClass = "important-text";
                                    lbColors1.CssClass = "important-text";
                                    lbTrim.CssClass = "important-text";
                                    lbLastName.CssClass = "important-text";
                                    lbFirstName.CssClass = "important-text";
                                    lbMiddleName.ForeColor = System.Drawing.Color.Black;
                                    lbAddressLine1.CssClass = "important-text";
                                    lbAddressLine2.ForeColor = System.Drawing.Color.Black;
                                    lbAddressLine3.ForeColor = System.Drawing.Color.Black;
                                    lbCity.CssClass = "important-text";
                                    lbStateProv.CssClass = "important-text";
                                    lbCountry.CssClass = "important-text";
                                    lbPostal.CssClass = "important-text";
                                    lbHomePhone.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessPhone.CssClass = "important-text";
                                    lbOtherPhone.ForeColor = System.Drawing.Color.Black;
                                    lbHomeFax.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessFax.CssClass = "important-text";
                                    lbBusinessEmail.CssClass = "important-text";
                                    lbPersonalEmail.ForeColor = System.Drawing.Color.Black;
                                    lbownercompanyname.CssClass = "important-text";
                                    lboperatorcompanyname.CssClass = "important-text";
                                    lbLastName1.CssClass = "important-text";
                                    lbFirstName1.CssClass = "important-text";
                                    lbMiddleName1.ForeColor = System.Drawing.Color.Black;
                                    lbAddress1Line1.CssClass = "important-text";
                                    lbAddress1Line2.ForeColor = System.Drawing.Color.Black;
                                    lbAddress1Line3.ForeColor = System.Drawing.Color.Black;
                                    lbCity1.CssClass = "important-text";
                                    lbState11.CssClass = "important-text";
                                    lbCountry1.CssClass = "important-text";
                                    lbPostal1.CssClass = "important-text";
                                    lbHomePhone1.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessPhone1.CssClass = "important-text";
                                    lbOtherPhone1.ForeColor = System.Drawing.Color.Black;
                                    lbHomeFax1.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessFax1.CssClass = "important-text";
                                    lbBusinessEmail1.CssClass = "important-text";
                                    lbPersonalEmail1.ForeColor = System.Drawing.Color.Black;
                                }
                                else
                                {
                                    lbDecalNo.ForeColor = System.Drawing.Color.Black;
                                    lbColors.ForeColor = System.Drawing.Color.Black;
                                    lbColors1.ForeColor = System.Drawing.Color.Black;
                                    lbTrim.ForeColor = System.Drawing.Color.Black;
                                    lbLastName.ForeColor = System.Drawing.Color.Black;
                                    lbFirstName.ForeColor = System.Drawing.Color.Black;
                                    lbMiddleName.ForeColor = System.Drawing.Color.Black;
                                    lbAddressLine1.ForeColor = System.Drawing.Color.Black;
                                    lbAddressLine2.ForeColor = System.Drawing.Color.Black;
                                    lbAddressLine3.ForeColor = System.Drawing.Color.Black;
                                    lbCity.ForeColor = System.Drawing.Color.Black;
                                    lbStateProv.ForeColor = System.Drawing.Color.Black;
                                    lbCountry.ForeColor = System.Drawing.Color.Black;
                                    lbPostal.ForeColor = System.Drawing.Color.Black;
                                    lbHomePhone.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessPhone.ForeColor = System.Drawing.Color.Black;
                                    lbOtherPhone.ForeColor = System.Drawing.Color.Black;
                                    lbHomeFax.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessFax.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessEmail.ForeColor = System.Drawing.Color.Black;
                                    lbPersonalEmail.ForeColor = System.Drawing.Color.Black;
                                    lbownercompanyname.ForeColor = System.Drawing.Color.Black;
                                    lboperatorcompanyname.ForeColor = System.Drawing.Color.Black;
                                    lbLastName1.ForeColor = System.Drawing.Color.Black;
                                    lbFirstName1.ForeColor = System.Drawing.Color.Black;
                                    lbMiddleName1.ForeColor = System.Drawing.Color.Black;
                                    lbAddress1Line1.ForeColor = System.Drawing.Color.Black;
                                    lbAddress1Line2.ForeColor = System.Drawing.Color.Black;
                                    lbAddress1Line3.ForeColor = System.Drawing.Color.Black;
                                    lbCity1.ForeColor = System.Drawing.Color.Black;
                                    lbState11.ForeColor = System.Drawing.Color.Black;
                                    lbCountry1.ForeColor = System.Drawing.Color.Black;
                                    lbPostal1.ForeColor = System.Drawing.Color.Black;
                                    lbHomePhone1.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessPhone1.ForeColor = System.Drawing.Color.Black;
                                    lbOtherPhone1.ForeColor = System.Drawing.Color.Black;
                                    lbHomeFax1.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessFax1.ForeColor = System.Drawing.Color.Black;
                                    lbBusinessEmail1.ForeColor = System.Drawing.Color.Black;
                                    lbPersonalEmail1.ForeColor = System.Drawing.Color.Black;
                                }
                            }
                            else
                            {
                                lbDecalNo.ForeColor = System.Drawing.Color.Black;
                                lbColors.ForeColor = System.Drawing.Color.Black;
                                lbColors1.ForeColor = System.Drawing.Color.Black;
                                lbTrim.ForeColor = System.Drawing.Color.Black;
                                lbLastName.ForeColor = System.Drawing.Color.Black;
                                lbFirstName.ForeColor = System.Drawing.Color.Black;
                                lbMiddleName.ForeColor = System.Drawing.Color.Black;
                                lbAddressLine1.ForeColor = System.Drawing.Color.Black;
                                lbAddressLine2.ForeColor = System.Drawing.Color.Black;
                                lbAddressLine3.ForeColor = System.Drawing.Color.Black;
                                lbCity.ForeColor = System.Drawing.Color.Black;
                                lbStateProv.ForeColor = System.Drawing.Color.Black;
                                lbCountry.ForeColor = System.Drawing.Color.Black;
                                lbPostal.ForeColor = System.Drawing.Color.Black;
                                lbHomePhone.ForeColor = System.Drawing.Color.Black;
                                lbBusinessPhone.ForeColor = System.Drawing.Color.Black;
                                lbOtherPhone.ForeColor = System.Drawing.Color.Black;
                                lbHomeFax.ForeColor = System.Drawing.Color.Black;
                                lbBusinessFax.ForeColor = System.Drawing.Color.Black;
                                lbBusinessEmail.ForeColor = System.Drawing.Color.Black;
                                lbPersonalEmail.ForeColor = System.Drawing.Color.Black;
                                lbownercompanyname.ForeColor = System.Drawing.Color.Black;
                                lboperatorcompanyname.ForeColor = System.Drawing.Color.Black;
                                lbLastName1.ForeColor = System.Drawing.Color.Black;
                                lbFirstName1.ForeColor = System.Drawing.Color.Black;
                                lbMiddleName1.ForeColor = System.Drawing.Color.Black;
                                lbAddress1Line1.ForeColor = System.Drawing.Color.Black;
                                lbAddress1Line2.ForeColor = System.Drawing.Color.Black;
                                lbAddress1Line3.ForeColor = System.Drawing.Color.Black;
                                lbCity1.ForeColor = System.Drawing.Color.Black;
                                lbState11.ForeColor = System.Drawing.Color.Black;
                                lbCountry1.ForeColor = System.Drawing.Color.Black;
                                lbPostal1.ForeColor = System.Drawing.Color.Black;
                                lbHomePhone1.ForeColor = System.Drawing.Color.Black;
                                lbBusinessPhone1.ForeColor = System.Drawing.Color.Black;
                                lbOtherPhone1.ForeColor = System.Drawing.Color.Black;
                                lbHomeFax1.ForeColor = System.Drawing.Color.Black;
                                lbBusinessFax1.ForeColor = System.Drawing.Color.Black;
                                lbBusinessEmail1.ForeColor = System.Drawing.Color.Black;
                                lbPersonalEmail1.ForeColor = System.Drawing.Color.Black;
                            }
                            if (Request.QueryString["FleetID"] != null)
                            {
                                hdnFleetID.Value = (Request.QueryString["FleetID"].ToString());
                                if (Request.QueryString["TailNum"].ToString().Trim() != "")
                                {
                                    tbTailNumber.Text = Server.UrlDecode(Request.QueryString["TailNum"].ToString());
                                }
                                LoadData(Convert.ToInt64(hdnFleetID.Value.ToString()));
                                btnsave.Visible = true;
                                btnCancel.Visible = true;
                                tbTailNumber.Enabled = false;
                                //tbTailNumber.ReadOnly = true;
                                ContentPlaceHolder cph = (ContentPlaceHolder)Master.Master.FindControl("MainContent");
                                if (cph != null)
                                {
                                    //RadPanelBar rpb = (RadPanelBar)cph.FindControl("pnlNaviGation");
                                    //RadPanelItem selectedItem = rpb.FindItemByValue(selectedItemValue);
                                    //selectedItem.Selected = true;
                                    //RadPanelItem selectedParentItem = rpb.FindItemByValue("SA");
                                    //selectedParentItem.Expanded = true;
                                }
                                CheckAutorization(Permission.Database.ViewFleetProfileInternationalData);
                                if (hdnMode.Value.ToLower() == "edit")
                                {
                                    if (IsAuthorized(Permission.Database.EditFleetProfileInternationalData))
                                    {
                                        btnsave.Visible = true;
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.Lock(EntitySet.Database.FleetProfile, Convert.ToInt64(Server.UrlDecode(Request.QueryString["FleetID"].ToString()).Trim()));
                                            if (!returnValue.ReturnFlag)
                                            {
                                                //ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetProfile);                                                
                                                string radPageLoadAlertScript = "<script language='javascript'>function PageLoad_Alert(){ jAlert('" + returnValue.LockMessage + "', '" + ModuleNameConstants.Database.FleetProfile + "'); Sys.Application.remove_load(PageLoad_Alert);}; Sys.Application.add_load(PageLoad_Alert);</script>";
                                                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radPageLoadAlertScript);
                                                btnsave.Enabled = false;
                                                btnsave.CssClass = "button-disable";
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    btnsave.Visible = IsAuthorized(Permission.Database.AddFleetProfileInternationalData);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private void LoadData(Int64 FleetID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID))
            {
                hdnMode.Value = "";
                hdnFPIId.Value = "0";
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ObjRetVal = Service.GetFleetPair(FleetID).EntityList.FirstOrDefault();
                    if (ObjRetVal != null)
                    {
                        if (ObjRetVal.FleetProfileInternationalDataID != null && ObjRetVal.FleetProfileInternationalDataID.ToString().Trim() != "")
                            hdnFPIId.Value = ObjRetVal.FleetProfileInternationalDataID.ToString().Trim();

                        if (ObjRetVal.BuyAircraftAdditionalFeeDOM != null && ObjRetVal.BuyAircraftAdditionalFeeDOM.ToString().Trim() != "")
                            tbdecalnumber.Text = ObjRetVal.BuyAircraftAdditionalFeeDOM.ToString().Trim();
                        else
                            tbdecalnumber.Text = string.Empty;
                        if (ObjRetVal.BuyAircraftAdditionalFeeDOM2 != null && ObjRetVal.BuyAircraftAdditionalFeeDOM2.ToString().Trim() != "")
                            tbdecalnumber2.Text = ObjRetVal.BuyAircraftAdditionalFeeDOM2.ToString().Trim();
                        else
                            tbdecalnumber2.Text = string.Empty;

                        if (ObjRetVal.AircraftMake != null && ObjRetVal.AircraftMake.ToString().Trim() != "")
                            tbaircraftmake.Text = ObjRetVal.AircraftMake.ToString().Trim();
                        else
                            tbaircraftmake.Text = string.Empty;
                        if (ObjRetVal.AircraftModel != null && ObjRetVal.AircraftModel.ToString().Trim() != "")
                            tbaircraftmodel.Text = ObjRetVal.AircraftModel.ToString().Trim();
                        else
                            tbaircraftmodel.Text = string.Empty;
                        if (ObjRetVal.ManufactureYear != null && ObjRetVal.ManufactureYear.ToString().Trim() != "")
                            tbyear.Text = ObjRetVal.ManufactureYear.ToString().Trim();
                        if (tbyear.Text == "0")
                            tbyear.Text = "";
                        if (ObjRetVal.ManufactureYear != null && ObjRetVal.ManufactureYear.ToString().Trim() != "")
                            tbyear.Text = ObjRetVal.ManufactureYear.ToString().Trim();
                        else
                            tbyear.Text = string.Empty;
                        if (ObjRetVal.Year1 != null && ObjRetVal.Year1.ToString().Trim() != "")
                            tbYear1.Text = ObjRetVal.Year1.ToString().Trim();
                        else
                            tbYear1.Text = string.Empty;
                        if (ObjRetVal.Year2 != null && ObjRetVal.Year2.ToString().Trim() != "")
                            tbYear2.Text = ObjRetVal.Year2.ToString().Trim();
                        else
                            tbYear2.Text = string.Empty;
                        if (ObjRetVal.AircraftColor1 != null && ObjRetVal.AircraftColor1.ToString().Trim() != "")
                            tbcolors.Text = ObjRetVal.AircraftColor1.ToString().Trim();
                        else
                            tbcolors.Text = string.Empty;
                        if (ObjRetVal.AircraftColor2 != null && ObjRetVal.AircraftColor2.ToString().Trim() != "")
                            tbcolors1.Text = ObjRetVal.AircraftColor2.ToString().Trim();
                        else
                            tbcolors1.Text = string.Empty;
                        if (ObjRetVal.AircraftTrimColor != null && ObjRetVal.AircraftTrimColor.ToString().Trim() != "")
                            tbtrim.Text = ObjRetVal.AircraftTrimColor.ToString().Trim();
                        else
                            tbtrim.Text = string.Empty;

                        //Owner/Lessee Details
                        if (ObjRetVal.OwnerLesse != null && ObjRetVal.OwnerLesse.ToString().Trim() != "")
                        {
                            tbownercompanyname.Text = ObjRetVal.OwnerLesse.ToString().Trim();
                            chkownerownedbycompany.Checked = true;
                            trownedcompanyname.Visible = true;
                        }
                        else
                        {
                            tbownercompanyname.Text = string.Empty;
                            chkownerownedbycompany.Checked = false;
                            trownedcompanyname.Visible = false;
                        }
                        if (ObjRetVal.OLastName != null && ObjRetVal.OLastName.ToString().Trim() != "")
                            tblastname.Text = ObjRetVal.OLastName.ToString().Trim();
                        else
                            tblastname.Text = string.Empty;
                        if (ObjRetVal.OLastName != null && ObjRetVal.OLastName.ToString().Trim() != "")
                            tblastname.Text = ObjRetVal.OLastName.ToString().Trim();
                        else
                            tblastname.Text = string.Empty;
                        if (ObjRetVal.OFirstName != null && ObjRetVal.OFirstName.ToString().Trim() != "")
                            tbfirstname.Text = ObjRetVal.OFirstName.ToString().Trim();
                        else
                            tbfirstname.Text = string.Empty;
                        if (ObjRetVal.OFirstName != null && ObjRetVal.OFirstName.ToString().Trim() != "")
                            tbfirstname.Text = ObjRetVal.OFirstName.ToString().Trim();
                        else
                            tbfirstname.Text = string.Empty;
                        if (ObjRetVal.OMiddleName != null && ObjRetVal.OMiddleName.ToString().Trim() != "")
                            tbmiddlename.Text = ObjRetVal.PMiddlename.ToString().Trim();
                        else
                            tbmiddlename.Text = string.Empty;
                        if (ObjRetVal.OMiddleName != null && ObjRetVal.OMiddleName.ToString().Trim() != "")
                            tbmiddlename.Text = ObjRetVal.PMiddlename.ToString().Trim();
                        else
                            tbmiddlename.Text = string.Empty;

                        if (ObjRetVal.Addr1 != null && ObjRetVal.Addr1.ToString().Trim() != "")
                            tbownerAddr1.Text = ObjRetVal.Addr1.ToString().Trim();
                        else
                            tbownerAddr1.Text = string.Empty;
                        if (ObjRetVal.Addr2 != null && ObjRetVal.Addr2.ToString().Trim() != "")
                            tbownerAddr2.Text = ObjRetVal.Addr2.ToString().Trim();
                        else
                            tbownerAddr2.Text = string.Empty;
                        if (ObjRetVal.Addr3 != null)
                        {
                            tbownerAddr3.Text = ObjRetVal.Addr3;
                        }
                        else
                        {
                            tbownerAddr3.Text = string.Empty;
                        }
                        if (ObjRetVal.CityName != null && ObjRetVal.CityName.ToString().Trim() != "")
                            tbownercity1.Text = ObjRetVal.CityName.ToString().Trim();
                        else
                            tbownercity1.Text = string.Empty;
                        if (ObjRetVal.StateName != null && ObjRetVal.StateName.ToString().Trim() != "")
                            tbownerstate.Text = ObjRetVal.StateName.ToString().Trim();
                        else
                            tbownerstate.Text = string.Empty;
                        if (ObjRetVal.CountryID != null && ObjRetVal.CountryID.ToString().Trim() != "")
                        {
                            hdnCountry1.Value = ObjRetVal.CountryID.ToString().Trim();
                            tbownercountry.Text = ObjRetVal.CountryCD.ToString().Trim();
                        }
                        else
                        {
                            hdnCountry1.Value = "0";
                            tbownercountry.Text = string.Empty;
                        }
                        if (ObjRetVal.PostalZipCD != null && ObjRetVal.PostalZipCD.ToString().Trim() != "")
                            tbownerpostal1.Text = ObjRetVal.PostalZipCD.ToString().Trim();
                        else
                            tbownerpostal1.Text = string.Empty;
                        if (ObjRetVal.OPhoneNum != null && ObjRetVal.OPhoneNum.ToString().Trim() != "")
                            tbownerphone.Text = ObjRetVal.OPhoneNum.ToString().Trim();
                        else
                            tbownerphone.Text = string.Empty;
                        if (ObjRetVal.BusinessPhone != null)
                        {
                            tbOwnerBusinessphone.Text = ObjRetVal.BusinessPhone;
                        }
                        else
                        {
                            tbOwnerBusinessphone.Text = string.Empty;
                        }
                        if (ObjRetVal.OLOtherPhone != null)
                        {
                            tbOwnerOtherphone.Text = ObjRetVal.OLOtherPhone;
                        }
                        else
                        {
                            tbOwnerOtherphone.Text = string.Empty;
                        }
                        if (ObjRetVal.OLHomeFax != null)
                        {
                            tbOwnerHomeFax.Text = ObjRetVal.OLHomeFax;
                        }
                        else
                        {
                            tbOwnerHomeFax.Text = string.Empty;
                        }
                        if (ObjRetVal.OFaxNum != null && ObjRetVal.OFaxNum.ToString().Trim() != "")
                            tbownerfax1.Text = ObjRetVal.OFaxNum.ToString().Trim();
                        else
                            tbownerfax1.Text = string.Empty;
                        if (ObjRetVal.OLCellPhoneNum != null)
                        {
                            tbOwnerPrimaryMobile.Text = ObjRetVal.OLCellPhoneNum;
                        }
                        else
                        {
                            tbOwnerPrimaryMobile.Text = string.Empty;
                        }
                        if (ObjRetVal.OLCellPhoneNum2 != null)
                        {
                            tbOwnerSecondaryMobile.Text = ObjRetVal.OLCellPhoneNum2;
                        }
                        else
                        {
                            tbOwnerSecondaryMobile.Text = string.Empty;
                        }
                        if (ObjRetVal.OEmailAddress != null && ObjRetVal.OEmailAddress.ToString().Trim() != "")
                            tbowneremail1.Text = ObjRetVal.OEmailAddress.ToString().Trim();
                        else
                            tbowneremail1.Text = string.Empty;
                        if (ObjRetVal.OLPersonalEmail != null)
                        {
                            tbOwnerPersonalEmail.Text = ObjRetVal.OLPersonalEmail;
                        }
                        else
                        {
                            tbOwnerPersonalEmail.Text = string.Empty;
                        }
                        if (ObjRetVal.OLOtherEmail != null)
                        {
                            tbOwnerOthermail.Text = ObjRetVal.OLOtherEmail;
                        }
                        else
                        {
                            tbOwnerOthermail.Text = string.Empty;
                        }
                        //Operator Details
                        if (ObjRetVal.Company != null && ObjRetVal.Company.ToString().Trim() != "")
                        {
                            tboperatorcompanyname.Text = ObjRetVal.Company.ToString().Trim();
                            chkoperatorOwnedbyCompany.Checked = true;
                            troperatorcompanyname.Visible = true;
                        }
                        else
                        {
                            tboperatorcompanyname.Text = string.Empty;
                            chkoperatorOwnedbyCompany.Checked = false;
                            troperatorcompanyname.Visible = false;
                        }
                        if (ObjRetVal.PLastName != null && ObjRetVal.PLastName.ToString().Trim() != "")
                            tboperatorlastname.Text = ObjRetVal.PLastName.ToString().Trim();
                        else
                            tboperatorlastname.Text = string.Empty;
                        if (ObjRetVal.PFirstName != null && ObjRetVal.PFirstName.ToString().Trim() != "")
                            tboperatorfirstname.Text = ObjRetVal.PFirstName.ToString().Trim();
                        else
                            tboperatorfirstname.Text = string.Empty;
                        if (ObjRetVal.PMiddlename != null && ObjRetVal.PMiddlename.ToString().Trim() != "")
                            tboperatormiddlename.Text = ObjRetVal.PMiddlename.ToString().Trim();
                        else
                            tboperatormiddlename.Text = string.Empty;
                        if (ObjRetVal.Address1 != null && ObjRetVal.Address1.ToString().Trim() != "")
                            tboperatoraddr1.Text = ObjRetVal.Address1.ToString().Trim();
                        else
                            tboperatoraddr1.Text = string.Empty;
                        if (ObjRetVal.Address2 != null && ObjRetVal.Address2.ToString().Trim() != "")
                            tboperatoraddr2.Text = ObjRetVal.Address2.ToString().Trim();
                        else
                            tboperatoraddr2.Text = string.Empty;
                        if (ObjRetVal.Address3 != null)
                        {
                            tboperatoraddr3.Text = ObjRetVal.Address3;
                        }
                        else
                        {
                            tboperatoraddr3.Text = string.Empty;
                        }
                        if (ObjRetVal.OCityName != null && ObjRetVal.OCityName.ToString().Trim() != "")
                            tboperatorcity1.Text = ObjRetVal.OCityName.ToString().Trim();
                        else
                            tboperatorcity1.Text = string.Empty;
                        if (ObjRetVal.OStateName != null && ObjRetVal.OStateName.ToString().Trim() != "")
                            tboperatorstateprov.Text = ObjRetVal.OStateName.ToString().Trim();
                        else
                            tboperatorstateprov.Text = string.Empty;
                        if (ObjRetVal.OCountryID != null && ObjRetVal.OCountryID.ToString().Trim() != "")
                        {
                            hdnCountry2.Value = ObjRetVal.OCountryID.ToString();
                            tboperatorcountry.Text = ObjRetVal.OCountryCD.ToString().Trim();
                        }
                        else
                        {
                            hdnCountry2.Value = "0";
                            tboperatorcountry.Text = string.Empty;
                        }
                        if (ObjRetVal.ZipCD != null && ObjRetVal.ZipCD.ToString().Trim() != "")
                            tboperatorpostal.Text = ObjRetVal.ZipCD.ToString().Trim();
                        else
                            tboperatorpostal.Text = string.Empty;
                        if (ObjRetVal.PhoneNum != null && ObjRetVal.PhoneNum.ToString().Trim() != "")
                            tboperatorphone.Text = ObjRetVal.PhoneNum.ToString().Trim();
                        else
                            tboperatorphone.Text = string.Empty;
                        if (ObjRetVal.OPBusinessPhone2 != null)
                        {
                            tbOperatorBusinessPhone.Text = ObjRetVal.OPBusinessPhone2;
                        }
                        else
                        {
                            tbOperatorBusinessPhone.Text = string.Empty;
                        }
                        if (ObjRetVal.OPOtherPhone != null)
                        {
                            tbOperatorOtherPhone.Text = ObjRetVal.OPOtherPhone;
                        }
                        else
                        {
                            tbOperatorOtherPhone.Text = string.Empty;
                        }
                        if (ObjRetVal.OPHomeFax != null)
                        {
                            tbOperatorHomeFax.Text = ObjRetVal.OPHomeFax;
                        }
                        else
                        {
                            tbOperatorHomeFax.Text = string.Empty;
                        }
                        if (ObjRetVal.FaxNum != null && ObjRetVal.FaxNum.ToString().Trim() != "")
                            tboperatorfax1.Text = ObjRetVal.FaxNum.ToString().Trim();
                        else
                            tboperatorfax1.Text = string.Empty;
                        if (ObjRetVal.OPCellPhoneNum != null)
                        {
                            tbOperatorPrimaryMobile.Text = ObjRetVal.OPCellPhoneNum;
                        }
                        else
                        {
                            tbOperatorPrimaryMobile.Text = string.Empty;
                        }
                        if (ObjRetVal.OPCellPhoneNum2 != null)
                        {
                            tbOperatorSecondaryMobile.Text = ObjRetVal.OPCellPhoneNum2;
                        }
                        else
                        {
                            tbOperatorSecondaryMobile.Text = string.Empty;
                        }
                        if (ObjRetVal.EmailAddress != null && ObjRetVal.EmailAddress.ToString().Trim() != "")
                            tboperatoremail1.Text = ObjRetVal.EmailAddress.ToString().Trim();
                        else
                            tboperatoremail1.Text = string.Empty;
                        if (ObjRetVal.EmailAddress != null && ObjRetVal.EmailAddress.ToString().Trim() != "")
                            tboperatoremail1.Text = ObjRetVal.EmailAddress.ToString().Trim();
                        else
                            tboperatoremail1.Text = string.Empty;
                        if (ObjRetVal.OPPersonalEmail != null)
                        {
                            tbOperatorPersonalEmail.Text = ObjRetVal.OPPersonalEmail;
                        }
                        else
                        {
                            tbOperatorPersonalEmail.Text = string.Empty;
                        }
                        if (ObjRetVal.OPOtherEmail != null)
                        {
                            tbOperatorOtherEmail.Text = ObjRetVal.OPOtherEmail;
                        }
                        else
                        {
                            tbOperatorOtherEmail.Text = string.Empty;
                        }

                        if (tboperatorcompanyname.Text.Trim() != "")
                        {
                            chkoperatorOwnedbyCompany.Checked = true;
                        }
                        else
                        {
                            chkoperatorOwnedbyCompany.Checked = false;
                        }
                        if (chkoperatorOwnedbyCompany.Checked)
                        {
                            troperatorcompanyname.Visible = true;
                            TrAdd2.Visible = false;
                        }
                        else
                        {
                            troperatorcompanyname.Visible = false;
                            TrAdd2.Visible = true;
                        }
                        if (tbownercompanyname.Text.Trim() != "")
                        {
                            chkownerownedbycompany.Checked = true;
                        }
                        else
                        {
                            chkownerownedbycompany.Checked = false;
                        }
                        if (chkownerownedbycompany.Checked)
                        {
                            trownedcompanyname.Visible = true;
                            TrAdd1.Visible = false;
                        }
                        else
                        {
                            trownedcompanyname.Visible = false;
                            TrAdd1.Visible = true;
                        }
                        hdnMode.Value = "Edit";
                    }
                    btnsave.Visible = true;
                    btnCancel.Visible = true;
                }
            }
        }
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    bool IsValidate = true;
                    Page.Validate();
                    if (!Page.IsValid)
                    {
                        IsValidate = false;
                    }
                    if (!CheckYear())
                    {
                        cvYear.IsValid = false;
                        IsValidate = false;
                    }
                    if (!CheckCountryExists())
                    {
                        cvOCountry.IsValid = false;
                        tboperatorcountry.Focus();
                        IsValidate = false;
                    }
                    if (!CheckOCountryExists())
                    {
                        cvCountry.IsValid = false;
                        tbownercountry.Focus();
                        IsValidate = false;
                    }
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsValidate)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FleetPair FleetPairs = new FlightPakMasterService.FleetPair();
                                FleetPairs.FleetID = Convert.ToInt64(hdnFleetID.Value.ToString());
                                FleetPairs.BuyAircraftAdditionalFeeDOM = tbdecalnumber.Text.Trim().ToUpper();
                                FleetPairs.BuyAircraftAdditionalFeeDOM2 = tbdecalnumber2.Text.Trim().ToUpper();
                                FleetPairs.AircraftMake = tbaircraftmake.Text.Trim().ToUpper();
                                FleetPairs.AircraftModel = tbaircraftmodel.Text.Trim().ToUpper();
                                if (tbyear.Text.Trim() != "")
                                {
                                    if (Convert.ToInt32(tbyear.Text) > 0)
                                        FleetPairs.ManufactureYear = Convert.ToInt32(tbyear.Text);
                                }
                                if (tbYear1.Text.Trim() != "")
                                {
                                    if (Convert.ToInt32(tbYear1.Text) > 0)
                                        FleetPairs.Year1 = Convert.ToInt32(tbYear1.Text);
                                }
                                if (tbYear2.Text.Trim() != "")
                                {
                                    if (Convert.ToInt32(tbYear2.Text) > 0)
                                        FleetPairs.Year2 = Convert.ToInt32(tbYear2.Text);
                                }
                                FleetPairs.AircraftColor1 = tbcolors.Text.Trim().ToUpper();
                                FleetPairs.AircraftColor2 = tbcolors1.Text.Trim().ToUpper();
                                FleetPairs.AircraftTrimColor = tbtrim.Text.Trim().ToUpper();
                                //Owner/Lessee Details
                                FleetPairs.OwnerLesse = tbownercompanyname.Text;
                                FleetPairs.OLastName = tblastname.Text;
                                FleetPairs.OFirstName = tbfirstname.Text;
                                FleetPairs.OMiddleName = tbmiddlename.Text;
                                FleetPairs.Addr1 = tbownerAddr1.Text;
                                FleetPairs.Addr2 = tbownerAddr2.Text;
                                FleetPairs.Addr3 = tbownerAddr3.Text;
                                FleetPairs.CityName = tbownercity1.Text;
                                FleetPairs.StateName = tbownerstate.Text;
                                if (tbownercountry.Text.Trim() != "")
                                {
                                    if (hdnCountry1.Value.ToString().Trim() != "")
                                    {
                                        if (Convert.ToInt64(hdnCountry1.Value) > 0)
                                        {
                                            FleetPairs.CountryID = Convert.ToInt64(hdnCountry1.Value);
                                        }
                                    }
                                }
                                else
                                {
                                    FleetPairs.CountryID = null;
                                }
                                FleetPairs.PostalZipCD = tbownerpostal1.Text;
                                FleetPairs.OPhoneNum = tbownerphone.Text;
                                FleetPairs.BusinessPhone = tbOwnerBusinessphone.Text;
                                FleetPairs.OLOtherPhone = tbOwnerOtherphone.Text;
                                FleetPairs.OLHomeFax = tbOwnerHomeFax.Text;
                                FleetPairs.OFaxNum = tbownerfax1.Text;
                                FleetPairs.OLCellPhoneNum = tbOwnerPrimaryMobile.Text;
                                FleetPairs.OLCellPhoneNum2 = tbOwnerSecondaryMobile.Text;
                                FleetPairs.OEmailAddress = tbowneremail1.Text;
                                FleetPairs.OLPersonalEmail = tbOwnerPersonalEmail.Text;
                                FleetPairs.OLOtherEmail = tbOwnerOthermail.Text;
                                //Operator Details
                                FleetPairs.Company = tboperatorcompanyname.Text.Trim().ToUpper();
                                FleetPairs.PLastName = tboperatorlastname.Text;
                                FleetPairs.PFirstName = tboperatorfirstname.Text;
                                FleetPairs.PMiddlename = tboperatormiddlename.Text;
                                FleetPairs.Address1 = tboperatoraddr1.Text;
                                FleetPairs.Address2 = tboperatoraddr2.Text;
                                FleetPairs.Address3 = tboperatoraddr3.Text;
                                FleetPairs.OCityName = tboperatorcity1.Text;
                                FleetPairs.OStateName = tboperatorstateprov.Text;
                                if (tboperatorcountry.Text.Trim() != "")
                                {
                                    if (hdnCountry2.Value.ToString().Trim() != "")
                                    {
                                        if (Convert.ToInt64(hdnCountry2.Value) > 0)
                                        {
                                            FleetPairs.OCountryID = Convert.ToInt64(hdnCountry2.Value);
                                        }
                                    }
                                }
                                else
                                {
                                    FleetPairs.OCountryID = null;
                                }
                                FleetPairs.ZipCD = tboperatorpostal.Text;
                                FleetPairs.PhoneNum = tboperatorphone.Text;
                                FleetPairs.OPBusinessPhone2 = tbOperatorBusinessPhone.Text;
                                FleetPairs.OPOtherPhone = tbOperatorOtherPhone.Text;
                                FleetPairs.OPHomeFax = tbOperatorHomeFax.Text;
                                FleetPairs.FaxNum = tboperatorfax1.Text;
                                FleetPairs.OPCellPhoneNum = tbOperatorPrimaryMobile.Text;
                                FleetPairs.OPCellPhoneNum2 = tbOperatorSecondaryMobile.Text;
                                FleetPairs.EmailAddress = tboperatoremail1.Text;
                                FleetPairs.OPPersonalEmail = tbOperatorPersonalEmail.Text;
                                FleetPairs.OPOtherEmail = tbOperatorOtherEmail.Text;
                                ReturnValueOfFleetPair Result;
                                if (hdnMode.Value.ToLower() == "edit")
                                {
                                    FleetPairs.FleetProfileInternationalDataID = Convert.ToInt64(hdnFPIId.Value);
                                    Result = Service.UpdateFleetinternationaldata(FleetPairs);
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.FleetProfile, Convert.ToInt64(Server.UrlDecode(Request.QueryString["FleetID"].ToString()).Trim()));
                                    }
                                }
                                else
                                {
                                    Result = Service.AddFleetInternationalType(FleetPairs);
                                }
                                btnsave.Enabled = false;
                                btnsave.CssClass = "button-disable";
                                EnableForm(false);
                                if (Result.ReturnFlag == true)
                                {
                                    //ShowSuccessMessage();
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowSuccessMessage", "ShowSuccessMessage();", true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void ownedbycompany_checkedchanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tblastname.Text = "";
                        tbfirstname.Text = "";
                        tbmiddlename.Text = "";
                        tbownercompanyname.Text = "";
                        if (chkownerownedbycompany.Checked)
                        {
                            trownedcompanyname.Visible = true;
                            TrAdd1.Visible = false;
                        }
                        else
                        {
                            trownedcompanyname.Visible = false;
                            TrAdd1.Visible = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void operatorownedbycompany_checkchanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tboperatorcompanyname.Text = "";
                        tboperatorlastname.Text = "";
                        tboperatorfirstname.Text = "";
                        tboperatormiddlename.Text = "";
                        if (chkoperatorOwnedbyCompany.Checked)
                        {
                            troperatorcompanyname.Visible = true;
                            TrAdd2.Visible = false;
                        }
                        else
                        {
                            troperatorcompanyname.Visible = false;
                            TrAdd2.Visible = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        //protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        //{
        //    //if ((e.Initiator.ID.IndexOf("btnSaveChanges") > -1))
        //    //{
        //    //    e.Updated = dgEmergencyContact;
        //    //}
        //}
        //private Boolean checkAllReadyExist()
        //{
        //    bool returnVal = false;
        //    AircraftCodes = (List<string>)Session["AircraftCodes"];
        //    if (AircraftCodes != null && AircraftCodes.Contains(this tbCode.Text.ToString().Trim().ToLower()))
        //        return true;
        //    return returnVal;
        //}
        protected void Cancel_Click(object sender, EventArgs e)
        {
            //ClearForm();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect("FleetProfileCatalog.aspx?FleetID=" + hdnFleetID.Value.ToString().Trim());
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tboperatoraddr1.Text = string.Empty;
                tboperatoraddr2.Text = string.Empty;
                tbcolors.Text = string.Empty;
                tbcolors1.Text = string.Empty;
                tbaircraftmake.Text = string.Empty;
                tbaircraftmodel.Text = string.Empty;
                tbtrim.Text = string.Empty;
                tbownercity1.Text = string.Empty;
                tboperatoremail1.Text = string.Empty;
                tboperatorfax1.Text = string.Empty;
                tbyear.Text = string.Empty;
                tboperatorcity1.Text = string.Empty;
                tboperatoremail1.Text = string.Empty;
                tbcolors1.Text = string.Empty;
                tbfirstname.Text = string.Empty;
                tblastname.Text = string.Empty;
                tbmiddlename.Text = string.Empty;
                tboperatorfirstname.Text = string.Empty;
                tboperatorlastname.Text = string.Empty;
                tboperatormiddlename.Text = string.Empty;
                tbownerphone.Text = string.Empty;
                tboperatorphone.Text = string.Empty;
                tbownerstate.Text = string.Empty;
                tbownerfax1.Text = string.Empty;
                tbdecalnumber.Text = string.Empty;
                tbdecalnumber2.Text = string.Empty;
                tbYear1.Text = string.Empty;
                tbYear2.Text = string.Empty;
                tbownerAddr1.Text = string.Empty;
                tbownerAddr2.Text = string.Empty;
                tboperatorfax1.Text = string.Empty;
                tbownerstate.Text = string.Empty;
                tbowneremail1.Text = string.Empty;
                tboperatorstateprov.Text = string.Empty;
                tboperatorpostal.Text = string.Empty;
                //<new fields>
                tbOwnerBusinessphone.Text = string.Empty;
                tbOperatorPrimaryMobile.Text = string.Empty;
                tblastname.Text = string.Empty;
                tbOperatorHomeFax.Text = string.Empty;
                tbOperatorSecondaryMobile.Text = string.Empty;
                tbOperatorOtherPhone.Text = string.Empty;
                tbOperatorPersonalEmail.Text = string.Empty;
                tbOperatorOtherEmail.Text = string.Empty;
                tbownerAddr3.Text = string.Empty;
                tbOperatorBusinessPhone.Text = string.Empty;
                tbOwnerPrimaryMobile.Text = string.Empty;
                tbOwnerHomeFax.Text = string.Empty;
                tbOwnerSecondaryMobile.Text = string.Empty;
                tbOwnerOtherphone.Text = string.Empty;
                tbOwnerPersonalEmail.Text = string.Empty;
                tbOwnerOthermail.Text = string.Empty;
                tboperatoraddr3.Text = string.Empty;
                //<\new fields>
                tbownercountry.Text = "";
                tboperatorcountry.Text = "";
                chkoperatorOwnedbyCompany.Checked = false;
                chkownerownedbycompany.Checked = false;
                tbownerpostal1.Text = string.Empty;
                tboperatorcompanyname.Text = string.Empty;
                tbownercompanyname.Text = string.Empty;
            }
        }
        protected void tbownercountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbownercountry.Text != string.Empty)
                        {
                            CheckCountryExists();
                        }
                        else
                        {
                            hdnCountry1.Value = "0";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void tboperatorcountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckOCountryExists();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        private bool CheckCountryExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (tbownercountry.Text.Trim() != "")
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CountryVal = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.ToString().ToUpper().Trim().Equals(tbownercountry.Text.ToString().ToUpper().Trim()));
                        if (CountryVal.Count() > 0 && CountryVal != null)
                        {
                            foreach (FlightPakMasterService.Country Cntry in CountryVal)
                            {
                                tbownercountry.Text = Cntry.CountryCD.ToString().ToUpper().Trim();
                                hdnCountry1.Value = Cntry.CountryID.ToString();
                            }
                        }
                        else
                        {
                            cvCountry.IsValid = false;
                            tbownercountry.Text = "";
                            hdnCountry1.Value = "0";
                            tbownercountry.Focus();
                            return false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        private bool CheckOCountryExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (tboperatorcountry.Text.Trim() != "")
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CountryVal = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.ToString().ToUpper().Trim().Equals(tboperatorcountry.Text.ToString().ToUpper().Trim()));
                        if (CountryVal.Count() > 0 && CountryVal != null)
                        {
                            foreach (FlightPakMasterService.Country Cntry in CountryVal)
                            {
                                tboperatorcountry.Text = Cntry.CountryCD.ToString().ToUpper().Trim();
                                hdnCountry2.Value = Cntry.CountryID.ToString();
                            }
                        }
                        else
                        {
                            cvOCountry.IsValid = false;
                            tboperatorcountry.Text = "";
                            hdnCountry2.Value = "0";
                            tboperatorcountry.Focus();
                            return false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        private void EnableForm(bool enable)
        {
            tboperatoraddr1.Enabled = enable;
            tboperatoraddr2.Enabled = enable;
            tbcolors.Enabled = enable;
            tbcolors1.Enabled = enable;
            tbaircraftmake.Enabled = enable;
            tbaircraftmodel.Enabled = enable;
            tbtrim.Enabled = enable;
            tbownercity1.Enabled = enable;
            tboperatoremail1.Enabled = enable;
            tboperatorfax1.Enabled = enable;
            tbyear.Enabled = enable;
            tboperatorcity1.Enabled = enable;
            tboperatoremail1.Enabled = enable;
            tbcolors1.Enabled = enable;
            tbfirstname.Enabled = enable;
            tblastname.Enabled = enable;
            tbmiddlename.Enabled = enable;
            tboperatorfirstname.Enabled = enable;
            tboperatorlastname.Enabled = enable;
            tboperatormiddlename.Enabled = enable;
            tbownerphone.Enabled = enable;
            tboperatorphone.Enabled = enable;
            tbownerstate.Enabled = enable;
            tbownerfax1.Enabled = enable;
            tbdecalnumber.Enabled = enable;
            tbownerAddr1.Enabled = enable;
            tbownerAddr2.Enabled = enable;
            tboperatorfax1.Enabled = enable;
            tbownerstate.Enabled = enable;
            tbowneremail1.Enabled = enable;
            tboperatorstateprov.Enabled = enable;
            tboperatorpostal.Enabled = enable;
            tbOwnerBusinessphone.Enabled = enable;
            tbOperatorPrimaryMobile.Enabled = enable;
            tblastname.Enabled = enable;
            tbOperatorHomeFax.Enabled = enable;
            tbOperatorSecondaryMobile.Enabled = enable;
            tbOperatorOtherPhone.Enabled = enable;
            tbOperatorPersonalEmail.Enabled = enable;
            tbOperatorOtherEmail.Enabled = enable;
            tbownerAddr3.Enabled = enable;
            tbOperatorBusinessPhone.Enabled = enable;
            tbOwnerPrimaryMobile.Enabled = enable;
            tbOwnerHomeFax.Enabled = enable;
            tbOwnerSecondaryMobile.Enabled = enable;
            tbOwnerOtherphone.Enabled = enable;
            tbOwnerPersonalEmail.Enabled = enable;
            tbOwnerOthermail.Enabled = enable;
            tboperatoraddr3.Enabled = enable;
            tbownercountry.Enabled = enable;
            tboperatorcountry.Enabled = enable;
            chkoperatorOwnedbyCompany.Checked = enable;
            chkownerownedbycompany.Checked = enable;
            tbownerpostal1.Enabled = enable;
            tboperatorcompanyname.Enabled = enable;
            tbownercompanyname.Enabled = enable;
            chkoperatorOwnedbyCompany.Enabled = enable;
            chkownerownedbycompany.Enabled = enable;
            tbdecalnumber2.Enabled = enable;
            tbYear1.Enabled = enable;
            tbYear2.Enabled = enable;
        }
        protected void Year_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbyear.Text.Trim() != "")
                        {
                            CheckYear();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetForeCast);
                }
            }
        }
        private bool CheckYear()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = true;
                if (tbyear.Text.ToString().Trim() != "")
                {
                    Int32 FcYear = Convert.ToInt32(tbyear.Text.ToString());
                    if (FcYear < 1900 || FcYear > 2100)
                    {
                        cvYear.IsValid = false;
                        tbyear.Focus();
                        returnVal = false;
                    }
                    else
                    {
                        cvYear.IsValid = true;
                    }
                }
                return returnVal;
            }
        }
    }
}