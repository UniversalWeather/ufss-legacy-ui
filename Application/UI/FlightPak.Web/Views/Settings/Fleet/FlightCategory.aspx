﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="FlightCategory.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.FlightCategory"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">

        function openReport() {

            url = "../../Reports/ExportReportInformation.aspx?Report=RptDBFlightCategoryExport&UserCD=UC";
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open(url, "RadExportData");
        }
        
        function CheckTxtBox(control) {
            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;
            //            if ((document.getElementById('<%=rfvCode.ClientID%>').value != "") || (document.getElementById('<%=cvCode.ClientID%>').value != "")) {          
            //                return false;
            //            }
            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);

                } return false;
            }

            if (txtDesc == "") {

                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);

                } return false;
            }
        }

        function ShowReports(radWin, ReportFormat, ReportName) {
            document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
            document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
            if (ReportFormat == "EXPORT") {
                url = "../../Reports/ExportReportInformation.aspx";
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, 'RadExportData');
                return true;
            }
            else {
                return true;
            }
        }
        function OnClientCloseExportReport(oWnd, args) {
            document.getElementById("<%=btnShowReports.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table style="width: 100%;" cellpadding="0" cellspacing="0" runat="server" id="table1">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Flight Categories</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBFlightCategory');"
                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBFlightCategoryExport');return false;"
                            OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                        <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                            Style="display: none;" />
                        <a href="../../Help/ViewHelp.aspx?Screen=FlightCategoriesHelp" class="help-icon"
                            target="_blank" title="Help"></a>
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                    </span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0" runat="server" id="table3">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openWin() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCode.ClientID%>").value, "RadWindow1");

            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event                
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }


            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = htmlDecode(arg.ClientCD);
                        document.getElementById("<%=hdnclientCD.ClientID%>").value = arg.ClientID;
                        document.getElementById("<%=cvClientCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                    }
                }

            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                //alert(new String("Width:" + bounds.width + " " + "Height: " + bounds.height));
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function forecolorChange(sender, args) {

                $get("<%=tbForeColor.ClientID%>").value = sender.get_selectedColor();
                $get("<%=hdnForeColor.ClientID%>").value = sender.get_selectedColor();

            }
            function backcolorChange(sender, args) {

                $get("<%=tbBackColor.ClientID%>").value = sender.get_selectedColor();
                $get("<%=hdnBackColor.ClientID%>").value = sender.get_selectedColor();

            }

            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgFlightCategories" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgFlightCategories" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFlightCategories">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgFlightCategories" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="tbDescription" />
                    <telerik:AjaxUpdatedControl ControlID="cvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbClientCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbClientCode" />
                    <telerik:AjaxUpdatedControl ControlID="tbForeColor" />
                    <telerik:AjaxUpdatedControl ControlID="cvClientCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkSearchActiveOnly">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgFlightCategories" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="CustomerCatalog.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" AutoSize="false"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table cellpadding="0" cellspacing="0" class="head-sub-menu" runat="server" id="table2">
        <tr>
            <td>
                <div class="status-list">
                    <span>
                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Display Inactive" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                            AutoPostBack="true" /></span>
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="dgFlightCategories" runat="server" AllowSorting="true" OnItemCreated="dgFlightCategories_ItemCreated"
        Visible="true" OnNeedDataSource="dgFlightCategories_BindData" OnItemCommand="dgFlightCategories_ItemCommand"
        OnUpdateCommand="dgFlightCategories_UpdateCommand" OnInsertCommand="dgFlightCategories_InsertCommand"
        OnDeleteCommand="dgFlightCategories_DeleteCommand" AutoGenerateColumns="false"
        OnItemDataBound="dgFlightCategories_ItemDataBound" PageSize="10" AllowPaging="true"
        OnSelectedIndexChanged="dgFlightCategories_SelectedIndexChanged" onRowDataBound="dgFlightCategories_RowDataBound"
        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPageIndexChanged="dgFlightCategories_PageIndexChanged"
        OnPreRender="dgFlightCategories_PreRender" Height="341px">
        <MasterTableView DataKeyNames="FlightCategoryID,FlightCatagoryCD,ClientCD,FlightCatagoryDescription,ClientID,ForeGrndCustomColor,BackgroundCustomColor,IsInActive,LastUpdUID,LastUpdTS,CallKey,IsDeadorFerryHead"
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="FlightCatagoryCD" HeaderText="Flight Category Code"
                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                    HeaderStyle-Width="120px" FilterControlWidth="100px" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FlightCategoryID" HeaderText="FlightCategoryID"
                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                    Display="false" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FlightCatagoryDescription" HeaderText="Description"
                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                    HeaderStyle-Width="540px" FilterControlWidth="520px" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" AutoPostBackOnFilter="true"
                    AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                    HeaderStyle-Width="100px">
                </telerik:GridCheckBoxColumn>
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                        Visible='<%# IsAuthorized(Permission.Database.AddFlightCategory)%>'><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkInitEdit" runat="server" Enabled="false" OnClientClick="javascript:return ProcessUpdate();"
                        ToolTip="Edit" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditFlightCategory)%>'><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkDelete" runat="server" Enabled="false" OnClientClick="javascript:return ProcessDelete();"
                        CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeleteFlightCategory)%>'><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                </div>
                <div>
                    <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
    </telerik:RadGrid>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel100" valign="top">
                                    <asp:CheckBox ID="chkInactive" runat="server" Text="Inactive" Font-Size="12px" />
                                </td>
                                <td valign="top">
                                    <asp:CheckBox ID="chkDeadferryhead" runat="server" Text="Deadhead/Ferry" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top" class="tdLabel130">
                                    <span class="mnd_text">Flight Category Code</span>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCode" runat="server" MaxLength="4" CssClass="text50" ValidationGroup="save"
                                                    OnTextChanged="tbCode_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvCode" runat="server" ErrorMessage="Unique Flight Category Code is Required"
                                                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbCode" CssClass="alert-text"
                                                    ValidationGroup="save"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="save" ControlToValidate="tbCode"
                                                    Display="Dynamic" CssClass="alert-text" SetFocusOnError="true" ErrorMessage="Flight Category Code is Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" runat="server" MaxLength="25" CssClass="text200"
                                                    ValidationGroup="save"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                    ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    Client Code
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="tdtext150">
                                                <asp:TextBox ID="tbClientCode" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                    CssClass="text50" ValidationGroup="save" onBlur="return RemoveSpecialChars(this)"
                                                    OnTextChanged="tbClientCode_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin();return false;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvClientCode" runat="server" ErrorMessage="Invalid Client Code."
                                                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbClientCode" CssClass="alert-text"
                                                    ValidationGroup="save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    <span class="mnd_text">Set Calendar Colors</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    Foreground Color
                                </td>
                                <td class="tdLabel72">
                                    <asp:TextBox ID="tbForeColor" runat="server" MaxLength="25" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                        Text="HEXVALUE" CssClass="text60" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <telerik:RadColorPicker runat="server" ID="RadColorPickerForeColor" OnClientColorChange="forecolorChange"
                                        ShowIcon="true" ZIndex="90000" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    Background Color
                                </td>
                                <td class="tdLabel72">
                                    <asp:TextBox ID="tbBackColor" runat="server" MaxLength="25" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                        ReadOnly="true" Text="HEXVALUE" CssClass="text60"></asp:TextBox>
                                </td>
                                <td align="left" style="z-index: 999;">
                                    <telerik:RadColorPicker runat="server" ID="RadColorPickerBackColor" OnClientColorChange="backcolorChange"
                                        ShowIcon="true" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="tblButtonArea">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            CssClass="button" ValidationGroup="save" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click" CssClass="button"
                            CausesValidation="false" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnForeColor" runat="server" />
                        <asp:HiddenField ID="hdnBackColor" runat="server" />
                        <asp:HiddenField ID="hdnclientCD" runat="server" />
                        <asp:HiddenField ID="hdnGridEnable" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
