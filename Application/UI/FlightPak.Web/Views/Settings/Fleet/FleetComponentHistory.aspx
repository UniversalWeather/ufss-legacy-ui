﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Settings.master"
    ClientIDMode="AutoID" CodeBehind="FleetComponentHistory.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.FleetComponentHistory"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script runat="server">
       
        void QueryStringButton_Click(object sender, EventArgs e)
        {
            string URL = "../Fleet/FleetProfileCatalog.aspx?FleetID=" + hdnFleetID.Value + "&Screen=FleetProfile";
            Response.Redirect(URL);
        }


    </script>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">
                        <asp:LinkButton ID="LinkButton1" Text="Fleet Profile " runat="server" OnClick="QueryStringButton_Click"></asp:LinkButton>&nbsp;>
                        Fleet Component History</span> <span class="tab-nav-icons">
                            <%--<a href="#" class="search-icon">
                        </a><a href="#" class="save-icon"></a><a href="#" class="print-icon"></a>--%><a href="#"
                            title="Help" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" class="sticky">
        <tr>
            <td class="tdLabel100">
                Tail No.
            </td>
            <td>
                <asp:TextBox ID="tbTailNumber" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgFComp" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFComp">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgFComp" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="none" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="none" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadGrid ID="dgFComp" runat="server" OnItemCreated="dgFComp_ItemCreated" OnPageIndexChanged="dgFComp_PageIndexChanged"
        Visible="true" OnNeedDataSource="dgFComp_BindData" OnItemCommand="dgFComp_ItemCommand"
        OnUpdateCommand="dgFComp_UpdateCommand" OnInsertCommand="dgFComp_InsertCommand1"
        OnItemDataBound="dgFComp_ItemDataBound" OnDeleteCommand="dgFComp_DeleteCommand"
        OnSelectedIndexChanged="dgFComp_SelectedIndexChanged" Height="341px" PageSize="10"
        AllowPaging="true" OnPreRender="dgFComp_PreRender">
        <MasterTableView DataKeyNames="FleetComponentID,FleetID,FleetComponentDescription,LastInspectionDT,IsHrsDaysCycles,InspectionHrs,WarningHrs,SericalNum,ComponentCD,LastUpdUID,LastUpdTS"
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn UniqueName="FleetComponentID" HeaderText="FleetComponent ID"
                    DataField="FleetComponentID" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FleetComponentDescription" HeaderText="Component Description"
                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                    HeaderStyle-Width="420px" FilterControlWidth="400px" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastInspectionDT" HeaderText="Last Inspection"
                    AllowFiltering="false" AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                    HeaderStyle-Width="80px" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IsHrsDaysCycles" HeaderText="H/D/C" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="60px"
                    FilterControlWidth="40px" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InspectionHrs" HeaderText="Next Inspection" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="EqualTo" HeaderStyle-Width="100px"
                    FilterControlWidth="80px" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="WarningHrs" HeaderText="Warning" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="EqualTo" HeaderStyle-Width="100px"
                    FilterControlWidth="80px" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ComponentCD" HeaderText="ComponentCD" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="Contains" Display="false" FilterDelay="500">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="FleetID" HeaderText="Fleet ID" DataField="FleetID"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="SericalNum" HeaderText="SericalNum" DataField="SericalNum"
                    Display="false">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                        Visible='<%# IsAuthorized(Permission.Database.AddFleetComponentHistory)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        Visible='<%# IsAuthorized(Permission.Database.EditFleetComponentHistory)%>' ToolTip="Edit"
                        CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                        Visible='<%# IsAuthorized(Permission.Database.DeleteFleetComponentHistory)%>'
                        runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                </div>
                <div style="padding: 5px 5px; float: right;">
                    <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false"></GroupingSettings>
    </telerik:RadGrid>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    <asp:Label ID="lbcomponent" runat="server" CssClass="mnd_text" Text="Component Description"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbcomponent" CssClass="text240" runat="server" ValidationGroup="Save"
                                        MaxLength="30"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvcomponent" runat="server" ControlToValidate="tbcomponent"
                                        ValidationGroup="Save" ErrorMessage="Description is Required" Display="Dynamic"
                                        CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    <asp:Label ID="lblastinspection" runat="server" Text=" Last Inspection"></asp:Label>
                                </td>
                                <td>
                                    <uc:DatePicker ID="ucDate" runat="server"></uc:DatePicker>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    <asp:Label ID="lbnextinspection" runat="server" Text="Next Inspection"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbnextinspection" runat="server" CssClass="text80" MaxLength="6"
                                        onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    <asp:Label ID="lbwarning" runat="server" Text="Warning"></asp:Label>
                                </td>
                                <td class="tdLabel100">
                                    <asp:TextBox ID="tbwarning" runat="server" CssClass="text80" MaxLength="6" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RadioButton ID="btnhours" Text="Hours" runat="server" GroupName="HrsDaysCycles" />
                                    <asp:RadioButton ID="btndays" Text="Days" runat="server" GroupName="HrsDaysCycles" />
                                    <asp:RadioButton ID="btncycles" Text="Cycles" runat="server" GroupName="HrsDaysCycles" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    <asp:Label ID="lbserialnumber" runat="server" CssClass="mnd_text" Text="Serial No."></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbserialnumber" CssClass="text250" runat="server" ValidationGroup="Save"
                                        AutoPostBack="true" MaxLength="30"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="revserialnumber" runat="server" ControlToValidate="tbserialnumber"
                                        ValidationGroup="Save" ErrorMessage="Serial Number is Required" Display="Dynamic"
                                        CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td align="right">
                        <asp:HiddenField ID="hdnFleetID" runat="server" />
                        <asp:HiddenField ID="hdnFleetComponentID" runat="server" />
                        <asp:Button ID="btnSaveChanges" Text="Save" OnClick="SaveChanges_Click" ValidationGroup="Save"
                            runat="server" CssClass="button" />
                        <asp:Button ID="btnCancel" Text="Cancel" OnClick="Cancel_Click" CausesValidation="false"
                            runat="server" CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
