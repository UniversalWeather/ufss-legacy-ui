﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Data;

//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetGroupPopUp : BaseSecuredPage
    {
        private string fleetgroupCode;
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["FleetGroupCD"] != null)
            {
                if (Request.QueryString["FleetGroupCD"].ToString() == "1")
                {
                    dgFleetGroup.AllowMultiRowSelection = true;
                }
                else
                {
                    dgFleetGroup.AllowMultiRowSelection = false;
                }
            }
            else
            {
                dgFleetGroup.AllowMultiRowSelection = false;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["FleetGroupCD"]))
                        {
                            string fleetgroupCD = Request.QueryString["FleetGroupCD"].ToUpper().Trim();

                            if (!string.IsNullOrEmpty(fleetgroupCD))
                            {

                                dgFleetGroup.Rebind();
                                foreach (GridDataItem item in dgFleetGroup.MasterTableView.Items)
                                {
                                    if (item["FleetGroupCD"].Text.Trim() == fleetgroupCD)
                                    {
                                        item.Selected = true;
                                        break;
                                    }
                                }
                            }
                            if (dgFleetGroup.Items.Count > 0)
                            {
                                dgFleetGroup.SelectedIndexes.Add(0);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        /// <summary>
        /// Bind Fleet Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgFleetGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient();
                        List<FlightPakMasterService.GetAllFleetGroup> lstFleetGroup = new List<FlightPakMasterService.GetAllFleetGroup>();
                        var ObjRetVal = ObjService.GetFleetGroup();
                        if (ObjRetVal.ReturnFlag == true)
                        {
                           lstFleetGroup = ObjRetVal.EntityList;
                        }
                        dgFleetGroup.DataSource = lstFleetGroup;
                        Session["FleetGroupPopup"] = lstFleetGroup;
                        if (chkSearchActiveOnly.Checked == true)
                        {
                            SearchAndFilter(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetGroup_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["FleetGroupCD"] != null)
                {
                    if (Request.QueryString["FleetGroupCD"].ToString() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }

        protected void dgFleetGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case RadGrid.InitInsertCommandName:
                    e.Canceled = true;
                    InsertSelectedRow();
                    break;
                case RadGrid.FilterCommandName:
		            Pair filterPair = (Pair)e.CommandArgument;
		            Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFleetGroup_InsertCommand(object source, GridCommandEventArgs e)
        {
            InsertSelectedRow();
        }

        protected void dgFleetGroup_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFleetGroup, Page.Session);
        }

        private void InsertSelectedRow()
        {
            if (dgFleetGroup.SelectedItems.Count > 0)
            {
                GridDataItem Item = (GridDataItem)dgFleetGroup.SelectedItems[0];

                DataTable dtSelectedCrew = new DataTable();
                DataRow oItem;
                dtSelectedCrew.Columns.Add("FleetGroupID");
                dtSelectedCrew.Columns.Add("FleetGroupCD");
                foreach (GridDataItem dataItem in dgFleetGroup.SelectedItems)
                {
                    if (dataItem.Selected)
                    {

                        oItem = dtSelectedCrew.NewRow();
                        oItem[0] = dataItem.GetDataKeyValue("FleetGroupID").ToString();
                        if (dataItem.GetDataKeyValue("FleetGroupCD") != null)
                            oItem[1] = dataItem.GetDataKeyValue("FleetGroupID").ToString();
                        else
                            oItem[1] = string.Empty;

                        dtSelectedCrew.Rows.Add(oItem);
                    }
                }
                Session["SelectedCrews"] = dtSelectedCrew;
                InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgFleetGroup;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(fleetgroupCode))
                {
                    foreach (GridDataItem item in dgFleetGroup.MasterTableView.Items)
                    {
                        if (item["FleetGroupCD"].Text.Trim().ToUpper() == fleetgroupCode)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgFleetGroup.Items.Count > 0)
                    {
                        dgFleetGroup.SelectedIndexes.Add(0);
                    }
                }

            }

        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllFleetGroup> lstFleetGroup = new List<FlightPakMasterService.GetAllFleetGroup>();
                if (Session["FleetGroupPopup"] != null)
                {
                    lstFleetGroup = (List<FlightPakMasterService.GetAllFleetGroup>)Session["FleetGroupPopup"];
                }
                if (lstFleetGroup.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstFleetGroup = lstFleetGroup.Where(x => x.IsInactive == false).ToList<GetAllFleetGroup>(); }
                    dgFleetGroup.DataSource = lstFleetGroup;
                    if (IsDataBind)
                    {
                        dgFleetGroup.DataBind();
                    }
                }
                return false;
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            SearchAndFilter(true);
        }
        #endregion
    }
}