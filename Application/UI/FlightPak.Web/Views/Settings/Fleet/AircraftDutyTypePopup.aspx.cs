﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class AircraftDutyTypePopup : BaseSecuredPage
    {
        private string AircraftDutyCD;
        private ExceptionManager exManager;
        public bool showCommandItems = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        showCommandItems = true;
                        if (Request.QueryString["AircraftDutyCD"] != null)
                        {
                            if (Request.QueryString["AircraftDutyCD"].ToString().Trim() == "1")
                            {
                                dgAircraftDutyTypes.AllowMultiRowSelection = true;
                            }
                            else
                            {
                                dgAircraftDutyTypes.AllowMultiRowSelection = false;
                            }
                        }
                        else
                        {
                            dgAircraftDutyTypes.AllowMultiRowSelection = false;
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["AircraftDutyCD"]))
                        {
                            AircraftDutyCD = Request.QueryString["AircraftDutyCD"].ToUpper().Trim();
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["fromPage"]) && Request.QueryString["fromPage"] == "SystemTools")
                        {
                            showCommandItems = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        /// <summary>
        /// Bind Metro Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgAircraftDutyTypes_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var ObjRetVal = ObjService.GetAircraftDuty();
                List<FlightPakMasterService.AircraftDuty> AircraftDutyList = new List<AircraftDuty>();
                if (ObjRetVal.ReturnFlag == true)
                {                    
                    AircraftDutyList = ObjRetVal.EntityList;
                }
                dgAircraftDutyTypes.DataSource = AircraftDutyList;
                Session["DutyCodes"] = AircraftDutyList; 
                
                if (chkSearchActiveOnly.Checked == true)
                {
                    SearchAndFilter(false);
                }
            }
        }

        protected void dgAircraftDutyTypes_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                dataItem.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ForeGrndCustomColor"), CultureInfo.CurrentCulture));
                dataItem.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackgroundCustomColor"), CultureInfo.CurrentCulture));
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            e.Updated = dgAircraftDutyTypes;
            SelectItem();
        }
        private void SelectItem()
        {

            if (!string.IsNullOrEmpty(AircraftDutyCD))
            {

                foreach (GridDataItem item in dgAircraftDutyTypes.MasterTableView.Items)
                {
                    if (item["AircraftDutyCD"].Text.Trim().ToUpper() == AircraftDutyCD)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (dgAircraftDutyTypes.Items.Count > 0)
                {
                    dgAircraftDutyTypes.SelectedIndexes.Add(0);
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.AircraftDuty> lstAircraftDuty = new List<FlightPakMasterService.AircraftDuty>();
                if (Session["DutyCodes"] != null)
                {
                    lstAircraftDuty = (List<FlightPakMasterService.AircraftDuty>)Session["DutyCodes"];
                }
                if (lstAircraftDuty.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstAircraftDuty = lstAircraftDuty.Where(x => x.IsInActive == false).ToList<AircraftDuty>(); }
                    dgAircraftDutyTypes.DataSource = lstAircraftDuty;
                    if (IsDataBind)
                    {
                        dgAircraftDutyTypes.DataBind();
                    }
                }
                return false;
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            SearchAndFilter(true);
        }
        #endregion

        protected void dgAircraftDutyTypes_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    string resolvedurl = string.Empty;
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                SelectItem();
                                GridDataItem item = dgAircraftDutyTypes.SelectedItems[0] as GridDataItem;
                                Session["AircraftDutyID"] = item["AircraftDutyID"].Text;
                                TryResolveUrl("/Views/Settings/Fleet/AircraftDutyType.aspx?IsPopup=", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "ProcessPopupAdd('AccountsCatalog.aspx?IsPopup=', 'Add Accounts', '1000','500', '50', '50');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdDutyCodeCRUDPopup');", true);
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/Fleet/AircraftDutyType.aspx?IsPopup=Add", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdDutyCodeCRUDPopup');", true);
                                break;
                            case RadGrid.FilterCommandName:
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
		                        Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Account);
                }
            }
        }

        protected void dgAircraftDutyTypes_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgAircraftDutyTypes, Page.Session);
        }

        protected void dgAircraftDutyTypes_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    String AircraftDutyID = string.Empty;;
                    try
                    {

                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.AircraftDuty AircraftDuty = new FlightPakMasterService.AircraftDuty();
                                GridDataItem item = dgAircraftDutyTypes.SelectedItems[0] as GridDataItem;
                                AircraftDutyID = item.GetDataKeyValue("AircraftDutyID").ToString();
                                //Lock the record
                                var returnValue = CommonService.Lock(EntitySet.Database.AircraftDuty, Convert.ToInt64(item.GetDataKeyValue("AircraftDutyID").ToString()));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.AircraftDuty);
                                    return;
                                }
                                AircraftDuty.AircraftDutyID = Convert.ToInt64(item.GetDataKeyValue("AircraftDutyID").ToString());
                                AircraftDuty.AircraftDutyCD = item.GetDataKeyValue("AircraftDutyCD").ToString();
                                AircraftDuty.IsDeleted = true;
                                AircraftDuty.IsInActive = false;
                                Service.DeleteAircraftDuty(AircraftDuty);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                SelectItem();

                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.AircraftDuty);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.AircraftDuty, Convert.ToInt64(AircraftDutyID));

                    }
                }
            }
        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftDutyTypes_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["AircraftDutyCD"] != null)
                {
                    if (Request.QueryString["AircraftDutyCD"].ToString().Trim() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }
    }
}