﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Settings.master"
    CodeBehind="FleetCharterRates.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.FleetCharterRates" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            function forecolorChange(sender, args) {
                $get('tbForeColor').value = sender.get_selectedColor();
            }
            function backcolorChange(sender, args) {
                $get('tbBackColor').value = sender.get_selectedColor();
            }

        </script>
        <script runat="server">
       
            void QueryStringButton_Click(object sender, EventArgs e)
            {
                string URL = "../Fleet/FleetProfileCatalog.aspx?="; Response.Redirect(URL);
            }

      
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table style="width: 100%;">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fleet Charter Rates</span> <span class="tab-nav-icons"><%--<a
                        href="#" class="search-icon"></a><a href="#" class="save-icon"></a><a href="#" class="print-icon">
                        </a>--%><a href="#" title="Help" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="head-sub-menu">
        <tr>
            <td>
                TailNo.:
                <asp:TextBox ID="tbtailnumber" runat="server"></asp:TextBox>
            </td>
            <td>
                TypeCode:
                <asp:TextBox ID="tbtypecode" runat="server"></asp:TextBox>
            </td>
            <td>
                Description:
                <asp:TextBox ID="tbdescription" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <%--<table>
        <tr>
            <td>--%>
    <%--      <div class="divGridPanel">
       <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
            ClientIDMode="AutoID">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgFleetProfile">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetProfile" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetProfile" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
       <%--  <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">

            function openWin(url, value, radWin) {

                var oWnd = radopen(url + value, radWin);
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }
            function OnClientCloseAirportMasterPopup(oWnd, args) {
                var combo = $find("<%= tbHomeBase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.ICAO;


                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";

                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseAirportMasterPopup1(oWnd, args) {
                var combo = $find("<%= tbClosestIcao.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = arg.ICAO;


                    }
                    else {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = "";

                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbBillCountry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbBillCountry.ClientID%>").value = arg.CountryCD;
                    }
                    else {
                        document.getElementById("<%=tbBillCountry.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbMetro.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbMetro.ClientID%>").value = arg.MetroCD;

                    }
                    else {
                        document.getElementById("<%=tbMetro.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

        </script>
    </telerik:RadCodeBlock>--%>
    <div id="DivForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <%--  <table style="width: 100%">
                <tr>
                    <td>--%>
            <telerik:RadPanelBar ID="pnlFleetProfileCatalog" Width="100%" ExpandAnimation-Type="none"
                CollapseAnimation-Type="none" runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Charter Rates">
                        <ContentTemplate>
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    Std.Charge:
                                                </td>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="rdbsch" runat="server" Text="H" />
                                                                <asp:RadioButton ID="rdbscn" runat="server" Text="N" />
                                                                <asp:RadioButton ID="rdbscs" runat="server" Text="S" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    Positioning Chrg:
                                                </td>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="rdbpch" runat="server" Text="H" />
                                                                <asp:RadioButton ID="rdbpcn" runat="server" Text="N" />
                                                                <asp:RadioButton ID="rdbpcs" runat="server" Text="S" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Domestic Std.Crew
                                                </td>
                                                <td>
                                                    <telerik:RadMaskedTextBox ID="tbdomesticstdcrew" runat="server" SelectionOnFocus="SelectAll"
                                                        Mask="<0..9>" CssClass="RadMaskedTextBox20">
                                                    </telerik:RadMaskedTextBox>
                                                </td>
                                                <td>
                                                    Intl.Std.Crew
                                                </td>
                                                <td>
                                                    <telerik:RadMaskedTextBox ID="tbintlstdcrew" runat="server" SelectionOnFocus="SelectAll"
                                                        Mask="<0..9>" CssClass="RadMaskedTextBox20">
                                                    </telerik:RadMaskedTextBox>
                                                </td>
                                                <td>
                                                    Min.Daily Usage/Hrs:
                                                </td>
                                                <td>
                                                    <telerik:RadMaskedTextBox ID="tbmindailyusagehrs" runat="server" SelectionOnFocus="SelectAll"
                                                        Mask="<0..9>.<0..99>" CssClass="RadMaskedTextBox20">
                                                    </telerik:RadMaskedTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadGrid ID="dgFleetProfile" runat="server" AllowSorting="true" OnItemCreated="dgFleetProfile_ItemCreated"
            OnNeedDataSource="dgFleetProfile_BindData" OnItemCommand="dgFleetProfile_ItemCommand"
            OnUpdateCommand="dgFleetProfile_UpdateCommand" OnInsertCommand="dgFleetProfile_InsertCommand"
            OnDeleteCommand="dgFleetProfile_DeleteCommand" AutoGenerateColumns="false" Height="330px"
            PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgFleetProfile_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
            <MasterTableView DataKeyNames="AircraftCD,TailNum,AircraftTypeCD,TypeDescription,HomeBase,MaximumPassenger,VendorCD" CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Code">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TailNum" HeaderText="TailNum">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AircraftTypeCD" HeaderText="Type Code">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TypeDescription" HeaderText="TypeDescription">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HomeBase" HeaderText="Home Base">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MaximumPassenger" HeaderText="MaximumPax">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Vendor">
                    </telerik:GridBoundColumn>
                </Columns>
                 <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        &nbsp;
                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                            runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                       <div style="padding: 5px 5px; float: right;">
                    <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                </div>
                </CommandItemTemplate>
            </MasterTableView>
             <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        </telerik:RadGrid>
        </div>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <br />
            <telerik:RadPanelBar ID="pnlbarnotes" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Account Information">
                        <Items>
                            <telerik:RadPanelItem Value="Notes" runat="server">
                                <ContentTemplate>
                                    <table width="100%" cellspacing="0">
                                        <tr>
                                            <td>
                                                <%-- <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadGrid ID="dgFleetProfile" runat="server" AllowSorting="true" OnItemCreated="dgFleetProfile_ItemCreated"
            OnNeedDataSource="dgFleetProfile_BindData" OnItemCommand="dgFleetProfile_ItemCommand"
            OnUpdateCommand="dgFleetProfile_UpdateCommand" OnInsertCommand="dgFleetProfile_InsertCommand"
            OnDeleteCommand="dgFleetProfile_DeleteCommand" AutoGenerateColumns="false" Height="330px"
            PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgFleetProfile_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
            <MasterTableView DataKeyNames="AircraftCD,TailNum,AircraftTypeCD,TypeDescription,HomeBase,MaximumPassenger,VendorCD" CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Code">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TailNum" HeaderText="TailNum">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AircraftTypeCD" HeaderText="Type Code">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TypeDescription" HeaderText="TypeDescription">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HomeBase" HeaderText="Home Base">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MaximumPassenger" HeaderText="MaximumPax">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Vendor">
                    </telerik:GridBoundColumn>
                </Columns>
                 <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        &nbsp;
                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                            runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                       <div style="padding: 5px 5px; float: right;">
                    <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                </div>
                </CommandItemTemplate>
            </MasterTableView>
             <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        </telerik:RadGrid>
        </div>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="width: 100%; border: 1px solid black">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                Year Manufactured:
                                                            </td>
                                                            <td>
                                                                <telerik:RadMaskedTextBox ID="tbyearmanufactured" runat="server" SelectionOnFocus="SelectAll"
                                                                    Mask="<0..9999>" CssClass="RadMaskedTextBox20">
                                                                </telerik:RadMaskedTextBox>
                                                            </td>
                                                            <td>
                                                                Exterior Colors:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbexteriorcolors" CssClass="text120" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkafis" runat="server" Text="AFIS" />
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkuvdatalink" runat="server" Text="UWA Datalink" />
                                                            </td>
                                                            <td>
                                                                Interior Colors:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbinteriorcolors" CssClass="text120" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <div align="right">
                            <asp:Button ID="btnsavechanges" ValidationGroup="Save" runat="server" Text="Save"
                                CssClass="button" />
                            <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
            <%-- </td>
                </tr>
            </table>--%>
        </asp:Panel>
    </div>
</asp:Content>
