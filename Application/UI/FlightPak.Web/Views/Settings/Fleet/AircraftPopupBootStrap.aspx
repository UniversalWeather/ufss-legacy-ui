﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AircraftPopupBootStrap.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.AircraftPopupBootStrap" %>

<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<%@ Import Namespace="System.Web.Optimization" %>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head runat="server">
    <title>Aircraft Types</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
     <script type="text/javascript" src="/Scripts/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <style type="text/css">
        #gridAircraft tr td:first-child {text-align: center!important}
    </style>
    <script type="text/javascript">
        var selectedRowData = "";
        var jqgridTableId = '#gridAircraft';
        var ismultiSelect = false;
        var aircraftcds = [];
        var isopenlookup = false;
        var isautoselect = false;
        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;

            selectedRowData = $.trim(decodeURI(getQuerystring("AircraftCD", "")));
            if (ismultiSelect) {

                if (selectedRowData.length < jQuery("#hdnSelectedRows").val().length) {
                    selectedRowData = jQuery("#hdnSelectedRows").value;
                }
                aircraftcds = selectedRowData.split(',');
            }

            $(document).ready(function() {
                $.extend(jQuery.jgrid.defaults, {
                    prmNames: {
                        page: "page",
                        rows: "size",
                        order: "dir",
                        sort: "sort"
                    }
                });
                selectedRowData = decodeURI(getQuerystring("AircraftCD", ""));
                if (selectedRowData != "") {
                    isopenlookup = true;
                }

                $("#btnSubmit").click(function () {
                    var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                    if (selr != null || (ismultiSelect==true && selectedRowData.length>0)) {
                        var rowData = $(jqgridTableId).getRowData(selr);
                        returnToParent(rowData);
                    }
                    else {
                        BootboxAlert("Please select an aircraft.");
                    }
                    return false;
                });

                jQuery(jqgridTableId).jqGrid({

                    url: '/Views/Utilities/ApiCallerwithFilter.aspx',
                    mtype: 'GET',
                    datatype: "json",
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                    serializeGridData: function (postData) {
                        $(jqgridTableId).jqGrid("clearGridData");
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }

                        postData.apiType = 'fss';
                        postData.method = 'aircraft';
                        postData.aircraftCD = function () {
                                                            if (ismultiSelect && aircraftcds.length>0) {
                                                                return aircraftcds[0];
                                                            } else {
                                                                return selectedRowData;
                                                            }
                                                        };
                        postData.aircraftId = 0;
                        postData.IsInActive = false;
                        postData.markSelectedRecord = isopenlookup;
                        isopenlookup = false;
                        return postData;
                    },
                    height: 300,
                    width: 550,
                    multiselectWidth: 100,
                    multiselect: ismultiSelect,
                    viewrecords: true,
                    rowNum: $("#rowNum").val(),

                    pager: "#pg_gridPager",
                    colNames: ['AircraftId', 'Type Code', 'Aircraft Type', 'Power Setting'],
                    colModel: [
                        { name: 'AircraftID', index: 'AircraftID', key: true, hidden: true },
                        { name: 'AircraftCD', index: 'AircraftCD' },
                        { name: 'AircraftDescription', index: 'AircraftDescription' },
                        { name: 'PowerSetting', index: 'PowerSetting' }

                    ],
                    ondblClickRow: function (rowId) {
                        var rowData = jQuery(this).getRowData(rowId);
                        returnToParent(rowData);
                    },
                    onSelectRow: function (id, status) {

                        var rowData = $(this).getRowData(id);
                        maintainMultiselectRows(id, status);
                        if (ismultiSelect != true) {
                            selectedRowData = $.trim(rowData['AircraftCD']);
                        }

                    },
                    onSelectAll: function(id, status) {
                        for (var i = 0; i < id.length; i++) {
                            maintainMultiselectRows(id[i], status);
                        }
                        aircraftcds = selectedRowData.split(",");
                    },
                afterInsertRow: function (rowid, rowObject) {
                    if (ismultiSelect) {
                        aircraftcds = selectedRowData.split(",");
                            if (jQuery.inArray($.trim(rowObject["AircraftCD"]), aircraftcds) != -1) {
                                $(jqgridTableId).setSelection(rowid, true);
                            }
                        } else {
                            if ($.trim(rowObject["AircraftCD"]) == selectedRowData) {
                                $(jqgridTableId).setSelection(rowid, true);
                            }
                        }
                    },
                    loadComplete: function(rowData) {

                        if (ismultiSelect) {

                            var gridid = jqgridTableId.substring(1,jqgridTableId.length);
                            $("#jqgh_" + gridid + "_cb").find("b").remove();


                            $("#jqgh_" + gridid + "_cb").append("<b> Select All</b>");
                            $("#jqgh_" + gridid + "_cb").css("height", "25px");
                            $(jqgridTableId + "_cb").css("width", "100px");
                            $(jqgridTableId + " tbody tr").children().first("td").css("width", "100px");
                            
                            if (selectedRowData.length < document.getElementById("hdnSelectedRows").value.length) {
                                selectedRowData = document.getElementById("hdnSelectedRows").value;
                            }
                            aircraftcds = selectedRowData.split(',');
                            
                        }
                        setScrollAircraft();
                    }
            });
                $("#pg_gridPager_left").html($("#pagesizebox"));
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
            });
        });
        function maintainMultiselectRows(id, status) {
            if (ismultiSelect) {
                var rowData = $(jqgridTableId).getRowData(id);
                var tempcds = selectedRowData.split(",");
                
                if (status) {
                    
                    if (selectedRowData.lastIndexOf(",") != selectedRowData.length-1) {
                        selectedRowData += ",";
                    }
                    if ($.inArray($.trim(rowData["AircraftCD"]), tempcds) == -1) {
                        selectedRowData += $.trim(rowData["AircraftCD"]) + ",";
                    }
                    selectedRowData.substr(0, selectedRowData.length - 1);
                 
                } else {
                    
                    if (selectedRowData.lastIndexOf(",") != selectedRowData.length - 1) {
                        selectedRowData += ",";
                    }

                    if ($.inArray($.trim(rowData["AircraftCD"]), tempcds) != -1) {
                        tempcds[$.inArray($.trim(rowData["AircraftCD"]), tempcds)] = undefined;
                    }
                    tempcds = jQuery.grep(tempcds, function (value) {
                        return value != undefined;
                    });
                    selectedRowData = tempcds.join(",");
                  
                }
                selectedRowData.substr(0, selectedRowData.length - 1);
                document.getElementById("hdnSelectedRows").value = selectedRowData;
            }
        }
        function localReloadChanges() {
            document.getElementById("hdnSelectedRows").value = "";
            aircraftcds = null;
            selectedRowData = "";
            selectedRowData = $.trim(decodeURI(getQuerystring("AircraftCD", "")));
        }
    </script>
    <script type="text/javascript">
        function returnToParent(rowData) {
            var oArg = new Object();
            if (ismultiSelect) {
                oArg.data = [];
                oArg.Arg1 = "";
                oArg.Arg1 = selectedRowData;
                oArg.Arg1 = oArg.Arg1.substring(0, oArg.Arg1.length - 1);
            } else {
                oArg.AircraftCD = rowData["AircraftCD"];
                oArg.AircraftDescription = rowData["AircraftDescription"];
                oArg.PowerSetting = rowData["PowerSetting"];
                oArg.AircraftID = rowData["AircraftID"];
                oArg.Arg1 = oArg.AircraftCD;
            }
          
            var parentPageName = getQuerystring("ParentPage", "");
            self.parent.SetHtmlControlValue(parentPageName, oArg.AircraftCD);
            self.parent.ManuallyCloseBootstrapPopup("#ModalAircraftPopup");
        }
        $(window).resize(function () {
            setScrollAircraft();
        });
        function setScrollAircraft() {
            var table_header = $('#gbox_gridAircraft').find('.ui-jqgrid-hbox').css("position", "relative");
            $('#gbox_gridAircraft').find('.ui-jqgrid-bdiv').bind('jsp-scroll-x', function (event, scrollPositionX, isAtLeft, isAtRight) {
                table_header.css('right', scrollPositionX);
            }).jScrollPane({
                scrollbarWidth: 15,
                scrollbarMargin: 0
            });
        }
    </script>

    <style type="text/css">
        body {
            height: 300px !important;
            width: 550px !important;
            overflow-y: no-display;
            background: none !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
          <%=Scripts.Render("~/bundles/BootStrapScriptBundle") %>
        <%=Styles.Render("~/bundles/BootStrapPopupCssBundle") %>
        <div class="jqgrid Aircraft_popup bootstrap-common">
            <input type="hidden" id="hdnSelectedRows" value=""/>
            <div>
                <table class="box1">
                    <tr>
                        <td colspan="2">
                            <table id="gridAircraft" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager" class="footer_type2"></div>
                                <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); localReloadChanges(); return false;" />
                                </div>
                            </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>