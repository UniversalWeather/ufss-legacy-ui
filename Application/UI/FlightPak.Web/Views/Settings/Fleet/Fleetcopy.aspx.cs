﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class Fleetcopy : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private string MainBase;
        private string OtherSelected;
        public bool showCommandItems = true;
        string[] strarr;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {


                        if (Request.QueryString["OldAircraftCD"] != null && Request.QueryString["OldAircraftID"] != null)
                        {
                            hdnOldAircraftID.Value = Request.QueryString["OldAircraftID"].ToString();
                            hdnOldAircraftCD.Value = Request.QueryString["OldAircraftCD"].ToString();
                        }
                        if (Request.QueryString["OldFleetID"] != null)
                        {
                            hdnOldFleetID.Value = Request.QueryString["OldFleetID"].ToString();
                        }
                        if (Request.QueryString["OldCQID"] != null)
                        {
                            hdnOldCQID.Value = Request.QueryString["OldCQID"].ToString();
                        }


                        if (Request.QueryString["Mode"] != null)
                        {
                            if (Request.QueryString["Mode"].ToString() == "0")
                            {
                                AircraftTable.Visible = true;
                                FleetTable.Visible = false;
                                CQTable.Visible = false;
                            }
                            if (Request.QueryString["Mode"].ToString() == "1")
                            {
                                AircraftTable.Visible = false;
                                FleetTable.Visible = true;
                                CQTable.Visible = false;
                            }
                            if (Request.QueryString["Mode"].ToString() == "2")
                            {
                                AircraftTable.Visible = false;
                                FleetTable.Visible = false;
                                CQTable.Visible = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetCharterRate);
                }
            }
        }

        protected void dgAircraftType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAircraftType.ClientSettings.Scrolling.ScrollTop = "0";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }

        protected void dgAircraftType_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetAircraftList();
                            List<FlightPakMasterService.GetAllAircraft> lstAircraftType = new List<FlightPakMasterService.GetAllAircraft>();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                lstAircraftType = ObjRetVal.EntityList;
                            }
                            dgAircraftType.DataSource = lstAircraftType;
                            Session["AircraftTypeList"] = lstAircraftType;
                            //if (chkSearchActiveOnly.Checked == true)
                            //{
                            //    SearchAndFilter(false);
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgAircraftType;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (!string.IsNullOrEmpty(OtherSelected))
                {
                    foreach (GridDataItem item in dgAircraftType.MasterTableView.Items)
                    {
                        //if (item["BindingDesc"].Text.Trim().ToUpper() == strarr[0])
                        //{
                        if (item["AircraftCD"].Text.Trim().ToUpper() == OtherSelected)
                        {
                            item.Selected = true;
                            break;
                        }
                        // }
                    }
                }
                else
                {
                    if (dgAircraftType.MasterTableView.Items.Count > 0)
                    {
                        if (Session["AircraftID"] != null)
                        {
                            string ID = Session["AircraftID"].ToString();
                            foreach (GridDataItem Item in dgAircraftType.MasterTableView.Items)
                            {
                                if (Item.GetDataKeyValue("AircraftID").ToString().Trim() == ID)
                                {
                                    Item.Selected = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            dgAircraftType.SelectedIndexes.Add(0);
                        }
                    }

                }
            }
        }

        protected void dgAircraftTypeCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    string resolvedurl = string.Empty;
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                GridDataItem item = dgAircraftType.SelectedItems[0] as GridDataItem;
                                Session["AircraftID"] = item["AircraftID"].Text;
                                TryResolveUrl("/Views/Settings/Fleet/AircraftType.aspx?IsPopup=", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "ProcessPopupAdd('AccountsCatalog.aspx?IsPopup=', 'Add Accounts', '1000','500', '50', '50');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radAircraftTypeCRUDPopup');", true);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/Fleet/AircraftType.aspx?IsPopup=Add", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radAircraftTypeCRUDPopup');", true);
                                break;

                            case RadGrid.DeleteSelectedCommandName:
                                e.Canceled = true;
                                dgAircraftTypeCatalog_DeleteCommand(null, null);
                                dgAircraftType.Rebind();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        protected void dgAircraftTypeCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string SelectedAircraftID = string.Empty;
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //e.Canceled = true;

                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            GridDataItem item = dgAircraftType.SelectedItems[0] as GridDataItem;
                            SelectedAircraftID = item["AircraftID"].Text;
                            Aircraft AircraftType = new Aircraft();
                            string AircraftID = SelectedAircraftID;
                            //string CrewCD = string.Empty;
                            AircraftType.AircraftID = Convert.ToInt64(AircraftID);
                            //Crew.CrewCD = item["CrewCD"].Text;                    // Doubt
                            AircraftType.IsDeleted = true;
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.Crew, Convert.ToInt64(SelectedAircraftID));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Aircraft);
                                    return;
                                }
                            }
                            CrewRosterService.DeleteAircraft(AircraftType);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Aircraft);
                }
                finally
                {
                    if (Session["AircraftID"] != null)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.Aircraft, Convert.ToInt64(SelectedAircraftID));
                        }
                    }
                }

            }
        }
        protected void dgCharterQuoteCustomer_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objMastersvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                CQCustomer oCQCustomer = new CQCustomer();
                List<GetCQCustomer> CQCustomerlist = new List<GetCQCustomer>();
                var objCQCustomer = objMastersvc.GetCQCustomer(oCQCustomer);
                if (objCQCustomer.EntityList != null && objCQCustomer.EntityList.Count > 0)
                {
                    CQCustomerlist = objCQCustomer.EntityList;
                }
                dgCharterQuoteCustomer.DataSource = CQCustomerlist;
            }
        }
        protected void dgFleetPopup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetAllFleetForPopupList();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgFleetPopup.DataSource = ObjRetVal.EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

        }

        protected void dgAircraftTypeOk_click(object sender, EventArgs e)
        {
            if (dgAircraftType.SelectedItems.Count > 0)
            {
                GridDataItem Item = (GridDataItem)dgAircraftType.SelectedItems[0];
                if (Item.GetDataKeyValue("AircraftID") != null)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        Int64 aircraftid = Convert.ToInt64(Item.GetDataKeyValue("AircraftID").ToString());
                        string aircraftcd = Item.GetDataKeyValue("AircraftCD").ToString();
                        var Val = FleetChargeRateService.GetAllFleetNewCharterRate().EntityList.Where(x => x.AircraftTypeID != null && x.AircraftTypeID == aircraftid).ToList();
                        
                        bool Message = false;
                        foreach (var x in Val)
                        {
                            if (x.BuyDOM != null && x.BuyIntl != null && x.BuyDOM.ToString().Trim() != "" && x.BuyIntl.ToString().Trim() != "" && x.BuyDOM != 0 && x.BuyIntl != 0 && x.SellDOM != null && x.SellIntl != null && x.SellDOM.ToString().Trim() != "" && x.SellIntl.ToString().Trim() != "" && x.SellDOM != 0 && x.SellIntl != 0)
                            {
                                Message = true;
                            }
                        }
                        RadWindowManager1.RadConfirm("Charter Rates for selected Aircraft already exists, Do you want to overwrite? (Y/N)?", "confirmSubmitCallBackFn", 400, 30, null, "Confirmation.");
                        hdnTempID.Value = aircraftid.ToString();
                        hdnOk.Value = "0";
                    }
                }
            }
            else
            {
                RadWindowManager1.RadAlert("Please select a record to Copy", 400, 100, "Alert", "");
            }
        }
        protected void dgCharterQuoteCustomerOk_Click(object sender, EventArgs e)
        {
            if (dgCharterQuoteCustomer.SelectedItems.Count > 0)
            {
                GridDataItem Item = (GridDataItem)dgCharterQuoteCustomer.SelectedItems[0];
                if (Item.GetDataKeyValue("CQCustomerID") != null)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        Int64 aircraftid = Convert.ToInt64(Item.GetDataKeyValue("CQCustomerID").ToString());
                        var Val = FleetChargeRateService.GetAllFleetNewCharterRate().EntityList.Where(x => x.CQCustomerID != null && x.CQCustomerID == aircraftid).ToList();

                        bool Message = false;
                        foreach (var x in Val)
                        {
                            if (x.BuyDOM != null && x.BuyIntl != null && x.BuyDOM.ToString().Trim() != "" && x.BuyIntl.ToString().Trim() != "" && x.BuyDOM != 0 && x.BuyIntl != 0 && x.SellDOM != null && x.SellIntl != null && x.SellDOM.ToString().Trim() != "" && x.SellIntl.ToString().Trim() != "" && x.SellDOM != 0 && x.SellIntl != 0)
                            {
                                Message = true;
                            }
                        }
                        RadWindowManager1.RadConfirm("Charter Rates for selected Charter Quote Customer already exists, Do you want to overwrite? (Y/N)?", "confirmSubmitCallBackFnCQ", 400, 30, null, "Confirmation.");
                        hdnTempID.Value = aircraftid.ToString();
                        hdnOk.Value = "0";
                    }
                }
            }
            else
            {
                RadWindowManager1.RadAlert("Please select a record to copy.", 400, 100, "Alert", "");
            }
        }
        protected void dgFleetPopupOk_Click(object sender, EventArgs e)
        {
            if (dgFleetPopup.SelectedItems.Count > 0)
            {
                GridDataItem Item = (GridDataItem)dgFleetPopup.SelectedItems[0];
                if (Item.GetDataKeyValue("FleetID") != null)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        Int64 aircraftid = Convert.ToInt64(Item.GetDataKeyValue("FleetID").ToString());
                        var Val = FleetChargeRateService.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID != null && x.FleetID == aircraftid).ToList();

                        bool Message = false;
                        foreach (var x in Val)
                        {
                            if (x.BuyDOM != null && x.BuyIntl != null && x.BuyDOM.ToString().Trim() != "" && x.BuyIntl.ToString().Trim() != "" && x.BuyDOM != 0 && x.BuyIntl != 0 && x.SellDOM != null && x.SellIntl != null && x.SellDOM.ToString().Trim() != "" && x.SellIntl.ToString().Trim() != "" && x.SellDOM != 0 && x.SellIntl != 0)
                            {
                                Message = true;
                            }
                        }
                        RadWindowManager1.RadConfirm("Charter Rates for selected Fleet already exists, Do you want to overwrite? (Y/N)?", "confirmSubmitCallBackFnFleet", 400, 30, null, "Confirmation");
                        hdnTempID.Value = aircraftid.ToString();
                        hdnOk.Value = "0";
                    }
                }
            }
            else
            {
                RadWindowManager1.RadAlert("Please select a record to copy.", 400, 100, "Alert", "");
            }
        }

        protected void chkActiveOnly_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void chkHomeBaseOnly_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void chkRotary_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void chkFixedWing_CheckedChanged(object sender, EventArgs e)
        {

        }

        //protected void dgFleetPopupOk_Click(object sender, EventArgs e)
        //{

        //}


        protected void btnSubmitAircraftYes_Click(object sender, EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                Int64 aircraftid = Convert.ToInt64(hdnTempID.Value);
                if (hdnOldAircraftID.Value != string.Empty)
                {
                    FleetChargeRateService.CopyAll(aircraftid, Convert.ToInt64(hdnOldAircraftID.Value));
                    RadWindowManager1.RadAlert("Copy Successful", 400, 100, "Alert", "CopySuccessful");
                }
            }
        }
        protected void btnSubmitFleetYes_Click(object sender, EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                Int64 aircraftid = Convert.ToInt64(hdnTempID.Value);
                if (hdnOldFleetID.Value != string.Empty)
                {
                    FleetChargeRateService.CopyAllByFleet(aircraftid, Convert.ToInt64(hdnOldFleetID.Value));
                    RadWindowManager1.RadAlert("Copy Successful", 400, 100, "Alert", "CopySuccessful");
                }
            }
        }
        protected void btnSubmitCQYes_Click(object sender, EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                Int64 aircraftid = Convert.ToInt64(hdnTempID.Value);
                if (hdnOldCQID.Value != string.Empty)
                {
                    FleetChargeRateService.CopyAllByCQ(aircraftid, Convert.ToInt64(hdnOldCQID.Value));
                    RadWindowManager1.RadAlert("Copy Successful", 400, 100, "Alert", "CopySuccessful");
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Passenger);
                };
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllAircraft> lstAircraftType = new List<FlightPakMasterService.GetAllAircraft>();
                if (Session["AircraftTypeList"] != null)
                {
                    lstAircraftType = (List<FlightPakMasterService.GetAllAircraft>)Session["AircraftTypeList"];
                }
                //if (lstAircraftType.Count != 0)
                //{
                //    if (chkSearchActiveOnly.Checked == true) { lstAircraftType = lstAircraftType.Where(x => x.IsInActive == false).ToList<GetAllAircraft>(); }
                //    dgAircraftType.DataSource = lstAircraftType;
                //    if (IsDataBind)
                //    {
                //        dgAircraftType.DataBind();
                //    }
                //}
                return false;
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            SearchAndFilter(true);
        }
        #endregion
    }
}