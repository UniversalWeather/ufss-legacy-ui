﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetChargeHistory : BaseSecuredPage
    {

        bool flag = true;
        string DateFormat = string.Empty;
        DateTime DateStart = new DateTime();
        DateTime DateEnd = new DateTime();
        private List<string> lstFleetChargeRateCodes = new List<string>();
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["FleetID"] != null && Request.QueryString["FleetID"].ToString().Trim() != "")
                        {
                            hdnFleetID.Value = Request.QueryString["FleetID"].ToString();
                        }
                        if (Request.QueryString["TailNum"] != null && Request.QueryString["TailNum"].ToString().Trim() != "")
                        {
                            tbTailNumber.Text = Request.QueryString["TailNum"].ToString();
                        }

                        tbTailNumber.Enabled = false;
                        tbTailNumber.ReadOnly = true;

                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFleetChargeRate, dgFleetChargeRate, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFleetChargeRate.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }
                        ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;

                        if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() != "" && (Request.QueryString["FromPage"].ToString().Trim() == "charter" || Request.QueryString["FromPage"].ToString().Trim() == "preflight" || Request.QueryString["FromPage"].ToString().Trim() == "postflight"))
                        {
                            TBLink.Visible = false;
                        }

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewFleetChargeHistory);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
            }

        }

        protected void dgFleetChargeRate_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFleetChargeRate.ClientSettings.Scrolling.ScrollTop = "0";
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            DefaultSelection();
                            btnSaveChanges.Visible = false;
                            btnCancel.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFleetChargeRate.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFleetChargeRate.Rebind();
                    SelectItem(hdnFleetChargeRateID.Value);
                    LoadData(hdnFleetChargeRateID.Value);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetChargeRate_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
            }

        }
        /// <summary>
        /// Bind Fleet Charge Rate Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetChargeRate_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (!string.IsNullOrEmpty(hdnFleetID.Value))
                            {
                                var DelVal = FleetChargeRateService.GetFleetChargeRateList(Convert.ToInt64(hdnFleetID.Value));
                                if (DelVal.ReturnFlag == true)
                                {
                                    dgFleetChargeRate.DataSource = DelVal.EntityList;
                                }
                                Session.Remove("FleetChargeRateCodes");
                                lstFleetChargeRateCodes = new List<string>();
                                foreach (FlightPakMasterService.FleetChargeRate FleetChargeRateTypeEntity in DelVal.EntityList)
                                {
                                    lstFleetChargeRateCodes.Add(FleetChargeRateTypeEntity.AircraftCD);
                                    Session["FleetChargeRateCodes"] = lstFleetChargeRateCodes;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
            }
        }

        /// <summary>
        /// Item Command for Fleet Component Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetChargeRate_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFleetChargeRate.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                btnhours.Checked = true;
                                hdnFleetChargeRateID.Value = "0";
                                hdnSave.Value = "Insert";
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.FleetChargeHistory, Convert.ToInt64(hdnFleetChargeRateID.Value));
                                    Session["IsFleetChargeHistoryEditLock"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetChargeHistory);
                                        DefaultSelection();
                                        return;
                                    }
                                    DisplayEditForm(hdnFleetChargeRateID.Value);
                                    GridEnable(false, true, false);
                                    hdnSave.Value = "Update";
                                    hdnRedirect.Value = "";
                                }
                                break;
                            case "UpdateEdited":
                                dgFleetChargeRate_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            case "Update":
                                dgFleetChargeRate_UpdateCommand(sender, e);
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
            }

        }
        /// <summary>
        /// Update Command for Fleet Charge Rate Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFleetChargeRate_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        if (hdnFleetChargeRateID.Value.ToString().Trim() != "" && Convert.ToInt64(hdnFleetChargeRateID.Value.ToString()) > 0)
                        {
                            TextBox tbenddate = new TextBox();
                            tbenddate.Text = ((TextBox)(uctbEnd.FindControl("tbDate"))).Text;

                            TextBox tbBegDate = new TextBox();
                            tbBegDate.Text = ((TextBox)(ucBegDate.FindControl("tbDate"))).Text;

                            ValidateSiflDateFormat();

                            if (string.IsNullOrEmpty(tbBegDate.Text.Trim().ToString()))
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Begin Date Required', 360, 50, 'Fleet Charge History Catalog');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                btnCancel.Visible = true;
                                btnSaveChanges.Visible = true;
                                flag = false;
                                return;
                            }
                            if (string.IsNullOrEmpty(tbenddate.Text.Trim().ToString()))
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('End Date Required', 360, 50, 'Fleet Charge History Catalog');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                btnCancel.Visible = true;
                                btnSaveChanges.Visible = true;
                                flag = false;
                                return;
                            }

                            if (DateStart > DateEnd)
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('End Date must be equal to or greater than Begin Date', 360, 50, 'Fleet Charge History Catalog');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                btnCancel.Visible = true;
                                btnSaveChanges.Visible = true;
                                flag = false;
                                return;
                            }

                            if (tbchgrate.Text.Trim() != "")
                            {
                                string strChg = tbchgrate.Text.Trim();
                                int TotIndex = strChg.Length;
                                int DotIndex = strChg.IndexOf(".");
                                if (DotIndex >= 0)
                                {
                                    if (TotIndex - DotIndex > 3 || DotIndex == 0 && TotIndex > 3)
                                    {
                                        //cvRate.IsValid = false;
                                        btnCancel.Visible = true;
                                        btnSaveChanges.Visible = true;
                                        flag = false;
                                        if (tbchgrate.Enabled)
                                            tbchgrate.Focus();
                                        return;
                                    }
                                }
                                if (TotIndex > 15)
                                {
                                    if (DotIndex <= 0 || DotIndex > 15)
                                    {
                                        //cvRate.IsValid = false;
                                        btnCancel.Visible = true;
                                        btnSaveChanges.Visible = true;
                                        flag = false;
                                        if (tbchgrate.Enabled)
                                            tbchgrate.Focus();
                                        return;
                                    }
                                }
                            }

                            Page.Validate();

                            MasterCatalogServiceClient Service = new MasterCatalogServiceClient();
                            var RateExists = Service.GetFleetChargeRateExists(DateStart, DateEnd, Convert.ToInt64(hdnFleetID.Value), Convert.ToInt64(hdnFleetChargeRateID.Value));
                            int RateCount = 0;
                            if (RateExists != null)
                            {
                                foreach (FlightPakMasterService.GetFleetChargeRateExists RateCnt in RateExists.EntityList)
                                {
                                    RateCount = Convert.ToInt16(RateCnt.Total);
                                }
                            }
                            if (RateCount > 0)
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Flight charge rate already exists in the specified date range', 360, 50, 'Fleet Charge History Catalog');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                btnCancel.Visible = true;
                                btnSaveChanges.Visible = true;
                                flag = false;
                                return;
                            }
                            else
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.FleetChargeRate objFleetChargeRate = new FleetChargeRate();
                                    objFleetChargeRate = GetItems(string.Empty);
                                    var RetVal = FleetChargeRateService.UpdateFleetChargeRateType(objFleetChargeRate);
                                    if (RetVal.ReturnFlag == true)
                                    {
                                        if (!string.IsNullOrEmpty(hdnCalculateFleetChargeRate.Value) && hdnCalculateFleetChargeRate.Value == "YES")
                                        {
                                            FleetChargeRateService.CalculateChargeRate(objFleetChargeRate);

                                            //Clear Postflight and preflight session to update fleet charge rate
                                            if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() != "" && Request.QueryString["FromPage"].ToString().Trim() == "preflight")
                                            { }
                                            else
                                                Session.Remove("CurrentPreFlightTrip");

                                            if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() != "" && Request.QueryString["FromPage"].ToString().Trim() == "postflight")
                                            { }
                                            else
                                                Session.Remove("POSTFLIGHTMAIN");
                                        }
                                        // Unlock the Record
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.FleetChargeHistory, Convert.ToInt64(hdnFleetChargeRateID.Value));
                                        }
                                        Session["IsFleetChargeHistoryEditLock"] = "False";
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;

                                        ShowSuccessMessage();

                                        GridEnable(true, true, true);
                                        DefaultSelection();
                                        EnableForm(false);
                                        btnSaveChanges.Visible = false;
                                        btnCancel.Visible = false;
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(RetVal.ErrorMessage, ModuleNameConstants.Database.FleetChargeHistory);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
            }

        }



        /// <summary>
        /// Update Command for Fleet Charge Rate Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFleetChargeRate_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        TextBox tbenddate = new TextBox();
                        tbenddate.Text = ((TextBox)(uctbEnd.FindControl("tbDate"))).Text;

                        TextBox tbBegDate = new TextBox();
                        tbBegDate.Text = ((TextBox)(ucBegDate.FindControl("tbDate"))).Text;

                        ValidateSiflDateFormat();

                        if (string.IsNullOrEmpty(tbBegDate.Text.Trim().ToString()))
                        {
                            
                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Begin Date Required', 360, 50, 'Fleet Charge History Catalog');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            btnCancel.Visible = true;
                            btnSaveChanges.Visible = true;
                            flag = false;
                            return;
                        }
                        if (string.IsNullOrEmpty(tbenddate.Text.Trim().ToString()))
                        {
                            
                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('End Date Required', 360, 50, 'Fleet Charge History Catalog');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            btnCancel.Visible = true;
                            btnSaveChanges.Visible = true;
                            flag = false;
                            return;
                        }

                        if (DateStart > DateEnd)
                        {
                            
                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('End Date must be equal to or greater than Begin Date', 360, 50, 'Fleet Charge History Catalog');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            btnCancel.Visible = true;
                            btnSaveChanges.Visible = true;
                            flag = false;
                            return;
                        }

                        if (tbchgrate.Text.Trim() != "")
                        {
                            string strChg = tbchgrate.Text.Trim();
                            int TotIndex = strChg.Length;
                            int DotIndex = strChg.IndexOf(".");
                            if (DotIndex >= 0)
                            {
                                if (TotIndex - DotIndex > 3 || DotIndex == 0 && TotIndex > 3)
                                {
                                    //cvRate.IsValid = false;
                                    btnCancel.Visible = true;
                                    btnSaveChanges.Visible = true;
                                    flag = false;
                                    if (tbchgrate.Enabled)
                                        tbchgrate.Focus();
                                    return;
                                }
                            }
                            if (TotIndex > 15)
                            {
                                if (DotIndex <= 0 || DotIndex > 15)
                                {
                                    //cvRate.IsValid = false;
                                    btnCancel.Visible = true;
                                    btnSaveChanges.Visible = true;
                                    flag = false;
                                    if (tbchgrate.Enabled)
                                        tbchgrate.Focus();
                                    return;
                                }
                            }
                        }

                        Page.Validate();
                        if (!Page.IsValid)
                        {
                            flag = false;
                        }

                        if (!string.IsNullOrEmpty(hdnFleetID.Value))
                        {
                            MasterCatalogServiceClient Service = new MasterCatalogServiceClient();
                            var RateExists = Service.GetFleetChargeRateExists(DateStart, DateEnd, Convert.ToInt64(hdnFleetID.Value), 0);
                            int RateCount = 0;
                            if (RateExists != null)
                            {
                                foreach (FlightPakMasterService.GetFleetChargeRateExists RateCnt in RateExists.EntityList)
                                {
                                    RateCount = Convert.ToInt16(RateCnt.Total);
                                }
                            }

                            if (RateCount > 0)
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Flight charge rate already exists in the specified date range', 360, 50, 'Fleet Charge History Catalog');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                btnCancel.Visible = true;
                                btnSaveChanges.Visible = true;
                                flag = false;
                                return;
                            }
                            else
                            {
                                using (MasterCatalogServiceClient FleetChargeRateTypeService = new MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.FleetChargeRate objFleetChargeRate = new FleetChargeRate();
                                    objFleetChargeRate = GetItems(string.Empty);
                                    var RetVal = FleetChargeRateTypeService.AddFleetChargeRateType(objFleetChargeRate);
                                    if (RetVal != null && RetVal.ReturnFlag == true)
                                    {
                                        if (!string.IsNullOrEmpty(hdnCalculateFleetChargeRate.Value) && hdnCalculateFleetChargeRate.Value == "YES")
                                        {
                                            var objFleetChargeRates = FleetChargeRateTypeService.GetFleetChargeRateList(objFleetChargeRate.FleetID.Value);
                                            var FleetChargeRateList = objFleetChargeRates.EntityList.Where(x => x.BeginRateDT == objFleetChargeRate.BeginRateDT &&
                                                                                                            x.EndRateDT == objFleetChargeRate.EndRateDT).ToList();

                                            if (FleetChargeRateList != null && ((List<FleetChargeRate>)FleetChargeRateList).Count > 0)
                                            {
                                                objFleetChargeRate.FleetChargeRateID = ((List<FleetChargeRate>)FleetChargeRateList)[0].FleetChargeRateID;
                                            }

                                            FleetChargeRateTypeService.CalculateChargeRate(objFleetChargeRate);
                                            //Clear Postflight and preflight session to update fleet charge rate
                                            if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() != "" && Request.QueryString["FromPage"].ToString().Trim() == "preflight")
                                            { }
                                            else
                                                Session.Remove("CurrentPreFlightTrip");

                                            if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() != "" && Request.QueryString["FromPage"].ToString().Trim() == "postflight")
                                            { }
                                            else
                                            Session.Remove("POSTFLIGHTMAIN");
                                        }
                                        DefaultSelection();

                                        ShowSuccessMessage();

                                        GridEnable(true, true, true);
                                        //DefaultSelection();
                                        EnableForm(false);
                                        btnSaveChanges.Visible = false;
                                        btnCancel.Visible = false;
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(RetVal.ErrorMessage, ModuleNameConstants.Database.FleetChargeHistory);
                                    }
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
            }

        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFleetChargeRate_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (hdnFleetChargeRateID.Value.ToString().Trim() != "" && Convert.ToInt64(hdnFleetChargeRateID.Value.ToString()) > 0)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FleetChargeRate FleetChargerateType = new FlightPakMasterService.FleetChargeRate();


                                FleetChargerateType.FleetChargeRateID = Convert.ToInt64(hdnFleetChargeRateID.Value);

                                FleetChargerateType.IsDeleted = true;

                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.FleetChargeHistory, Convert.ToInt64(hdnFleetChargeRateID.Value));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection();
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetChargeHistory);
                                        return;
                                    }
                                    FleetChargeRateService.DeleteFleetChargeRateType(FleetChargerateType);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    ClearForm();
                                    btnSaveChanges.Visible = false;
                                    btnCancel.Visible = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FleetChargeHistory, Convert.ToInt64(hdnFleetChargeRateID.Value));
                    }
                }
            }

        }
        /// <summary>
        /// Bind Selected Item  for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetChargeRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem(hdnFleetChargeRateID.Value);

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {

                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            GridDataItem item = dgFleetChargeRate.SelectedItems[0] as GridDataItem;
                            hdnFleetChargeRateID.Value = item.GetDataKeyValue("FleetChargeRateID").ToString();

                            Label lblUser;

                            lblUser = (Label)dgFleetChargeRate.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + item.GetDataKeyValue("LastUpdUID").ToString());
                            }
                            else
                            {
                                lblUser.Text = string.Empty;
                            }
                            if (item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.GetDataKeyValue("LastUpdTS").ToString())));
                            }
                            LoadData(hdnFleetChargeRateID.Value);   //replace DisplayEditForm() with LoadData()

                            GridEnable(true, true, true);
                            EnableForm(false);
                            btnSaveChanges.Visible = false;
                            btnCancel.Visible = false;
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                    }
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();
                EnableForm(true);
                tbchgrate.ReadOnly = false;
                btnCancel.Visible = true;
                btnSaveChanges.Visible = true;
            }
        }


        /// <summary>
        /// Command Event Trigger for Save or Update Fleet Charge Rate Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value.ToString().ToUpper() == "UPDATE")
                        {
                            (dgFleetChargeRate.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgFleetChargeRate.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                        //if (flag)
                        //{
                        //    GridEnable(true, true, true);
                        //    DefaultSelection();
                        //    EnableForm(false);
                        //    btnSaveChanges.Visible = false;
                        //    btnCancel.Visible = false;
                        //}

                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
            }

        }
        /// <summary>
        /// Cancel Fleet Charge Rate Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //pnlExternalForm.Visible = false;
                        // Unlock the Record
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.FleetChargeHistory, Convert.ToInt64(hdnFleetChargeRateID.Value));
                        }
                        btnSaveChanges.Visible = false;
                        btnCancel.Visible = false;
                        GridEnable(true, true, true);
                        ClearForm();
                        DefaultSelection();
                        EnableForm(false);
                        //SelectItem(hdnFleetChargeRateID.Value);
                        //LoadData(hdnFleetChargeRateID.Value);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
            }

            // tbCode.ReadOnly = true;
            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFleetChargeRate;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetChargeHistory);
                }
            }

        }

        /// <summary>
        /// It Select The Default Item In The Grid
        /// </summary>
        protected void DefaultSelection()
        {

            dgFleetChargeRate.Rebind();
            //dgFleetChargeRate.SelectedIndexes.Add(0);
            if (dgFleetChargeRate.Items.Count > 0)
            {
                //if (!IsPostBack)
                //{
                //    hdnFleetChargeRateID.Value = "0";
                //}
                if (string.IsNullOrEmpty(hdnFleetChargeRateID.Value) || hdnFleetChargeRateID.Value == "0")
                {
                    dgFleetChargeRate.SelectedIndexes.Add(0);
                    GridDataItem item = dgFleetChargeRate.SelectedItems[0] as GridDataItem;
                    hdnFleetChargeRateID.Value = item.GetDataKeyValue("FleetChargeRateID").ToString();
                }

                if (dgFleetChargeRate.SelectedIndexes.Count == 0)
                    dgFleetChargeRate.SelectedIndexes.Add(0);

                //TextBox tbtbEnd = new TextBox();
                //tbtbEnd.Text = ((TextBox)(uctbEnd.FindControl("tbDate"))).Text;

                //TextBox tbBeginDate = new TextBox();
                //tbBeginDate.Text = ((TextBox)(ucBegDate.FindControl("tbDate"))).Text;

                //GridDataItem item = dgFleetChargeRate.SelectedItems[0] as GridDataItem;
                //hdnFleetChargeRateID.Value = item.GetDataKeyValue("FleetChargeRateID").ToString();
                //Label lblUser;

                //lblUser = (Label)dgFleetChargeRate.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                //if (item.GetDataKeyValue("LastUpdUID") != null)
                //{
                //    lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + item.GetDataKeyValue("LastUpdUID").ToString());
                //}
                //else
                //{
                //    lblUser.Text = string.Empty;
                //}
                //if (item.GetDataKeyValue("LastUpdTS") != null)
                //{
                //    lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.GetDataKeyValue("LastUpdTS").ToString())));
                //}

                //if (item.GetDataKeyValue("BeginRateDT") != null)
                //{
                //    tbBeginDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", item.GetDataKeyValue("BeginRateDT"));
                //    ((TextBox)(ucBegDate.FindControl("tbDate"))).Text = tbBeginDate.Text;
                //}
                //else
                //{
                //    tbBeginDate.Text = string.Empty;
                //}

                //if (item.GetDataKeyValue("EndRateDT") != null)
                //{
                //    tbtbEnd.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", item.GetDataKeyValue("EndRateDT"));
                //    ((TextBox)(uctbEnd.FindControl("tbDate"))).Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", item.GetDataKeyValue("EndRateDT"));
                //}
                //else
                //{
                //    tbtbEnd.Text = string.Empty;
                //}
                //if (item.GetDataKeyValue("ChargeRate") != null)
                //{
                //    tbchgrate.Text = item.GetDataKeyValue("ChargeRate").ToString().Trim();
                //}
                //else
                //{
                //    tbchgrate.Text = string.Empty;
                //}

                //if (item.GetDataKeyValue("ChargeUnit") != null)
                //{
                //    if (item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "H")
                //    {
                //        btnhours.Checked = true;
                //        btndays.Checked = false;
                //        btncycles.Checked = false;
                //        btnKM.Checked = false;
                //    }
                //    if (item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "N")
                //    {
                //        btndays.Checked = true;
                //        btnhours.Checked = false;
                //        btncycles.Checked = false;
                //        btnKM.Checked = false;
                //    }
                //    if (item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "S")
                //    {
                //        btncycles.Checked = true;
                //        btndays.Checked = false;
                //        btnhours.Checked = false;
                //        btnKM.Checked = false;
                //    }
                //    if (item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "K")
                //    {
                //        btnKM.Checked = true;
                //        btncycles.Checked = false;
                //        btndays.Checked = false;
                //        btnhours.Checked = false;
                //    }
                //}
                //lbColumnName1.Text = "Begin Date";
                //lbColumnName2.Text = "End Date";
                //lbColumnValue1.Text = item["BeginRateDT"].Text;
                //lbColumnValue2.Text = item["EndRateDT"].Text;

                LoadData(hdnFleetChargeRateID.Value);
                //SelectItem(hdnFleetChargeRateID.Value);
            }
            EnableForm(false);
        }


        private void SelectItem(string itm)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(itm))
            {
                foreach (GridDataItem Item in dgFleetChargeRate.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("FleetChargeRateID").ToString() == itm)
                    {
                        //hdnFleetChargeRateID.Value = itm;
                        Item.Selected = true;
                        break;
                    }
                }

            }

        }

        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="delayTypeCode"></param>
        /// <returns></returns>
        private FleetChargeRate GetItems(string delayTypeCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(delayTypeCode))
            {
                FlightPakMasterService.FleetChargeRate FleetChargeRateType = new FlightPakMasterService.FleetChargeRate();

                if (hdnFleetChargeRateID.Value.ToString().Trim() != "")
                    FleetChargeRateType.FleetChargeRateID = Convert.ToInt64(hdnFleetChargeRateID.Value);

                if (hdnFleetID.Value.ToString().Trim() != "")
                    FleetChargeRateType.FleetID = Convert.ToInt64(hdnFleetID.Value);

                TextBox tbBeginDatee = (TextBox)ucBegDate.FindControl("tbDate");
                if (!string.IsNullOrEmpty(tbBeginDatee.Text.Trim().ToString()))
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    if (DateFormat != null)
                    {
                        FleetChargeRateType.BeginRateDT = Convert.ToDateTime(FormatDate(tbBeginDatee.Text, DateFormat));
                    }
                    else
                    {
                        FleetChargeRateType.BeginRateDT = Convert.ToDateTime(tbBeginDatee.Text);
                    }
                }
                TextBox tbenddate = (TextBox)uctbEnd.FindControl("tbDate");
                if (!string.IsNullOrEmpty(tbenddate.Text.Trim().ToString()))
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    if (DateFormat != null)
                    {
                        FleetChargeRateType.EndRateDT = Convert.ToDateTime(FormatDate(tbenddate.Text, DateFormat));
                    }
                    else
                    {
                        FleetChargeRateType.EndRateDT = Convert.ToDateTime(tbenddate.Text);
                    }
                }

                if (btnhours.Checked)
                {
                    FleetChargeRateType.ChargeUnit = "H";
                }

                else if (btndays.Checked)
                {
                    FleetChargeRateType.ChargeUnit = "N";
                }

                else if (btncycles.Checked)
                {
                    FleetChargeRateType.ChargeUnit = "S";
                }
                else if (btnKM.Checked)
                {
                    FleetChargeRateType.ChargeUnit = "K";
                }


                if (!string.IsNullOrEmpty(tbchgrate.Text))
                {
                    FleetChargeRateType.ChargeRate = Convert.ToDecimal(tbchgrate.Text);
                }
                FleetChargeRateType.IsDeleted = false;
                if (!string.IsNullOrEmpty(Request.QueryString["AircraftID"]))
                {
                    FleetChargeRateType.AircraftID = Convert.ToInt64(Request.QueryString["AircraftID"]);
                }
                //pnlExternalForm.Visible = false;
                return FleetChargeRateType;
            }
        }

        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm(string Code)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                TextBox tbBeginDate = (TextBox)ucBegDate.FindControl("tbDate");
                TextBox tbtbEnd = (TextBox)uctbEnd.FindControl("tbDate");
                foreach (GridDataItem Item in dgFleetChargeRate.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("FleetChargeRateID").ToString().ToUpper() == Code.ToUpper())
                    {
                        if (Item.GetDataKeyValue("FleetChargeRateID") != null)
                            hdnFleetChargeRateID.Value = Item.GetDataKeyValue("FleetChargeRateID").ToString().Trim();
                        else
                            hdnFleetChargeRateID.Value = "0";
                        if (Item.GetDataKeyValue("FleetID") != null)
                            hdnFleetID.Value = Item.GetDataKeyValue("FleetID").ToString().Trim();
                        else
                            hdnFleetID.Value = string.Empty;

                        if (Item.GetDataKeyValue("BeginRateDT") != null)
                        {
                            tbBeginDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Item.GetDataKeyValue("BeginRateDT"));
                        }
                        else
                            tbBeginDate.Text = string.Empty;
                        if (Item.GetDataKeyValue("EndRateDT") != null)
                            tbtbEnd.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Item.GetDataKeyValue("EndRateDT"));
                        else
                            tbtbEnd.Text = string.Empty;

                        if (Item.GetDataKeyValue("ChargeRate") != null)
                            tbchgrate.Text = Item.GetDataKeyValue("ChargeRate").ToString().Trim();
                        else
                            tbchgrate.Text = string.Empty;

                        if (Item.GetDataKeyValue("ChargeUnit") != null)
                        {
                            if (Item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "H")
                            {
                                btnhours.Checked = true;
                                btndays.Checked = false;
                                btncycles.Checked = false;
                                btnKM.Checked = false;
                            }
                            if (Item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "N")
                            {
                                btndays.Checked = true;
                                btnhours.Checked = false;
                                btncycles.Checked = false;
                                btnKM.Checked = false;
                            }
                            if (Item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "S")
                            {
                                btncycles.Checked = true;
                                btndays.Checked = false;
                                btnhours.Checked = false;
                                btnKM.Checked = false;
                            }
                            if (Item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "K")
                            {
                                btnKM.Checked = true;
                                btncycles.Checked = false;
                                btndays.Checked = false;
                                btnhours.Checked = false;
                            }
                        }

                        Item.Selected = true;
                        break;
                    }
                }

                btnCancel.Visible = true;
                btnSaveChanges.Visible = true;
                EnableForm(true);
            }

        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, delCtl, editCtl;
                insertCtl = (LinkButton)dgFleetChargeRate.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                delCtl = (LinkButton)dgFleetChargeRate.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                editCtl = (LinkButton)dgFleetChargeRate.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                if (IsAuthorized(Permission.Database.AddFleetChargeHistory))
                {
                    insertCtl.Visible = true;
                    if (add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteFleetChargeHistory))
                {
                    delCtl.Visible = true;
                    if (delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = "";
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditFleetChargeHistory))
                {
                    editCtl.Visible = true;
                    if (edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = "";
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                TextBox tbplastinspection = (TextBox)ucBegDate.FindControl("tbDate");
                tbplastinspection.Text = string.Empty;
                TextBox tbenddate = (TextBox)uctbEnd.FindControl("tbDate");
                tbenddate.Text = string.Empty;
                tbchgrate.Text = string.Empty;
                btncycles.Checked = false;
                btndays.Checked = false;
                btnhours.Checked = true;
                btnKM.Checked = false;
                //hdnFleetChargeRateID.Value = "0";
                hdnCalculateFleetChargeRate.Value = string.Empty;
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                tbchgrate.Enabled = enable;
                TextBox tbplastinspection = (TextBox)ucBegDate.FindControl("tbDate");
                tbplastinspection.Enabled = enable;
                TextBox tbenddate = (TextBox)uctbEnd.FindControl("tbDate");
                tbenddate.Enabled = enable;
                btncycles.Enabled = enable;
                btnhours.Enabled = enable;
                btndays.Enabled = enable;
                btnKM.Enabled = enable;
            }
        }


        private void ValidateSiflDateFormat()
        {
            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
            string StartDate = ((TextBox)ucBegDate.FindControl("tbDate")).Text;
            string EndDate = ((TextBox)uctbEnd.FindControl("tbDate")).Text;
            if (!string.IsNullOrEmpty(StartDate))
            {
                if (DateFormat != null)
                {
                    DateStart = Convert.ToDateTime(FormatDate(StartDate, DateFormat));
                }
                else
                {
                    DateStart = Convert.ToDateTime(StartDate);
                }
            }
            if (!string.IsNullOrEmpty(EndDate))
            {
                if (DateFormat != null)
                {
                    DateEnd = Convert.ToDateTime(FormatDate(EndDate, DateFormat));
                }
                else
                {
                    DateEnd = Convert.ToDateTime(EndDate);
                }
            }
        }

        /// <summary>
        /// for setting format for date in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetChargeRate_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem DataItem = e.Item as GridDataItem;
                            GridColumn Column = dgFleetChargeRate.MasterTableView.GetColumn("BeginRateDT");
                            string UwaValue = DataItem["BeginRateDT"].Text.Trim();
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["BeginRateDT"];
                            TableCell cell1 = (TableCell)Item["EndRateDT"];
                            if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                            {
                                cell.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(cell.Text)));
                            }
                            if ((!string.IsNullOrEmpty(cell1.Text)) && (cell1.Text != "&nbsp;"))
                            {
                                cell1.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(cell1.Text)));
                            }


                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {

                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }
        protected void LoadData(string Code)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                TextBox tbBeginDate = (TextBox)ucBegDate.FindControl("tbDate");
                TextBox tbtbEnd = (TextBox)uctbEnd.FindControl("tbDate");
                foreach (GridDataItem Item in dgFleetChargeRate.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("FleetChargeRateID").ToString().ToUpper() == Code.ToUpper())
                    {
                        if (Item.GetDataKeyValue("FleetChargeRateID") != null)
                            hdnFleetChargeRateID.Value = Item.GetDataKeyValue("FleetChargeRateID").ToString().Trim();
                        else
                            hdnFleetChargeRateID.Value = "0";
                        if (Item.GetDataKeyValue("FleetID") != null)
                            hdnFleetID.Value = Item.GetDataKeyValue("FleetID").ToString().Trim();
                        else
                            hdnFleetID.Value = string.Empty;

                        if (Item.GetDataKeyValue("BeginRateDT") != null)
                        {
                            tbBeginDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Item.GetDataKeyValue("BeginRateDT"));
                        }
                        else
                            tbBeginDate.Text = string.Empty;
                        if (Item.GetDataKeyValue("EndRateDT") != null)
                            tbtbEnd.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Item.GetDataKeyValue("EndRateDT"));
                        else
                            tbtbEnd.Text = string.Empty;

                        if (Item.GetDataKeyValue("ChargeRate") != null)
                            tbchgrate.Text = Item.GetDataKeyValue("ChargeRate").ToString().Trim();
                        else
                            tbchgrate.Text = string.Empty;

                        if (Item.GetDataKeyValue("ChargeUnit") != null)
                        {
                            if (Item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "H")
                            {
                                btnhours.Checked = true;
                                btndays.Checked = false;
                                btncycles.Checked = false;
                                btnKM.Checked = false;
                            }
                            if (Item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "N")
                            {
                                btndays.Checked = true;
                                btnhours.Checked = false;
                                btncycles.Checked = false;
                                btnKM.Checked = false;
                            }
                            if (Item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "S")
                            {
                                btncycles.Checked = true;
                                btndays.Checked = false;
                                btnhours.Checked = false;
                                btnKM.Checked = false;
                            }
                            if (Item.GetDataKeyValue("ChargeUnit").ToString().Trim() == "K")
                            {
                                btnKM.Checked = true;
                                btncycles.Checked = false;
                                btndays.Checked = false;
                                btnhours.Checked = false;
                            }
                        }

                        lbColumnName1.Text = "Begin Date";
                        lbColumnName2.Text = "End Date";
                        lbColumnValue1.Text = Item["BeginRateDT"].Text;
                        lbColumnValue2.Text = Item["EndRateDT"].Text;

                        Item.Selected = true;
                        break;
                    }
                }
            }

        }
        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFleetChargeRate.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var ChargeValue = FPKMstService.GetFleetChargeRateList(Convert.ToInt64(hdnFleetID.Value));
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, ChargeValue);
            List<FlightPakMasterService.FleetChargeRate> filteredList = GetFilteredList(ChargeValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FleetChargeRateID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFleetChargeRate.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFleetChargeRate.CurrentPageIndex = PageNumber;
            dgFleetChargeRate.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfFleetChargeRate ChargeValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    hdnFleetChargeRateID.Value = Session["SearchItemPrimaryKeyValue"].ToString();
                }
            }
            else
            {
                PrimaryKeyValue = ChargeValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FleetChargeRateID;
                hdnFleetChargeRateID.Value = PrimaryKeyValue.ToString();
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.FleetChargeRate> GetFilteredList(ReturnValueOfFleetChargeRate ChargeValue)
        {
            List<FlightPakMasterService.FleetChargeRate> filteredList = new List<FlightPakMasterService.FleetChargeRate>();

            if (ChargeValue.ReturnFlag)
            {
                filteredList = ChargeValue.EntityList;
            }

            return filteredList;
        }

        protected void dgFleetChargeRate_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFleetChargeRate, Page.Session);
        }
        
    }
}