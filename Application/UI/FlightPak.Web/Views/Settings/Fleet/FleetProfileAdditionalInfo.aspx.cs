﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

using System.Data;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetProfileAdditionalInfo : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender,e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            lblMessage.Text = string.Empty; 
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }
	
        }

        protected void dgAircraftType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPassengerAddInfo.ClientSettings.Scrolling.ScrollTop = "0";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }

        /// <summary>
        /// Method to Bind Grid Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PassengerAddInfo_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender,e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetVal = objService.GetFleetProfileInformationList();
                            if (RetVal.ReturnFlag == true)
                            {
                                dgPassengerAddInfo.DataSource = RetVal.EntityList.Where(x => x.IsDeleted == false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }
	
        }

        /// <summary>
        /// Method to trigger Grid Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PassengerAddInfo_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender,e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }
	
        }

        protected void btnSub_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgPassengerAddInfo.SelectedItems.Count > 0)
                {
                    GridDataItem item = (GridDataItem)dgPassengerAddInfo.SelectedItems[0];

                    if (CheckIfExists(Convert.ToInt64(item.GetDataKeyValue("FleetProfileInformationID").ToString())))
                    {
                        lblMessage.Text = "Warning, Additional Info. Code Already Exists.";
                        //lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        if (CheckIfExists(Convert.ToInt64(item.GetDataKeyValue("FleetProfileInformationID").ToString())))
                        {
                            lblMessage.Text = "Warning, Additional Info. Code Already Exists.";
                            //lblMessage.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            bool IsExist = false;
                            List<FlightPakMasterService.GetFleetProfileDefinition> CrewDefinitionInfo = (List<FlightPakMasterService.GetFleetProfileDefinition>)Session["FleetProfileDefinition"];
                            for (int Index = 0; Index < CrewDefinitionInfo.Count; Index++)
                            {
                                if (CrewDefinitionInfo[Index].FleetInfoCD == item.GetDataKeyValue("FleetInfoCD").ToString())
                                {
                                    CrewDefinitionInfo[Index].InformationValue = string.Empty;
                                    CrewDefinitionInfo[Index].IsDeleted = false;
                                    IsExist = true;
                                }
                            }
                            Session["FleetProfileDefinition"] = CrewDefinitionInfo;
                            if (IsExist == false)
                            {
                                List<FlightPakMasterService.GetFleetProfileDefinition> FPInfo = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                                FlightPakMasterService.GetFleetProfileDefinition FleetAdditionalInfoDef = new FlightPakMasterService.GetFleetProfileDefinition();
                                FleetAdditionalInfoDef.FleetProfileInfoXRefID = 0;
                                FleetAdditionalInfoDef.FleetInfoCD = item.GetDataKeyValue("FleetInfoCD").ToString();
                                FleetAdditionalInfoDef.CustomerID = Convert.ToInt64(item.GetDataKeyValue("CustomerID").ToString());
                                FleetAdditionalInfoDef.FleetProfileInformationID = Convert.ToInt64(item.GetDataKeyValue("FleetProfileInformationID").ToString());
                                FleetAdditionalInfoDef.FleetDescription = item.GetDataKeyValue("FleetProfileAddInfDescription").ToString();
                                FleetAdditionalInfoDef.IsDeleted = false;
                                FPInfo.Add(FleetAdditionalInfoDef);
                                Session["FleetAdditionalInfoNew"] = FPInfo;
                            }
                            InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void PassengerAddInfo_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source,e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }
	
        }

        /// <summary>
        /// Method to Bind into "CrewNewAddlInfo" session, and 
        /// Call "CloseAndRebind" javascript function to Close and
        /// Rebind the selected record into parent page.
        /// </summary>
        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgPassengerAddInfo.SelectedItems.Count > 0)
                {
                    GridDataItem item = (GridDataItem)dgPassengerAddInfo.SelectedItems[0];

                    if (CheckIfExists(Convert.ToInt64(item.GetDataKeyValue("FleetProfileInformationID").ToString())))
                    {
                        lblMessage.Text = "Warning, Additional Info. Code Already Exists.";
                        //lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        if (CheckIfExists(Convert.ToInt64(item.GetDataKeyValue("FleetProfileInformationID").ToString())))
                        {
                            lblMessage.Text = "Warning, Additional Info. Code Already Exists.";
                            //lblMessage.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            bool IsExist = false;
                            List<FlightPakMasterService.GetFleetProfileDefinition> CrewDefinitionInfo = (List<FlightPakMasterService.GetFleetProfileDefinition>)Session["FleetProfileDefinition"];
                            for (int Index = 0; Index < CrewDefinitionInfo.Count; Index++)
                            {
                                if (CrewDefinitionInfo[Index].FleetInfoCD == item.GetDataKeyValue("FleetInfoCD").ToString())
                                {
                                    CrewDefinitionInfo[Index].InformationValue = string.Empty;
                                    CrewDefinitionInfo[Index].IsDeleted = false;
                                    IsExist = true;
                                }
                            }
                            Session["FleetProfileDefinition"] = CrewDefinitionInfo;
                            if (IsExist == false)
                            {
                                List<FlightPakMasterService.GetFleetProfileDefinition> FPInfo = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                                FlightPakMasterService.GetFleetProfileDefinition FleetAdditionalInfoDef = new FlightPakMasterService.GetFleetProfileDefinition();
                                FleetAdditionalInfoDef.FleetProfileInfoXRefID = 0;
                                FleetAdditionalInfoDef.FleetInfoCD = item.GetDataKeyValue("FleetInfoCD").ToString();
                                FleetAdditionalInfoDef.CustomerID = Convert.ToInt64(item.GetDataKeyValue("CustomerID").ToString());
                                FleetAdditionalInfoDef.FleetProfileInformationID = Convert.ToInt64(item.GetDataKeyValue("FleetProfileInformationID").ToString());
                                FleetAdditionalInfoDef.FleetDescription = item.GetDataKeyValue("FleetProfileAddInfDescription").ToString();
                                FleetAdditionalInfoDef.IsDeleted = false;
                                FPInfo.Add(FleetAdditionalInfoDef);
                                Session["FleetAdditionalInfoNew"] = FPInfo;
                            }
                            InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Function to Check if Crew Information Code alerady exists or not, by passing Crew Code
        /// </summary>
        /// <returns>True / False</returns>
        private bool CheckIfExists(Int64 InfoID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InfoID))
            {
                bool returnVal = false;
                if (Session["FleetProfileDefinition"] != null)
                {
                    List<FlightPakMasterService.GetFleetProfileDefinition> FleetAdditionalInfoDef = (List<FlightPakMasterService.GetFleetProfileDefinition>)Session["FleetProfileDefinition"];
                    var result = (from addInf in FleetAdditionalInfoDef where addInf.FleetProfileInformationID == InfoID && addInf.IsDeleted == false select addInf);
                    if (result.Count() > 0) { returnVal = true; }
                }
                return returnVal;
            }
        }

        protected void dgPassengerAddInfo_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPassengerAddInfo, Page.Session);
        }

    }
}
