﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="AircraftType.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.AircraftType"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
    <script type="text/javascript">
       
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">


            function openReport() {
                url = "../../Reports/ExportReportInformation.aspx?Report=RptDBAircraftTypeExport&UserCD=UC";
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, "RadExportData");
            }
            
            function ValidateEmptyTextbox(ctrlID, e) {

                if (ctrlID.value == "") {

                    ctrlID.value = "0.00";
                }
            }
            function openWin() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Fleet/AircraftTypePopUp.aspx?AircraftCD=" + document.getElementById("<%= tbAssociateTypeCode.ClientID%>").value + "&hdnAssociateTypeCode=" + document.getElementById("<%= hdnAssociateTypeCode.ClientID%>").value, "RadWindow1");
            }
            function ShowReports(radWin, ReportFormat, ReportName) {
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                if (ReportFormat == "EXPORT") {
                    url = "../../Reports/ExportReportInformation.aspx";
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    var oWnd = oManager.open(url, 'RadExportData');
                    return true;
                }
                else {
                    return true;
                }
            }
            function OnClientCloseExportReport(oWnd, args) {
                document.getElementById("<%=btnShowReports.ClientID%>").click();
            }
            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }
            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        if (arg.AircraftCD.indexOf(",") > 0) {
                            document.getElementById("<%=lbMultiple.ClientID%>").style.display = "inline";
                        }
                        else {
                            document.getElementById("<%=lbMultiple.ClientID%>").style.display = "none";
                        }
                        var AircraftCD = arg.AircraftCD.split(",");
                        document.getElementById("<%=tbAssociateTypeCode.ClientID%>").value = AircraftCD[0];
                        document.getElementById("<%=hdnAssociateTypeCode.ClientID%>").value = arg.AircraftID; // hdnAircraftId
                        document.getElementById("<%=cvAssociateTypeCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbAssociateTypeCode.ClientID%>").value = "";
                        document.getElementById("<%= hdnAssociateTypeCode.ClientID%>").value = "";
                    }
                }

            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function SetSelected(ctrl) {
                alert(ctrl);
                //document.Form1.ctrl.select();
            }
 
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel" runat="server">
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgAircraftTypeCatalog">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftTypeCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%-- <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftTypeCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSaveChanges1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftTypeCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftTypeCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnDelete">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgAircraftTypeCatalog">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftTypeCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftTypeCatalog" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbCode">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbCode" />
                        <telerik:AjaxUpdatedControl ControlID="tbDescription" />
                        <telerik:AjaxUpdatedControl ControlID="cvAircraftCode" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbAssociateTypeCode">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbAssociateTypeCode" />
                        <telerik:AjaxUpdatedControl ControlID="cvAssociateTypeCode" />
                        <telerik:AjaxUpdatedControl ControlID="tbDefaultPowerSetting" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbDefaultPowerSetting">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbDefaultPowerSetting" />
                        <telerik:AjaxUpdatedControl ControlID="tbWindAltitude" />
                        <telerik:AjaxUpdatedControl ControlID="rvDefaultPowerSetting" />
                        <telerik:AjaxUpdatedControl ControlID="lbDefaultPowerSetText" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                    Title="Aircraft Type Code" OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" VisibleStatusbar="false" Behaviors="Close" NavigateUrl="~/Views/Settings/Fleet/AircraftTypePopUp.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                    Title="Export Report Information" KeepInScreenBounds="true" AutoSize="false"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        
        <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                        <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
        </telerik:RadWindowManager>
        <br />
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <div class="tab-nav-top">
                        <span class="head-title">Aircraft Types</span> <span class="tab-nav-icons">
                            <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBAircraftType');"
                                OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                            <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBAircraftTypeExport');return false;"
                                OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                            <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                                Style="display: none;" />
                            <a href="../../Help/ViewHelp.aspx?Screen=AircraftTypeHelp" class="help-icon" target="_blank"
                                title="Help"></a>
                            <asp:HiddenField ID="hdnReportName" runat="server" />
                            <asp:HiddenField ID="hdnReportFormat" runat="server" />
                            <asp:HiddenField ID="hdnReportParameters" runat="server" />
                        </span>
                    </div>
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" id="table1" runat="server" class="head-sub-menu"
            width="100%">
            <tr>
                <td>
                    <div class="fleet_select">
                        <asp:LinkButton ID="lbCharterRates" CssClass="fleet_link" runat="server" Visible="false">Charter Rates</asp:LinkButton>
                        <asp:LinkButton ID="lbNewCharterRates" CssClass="fleet_link" OnClick="Charterrates_Click"
                            runat="server">Charter Rates</asp:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
        <div id="DivExternalForm" runat="server" class="dgpExternalForm">
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="True">
                <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                    <tr>
                        <td>
                            <div class="status-list">
                                <span>
                                    <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                        AutoPostBack="true" /></span>
                            </div>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="dgAircraftTypeCatalog" runat="server" AllowSorting="true" OnItemCreated="dgAircraftTypeCatalog_ItemCreated"
                    OnNeedDataSource="dgAircraftTypeCatalog_BindData" OnItemCommand="dgAircraftTypeCatalog_ItemCommand"
                    OnUpdateCommand="dgAircraftTypeCatalog_UpdateCommand" OnInsertCommand="dgAircraftTypeCatalog_InsertCommand"
                    OnDeleteCommand="dgAircraftTypeCatalog_DeleteCommand" AutoGenerateColumns="false"
                    OnItemDataBound="dgAircraftTypeCatalog_ItemDataBound" OnSelectedIndexChanged="dgAircraftTypeCatalog_SelectedIndexChanged"
                    OnPreRender="dgAircraftTypeCatalog_PreRender" OnPageIndexChanged="dgAircraftTypeCatalog_PageIndexChanged"
                    AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" AllowPaging="true"
                    PageSize="10" Height="341px">
                    <MasterTableView DataKeyNames="AircraftID,AircraftCD,AircraftDescription,AircraftTypeCD,ChargeRate,ChargeUnit,PowerSetting,PowerDescription,WindAltitude,PowerSettings1Description,PowerSettings1TrueAirSpeed,PowerSettings1HourRange,
             PowerSettings1TakeOffBias,PowerSettings1LandingBias,PowerSettings2Description,PowerSettings2TrueAirSpeed,PowerSettings2HourRange,PowerSettings2TakeOffBias,
             PowerSettings2LandingBias,PowerSettings3Description,PowerSettings3TrueAirSpeed,PowerSettings3HourRange,PowerSettings3TakeOffBias,PowerSettings3LandingBias,
             IsFixedRotary,ClientID,CQChargeRate,CQChargeUnit,PositionRate,PostionUnit,StandardCrew,StandardCrewRON,AdditionalCrew,AdditionalCrewRON,CharterQuoteWaitTM,
             LandingFee,CostBY,FixedCost,PercentageCost,TaxRON,AdditionalCrewTAX,IsCharterQuoteWaitTAX,LandingTAX,DescriptionRON,AdditionalCrewDescription,MinDailyREQ,LastUpdUID,
             LastUpdTS,AircraftSize,IntlStdCrewNUM,DomesticStdCrewNUM,MinimumDayUseHRS,DailyUsageAdjTAX,LandingFeeTAX,IsInActive,MarginalPercentage"
                        CommandItemDisplay="Bottom" Width="100%">
                        <Columns>
                            <telerik:GridBoundColumn DataField="AircraftID" HeaderText="AircraftID" AutoPostBackOnFilter="false"
                                AllowFiltering="true" CurrentFilterFunction="EqualTo" ShowFilterIcon="false"
                                Display="false" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Aircraft Type Code" AutoPostBackOnFilter="false"
                                AllowFiltering="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="false"
                                HeaderStyle-Width="100px" FilterControlWidth="80px" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AircraftDescription" HeaderText="Aircraft Description"
                                AutoPostBackOnFilter="false" AllowFiltering="true" CurrentFilterFunction="StartsWith"
                                ShowFilterIcon="false" HeaderStyle-Width="560px" FilterControlWidth="540px" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AircraftDescription" HeaderText="Aircraft Type"
                                AutoPostBackOnFilter="false" AllowFiltering="true" CurrentFilterFunction="StartsWith"
                                Display="false" ShowFilterIcon="false" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AircraftTypeCD" HeaderText="AircraftTypeCode"
                                AutoPostBackOnFilter="true" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                                ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                            </telerik:GridCheckBoxColumn>
                        </Columns>
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px; float: left; clear: both;">
                                <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                    Visible='<%# IsAuthorized(Permission.Database.AddAircraftType)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                    ToolTip="Edit" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditAircraftType)%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                    runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeleteAircraftType)%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                            </div>
                            <div>
                                <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                            </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings CaseSensitive="false"></GroupingSettings>
                </telerik:RadGrid>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="tdLabel160">
                            <div id="tdSuccessMessage" class="success_msg">
                                Record saved successfully.</div>
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table class="border-box">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel80">
                                        <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel130" valign="top">
                                        <span class="mnd_text">Aircraft Type Code</span>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbCode" runat="server" MaxLength="10" ValidationGroup="save" OnTextChanged="tbCode_TextChanged"
                                                        AutoPostBack="true" CssClass="text50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="Code is Required"
                                                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbCode" CssClass="alert-text"
                                                        ValidationGroup="save">Aircraft Type Code is Required.</asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvAircraftCode" runat="server" ControlToValidate="tbCode"
                                                        ErrorMessage="Unique Aircraft Type Code is Required" Display="Dynamic" CssClass="alert-text"
                                                        ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel130" valign="top">
                                        <span class="mnd_text">Description</span>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbDescription" runat="server" MaxLength="15" CssClass="text200"
                                                        ValidationGroup="save"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel130">
                                        Associated Type Code
                                    </td>
                                    <td class="tdLabel220">
                                        <asp:TextBox ID="tbAssociateTypeCode" runat="server" CssClass="text120" AutoPostBack="true"
                                            OnTextChanged="tbAssociatetype_TextChanged"></asp:TextBox>
                                        <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin();return false;"
                                            Enabled="false" />
                                    </td>
                                    <td class="tdLabel130 mnd_text">
                                        Default Power Setting
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbDefaultPowerSetting" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                            AutoPostBack="true" MaxLength="1" CssClass="text120" ValidationGroup="save" OnTextChanged="tbDefaultPowerSetting_TextChanged"
                                            onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel130">
                                    </td>
                                    <td class="tdLabel220">
                                        <asp:CustomValidator ID="cvAssociateTypeCode" runat="server" ControlToValidate="tbAssociateTypeCode"
                                            ErrorMessage="Invalid Associated Type Code." Display="Dynamic" CssClass="alert-text"
                                            SetFocusOnError="true" ValidationGroup="save"></asp:CustomValidator>
                                        <asp:HiddenField ID="hdnAssociateTypeCode" runat="server" />
                                        <asp:Label ID="lbMultiple" runat="server" Text="(Multiple)" CssClass="input_no_bg"
                                            Style="display: none;"></asp:Label>
                                    </td>
                                    <td class="tdLabel130">
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="reqDefaultPowerSetting" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbDefaultPowerSetting" Display="Dynamic" CssClass="alert-text"
                                            SetFocusOnError="true" ErrorMessage="Default Power Setting is Required"></asp:RequiredFieldValidator>
                                        <asp:RangeValidator ID="rvDefaultPowerSetting" runat="server" ControlToValidate="tbDefaultPowerSetting"
                                            Display="Dynamic" ValidationGroup="save" ErrorMessage="Enter 1 or 2 or 3" MinimumValue="1"
                                            CssClass="alert-text" MaximumValue="3" ToolTip="Value should be between 1 and 3."
                                            Type="Integer"></asp:RangeValidator>
                                        <asp:Label ID="lbDefaultPowerSetText" runat="server" CssClass="input_no_bg"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel130" valign="top">
                                        <span class="mnd_text">Max. Wind Altitude</span>
                                        <%--  <asp:Label ID="lbWindAltitude" runat="server" Text="Max Wind Altitude"></asp:Label>--%>
                                    </td>
                                    <td class="tdLabel220" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbWindAltitude" runat="server" MaxLength="5" onKeyPress="return fnAllowNumeric(this, event)"
                                                        onBlur="return RemoveSpecialChars(this)" CssClass="text120" ToolTip="Value indicated in Feet">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="reqWindAltitude" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbWindAltitude" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Max Wind Altitude is Required"></asp:RequiredFieldValidator>
                                                    <asp:RangeValidator ID="rvWindAltitude" runat="server" ControlToValidate="tbWindAltitude"
                                                        ValidationGroup="save" CssClass="alert-text" ErrorMessage="Invalid Format" Display="Dynamic"
                                                        Type="Integer" MinimumValue="0" MaximumValue="99999"></asp:RangeValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel130" valign="top">
                                        <asp:Label ID="lbChargeRate" runat="server" Text="Charge Rate"></asp:Label>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="pr_radtextbox_180">
                                                    <%-- <asp:TextBox ID="tbChargeRate" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        ValidationGroup="save" onBlur="return ValidateEmptyTextbox(this, event)" MaxLength="17"
                                                        CssClass="text120" Text="00000000000000.00">
                                                    </asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbChargeRate" runat="server" Type="Currency" Culture="en-US"
                                                        MaxLength="14" Value="0.00" NumberFormat-DecimalSeparator="." ValidationGroup="save"
                                                        EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revchargeRate" runat="server" Display="Dynamic"
                                                        ErrorMessage="Invalid Format" ControlToValidate="tbChargeRate" CssClass="alert-text"
                                                        ValidationExpression="^[0-9]{0,14}(\.[0-9]{1,2})?$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel130" valign="top">
                                        <asp:Label ID="lbFixedorRotary" runat="server" CssClass="mnd_text" Text="Fixed/Rotary"></asp:Label>
                                    </td>
                                    <td class="tdLabel220">
                                        <asp:RadioButton ID="rbfixed" runat="server" Text="Fixed" GroupName="fixedorrotary"
                                            Checked="true" Font-Size="12px" />
                                        <asp:RadioButton ID="rbRotary" runat="server" Text="Rotary" GroupName="fixedorrotary"
                                            Font-Size="12px" />
                                    </td>
                                    <td class="tdLabel130">
                                        <asp:Label ID="lbChargeUnit" runat="server" Text="Charge Unit"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rbHours" runat="server" Text="Hours" GroupName="ChargeUnit"
                                            Checked="true" />
                                        <asp:RadioButton ID="rbNautical" runat="server" Text="Nautical" GroupName="ChargeUnit" />
                                        <asp:RadioButton ID="rbStatute" runat="server" Text="Statute" GroupName="ChargeUnit" />
                                        <asp:RadioButton ID="rbKilometers" runat="server" Text="Kilometers" GroupName="ChargeUnit" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel130" valign="top">
                                        Margin Percentage
                                    </td>
                                    <td class="tdLabel220">
                                        <asp:TextBox ID="tbMarginPercentage" runat="server" Text="0.0" MaxLength="8" ValidationGroup="Save"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                            CssClass="text80"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:RegularExpressionValidator ID="revtbMarginPercentage" runat="server" Display="Dynamic"
                                ErrorMessage="Invalid Format (NNNNNNN.NN)" ControlToValidate="tbMarginPercentage"
                                CssClass="alert-text" ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="top">
                                        <div class="tblspace_10" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr class="aircraft_table_row">
                                    <td class="tdLabel130">
                                        Power Setting
                                    </td>
                                    <td class="tdLabel150">
                                        <span class="mnd_text">True Air Speed</span>
                                    </td>
                                    <td class="tdLabel150">
                                        <span class="mnd_text">Hour Range</span>
                                    </td>
                                    <td class="tdLabel150">
                                        T/O Bias
                                    </td>
                                    <td>
                                        Lnd Bias
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tablepadding5" valign="top">
                                        <asp:Label ID="lbConstantMach" Text="1 - Constant Mach" runat="server"></asp:Label>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel50">
                                                    <asp:TextBox ID="tbPS1TAS" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                        MaxLength="3" CssClass="text40" ValidationGroup="save">
                                                    </asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;NM/hr.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="reqPS1TAS" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS1TAS" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="True Air Speed for Constant Mach  is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revPS1TAS" runat="server" ErrorMessage="Enter only numeric"
                                                        Display="Dynamic" ControlToValidate="tbPS1TAS" CssClass="alert-text" ValidationExpression="^\d+$"
                                                        ValidationGroup="save"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel50">
                                                    <asp:TextBox ID="tbPS1HRRNG" runat="server" CssClass="tdLabel50" MaxLength="5" onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;hrs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="reqPS1HRRNG" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS1HRRNG" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Hour Range for Constant Mach  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPS1HRRNG" runat="server" ControlToValidate="tbPS1HRRNG"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel50">
                                                    <asp:TextBox ID="tbPS1TOBIAS" runat="server" onfocus="this.select();" MaxLength="5"
                                                        CssClass="text50"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;hrs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="rfvPS1TOBIAS" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS1TOBIAS" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage=" T/O Bias for Constant Mach  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPS1TOBIAS" runat="server" ControlToValidate="tbPS1TOBIAS"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel50">
                                                    <asp:TextBox ID="tbPS1LNDBIAS" runat="server" onfocus="this.select();" MaxLength="5"
                                                        CssClass="text50"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;hrs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="rfvPS1LNDBIAS" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS1LNDBIAS" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Lnd Bias  for Constant Mach  is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPS1LNDBIAS" runat="server" ControlToValidate="tbPS1LNDBIAS"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tablepadding5" valign="top">
                                        <asp:Label ID="lbLongRangeCruise" Text="2 - Long Range Cruise" runat="server"></asp:Label>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top" class="tdLabel50">
                                                    <asp:TextBox ID="tbPS2TAS" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                        onBlur="return RemoveSpecialChars(this)" MaxLength="3" CssClass="text40" ValidationGroup="save">
                                                    </asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;NM/hr.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="reqPS2TAS" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS2TAS" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="True Air Speed for Long Range Cruise is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revPS2TAS" runat="server" ErrorMessage="Enter only numeric"
                                                        Display="Dynamic" ControlToValidate="tbPS2TAS" CssClass="alert-text" ValidationExpression="^\d+$"
                                                        ValidationGroup="save"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0" style="border: 1">
                                            <tr>
                                                <td valign="top" class="tdLabel50">
                                                    <asp:TextBox ID="tbPS2HRRNG" CssClass="text50" runat="server" onfocus="this.select();"
                                                        MaxLength="5"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;hrs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="rfvPS2HRRNG" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS2HRRNG" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Hour Range for Long Range Cruise Constant is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPS2HRRNG" runat="server" ControlToValidate="tbPS2HRRNG"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top" class="tdLabel50">
                                                    <asp:TextBox ID="tbPS2TOBIAS" runat="server" onfocus="this.select();" MaxLength="5"
                                                        CssClass="text50"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;hrs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="rfvPS2TOBIAS" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS2TOBIAS" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="T/O Bias for Long Range Cruise Constant is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPS2TOBIAS" runat="server" ControlToValidate="tbPS2TOBIAS"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top" class="tdLabel50">
                                                    <asp:TextBox ID="tbPS2LNDBIAS" runat="server" onfocus="this.select();" MaxLength="5"
                                                        CssClass="text50"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;hrs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="rfvPS2LNDBIAS" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS2LNDBIAS" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Lnd Bias  for Long Range Cruise Constant is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPS2LNDBIAS" runat="server" ControlToValidate="tbPS2TOBIAS"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tablepadding5" valign="top">
                                        <asp:Label ID="lbHighSpeedCruise" Text="3 - High Speed Cruise" runat="server"></asp:Label>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top" class="tdLabel50">
                                                    <asp:TextBox ID="tbPS3TAS" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                        MaxLength="3" CssClass="text40" ValidationGroup="save">
                                                    </asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;NM/hr.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="reqPS3TAS" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS3TAS" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="True Air Speed for High Speed Cruise is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revPS3TAS" runat="server" ErrorMessage="Enter only numeric"
                                                        Display="Dynamic" ControlToValidate="tbPS3TAS" CssClass="alert-text" ValidationExpression="^\d+$"
                                                        ValidationGroup="save"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0" style="border: 1">
                                            <tr>
                                                <td valign="top" class="tdLabel50">
                                                    <asp:TextBox ID="tbPS3HRRNG" runat="server" onfocus="this.select();" MaxLength="5"
                                                        CssClass="text50"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;hrs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="rfvPS3HRRNG" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS3HRRNG" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Hour Range for High Speed Cruise is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPS3HRRNG" runat="server" ControlToValidate="tbPS3HRRNG"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top" class="tdLabel50">
                                                    <asp:TextBox ID="tbPS3TOBIAS" runat="server" MaxLength="5" CssClass="text50" onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;hrs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="rfvPS3TOBIAS" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS3TOBIAS" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="T/O Bias  for High Speed Cruise is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPS3TOBIAS" runat="server" ControlToValidate="tbPS3HRRNG"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tablepadding5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top" class="tdLabel50">
                                                    <asp:TextBox ID="tbPS3LNDBIAS" runat="server" MaxLength="5" CssClass="text50" onfocus="this.select();"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    &nbsp;hrs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RequiredFieldValidator ID="rfvPS3LNDBIAS" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbPS3LNDBIAS" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                        ErrorMessage="Lnd Bias  for High Speed Cruise is Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPS3LNDBIAS" runat="server" ControlToValidate="tbPS3HRRNG"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnSaveChanges" CssClass="button" Text="Save" runat="server" OnClick="SaveChanges_Click"
                                ValidationGroup="save" />
                            <asp:HiddenField ID="hdnRedirect" runat="server" />
                            <asp:Button ID="btnCancel" CssClass="button" OnClick="Cancel_Click" Text="Cancel"
                                runat="server" CausesValidation="false" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                            <asp:HiddenField ID="hdnAircraftId" runat="server" />
                        </td>
                    </tr>
                </table>
                <div id="toolbar" class="scr_esp_down_db">
                    <div class="floating_main_db">
                        <div class="floating_bar_db">
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                            <span class="padright_20">
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                            <span>
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
                <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
