﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="AircraftDutyType.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.AircraftDutyType"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function validateHourAndMinute(oSrc, args) {
            var array = args.Value.split(":");
            if (array[0] > 23 && array[1] > 59) {
                args.IsValid = false;
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    oSrc.textContent = "Hour entry must be 0-23; Minute entry must be 0-59";
                } else {
                    oSrc.innerText = "Hour entry must be 0-23; Minute entry must be 0-59";
                }
                if (oSrc.id.indexOf("tbStart") != -1) {
                    setTimeout("document.getElementById('<%=tbStart.ClientID%>').focus()", 0);
                }
                else if (oSrc.id.indexOf("tbEnd") != -1) {
                    setTimeout("document.getElementById('<%=tbEnd.ClientID%>').focus()", 0);
                }
                return false;
            }
            else if ((array[0] > 23)) {
                args.IsValid = false;
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    oSrc.textContent = "Hour entry must be 0-23";
                } else {
                    oSrc.innerText = "Hour entry must be 0-23";
                }
                if (oSrc.id.indexOf("tbStart") != -1) {
                    setTimeout("document.getElementById('<%=tbStart.ClientID%>').focus()", 0);
                }
                else if (oSrc.id.indexOf("tbEnd") != -1) {
                    setTimeout("document.getElementById('<%=tbEnd.ClientID%>').focus()", 0);
                }
                return false;
            }
            else {
                if (array[1] > 59) {
                    args.IsValid = false;
                    if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                        oSrc.textContent = "Minute entry must be 0-59";
                    } else {
                        oSrc.innerText = "Minute entry must be 0-59";
                    }
                    if (oSrc.id.indexOf("tbStart") != -1) {
                        setTimeout("document.getElementById('<%=tbStart.ClientID%>').focus()", 0);
                    }
                    else if (oSrc.id.indexOf("tbEnd") != -1) {
                        setTimeout("document.getElementById('<%=tbEnd.ClientID%>').focus()", 0);
                    }
                    return false;
                }
            }
        }
        function FocusCallBackFn() {
            setTimeout("document.getElementById('<%=tbStart.ClientID%>').focus()", 0);
            return false;
        }
        function forecolorChange(sender, args) {

            $get("<%=tbForeColor.ClientID%>").value = sender.get_selectedColor();
            $get("<%=hdnForeColor.ClientID%>").value = sender.get_selectedColor();

        }
        function backcolorChange(sender, args) {

            $get("<%=tbBackColor.ClientID%>").value = sender.get_selectedColor();
            $get("<%=hdnBackColor.ClientID%>").value = sender.get_selectedColor();
        }

        function CheckTxtBox(control) {
            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);
                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);
                } return false;
            }
        }
        
        function ShowReports(radWin, ReportFormat, ReportName) {
            document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
            document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
            if (ReportFormat == "EXPORT") {
                url = "../../Reports/ExportReportInformation.aspx";
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, 'RadExportData');
                return true;
            }
            else {
                return true;
            }
        }
        function OnClientCloseExportReport(oWnd, args) {
            document.getElementById("<%=btnShowReports.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgAircraftDutyTypes">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAircraftDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAircraftDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgAircraftDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgAircraftDutyTypes">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAircraftDutyTypes" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="tbDescription" />
                    <telerik:AjaxUpdatedControl ControlID="cvCode" />
                    <telerik:AjaxUpdatedControl ControlID="rfvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function openReport() {

                url = "../../Reports/ExportReportInformation.aspx?Report=RptDBAircraftDutyExport&UserCD=UC";

                // url = "../../Reports/ExportReportInformation.aspx?Report=RptDBAircraftDuty";

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, "RadExportData");
            }
            
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExportReport" Title="Export Report Information" KeepInScreenBounds="true"
                AutoSize="false" Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Aircraft Duty Types</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBAircraftDuty');"
                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBAircraftDuty');return false;"
                            OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                        <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                            Style="display: none;" />
                        <a href="../../Help/ViewHelp.aspx?Screen=AircraftDutyType" class="help-icon" target="_blank"
                            title="Help"></a>
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                    </span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgAircraftDutyTypes" runat="server" OnItemCreated="dgAircraftDutyTypes_ItemCreated"
                OnNeedDataSource="dgAircraftDutyTypes_BindData" OnItemCommand="dgAircraftDutyTypes_ItemCommand"
                OnUpdateCommand="dgAircraftDutyTypes_UpdateCommand" OnInsertCommand="dgAircraftDutyTypes_InsertCommand"
                OnDeleteCommand="dgAircraftDutyTypes_DeleteCommand" OnSelectedIndexChanged="dgAircraftDutyTypes_SelectedIndexChanged"
                OnItemDataBound="dgAircraftDutyTypes_ItemDataBound" OnPreRender="dgAircraftDutyTypes_PreRender"
                OnPageIndexChanged="dgAircraftDutyTypes_PageIndexChanged" Height="341px">
                <MasterTableView DataKeyNames="AircraftDutyID,AircraftDutyCD,AircraftDutyDescription,ClientID,ForeGrndCustomColor,BackgroundCustomColor,IsCalendarEntry,IsAircraftStandby,IsDeleted,DefaultStartTM,DefualtEndTM,LastUpdUID,LastUpdTS,IsInActive"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="AircraftDutyID" HeaderText="AircraftDutyID" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="EqualTo" Display="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AircraftDutyCD" HeaderText="Aircraft Duty Code"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="100px" FilterControlWidth="80px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AircraftDutyDescription" HeaderText="Description"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="560px" FilterControlWidth="540px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%#  IsUIReports && IsAuthorized(Permission.Database.AddAircraftDutyType)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                ToolTip="Edit" CommandName="Edit" Visible='<%# IsUIReports && IsAuthorized(Permission.Database.EditAircraftDutyType)%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsUIReports && IsAuthorized(Permission.Database.DeleteAircraftDutyType)%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel150" valign="top">
                                    <asp:CheckBox ID="chkQuickCalendarEntry" runat="server" Text="Quick Calendar Entry"
                                        TabIndex="9" />
                                </td>
                                <td class="tdLabel140" valign="top">
                                    <asp:CheckBox ID="chkAircraftStandBy" runat="server" Text="Aircraft Standby" TabIndex="10" />
                                </td>
                                <td valign="top">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" TabIndex="11" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top" class="tdLabel120">
                                    <span class="mnd_text">Aircraft Duty Code</span>
                                </td>
                                <td class="tdLabel250">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCode" runat="server" MaxLength="2" CssClass="text50" ValidationGroup="save"
                                                    OnTextChanged="Code_TextChanged" AutoPostBack="true" TabIndex="12"></asp:TextBox>
                                                <asp:HiddenField ID="hdnAircraftDutyID" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="save" ControlToValidate="tbCode"
                                                    Display="Dynamic" CssClass="alert-text" SetFocusOnError="true" ErrorMessage="Aircraft Duty Code is Required"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvCode" runat="server" ValidationGroup="save" ControlToValidate="tbCode"
                                                    Display="Dynamic" CssClass="alert-text" SetFocusOnError="true" ErrorMessage="Unique Aircraft Duty Code is Required"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel100" valign="top">
                                    Start Time
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top">
                                                <telerik:RadMaskedTextBox ID="tbStart" runat="server" SelectionOnFocus="SelectAll"
                                                    ValidationGroup="save" Mask="<0..99>:<0..99>" CssClass="RadMaskedTextBox50" TabIndex="14">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="reqtbStart" runat="server" ValidationGroup="save" ControlToValidate="tbStart"
                                                    CssClass="alert-text" Display="Dynamic" ClientValidationFunction="validateHourAndMinute"
                                                    SetFocusOnError="true" ErrorMessage="">
                                                </asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top" class="tdLabel120">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td class="tdLabel250" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" runat="server" MaxLength="25" CssClass="text200"
                                                    ValidationGroup="save" TabIndex="13"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                    ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel100" valign="top">
                                    End Time
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top">
                                                <telerik:RadMaskedTextBox ID="tbEnd" runat="server" SelectionOnFocus="SelectAll"
                                                    ValidationGroup="save" Mask="<0..99>:<0..99>" CssClass="RadMaskedTextBox50" TabIndex="15">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="reqtbEnd" runat="server" ValidationGroup="save" ControlToValidate="tbEnd"
                                                    CssClass="alert-text" Display="Dynamic" ClientValidationFunction="validateHourAndMinute"
                                                    SetFocusOnError="true">
                                                </asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" valign="top">
                                    Set Calendar Colors
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120">
                                    Foreground Color
                                </td>
                                <td>
                                    <asp:TextBox ID="tbForeColor" runat="server" MaxLength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                        ReadOnly="true" Text="" CssClass="text60" TabIndex="16"></asp:TextBox>
                                    <asp:HiddenField ID="hdnForeColor" runat="server" />
                                    &nbsp;&nbsp;
                                </td>
                                <td>
                                    <telerik:RadColorPicker runat="server" ID="rcpForeColor" OnClientColorChange="forecolorChange"
                                        ShowIcon="true" ZIndex="90000" RadComboBoxImagePosition="Right" TabIndex="17" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120">
                                    Background Color
                                </td>
                                <td>
                                    <asp:TextBox ID="tbBackColor" runat="server" MaxLength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                        ReadOnly="true" Text="" CssClass="text60" TabIndex="18"></asp:TextBox>
                                    <asp:HiddenField ID="hdnBackColor" runat="server" />
                                    &nbsp;&nbsp;
                                </td>
                                <td valign="top">
                                    <telerik:RadColorPicker runat="server" ID="rcpBackColor" OnClientColorChange="backcolorChange"
                                        ShowIcon="true" ZIndex="90000" RadComboBoxImagePosition="Right" TabIndex="19" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" CssClass="button" ValidationGroup="save"
                            OnClick="SaveChanges_Click" TabIndex="20" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClick="Cancel_Click"
                            CausesValidation="false" TabIndex="21" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnGridEnable" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
