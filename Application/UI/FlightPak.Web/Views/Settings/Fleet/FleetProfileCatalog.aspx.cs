﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.IO;
using FlightPak.Web.Framework.Prinicipal;
using System.Reflection;
using System.Text.RegularExpressions;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetProfileCatalog : BaseSecuredPage
    {
        protected Label lblFile;
        private ExceptionManager exManager;
        //protected System.Web.UI.WebControls.Label lblInfo;
        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }
        string DateFormat = "MM/dd/yyyy";
        
        public List<string> AircraftCodes = new List<string>();
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewFleetProfileReports);
                        // To Assign Date Format from User Control, based on Company Profile 
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }
                        if (UserPrincipal.Identity._fpSettings._IsAPISSupport != null)
                        {
                            if (UserPrincipal.Identity._fpSettings._IsAPISSupport == true)
                            {
                                lbDescription.CssClass = "important-text";
                                lbEmergencyContact.CssClass = "important-text";
                            }
                            else
                            {
                                lbDescription.ForeColor = System.Drawing.Color.Black;
                                lbEmergencyContact.ForeColor = System.Drawing.Color.Black;
                            }
                        }
                        else
                        {
                            lbDescription.ForeColor = System.Drawing.Color.Black;
                            lbEmergencyContact.ForeColor = System.Drawing.Color.Black;
                        }
                        ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFleetProfile, dgFleetProfile, this.Page.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                        //store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFleetProfile.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewFleetProfile);
                            // Method to display first record in read only format   
                            if (Request.QueryString["FleetID"] != null && Request.QueryString["FleetID"].ToString() != "")
                            {
                                Session["FPSelectedItem"] = Request.QueryString["FleetID"];
                                SetControlState();
                            }
                            if (!string.IsNullOrEmpty(Request.QueryString["ScTailNum"]))
                            {
                                using (MasterCatalogServiceClient svc = new MasterCatalogServiceClient())
                                {
                                    var fleet = svc.GetFleetByTailNumber(Convert.ToString(Request.QueryString["ScTailNum"])).EntityInfo;
                                    if (fleet != null)
                                    {
                                        hdnScalendar.Value = "Sc";
                                        Session["FPSelectedItem"] = fleet.FleetID;// fleet[0].FleetID;
                                        DefaultSelection();

                                    }
                                }
                            }
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                BindAircraftClassCombo();
                                BindAircraftTypeCombo();
                                dgFleetProfile.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                EnableForm(false);
                                //Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                                {
                                    chkHomeBaseOnly.Checked = true;
                                }
                                chkActiveOnly.Checked = true;
                                BindAircraftClassCombo();
                                BindAircraftTypeCombo();
                                // DefaultSelection();
                            }
                            
                           
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                            CreateDictionayForImgUpload();

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }

        protected void dgAircraftType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFleetProfile.ClientSettings.Scrolling.ScrollTop = "0";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> DicImgage = new Dictionary<string, byte[]>();
                Session["DicImg"] = DicImgage;
                Dictionary<string, string> DicImageDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = DicImageDelete;
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //lblError.Text = "";
                        if (!IsPostBack)
                        {
                            if (Request.QueryString["FleetID"] != null && Request.QueryString["FleetID"].ToString() != "")
                            {
                                SelectFltItm(Request.QueryString["FleetID"].ToString());
                                if (Session["IsEdit"] != null)
                                {
                                    if ("YES" == Session["IsEdit"].ToString())
                                    {
                                        EnableForm(false);
                                    }
                                    else
                                    {
                                        EnableForm(false);
                                    }
                                }
                                else
                                {
                                    EnableForm(false);
                                }
                            }
                            //else if (Session["SearchItemPrimaryKeyValue"] == null)
                            //{
                            //    DefaultSelection();
                            //}
                            //if (Request.QueryString["FleetID"] != null && Request.QueryString["FleetID"].ToString() != "")
                            //{
                            //    PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
                            //    isreadonly.SetValue(this.Request.QueryString, false, null);
                            //    this.Request.QueryString.Remove("FleetID");
                            //}
                            if (Session["SearchItemPrimaryKeyValue"] == null)
                            {
                                DefaultSelection();
                            }
                            else
                            {
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                        }
                        else
                        {
                            string EventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (EventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                            else if (EventArgument == "DeleteImage_Click")
                            {
                                DeleteImage_Click(sender, e);
                            }
                            else if (ViewState["EditFleetID"] != null && ViewState["EditFleetID"].ToString() != "")
                            {
                                DefaultSelection();
                            }
                            else if (ViewState["RefreshGrid"] != null && ViewState["RefreshGrid"].ToString() != "")
                            {
                                DefaultSelection();
                            }
                        }

                        if (!IsPostBack && IsPopUp)
                        {
                            pnlGrid.Visible = false;
                            if (IsAdd)
                            {
                                (dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                            }
                            else
                            {
                                (dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFleetProfile.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFleetProfile.Rebind();
                    SelectFleetItem();
                    ReadOnlyForm();
                }
            }

            if (dgFleetProfile.MasterTableView.Items.Count > 0)
            {
                SelectFleetItem();
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgFleetProfile.Items.Count > 0)
                    {

                        if (Session["SearchItemPrimaryKeyValue"] == null)
                        {
                            dgFleetProfile.SelectedIndexes.Add(0);
                            //GridDataItem Item = (GridDataItem)dgFleetProfile.SelectedItems[0];
                        }

                        //if (!IsPostBack)
                        //{
                        //    Session["FPSelectedItem"] = null;
                        //}
                        if (Session["FPSelectedItem"] == null)
                        {
                            dgFleetProfile.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                            {
                                Session["FPSelectedItem"] = dgFleetProfile.Items[0].GetDataKeyValue("FleetID").ToString();

                            }

                            Session["aircraftCode"] = dgFleetProfile.Items[0].GetDataKeyValue("AircraftCD").ToString();
                            Session["FleetID"] = dgFleetProfile.Items[0].GetDataKeyValue("FleetID").ToString();
                            hdnFleetID.Value = dgFleetProfile.Items[0].GetDataKeyValue("FleetID").ToString();
                            Session.Remove("FleetProfileDefinition");
                            Label lblUser = (Label)dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (dgFleetProfile.Items[0].GetDataKeyValue("LastUpdUID") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + dgFleetProfile.Items[0].GetDataKeyValue("LastUpdUID").ToString());
                            }
                            else
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            }
                            if (dgFleetProfile.Items[0].GetDataKeyValue("LastUpdTS") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(dgFleetProfile.Items[0].GetDataKeyValue("LastUpdTS").ToString())));
                            }
                        }
                        else
                        {
                            hdnFleetID.Value = Session["FPSelectedItem"].ToString();
                            SelectItem(Session["FPSelectedItem"].ToString());
                        }

                        if (dgFleetProfile.SelectedIndexes.Count == 0)
                            dgFleetProfile.SelectedIndexes.Add(0);
                        
                        //chkActiveOnly.Enabled = true;
                        //chkRotary.Enabled = true;
                        //chkFixedWing.Enabled = true;
                        //DisplayEditForm();
                        ReadOnlyForm();
                        EnableForm(false);
                        ViewState["EditFleetID"] = null;
                        ViewState["RefreshGrid"] = null;
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void btnSaveTop_Click(object sender, EventArgs e)
        {
            if (hdnSave.Value != "Update")
            {
                bool IsValidate = true;
                if (tbaircraftcode.Text.Trim() == "")
                {
                    rfvaircraftcode.IsValid = false;
                    IsValidate = false;
                }
                if (tbtailnumber.Text.Trim() == "")
                {
                    rfvtailnumber.IsValid = false;
                    IsValidate = false;
                }
                if (CheckFleetCodeExists())
                {
                    cvAircraftCode.IsValid = false;
                    RadAjaxManager1.FocusControl(tbaircraftcode.ClientID);
                    IsValidate = false;
                    rfvaircraftcode.IsValid = true;
                }
                if (CheckFleetTailNumExists())
                {
                    cvTailNum.IsValid = false;
                    RadAjaxManager1.FocusControl(tbtailnumber.ClientID);
                    IsValidate = false;
                    rfvtailnumber.IsValid = true;
                }
                if (IsValidate)
                {
                    SaveFleet();
                }
            }
            else
            {
                SaveFleet();
            }
        }
        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            if (hdnSave.Value != "Update")
            {
                bool IsValidate = true;
                if (CheckFleetCodeExists())
                {
                    rfvaircraftcode.IsValid = false;
                    RadAjaxManager1.FocusControl(tbaircraftcode.ClientID);
                    IsValidate = false;
                }
                if (CheckFleetTailNumExists())
                {
                    rfvtailnumber.IsValid = false;
                    RadAjaxManager1.FocusControl(tbtailnumber.ClientID);
                    IsValidate = false;
                }
                if (IsValidate)
                {
                    SaveFleet();
                }
            }
            else
            {
                SaveFleet();
            }
        }
        private void SaveFleet()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs("SaveFleet"))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1) || (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1))
                        {
                            e.Updated = dgFleetProfile;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void dgFleetProfile_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton lbtnEditButton = ((e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton);
                            //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton lbtnInsertButton = ((e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton);
                            //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        /// <summary>
        /// Bind Fleet Type Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetProfile_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string IsFixedRotary = "B";
                        bool IsInActive = false;
                        Int64 HomeBaseID = 0;
                        if (chkRotary.Checked && !chkFixedWing.Checked)
                        {
                            IsFixedRotary = "R";
                        }
                        else if (!chkRotary.Checked && chkFixedWing.Checked)
                        {
                            IsFixedRotary = "F";
                        }
                        if (chkActiveOnly.Checked)
                        {
                            IsInActive = true;
                        }
                        if (chkHomeBaseOnly.Checked)
                        {
                            if (UserPrincipal != null && UserPrincipal.Identity._homeBaseId != null)
                            {
                                HomeBaseID = Convert.ToInt64(UserPrincipal.Identity._homeBaseId);
                            }
                        }
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (IsPopUp)
                                //Get All the records for popup
                                dgFleetProfile.DataSource = Service.GetFleetSearch(false, "B", HomeBaseID).EntityList.ToList();
                            else
                                dgFleetProfile.DataSource = Service.GetFleetSearch(IsInActive, IsFixedRotary, HomeBaseID).EntityList.ToList();
                            // DefaultSelection();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private void BindData()
        {
            string IsFixedRotary = "B";
            bool IsInActive = false;
            Int64 HomeBaseID = 0;
            if (chkRotary.Checked && !chkFixedWing.Checked)
            {
                IsFixedRotary = "R";
            }
            else if (!chkRotary.Checked && chkFixedWing.Checked)
            {
                IsFixedRotary = "F";
            }
            if (chkActiveOnly.Checked)
            {
                IsInActive = true;
            }
            if (chkHomeBaseOnly.Checked)
            {
                if (UserPrincipal != null && UserPrincipal.Identity._homeBaseId != null)
                {
                    HomeBaseID = Convert.ToInt64(UserPrincipal.Identity._homeBaseId);
                }
            }
            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                dgFleetProfile.DataSource = Service.GetFleetSearch(IsInActive, IsFixedRotary, HomeBaseID).EntityList.ToList();
                dgFleetProfile.Rebind();
                DefaultSelection();
            }
            if (dgFleetProfile.Items.Count >= 1)
            {
                (dgFleetProfile.Items[0] as GridDataItem).FireCommandEvent("Select", string.Empty);
            }
            if (dgFleetProfile.Items.Count != 0)
                EnableNavLinks(true);
            else
                EnableNavLinks(false);
        }
        protected void chkActiveOnly_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }
        protected void chkRotary_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }
        protected void chkFixedWing_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }
        protected void chkHomeBaseOnly_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }
        protected void dgFleetProfile_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["CommandName"] = string.Empty;

                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    //if (Session["FPSelectedItem"] != null)
                                    var ReturnValue = CommonService.Lock(EntitySet.Database.FleetProfile, Convert.ToInt64(String.IsNullOrWhiteSpace(Convert.ToString(Session["FPSelectedItem"])) ? "0" : Convert.ToString(Session["FPSelectedItem"]).Trim()));
                                    // Session["IsAirportEditLock"] = "True";
                                    if (!ReturnValue.ReturnFlag)
                                    {
                                        if (IsPopUp)
                                            ShowLockAlertPopup(ReturnValue.LockMessage, ModuleNameConstants.Database.FleetProfile);
                                        else
                                            ShowAlert(ReturnValue.LockMessage, ModuleNameConstants.Database.FleetProfile);
                                        SelectItem(string.Empty);
                                        return;
                                    }
                                    if (IsPopUp)
                                        hdnFleetID.Value = Convert.ToString(Session["FPSelectedItem"]);
                                    DisplayEditForm();
                                    Session["IsEdit"] = "YES";
                                    GridEnable(false, true, false);
                                    ddlImg.Enabled = true;
                                    if (imgFile.ImageUrl.Trim() != "" || lnkFileName.NavigateUrl.Trim() != "")
                                    {
                                        btndeleteImage.Enabled = true;
                                    }
                                    //chkActiveOnly.Enabled = false;
                                    //chkRotary.Enabled = false;
                                    //chkFixedWing.Enabled = false;
                                    EnableForm(true);
                                    tbaircraftcode.Enabled = false;
                                    tbtailnumber.Enabled = false;
                                    tbImgName.Enabled = true;
                                    fileUL.Enabled = true;
                                    ViewState["EditFleetID"] = null;
                                    ViewState["RefreshGrid"] = null;
                                    SelectFleetItem();
                                    tbdescription.Enabled = false;
                                    RadAjaxManager1.FocusControl(tbtypecode.ClientID);
                                    DisableLinks();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFleetProfile.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                EnableForm(true);
                                tbImgName.Enabled = true;
                                ddlImg.Enabled = true;
                                btndeleteImage.Enabled = true;
                                ViewState["EditFleetID"] = null;
                                ViewState["RefreshGrid"] = null;
                                RadAjaxManager1.FocusControl(tbaircraftcode.ClientID);
                                EnableForm(true);
                                //fileUL.Enabled = false; //Commented for removing document name
                                tbdescription.Enabled = false;
                                DisableLinks();
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
                                Session["CommandName"] = "Filter";
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            case "RowClick":
                                GridDataItem item = dgFleetProfile.SelectedItems[0] as GridDataItem;
                                Session["FPSelectedItem"] = item["FleetID"].Text;
                                var key = item.ItemIndex;
                                dgFleetProfile.Rebind();
                                dgFleetProfile.SelectedIndexes.Add(key);
                                GridEnable(true, true, true);
                                ReadOnlyForm();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        /// <summary>
        /// Binding the colors for grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetProfile_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                            dataItem.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ForeGrndCustomColor"), CultureInfo.CurrentCulture));
                            dataItem.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackgroundCustomColor"), CultureInfo.CurrentCulture));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            lnkForecast.Enabled = false;
            lnkForecast.CssClass = "fleet_link_disable";
            lbtnEngineAirframe.Enabled = false;
            lbtnEngineAirframe.CssClass = "fleet_link_disable";
            lbtnComponent.Enabled = false;
            lbtnComponent.CssClass = "fleet_link_disable";
            lbtnIntl.Enabled = false;
            lbtnIntl.CssClass = "fleet_link_disable";
            lbtn.Enabled = false;
            lbtn.CssClass = "fleet_link_disable";
            chkActiveOnly.Enabled = false;
            chkRotary.Enabled = false;
            chkFixedWing.Enabled = false;
            chkHomeBaseOnly.Enabled = false;
            lbCharterRates.Enabled = false;
            lbCharterRates.CssClass = "fleet_link_disable";
            lbNewCharterRates.Enabled = false;
            lbNewCharterRates.CssClass = "fleet_link_disable";

        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            lnkForecast.Enabled = true;
            lnkForecast.CssClass = "fleet_link";
            lbtnEngineAirframe.Enabled = true;
            lbtnEngineAirframe.CssClass = "fleet_link";
            lbtnComponent.Enabled = true;
            lbtnComponent.CssClass = "fleet_link";
            lbtnIntl.Enabled = true;
            lbtnIntl.CssClass = "fleet_link";
            lbtn.Enabled = true;
            lbtn.CssClass = "fleet_link";
            chkActiveOnly.Enabled = true;
            chkRotary.Enabled = true;
            chkFixedWing.Enabled = true;
            chkHomeBaseOnly.Enabled = true;
            lbCharterRates.Enabled = true;
            lbCharterRates.CssClass = "fleet_link_";
            lbNewCharterRates.Enabled = true;
            lbNewCharterRates.CssClass = "fleet_link";
        }
        private void EnableNavLinks(bool Enable)
        {
            string CSSStyle = string.Empty;
            if (Enable)
                CSSStyle = "fleet_link";
            else
                CSSStyle = "fleet_link_disable";
            lnkForecast.Enabled = Enable;
            lnkForecast.CssClass = CSSStyle;
            lbtnEngineAirframe.Enabled = Enable;
            lbtnEngineAirframe.CssClass = CSSStyle;
            lbtnComponent.Enabled = Enable;
            lbtnComponent.CssClass = CSSStyle;
            lbtnIntl.Enabled = Enable;
            lbtnIntl.CssClass = CSSStyle;
            lbtn.Enabled = Enable;
            lbtn.CssClass = CSSStyle;
            lbCharterRates.Enabled = Enable;
            lbCharterRates.CssClass = CSSStyle;
            lbNewCharterRates.Enabled = Enable;
            lbNewCharterRates.CssClass = CSSStyle;

        }
        protected void dgFleetProfile_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (!CheckCanEdit())
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Maximum record count reached. Please delete/inactive record(s) to activate existing Fleet.', 360, 50, 'Fleet');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return;
                        }
                        if (CustomValidations())
                        {
                            if ((Session["FPSelectedItem"] != null))
                            {
                                //using (TransactionScope scope = new TransactionScope())
                                //{
                                using (MasterCatalogServiceClient FleetService = new MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.Fleet Fleet = new FlightPakMasterService.Fleet();
                                    Fleet = GetItems();
                                    Fleet.FleetProfileDefinition = SaveFleetAdditionalInfo();
                                    var FleetValue = FleetService.UpdateFleetProfileType(Fleet);
                                    if (FleetValue.ReturnFlag == true)
                                    {
                                        #region Image Upload in Edit Mode
                                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                                        foreach (var DicItem in DicImage)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient MasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "Fleet";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(hdnFleetID.Value);
                                                Service.UWAWebpageName = "FleetProfileCatalog.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                MasterService.AddFWHType(Service);
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in Delete
                                        using (FlightPakMasterService.MasterCatalogServiceClient FWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse FWHType = new FlightPakMasterService.FileWarehouse();
                                            Dictionary<string, string> DicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                            foreach (var DicItem in DicImgDelete)
                                            {
                                                FWHType.UWAFileName = DicItem.Key;
                                                FWHType.RecordID = Convert.ToInt64(hdnFleetID.Value);
                                                FWHType.RecordType = "Fleet";
                                                FWHType.IsDeleted = true;
                                                FWHType.IsDeleted = true;
                                                FWHTypeService.DeleteFWHType(FWHType);
                                            }
                                        }
                                        #endregion
                                        //}
                                        // scope.Complete();
                                        // Unlock the Record
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.FleetProfile, Convert.ToInt64(Session["FPSelectedItem"].ToString().Trim()));
                                        }
                                        ViewState["EditFleetID"] = Session["FPSelectedItem"].ToString();
                                        SelectFleetItem();
                                        ReadOnlyForm();

                                        ShowSuccessMessage();
                                        EnableLinks();
                                        _selectLastModified = true;

                                        // Moved the below set of code from SaveFleet()
                                        if (tbaircraftcode.Text.Trim() == "" || tbtailnumber.Text.Trim() == "" || tbserialnumber.Text.Trim() == "" || tbtypecode.Text.Trim() == "")
                                        {
                                            btnCancel.Visible = true;
                                            btnCancelTop.Visible = true;
                                            btnSaveTop.Visible = true;
                                            btnSaveChanges.Visible = true;
                                        }
                                        else
                                        {
                                            btnSaveChanges.Visible = false;
                                            btnCancel.Visible = false;
                                            btnSaveTop.Visible = false;
                                            btnCancelTop.Visible = false;
                                        }
                                        Session["IsEdit"] = "NO";
                                        dgFleetProfile.Rebind();
                                        GridEnable(true, true, true);
                                        // Moved the above set of code from SaveFleet()
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(FleetValue.ErrorMessage, ModuleNameConstants.Database.FleetProfile);
                                    }
                                }
                            }

                            //karthik - Changed Condition for the Popup to Close without affecting the previous logic.
                            if ((IsPopUp) && (!string.IsNullOrEmpty(hdnScalendar.Value)))
                            {
                                //Clear session & close browser
                                // Session["FPSelectedItem"] = null;
                                if (IsCalender)
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                else 
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                            else
                            {
                                if ((IsPopUp))
                                {
                                    if (IsCalender)
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                    else
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                }
                                else
                                {
                                    hdnScalendar.Value = string.Empty;
                                }
                            }
                            
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton lbtnInsertCtl = (LinkButton)dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                LinkButton lbtnDelCtl = (LinkButton)dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                LinkButton lbtnEditCtl = (LinkButton)dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                if (IsAuthorized(Permission.Database.AddFleetProfile))
                {
                    lbtnInsertCtl.Visible = true;
                    if (Add && CheckCanAdd())
                    {
                        lbtnInsertCtl.Enabled = true;
                    }
                    else
                    {
                        lbtnInsertCtl.Enabled = false;
                    }
                }
                else
                {
                    lbtnInsertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteFleetProfile))
                {
                    lbtnDelCtl.Visible = true;
                    if (Delete)
                    {
                        lbtnDelCtl.Enabled = true;
                        lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        lbtnDelCtl.Enabled = false;
                        lbtnDelCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnDelCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditFleetProfile))
                {
                    lbtnEditCtl.Visible = true;
                    if (Edit)
                    {
                        lbtnEditCtl.Enabled = true;
                        lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        lbtnEditCtl.Enabled = false;
                        lbtnEditCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnEditCtl.Visible = false;
                }
            }
        }

        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                tbaircraftcode.Enabled = Enable;
                tbserialnumber.Enabled = Enable;
                tbtailnumber.Enabled = Enable;
                tbemergency.Enabled = Enable;
                tbmaxpax.Enabled = Enable;
                tbseccontrol.Enabled = Enable;
                tbdescription.Enabled = Enable;
                tbmaxrsvtns.Enabled = Enable;
                tbdomflightphone.Enabled = Enable;
                tbminrunway.Enabled = Enable;
                tblntlflightphone.Enabled = Enable;
                tbmaxfuel.Enabled = Enable;
                tbminfuel.Enabled = Enable;
                tbtaxifuel.Enabled = Enable;
                tbmaxtakeoffwt.Enabled = Enable;
                tbmaxlandingwt.Enabled = Enable;
                tbmaxzerofuelwt.Enabled = Enable;
                tbbasicoperatingwt.Enabled = Enable;
                tbcruisespeed.Enabled = Enable;
                tbmaxflightlevel.Enabled = Enable;
                tbhourstowarning.Enabled = Enable;
                tbMarginPercentage.Enabled = Enable;
                tbAirCraftNationality.Enabled = Enable;
                tbhourstonext.Enabled = Enable;
                tbcontrol.Enabled = Enable;
                tbnoncontrol.Enabled = Enable;
                drpaircrafttype.Enabled = Enable;
                drpairfactclass.Enabled = Enable;
                tbvendor.Enabled = Enable;
                tbtypecode.Enabled = Enable;
                tbhomebase.Enabled = Enable;
                chkfar135.Enabled = Enable;
                chkfar91.Enabled = Enable;
                chkinactiveairfact.Enabled = Enable;
                // tblasthourly.Enabled = enable;
                // Set Logged-in User Name
                if (UserPrincipal != null && UserPrincipal.Identity._clientId != null)
                {
                    tbclient.Enabled = false;
                    tbclient.Text = UserPrincipal.Identity._clientCd.ToString().Trim();
                    hdnClient.Value = UserPrincipal.Identity._clientId.ToString().Trim();
                    btnclient.Enabled = false;
                }
                else
                {
                    tbclient.Enabled = Enable;
                    btnclient.Enabled = Enable;
                }
                tbmaintcrew.Enabled = Enable;
                btntypecode.Enabled = Enable;
                btnmaintcrew.Enabled = Enable;
                btnHB.Enabled = Enable;
                btnvendor.Enabled = Enable;
                btnemergency.Enabled = Enable;
                rcpBackColor.Enabled = Enable;
                rcpForeColor.Enabled = Enable;
                // tbBackColor.Enabled = Enable;                
                // tbForeColor.Enabled = Enable;
                TextBox tbDateofBirth = (TextBox)ucLastInspectionDate.FindControl("tbDate");
                tbDateofBirth.Enabled = Enable;
                tbNotes.Enabled = Enable;
                ddlImg.Enabled = Enable;
                imgFile.Style.Add("disabled", Enable.ToString());
                tbImgName.Enabled = Enable;
                btndeleteImage.Enabled = Enable;
                btndeleteImage.Visible = Enable;
                fileUL.Enabled = Enable;
                btnAddlInfo.Visible = Enable;
                btnDeleteInfo.Visible = Enable;
                tbSecondaryDomFlightPhone.Enabled = Enable;
                tbSecondaryIntlFlightPhone.Enabled = Enable;
                btnSaveChanges.Visible = Enable;
                btnSaveTop.Visible = Enable;
                btnCancel.Visible = Enable;
                btnCancelTop.Visible = Enable;
                dgPassengerAddlInfo.Enabled = Enable;
                if (dgFleetProfile.Items.Count != 0)
                    EnableNavLinks(true);
                else
                    EnableNavLinks(false);
            }
        }
        private FlightPakMasterService.Fleet GetItems(string FleetGroupCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetGroupCode))
            {
                FlightPakMasterService.Fleet FleetType = new FlightPakMasterService.Fleet();
                FleetType.AircraftCD = tbaircraftcode.Text;
                FleetType.SerialNum = tbserialnumber.Text;
                FleetType.TailNum = tbtailnumber.Text;
                FleetType.HomebaseID = null;
                if (tbhomebase.Text.Trim() != "")
                {
                    if (Convert.ToInt64(tbhomebase.Text) > 0)
                    {
                        FleetType.HomebaseID = Convert.ToInt64(tbhomebase.Text);
                    }
                }
                FleetType.VendorID = null;
                if (tbvendor.Text.Trim() != "")
                {
                    if (Convert.ToInt64(tbvendor.Text) > 0)
                    {
                        FleetType.VendorID = Convert.ToInt64(tbvendor.Text);
                    }
                }
                if (!string.IsNullOrEmpty(tbmaxpax.Text))
                {
                    FleetType.MaximumPassenger = Convert.ToDecimal(tbmaxpax.Text);
                }
                FleetType.TypeDescription = tbdescription.Text;
                if (!string.IsNullOrEmpty(tbmaxrsvtns.Text))
                {
                    FleetType.MaximumReservation = Convert.ToDecimal(tbmaxrsvtns.Text);
                }
                FleetType.FlightPhoneNum = tbdomflightphone.Text;
                if (!string.IsNullOrEmpty(tbminrunway.Text))
                {
                    FleetType.MimimumRunway = Convert.ToDecimal(tbminrunway.Text);
                }
                FleetType.FlightPhoneIntlNum = tblntlflightphone.Text;
                if (!string.IsNullOrEmpty(tbmaxfuel.Text))
                {
                    FleetType.MaximumFuel = Convert.ToDecimal(tbmaxfuel.Text);
                }
                if (!string.IsNullOrEmpty(tbminfuel.Text))
                {
                    FleetType.MinimumFuel = Convert.ToDecimal(tbminfuel.Text);
                }
                if (!string.IsNullOrEmpty(tbtaxifuel.Text))
                {
                    FleetType.TaxiFuel = Convert.ToDecimal(tbtaxifuel.Text);
                }
                if (!string.IsNullOrEmpty(tbmaxtakeoffwt.Text))
                {
                    FleetType.MaximumTakeOffWeight = Convert.ToDecimal(tbmaxtakeoffwt.Text);
                }
                if (!string.IsNullOrEmpty(tbmaxlandingwt.Text))
                {
                    FleetType.MaximumLandingWeight = Convert.ToDecimal(tbmaxlandingwt.Text);
                }
                if (!string.IsNullOrEmpty(tbmaxzerofuelwt.Text))
                {
                    FleetType.MaximumWeightZeroFuel = Convert.ToDecimal(tbmaxzerofuelwt.Text);
                }
                if (!string.IsNullOrEmpty(tbbasicoperatingwt.Text))
                {
                    FleetType.BasicOperatingWeight = Convert.ToDecimal(tbbasicoperatingwt.Text);
                }
                FleetType.FlightPlanCruiseSpeed = tbcruisespeed.Text;
                if (!string.IsNullOrEmpty(tbmaxflightlevel.Text))
                {
                    FleetType.FlightPlanMaxFlightLevel = Convert.ToDecimal(tbmaxflightlevel.Text);
                }
                if (!string.IsNullOrEmpty(tbhourstowarning.Text))
                {
                    FleetType.WarningHrs = Convert.ToDecimal(tbhourstowarning.Text);
                }
                if (!string.IsNullOrEmpty(tbhourstonext.Text))
                {
                    FleetType.InspectionHrs = Convert.ToDecimal(tbhourstonext.Text);
                }
                if (!string.IsNullOrEmpty(tbcontrol.Text))
                {
                    FleetType.SIFLMultiControlled = Convert.ToDecimal(tbcontrol.Text);
                }
                if (!string.IsNullOrEmpty(tbnoncontrol.Text))
                {
                    FleetType.SIFLMultiNonControled = Convert.ToDecimal(tbnoncontrol.Text);
                }
                if (drpairfactclass.SelectedIndex > 0)
                {
                    FleetType.Class = Convert.ToDecimal(drpairfactclass.SelectedValue);
                }
                if (drpaircrafttype.SelectedIndex > 0)
                {
                    FleetType.FleetSize = drpaircrafttype.SelectedValue;  //Changeed to fleetsize from fleettype
                }
                if (tbForeColor.Text.Trim() != "")
                    FleetType.ForeGrndCustomColor = tbForeColor.Text.Trim();
                else
                    FleetType.ForeGrndCustomColor = null;
                if (tbBackColor.Text.Trim() != "")
                    FleetType.BackgroundCustomColor = tbBackColor.Text.Trim();
                else
                    FleetType.BackgroundCustomColor = null;
                TextBox tbLastInspectionDate = (TextBox)ucLastInspectionDate.FindControl("tbDate");
                if (!string.IsNullOrEmpty(tbLastInspectionDate.Text.Trim().ToString()))
                {
                    FleetType.LastInspectionDT = DateTime.ParseExact(tbLastInspectionDate.Text, DateFormat, CultureInfo.InvariantCulture);
                }
                else
                    FleetType.LastInspectionDT = null;
                FleetType.IsDeleted = false;
                pnlExternalForm.Visible = false;
                return FleetType;
            }
        }
        private FlightPakMasterService.Fleet GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Fleet FleetService = new FlightPakMasterService.Fleet();
                FleetService.SerialNum = tbserialnumber.Text.ToUpper();
                FleetService.TailNum = tbtailnumber.Text.ToUpper();
                FleetService.AircraftCD = tbaircraftcode.Text.ToUpper();
                string FltId = hdnFleetID.Value;
                int idx = FltId.IndexOf(",", 0);
                if (idx > 0)
                    hdnFleetID.Value = FltId.Substring(0, idx);
                if (hdnFleetID.Value.Trim() != "")
                    FleetService.FleetID = Convert.ToInt64(hdnFleetID.Value);
                #region Foreign key colums
                FleetService.HomebaseID = null;
                if (hdnHomeBaseID.Value.Trim() != "")
                {
                    if (hdnHomeBaseID.Value.ToString().Trim() != "" && Convert.ToInt64(hdnHomeBaseID.Value) > 0)
                    {
                        FleetService.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                    }
                }
                FleetService.VendorID = null;
                if (tbvendor.Text.Trim() != "")
                {
                    if (hdnVendorID.Value.ToString().Trim() != "" && Convert.ToInt64(hdnVendorID.Value) > 0)
                    {
                        FleetService.VendorID = Convert.ToInt64(hdnVendorID.Value);
                    }
                }
                FleetService.AircraftID = null;
                if (tbtypecode.Text.Trim() != "")
                {
                    if (hdnType.Value.ToString().Trim() != "" && Convert.ToInt64(hdnType.Value) > 0)
                    {
                        FleetService.AircraftID = Convert.ToInt64(hdnType.Value);
                    }
                }
                FleetService.EmergencyContactID = null;
                if (tbemergency.Text.Trim() != "")
                {
                    if (hdnContact.Value.ToString().Trim() != "" && Convert.ToInt64(hdnContact.Value) > 0)
                    {
                        FleetService.EmergencyContactID = Convert.ToInt64(hdnContact.Value);
                    }
                }
                FleetService.ClientID = null;
                if (tbclient.Text.Trim() != "")
                {
                    if (hdnClient.Value.ToString().Trim() != "" && Convert.ToInt64(hdnClient.Value) > 0)
                    {
                        FleetService.ClientID = Convert.ToInt64(hdnClient.Value);
                    }
                }
                FleetService.CrewID = null;
                if (tbmaintcrew.Text.Trim() != "")
                {
                    if (hdnCrew.Value.ToString().Trim() != "" && Convert.ToInt64(hdnCrew.Value) > 0)
                    {
                        FleetService.CrewID = Convert.ToInt64(hdnCrew.Value);
                    }
                }
                #endregion
                FleetService.FlightPhoneNum = tbdomflightphone.Text.ToUpper();
                if (tbMarginPercentage.Text.Trim() != "")
                {
                    FleetService.MarginalPercentage = Convert.ToDecimal(tbMarginPercentage.Text);
                }
                FleetService.FlightPhoneIntlNum = tblntlflightphone.Text.ToUpper();
                TextBox tbLastInspectionDate = (TextBox)ucLastInspectionDate.FindControl("tbDate");
                if (!string.IsNullOrEmpty(tbLastInspectionDate.Text.Trim().ToString()))
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    if (DateFormat != null)
                    {
                        FleetService.LastInspectionDT = Convert.ToDateTime(FormatDate(((TextBox)tbLastInspectionDate.FindControl("tbDate")).Text, DateFormat));
                    }
                    else
                    {
                        FleetService.LastInspectionDT = Convert.ToDateTime(((TextBox)tbLastInspectionDate.FindControl("tbDate")).Text);
                    }
                    // FleetService.LastInspectionDT = DateTime.ParseExact(tbLastInspectionDate.Text, DateFormat, CultureInfo.InvariantCulture);
                }
                else
                    FleetService.LastInspectionDT = null;
                // need to implement here
                if (chkfar91.Checked)
                    FleetService.IsFAR91 = true;
                else
                    FleetService.IsFAR91 = false;
                if (chkfar135.Checked)
                    FleetService.IsFAR135 = true;
                else
                    FleetService.IsFAR135 = false;
                if (tbcontrol.Text.Trim() != "")
                    //FleetService.SIFLMultiControlled = Convert.ToDecimal(tbcontrol.TextWithLiterals);
                    FleetService.SIFLMultiControlled = Convert.ToDecimal(tbcontrol.Text);
                else
                    FleetService.SIFLMultiControlled = Convert.ToDecimal(0.0);
                if (tbnoncontrol.Text.Trim() != "")
                    //FleetService.SIFLMultiNonControled = Convert.ToDecimal(tbnoncontrol.TextWithLiterals);
                    FleetService.SIFLMultiNonControled = Convert.ToDecimal(tbnoncontrol.Text);
                else
                    FleetService.SIFLMultiNonControled = Convert.ToDecimal(0.0);
                if (tbseccontrol.Text.Trim() != "")
                    //FleetService.MultiSec = Convert.ToDecimal(tbseccontrol.TextWithLiterals);
                    FleetService.MultiSec = Convert.ToDecimal(tbseccontrol.Text);
                else
                    FleetService.MultiSec = Convert.ToDecimal(0.0);
                if (!string.IsNullOrEmpty(tbmaxpax.Text))
                    FleetService.MaximumPassenger = Convert.ToDecimal(tbmaxpax.Text);
                else
                    FleetService.MaximumPassenger = 0;
                FleetService.TypeDescription = tbdescription.Text;
                if (!string.IsNullOrEmpty(tbmaxrsvtns.Text))
                    FleetService.MaximumReservation = Convert.ToDecimal(tbmaxrsvtns.Text);
                else
                    FleetService.MaximumReservation = 0;
                if (!string.IsNullOrEmpty(tbminrunway.Text))
                    FleetService.MimimumRunway = Convert.ToDecimal(tbminrunway.Text);
                else
                    FleetService.MimimumRunway = 0;
                if (!string.IsNullOrEmpty(tbmaxfuel.Text))
                    FleetService.MaximumFuel = Convert.ToDecimal(tbmaxfuel.Text);
                else
                    FleetService.MaximumFuel = 0;
                if (!string.IsNullOrEmpty(tbminfuel.Text))
                    FleetService.MinimumFuel = Convert.ToDecimal(tbminfuel.Text);
                else
                    FleetService.MinimumFuel = 0;
                if (!string.IsNullOrEmpty(tbtaxifuel.Text))
                    FleetService.TaxiFuel = Convert.ToDecimal(tbtaxifuel.Text);
                else
                    FleetService.TaxiFuel = 0;
                if (!string.IsNullOrEmpty(tbmaxtakeoffwt.Text))
                    FleetService.MaximumTakeOffWeight = Convert.ToDecimal(tbmaxtakeoffwt.Text);
                else
                    FleetService.MaximumTakeOffWeight = 0;
                if (!string.IsNullOrEmpty(tbmaxlandingwt.Text))
                    FleetService.MaximumLandingWeight = Convert.ToDecimal(tbmaxlandingwt.Text);
                else
                    FleetService.MaximumLandingWeight = 0;
                if (!string.IsNullOrEmpty(tbmaxzerofuelwt.Text))
                    FleetService.MaximumWeightZeroFuel = Convert.ToDecimal(tbmaxzerofuelwt.Text);
                else
                    FleetService.MaximumWeightZeroFuel = 0;
                if (!string.IsNullOrEmpty(tbbasicoperatingwt.Text))
                    FleetService.BasicOperatingWeight = Convert.ToDecimal(tbbasicoperatingwt.Text);
                else
                    FleetService.BasicOperatingWeight = 0;
                FleetService.FlightPlanCruiseSpeed = tbcruisespeed.Text;
                if (!string.IsNullOrEmpty(tbmaxflightlevel.Text))
                    FleetService.FlightPlanMaxFlightLevel = Convert.ToDecimal(tbmaxflightlevel.Text);
                else
                    FleetService.FlightPlanMaxFlightLevel = 0;
                if (!string.IsNullOrEmpty(tbhourstowarning.Text))
                    FleetService.WarningHrs = Convert.ToDecimal(tbhourstowarning.Text);
                else
                    FleetService.WarningHrs = 0;
                if (!string.IsNullOrEmpty(tbhourstonext.Text))
                    FleetService.InspectionHrs = Convert.ToDecimal(tbhourstonext.Text);
                else
                    FleetService.InspectionHrs = 0;
                if (drpairfactclass.SelectedIndex > 0)
                    FleetService.Class = Convert.ToDecimal(drpairfactclass.SelectedValue);
                else
                    FleetService.Class = null;
                if (drpaircrafttype.SelectedIndex > 0)
                    FleetService.FleetSize = drpaircrafttype.SelectedValue;
                else
                    FleetService.FleetSize = null;
                if (!string.IsNullOrEmpty(tbNotes.Text))
                {
                    FleetService.Notes = tbNotes.Text.Trim();
                }
                else
                {
                    FleetService.Notes = "";
                }
                if (tbBackColor.Text.Trim() != "")
                {
                    FleetService.BackgroundCustomColor = tbBackColor.Text.Trim();
                }
                else
                {
                    FleetService.BackgroundCustomColor = null;
                }
                if (tbForeColor.Text.Trim() != "")
                {
                    FleetService.ForeGrndCustomColor = tbForeColor.Text.Trim();
                }
                else
                {
                    FleetService.ForeGrndCustomColor = null;
                }
                FleetService.IsDeleted = false;
                if (tbSecondaryDomFlightPhone.Text.Trim() != "")
                {
                    FleetService.SecondaryDomFlightPhone = tbSecondaryDomFlightPhone.Text.Trim();
                }
                else
                {
                    FleetService.SecondaryDomFlightPhone = null;
                }
                if (tbSecondaryIntlFlightPhone.Text.Trim() != "")
                {
                    FleetService.SecondaryIntlFlightPhone = tbSecondaryIntlFlightPhone.Text.Trim();
                }
                else
                {
                    FleetService.SecondaryIntlFlightPhone = null;
                }
                if (chkinactiveairfact.Checked)
                {
                    FleetService.IsInActive = true;
                }
                else
                {
                    FleetService.IsInActive = false;
                }
                if (!string.IsNullOrEmpty(hdnAirCraftNationalityId.Value) && Convert.ToInt64(hdnAirCraftNationalityId.Value)>0)
                    FleetService.NationalityCountryID = Convert.ToInt64(hdnAirCraftNationalityId.Value);
                else
                    FleetService.NationalityCountryID = 0;
                return FleetService;
            }
        }
        /// <summary>
        /// Insert Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFleetProfile_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (!CheckCanAdd())
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Maximum record count reached. Please delete record(s) to add new Fleet.', 360, 50, 'Fleet');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return;
                        }
                        if (CustomValidations())
                        {
                            using (MasterCatalogServiceClient FleetService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Fleet Fleet = new FlightPakMasterService.Fleet();
                                Fleet = GetItems();
                                Fleet.FleetProfileDefinition = SaveFleetAdditionalInfo();
                                var ReturnValue = FleetService.AddFleetProfileType(Fleet);
                                //For Data Anotation
                                if (ReturnValue.ReturnFlag == true)
                                {
                                    #region Image Upload in New Mode
                                    var InsertedFleetID = FleetService.GetFleetID(tbtailnumber.Text.Trim(), tbserialnumber.Text.Trim());
                                    if (InsertedFleetID != null)
                                    {
                                        foreach (FlightPakMasterService.GetFleetID FltId in InsertedFleetID.EntityList)
                                        {
                                            hdnFleetID.Value = FltId.FleetID.ToString();
                                        }
                                    }
                                    Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                                    foreach (var DicItem in DicImage)
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient MasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                            Service.FileWarehouseID = 0;
                                            Service.RecordType = "Fleet";
                                            Service.UWAFileName = DicItem.Key;
                                            Service.RecordID = Convert.ToInt64(hdnFleetID.Value);
                                            Service.UWAWebpageName = "FleetProfileCatalog.aspx";
                                            Service.UWAFilePath = DicItem.Value;
                                            Service.IsDeleted = false;
                                            Service.FileWarehouseID = 0;
                                            MasterService.AddFWHType(Service);
                                        }
                                    }
                                    #endregion

                                    ShowSuccessMessage();
                                    ViewState["RefreshGrid"] = "RG";
                                    EnableLinks();

                                    // Moved the below set of code from SaveFleet()
                                    if (tbaircraftcode.Text.Trim() == "" || tbtailnumber.Text.Trim() == "" || tbserialnumber.Text.Trim() == "" || tbtypecode.Text.Trim() == "")
                                    {
                                        btnCancel.Visible = true;
                                        btnCancelTop.Visible = true;
                                        btnSaveTop.Visible = true;
                                        btnSaveChanges.Visible = true;
                                    }
                                    else
                                    {
                                        btnSaveChanges.Visible = false;
                                        btnCancel.Visible = false;
                                        btnSaveTop.Visible = false;
                                        btnCancelTop.Visible = false;
                                    }
                                    Session["IsEdit"] = "NO";
                                    dgFleetProfile.Rebind();
                                    GridEnable(true, true, true);
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.FleetProfile);
                                }
                            }

                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                            // Moved the above set of code from SaveFleet()
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void dgFleetProfile_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                bool isDeleted = false;

                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if ((Session["FPSelectedItem"] != null))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient FleetTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Fleet FleetType = new FlightPakMasterService.Fleet();
                                FleetType.FleetID = Convert.ToInt64(hdnFleetID.Value);
                                FleetType.IsDeleted = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var ReturnValue = CommonService.Lock(EntitySet.Database.FleetProfile, Convert.ToInt64(Session["FPSelectedItem"].ToString().Trim()));
                                    if (!ReturnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        if (IsPopUp)
                                            ShowLockAlertPopup(ReturnValue.LockMessage, ModuleNameConstants.Database.FleetProfile);
                                        else
                                            ShowAlert(ReturnValue.LockMessage, ModuleNameConstants.Database.FleetProfile);
                                        return;
                                    }
                                    FleetTypeService.DeleteFleetProfileType(FleetType);
                                    ViewState["RefreshGrid"] = "RG";
                                    isDeleted = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FleetProfile, Convert.ToInt64(Session["FPSelectedItem"].ToString().Trim()));
                    }

                    if (isDeleted)
                    {
                        Session["FPSelectedItem"] = null;
                        DefaultSelection();
                    }
                }
            }
        }
        protected void lnkBtnInsert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFleetProfile.SelectedIndexes.Clear();
                        DisplayInsertForm();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            CancelFleet();
            if (IsPopUp && IsCalender)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
            else if (IsPopUp)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
        }
        protected void btnCancelTop_Click(object sender, EventArgs e)
        {
            CancelFleet();
            if (IsPopUp && IsCalender)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
            else if (IsPopUp)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
        }
        private void CancelFleet()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs("Cancel"))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Session.Remove("FPSelectedItem");
                        GridEnable(true, true, true);
                        dgFleetProfile.Rebind();
                        
                        //if (dgFleetProfile.Items.Count > 0)
                        //{
                        //    dgFleetProfile.SelectedIndexes.Add(0);
                        //    GridDataItem Item = dgFleetProfile.SelectedItems[0] as GridDataItem;
                        //    Session["FPSelectedItem"] = Item["FleetID"].Text;
                        //    hdnFleetID.Value = Item["FleetID"].Text;
                        //    hdnCustomerID.Value = Item["CustomerID"].Text;
                        //}
                        //SelectFleetItem();
                        if (Session["FPSelectedItem"] != null)
                        {
                            SelectItem(Session["FPSelectedItem"].ToString());
                        }
                        

                        ReadOnlyForm();
                        EnableForm(false);
                        btnSaveChanges.Visible = false;
                        btnCancel.Visible = false;
                        btnSaveTop.Visible = false;
                        btnCancelTop.Visible = false;
                        Session["IsEdit"] = "NO";
                        EnableLinks();
                        //chkActiveOnly.Enabled = true;
                        //chkRotary.Enabled = true;
                        //chkFixedWing.Enabled = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["FPSelectedItem"] != null)
                {
                    string Code = Session["FPSelectedItem"].ToString().Trim();
                }
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    if (!string.IsNullOrEmpty(hdnFleetID.Value))
                    {
                        var CheckFleet = Service.GetFleetIDInfo(Convert.ToInt64(hdnFleetID.Value)).EntityList;
                        foreach (FlightPakMasterService.GetFleetIDInfo Fleet in CheckFleet)
                        {
                            if (Fleet.ClientID != null && Fleet.ClientID.ToString().Trim() != "")
                            {
                                if (Convert.ToInt64(Fleet.ClientID) > 0)
                                {
                                    hdnClient.Value = Fleet.ClientID.ToString();
                                    tbclient.Text = Fleet.ClientCD.ToString();
                                }
                                else
                                {
                                    tbclient.Text = string.Empty;
                                    hdnClient.Value = "0";
                                }
                            }
                            else
                            {
                                tbclient.Text = string.Empty;
                                hdnClient.Value = "0";
                            }
                            if (Fleet.HomebaseID != null && Fleet.HomebaseID.ToString().Trim() != "")
                            {
                                if (Convert.ToInt64(Fleet.HomebaseID) > 0)
                                {
                                    hdnHomeBaseID.Value = Fleet.HomebaseID.ToString();
                                    tbhomebase.Text = Convert.ToString(Fleet.HomeBaseCD);
                                }
                                else
                                {
                                    hdnHomeBaseID.Value = "0";
                                    tbhomebase.Text = string.Empty;
                                }
                            }
                            else
                            {
                                hdnHomeBaseID.Value = "0";
                                tbhomebase.Text = string.Empty;
                            }
                            if (Fleet.NationalityCountryID != null && Fleet.NationalityCountryID.ToString().Trim() != "")
                            {
                                if (Convert.ToInt64(Fleet.NationalityCountryID) > 0)
                                {
                                    hdnAirCraftNationalityId.Value = Fleet.NationalityCountryID.ToString();
                                    tbAirCraftNationality.Text = Convert.ToString(Fleet.CountryName);
                                    List<Country> c = Service.GetCountryMasterList().EntityList;
                                    Country fc = c.Where(x => x.CountryID == Fleet.NationalityCountryID).FirstOrDefault();
                                    hdnCountryCode.Value = fc.CountryCD;
                                    hdnAirCraftNationalityName.Value = Convert.ToString(Fleet.CountryName);
                                }
                                else
                                {
                                    hdnAirCraftNationalityId.Value = "0";
                                    hdnAirCraftNationalityName.Value = string.Empty;
                                    tbAirCraftNationality.Text = string.Empty;
                                }
                            }
                            else
                            {
                                hdnAirCraftNationalityId.Value = "0";
                                hdnAirCraftNationalityName.Value = string.Empty;
                                tbAirCraftNationality.Text = string.Empty;
                            }
                            if (Fleet.VendorID != null && Fleet.VendorID.ToString().Trim() != "")
                            {
                                if (Convert.ToInt64(Fleet.VendorID) > 0)
                                {
                                    tbvendor.Text = Fleet.VendorCD.ToString();
                                    hdnVendorID.Value = Fleet.VendorID.ToString();
                                }
                                else
                                {
                                    tbvendor.Text = string.Empty;
                                    hdnVendorID.Value = "0";
                                }
                            }
                            else
                            {
                                tbvendor.Text = string.Empty;
                                hdnVendorID.Value = "0";
                            }
                            if (Fleet.EmergencyContactID != null && Fleet.EmergencyContactID.ToString().Trim() != "")
                            {
                                if (Convert.ToInt64(Fleet.EmergencyContactID) > 0)
                                {
                                    hdnContact.Value = Fleet.EmergencyContactID.ToString();
                                    tbemergency.Text = Fleet.EmergencyContactCD.ToString();
                                }
                                else
                                {
                                    hdnContact.Value = "0";
                                    tbemergency.Text = "";
                                }
                            }
                            else
                            {
                                hdnContact.Value = "0";
                                tbemergency.Text = "";
                            }
                            if (Fleet.CrewID != null && Fleet.CrewID.ToString().Trim() != "")
                            {
                                hdnCrew.Value = Fleet.CrewID.ToString();
                                tbmaintcrew.Text = Fleet.CrewCD.ToString();
                            }
                            else
                            {
                                hdnCrew.Value = "0";
                                tbmaintcrew.Text = "";
                            }
                            if (Fleet.AircraftID != null && Fleet.AircraftID.ToString().Trim() != "")
                            {
                                hdnType.Value = Fleet.AircraftID.ToString();
                                tbtypecode.Text = Fleet.AirCraft_AircraftCD.ToString();
                            }
                            else
                            {
                                hdnType.Value = "0";
                                tbtypecode.Text = "";
                            }
                            if (Fleet.AircraftCD != null && Fleet.AircraftCD.ToString().Trim() != "")
                            {
                                tbaircraftcode.Text = Fleet.AircraftCD.ToString().Trim();
                            }
                            else
                                tbaircraftcode.Text = "";




                            if (Fleet.SerialNum != null && Fleet.SerialNum.ToString().Trim() != "")
                            {
                                tbserialnumber.Text = Fleet.SerialNum.ToString();
                            }
                            else
                                tbserialnumber.Text = string.Empty;
                            if (Fleet.TailNum != null && Fleet.TailNum.ToString().Trim() != "")
                            {
                                tbtailnumber.Text = Fleet.TailNum.ToString();
                            }
                            else
                                tbtailnumber.Text = string.Empty;
                            if (Fleet.MaximumPassenger != null && Fleet.MaximumPassenger.ToString().Trim() != "")
                            {
                                tbmaxpax.Text = Fleet.MaximumPassenger.ToString();
                            }
                            else
                                tbmaxpax.Text = string.Empty;
                            if (Fleet.TypeDescription != null && Fleet.TypeDescription.ToString().Trim() != "")
                            {
                                tbdescription.Text = Fleet.TypeDescription.ToString().Trim();
                            }
                            else
                                tbdescription.Text = string.Empty;
                            if (Fleet.MaximumReservation != null && Fleet.MaximumReservation.ToString().Trim() != "")
                            {
                                tbmaxrsvtns.Text = Fleet.MaximumReservation.ToString();
                            }
                            else
                                tbmaxrsvtns.Text = string.Empty;
                            if (Fleet.FlightPhoneNum != null && Fleet.FlightPhoneNum.ToString().Trim() != "")
                            {
                                tbdomflightphone.Text = Fleet.FlightPhoneNum.ToString();
                            }
                            else
                                tbdomflightphone.Text = string.Empty;
                            if (Fleet.MimimumRunway != null && Fleet.MimimumRunway.ToString().Trim() != "")
                            {
                                tbminrunway.Text = Fleet.MimimumRunway.ToString();
                            }
                            else
                                tbminrunway.Text = string.Empty;
                            if (Fleet.MaximumFuel != null && Fleet.MaximumFuel.ToString().Trim() != "")
                            {
                                tbmaxfuel.Text = Fleet.MaximumFuel.ToString();
                            }
                            else
                                tbmaxfuel.Text = string.Empty;
                            if (Fleet.TaxiFuel != null && Fleet.TaxiFuel.ToString().Trim() != "")
                            {
                                tbtaxifuel.Text = Fleet.TaxiFuel.ToString();
                            }
                            else
                                tbtaxifuel.Text = string.Empty;
                            if (Fleet.MinimumFuel != null && Fleet.MinimumFuel.ToString().Trim() != "")
                            {
                                tbminfuel.Text = Fleet.MinimumFuel.ToString();
                            }
                            else
                                tbminfuel.Text = string.Empty;
                            if (Fleet.MaximumTakeOffWeight != null && Fleet.MaximumTakeOffWeight.ToString().Trim() != "")
                            {
                                tbmaxtakeoffwt.Text = Fleet.MaximumTakeOffWeight.ToString();
                            }
                            else
                                tbmaxtakeoffwt.Text = string.Empty;
                            if (Fleet.MaximumLandingWeight != null && Fleet.MaximumLandingWeight.ToString().Trim() != "")
                            {
                                tbmaxlandingwt.Text = Fleet.MaximumLandingWeight.ToString();
                            }
                            else
                                tbmaxlandingwt.Text = string.Empty;
                            if (Fleet.MaximumWeightZeroFuel != null && Fleet.MaximumWeightZeroFuel.ToString().Trim() != "")
                            {
                                tbmaxzerofuelwt.Text = Fleet.MaximumWeightZeroFuel.ToString();
                            }
                            else
                                tbmaxzerofuelwt.Text = string.Empty;
                            if (Fleet.BasicOperatingWeight != null && Fleet.BasicOperatingWeight.ToString().Trim() != "")
                            {
                                tbbasicoperatingwt.Text = Fleet.BasicOperatingWeight.ToString();
                            }
                            else
                                tbbasicoperatingwt.Text = string.Empty;
                            if (Fleet.FlightPlanCruiseSpeed != null && Fleet.FlightPlanCruiseSpeed.ToString().Trim() != "")
                            {
                                tbcruisespeed.Text = Fleet.FlightPlanCruiseSpeed.ToString();
                            }
                            else
                                tbcruisespeed.Text = string.Empty;
                            if (Fleet.FlightPlanMaxFlightLevel != null && Fleet.FlightPlanMaxFlightLevel.ToString().Trim() != "")
                            {
                                tbmaxflightlevel.Text = Fleet.FlightPlanMaxFlightLevel.ToString();
                            }
                            else
                                tbmaxflightlevel.Text = string.Empty;
                            if (Fleet.WarningHrs != null && Fleet.WarningHrs.ToString().Trim() != "")
                            {
                                tbhourstowarning.Text = Fleet.WarningHrs.ToString();
                            }
                            else
                                tbhourstowarning.Text = string.Empty;
                            if (Fleet.InspectionHrs != null && Fleet.InspectionHrs.ToString().Trim() != "")
                            {
                                tbhourstonext.Text = Fleet.InspectionHrs.ToString();
                            }
                            else
                                tbhourstonext.Text = string.Empty;
                            if (Fleet.Class.ToString().Trim() != "" && Convert.ToInt32(Fleet.Class.ToString().Trim()) > 0)
                            {
                                drpairfactclass.SelectedValue = Fleet.Class.ToString().Trim();
                            }
                            else
                                drpairfactclass.SelectedIndex = 0;
                            if (Fleet.FleetSize != null)
                            {
                                if (Fleet.FleetSize.ToString() == "S" || Fleet.FleetSize.ToString() == "M" || Fleet.FleetSize.ToString() == "L")
                                {
                                    drpaircrafttype.SelectedValue = Fleet.FleetSize.ToString().Trim();
                                }
                                else
                                {
                                    drpaircrafttype.SelectedIndex = 0;
                                }
                            }
                            else
                                drpaircrafttype.SelectedIndex = 0;
                            if (Fleet.FlightPhoneIntlNum != null && Fleet.FlightPhoneIntlNum.ToString().Trim() != "")
                            {
                                tblntlflightphone.Text = Fleet.FlightPhoneIntlNum.ToString().Trim();
                            }
                            else
                                tblntlflightphone.Text = string.Empty;
                            if (Fleet.SIFLMultiControlled != null && Fleet.SIFLMultiControlled.ToString().Trim() != "")
                            {
                                tbcontrol.Text = Fleet.SIFLMultiControlled.ToString();
                            }
                            else
                                tbcontrol.Text = "0.0";
                            if (Fleet.SIFLMultiNonControled != null && Fleet.SIFLMultiNonControled.ToString().Trim() != "")
                            {
                                tbnoncontrol.Text = Fleet.SIFLMultiNonControled.ToString();
                            }
                            else
                                tbnoncontrol.Text = "0.0";
                            if (!string.IsNullOrEmpty(Fleet.SecondaryDomFlightPhone))
                            {
                                tbSecondaryDomFlightPhone.Text = Fleet.SecondaryDomFlightPhone.ToString();
                            }
                            else
                            {
                                tbSecondaryDomFlightPhone.Text = string.Empty;
                            }
                            if (!string.IsNullOrEmpty(Fleet.SecondaryIntlFlightPhone))
                            {
                                tbSecondaryIntlFlightPhone.Text = Fleet.SecondaryIntlFlightPhone.ToString();
                            }
                            else
                            {
                                tbSecondaryIntlFlightPhone.Text = string.Empty;
                            }
                            if (Fleet.MultiSec != null && Fleet.MultiSec.ToString().Trim() != "")
                            {
                                tbseccontrol.Text = Fleet.MultiSec.ToString().Trim();
                            }
                            else
                                tbseccontrol.Text = "0.0";
                            chkfar91.Checked = false;
                            if (Fleet.IsFAR91 != null && Fleet.IsFAR91.ToString().Trim() != "")
                            {
                                if (Fleet.IsFAR91 == true || Fleet.IsFAR91.ToString().ToLower() == "true")
                                    chkfar91.Checked = true;
                            }
                            chkfar135.Checked = false;
                            if (Fleet.IsFAR135 != null && Fleet.IsFAR135.ToString().Trim() != "")
                            {
                                if (Fleet.IsFAR135 == true || Fleet.IsFAR135.ToString().ToLower() == "true")
                                    chkfar135.Checked = true;
                            }
                            TextBox tbDateofBirth = (TextBox)ucLastInspectionDate.FindControl("tbDate");
                            if (!string.IsNullOrEmpty(Fleet.LastInspectionDT.ToString()))
                            {
                                tbDateofBirth.Text = String.Format("{0:" + DateFormat + "}", Fleet.LastInspectionDT);
                                // tbDateofBirth.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Fleet.LastInspectionDT);
                            }
                            else
                            {
                                tbDateofBirth.Text = string.Empty;
                            }
                            if (Fleet.ForeGrndCustomColor != null && Fleet.ForeGrndCustomColor.ToString().Trim() != "")
                            {
                                System.Drawing.Color foreColor;
                                tbForeColor.Text = Fleet.ForeGrndCustomColor.ToString();
                                hdnForeColor.Value = Fleet.ForeGrndCustomColor.ToString();
                                foreColor = System.Drawing.Color.FromName(Convert.ToString(tbForeColor.Text, CultureInfo.CurrentCulture));
                                rcpForeColor.SelectedColor = foreColor;
                            }
                            else
                                tbForeColor.Text = "";

                            if (Fleet.BackgroundCustomColor != null && Fleet.BackgroundCustomColor.ToString().Trim() != "")
                            {
                                System.Drawing.Color backColor;
                                tbBackColor.Text = Fleet.BackgroundCustomColor.ToString();
                                hdnBackColor.Value = Fleet.BackgroundCustomColor.ToString();
                                backColor = System.Drawing.Color.FromName(Convert.ToString(tbBackColor.Text, CultureInfo.CurrentCulture));
                                rcpBackColor.SelectedColor = backColor;
                            }
                            else
                                tbBackColor.Text = "";

                            if (Fleet.Notes != null && Fleet.Notes.ToString().Trim() != "")
                                tbNotes.Text = Fleet.Notes.ToString();
                            else
                                tbNotes.Text = "";
                            if (Fleet.ForeGrndCustomColor != null && Fleet.ForeGrndCustomColor.ToString().Trim() != "")
                            {
                                System.Drawing.Color foreColor;
                                tbForeColor.Text = Fleet.ForeGrndCustomColor.ToString();
                                hdnForeColor.Value = Fleet.ForeGrndCustomColor.ToString();
                                foreColor = System.Drawing.Color.FromName(Convert.ToString(tbForeColor.Text, CultureInfo.CurrentCulture));
                                rcpForeColor.SelectedColor = foreColor;
                            }
                            else
                            {
                                tbForeColor.Text = "";
                                rcpForeColor.SelectedColor = System.Drawing.Color.White;
                            }
                            if (Fleet.BackgroundCustomColor != null && Fleet.BackgroundCustomColor.ToString().Trim() != "")
                            {
                                System.Drawing.Color BackColor;
                                tbBackColor.Text = Fleet.BackgroundCustomColor.ToString();
                                hdnBackColor.Value = Fleet.BackgroundCustomColor.ToString();
                                BackColor = System.Drawing.Color.FromName(Convert.ToString(tbBackColor.Text, CultureInfo.CurrentCulture));
                                rcpBackColor.SelectedColor = BackColor;
                            }
                            else
                            {
                                tbBackColor.Text = "";
                                rcpBackColor.SelectedColor = System.Drawing.Color.White;
                            }
                            chkinactiveairfact.Checked = false;
                            if (Fleet.IsInActive != null)
                            {
                                if (Fleet.IsInActive == true || Fleet.IsInActive.ToString().Trim() == "1")
                                {
                                    chkinactiveairfact.Checked = true;
                                }
                            }
                            lbColumnName1.Text = "Aircraft Code";
                            lbColumnName2.Text = "Tail No.";
                            lbColumnValue1.Text = System.Web.HttpUtility.HtmlEncode(Fleet.AircraftCD.ToString());
                            lbColumnValue2.Text = System.Web.HttpUtility.HtmlEncode(Fleet.TailNum.ToString());


                            var CheckFleet1 = Service.GetFleet().EntityList.Where(x => x.FleetID == Convert.ToInt64(Fleet.FleetID)).ToList();
                            if (CheckFleet1.Count > 0)
                            {
                                if (CheckFleet1[0].marginalpercentage != null)
                                {
                                    tbMarginPercentage.Text = CheckFleet1[0].marginalpercentage.ToString();
                                }
                                else
                                {
                                    tbMarginPercentage.Text = string.Empty;
                                }
                            }
                            else
                            {
                                tbMarginPercentage.Text = string.Empty;
                            }
                            break;
                        }
                    }
                    #region Get Image from Database
                    CreateDictionayForImgUpload();
                    imgFile.ImageUrl = "";
                    ImgPopup.ImageUrl = null;
                    lnkFileName.NavigateUrl = "";
                    using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ReturnImage = ImgService.GetFileWarehouseList("Fleet", Convert.ToInt64(hdnFleetID.Value)).EntityList;
                        ddlImg.Items.Clear();
                        ddlImg.Text = "";
                        int i = 0;
                        foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImage)
                        {
                            Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                            string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FWH.UWAFileName);
                            ddlImg.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                            DicImage.Add(FWH.UWAFileName, FWH.UWAFilePath);
                            if (i == 0)
                            {
                                byte[] picture = FWH.UWAFilePath;
                                
                                //Ramesh: Set the URL for image file or link based on the file type
                                SetURL(FWH.UWAFileName, picture);

                                ////start of modification for image issue
                                //if (hdnBrowserName.Value == "Chrome")
                                //{
                                //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                //}
                                //else
                                //{
                                //    Session["Base64"] = picture;
                                //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                //}
                                ////end of modification for image issue
                            }
                            i = i + 1;
                        }
                        if (ddlImg.Items.Count > 0)
                        {
                            ddlImg.SelectedIndex = 0;
                        }
                        else
                        {
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            lnkFileName.NavigateUrl = "";
                        }
                    }
                    #endregion
                    if (hdnFleetID.Value != null)
                    {
                        BindAdditionalInfoGrid(Convert.ToInt64(hdnFleetID.Value));
                    }
                    // dgFleetProfile.Rebind();
                    //  EnableForm(false);
                }
            }
        }
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string Code = Session["FPSelectedItem"].ToString().Trim();
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var CheckFleet = Service.GetFleetIDInfo(Convert.ToInt64(hdnFleetID.Value)).EntityList;
                    foreach (FlightPakMasterService.GetFleetIDInfo Fleet in CheckFleet)
                    {
                        if (Fleet.ClientID != null && Fleet.ClientID.ToString().Trim() != "")
                        {
                            if (Convert.ToInt64(Fleet.ClientID) > 0)
                            {
                                hdnClient.Value = System.Web.HttpUtility.HtmlEncode(Fleet.ClientID.ToString());
                                tbclient.Text = Fleet.ClientCD.ToString();
                            }
                            else
                            {
                                tbclient.Text = string.Empty;
                                hdnClient.Value = System.Web.HttpUtility.HtmlEncode("0");
                            }
                        }
                        else
                        {
                            tbclient.Text = string.Empty;
                            hdnClient.Value = System.Web.HttpUtility.HtmlEncode("0");
                        }
                        if (Fleet.HomebaseID != null && Fleet.HomebaseID.ToString().Trim() != "")
                        {
                            if (Convert.ToInt64(Fleet.HomebaseID) > 0)
                            {
                                hdnHomeBaseID.Value = System.Web.HttpUtility.HtmlEncode(Fleet.HomebaseID.ToString());
                                tbhomebase.Text = Convert.ToString(Fleet.HomeBaseCD);
                            }
                            else
                            {
                                hdnHomeBaseID.Value = System.Web.HttpUtility.HtmlEncode("0");
                                tbhomebase.Text = string.Empty;
                            }
                        }
                        else
                        {
                            hdnHomeBaseID.Value = System.Web.HttpUtility.HtmlEncode("0");
                            tbhomebase.Text = string.Empty;
                        }
                        if (Fleet.NationalityCountryID != null && Fleet.NationalityCountryID.ToString().Trim() != "")
                        {
                            if (Convert.ToInt64(Fleet.NationalityCountryID) > 0)
                            {
                                hdnAirCraftNationalityId.Value = Fleet.NationalityCountryID.ToString();
                                tbAirCraftNationality.Text = Convert.ToString(Fleet.CountryName);
                                hdnAirCraftNationalityName.Value = Convert.ToString(Fleet.CountryName);
                            }
                            else
                            {
                                hdnAirCraftNationalityId.Value = "0";
                                hdnAirCraftNationalityName.Value = string.Empty;
                                tbAirCraftNationality.Text = string.Empty;
                            }
                        }
                        else
                        {
                            hdnAirCraftNationalityId.Value = "0";
                            hdnAirCraftNationalityName.Value = string.Empty;
                            tbAirCraftNationality.Text = string.Empty;
                        }
                        if (Fleet.VendorID != null && Fleet.VendorID.ToString().Trim() != "")
                        {
                            if (Convert.ToInt64(Fleet.VendorID) > 0)
                            {
                                tbvendor.Text = Fleet.VendorCD.ToString();
                                hdnVendorID.Value = System.Web.HttpUtility.HtmlEncode(Fleet.VendorID.ToString());
                            }
                            else
                            {
                                tbvendor.Text = string.Empty;
                                hdnVendorID.Value = System.Web.HttpUtility.HtmlEncode("0");
                            }
                        }
                        else
                        {
                            tbvendor.Text = string.Empty;
                            hdnVendorID.Value = System.Web.HttpUtility.HtmlEncode("0");
                        }
                        if (Fleet.EmergencyContactID != null && Fleet.EmergencyContactID.ToString().Trim() != "")
                        {
                            if (Convert.ToInt64(Fleet.EmergencyContactID) > 0)
                            {
                                hdnContact.Value = System.Web.HttpUtility.HtmlEncode(Fleet.EmergencyContactID.ToString());
                                tbemergency.Text = Fleet.EmergencyContactCD.ToString();
                            }
                            else
                            {
                                hdnContact.Value = System.Web.HttpUtility.HtmlEncode("0");
                                tbemergency.Text = "";
                            }
                        }
                        else
                        {
                            hdnContact.Value = System.Web.HttpUtility.HtmlEncode("0");
                            tbemergency.Text = "";
                        }
                        if (Fleet.CrewID != null && Fleet.CrewID.ToString().Trim() != "")
                        {
                            hdnCrew.Value = System.Web.HttpUtility.HtmlEncode(Fleet.CrewID.ToString());
                            tbmaintcrew.Text = Fleet.CrewCD.ToString();
                        }
                        else
                        {
                            hdnCrew.Value = System.Web.HttpUtility.HtmlEncode("0");
                            tbmaintcrew.Text = "";
                        }
                        if (Fleet.AircraftID != null && Fleet.AircraftID.ToString().Trim() != "")
                        {
                            hdnType.Value = System.Web.HttpUtility.HtmlEncode(Fleet.AircraftID.ToString());
                            tbtypecode.Text = Fleet.AirCraft_AircraftCD.ToString();
                        }
                        else
                        {
                            hdnType.Value = System.Web.HttpUtility.HtmlEncode("0");
                            tbtypecode.Text = "";
                        }
                        if (Fleet.AircraftCD != null && Fleet.AircraftCD.ToString().Trim() != "")
                        {
                            tbaircraftcode.Text = Fleet.AircraftCD.ToString().Trim();
                        }
                        else
                            tbaircraftcode.Text = "";
                        if (Fleet.SerialNum != null && Fleet.SerialNum.ToString().Trim() != "")
                        {
                            tbserialnumber.Text = Fleet.SerialNum.ToString();
                        }
                        else
                            tbserialnumber.Text = string.Empty;
                        if (Fleet.TailNum != null && Fleet.TailNum.ToString().Trim() != "")
                        {
                            tbtailnumber.Text = Fleet.TailNum.ToString();
                        }
                        else
                            tbtailnumber.Text = string.Empty;
                        if (Fleet.MaximumPassenger != null && Fleet.MaximumPassenger.ToString().Trim() != "")
                        {
                            tbmaxpax.Text = Fleet.MaximumPassenger.ToString();
                        }
                        else
                            tbmaxpax.Text = string.Empty;
                        if (Fleet.TypeDescription != null && Fleet.TypeDescription.ToString().Trim() != "")
                        {
                            tbdescription.Text = Fleet.TypeDescription.ToString().Trim();
                        }
                        else
                            tbdescription.Text = string.Empty;
                        if (Fleet.MaximumReservation != null && Fleet.MaximumReservation.ToString().Trim() != "")
                        {
                            tbmaxrsvtns.Text = Fleet.MaximumReservation.ToString();
                        }
                        else
                            tbmaxrsvtns.Text = string.Empty;
                        if (Fleet.FlightPhoneNum != null && Fleet.FlightPhoneNum.ToString().Trim() != "")
                        {
                            tbdomflightphone.Text = Fleet.FlightPhoneNum.ToString();
                        }
                        else
                            tbdomflightphone.Text = string.Empty;
                        if (Fleet.MimimumRunway != null && Fleet.MimimumRunway.ToString().Trim() != "")
                        {
                            tbminrunway.Text = Fleet.MimimumRunway.ToString();
                        }
                        else
                            tbminrunway.Text = string.Empty;
                        if (Fleet.MaximumFuel != null && Fleet.MaximumFuel.ToString().Trim() != "")
                        {
                            tbmaxfuel.Text = Fleet.MaximumFuel.ToString();
                        }
                        else
                            tbmaxfuel.Text = string.Empty;
                        if (Fleet.TaxiFuel != null && Fleet.TaxiFuel.ToString().Trim() != "")
                        {
                            tbtaxifuel.Text = Fleet.TaxiFuel.ToString();
                        }
                        else
                            tbtaxifuel.Text = string.Empty;
                        if (Fleet.MinimumFuel != null && Fleet.MinimumFuel.ToString().Trim() != "")
                        {
                            tbminfuel.Text = Fleet.MinimumFuel.ToString();
                        }
                        else
                            tbminfuel.Text = string.Empty;
                        if (Fleet.MaximumTakeOffWeight != null && Fleet.MaximumTakeOffWeight.ToString().Trim() != "")
                        {
                            tbmaxtakeoffwt.Text = Fleet.MaximumTakeOffWeight.ToString();
                        }
                        else
                            tbmaxtakeoffwt.Text = string.Empty;
                        if (Fleet.MaximumLandingWeight != null && Fleet.MaximumLandingWeight.ToString().Trim() != "")
                        {
                            tbmaxlandingwt.Text = Fleet.MaximumLandingWeight.ToString();
                        }
                        else
                            tbmaxlandingwt.Text = string.Empty;
                        if (Fleet.MaximumWeightZeroFuel != null && Fleet.MaximumWeightZeroFuel.ToString().Trim() != "")
                        {
                            tbmaxzerofuelwt.Text = Fleet.MaximumWeightZeroFuel.ToString();
                        }
                        else
                            tbmaxzerofuelwt.Text = string.Empty;
                        if (Fleet.BasicOperatingWeight != null && Fleet.BasicOperatingWeight.ToString().Trim() != "")
                        {
                            tbbasicoperatingwt.Text = Fleet.BasicOperatingWeight.ToString();
                        }
                        else
                            tbbasicoperatingwt.Text = string.Empty;
                        if (Fleet.FlightPlanCruiseSpeed != null && Fleet.FlightPlanCruiseSpeed.ToString().Trim() != "")
                        {
                            tbcruisespeed.Text = Fleet.FlightPlanCruiseSpeed.ToString();
                        }
                        else
                            tbcruisespeed.Text = string.Empty;
                        if (Fleet.FlightPlanMaxFlightLevel != null && Fleet.FlightPlanMaxFlightLevel.ToString().Trim() != "")
                        {
                            tbmaxflightlevel.Text = Fleet.FlightPlanMaxFlightLevel.ToString();
                        }
                        else
                            tbmaxflightlevel.Text = string.Empty;
                        if (Fleet.WarningHrs != null && Fleet.WarningHrs.ToString().Trim() != "")
                        {
                            tbhourstowarning.Text = Fleet.WarningHrs.ToString();
                        }
                        else
                            tbhourstowarning.Text = string.Empty;
                        if (Fleet.InspectionHrs != null && Fleet.InspectionHrs.ToString().Trim() != "")
                        {
                            tbhourstonext.Text = Fleet.InspectionHrs.ToString();
                        }
                        else
                            tbhourstonext.Text = string.Empty;
                        if (Fleet.Class.ToString().Trim() != "" && Convert.ToInt32(Fleet.Class.ToString().Trim()) > 0)
                        {
                            drpairfactclass.SelectedValue = Fleet.Class.ToString().Trim();
                        }
                        else
                            drpairfactclass.SelectedIndex = 0;
                        if (Fleet.FleetSize != null)
                        {
                            if (Fleet.FleetSize.ToString() == "S" || Fleet.FleetSize.ToString() == "M" || Fleet.FleetSize.ToString() == "L")
                            {
                                drpaircrafttype.SelectedValue = Fleet.FleetSize.ToString().Trim();
                            }
                            else
                            {
                                drpaircrafttype.SelectedIndex = 0;
                            }
                        }
                        else
                            drpaircrafttype.SelectedIndex = 0;
                        if (Fleet.FlightPhoneIntlNum != null && Fleet.FlightPhoneIntlNum.ToString().Trim() != "")
                        {
                            tblntlflightphone.Text = Fleet.FlightPhoneIntlNum.ToString().Trim();
                        }
                        else
                            tblntlflightphone.Text = string.Empty;
                        if (Fleet.SIFLMultiControlled != null && Fleet.SIFLMultiControlled.ToString().Trim() != "")
                        {
                            tbcontrol.Text = Fleet.SIFLMultiControlled.ToString();
                        }
                        else
                            tbcontrol.Text = "0.0";
                        if (Fleet.SIFLMultiNonControled != null && Fleet.SIFLMultiNonControled.ToString().Trim() != "")
                        {
                            tbnoncontrol.Text = Fleet.SIFLMultiNonControled.ToString();
                        }
                        else
                            tbnoncontrol.Text = "0.0";
                        if (!string.IsNullOrEmpty(Fleet.SecondaryDomFlightPhone))
                        {
                            tbSecondaryDomFlightPhone.Text = Fleet.SecondaryDomFlightPhone.ToString();
                        }
                        else
                        {
                            tbSecondaryDomFlightPhone.Text = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(Fleet.SecondaryIntlFlightPhone))
                        {
                            tbSecondaryIntlFlightPhone.Text = Fleet.SecondaryIntlFlightPhone.ToString();
                        }
                        else
                        {
                            tbSecondaryIntlFlightPhone.Text = string.Empty;
                        }
                        if (Fleet.MultiSec != null && Fleet.MultiSec.ToString().Trim() != "")
                        {
                            tbseccontrol.Text = Fleet.MultiSec.ToString().Trim();
                        }
                        else
                            tbseccontrol.Text = "0.0";
                        chkfar91.Checked = false;
                        if (Fleet.IsFAR91 != null && Fleet.IsFAR91.ToString().Trim() != "")
                        {
                            if (Fleet.IsFAR91 == true || Fleet.IsFAR91.ToString().ToLower() == "true")
                                chkfar91.Checked = true;
                        }
                        chkfar135.Checked = false;
                        if (Fleet.IsFAR135 != null && Fleet.IsFAR135.ToString().Trim() != "")
                        {
                            if (Fleet.IsFAR135 == true || Fleet.IsFAR135.ToString().ToLower() == "true")
                                chkfar135.Checked = true;
                        }
                        TextBox tbDateofBirth = (TextBox)ucLastInspectionDate.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(Fleet.LastInspectionDT.ToString()))
                        {
                            tbDateofBirth.Text = String.Format("{0:" + DateFormat + "}", Fleet.LastInspectionDT);
                            //tbDateofBirth.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Fleet.LastInspectionDT);
                        }
                        else
                        {
                            tbDateofBirth.Text = string.Empty;
                        }
                        //if (Fleet.ForeGrndCustomColor != null && Fleet.ForeGrndCustomColor.ToString().Trim() != "")
                        //{
                        //    tbForeColor.Text = Fleet.ForeGrndCustomColor.ToString();
                        //}
                        //else
                        //    tbForeColor.Text = "";
                        //if (Fleet.BackgroundCustomColor != null && Fleet.BackgroundCustomColor.ToString().Trim() != "")
                        //    tbBackColor.Text = Fleet.BackgroundCustomColor.ToString();
                        //else
                        //    tbBackColor.Text = "";
                        if (Fleet.Notes != null && Fleet.Notes.ToString().Trim() != "")
                            tbNotes.Text = Fleet.Notes.ToString();
                        else
                            tbNotes.Text = "";
                        if (Fleet.ForeGrndCustomColor != null && Fleet.ForeGrndCustomColor.ToString().Trim() != "")
                        {
                            System.Drawing.Color foreColor;
                            tbForeColor.Text = Fleet.ForeGrndCustomColor.ToString();
                            hdnForeColor.Value = System.Web.HttpUtility.HtmlEncode(Fleet.ForeGrndCustomColor.ToString());
                            foreColor = System.Drawing.Color.FromName(Convert.ToString(tbForeColor.Text, CultureInfo.CurrentCulture));
                            rcpForeColor.SelectedColor = foreColor;
                        }
                        else
                        {
                            tbForeColor.Text = "";
                            rcpForeColor.SelectedColor = System.Drawing.Color.White;
                        }
                        if (Fleet.BackgroundCustomColor != null && Fleet.BackgroundCustomColor.ToString().Trim() != "")
                        {
                            System.Drawing.Color BackColor;
                            tbBackColor.Text = Fleet.BackgroundCustomColor.ToString();
                            hdnBackColor.Value = System.Web.HttpUtility.HtmlEncode(Fleet.BackgroundCustomColor.ToString());
                            BackColor = System.Drawing.Color.FromName(Convert.ToString(tbBackColor.Text, CultureInfo.CurrentCulture));
                            rcpBackColor.SelectedColor = BackColor;
                        }
                        else
                        {
                            tbBackColor.Text = "";
                            rcpBackColor.SelectedColor = System.Drawing.Color.White;
                        }
                        chkinactiveairfact.Checked = false;
                        if (Fleet.IsInActive != null)
                        {
                            if (Fleet.IsInActive == true || Fleet.IsInActive.ToString().Trim() == "1")
                            {
                                chkinactiveairfact.Checked = true;
                            }
                        }
                    }
                    #region Get Image from Database
                    CreateDictionayForImgUpload();
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("");
                    ImgPopup.ImageUrl = null;
                    lnkFileName.NavigateUrl = "";
                    using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ReturnImage = ImgService.GetFileWarehouseList("Fleet", Convert.ToInt64(hdnFleetID.Value)).EntityList;
                        ddlImg.Items.Clear();
                        ddlImg.Text = "";
                        int i = 0;
                        foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImage)
                        {
                            Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                            string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FWH.UWAFileName);
                            ddlImg.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                            DicImage.Add(FWH.UWAFileName, FWH.UWAFilePath);
                            if (i == 0)
                            {
                                byte[] picture = FWH.UWAFilePath;

                                //Ramesh: Set the URL for image file or link based on the file type
                                SetURL(FWH.UWAFileName, picture);

                                ////start of modification for image issue
                                //if (hdnBrowserName.Value == "Chrome")
                                //{
                                //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                //}
                                //else
                                //{
                                //    Session["Base64"] = picture;
                                //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?cd=" + Guid.NewGuid());
                                //}
                                ////end of modification for image issue
                            }
                            i = i + 1;
                        }
                        if (ddlImg.Items.Count > 0)
                        {
                            ddlImg.SelectedIndex = 0;
                        }
                        else
                        {
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            lnkFileName.NavigateUrl = "";
                        }
                        if (hdnFleetID.Value != null)
                        {
                            BindAdditionalInfoGrid(Convert.ToInt64(hdnFleetID.Value));
                        }
                    }
                    #endregion
                    hdnSave.Value = System.Web.HttpUtility.HtmlEncode("Update");
                    hdnRedirect.Value = "";
                    dgFleetProfile.Rebind();
                    EnableForm(false);
                }
            }
        }
        protected void dgFleetProfile_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectFleetItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem Item = dgFleetProfile.SelectedItems[0] as GridDataItem;
                                Session["FPSelectedItem"] = Item["FleetID"].Text;
                                Session["FleetID"] = Item["FleetID"].Text.ToString();
                                hdnFleetID.Value = Item["FleetID"].Text.ToString();
                                hdnCustomerID.Value = Item["CustomerID"].Text.ToString();
                                ReadOnlyForm();

                                GridEnable(true, true, true);
                                Item.Selected = true;
                                Label lblUser = (Label)dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString().Trim());
                                }
                                else
                                {
                                    lblUser.Text = string.Empty;
                                }
                                if(Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                                }
                            }
                            else
                            {
                                if (hdnSave.Value == "Update")
                                {
                                    if (Session["FPSelectedItem"] != null)
                                    {
                                        //GridDataItem items = (GridDataItem)Session["SelectedCrewRosterID"];
                                        string CrewID = Session["FPSelectedItem"].ToString();
                                        //   string Code = items.GetDataKeyValue("CrewCD").ToString();
                                        int CurrentPageIndex = dgFleetProfile.CurrentPageIndex;
                                        for (int Page = 0; Page < dgFleetProfile.PageCount; Page++)
                                        {
                                            foreach (GridDataItem Item in dgFleetProfile.Items)
                                            {
                                                if (Item["FleetID"].Text.Trim() == Convert.ToString(Session["FPSelectedItem"]))
                                                {
                                                    Item.Selected = true;
                                                    CurrentPageIndex = -1; // flag exit
                                                    break;
                                                }
                                            }
                                            // If item is found then exit dgSiflRateCatalog page loop
                                            if (CurrentPageIndex.Equals(-1))
                                            {
                                                break;
                                            }
                                            // Go to next dgSiflRateCatalog page
                                            CurrentPageIndex++;
                                            if (CurrentPageIndex >= dgFleetProfile.PageCount)
                                            {
                                                CurrentPageIndex = 0;
                                            }
                                            dgFleetProfile.CurrentPageIndex = CurrentPageIndex;
                                            dgFleetProfile.Rebind();
                                        }
                                    }
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                    }
                }
            }
        }
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbaircraftcode.Text = string.Empty;
                tbserialnumber.Text = string.Empty;
                tbtailnumber.Text = string.Empty;
                tbmaxpax.Text = string.Empty;
                tbdescription.Text = string.Empty;
                tbmaxrsvtns.Text = string.Empty;
                tbdomflightphone.Text = string.Empty;
                tbminrunway.Text = string.Empty;
                tblntlflightphone.Text = string.Empty;
                tbmaxfuel.Text = string.Empty;
                tbminfuel.Text = string.Empty;
                tbtaxifuel.Text = string.Empty;
                tbmaxtakeoffwt.Text = string.Empty;
                tbMarginPercentage.Text = "0.00";
                tbAirCraftNationality.Text = string.Empty;
                hdnAirCraftNationalityId.Value = "0";
                hdnAirCraftNationalityName.Value = string.Empty;
                tbmaxlandingwt.Text = string.Empty;
                tbmaxzerofuelwt.Text = string.Empty;
                tbbasicoperatingwt.Text = string.Empty;
                tbcruisespeed.Text = string.Empty;
                tbmaxflightlevel.Text = string.Empty;
                tbhourstowarning.Text = string.Empty;
                tbhourstonext.Text = string.Empty;
                tbcontrol.Text = "0.0";
                tbnoncontrol.Text = "0.0";
                tbseccontrol.Text = "0.0";
                drpaircrafttype.SelectedIndex = 0;
                chkfar91.Checked = false;
                chkfar135.Checked = false;
                chkinactiveairfact.Checked = false;
                drpairfactclass.SelectedIndex = 0;
                tbmaintcrew.Text = "";
                tbtypecode.Text = "";
                TextBox tbDateofBirth = (TextBox)ucLastInspectionDate.FindControl("tbDate");
                tbDateofBirth.Text = string.Empty;
                hdnType.Value = "0";
                hdnClient.Value = "0";
                hdnVendorID.Value = "0";
                hdnContact.Value = "0";
                hdnCrew.Value = "0";
                hdnHomeBaseID.Value = "0";
                hdnFleetID.Value = "0";
                ViewState["EditFleetID"] = null;
                ViewState["RefreshGrid"] = null;
                tbhomebase.Text = string.Empty;
                tbvendor.Text = string.Empty;
                tbemergency.Text = string.Empty;
                tbclient.Text = string.Empty;
                CreateDictionayForImgUpload();
                fileUL.Enabled = false;
                ddlImg.Enabled = false;
                imgFile.ImageUrl = "";
                ImgPopup.ImageUrl = null;
                lnkFileName.NavigateUrl = "";
                ddlImg.Items.Clear();
                ddlImg.Text = "";
                tbNotes.Text = "";
                tbImgName.Text = "";
                Session.Remove("FleetProfileDefinition");
                Session.Remove("FleetAdditionalInfo");
                BindAdditionalInfoGrid(0);
                rcpBackColor.SelectedColor = System.Drawing.Color.White;
                rcpForeColor.SelectedColor = System.Drawing.Color.White;
                tbForeColor.Text = string.Empty;
                hdnForeColor.Value = string.Empty;
                tbBackColor.Text = string.Empty;
                hdnBackColor.Value = string.Empty;
                tbSecondaryDomFlightPhone.Text = string.Empty;
                tbSecondaryIntlFlightPhone.Text = string.Empty;
                pnlExternalForm.Visible = true;
            }
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSave.Value = "Save";
                ClearForm();
                EnableForm(true);
                btnCancel.Visible = true;
                btnSaveChanges.Visible = true;
                btnSaveTop.Visible = true;
                btnCancelTop.Visible = true;
                ddlImg.Items.Clear();
                ddlImg.Text = "";
                fileUL.Enabled = false;
                ddlImg.Enabled = false;
                imgFile.ImageUrl = "";
                ImgPopup.ImageUrl = null;
                lnkFileName.NavigateUrl = "";
                hdnFleetID.Value = "0";
                btndeleteImage.Enabled = false;
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                if (UserPrincipal.Identity._homeBaseId != null && UserPrincipal.Identity._airportICAOCd != null)
                {
                    hdnHomeBaseID.Value = UserPrincipal.Identity._homeBaseId.ToString().Trim();
                    tbhomebase.Text = UserPrincipal.Identity._airportICAOCd.ToString().Trim();
                }
                drpairfactclass.SelectedValue = "1";
                //Session["FleetAdditionalInfo"] = null;
                //BindAdditionalInfoGrid(0);
            }
        }
        #region "Fleet Profile"
        protected void Forecast_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["FleetID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Session["FleetID"].ToString()))
                            {
                                GetControlState();
                                RedirectToPage(string.Format("{0}?FleetID={1}&TailNum={2}&Screen=FleetProfile", WebConstants.URL.FleetForecast, Microsoft.Security.Application.Encoder.HtmlEncode(hdnFleetID.Value), Microsoft.Security.Application.Encoder.HtmlEncode(tbtailnumber.Text)));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void Component_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["FleetID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Session["FleetID"].ToString()))
                            {
                                GetControlState();
                                string FleetID = Session["FleetID"].ToString().Trim();
                                RedirectToPage(String.Format("{0}?FleetID={1}&TailNum={2}&Screen=FleetProfile", WebConstants.URL.FleetComponentHistory, Microsoft.Security.Application.Encoder.HtmlEncode(hdnFleetID.Value), Microsoft.Security.Application.Encoder.HtmlEncode(tbtailnumber.Text)));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }

        protected void Charterrates_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["FleetID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Session["FleetID"].ToString()))
                            {
                                GetControlState();
                                Session["IsFleet"] = "true";
                                string FleetID = Session["FleetID"].ToString().Trim();
                                Session["DispTypeCode"] = System.Web.HttpUtility.HtmlEncode(tbtypecode.Text);
                                Session["DisplayDescription"] = System.Web.HttpUtility.HtmlEncode(tbdescription.Text);
                                    Session["SelectedFleetNewCharterID"] = null;
                                    RedirectToPage(String.Format("{0}?FleetID={1}&TailNum={2}&Screen=FleetProfile", WebConstants.URL.FleetNewCharterRate, Microsoft.Security.Application.Encoder.HtmlEncode(hdnFleetID.Value), Microsoft.Security.Application.Encoder.HtmlEncode(tbtailnumber.Text)));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }


        protected void FleetAdditionalInfo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetControlState();
                        Response.Redirect("FleetProfileAdditionalInfoCatalog.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void Intl_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetControlState();
                        RedirectToPage(string.Format("{0}?FleetID={1}&TailNum={2}&Screen=FleetProfile", WebConstants.URL.FleetProfileInternationalData, Microsoft.Security.Application.Encoder.HtmlEncode(hdnFleetID.Value), Microsoft.Security.Application.Encoder.HtmlEncode(tbtailnumber.Text)));
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void Chargerates_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["FleetID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Session["FleetID"].ToString()))
                            {
                                GetControlState();
                                string FleetID = Session["FleetID"].ToString().Trim();
                                RedirectToPage(string.Format("{0}?FleetID={1}&TailNum={2}&Screen=FleetProfile", WebConstants.URL.FleetChargeHistory, Microsoft.Security.Application.Encoder.HtmlEncode(FleetID), Microsoft.Security.Application.Encoder.HtmlEncode(tbtailnumber.Text)));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        protected void Engineairframe_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetControlState();
                        RedirectToPage(String.Format("{0}?FleetID={1}&TailNum={2}&Screen=FleetProfile", WebConstants.URL.FleetProfileEngineAirframeInfo, Microsoft.Security.Application.Encoder.HtmlEncode(hdnFleetID.Value), Microsoft.Security.Application.Encoder.HtmlEncode(tbtailnumber.Text)));
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        #endregion
        private void BindAircraftClassCombo()
        {
            //Fleet Profile Catalog :  Aircraft Type
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, string> AircraftClasses = new Dictionary<string, string> 
           { 
	         {"1", "1 - Single Engine Land"},
             {"2", "2 - Multi-Engine Land"},  
	         {"3", "3 - Single Engine Sea"},
	         {"4", "4 - Multi-Engine Sea"},
             {"5", "5 - Turbojet"},  
	         {"6", "6 - Helicopter"},
           };
                foreach (KeyValuePair<string, string> Item in AircraftClasses)
                {
                    drpairfactclass.Items.Add(new ListItem(Item.Value, Item.Key));
                }
                drpairfactclass.Items.Insert(0, "Select");
                drpairfactclass.SelectedIndex = 0;
            }
        }
        private void BindAircraftTypeCombo()
        {
            //Fleet Profile Catalog :  Aircraft Class   
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, string> AircraftTypes = new Dictionary<string, string> 
           { 
              {"S", "S - Small"},
              {"M", "M - Medium"},  
	          {"L", "L - Large"},	
           };
                foreach (KeyValuePair<string, string> Item in AircraftTypes)
                {
                    drpaircrafttype.Items.Add(new ListItem(Item.Value, Item.Key));
                }
                drpaircrafttype.Items.Insert(0, "Select");
                drpaircrafttype.SelectedIndex = 0;
            }
        }
        protected void tbhomebase_TextChanged(object sender, EventArgs e)
        {
            CheckHomeBaseExist();
        }
        private bool CheckHomeBaseExist()
        {
            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            //{
            //    bool ReturnValue = true;
            //    if (tbhomebase.Text.Trim() == "undefined")
            //        tbhomebase.Text = "";
            //    if (tbhomebase.Text.Trim() != "")
            //    {
            //        using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
            //        {
            //            var HomeBaseVal = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.ToString().ToUpper().Trim().Equals(tbhomebase.Text.ToString().ToUpper().Trim()));
            //            if (HomeBaseVal.Count() > 0 && HomeBaseVal != null)
            //            {
            //                foreach (FlightPakMasterService.GetAllCompanyMaster HomeBaseCode in HomeBaseVal)
            //                {
            //                    tbhomebase.Text = HomeBaseCode.HomebaseCD.ToString().ToUpper().Trim();
            //                    hdnHomeBaseID.Value = HomeBaseCode.HomebaseID.ToString().ToUpper().Trim();
            //                    RadAjaxManager1.FocusControl(tbtypecode.ClientID);
            //                    cvHomeBase.Text = string.Empty;
            //                }
            //            }
            //            else
            //            {
            //                cvHomeBase.IsValid = false;
            //                RadAjaxManager1.FocusControl(tbhomebase.ClientID);
            //                return false;
            //            }
            //        }
            //    }
            //    return ReturnValue;
            //}
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbhomebase.Text != string.Empty) && (tbhomebase.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient HomeBaseService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var HomeBaseValue = HomeBaseService.GetAirportByAirportICaoID(tbhomebase.Text).EntityList;
                        if (HomeBaseValue.Count() == 0 || HomeBaseValue == null)
                        {
                            cvHomeBase.IsValid = false;
                            tbhomebase.Text = string.Empty;
                            hdnHomeBaseID.Value = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbhomebase);
                            return false;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.GetAllAirport cm in HomeBaseValue)
                            {
                                hdnHomeBaseID.Value = cm.AirportID.ToString();
                                tbhomebase.Text = cm.IcaoID;
                            }
                            return true;
                        }
                    }
                }
                else
                {
                    tbhomebase.Text = string.Empty;
                    hdnHomeBaseID.Value = string.Empty;
                }
                return true;
            }
        }
        protected void tbtypecode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckTypeCodeExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private bool CheckTypeCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (tbtypecode.Text.Trim() == "undefined")
                    tbtypecode.Text = "";
                if (tbtypecode.Text.Trim() != "")
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var TypeCodeVal = Service.GetAircraftList().EntityList.Where(x => x.AircraftCD.ToString().ToUpper().Trim().Equals(tbtypecode.Text.ToString().ToUpper().Trim()));
                        if (TypeCodeVal.Count() > 0 && TypeCodeVal != null)
                        {
                            foreach (FlightPakMasterService.GetAllAircraft TypCode in TypeCodeVal)
                            {
                                tbtypecode.Text = TypCode.AircraftCD.ToString().Trim();
                                tbdescription.Text = TypCode.AircraftDescription.ToString().ToUpper().Trim();
                                hdnType.Value = TypCode.AircraftID.ToString();
                                RadAjaxManager1.FocusControl(tbmaxpax.ClientID);
                            }
                        }
                        else
                        {
                            cvTypeCode.IsValid = false;
                            tbdescription.Text = string.Empty;
                            hdnType.Value = string.Empty;
                            RadAjaxManager1.FocusControl(tbtypecode.ClientID);
                            return false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        protected void tbclient_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckClientExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private bool CheckClientExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (tbclient.Text.Trim() == "undefined")
                    tbclient.Text = "";
                if (tbclient.Text.Trim() != "")
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Clients = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ClientVal = Clients.GetClientCodeList().EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbclient.Text.ToString().ToUpper().Trim()));
                        if (ClientVal.Count() > 0 && ClientVal != null)
                        {
                            foreach (FlightPakMasterService.Client ClientCode in ClientVal)
                            {
                                tbclient.Text = ClientCode.ClientCD.ToString().Trim();
                                hdnClient.Value = ClientCode.ClientID.ToString();
                                RadAjaxManager1.FocusControl(tbvendor.ClientID);
                            }
                        }
                        else
                        {
                            cvClient.IsValid = false;
                            RadAjaxManager1.FocusControl(tbclient.ClientID);
                            return false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        protected void tbvendor_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckVendorExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private bool CheckVendorExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (tbvendor.Text.Trim() == "undefined")
                    tbvendor.Text = "";
                if (tbvendor.Text.Trim() != "")
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Vendors = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var VendorVal = Vendors.GetVendorInfo().EntityList.Where(x => x.VendorCD.ToString().ToUpper().Trim().Equals(tbvendor.Text.ToString().ToUpper().Trim()));
                        if (VendorVal.Count() > 0 && VendorVal != null)
                        {
                            foreach (FlightPakMasterService.GetAllVendor VendorCode in VendorVal)
                            {
                                tbvendor.Text = VendorCode.VendorCD.ToString().Trim();
                                hdnVendorID.Value = VendorCode.VendorID.ToString();
                                RadAjaxManager1.FocusControl(drpairfactclass.ClientID);
                            }
                        }
                        else
                        {
                            cvVendor.IsValid = false;
                            RadAjaxManager1.FocusControl(tbvendor.ClientID);
                            return false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        protected void tbmaintcrew_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckMaintCrewExists();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private bool CheckMaintCrewExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (tbmaintcrew.Text.Trim() == "undefined")
                    tbmaintcrew.Text = "";
                if (tbmaintcrew.Text.Trim() != "")
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CrewVal = Service.GetCrewList().EntityList.Where(x => x.CrewCD != null && x.CrewCD.ToString().ToUpper().Trim().Equals(tbmaintcrew.Text.ToString().ToUpper().Trim()));
                        if (CrewVal.Count() > 0 && CrewVal != null)
                        {
                            foreach (FlightPakMasterService.GetAllCrew Crews in CrewVal)
                            {
                                tbmaintcrew.Text = Crews.CrewCD.ToString().Trim();
                                hdnCrew.Value = Crews.CrewID.ToString();
                                RadAjaxManager1.FocusControl(tbclient.ClientID);
                            }
                        }
                        else
                        {
                            cvMaintCrew.IsValid = false;
                            RadAjaxManager1.FocusControl(tbmaintcrew.ClientID);
                            return false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        protected void tbemergency_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckEmergencyExists();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private bool CheckEmergencyExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (tbemergency.Text.Trim() == "undefined")
                    tbemergency.Text = "";
                if (tbemergency.Text.Trim() != "")
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var EmergencyVal = Service.GetEmergencyList().EntityList.Where(x => x.EmergencyContactCD.ToString().ToUpper().Trim().Equals(tbemergency.Text.ToString().ToUpper().Trim()));
                        if (EmergencyVal.Count() > 0 && EmergencyVal != null)
                        {
                            foreach (FlightPakMasterService.EmergencyContact EContact in EmergencyVal)
                            {
                                tbemergency.Text = EContact.EmergencyContactCD.ToString().Trim();
                                hdnContact.Value = EContact.EmergencyContactID.ToString();
                                RadAjaxManager1.FocusControl(chkfar91.ClientID);
                            }
                        }
                        else
                        {
                            cvEmergency.IsValid = false;
                            RadAjaxManager1.FocusControl(tbemergency.ClientID);
                            return false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        protected void FleetTailNum_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckFleetTailNumExists();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private bool CheckFleetTailNumExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ReturnFleet = Service.GetFleet().EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbtailnumber.Text.ToString().ToUpper().Trim())).ToList();
                    if (ReturnFleet.Count() > 0 && ReturnFleet != null)
                    {
                        foreach (FlightPakMasterService.GetFleet cm in ReturnFleet)
                        {
                            cvTailNum.IsValid = false;
                            tbtailnumber.Text = cm.TailNum;
                            RadAjaxManager1.FocusControl(tbtailnumber.ClientID);
                            return true;
                        }
                    }
                    return ReturnValue;
                }
            }
        }
        protected void FleetCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckFleetCodeExists();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        private bool CheckFleetCodeExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ReturnFleet = Service.GetFleet().EntityList.Where(x => x.AircraftCD.ToString().ToUpper().Trim().Equals(tbaircraftcode.Text.ToString().ToUpper().Trim())).ToList();
                    if (ReturnFleet.Count() > 0 && ReturnFleet != null)
                    {
                        //foreach (FlightPakMasterService.GetFleet cm in ReturnFleet)
                        //{
                        cvAircraftCode.IsValid = false;
                        tbaircraftcode.Text = ((GetFleet)ReturnFleet[0]).AircraftCD;
                        RadAjaxManager1.FocusControl(tbaircraftcode.ClientID);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbaircraftcode.ClientID + "');", true);
                        ReturnValue = true;
                        //}
                    }
                    else
                    {
                        RadAjaxManager1.FocusControl(tbserialnumber.ClientID);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbserialnumber.ClientID + "');", true);
                        ReturnValue = false;
                    }
                    return ReturnValue;
                }
            }
        }
        private void SelectItem(string itm)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(itm))
            {
                string Code = string.Empty;
                if (Session["FPSelectedItem"] != null && Session["FPSelectedItem"].ToString().Trim() != "")
                {
                    Code = Session["FPSelectedItem"].ToString().Trim();
                }
                else if (itm.Trim() != "")
                {
                    Code = itm;
                }
                else
                {
                    DefaultSelection();
                    return;
                }
                foreach (GridDataItem Item in dgFleetProfile.MasterTableView.Items)
                {
                    if (Item["FleetID"].Text.Trim() == Code)
                    {
                        Item.Selected = true;
                        Session["FPSelectedItem"] = Item["FleetID"].Text;
                        Session["aircraftCode"] = Item["AircraftCD"].Text.ToString();
                        Session["FleetID"] = Item["FleetID"].Text.ToString();
                        hdnFleetID.Value = Item["FleetID"].Text.ToString();
                        Session.Remove("FleetProfileDefinition");
                        Label lblUser = (Label)dgFleetProfile.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item["LastUpdUID"].Text.Trim() != "")
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item["LastUpdUID"].Text.Trim());
                        }
                        else
                        {
                            lblUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                        }
                        break;
                    }
                }
            }
        }
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                        int count = DicImage.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            DicImage.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = DicImage;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                            {
                                dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            }
                            Session["DicImgDelete"] = dicImgDelete;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in DicImage)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            if (ddlImg.Items.Count > 0)
                            {
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                if (Count > 0)
                                {
                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                            }
                            else
                            {
                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                lnkFileName.NavigateUrl = "";
                            }
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        //veracode fix for 2578
        //public static byte[] ImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream FileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[FileStream.Length];
        //        FileStream.Read(buffer, 0, (int)FileStream.Length);
        //        FileStream.Close();
        //        return buffer;
        //    }
        //}
        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream MyStream;
                Int32 FileLen;
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        string FileName = fileUL.FileName;
                        FileLen = fileUL.PostedFile.ContentLength;
                        Byte[] Input = new Byte[FileLen];
                        MyStream = fileUL.FileContent;
                        MyStream.Read(Input, 0, FileLen);

                        //Ramesh: Set the URL for image file or link based on the file type
                        SetURL(FileName, Input);

                        ////start of modification for image issue
                        //if (hdnBrowserName.Value == "Chrome")
                        //{
                        //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(Input));
                        //}
                        //else
                        //{
                        //    Session["Base64"] = Input;
                        //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?cd=" + Guid.NewGuid());
                        //}
                        ////end of modification for image issue
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                        //Commented for removing document name
                        //if (tbImgName.Text.Trim() != "")
                        //{
                        //    FileName = tbImgName.Text.Trim();
                        //}
                        int Count = DicImage.Count(D => D.Key.Equals(FileName));
                        if (Count > 0)
                        {
                            DicImage[FileName] = Input;
                        }
                        else
                        {
                            DicImage.Add(FileName, Input);
                            string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FileName);
                            ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(encodedFileName, encodedFileName));

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);
                        }
                        tbImgName.Text = "";
                        if (ddlImg.Items.Count != 0)
                        {
                            //ddlImg.SelectedIndex = ddlImg.Items.Count - 1;
                            ddlImg.SelectedValue = FileName;
                        }
                        btndeleteImage.Enabled = true;
                    }
                }
            }
        }
        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool ImgFound = false;
                            Session["Base64"] = null;   //added for image issue
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ReturnImg = ImgService.GetFileWarehouseList("Fleet", Convert.ToInt64(hdnFleetID.Value)).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());
                                foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImg)
                                {
                                    ImgFound = true;                                    
                                    byte[] Picture = FWH.UWAFilePath;

                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(FWH.UWAFileName, Picture);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Picture);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = Picture;
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                                //tbImgName.Text = ddlImg.Text.Trim(); //Commented for removing document name
                                if (!ImgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (Count > 0)
                                    {

                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";TailNum=" + tbtailnumber.Text;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        #endregion
        #region "Passenger Additional Information"
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                // Additional Info
                                // Declaring Lists
                                List<FlightPakMasterService.GetFleetProfileDefinition> FinalList = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                                List<FlightPakMasterService.GetFleetProfileDefinition> RetainList = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                                List<FlightPakMasterService.GetFleetProfileDefinition> NewList = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                                // Retain Grid Items and bind into List and add into Final List
                                RetainList = RetainPassengerAddlInfoGrid(dgPassengerAddlInfo);
                                FinalList.AddRange(RetainList);
                                // Bind Selected New Item and add into Final List
                                if (Session["FleetAdditionalInfoNew"] != null)
                                {
                                    NewList = (List<FlightPakMasterService.GetFleetProfileDefinition>)Session["FleetAdditionalInfoNew"];
                                    FinalList.AddRange(NewList);
                                }
                                // Bind final list into Session
                                Session["FleetProfileDefinition"] = FinalList;
                                // Bind final list into Grid
                                dgPassengerAddlInfo.DataSource = FinalList.Where(x => x.IsDeleted == false).ToList();
                                dgPassengerAddlInfo.DataBind();
                                // Empty Newly added Session item
                                Session.Remove("FleetAdditionalInfoNew");
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        /// <summary>
        /// Delete Selected Crew Roster Additional Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteInfo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem Item = (GridDataItem)dgPassengerAddlInfo.SelectedItems[0];
                        Int64 FleetProfileInfoXRefID = Convert.ToInt64(Item.GetDataKeyValue("FleetProfileInfoXRefID").ToString());
                        string FleetInfoCD = Item.GetDataKeyValue("FleetInfoCD").ToString();
                        List<FlightPakMasterService.GetFleetProfileDefinition> FleetProfileDefinitionList = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                        FleetProfileDefinitionList = (List<FlightPakMasterService.GetFleetProfileDefinition>)Session["FleetProfileDefinition"];
                        for (int Index = 0; Index < FleetProfileDefinitionList.Count; Index++)
                        {
                            if (FleetProfileDefinitionList[Index].FleetInfoCD == FleetInfoCD)
                            {
                                FleetProfileDefinitionList[Index].IsDeleted = true;
                            }
                        }
                        Session["FleetProfileDefinition"] = FleetProfileDefinitionList;
                        List<FlightPakMasterService.GetFleetProfileDefinition> FleetProfileDefinitionInfoList = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                        // Retain the Grid Items and bind into List to remove the selected item.
                        FleetProfileDefinitionInfoList = RetainPassengerAddlInfoGrid(dgPassengerAddlInfo);
                        dgPassengerAddlInfo.DataSource = FleetProfileDefinitionInfoList.Where(x => x.IsDeleted == false).ToList();
                        dgPassengerAddlInfo.DataBind();
                        Session["FleetProfileDefinition"] = FleetProfileDefinitionList;// PassengerAdditionalInfoList;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }
        /// <summary>
        /// Method to Bind Additional Information Grid, based on Crew Code
        /// </summary>
        /// <param name="crewCode">Pass Crew Code</param>
        private void BindAdditionalInfoGrid(Int64 FleetID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID))
            {
                //if (Session["FleetProfileDefinition"] != null)
                //{
                //    List<FlightPakMasterService.GetFleetProfileDefinition> FleetProfileDefinition = (List<FlightPakMasterService.GetFleetProfileDefinition>)Session["FleetProfileDefinition"];
                //    dgPassengerAddlInfo.DataSource = FleetProfileDefinition;
                //    dgPassengerAddlInfo.DataBind();
                //}
                //else
                //{
                using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                {
                    List<FlightPakMasterService.GetFleetProfileDefinition> PassengerAdditionalInfoList = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                    var PassengerAddlInfo = Service.GetFleetAdditionalInfoList();
                    if (PassengerAddlInfo.ReturnFlag == true)
                    {
                        PassengerAdditionalInfoList = PassengerAddlInfo.EntityList.Where(x => x.FleetID == FleetID && x.IsDeleted == false).ToList<FlightPakMasterService.GetFleetProfileDefinition>();
                        dgPassengerAddlInfo.DataSource = PassengerAdditionalInfoList;
                        dgPassengerAddlInfo.DataBind();
                        Session["FleetProfileDefinition"] = PassengerAdditionalInfoList;
                    }
                }
                //}
            }
        }
        /// <summary>
        /// Method to Retain Additional Info Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Definition List</returns>
        private List<FlightPakMasterService.GetFleetProfileDefinition> RetainPassengerAddlInfoGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                //List<FlightPakMasterService.GetFleetProfileDefinition> PassengerAdditionalInfoList = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                //foreach (GridDataItem item in grid.MasterTableView.Items)
                //{
                //    FlightPakMasterService.GetFleetProfileDefinition PassengerAdditionalInfoDef = new FlightPakMasterService.GetFleetProfileDefinition();
                //    PassengerAdditionalInfoDef.FleetProfileInfoXRefID = Convert.ToInt64(item.GetDataKeyValue("FleetProfileInfoXRefID").ToString());
                //    PassengerAdditionalInfoDef.FleetProfileInformationID = Convert.ToInt64(item.GetDataKeyValue("FleetProfileInformationID").ToString());
                //    PassengerAdditionalInfoDef.FleetInfoCD = item.GetDataKeyValue("FleetInfoCD").ToString();
                //    PassengerAdditionalInfoDef.CustomerID = Convert.ToInt64(item.GetDataKeyValue("CustomerID").ToString());
                //    PassengerAdditionalInfoDef.FleetDescription = item.GetDataKeyValue("FleetDescription").ToString();
                //    PassengerAdditionalInfoDef.InformationValue = ((TextBox)item["InformationValue"].FindControl("tbAddlInfo")).Text;
                //    PassengerAdditionalInfoList.Add(PassengerAdditionalInfoDef);
                //}
                //return PassengerAdditionalInfoList;
                List<GetFleetProfileDefinition> CrewDefinitionList = new List<GetFleetProfileDefinition>();
                List<FlightPakMasterService.GetFleetProfileDefinition> CrewRetainDefinition = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                CrewRetainDefinition = (List<FlightPakMasterService.GetFleetProfileDefinition>)Session["FleetProfileDefinition"];
                List<FlightPakMasterService.GetFleetProfileDefinition> CrewRetainDefinitionList = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                for (int Index = 0; Index < CrewRetainDefinition.Count; Index++)
                {
                    foreach (GridDataItem item in grid.MasterTableView.Items)
                    {
                        if (item.GetDataKeyValue("FleetInfoCD").ToString() == CrewRetainDefinition[Index].FleetInfoCD)
                        {
                            CrewRetainDefinition[Index].FleetID = Convert.ToInt64(hdnFleetID.Value);
                            CrewRetainDefinition[Index].InformationValue = ((TextBox)item["InformationValue"].FindControl("tbAddlInfo")).Text;
                            CrewRetainDefinition[Index].IsPrintable = ((CheckBox)item["IsPrintable"].FindControl("chkPrintable")).Checked;
                            break;
                        }
                    }
                }
                return CrewRetainDefinition;
            }
        }
        private void SelectFltItm(string Itm)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Itm.Trim() != "")
                {
                    bool FoundItem = false;
                    int CurrentPageIndex = dgFleetProfile.CurrentPageIndex; // the current page index of the Grid
                    //loop through the items of all item page from the current to the last
                    for (int i = CurrentPageIndex; i < dgFleetProfile.PageCount; i++)
                    {
                        foreach (GridDataItem Item in dgFleetProfile.Items)
                        {
                            if (Item["FleetID"].Text.Trim().ToUpper() == Itm.ToString().Trim().ToUpper())
                            {
                                Item.Selected = true;
                                GridDataItem item = dgFleetProfile.SelectedItems[0] as GridDataItem;
                                Session["FPSelectedItem"] = Item["FleetID"].Text;
                                Session["aircraftCode"] = Item["AircraftCD"].Text.ToString();
                                Session["FleetID"] = Item["FleetID"].Text.ToString();
                                hdnFleetID.Value = Item["FleetID"].Text.ToString();
                                Session.Remove("FleetProfileDefinition");
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                                FoundItem = true;
                                break;
                            }
                        }
                        if (FoundItem)
                        {
                            //in order selection to be applied
                            break;
                        }
                        else
                        {
                            dgFleetProfile.CurrentPageIndex = i + 1;
                            dgFleetProfile.Rebind();
                        }
                    }
                }
            }
        }
        private void SelectFleetItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["FPSelectedItem"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedCrewRosterID"];
                        string CrewID = Session["FPSelectedItem"].ToString();
                        //   string Code = items.GetDataKeyValue("CrewCD").ToString();
                        foreach (GridDataItem item in dgFleetProfile.MasterTableView.Items)
                        {
                            if (item["FleetID"].Text.Trim() == CrewID.Trim())
                            {
                                item.Selected = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private List<FlightPakMasterService.FleetProfileDefinition> SaveFleetAdditionalInfo()
        {
            FlightPakMasterService.Fleet Fleet = new FlightPakMasterService.Fleet();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Fleet.FleetProfileDefinition = new List<FleetProfileDefinition>();
                Int64 Identity = 0;
                List<FlightPakMasterService.GetFleetProfileDefinition> CrewDefinitionInfoList = new List<FlightPakMasterService.GetFleetProfileDefinition>();
                FlightPakMasterService.FleetProfileDefinition CrewDefinition = new FlightPakMasterService.FleetProfileDefinition();
                CrewDefinitionInfoList = (List<FlightPakMasterService.GetFleetProfileDefinition>)Session["FleetProfileDefinition"];
                bool IsExist = false;
                for (int Index = 0; Index < CrewDefinitionInfoList.Count; Index++)
                {
                    IsExist = false;
                    CrewDefinition = new FleetProfileDefinition();
                    foreach (GridDataItem Item in dgPassengerAddlInfo.MasterTableView.Items)
                    {
                        if (Item.GetDataKeyValue("FleetProfileInformationID").ToString() == CrewDefinitionInfoList[Index].FleetProfileInformationID.ToString())
                        {
                            if (Convert.ToInt64(Item.GetDataKeyValue("FleetProfileInfoXRefID").ToString()) != 0)
                            {
                                CrewDefinition.FleetProfileInfoXRefID = Convert.ToInt64(Item.GetDataKeyValue("FleetProfileInfoXRefID").ToString());
                            }
                            else
                            {
                                Identity = Identity - 1;
                                CrewDefinition.FleetProfileInfoXRefID = Identity;
                            }
                            if (Convert.ToInt64(hdnFleetID.Value) > 0)
                            {
                                CrewDefinition.FleetID = Convert.ToInt64(hdnFleetID.Value);
                            }
                            else
                            {
                                CrewDefinition.FleetID = CrewDefinitionInfoList[Index].FleetID;
                            }
                            CrewDefinition.FleetProfileInformationID = CrewDefinitionInfoList[Index].FleetProfileInformationID;
                            CrewDefinition.CustomerID = Fleet.CustomerID;
                            CrewDefinition.FleetDescription = CrewDefinitionInfoList[Index].FleetDescription;
                            CrewDefinition.InformationValue = ((TextBox)Item["InformationValue"].FindControl("tbAddlInfo")).Text;
                            CrewDefinition.IsPrintable = ((CheckBox)Item["IsPrintable"].FindControl("chkPrintable")).Checked;
                            CrewDefinition.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted.Value;
                            CrewDefinition.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                            CrewDefinition.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                            CrewDefinition.IsDeleted = false;
                            IsExist = true;
                            break;
                        }
                    }
                    //IsExist
                    if (IsExist == false)
                    {
                        if (CrewDefinitionInfoList[Index].FleetProfileInfoXRefID != 0)
                        {
                            CrewDefinition.FleetProfileInfoXRefID = CrewDefinitionInfoList[Index].FleetProfileInfoXRefID;
                        }
                        else
                        {
                            Identity = Identity - 1;
                            CrewDefinition.FleetProfileInfoXRefID = Identity;
                        }
                        CrewDefinition.FleetID = CrewDefinitionInfoList[Index].FleetID;
                        CrewDefinition.FleetProfileInformationID = CrewDefinitionInfoList[Index].FleetProfileInformationID;
                        CrewDefinition.FleetProfileInfoXRefID = CrewDefinitionInfoList[Index].FleetProfileInfoXRefID;
                        CrewDefinition.CustomerID = CrewDefinitionInfoList[Index].CustomerID;
                        CrewDefinition.FleetDescription = CrewDefinitionInfoList[Index].FleetDescription;
                        CrewDefinition.InformationValue = CrewDefinitionInfoList[Index].InformationValue;
                        CrewDefinition.IsPrintable = CrewDefinitionInfoList[Index].IsPrintable;
                        CrewDefinition.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted.Value;
                        CrewDefinition.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                        CrewDefinition.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                    }
                    if (CrewDefinition.FleetProfileInfoXRefID == 0)
                    {
                        Identity = Identity - 1;
                        CrewDefinition.FleetProfileInfoXRefID = Identity;
                    }
                    Fleet.FleetProfileDefinition.Add(CrewDefinition);
                }
                return Fleet.FleetProfileDefinition;
            }
        }
        #endregion

        /// <summary>
        /// Method for validating all popup values before insert or update
        /// </summary>
        /// <returns></returns>
        private bool CustomValidations()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!CheckHomeBaseExist())
                {
                    ReturnValue = false;
                }
                if (!CheckTypeCodeExist())
                {
                    ReturnValue = false;
                }
                if (!CheckMaintCrewExists())
                {
                    ReturnValue = false;
                }
                if (!CheckClientExist())
                {
                    ReturnValue = false;
                }
                if (!CheckVendorExist())
                {
                    ReturnValue = false;
                }
                if (!CheckEmergencyExists())
                {
                    ReturnValue = false;
                }
                return ReturnValue;
            }
        }
        private void GetControlState()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, bool> dicControlState = new Dictionary<string, bool>();
                dicControlState.Add("chkActiveOnly", chkActiveOnly.Checked);
                dicControlState.Add("chkHomeBaseOnly", chkHomeBaseOnly.Checked);
                dicControlState.Add("chkRotary", chkRotary.Checked);
                dicControlState.Add("chkFixedWing", chkFixedWing.Checked);
                Session["FleetProfileControlState"] = dicControlState;
            }
        }
        private void SetControlState()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["FleetProfileControlState"] != null)
                {
                    Dictionary<string, bool> dicControlState = new Dictionary<string, bool>();
                    dicControlState = (Dictionary<string, bool>)Session["FleetProfileControlState"];
                    chkActiveOnly.Checked = Convert.ToBoolean(dicControlState["chkActiveOnly"]);
                    chkHomeBaseOnly.Checked = Convert.ToBoolean(dicControlState["chkHomeBaseOnly"]);
                    chkRotary.Checked = Convert.ToBoolean(dicControlState["chkRotary"]);
                    chkFixedWing.Checked = Convert.ToBoolean(dicControlState["chkFixedWing"]);
                    Session.Remove("FleetProfileControlState");
                }
            }
        }

        #region "Record count validation"
        private bool CheckCanAdd()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var AircraftActualCount = objService.GetAircraftActualCount();
                    var AircraftUsedCount = objService.GetAircraftUsedCount();
                    return AircraftActualCount == null || AircraftUsedCount < AircraftActualCount;
                }
            }
        }

        private bool CheckCanEdit()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var AircraftActualCount = objService.GetAircraftActualCount();
                    var AircraftUsedCount = objService.GetAircraftUsedCount();

                    var isInactiveAircraft = false;
                    var CheckFleet = objService.GetFleetIDInfo(Convert.ToInt64(hdnFleetID.Value)).EntityList;
                    foreach (FlightPakMasterService.GetFleetIDInfo Fleet in CheckFleet)
                    {
                        if (Fleet.IsInActive != null)
                        {
                            if (Fleet.IsInActive == true || Fleet.IsInActive.ToString().Trim() == "1")
                            {
                                isInactiveAircraft = true;
                            }
                        }
                    }
                    
                    if (AircraftActualCount == null || AircraftActualCount == 0)
                         return true;

                    if (AircraftUsedCount >= AircraftActualCount && chkinactiveairfact.Checked == false && isInactiveAircraft == true)
                        return false;

                    return true;
                }
            }
        }

        #endregion

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFleetProfile.SelectedIndexes.Clear();
                    chkActiveOnly.Checked = false;
                    chkHomeBaseOnly.Checked = false;
                    chkRotary.Checked = false;
                    chkFixedWing.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var FleetValue = GetFleetValue(FPKMstService);
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, FleetValue);
            hdnFleetID.Value = PrimaryKeyValue.ToString();

            foreach (var Item in FleetValue.EntityList)
            {
                if (PrimaryKeyValue == Item.FleetID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFleetProfile.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFleetProfile.CurrentPageIndex = PageNumber;
            dgFleetProfile.SelectedIndexes.Add(ItemIndex);
        }
        
        private ReturnValueOfGetFleetSearch GetFleetValue(FlightPakMasterService.MasterCatalogServiceClient FPKMstService)
        {
            ReturnValueOfGetFleetSearch FleetValue = new ReturnValueOfGetFleetSearch();

            string IsFixedRotary = "B";
            bool IsInActive = false;
            Int64 HomeBaseID = 0;
            if (chkRotary.Checked && !chkFixedWing.Checked)
            {
                IsFixedRotary = "R";
            }
            else if (!chkRotary.Checked && chkFixedWing.Checked)
            {
                IsFixedRotary = "F";
            }
            if (chkActiveOnly.Checked)
            {
                IsInActive = true;
            }
            if (chkHomeBaseOnly.Checked)
            {
                if (UserPrincipal != null && UserPrincipal.Identity._homeBaseId != null)
                {
                    HomeBaseID = Convert.ToInt64(UserPrincipal.Identity._homeBaseId);
                }
            }
            if (IsPopUp)
                //Get All the records for popup
                FleetValue = FPKMstService.GetFleetSearch(false, "B", HomeBaseID);
            else
                FleetValue = FPKMstService.GetFleetSearch(IsInActive, IsFixedRotary, HomeBaseID);

            return FleetValue;
        }

        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetFleetSearch FleetValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch || (FleetValue.EntityList == null || FleetValue.EntityList.Count == 0))
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["FPSelectedItem"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = FleetValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FleetID;
                Session["FPSelectedItem"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            //Ramesh: Code to allow any file type for upload control.
            
            int iIndex = filename.Trim().IndexOf('.');
            //string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);  
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            
            if (regMatch.Success)
            {           
                if (Request.UserAgent.Contains("Chrome"))
                {
                    hdnBrowserName.Value = "Chrome";
                }

                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                else
                {
                    Session["Base64"] = filedata;
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid();
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgFile.Visible = false;
                lnkFileName.Visible = true;
                Session["Base64"] = filedata;
                lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();
            }
        }

        protected void dgFleetProfile_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFleetProfile, Page.Session);
            if (Session["CommandName"] as string == "Filter")
            {
                if (dgFleetProfile.Items.Count >= 1)
                {
                    (dgFleetProfile.Items[0] as GridDataItem).FireCommandEvent("Select", string.Empty);
                }
            }
        }

        protected void tbClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckClientCodeExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }
        }

        private bool CheckClientCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (tbClientCode.Text.Trim() == "undefined")
                    tbClientCode.Text = "";
                if (tbClientCode.Text.Trim() != "")
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Clients = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ClientVal = Clients.GetClientCodeList().EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbClientCode.Text.ToString().ToUpper().Trim()));
                        if (ClientVal.Count() > 0 && ClientVal != null)
                        {
                            foreach (FlightPakMasterService.Client ClientCode in ClientVal)
                            {
                                tbClientCode.Text = ClientCode.ClientCD.ToString().Trim();
                                hdnClientCode.Value = ClientCode.ClientID.ToString();
                                RadAjaxManager1.FocusControl(tbClientCode.ClientID);

                                dgFleetProfile.MasterTableView.FilterExpression = "([ClientCD] LIKE \'" + tbClientCode.Text + "%\') ";
                                GridColumn column = dgFleetProfile.MasterTableView.GetColumnSafe("ClientCD");
                                column.CurrentFilterFunction = GridKnownFunction.StartsWith;
                                column.CurrentFilterValue = tbClientCode.Text;
                                dgFleetProfile.MasterTableView.Rebind();
                            }
                        }
                        else
                        {
                            cvClientCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbClientCode.ClientID);
                            return false;
                        }
                    }
                }
                return true;
            }
        }
    }
}
