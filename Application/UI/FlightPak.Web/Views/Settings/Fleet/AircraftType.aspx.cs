﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using System.Text.RegularExpressions;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class AircraftType : BaseSecuredPage		// System.Web.UI.Page
    {
        bool IsValid = true;
        private bool IsEmptyCheck = true;
        private List<string> aircraftCodes = new List<string>();
        private bool AircraftTypePageNavigated = false;
        private ExceptionManager exManager;
        private string ErrorMsg = string.Empty;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAircraftTypeCatalog, dgAircraftTypeCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgAircraftTypeCatalog.ClientID));
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewAircraftTypeReport);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewAircraftType);
                            Session["StandardFlightpakConversion"] = LoadStandardFPConversion();
                            TimeDisplay();
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgAircraftTypeCatalog.Rebind();
                                ReadOnlyForm();
                                EnableForm(false);
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);

                                if (Request.QueryString["AircraftID"] != null && Request.QueryString["AircraftID"].ToString() != "")
                                {
                                    Session["AircraftID"] = Request.QueryString["AircraftID"].ToString();
                                    SelectItem();
                                    ReadOnlyForm();
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            dgAircraftTypeCatalog.AllowPaging = false;
                            chkSearchActiveOnly.Visible = false;
                        }                        

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }

        #region CompanyProfile DateTimeCalculation

        /// <summary>
        /// Method to Set Default Standard and Flightpak Conversion into List
        /// </summary>
        public List<StandardFlightpakConversion> LoadStandardFPConversion()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<StandardFlightpakConversion> conversionList = new List<StandardFlightpakConversion>();
                // Set Standard Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 5, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 6, EndMinutes = 11, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 12, EndMinutes = 17, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 18, EndMinutes = 23, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 24, EndMinutes = 29, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 30, EndMinutes = 35, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 36, EndMinutes = 41, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 42, EndMinutes = 47, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 48, EndMinutes = 53, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 54, EndMinutes = 59, ConversionType = 1 });

                // Set Flightpak Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 2, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 3, EndMinutes = 8, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 9, EndMinutes = 14, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 15, EndMinutes = 21, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 22, EndMinutes = 27, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 28, EndMinutes = 32, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 33, EndMinutes = 39, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 40, EndMinutes = 44, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 45, EndMinutes = 50, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 51, EndMinutes = 57, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 1.0M, StartMinutes = 58, EndMinutes = 59, ConversionType = 2 });

                return conversionList;
            }
        }

        /// <summary>
        /// Method to Set Time Format, based on Company Profile
        /// </summary>
        public void TimeDisplay()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UserPrincipal.Identity != null)
                {

                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                    if (UserPrincipal.Identity != null)
                    {
                        // Display Time Fields based on Tenths / Minutes - Company Profile
                        if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == null) || (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))
                        {
                            string OnPress = "javascript:return fnAllowNumericAndChar(this, event,'.')";
                            SetMaskAdjustment("000.0", OnPress);
                        }
                        else
                        {
                            string OnPress = "javascript:return fnAllowNumericAndChar(this, event,':')";
                            SetMaskAdjustment("00:00", OnPress);
                        }
                    }
                    else
                    {
                        string OnPress = "javascript:return fnAllowNumericAndChar(this, event,':')";
                        SetMaskAdjustment("00:00", OnPress);
                    }
                }
            }
        }

        /// <summary>
        /// Method to Set Mask Fields
        /// </summary>
        /// <param name="display">Pass Display Type</param>
        /// <param name="mask">Pass Mask Type</param>
        private void SetMaskAdjustment(string display, string OnPress)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(display,OnPress))
            {
                tbPS1HRRNG.Text = display;
                tbPS1HRRNG.Attributes.Add("onkeypress", OnPress);
                tbPS1TOBIAS.Text = display;
                tbPS1TOBIAS.Attributes.Add("onkeypress", OnPress);
                tbPS1LNDBIAS.Text = display;
                tbPS1LNDBIAS.Attributes.Add("onkeypress", OnPress);
                tbPS2HRRNG.Text = display;
                tbPS2HRRNG.Attributes.Add("onkeypress", OnPress);
                tbPS2TOBIAS.Text = display;
                tbPS2TOBIAS.Attributes.Add("onkeypress", OnPress);
                tbPS2LNDBIAS.Text = display;
                tbPS2LNDBIAS.Attributes.Add("onkeypress", OnPress);
                tbPS3HRRNG.Text = display;
                tbPS3HRRNG.Attributes.Add("onkeypress", OnPress);
                tbPS3TOBIAS.Text = display;
                tbPS3TOBIAS.Attributes.Add("onkeypress", OnPress);
                tbPS3LNDBIAS.Text = display;
                tbPS3LNDBIAS.Attributes.Add("onkeypress", OnPress);              


            }
        }   
        /// <summary>
        /// Method to Get Tenths Conversion Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <param name="isTenths">Pass Boolean Value</param>
        /// <param name="conversionType">Pass Conversion Type</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertMinToTenths(String time, Boolean isTenths, String conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time, isTenths, conversionType))
            {
                string result = "00.0";
                decimal minute; decimal hour;

                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    if (!string.IsNullOrEmpty(timeArray[0]))
                        hour = Convert.ToDecimal(timeArray[0]);
                    else
                        hour = 00;
                    if (!string.IsNullOrEmpty(timeArray[1]))
                        minute = Convert.ToDecimal(timeArray[1]);
                    else
                        minute = 00;

                    if (isTenths && (conversionType == "1" || conversionType == "2")) // Standard and Flightpak Conversion
                    {
                        if (Session["StandardFlightpakConversion"] != null)
                        {
                            List<StandardFlightpakConversion> StandardFlightpakConversionList = (List<StandardFlightpakConversion>)Session["StandardFlightpakConversion"];
                            var standardList = StandardFlightpakConversionList.ToList().Where(x => minute >= x.StartMinutes && minute <= x.EndMinutes && x.ConversionType.ToString() == conversionType).ToList();

                            if (standardList.Count > 0)
                            {
                                result = Convert.ToString(hour + standardList[0].Tenths);
                            }
                        }
                    }
                    else
                    {
                        decimal decimalOfMin = 0;
                        if (minute > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = minute / 60;

                        result = Convert.ToString(hour + decimalOfMin);
                    }
                }
                return result;
            }

        }

        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "00:00";
                int val;
                if (time.IndexOf(".") != -1)
                {
                    string[] timeArray = time.Split('.');
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    string minute = timeArray[1].ToString();//Convert.ToDecimal(timeArray[1]);
                    decimal decimal_min = Convert.ToDecimal(String.Concat("0.", minute));
                    decimal decimalOfMin = 0;
                    if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                        decimalOfMin = (decimal_min * 60 / 100);
                    if (hour.ToString().Length > 2)
                    {
                        hour = Convert.ToDecimal(hour.ToString().Substring(0, 2));
                    }
                    decimal finalval = Math.Round(hour + decimalOfMin, 2);
                    result = Convert.ToString(finalval).Replace(".", ":");
                    string[] strArr = new string[2];
                    string ReturnValue = string.Empty;
                    strArr = result.Split(':');
                    var charExists = (result.IndexOf(':') >= 0) ? true : false;
                    if (charExists == false)
                    {
                        ReturnValue = "00";
                        result = strArr[0] + ":" + ReturnValue;
                    }
                }
                else if (int.TryParse(time, out val))
                {
                    result = time;
                }

                return result;
            }
        }


        #endregion

        protected void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (BindDataSwitch)
                        {
                            dgAircraftTypeCatalog.Rebind();
                        }
                        if (dgAircraftTypeCatalog.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["AircraftID"] = null;
                            //}
                            if (Session["AircraftID"] == null)
                            {
                                dgAircraftTypeCatalog.SelectedIndexes.Add(0);
                                if (!IsPopUp)
                                {
                                    Session["AircraftID"] = dgAircraftTypeCatalog.Items[0].GetDataKeyValue("AircraftID").ToString();
                                }
                            }

                            if (dgAircraftTypeCatalog.SelectedIndexes.Count == 0)
                                dgAircraftTypeCatalog.SelectedIndexes.Add(0);

                                ReadOnlyForm();
                                GridEnable(true, true, true);
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["AircraftID"] != null)
                        {
                            Boolean flag = false;
                            string ID = Session["AircraftID"].ToString();
                            foreach (GridDataItem Item in dgAircraftTypeCatalog.MasterTableView.Items)
                            {
                                if (Item["AircraftID"].Text.Trim() == ID.Trim())
                                {
                                    //Item.Selected = true;
                                    flag = true;
                                    break;
                                }
                            }
                            if (flag == false)
                            {
                                DefaultSelection(false);
                            }
                        }
                        else
                        {
                            DefaultSelection(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton insertCtl, delCtl, editCtl;
                        insertCtl = (LinkButton)dgAircraftTypeCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                        delCtl = (LinkButton)dgAircraftTypeCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                        editCtl = (LinkButton)dgAircraftTypeCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                        if (IsAuthorized(Permission.Database.AddAircraftType))
                        {
                            insertCtl.Visible = true;
                            if (add)
                            {
                                insertCtl.Enabled = true;
                            }
                            else
                            {
                                insertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            insertCtl.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.DeleteAircraftType))
                        {
                            delCtl.Visible = true;
                            if (delete)
                            {
                                delCtl.Enabled = true;
                                delCtl.OnClientClick = "javascript:return ProcessDelete();";
                            }
                            else
                            {
                                delCtl.Enabled = false;
                                delCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            delCtl.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.EditAircraftType))
                        {
                            editCtl.Visible = true;
                            if (edit)
                            {
                                editCtl.Enabled = true;
                                editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                editCtl.Enabled = false;
                                editCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            editCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                       
                        hdnSave.Value = "Save";
                        EnableForm(true);
                        rbfixed.Checked = true;
                        rbRotary.Checked = false;

                        rbHours.Checked = true;
                        rbNautical.Checked = false;
                        rbStatute.Checked = false;
                        rbKilometers.Checked = false;

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        ///  /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //tbCode.ReadOnly = true;
                        hdnSave.Value = "Update";
                        hdnRedirect.Value = "";
                        if (IsPopUp)
                        {
                            dgAircraftTypeCatalog.AllowPaging = false;
                            dgAircraftTypeCatalog.Rebind();
                        }
                        LoadControlData();
                        EnableForm(true);
                        tbCode.Enabled = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbCode.Enabled = enable;
                        tbDescription.Enabled = enable;
                        tbChargeRate.Enabled = enable;
                        tbMarginPercentage.Enabled = enable;
                        tbAssociateTypeCode.Enabled = enable;
                        tbDefaultPowerSetting.Enabled = enable;
                        tbWindAltitude.Enabled = enable;
                        tbPS1TAS.Enabled = enable;
                        tbPS1HRRNG.Enabled = enable;
                        tbPS1TOBIAS.Enabled = enable;
                        tbPS1LNDBIAS.Enabled = enable;
                        rbfixed.Enabled = enable;
                        rbHours.Enabled = enable;
                        rbKilometers.Enabled = enable;
                        rbNautical.Enabled = enable;
                        rbRotary.Enabled = enable;
                        rbStatute.Enabled = enable;
                        tbPS2TAS.Enabled = enable;
                        tbPS2HRRNG.Enabled = enable;
                        tbPS2TOBIAS.Enabled = enable;
                        tbPS2LNDBIAS.Enabled = enable;
                        btnHomeBase.Enabled = enable;
                        lbDefaultPowerSetText.Enabled = enable;
                        tbPS3TAS.Enabled = enable;
                        tbPS3HRRNG.Enabled = enable;
                        tbPS3TOBIAS.Enabled = enable;
                        tbPS3LNDBIAS.Enabled = enable;
                        btnSaveChanges.Visible = enable;
                        btnCancel.Visible = enable;
                        chkInactive.Enabled = enable;                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// To Clear the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAircraftTypeCatalog.SelectedIndexes.Clear();
                        tbCode.Text = string.Empty;
                        hdnAircraftId.Value = string.Empty;
                        tbDescription.Text = string.Empty;
                        tbWindAltitude.Text = "0";
                        tbChargeRate.Text = "0.00";
                        tbMarginPercentage.Text = "0.00";
                        tbAssociateTypeCode.Text = string.Empty;
                        hdnAssociateTypeCode.Value = string.Empty;
                        tbDefaultPowerSetting.Text = string.Empty;
                        lbDefaultPowerSetText.Visible = false;
                        tbPS1TAS.Text = "0";                      
                        tbPS2TAS.Text = "0";                     
                        tbPS3TAS.Text = "0";                     
                        lbMultiple.Style.Add("display", "none");                     
                        rbHours.Checked = true;
                        rbNautical.Checked = false;
                        rbKilometers.Checked = false;
                        rbStatute.Checked = false;
                        chkInactive.Checked = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LoadControlData();
                        EnableForm(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["AircraftID"] != null)
                    {
                        foreach (GridDataItem Item in dgAircraftTypeCatalog.MasterTableView.Items)
                        {
                            if (Item["AircraftID"].Text.Trim() == Session["AircraftID"].ToString().Trim())
                            {
                                Item.Selected = true;
                                decimal checkforDecimal = 0;
                                btnHomeBase.Enabled = false;
                                if (Item.GetDataKeyValue("AircraftID") != null)
                                {
                                    hdnAircraftId.Value = Item.GetDataKeyValue("AircraftID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("AircraftCD") != null)
                                {

                                    tbCode.Text = Item.GetDataKeyValue("AircraftCD").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("AircraftDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("AircraftDescription").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("AircraftTypeCD") != null)
                                {
                                    if (Item["AircraftTypeCD"].Text.IndexOf(',') > 0)
                                    {
                                        string[] MainAircraftTypeCode = Item.GetDataKeyValue("AircraftTypeCD").ToString().Split(',');
                                        tbAssociateTypeCode.Text = MainAircraftTypeCode[0].Trim();
                                        hdnAssociateTypeCode.Value = Item.GetDataKeyValue("AircraftTypeCD").ToString().Trim();
                                        lbMultiple.Style.Add("display", "inline");
                                    }
                                    else
                                    {
                                        tbAssociateTypeCode.Text = Item.GetDataKeyValue("AircraftTypeCD").ToString().Trim();
                                        hdnAssociateTypeCode.Value = Item.GetDataKeyValue("AircraftTypeCD").ToString().Trim();
                                        lbMultiple.Style.Add("display", "none");
                                    }
                                }
                                else
                                {
                                    lbMultiple.Style.Add("display", "none");
                                    tbAssociateTypeCode.Text = string.Empty;
                                    hdnAssociateTypeCode.Value = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ChargeRate") != null)
                                {
                                    tbChargeRate.Text = Item.GetDataKeyValue("ChargeRate").ToString();
                                }
                                else
                                    tbChargeRate.Text = "00000.00";
                                if (Item.GetDataKeyValue("MarginalPercentage") != null)
                                {
                                    tbMarginPercentage.Text = Item.GetDataKeyValue("MarginalPercentage").ToString();
                                }
                                else
                                    tbMarginPercentage.Text = "0.00";
                                
                                if (Item.GetDataKeyValue("IsFixedRotary") != null)
                                {
                                    if (Item.GetDataKeyValue("IsFixedRotary").ToString() == "F")
                                    {
                                        rbfixed.Checked = true;
                                        rbRotary.Checked = false;
                                    }
                                    else if (Item.GetDataKeyValue("IsFixedRotary").ToString() == "R")
                                    {
                                        rbfixed.Checked = false;
                                        rbRotary.Checked = true;
                                    }
                                    else
                                    {
                                        rbfixed.Checked = true;
                                        rbRotary.Checked = false;
                                    }
                                }
                                if (Item.GetDataKeyValue("ChargeUnit") != null)
                                {
                                    if (Item.GetDataKeyValue("ChargeUnit").ToString() == "H")
                                    {
                                        rbHours.Checked = true;
                                        rbNautical.Checked = false;
                                        rbKilometers.Checked = false;
                                        rbStatute.Checked = false;
                                    }
                                    else if (Item.GetDataKeyValue("ChargeUnit").ToString() == "N")
                                    {
                                        rbNautical.Checked = true;
                                        rbHours.Checked = false;
                                        rbKilometers.Checked = false;
                                        rbStatute.Checked = false;
                                    }
                                    else if (Item.GetDataKeyValue("ChargeUnit").ToString() == "K")
                                    {
                                        rbKilometers.Checked = true;
                                        rbNautical.Checked = false;
                                        rbHours.Checked = false;
                                        rbStatute.Checked = false;
                                    }
                                    else
                                    {
                                        rbStatute.Checked = true;
                                        rbKilometers.Checked = false;
                                        rbNautical.Checked = false;
                                        rbHours.Checked = false;
                                    }
                                }
                                if (Item.GetDataKeyValue("PowerSetting") != null)
                                {
                                    tbDefaultPowerSetting.Text = Item.GetDataKeyValue("PowerSetting").ToString().Trim();
                                }
                                else
                                    tbDefaultPowerSetting.Text = string.Empty;
                                if ((Item).GetDataKeyValue("PowerDescription") != null)
                                {
                                    lbDefaultPowerSetText.Text = (Item).GetDataKeyValue("PowerDescription").ToString().Trim();
                                    lbDefaultPowerSetText.Visible = true;
                                }
                                else
                                    lbDefaultPowerSetText.Text = string.Empty;
                                if (Item.GetDataKeyValue("WindAltitude") != null)
                                {
                                    tbWindAltitude.Text = Item.GetDataKeyValue("WindAltitude").ToString();
                                }
                                else
                                    tbWindAltitude.Text = "0";
                                if (Item.GetDataKeyValue("PowerSettings1TrueAirSpeed") != null)
                                {
                                    tbPS1TAS.Text = Item.GetDataKeyValue("PowerSettings1TrueAirSpeed").ToString();
                                }
                                else
                                    tbPS1TAS.Text = "0";
                                if (Item.GetDataKeyValue("PowerSettings2TrueAirSpeed") != null)
                                {
                                    tbPS2TAS.Text = Item.GetDataKeyValue("PowerSettings2TrueAirSpeed").ToString();
                                }
                                else
                                    tbPS2TAS.Text = "0";
                                if (Item.GetDataKeyValue("PowerSettings3TrueAirSpeed") != null)
                                {
                                    tbPS3TAS.Text = Item.GetDataKeyValue("PowerSettings3TrueAirSpeed").ToString();
                                }
                                else
                                    tbPS3TAS.Text = "0";
                                TimeDisplay();
                                if ((Item.GetDataKeyValue("PowerSettings1HourRange") != null) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)) && ((Item.GetDataKeyValue("PowerSettings1HourRange") != "")))
                                {
                                    if ((Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1HourRange")) == 0))
                                        tbPS1HRRNG.Text = "00:00";
                                    else
                                        tbPS1HRRNG.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1HourRange").ToString()), 3).ToString());
                                }
                                else
                                {
                                    if ((Item.GetDataKeyValue("PowerSettings1HourRange") != null) && ((Item.GetDataKeyValue("PowerSettings1HourRange") != string.Empty)) && (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))
                                        tbPS1HRRNG.Text = AddPadding(Item.GetDataKeyValue("PowerSettings1HourRange").ToString()); // AddPadding(Item.GetDataKeyValue("PowerSettings1HourRange").ToString());
                                }

                                if ((Item.GetDataKeyValue("PowerSettings2HourRange") != null) && ((Item.GetDataKeyValue("PowerSettings2HourRange") != string.Empty)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                                {
                                    if ((Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2HourRange")) == 0))
                                        tbPS2HRRNG.Text = "00:00";
                                    else
                                        tbPS2HRRNG.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2HourRange").ToString()), 3).ToString());
                                }
                                else
                                {
                                    if ((Item.GetDataKeyValue("PowerSettings2HourRange") != null) && ((Item.GetDataKeyValue("PowerSettings2HourRange") != string.Empty)) && (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))
                                        tbPS2HRRNG.Text = AddPadding(Item.GetDataKeyValue("PowerSettings2HourRange").ToString().Trim());
                                }
                                if ((Item.GetDataKeyValue("PowerSettings3HourRange") != null) && ((Item.GetDataKeyValue("PowerSettings3HourRange") != string.Empty)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                                {
                                    if ((Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3HourRange")) == 0))
                                        tbPS3HRRNG.Text = "00:00";
                                    else
                                        tbPS3HRRNG.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3HourRange").ToString()), 3).ToString());
                                }
                                else
                                {
                                    if ((Item.GetDataKeyValue("PowerSettings3HourRange") != null) && ((Item.GetDataKeyValue("PowerSettings3HourRange") != string.Empty)) && (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))
                                        tbPS3HRRNG.Text = AddPadding(Item.GetDataKeyValue("PowerSettings3HourRange").ToString());
                                }
                                if ((Item.GetDataKeyValue("PowerSettings1TakeOffBias") != null) && ((Item.GetDataKeyValue("PowerSettings1TakeOffBias") != string.Empty)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                                {
                                    if ((Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1TakeOffBias")) == 0))
                                        tbPS1TOBIAS.Text = "00:00";
                                    else
                                        tbPS1TOBIAS.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1TakeOffBias").ToString()), 3).ToString());
                                }
                                else
                                {
                                    if ((Item.GetDataKeyValue("PowerSettings1TakeOffBias") != null) && ((Item.GetDataKeyValue("PowerSettings1TakeOffBias") != string.Empty)) && (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))
                                        tbPS1TOBIAS.Text = AddPadding(Item.GetDataKeyValue("PowerSettings1TakeOffBias").ToString());
                                }
                                if ((Item.GetDataKeyValue("PowerSettings2TakeOffBias") != null) && ((Item.GetDataKeyValue("PowerSettings2TakeOffBias") != string.Empty)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                                {
                                    if ((Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2TakeOffBias")) == 0))
                                        tbPS2TOBIAS.Text = "00:00";
                                    else
                                        tbPS2TOBIAS.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2TakeOffBias").ToString()), 3).ToString());
                                }
                                else
                                {
                                    if ((Item.GetDataKeyValue("PowerSettings2TakeOffBias") != null) && ((Item.GetDataKeyValue("PowerSettings2TakeOffBias") != string.Empty)) && (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))
                                        tbPS2TOBIAS.Text = AddPadding(Item.GetDataKeyValue("PowerSettings2TakeOffBias").ToString());
                                }
                                if ((Item.GetDataKeyValue("PowerSettings3TakeOffBias") != null) && ((Item.GetDataKeyValue("PowerSettings3TakeOffBias") != string.Empty)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                                {
                                    if ((Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3TakeOffBias")) == 0))
                                        tbPS3TOBIAS.Text = "00:00";
                                    else
                                        tbPS3TOBIAS.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3TakeOffBias").ToString()), 3).ToString());
                                }
                                else
                                {
                                    if ((Item.GetDataKeyValue("PowerSettings3TakeOffBias") != null) && ((Item.GetDataKeyValue("PowerSettings3TakeOffBias") != string.Empty)) && (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))
                                        tbPS3TOBIAS.Text = AddPadding(Item.GetDataKeyValue("PowerSettings3TakeOffBias").ToString());
                                }
                                if ((Item.GetDataKeyValue("PowerSettings1LandingBias") != null) && ((Item.GetDataKeyValue("PowerSettings1LandingBias") != string.Empty)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                                {
                                    if ((Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1LandingBias")) == 0))
                                        tbPS1LNDBIAS.Text = "00:00";
                                    else
                                        tbPS1LNDBIAS.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1LandingBias").ToString()), 3).ToString());
                                }
                                else
                                {
                                    if ((Item.GetDataKeyValue("PowerSettings1LandingBias") != null) && ((Item.GetDataKeyValue("PowerSettings1LandingBias") != string.Empty)) && (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))
                                        tbPS1LNDBIAS.Text = AddPadding(Item.GetDataKeyValue("PowerSettings1LandingBias").ToString());
                                }
                                if ((Item.GetDataKeyValue("PowerSettings2LandingBias") != null) && ((Item.GetDataKeyValue("PowerSettings2LandingBias") != string.Empty)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                                {
                                    if ((Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2LandingBias")) == 0))
                                        tbPS2LNDBIAS.Text = "00:00";
                                    else
                                        tbPS2LNDBIAS.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2LandingBias").ToString()), 3).ToString());
                                }
                                else
                                {
                                    if ((Item.GetDataKeyValue("PowerSettings2LandingBias") != null) && ((Item.GetDataKeyValue("PowerSettings2LandingBias") != string.Empty)) && (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))

                                        tbPS2LNDBIAS.Text = AddPadding(Item.GetDataKeyValue("PowerSettings2LandingBias").ToString());
                                }
                                if ((Item.GetDataKeyValue("PowerSettings3LandingBias") != null) && ((Item.GetDataKeyValue("PowerSettings3LandingBias") != string.Empty)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                                {
                                    if ((Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3LandingBias")) == 0))
                                        tbPS3LNDBIAS.Text = "00:00";
                                    else
                                        tbPS3LNDBIAS.Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3LandingBias").ToString()), 3).ToString());
                                }
                                else
                                {
                                    if ((Item.GetDataKeyValue("PowerSettings3LandingBias") != null) && ((Item.GetDataKeyValue("PowerSettings3LandingBias") != string.Empty)) && (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1))
                                        tbPS3LNDBIAS.Text = AddPadding(Item.GetDataKeyValue("PowerSettings3LandingBias").ToString());
                                }
                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgAircraftTypeCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                                }
                                if (tbDefaultPowerSetting.Text == "1")
                                {
                                    lbDefaultPowerSetText.Text = " Constant Mach";
                                    lbDefaultPowerSetText.Visible = true;
                                }
                                else if (tbDefaultPowerSetting.Text == "2")
                                {
                                    lbDefaultPowerSetText.Text = " Long Range Cruise";
                                    lbDefaultPowerSetText.Visible = true;
                                }
                                else if (tbDefaultPowerSetting.Text == "3")
                                {
                                    lbDefaultPowerSetText.Text = " High Speed Cruise";
                                    lbDefaultPowerSetText.Visible = true;
                                }
                                else
                                {
                                    lbDefaultPowerSetText.Visible = false;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Aircraft Type Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["AircraftCD"].Text;
                                lbColumnValue2.Text = Item["AircraftDescription"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private Aircraft GetItems(FlightPakMasterService.Aircraft objAircraft)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objAircraft))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // btnHomeBase.Disabled = false;
                    GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                    decimal checkforDecimal;
                    checkforDecimal = 0;
                    if (hdnSave.Value == "Update")
                    {
                        objAircraft.AircraftID = Convert.ToInt64(hdnAircraftId.Value); //Convert.ToInt64(Item.GetDataKeyValue("AircraftID"));
                    }
                    objAircraft.AircraftCD = tbCode.Text.ToString().Trim();
                    objAircraft.AircraftDescription = tbDescription.Text.Trim();
                    if (tbAssociateTypeCode.Text.Trim() != string.Empty)
                    {
                        objAircraft.AircraftTypeCD = tbAssociateTypeCode.Text.Trim();
                    }
                    if (hdnAssociateTypeCode.Value.IndexOf(",") > 0)
                    {
                        string AircraftTypeCDcsv = string.Empty;
                        foreach (string str in hdnAssociateTypeCode.Value.Split(','))
                        {
                            if (tbAssociateTypeCode.Text.IndexOf(str.Trim()) < 0)
                                AircraftTypeCDcsv += str.Trim() + ",";
                        }
                        if (AircraftTypeCDcsv != string.Empty)
                            objAircraft.AircraftTypeCD += "," + AircraftTypeCDcsv.Substring(0, AircraftTypeCDcsv.Length - 1);
                    }
                    if (hdnAssociateTypeCode.Value.IndexOf(',') > 0)
                    {
                        lbMultiple.Style.Add("display", "inline");
                    }
                    if (tbChargeRate.Text != null && tbChargeRate.Text != string.Empty)
                    {
                        objAircraft.ChargeRate = decimal.TryParse(tbChargeRate.Text, out checkforDecimal) ? checkforDecimal : 0;
                    }
                    if (!string.IsNullOrEmpty(tbMarginPercentage.Text))
                    {
                        objAircraft.MarginalPercentage = Convert.ToDecimal(tbMarginPercentage.Text);
                    }


                    objAircraft.PowerSetting = tbDefaultPowerSetting.Text.ToString().Trim();
                    if (tbWindAltitude.Text != null && tbWindAltitude.Text != string.Empty)
                    {
                        objAircraft.WindAltitude = decimal.TryParse(tbWindAltitude.Text, out checkforDecimal) ? checkforDecimal : 0;
                    }
                    if (!string.IsNullOrEmpty(tbDefaultPowerSetting.Text))
                    {
                        objAircraft.PowerDescription = lbDefaultPowerSetText.Text;
                    }
                    objAircraft.PowerSettings1Description = lbConstantMach.Text;
                    objAircraft.PowerSettings1TrueAirSpeed = Convert.ToDecimal(tbPS1TAS.Text);
                    objAircraft.PowerSettings2Description = lbLongRangeCruise.Text;
                    objAircraft.PowerSettings2TrueAirSpeed = Convert.ToDecimal(tbPS2TAS.Text);
                    objAircraft.PowerSettings3TrueAirSpeed = Convert.ToDecimal(tbPS3TAS.Text);
                    objAircraft.PowerSettings3Description = lbHighSpeedCruise.Text;
                    if ((!string.IsNullOrEmpty(tbPS1HRRNG.Text)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                    {
                        var charExists = (tbPS1HRRNG.Text.IndexOf(':') >= 0) ? true : false;
                        if (!charExists)
                            tbPS1HRRNG.Text = tbPS1HRRNG.Text + ":00";
                        objAircraft.PowerSettings1HourRange = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPS1HRRNG.Text, true, "GENERAL")), 3);

                    }
                    else
                    {
                        objAircraft.PowerSettings1HourRange = Convert.ToDecimal(tbPS1HRRNG.Text);
                    }
                    if ((!string.IsNullOrEmpty(tbPS2HRRNG.Text)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                    {
                        var charExists = (tbPS2HRRNG.Text.IndexOf(':') >= 0) ? true : false;
                        if (!charExists)
                            tbPS2HRRNG.Text = tbPS2HRRNG.Text + ":00";
                        objAircraft.PowerSettings2HourRange = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPS2HRRNG.Text, true, "GENERAL")), 3);
                    }
                    else
                    {
                        objAircraft.PowerSettings2HourRange = Convert.ToDecimal(tbPS2HRRNG.Text);
                    }
                    if ((!string.IsNullOrEmpty(tbPS3HRRNG.Text)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                    {
                        var charExists = (tbPS3HRRNG.Text.IndexOf(':') >= 0) ? true : false;
                        if (!charExists)
                            tbPS3HRRNG.Text = tbPS3HRRNG.Text + ":00";
                        objAircraft.PowerSettings3HourRange = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPS3HRRNG.Text, true, "GENERAL")), 3);
                    }
                    else
                    {
                        objAircraft.PowerSettings3HourRange = Convert.ToDecimal(tbPS3HRRNG.Text);
                    }
                    if ((!string.IsNullOrEmpty(tbPS1TOBIAS.Text)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                    {
                        var charExists = (tbPS1TOBIAS.Text.IndexOf(':') >= 0) ? true : false;
                        if (!charExists)
                            tbPS1TOBIAS.Text = tbPS1TOBIAS.Text + ":00";
                        objAircraft.PowerSettings1TakeOffBias = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPS1TOBIAS.Text, true, "GENERAL")), 3);
                    }
                    else
                    {
                        objAircraft.PowerSettings1TakeOffBias = Convert.ToDecimal(tbPS1TOBIAS.Text);
                    }
                    if ((!string.IsNullOrEmpty(tbPS2TOBIAS.Text)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                    {
                        var charExists = (tbPS2TOBIAS.Text.IndexOf(':') >= 0) ? true : false;
                        if (!charExists)
                            tbPS2TOBIAS.Text = tbPS2TOBIAS.Text + ":00";
                        objAircraft.PowerSettings2TakeOffBias = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPS2TOBIAS.Text, true, "GENERAL")), 3);
                    }
                    else
                    {
                        objAircraft.PowerSettings2TakeOffBias = Convert.ToDecimal(tbPS2TOBIAS.Text);
                    }
                    if ((!string.IsNullOrEmpty(tbPS3TOBIAS.Text)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                    {
                        var charExists = (tbPS3TOBIAS.Text.IndexOf(':') >= 0) ? true : false;
                        if (!charExists)
                            tbPS3TOBIAS.Text = tbPS3TOBIAS.Text + ":00";
                        objAircraft.PowerSettings3TakeOffBias = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPS3TOBIAS.Text, true, "GENERAL")), 3);
                    }
                    else
                    {
                        objAircraft.PowerSettings3TakeOffBias = Convert.ToDecimal(tbPS3TOBIAS.Text);
                    }
                    if ((!string.IsNullOrEmpty(tbPS1LNDBIAS.Text)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                    {
                        var charExists = (tbPS1LNDBIAS.Text.IndexOf(':') >= 0) ? true : false;
                        if (!charExists)
                            tbPS1LNDBIAS.Text = tbPS1LNDBIAS.Text + ":00";
                        objAircraft.PowerSettings1LandingBias = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPS1LNDBIAS.Text, true, "GENERAL")), 3);
                    }
                    else
                    {
                        objAircraft.PowerSettings1LandingBias = Convert.ToDecimal(tbPS1LNDBIAS.Text);
                    }
                    if ((!string.IsNullOrEmpty(tbPS2LNDBIAS.Text)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                    {
                        var charExists = (tbPS2LNDBIAS.Text.IndexOf(':') >= 0) ? true : false;
                        if (!charExists)
                            tbPS2LNDBIAS.Text = tbPS2LNDBIAS.Text + ":00";
                        objAircraft.PowerSettings2LandingBias = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPS2LNDBIAS.Text, true, "GENERAL")), 3);
                    }
                    else
                    {
                        objAircraft.PowerSettings2LandingBias = Convert.ToDecimal(tbPS2LNDBIAS.Text);
                    }
                    if ((!string.IsNullOrEmpty(tbPS3LNDBIAS.Text)) && ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)))
                    {
                        var charExists = (tbPS3LNDBIAS.Text.IndexOf(':') >= 0) ? true : false;
                        if (!charExists)
                            tbPS3LNDBIAS.Text = tbPS3LNDBIAS.Text + ":00";
                        objAircraft.PowerSettings3LandingBias = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(tbPS3LNDBIAS.Text, true, "GENERAL")), 3);
                    }
                    else
                    {
                        objAircraft.PowerSettings3LandingBias = Convert.ToDecimal(tbPS3LNDBIAS.Text);
                    }
                    if (rbHours.Checked)
                    {
                        objAircraft.ChargeUnit = "H";
                    }
                    else if (rbNautical.Checked)
                    {
                        objAircraft.ChargeUnit = "N";
                    }
                    else if (rbStatute.Checked)
                    {
                        objAircraft.ChargeUnit = "S";
                    }
                    else if (rbKilometers.Checked)
                    {
                        objAircraft.ChargeUnit = "K";
                    }
                    if (rbfixed.Checked)
                    {
                        objAircraft.IsFixedRotary = "F";
                    }
                    else if (rbRotary.Checked)
                    {
                        objAircraft.IsFixedRotary = "R";
                    }
                    objAircraft.IsInActive = chkInactive.Checked;
                    objAircraft.IsDeleted = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return objAircraft;
            }
        }
        #region "Grid Events"
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftTypeCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// Bind Aircraft Type Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftTypeCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objAircraftTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPakMasterService.GetAllAircraft> AircraftList = new List<GetAllAircraft>();
                            var objDelVal = objAircraftTypeService.GetAircraftList();
                            if (objDelVal.ReturnFlag == true)
                            {
                                AircraftList = objDelVal.EntityList;
                            }
                            dgAircraftTypeCatalog.DataSource = AircraftList;
                            Session["AircraftCodes"] = AircraftList;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Command for Aircraft Type Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftTypeCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Aircraft, Convert.ToInt64(Session["AircraftID"]));
                                    Session["IsEditLockAircraft"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Aircraft);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Aircraft);
                                        return;
                                    }
                                    TimeDisplay();
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                    SelectItem();
                                    DisableLinks();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgAircraftTypeCatalog.SelectedIndexes.Clear();
                                ClearForm();
                                TimeDisplay();
                                DisplayInsertForm();
                                //tbDefaultPowerSetting.Text = "1";
                                //lbDefaultPowerSetText.Text = "CONSTANT MACH";
                                //lbDefaultPowerSetText.Visible = true;
                                GridEnable(true, false, false);
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                DisableLinks();
                                break;
                            case "UpdateEdited":
                                dgAircraftTypeCatalog_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// Update Command for updating the values of Aircraft type in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAircraftTypeCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (MasterCatalogServiceClient objAircraftTypeService = new MasterCatalogServiceClient())
                        {
                            FlightPakMasterService.Aircraft objAircraft = new FlightPakMasterService.Aircraft();
                            objAircraft = GetItems(objAircraft);
                            var ReturnValue = objAircraftTypeService.UpdateAircraft(objAircraft);

                            if (ReturnValue.ReturnFlag == true)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Aircraft, Convert.ToInt64(Session["AircraftID"]));
                                }
                                Session["IsEditLockAircraft"] = "False";

                                //rebind the table and select the item after the update operation
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                GridEnable(true, true, true);
                                dgAircraftTypeCatalog.Rebind();
                                SelectItem();
                                ReadOnlyForm();

                                ShowSuccessMessage();
                                EnableLinks();
                                _selectLastModified = true;
                            }
                            else
                            {
                                //For Data Anotation
                                ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.Aircraft);
                            }
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                //Session["HomeBaseID"] = null;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('RadWindow1');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.reloadAircraftGrid('" + objAircraft.AircraftCD + "');", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// AircraftType Insert Command for inserting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAircraftTypeCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (checkAllReadyExist())
                        {
                            cvAircraftCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objAircraftTypeService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Aircraft objAircraft = new FlightPakMasterService.Aircraft();
                                objAircraft = GetItems(objAircraft);
                                var ReturnValue = objAircraftTypeService.AddAircraft(objAircraft);
                                //For Data Anotation
                                if (ReturnValue.ReturnFlag == true)
                                {
                                    dgAircraftTypeCatalog.Rebind();
                                    GridEnable(true, true, true);
                                    DefaultSelection(true);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation    
                                    ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.Aircraft);
                                }
                                if (IsPopUp)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); var dialog1 = oWnd.get_windowManager().getWindowByName('RadWindow1');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.reloadAircraftGrid('" + objAircraft.AircraftCD + "');oWnd.close();", true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        ///  Aircraft Type Delete Command for deleting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAircraftTypeCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["AircraftID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objAircrafttypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    GridDataItem Item = dgAircraftTypeCatalog.SelectedItems[0] as GridDataItem;
                                    FlightPakMasterService.Aircraft objAircraft = new FlightPakMasterService.Aircraft();
                                    objAircraft.AircraftID = Convert.ToInt64(Item.GetDataKeyValue("AircraftID"));
                                    objAircraft.AircraftCD = Item.GetDataKeyValue("AircraftCD").ToString();
                                    objAircraft.AircraftDescription = Item.GetDataKeyValue("AircraftDescription").ToString();
                                    if (Item.GetDataKeyValue("ChargeRate") != null)
                                    {
                                        objAircraft.ChargeRate = Convert.ToDecimal(Item.GetDataKeyValue("ChargeRate").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    objAircraft.PowerSetting = Item.GetDataKeyValue("PowerSetting").ToString();
                                    if (Item.GetDataKeyValue("PowerDescription") != null)
                                    {
                                        objAircraft.PowerDescription = Item.GetDataKeyValue("PowerDescription").ToString();
                                    }
                                    if (Item.GetDataKeyValue("WindAltitude") != null)
                                    {
                                        objAircraft.WindAltitude = Convert.ToDecimal(Item.GetDataKeyValue("WindAltitude").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    objAircraft.PowerSettings1Description = Item.GetDataKeyValue("PowerSettings1Description").ToString();
                                    if (Item.GetDataKeyValue("PowerSettings1TrueAirSpeed") != null)
                                    {
                                        objAircraft.PowerSettings1TrueAirSpeed = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1TrueAirSpeed").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (Item.GetDataKeyValue("PowerSettings1HourRange") != null)
                                    {
                                        objAircraft.PowerSettings1HourRange = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1HourRange").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (Item.GetDataKeyValue("PowerSettings1TakeOffBias") != null)
                                    {
                                        objAircraft.PowerSettings1TakeOffBias = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1TakeOffBias").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (Item.GetDataKeyValue("PowerSettings1LandingBias") != null)
                                    {
                                        objAircraft.PowerSettings1LandingBias = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings1LandingBias").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    objAircraft.PowerSettings2Description = Item.GetDataKeyValue("PowerSettings2Description").ToString();
                                    if (Item.GetDataKeyValue("PowerSettings2TrueAirSpeed") != null)
                                    {
                                        objAircraft.PowerSettings2TrueAirSpeed = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2TrueAirSpeed").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (Item.GetDataKeyValue("PowerSettings2HourRange") != null)
                                    {
                                        objAircraft.PowerSettings2HourRange = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2HourRange").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (Item.GetDataKeyValue("PowerSettings2LandingBias") != null)
                                    {
                                        objAircraft.PowerSettings2LandingBias = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2LandingBias").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (Item.GetDataKeyValue("PowerSettings2TakeOffBias") != null)
                                    {
                                        objAircraft.PowerSettings2TakeOffBias = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings2TakeOffBias").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    objAircraft.PowerSettings3Description = Item.GetDataKeyValue("PowerSettings3Description").ToString();
                                    if (Item.GetDataKeyValue("PowerSettings3TrueAirSpeed") != null)
                                    {
                                        objAircraft.PowerSettings3TrueAirSpeed = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3TrueAirSpeed").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (Item.GetDataKeyValue("PowerSettings3HourRange") != null)
                                    {
                                        objAircraft.PowerSettings3HourRange = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3HourRange").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (Item.GetDataKeyValue("PowerSettings3TakeOffBias") != null)
                                    {
                                        objAircraft.PowerSettings3TakeOffBias = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3TakeOffBias").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (Item.GetDataKeyValue("PowerSettings3LandingBias") != null)
                                    {
                                        objAircraft.PowerSettings3LandingBias = Convert.ToDecimal(Item.GetDataKeyValue("PowerSettings3LandingBias").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    if (rbHours.Checked)
                                    {
                                        objAircraft.ChargeUnit = "H";
                                    }
                                    else if (rbNautical.Checked)
                                    {
                                        objAircraft.ChargeUnit = "N";
                                    }
                                    else if (rbStatute.Checked)
                                    {
                                        objAircraft.ChargeUnit = "S";
                                    }
                                    else if (rbKilometers.Checked)
                                    {
                                        objAircraft.ChargeUnit = "K";
                                    }
                                    if (rbfixed.Checked)
                                    {
                                        objAircraft.IsFixedRotary = "F";
                                    }
                                    else if (rbRotary.Checked)
                                    {
                                        objAircraft.IsFixedRotary = "R";
                                    }
                                    objAircraft.IsDeleted = true;
                                    //objAircraft.ClientID = bas;
                                    var objRetVal = objAircrafttypeService.GetFleetProfileList().EntityList.Where(x => x.AircraftCD.ToString().ToUpper().Trim().Equals(objAircraft.AircraftCD.ToUpper().Trim()));
                                    if (objRetVal.Count() > 0)
                                    {
                                        
                                        string alertMsg = @"var oManager = $find('" + rwmUpdateConfirmation.ClientID + "');"
                                            + @" oManager.radalert('This Aircraft cannot be Deleted since it has a Fleet Profile Catalog entry', 360, 50, 'Aircraft Type');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                    }
                                    else
                                    {
                                        //Lock the record
                                        var returnValue = CommonService.Lock(EntitySet.Database.Aircraft, Convert.ToInt64(Session["AircraftID"]));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            e.Item.Selected = true;
                                            DefaultSelection(false);
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Aircraft);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Aircraft);
                                            return;
                                        }
                                        objAircrafttypeService.DeleteAircraft(objAircraft);
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                    }
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Aircraft, Convert.ToInt64(Session["AircraftID"]));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftTypeCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgAircraftTypeCatalog.SelectedItems[0] as GridDataItem;
                                Session["AircraftID"] = item["AircraftID"].Text;
                                Session["AircraftCD"] = item["AircraftCD"].Text.Trim().ToString();
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                    }
                }
            }
        }
        /// <summary>
        /// Bind Code and Description to display in Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftTypeCatalog_ItemDataBound(object aSender, GridItemEventArgs anEventArgs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aSender, anEventArgs))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (anEventArgs.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)anEventArgs.Item;
                            //  item["BindingDesc"].Text = item["AircraftCD"].Text.ToString() + "," + item["AircraftDescription"].Text.ToString();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        protected void dgAircraftTypeCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAircraftTypeCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                        AircraftTypePageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        protected void dgAircraftTypeCatalog_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (AircraftTypePageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgAircraftTypeCatalog, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        #endregion
        /// <summary>
        /// Updating grid after insert and update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgAircraftTypeCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }

        protected void ValidateHours(string TextboxValue)
        {
            string Value = TextboxValue;
            string[] words = Value.Split(':');
            if (Value.Contains(":"))
            {
                if ((words[1].Trim() != ""))
                {
                    if ((Convert.ToInt32(words[1]) > 59))
                    {
                        
                        string AlertMsg = @"var oManager = $find('" + rwmUpdateConfirmation.ClientID + "');"
                            + @" oManager.radalert('Minutes must be between 0 and 59', 360, 50, 'Aircraft Type Catalog');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        IsValid = false;
                    }
                }
                else
                {
                    
                    string AlertMsg = @"var oManager = $find('" + rwmUpdateConfirmation.ClientID + "');"
                        + @" oManager.radalert('Minutes must be between 0 and 59', 360, 50, 'Aircraft Type Catalog');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                    IsValid = false;
                }
            }

        }
        /// <summary>
        /// Save and Update Aircraft Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        bool chknAssociateNotExists = true;
                        if (IsEmptyCheck)
                        {
                            if (!string.IsNullOrEmpty(tbAssociateTypeCode.Text.Trim()))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objAircraftTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objAircraftTypeService.GetAircraftList();
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        foreach (GetAllAircraft am in objAircraftTypeService.GetAircraftList().EntityList)
                                        {
                                            if (tbAssociateTypeCode.Text.Trim() != am.AircraftCD.Trim())
                                            {
                                                cvAssociateTypeCode.IsValid = false;
                                                chknAssociateNotExists = false;
                                            }
                                            else
                                            {
                                                chknAssociateNotExists = true;
                                                cvAssociateTypeCode.IsValid = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (UserPrincipal.Identity != null)
                        {
                            // Display Time Fields based on Tenths / Minutes - Company Profile
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                            {
                                string text = "NNN.N";
                                string expression = "^[0-9]{0,3}(\\.[0-9]{0,1})?$";
                                ValidateText(text, expression);
                            }
                            else
                            {
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                {
                                    string text = "HH:MM";
                                    string expression = "^[0-9]{0,2}(:([0-5]{0,1}[0-9]{0,1}))?$";
                                    ValidateText(text, expression);
                                }
                            }
                        }

                        if (IsValid)
                        {
                            if (chknAssociateNotExists)
                            {
                                if (hdnSave.Value == "Update")
                                {
                                    (dgAircraftTypeCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgAircraftTypeCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                                }
                                ///dgAircraftTypeCatalog.Rebind();
                                hdnSave.Value = "";
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ValidateText(string text, string expression)
        {
            bool IsCheck = true;
            int minute = 0;
            if (UserPrincipal.Identity != null)
            {
                Regex regex = new Regex(expression);
                // Display Time Fields based on Tenths / Minutes - Company Profile
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {

                    if (!string.IsNullOrEmpty(tbPS1HRRNG.Text))
                    {
                        if (!regex.IsMatch(tbPS1HRRNG.Text))
                        {
                            cvPS1HRRNG.IsValid = false;
                            cvPS1HRRNG.ErrorMessage = text;
                            RadAjaxManager1.FocusControl(tbPS1HRRNG.ClientID); //tbPS1HRRNG.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS1TOBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS1TOBIAS.Text))
                        {
                            cvPS1TOBIAS.IsValid = false;
                            cvPS1TOBIAS.ErrorMessage = text;
                            RadAjaxManager1.FocusControl(tbPS1TOBIAS.ClientID); //tbPS1TOBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS1LNDBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS1LNDBIAS.Text))
                        {
                            cvPS1LNDBIAS.IsValid = false;
                            cvPS1LNDBIAS.ErrorMessage = text;
                            RadAjaxManager1.FocusControl(tbPS1LNDBIAS.ClientID); //tbPS1LNDBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS2HRRNG.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS2HRRNG.Text))
                        {
                            cvPS2HRRNG.IsValid = false;
                            cvPS2HRRNG.ErrorMessage = text;
                            RadAjaxManager1.FocusControl(tbPS2HRRNG.ClientID); //tbPS2HRRNG.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS2TOBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS2TOBIAS.Text))
                        {
                            cvPS2TOBIAS.IsValid = false;
                            cvPS2TOBIAS.ErrorMessage = text;
                            RadAjaxManager1.FocusControl(tbPS2TOBIAS.ClientID); //tbPS2TOBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS2LNDBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS2LNDBIAS.Text))
                        {
                            cvPS2LNDBIAS.IsValid = false;
                            cvPS2LNDBIAS.ErrorMessage = text;
                            RadAjaxManager1.FocusControl(tbPS2LNDBIAS.ClientID); //tbPS2LNDBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS3HRRNG.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS3HRRNG.Text))
                        {
                            cvPS3HRRNG.IsValid = false;
                            cvPS3HRRNG.ErrorMessage = text;
                            RadAjaxManager1.FocusControl(tbPS3HRRNG.ClientID); //tbPS3HRRNG.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS3TOBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS3TOBIAS.Text))
                        {
                            cvPS3TOBIAS.IsValid = false;
                            cvPS3TOBIAS.ErrorMessage = text;
                            RadAjaxManager1.FocusControl(tbPS3TOBIAS.ClientID); //tbPS3TOBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS3LNDBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS3LNDBIAS.Text))
                        {
                            cvPS3LNDBIAS.IsValid = false;
                            cvPS3LNDBIAS.ErrorMessage = text;
                            IsValid = false;
                            RadAjaxManager1.FocusControl(tbPS3LNDBIAS.ClientID); //tbPS3LNDBIAS.Focus();
                        }
                    }
                }
                else
                {

                    if (!string.IsNullOrEmpty(tbPS1HRRNG.Text))
                    {
                        if (!regex.IsMatch(tbPS1HRRNG.Text))
                        {
                            cvPS1HRRNG.IsValid = false;
                            if (tbPS1HRRNG.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbPS1HRRNG.Text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    minute = Convert.ToInt16(timeArray[1]);
                                }
                                if (minute > 59)
                                {
                                    cvPS1HRRNG.ErrorMessage = "Minute entry must be between 0 and 59";
                                }
                                else
                                {
                                    cvPS1HRRNG.ErrorMessage = text;
                                }
                            }
                            else
                            {
                                cvPS1HRRNG.ErrorMessage = text;
                            }
                            RadAjaxManager1.FocusControl(tbPS1HRRNG.ClientID); //tbPS1HRRNG.Focus();
                            IsValid = false;
                            IsCheck = false;

                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS1TOBIAS.Text)) && (IsCheck))
                    {

                        if (!regex.IsMatch(tbPS1TOBIAS.Text))
                        {
                            cvPS1TOBIAS.IsValid = false;
                            if (tbPS1TOBIAS.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbPS1TOBIAS.Text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    minute = Convert.ToInt16(timeArray[1]);
                                }
                                if (minute > 59)
                                {

                                    cvPS1TOBIAS.ErrorMessage = "Minute entry must be between 0 and 59";
                                }
                                else
                                {
                                    cvPS1TOBIAS.ErrorMessage = text;
                                }

                            }
                            else
                            {
                                cvPS1TOBIAS.ErrorMessage = text;
                            }
                            RadAjaxManager1.FocusControl(tbPS1TOBIAS.ClientID); //tbPS1TOBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS1LNDBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS1LNDBIAS.Text))
                        {
                            cvPS1LNDBIAS.IsValid = false;
                            if (tbPS1LNDBIAS.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbPS1LNDBIAS.Text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    minute = Convert.ToInt16(timeArray[1]);
                                }
                                if (minute > 59)
                                {
                                    cvPS1LNDBIAS.ErrorMessage = "Minute entry must be between 0 and 59";
                                }
                                else
                                {
                                    cvPS1LNDBIAS.ErrorMessage = text;
                                }

                            }
                            else
                            {
                                cvPS1LNDBIAS.ErrorMessage = text;
                            }
                            RadAjaxManager1.FocusControl(tbPS1LNDBIAS.ClientID); //tbPS1LNDBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS2HRRNG.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS2HRRNG.Text))
                        {
                            cvPS2HRRNG.IsValid = false;
                            if (tbPS2HRRNG.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbPS2HRRNG.Text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    minute = Convert.ToInt16(timeArray[1]);
                                }
                                if (minute > 59)
                                {
                                    cvPS2HRRNG.ErrorMessage = "Minute entry must be between 0 and 59";
                                }
                                else
                                {
                                    cvPS2HRRNG.ErrorMessage = text;
                                }

                            }
                            else
                            {
                                cvPS2HRRNG.ErrorMessage = text;
                            }
                            RadAjaxManager1.FocusControl(tbPS2HRRNG.ClientID); //tbPS2HRRNG.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS2TOBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS2TOBIAS.Text))
                        {
                            cvPS2TOBIAS.IsValid = false;
                            if (tbPS2TOBIAS.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbPS2TOBIAS.Text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    minute = Convert.ToInt16(timeArray[1]);
                                }
                                if (minute > 59)
                                {
                                    cvPS2TOBIAS.ErrorMessage = "Minute entry must be between 0 and 59";
                                }
                                else
                                {
                                    cvPS2TOBIAS.ErrorMessage = text;
                                }

                            }
                            else
                            {
                                cvPS2TOBIAS.ErrorMessage = text;
                            }
                            RadAjaxManager1.FocusControl(tbPS2TOBIAS.ClientID); //tbPS2TOBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS2LNDBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS2LNDBIAS.Text))
                        {
                            cvPS2LNDBIAS.IsValid = false;
                            if (tbPS2LNDBIAS.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbPS2LNDBIAS.Text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    minute = Convert.ToInt16(timeArray[1]);
                                }
                                if (minute > 59)
                                {
                                    cvPS2LNDBIAS.ErrorMessage = "Minute entry must be between 0 and 59";
                                }
                                else
                                {
                                    cvPS2LNDBIAS.ErrorMessage = text;
                                }

                            }
                            else
                            {
                                cvPS2LNDBIAS.ErrorMessage = text;
                            }
                            RadAjaxManager1.FocusControl(tbPS2LNDBIAS.ClientID); //tbPS2LNDBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS3HRRNG.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS3HRRNG.Text))
                        {
                            cvPS3HRRNG.IsValid = false;
                            if (tbPS3HRRNG.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbPS3HRRNG.Text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    minute = Convert.ToInt16(timeArray[1]);
                                }
                                if (minute > 59)
                                {
                                    cvPS3HRRNG.ErrorMessage = "Minute entry must be between 0 and 59";
                                }
                                else
                                {
                                    cvPS3HRRNG.ErrorMessage = text;
                                }

                            }
                            else
                            {
                                cvPS3HRRNG.ErrorMessage = text;
                            }
                            RadAjaxManager1.FocusControl(tbPS3HRRNG.ClientID); //tbPS3HRRNG.Focus();
                            IsValid = false;
                            IsCheck = false;
                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS3TOBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS3TOBIAS.Text))
                        {
                            cvPS3TOBIAS.IsValid = false;
                            if (tbPS3TOBIAS.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbPS3TOBIAS.Text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    minute = Convert.ToInt16(timeArray[1]);
                                }
                                if (minute > 59)
                                {
                                    cvPS3TOBIAS.ErrorMessage = "Minute entry must be between 0 and 59";
                                }
                                else
                                {
                                    cvPS3TOBIAS.ErrorMessage = text;
                                }
                            }
                            else
                            {
                                cvPS3TOBIAS.ErrorMessage = text;
                            }
                            RadAjaxManager1.FocusControl(tbPS3TOBIAS.ClientID); //tbPS3TOBIAS.Focus();
                            IsValid = false;
                            IsCheck = false;

                        }
                    }
                    if ((!string.IsNullOrEmpty(tbPS3LNDBIAS.Text)) && (IsCheck))
                    {
                        if (!regex.IsMatch(tbPS3LNDBIAS.Text))
                        {
                            cvPS3LNDBIAS.IsValid = false;
                            if (tbPS3LNDBIAS.Text.IndexOf(":") != -1)
                            {
                                string[] timeArray = tbPS3LNDBIAS.Text.Split(':');
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    minute = Convert.ToInt16(timeArray[1]);
                                }

                                if (minute > 59)
                                {
                                    cvPS3LNDBIAS.ErrorMessage = "Minute entry must be between 0 and 59";
                                }
                                else
                                {
                                    cvPS3LNDBIAS.ErrorMessage = text;
                                }
                            }
                            else
                            {
                                cvPS3LNDBIAS.ErrorMessage = text;
                            }
                            IsValid = false;
                            RadAjaxManager1.FocusControl(tbPS3LNDBIAS.ClientID); //tbPS3LNDBIAS.Focus();
                        }
                    }
                }
            }

        }
        /// <summary>
        /// Cancel Aircraft Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.Aircraft, Convert.ToInt64(Session["AircraftID"]));
                            //Session.Remove("AircraftID");
                            GridEnable(true, true, true);
                            DefaultSelection(true);
                            EnableLinks();
                        }
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radAircraftTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        ///  Default Power Setting Text Changes according to the value in textbox
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void tbDefaultPowerSetting_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbDefaultPowerSetText.Visible = true;
                        if (tbDefaultPowerSetting.Text == "1")
                        {
                            lbDefaultPowerSetText.Text = " Constant Mach";
                            RadAjaxManager1.FocusControl(tbWindAltitude.ClientID); //tbWindAltitude.Focus();
                        }
                        else if (tbDefaultPowerSetting.Text == "2")
                        {
                            lbDefaultPowerSetText.Text = " Long Range Cruise";
                            RadAjaxManager1.FocusControl(tbWindAltitude.ClientID); //tbWindAltitude.Focus();
                        }
                        else if (tbDefaultPowerSetting.Text == "3")
                        {
                            lbDefaultPowerSetText.Text = " High Speed Cruise";
                            RadAjaxManager1.FocusControl(tbWindAltitude.ClientID); //tbWindAltitude.Focus();
                        }
                        else
                        {
                            RadAjaxManager1.FocusControl(tbDefaultPowerSetting.ClientID); //tbDefaultPowerSetting.Focus();
                            lbDefaultPowerSetText.Text = "";
                            rvDefaultPowerSetting.Validate();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// To check unique Aircraft Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            if (checkAllReadyExist())
                            {
                                cvAircraftCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// To check whether the code is unique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private Boolean checkAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objDelaysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPakMasterService.GetAllAircraft> AircraftList = new List<GetAllAircraft>();
                            AircraftList = ((List<FlightPakMasterService.GetAllAircraft>)Session["AircraftCodes"]).Where(x => x.AircraftCD.Trim().ToUpper().Equals(tbCode.Text.Trim().ToUpper())).ToList();
                            if (AircraftList.Count != 0)
                            {
                                cvAircraftCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                returnVal = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
                return returnVal;
            }
        }
        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbChargeRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbChargeRate.Text.Trim() != string.Empty)
                        {
                            double test = 0;
                            if (double.TryParse(tbChargeRate.Text.Trim(), out test))
                            {
                                if ((test < 0) || (test > 99999999999999.99))
                                    revchargeRate.IsValid = false;
                            }
                            else
                                revchargeRate.IsValid = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        #endregion
       /// <summary>
       /// TextChange for AssociateType
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        protected void tbAssociatetype_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbAssociateTypeCode.Text != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient AircraftService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                //To check for unique metro code
                                var AssociateValue = AircraftService.GetAircraftList().EntityList.Where(x => x.AircraftCD.Trim().ToUpper().ToString() == tbAssociateTypeCode.Text.Trim().ToUpper().ToString()).ToList();
                                if (AssociateValue.Count() == 0 || AssociateValue == null)
                                {
                                    cvAssociateTypeCode.IsValid = false;
                                    RadAjaxManager1.FocusControl(tbAssociateTypeCode.ClientID); //tbAssociateTypeCode.Focus();
                                }
                                else
                                {
                                    tbAssociateTypeCode.Text = ((FlightPakMasterService.GetAllAircraft)AssociateValue[0]).AircraftCD;                                    
                                    RadAjaxManager1.FocusControl(tbDefaultPowerSetting.ClientID); //tbDefaultPowerSetting.Focus();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        /// <summary>
        /// Adding zero's for binding
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        private string AddPadding(string Value)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string[] strArr = new string[2];
                string ReturnValue = string.Empty;
                strArr = Value.Split('.');
                if (strArr[1].Length == 1)
                {
                    ReturnValue = strArr[1].ToString();
                }
                else if (strArr[1].Length == 2)
                {
                    ReturnValue = strArr[1].ToString().Substring(0, 1);
                }
                else if (strArr[1].Length == 3)
                {
                    ReturnValue = strArr[1].ToString().Substring(0, 1);
                }
                return strArr[0] + "." + ReturnValue;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = false;
                                //table2.Visible = false;
                                dgAircraftTypeCatalog.Visible = false;
                                if (IsAdd)
                                {
                                    (dgAircraftTypeCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgAircraftTypeCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }

            
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgAircraftTypeCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgAircraftTypeCatalog.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        protected void Charterrates_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["AircraftID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Session["AircraftID"].ToString()))
                            {

                                Session["IsFleet"] = "false";
                                string FleetID = Session["AircraftID"].ToString().Trim();
                                Session["DispTypeCode"] = System.Web.HttpUtility.HtmlEncode(tbCode.Text);
                                Session["DisplayDescription"] = System.Web.HttpUtility.HtmlEncode(tbDescription.Text);
                                
                                Session["SelectedFleetNewCharterID"] = null;
                                Response.Redirect("FleetNewCharterRate.aspx?AicraftID=" + Microsoft.Security.Application.Encoder.HtmlEncode(FleetID) + "&Screen=AircraftType");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllAircraft> lstAircraft = new List<FlightPakMasterService.GetAllAircraft>();
                if (Session["AircraftCodes"] != null)
                {
                    lstAircraft = (List<FlightPakMasterService.GetAllAircraft>)Session["AircraftCodes"];
                }
                if (lstAircraft.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstAircraft = lstAircraft.Where(x => x.IsInActive == false).ToList<GetAllAircraft>(); }
                    dgAircraftTypeCatalog.DataSource = lstAircraft;
                    if (IsDataBind)
                    {
                        dgAircraftTypeCatalog.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {            
            chkSearchActiveOnly.Enabled = true;         
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgAircraftTypeCatalog.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var AircraftTypeValue = FPKMstService.GetAircraftList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, AircraftTypeValue);
            List<FlightPakMasterService.GetAllAircraft> filteredList = GetFilteredList(AircraftTypeValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.AircraftID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgAircraftTypeCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgAircraftTypeCatalog.CurrentPageIndex = PageNumber;
            dgAircraftTypeCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllAircraft AircraftTypeValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["AircraftID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = AircraftTypeValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().AircraftID;
                Session["AircraftID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllAircraft> GetFilteredList(ReturnValueOfGetAllAircraft AircraftTypeValue)
        {
            List<FlightPakMasterService.GetAllAircraft> filteredList = new List<FlightPakMasterService.GetAllAircraft>();

            if (AircraftTypeValue.ReturnFlag)
            {
                filteredList = AircraftTypeValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetAllAircraft>(); }
                }
            }

            return filteredList;
        }
    }
}