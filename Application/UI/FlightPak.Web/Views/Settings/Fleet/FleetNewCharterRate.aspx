﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Settings.master"
    ClientIDMode="AutoID" CodeBehind="FleetNewCharterRate.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.FleetNewCharterRate"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script runat="server">       
            void QueryFleetButton_Click(object sender, EventArgs e)
            {
                string URL = "../Fleet/FleetProfileCatalog.aspx?FleetID=" + hdnFleetID.Value + "&Screen=FleetProfile";
                Response.Redirect(URL);
            }

            void QueryAircraftButton_Click(object sender, EventArgs e)
            {
                string URL = "../Fleet/AircraftType.aspx?AircraftID=" + hdnAircraftID.Value + "&Screen=AircraftType";
                Response.Redirect(URL);
            }



            void QueryCQButton_Click(object sender, EventArgs e)
            {
                string URL = "../People/CharterQuoteCustomerCatalog.aspx?CqCustomerID=" + hdnCqCustomerId.Value + "&Screen=CharterQuote";
                Response.Redirect(URL);
            } 
            
                 
        </script>
        <script type="text/javascript">

            function GenerateExpression(type1) {
                if (type1 == 'DomesticFixedCostBuyAccountNumber') {
                    var AccountFormat = document.getElementById('<%=tbDomesticFixedCostBuyAccountNumber.ClientID%>').value;
                }
                if (type1 == 'DomesticSellAccountNumber') {
                    var AccountFormat = document.getElementById('<%=tbDomesticSellAccountNumber.ClientID%>').value;
                }
                if (type1 == 'InternationalFixedCostBuyAccountNumber') {
                    var AccountFormat = document.getElementById('<%=tbInternationalFixedCostBuyAccountNumber.ClientID%>').value;
                }
                if (type1 == 'InternationalSellAccountNumber') {
                    var AccountFormat = document.getElementById('<%=tbInternationalSellAccountNumber.ClientID%>').value;
                }
                var splitdot = AccountFormat.split('');
                var regx = "^";
                for (i = 0; i < splitdot.length; i++) {
                    if (splitdot[i] == '9') {
                        regx = regx + "[0-9]";
                    }
                    if (splitdot[i] == '.') {
                        regx = regx + "[\\. ]";
                    }
                    if (splitdot[i] == '!') {
                        regx = regx + "[\\! ]";
                    }
                }
                regx = regx + "$";
                return regx;
            }

            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            function ValidateAccountFormat(Control, type) {
                if (type == 'DomesticFixedCostBuyAccountNumber') {
                    var AccountFormat = document.getElementById('<%=tbDomesticFixedCostBuyAccountNumber.ClientID%>').value;
                }
                if (type == 'DomesticSellAccountNumber') {
                    var AccountFormat = document.getElementById('<%=tbDomesticSellAccountNumber.ClientID%>').value;
                }
                if (type == 'InternationalFixedCostBuyAccountNumber') {
                    var AccountFormat = document.getElementById('<%=tbInternationalFixedCostBuyAccountNumber.ClientID%>').value;
                }
                if (type == 'InternationalSellAccountNumber') {
                    var AccountFormat = document.getElementById('<%=tbInternationalSellAccountNumber.ClientID%>').value;
                }
                if ((AccountFormat != null) && (AccountFormat != undefined) && (AccountFormat != '')) {
                    var Expression = GenerateExpression(type);
                    var Pattern = new RegExp(Expression);
                    if (Control.value != "") {
                        if (Pattern.exec(Control.value)) {
                            return true;
                        } else {
                            alert(type + " No. didn't match the Account format pattern.");
                            return false;
                        }
                    }
                }
            }

            var ClosingAccount = '';


            function openWinAccount(type) {
                var url = '';

                ClosingAccount = type;

                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");

                if (type == 'DomesticFixedCostBuyAccountNumber') {
                    url = '../../Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbDomesticFixedCostBuyAccountNumber.ClientID%>').value;
                    var oWnd = oManager.open(url, 'radAccountMasterPopup'); 

                    oWnd.add_close(OnClientCloseAccountMaster);


                }
                if (type == 'DomesticSellAccountNumber') {
                    url = '../../Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbDomesticSellAccountNumber.ClientID%>').value;
                    var oWnd = oManager.open(url, 'radAccountMasterPopup'); 

                    oWnd.add_close(OnClientCloseAccountMaster);


                }
                if (type == 'InternationalFixedCostBuyAccountNumber') {
                    url = '../../Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbInternationalFixedCostBuyAccountNumber.ClientID%>').value;
                    var oWnd = oManager.open(url, 'radAccountMasterPopup'); 

                    oWnd.add_close(OnClientCloseAccountMaster);


                }
                if (type == 'InternationalSellAccountNumber') {
                    url = '../../Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbInternationalSellAccountNumber.ClientID%>').value;
                    var oWnd = oManager.open(url, 'radAccountMasterPopup'); 

                    oWnd.add_close(OnClientCloseAccountMaster);


                }

            }

            function openAircraftCopy() {

                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");

                if (document.getElementById('<%=hdnMode.ClientID%>').value == 'Fleet') {
                    var url = '';
                    url = '../../Settings/Fleet/Fleetcopy.aspx?seltab=Fleet&Mode=1&OldFleetID=' + document.getElementById('<%=hdnFleetID.ClientID%>').value;
                    var oWnd = oManager.open(url, 'radCopyAircraft'); 
                }
                else if (document.getElementById('<%=hdnMode.ClientID%>').value == 'CQCD') {
                    var url = '';
                    url = '../../Settings/Fleet/Fleetcopy.aspx?seltab=Fleet&Mode=2&OldCQID=' + document.getElementById('<%=hdnCqCustomerId.ClientID%>').value;
                    var oWnd = oManager.open(url, 'radCopyAircraft'); 
                }
                else {

                    var url = '';
                    url = '../../Settings/Fleet/Fleetcopy.aspx?seltab=Fleet&Mode=0&OldAircraftID=' + document.getElementById('<%=hdnAircraftID.ClientID%>').value + '&OldAircraftCD=' + document.getElementById('<%=hdnAircraftCD.ClientID%>').value;
                    var oWnd = oManager.open(url, 'radCopyAircraft'); 
                }

            }

            function OnClientCloseAccountMaster(oWnd, args) {

                if (ClosingAccount == 'DomesticFixedCostBuyAccountNumber') {

                    var arg = args.get_argument();
                    if (arg != null) {
                        if (arg) {
                            document.getElementById("<%=tbDomesticFixedCostBuyAccountNumber.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvFixedCostBuyAccountNumber.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnDBAccountID.ClientID%>").innerHTML = arg.AccountID;
                        }
                        else {
                            document.getElementById("<%=tbDomesticFixedCostBuyAccountNumber.ClientID%>").value = "";
                            document.getElementById("<%=cvFixedCostBuyAccountNumber.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnDBAccountID.ClientID%>").innerHTML = "";


                        }
                    }
                }
                if (ClosingAccount == 'DomesticSellAccountNumber') {

                    var arg = args.get_argument();
                    if (arg != null) {
                        if (arg) {
                            document.getElementById("<%=tbDomesticSellAccountNumber.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvSellAccountNumber.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnDSAccountID.ClientID%>").innerHTML = arg.AccountID;
                        }
                        else {
                            document.getElementById("<%=tbDomesticSellAccountNumber.ClientID%>").value = "";
                            document.getElementById("<%=hdnDSAccountID.ClientID%>").innerHTML = "";
                            document.getElementById("<%=cvSellAccountNumber.ClientID%>").innerHTML = "";
                        }
                    }

                }
                if (ClosingAccount == 'InternationalFixedCostBuyAccountNumber') {



                    var arg = args.get_argument();
                    if (arg != null) {
                        if (arg) {
                            document.getElementById("<%=tbInternationalFixedCostBuyAccountNumber.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvDomesticFixedCostBuyAccountNumber.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdBuyAccountID.ClientID%>").innerHTML = arg.AccountID;
                        }
                        else {
                            document.getElementById("<%=tbInternationalFixedCostBuyAccountNumber.ClientID%>").value = "";
                            document.getElementById("<%=hdBuyAccountID.ClientID%>").innerHTML = "";
                            document.getElementById("<%=cvDomesticFixedCostBuyAccountNumber.ClientID%>").innerHTML = "";
                        }
                    }
                }
                if (ClosingAccount == 'InternationalSellAccountNumber') {


                    var arg = args.get_argument();
                    if (arg != null) {
                        if (arg) {
                            document.getElementById("<%=tbInternationalSellAccountNumber.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvInternationalSellAccountNumber.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnSellAccountID.ClientID%>").innerHTML = arg.AccountID;
                        }
                        else {
                            document.getElementById("<%=tbInternationalSellAccountNumber.ClientID%>").value = "";
                            document.getElementById("<%=hdnSellAccountID.ClientID%>").innerHTML = "";
                            document.getElementById("<%=cvInternationalSellAccountNumber.ClientID%>").innerHTML = "";
                        }
                    }
                }

            }



            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function prepareSearchInput(input) { input.value = input.value; }            
        </script>
        <style>
            .art-setting-PostContent .RadAjaxPanel .rgHeaderWrapper .rgHeaderDiv{width:751px !important}
            .art-contentLayout_popup .RadAjaxPanel .rgHeaderWrapper .rgHeaderDiv{width:961px !important}
        </style>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div id="TBLlINK" runat="server" class="tab-nav-top">
                    <span class="head-title">
                        <asp:LinkButton ID="lnkFleet" Text="Fleet Profile" runat="server" OnClick="QueryFleetButton_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkAircraft" Text="Aircraft Type" runat="server" OnClick="QueryAircraftButton_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkCqCustomer" Text="Charter Quote" runat="server" OnClick="QueryCQButton_Click"></asp:LinkButton>&nbsp;>
                        Charter Rates</span> <span class="tab-nav-icons"><a href="#" title="Help" class="help-icon">
                        </a></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" class="sticky">
        <tr style="display: none;">
            <td colspan="3">
                <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTailNum" Text="Tail No." CssClass="tdLabel170" runat="server"></asp:Label>
                <asp:TextBox ID="tbTailNumber" CssClass="tdLabel50" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lblTypeCode" CssClass="tdLabel170" Text="Type Code" runat="server"></asp:Label>
                <asp:TextBox ID="tbAircraftTypeCode" CssClass="tdLabel100" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lblDescription" CssClass="tdLabel70" Text="Description" runat="server"></asp:Label>
                <asp:TextBox ID="tbAircraftTypeDescription" CssClass="tdLabel130" runat="server"></asp:TextBox>
                <%-- <td class="tdLabel100">
                <!--Customer Code-->
            </td>
            <td>
                <asp:TextBox ID="tbCustomerCD" runat="server" Visible="false"></asp:TextBox>
            </td>--%>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCopyAircraft" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Fleet/Fleetcopy.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0" class="head-sub-menu" width="100%">
            <tr>
                <td>
                    <div class="fleet_select">
                        <asp:LinkButton ID="lbtnCopy" CssClass="fleet_link" runat="server" OnClientClick="javascript:openAircraftCopy();return false;">Copy</asp:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:RadioButton ID="rbCharterRates" runat="server" Checked="true" Text="Charter Rate"
                        GroupName="Info" OnCheckedChanged="CharterRates_CheckedChanged" AutoPostBack="true"
                        ToolTip="Charter Rates" />
                </td>
                <td>
                    <asp:RadioButton ID="rbAccountInformation" runat="server" Text="Account Information"
                        GroupName="Info" OnCheckedChanged="CharterRates_CheckedChanged" AutoPostBack="true"
                        ToolTip="Account Information" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="dgFleetNewCharterRate" runat="server" AllowSorting="true" OnItemCreated="dgFleetNewCharterRate_ItemCreated"
            OnPageIndexChanged="dgAircraftType_PageIndexChanged" OnNeedDataSource="dgFleetNewCharterRate_BindData"
            OnItemCommand="dgFleetNewCharterRate_ItemCommand" OnColumnCreated="dgFleetNewCharterRate_ColumnCreated"
            OnInsertCommand="dgFleetNewCharterRate_InsertCommand" OnUpdateCommand="dgFleetNewCharterRate_UpdateCommand"
            OnDeleteCommand="dgFleetNewCharterRate_DeleteCommand" OnItemDataBound="dgFleetNewCharterRate_ItemDataBound"
            AutoGenerateColumns="false" Height="341px" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgFleetNewCharterRate_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPreRender="dgFleetNewCharterRate_PreRender">
            <MasterTableView DataKeyNames="FleetNewCharterRateID,CustomerID,FleetID,IsUWAData"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="FleetNewCharterRateID" HeaderText="Description"
                        Display="false" UniqueName="FleetNewCharterRateID" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FleetNewCharterRateDescription" HeaderText="Description"
                        CurrentFilterFunction="StartsWith" UniqueName="FleetNewCharterRateDescription"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="140px"
                        FilterControlWidth="120px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ChargeUnit" HeaderText="Charge Unit" CurrentFilterFunction="StartsWith"
                        AutoPostBackOnFilter="false" UniqueName="ChargeUnit" ShowFilterIcon="false" HeaderStyle-Width="100px"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="BuyDOM" HeaderText="Domestic Fixed Cost Buy"
                        AutoPostBackOnFilter="false" UniqueName="BuyDOM" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        HeaderStyle-Width="70px" FilterControlWidth="50px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SellDOM" HeaderText="Domestic Sell" AutoPostBackOnFilter="false"
                        UniqueName="SellDOM" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        HeaderStyle-Width="70px" FilterControlWidth="50px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsTaxDOM" HeaderText="Tax" AutoPostBackOnFilter="true"
                        UniqueName="IsTaxDOM" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        AllowFiltering="false" HeaderStyle-Width="40px">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridCheckBoxColumn DataField="IsDiscountDOM" HeaderText="Disc" AutoPostBackOnFilter="true"
                        AllowFiltering="false" UniqueName="IsDiscountDOM" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        HeaderStyle-Width="40px">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridBoundColumn DataField="BuyIntl" HeaderText="International Fixed Cost Buy"
                        UniqueName="BuyIntl" ShowFilterIcon="false" HeaderStyle-Width="100px" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="EqualTo" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SellIntl" HeaderText="International Sell" AutoPostBackOnFilter="false"
                        UniqueName="SellIntl" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        HeaderStyle-Width="100px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsTaxIntl" HeaderText="Tax" AutoPostBackOnFilter="true"
                        AllowFiltering="false" UniqueName="IsTaxIntl" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        HeaderStyle-Width="40px">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridCheckBoxColumn DataField="IsDiscountIntl" HeaderText="Disc" AutoPostBackOnFilter="true"
                        AllowFiltering="false" UniqueName="IsDiscountIntl" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        HeaderStyle-Width="40px">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridBoundColumn DataField="DBAccountNum" HeaderText="Domestic Fixed Cost Buy Account Number"
                        UniqueName="DBAircraftFlightChg" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                        CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DSAccountNum" HeaderText="Domestic Sell Account Number"
                        UniqueName="DSAircraftFlightChg" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                        CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="BuyAccountNum" HeaderText="International Fixed Cost Buy Account Number"
                        UniqueName="BuyAircraftFlightIntl" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                        CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SellAccountNum" HeaderText="International Sell Account Number"
                        UniqueName="IsSellAircraftFlightIntl" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                        CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IsUWAData" HeaderText="IsUWAData" Display="false"
                        UniqueName="IsUWAData" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            Visible='<%# IsAuthorized(Permission.Database.AddFleetChargeHistory)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            Visible='<%# IsAuthorized(Permission.Database.EditFleetChargeHistory)%>' ToolTip="Edit"
                            CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                            Visible='<%# IsAuthorized(Permission.Database.DeleteFleetChargeHistory)%>' runat="server"
                            CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                    <div style="padding: 5px 5px; float: right;">
                        <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false"></GroupingSettings>
        </telerik:RadGrid>
        <asp:Panel ID="PnlBody" runat="server">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" class="border-box">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr valign="top">
                                <td class="tdLabel150">
                                    Description
                                </td>
                                <td valign="top" class="tdLabel180">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbdescription" runat="server" CssClass="text150" MaxLength="40"
                                                    ValidationGroup="Save"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="tbdescription"
                                                    ValidationGroup="Save" Text="Description is Required" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="alert-text"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel180">
                                    Minimum Margin %
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbMarginPercentage" runat="server" CssClass="text150" ValidationGroup="Save"
                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="6">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revMarginalPercentage" runat="server" Display="Dynamic"
                                                    ValidationExpression="^[0-1]{0,1}[0-9]{0,2}(\.[0-9]{1,2})?$" ErrorMessage="Invalid Format(Upto 100 Only)"
                                                    ControlToValidate="tbMarginPercentage" CssClass="alert-text" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr valign="top">
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="tdLabel150">
                                                Charge Unit
                                            </td>
                                            <td class="tdLabel180">
                                                <asp:DropDownList CssClass="text160" ID="rblChargeUnit" runat="server">
                                                    <asp:ListItem Text="Per Flight Hour" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Per Nautical Mile" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Per Statute Mile" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Per Kilometer" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="% of Daily Usage" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="Per Hour" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="Per Leg" Value="7"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr valign="top">
                                <td class="nav-space">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <table>
                                <tr valign="top">
                                    <td class="tdLabel170">
                                        Minimum Daily Usage/Hours
                                    </td>
                                    <td class="tdLabel180">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbMinimumDailyUsageHours" runat="server" CssClass="text150" ValidationGroup="Save"
                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="16">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revMinimumDailyUsageHours" runat="server" Display="Dynamic"
                                                        ErrorMessage="Invalid Format" ControlToValidate="tbMinimumDailyUsageHours" CssClass="alert-text"
                                                        ValidationGroup="Save" ValidationExpression="^[0-9]{0,13}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel180">
                                        <asp:CheckBox ID="chkTaxDailyUsage" runat="server" Text="Tax Daily Usage Adjustment" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkTaxLandingFee" runat="server" Text="Tax Landing Fee" />
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        Domestic Standard Crew
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbDomesticStandardCrew" runat="server" CssClass="text150" ValidationGroup="Save"
                                                        onKeyPress="return fnAllowNumeric(this, event)" MaxLength="2">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- <asp:RegularExpressionValidator ID="revDomesticStandardCrew" runat="server" Display="Dynamic"
                                                                ErrorMessage="Invalid Format" ControlToValidate="tbDomesticStandardCrew" CssClass="alert-text"
                                                                ValidationGroup="Save"></asp:RegularExpressionValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel180">
                                        International Standard Crew
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbInternationalStandardCrew" runat="server" CssClass="text150" ValidationGroup="Save"
                                                        onKeyPress="return fnAllowNumeric(this, event)" MaxLength="2">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                                                ErrorMessage="Invalid Format" ControlToValidate="tbMinimumDailyUsageHours" CssClass="alert-text"
                                                                ValidationGroup="Save" ValidationExpression="^[0-9]{0,13}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Domestic</legend>
                            <table>
                                <tr valign="top">
                                    <td class="tdLabel100" valign="top">
                                        Fixed Cost/Buy
                                    </td>
                                    <td class="pr_radtextbox_150">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <%--<asp:TextBox ID="tbDomesticFixedCostBuy" runat="server" CssClass="text150" ValidationGroup="Save"
                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="17">
                                                    </asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbDomesticFixedCostBuy" runat="server" Type="Currency"
                                                        Culture="en-US" MaxLength="14" Value="0.00" NumberFormat-DecimalSeparator="."
                                                        EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%--<asp:RegularExpressionValidator ID="revDomesticFixedCostBuy" runat="server" Display="Dynamic"
                                                        ErrorMessage="Invalid Format" ControlToValidate="tbDomesticFixedCostBuy" CssClass="alert-text"
                                                        ValidationGroup="Save" ValidationExpression="^[0-9]{0,14}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel80">
                                        <asp:CheckBox ID="chkDomesticTax" runat="server" Text="Tax" />
                                    </td>
                                    <td class="tdLabel160">
                                        Fixed Cost/Buy Account No.
                                    </td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbDomesticFixedCostBuyAccountNumber" runat="server" CssClass="text150"
                                                        MaxLength="32" OnTextChanged="DomesticFixedCostBuyAccountNumber_TextChanged"
                                                        AutoPostBack="true" OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')"></asp:TextBox>
                                                    <asp:Button ID="btnFedTaxAccount" OnClientClick="javascript:openWinAccount('DomesticFixedCostBuyAccountNumber');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="cvFixedCostBuyAccountNumber" runat="server" ControlToValidate="tbDomesticFixedCostBuyAccountNumber"
                                                        ErrorMessage="Invalid Account No." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        Sell
                                    </td>
                                    <td class="pr_radtextbox_150">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <%--<asp:TextBox ID="tbDomesticSell" runat="server" CssClass="text150" ValidationGroup="Save"
                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="17">
                                                    </asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbDomesticSell" runat="server" Type="Currency" Culture="en-US"
                                                        MaxLength="17" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- <asp:RegularExpressionValidator ID="revSell" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                        ControlToValidate="tbDomesticSell" CssClass="alert-text" ValidationGroup="Save"
                                                        ValidationExpression="^[0-9]{0,14}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkDomesticDisc" runat="server" Text="Discount" />
                                    </td>
                                    <td>
                                        Sell Account No.
                                    </td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbDomesticSellAccountNumber" runat="server" CssClass="text150" MaxLength="32"
                                                        OnTextChanged="DomesticSellAccountNumber_TextChanged" AutoPostBack="true" OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')"></asp:TextBox>
                                                    <asp:Button ID="Button1" OnClientClick="javascript:openWinAccount('DomesticSellAccountNumber');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="cvSellAccountNumber" runat="server" ControlToValidate="tbDomesticSellAccountNumber"
                                                        ErrorMessage="Invalid Account No." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>International</legend>
                            <table>
                                <tr valign="top">
                                    <td class="tdLabel100">
                                        Fixed Cost/Buy
                                    </td>
                                    <td class="pr_radtextbox_150">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <%--<asp:TextBox ID="tbInternationalFixedCostBuy" runat="server" CssClass="text150" ValidationGroup="Save"
                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="17">
                                                    </asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbInternationalFixedCostBuy" runat="server" Type="Currency"
                                                        Culture="en-US" MaxLength="14" Value="0.00" NumberFormat-DecimalSeparator="."
                                                        EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%--<asp:RegularExpressionValidator ID="revInternationalFixedCostBuy" runat="server"
                                                        Display="Dynamic" ErrorMessage="Invalid Format" ControlToValidate="tbInternationalFixedCostBuy"
                                                        CssClass="alert-text" ValidationGroup="Save" ValidationExpression="^[0-9]{0,14}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel80">
                                        <asp:CheckBox ID="chkInternationalTax" runat="server" Text="Tax" />
                                    </td>
                                    <td class="tdLabel160">
                                        Fixed Cost/Buy Account No.
                                    </td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbInternationalFixedCostBuyAccountNumber" runat="server" CssClass="text150"
                                                        MaxLength="32" OnTextChanged="InternationalFixedCostBuyAccountNumber_TextChanged"
                                                        AutoPostBack="true" OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')"></asp:TextBox>
                                                    <asp:Button ID="Button2" OnClientClick="javascript:openWinAccount('InternationalFixedCostBuyAccountNumber');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="cvDomesticFixedCostBuyAccountNumber" runat="server" ControlToValidate="tbDomesticFixedCostBuyAccountNumber"
                                                        ErrorMessage="Invalid Account No." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        Sell
                                    </td>
                                    <td class="pr_radtextbox_150">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <%-- <asp:TextBox ID="tbInternationalSell" runat="server" CssClass="text150" ValidationGroup="Save"
                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="17">
                                                    </asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbInternationalSell" runat="server" Type="Currency"
                                                        Culture="en-US" MaxLength="17" Value="0.00" NumberFormat-DecimalSeparator="."
                                                        EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%--<asp:RegularExpressionValidator ID="revInternationalSell" runat="server" Display="Dynamic"
                                                        ErrorMessage="Invalid Format" ControlToValidate="tbInternationalSell" CssClass="alert-text"
                                                        ValidationGroup="Save" ValidationExpression="^[0-9]{0,14}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkInternationalDisc" runat="server" Text="Discount" />
                                    </td>
                                    <td>
                                        Sell Account No.
                                    </td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbInternationalSellAccountNumber" runat="server" CssClass="text150"
                                                        MaxLength="32" OnTextChanged="InternationalSellAccountNumber_TextChanged" AutoPostBack="true"
                                                        OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')"></asp:TextBox>
                                                    <asp:Button ID="Button3" OnClientClick="javascript:openWinAccount('InternationalSellAccountNumber');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="cvInternationalSellAccountNumber" runat="server" ControlToValidate="tbInternationalSellAccountNumber"
                                                        ErrorMessage="Invalid Account No." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea" width="100%">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="right" class="custom_radbutton">
                                    <telerik:RadButton ID="btnSaveChanges" runat="server" Text="Save" ValidationGroup="Save"
                                        CausesValidation="true" OnClick="SaveChanges_Click" />
                                    <asp:HiddenField ID="hdnRedirect" runat="server" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" Text="Cancel" OnClick="Cancel_Click" CausesValidation="false"
                                        runat="server" CssClass="button" />
                                    <asp:HiddenField ID="hdnFleetID" runat="server" />
                                    <asp:HiddenField ID="hdnAircraftID" runat="server" />
                                    <asp:HiddenField ID="hdnAircraftCD" runat="server" />
                                    <asp:HiddenField ID="hdnCqCustomerId" runat="server" />
                                    <asp:HiddenField ID="hdnFleetChargeRateID" runat="server" />
                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                    <asp:HiddenField ID="hdnCalculateFleetChargeRate" runat="server" />
                                    <asp:HiddenField ID="hdnDBAccountID" runat="server" />
                                    <asp:HiddenField ID="hdnDSAccountID" runat="server" />
                                    <asp:HiddenField ID="hdBuyAccountID" runat="server" />
                                    <asp:HiddenField ID="hdnSellAccountID" runat="server" />
                                    <asp:HiddenField ID="hdnMode" runat="server" />
                                    <asp:HiddenField ID="hdnOrderNum" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
