﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetNewCharterRate : BaseSecuredPage
    {
        private bool DailyUsage = false;
        private bool LandingFee = false;
        private string MinimumDailyUsageHours = string.Empty;
        private string DomesticStandardCrew = string.Empty;
        private string InternationalStandardCrew = string.Empty;
        private bool _selectLastModified = false;

        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Request.QueryString["FleetID"] != null && Request.QueryString["FleetID"].ToString().Trim() != "")
                        {
                            hdnFleetID.Value = Request.QueryString["FleetID"].ToString();
                            Session["FleetID"] = hdnFleetID.Value;
                            hdnMode.Value = "Fleet";
                        }
                        if (Request.QueryString["TailNum"] != null && Request.QueryString["TailNum"].ToString().Trim() != "")
                        {
                            tbTailNumber.Text = Request.QueryString["TailNum"].ToString();
                        }
                        if (Session["DispTypeCode"] != null && Session["DispTypeCode"].ToString().Trim() != "")
                        {
                            tbAircraftTypeCode.Text = Session["DispTypeCode"].ToString();
                        }
                        //if (Request.QueryString["CQCD"] != null && Request.QueryString["CQCD"].ToString().Trim() != "")
                        //{
                        //    tbCustomerCD.Text = Request.QueryString["CQCD"].ToString();
                        //}
                        if (Session["DisplayDescription"] != null && Session["DisplayDescription"].ToString().Trim() != "")
                        {
                            tbAircraftTypeDescription.Text = Session["DisplayDescription"].ToString();
                        }
                        if (Request.QueryString["AicraftID"] != null && Request.QueryString["AicraftID"].ToString().Trim() != "")
                        {
                            hdnAircraftID.Value = Request.QueryString["AicraftID"].ToString();
                            hdnMode.Value = "Aircraft";
                        }
                        if (Request.QueryString["CQcustomerID"] != null && Request.QueryString["CQcustomerID"].ToString().Trim() != "")
                        {
                            hdnCqCustomerId.Value = Request.QueryString["CQcustomerID"].ToString();
                            hdnMode.Value = "CQCD";
                        }
                        lbtnCopy.ToolTip = "";
                        if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() != "" && Request.QueryString["FromPage"].ToString().Trim() == "charter")
                        {
                            TBLlINK.Visible = false;
                            if (Request.QueryString["FleetID"] != null && Request.QueryString["FleetID"].ToString().Trim() != "")
                            {
                                Session["IsFleet"] = "true";
                            }
                            if (Request.QueryString["CQcustomerID"] != null && Request.QueryString["CQcustomerID"].ToString().Trim() != "")
                            {
                                Session["IsFleet"] = "CQ";
                            }
                            if (Request.QueryString["AicraftID"] != null && Request.QueryString["AicraftID"].ToString().Trim() != "")
                            {
                                Session["IsFleet"] = "Aircraft";
                            }
                            if (Request.QueryString["Description"] != null && Request.QueryString["Description"].ToString().Trim() != "")
                            {
                                tbAircraftTypeDescription.Text = Request.QueryString["Description"];
                            }
                        }

                        if (Session["IsFleet"] != null)
                        {
                            string Isfleet = (string)Session["IsFleet"];
                            if (Isfleet == "true")
                            {
                                lbtnCopy.ToolTip = "Copy Charter Rates to new or existing Tail No.";
                                lnkFleet.Visible = true;
                                lnkAircraft.Visible = false;
                                lnkCqCustomer.Visible = false;
                                //lblTypeCode.Visible = false;
                                //tbAircraftTypeCode.Visible = false;
                            }
                            else if (Isfleet == "CQ")
                            {
                                lnkAircraft.Visible = false;
                                lnkFleet.Visible = false;
                                lnkCqCustomer.Visible = true;
                            }
                            else
                            {
                                lbtnCopy.ToolTip = "Copy Charter Rates to new or existing Aircraft Type";
                                lnkAircraft.Visible = true;
                                lnkFleet.Visible = false;
                                lnkCqCustomer.Visible = false;
                                lblTailNum.Visible = false;
                                tbTailNumber.Visible = false;
                            }
                        }
                        
                        if (IsPopUp)
                        {
                            if (hdnFleetID.Value != null && hdnFleetID.Value != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var DelVal = FleetChargeRateService.GetFleetIDInfo(Convert.ToInt64(hdnFleetID.Value)).EntityList.ToList();
                                    if (DelVal[0].TailNum != null && DelVal[0].TailNum != string.Empty)
                                    {
                                        tbTailNumber.Text = DelVal[0].TailNum;
                                    }
                                    if (DelVal[0].AirCraft_AircraftCD != null && DelVal[0].AirCraft_AircraftCD != string.Empty)
                                    {
                                        tbAircraftTypeCode.Text = DelVal[0].AirCraft_AircraftCD;
                                    }
                                    if (DelVal[0].TypeDescription != null && DelVal[0].TypeDescription != string.Empty)
                                    {
                                        tbAircraftTypeDescription.Text = DelVal[0].TypeDescription;
                                    }
                                }
                            }

                            if (hdnAircraftID.Value != null && hdnAircraftID.Value != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var DelVal = FleetChargeRateService.GetAircraftByAircraftID(Convert.ToInt64(hdnAircraftID.Value)).EntityList.ToList();
                                    if (DelVal[0].AircraftCD != null && DelVal[0].AircraftCD != string.Empty)
                                    {
                                        tbAircraftTypeCode.Text = DelVal[0].AircraftCD;
                                    }
                                    if (DelVal[0].AircraftDescription != null && DelVal[0].AircraftDescription != string.Empty)
                                    {
                                        tbAircraftTypeDescription.Text = DelVal[0].AircraftDescription;
                                    }
                                }
                            }
                           
                        }

                        tbTailNumber.Enabled = false;
                        tbTailNumber.ReadOnly = true;
                        tbAircraftTypeCode.Enabled = false;
                        tbAircraftTypeCode.ReadOnly = true;
                        tbAircraftTypeDescription.Enabled = false;
                        tbAircraftTypeDescription.ReadOnly = true;
                        //tbCustomerCD.Enabled = false;
                        //tbCustomerCD.ReadOnly = true;
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFleetNewCharterRate, dgFleetNewCharterRate, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFleetNewCharterRate.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewFleetCharterRates);
                            Session["CQConfirmCharterRate"] = null;
                        }
                        if (!IsPostBack)
                        {
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

        }

        protected void dgAircraftType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFleetNewCharterRate.ClientSettings.Scrolling.ScrollTop = "0";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
            }
        }



        protected void Page_PreRender(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedFleetNewCharterID"] != null)
                        {
                            string ID = Session["SelectedFleetNewCharterID"].ToString();
                            foreach (GridDataItem Item in dgFleetNewCharterRate.MasterTableView.Items)
                            {
                                if (Item["FleetNewCharterRateID"].Text.Trim() == ID)
                                {
                                    Item.Selected = true;
                                    break;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFleetNewCharterRate.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFleetNewCharterRate.Rebind();
                    SelectItem();
                    ReadOnlyForm();
        }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetNewCharterRate_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

        }
        /// <summary>
        /// Bind Fleet Charge Rate Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetNewCharterRate_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (Session["IsFleet"] != null)
                            {
                                string Isfleet = (string)Session["IsFleet"];
                                dgFleetNewCharterRate.Columns[3].Visible = rbCharterRates.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[4].Visible = rbCharterRates.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[5].Visible = rbCharterRates.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[6].Visible = rbCharterRates.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[7].Visible = rbCharterRates.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[8].Visible = rbCharterRates.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[9].Visible = rbCharterRates.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[10].Visible = rbCharterRates.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[11].Visible = rbAccountInformation.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[12].Visible = rbAccountInformation.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[13].Visible = rbAccountInformation.Checked ? true : false;
                                dgFleetNewCharterRate.Columns[14].Visible = rbAccountInformation.Checked ? true : false;

                                if (Isfleet == "true")
                                {
                                    if (!string.IsNullOrEmpty(hdnFleetID.Value))
                                    {
                                        var DelVal = FleetChargeRateService.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID != null && x.FleetID == Convert.ToInt64(hdnFleetID.Value) && x.AircraftTypeID == null && x.CQCustomerID == null).ToList();
                                        dgFleetNewCharterRate.DataSource = DelVal;
                                    }
                                }
                                else if (Isfleet == "CQ")
                                {
                                    if (!string.IsNullOrEmpty(hdnCqCustomerId.Value))
                                    {
                                        var DelVal = FleetChargeRateService.GetAllFleetNewCharterRate().EntityList.Where(x => x.CQCustomerID != null && x.CQCustomerID == Convert.ToInt64(hdnCqCustomerId.Value) && x.FleetID == null && x.AircraftTypeID == null).ToList();
                                        dgFleetNewCharterRate.DataSource = DelVal;
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(hdnAircraftID.Value))
                                    {
                                        var DelVal = FleetChargeRateService.GetAllFleetNewCharterRate().EntityList.Where(x => x.AircraftTypeID != null && x.AircraftTypeID == Convert.ToInt64(hdnAircraftID.Value) && x.FleetID == null && x.CQCustomerID == null).ToList();
                                        if (DelVal.Count > 0)
                                        {
                                            hdnAircraftCD.Value = DelVal[0].AircraftTypeCD.ToString();
                                        }
                                        dgFleetNewCharterRate.DataSource = DelVal;
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }


        }
        /// <summary>
        /// Item Command for Fleet Component Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        private void SelectItem()
        {
            if (Session["SelectedFleetNewCharterID"] != null)
            {
                string ID = Session["SelectedFleetNewCharterID"].ToString();
                foreach (GridDataItem Item in dgFleetNewCharterRate.MasterTableView.Items)
                {
                    if (Item["FleetNewCharterRateID"].Text.Trim() == ID)
                    {
                        Item.Selected = true;
                        break;
                    }
                }
                if (dgFleetNewCharterRate.SelectedItems.Count == 0)
                {
                    DefaultSelection(false);
                }
            }
            else
            {
                DefaultSelection(false);
            }
        }

        protected void dgFleetNewCharterRate_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedFleetNewCharterID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.FleetNewCharterRate, Convert.ToInt64(Session["SelectedFleetNewCharterID"].ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FleetNewCharterRate);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetNewCharterRate);
                                            return;
                                        }

                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbdescription.Focus();
                                        SelectItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFleetNewCharterRate.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                tbdescription.Focus();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

        }


        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    if (hdnOrderNum.Value == "1" || hdnOrderNum.Value == "2" || hdnOrderNum.Value == "3")
                    {
                        rblChargeUnit.Enabled = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        public FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);

                    if (objPowerSetting.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;
                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;

                }
            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFleetNewCharterID"] != null)
                    {
                        Session["ISUWAData"] = false;
                        using (FlightPakMasterService.MasterCatalogServiceClient objCharterService = new MasterCatalogServiceClient())
                        {
                            var Results = objCharterService.GetFleetNewCharterByFleetNewCharterID(Convert.ToInt64(Session["SelectedFleetNewCharterID"].ToString())).EntityList;
                            if (Results.Count() > 0 && Results != null)
                            {
                                if (Results[0] != null)
                                {
                                    if (Convert.ToBoolean(Results[0].IsUWAData))
                                    {
                                        Session["ISUWAData"] = Results[0].IsUWAData;
                                    }                                    

                                    if (Results[0].FleetNewCharterRateDescription != null)
                                    {
                                        tbdescription.Text = Results[0].FleetNewCharterRateDescription;
                                    }
                                    else
                                    {
                                        tbdescription.Text = string.Empty;
                                    }

                                    if (Results[0].OrderNum != null)
                                    {
                                        hdnOrderNum.Value = Convert.ToString(Results[0].OrderNum);
                                    }
                                    else
                                    {
                                        hdnOrderNum.Value = string.Empty;
                                    }
                                    if (Results[0].BuyDOM != null)
                                    {
                                        tbDomesticFixedCostBuy.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Results[0].BuyDOM), 2));
                                    }
                                    else
                                    {
                                        tbDomesticFixedCostBuy.Text = string.Empty;
                                    }
                                    if (Results[0].BuyIntl != null)
                                    {
                                        tbInternationalFixedCostBuy.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Results[0].BuyIntl), 2));
                                    }
                                    else
                                    {
                                        tbInternationalFixedCostBuy.Text = string.Empty;
                                    }
                                    if (Results[0].SellDOM != null)
                                    {
                                        tbDomesticSell.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Results[0].SellDOM), 2));
                                    }
                                    else
                                    {
                                        tbDomesticSell.Text = string.Empty;
                                    }
                                    if (Results[0].SellIntl != null)
                                    {
                                        tbInternationalSell.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Results[0].SellIntl), 2));
                                    }
                                    else
                                    {
                                        tbInternationalSell.Text = string.Empty;
                                    }


                                    if (Results[0].IsTaxDOM != null && Results[0].IsTaxDOM == true)
                                    {
                                        chkDomesticTax.Checked = true;
                                    }
                                    else
                                    {
                                        chkDomesticTax.Checked = false;
                                    }
                                    if (Results[0].IsTaxIntl != null && Results[0].IsTaxIntl == true)
                                    {
                                        chkInternationalTax.Checked = true;
                                    }
                                    else
                                    {
                                        chkInternationalTax.Checked = false;
                                    }
                                    if (Results[0].IsDiscountDOM != null && Results[0].IsDiscountDOM == true)
                                    {
                                        chkDomesticDisc.Checked = true;
                                    }
                                    else
                                    {
                                        chkDomesticDisc.Checked = false;
                                    }
                                    if (Results[0].IsDiscountIntl != null && Results[0].IsDiscountIntl == true)
                                    {
                                        chkInternationalDisc.Checked = true;
                                    }
                                    else
                                    {
                                        chkInternationalDisc.Checked = false;
                                    }

                                    if (Results[0].DBAccountNum != null)
                                    {
                                        tbDomesticFixedCostBuyAccountNumber.Text = Convert.ToString(Results[0].DBAccountNum);
                                    }
                                    else
                                    {
                                        tbDomesticFixedCostBuyAccountNumber.Text = string.Empty;
                                    }
                                    if (Results[0].DSAccountNum != null)
                                    {
                                        tbDomesticSellAccountNumber.Text = Convert.ToString(Results[0].DSAccountNum);
                                    }
                                    else
                                    {
                                        tbDomesticSellAccountNumber.Text = string.Empty;
                                    }
                                    if (Results[0].BuyAccountNum != null)
                                    {
                                        tbInternationalFixedCostBuyAccountNumber.Text = Convert.ToString(Results[0].BuyAccountNum);
                                    }
                                    else
                                    {
                                        tbInternationalFixedCostBuyAccountNumber.Text = string.Empty;
                                    }
                                    if (Results[0].SellAccountNum != null)
                                    {
                                        tbInternationalSellAccountNumber.Text = Convert.ToString(Results[0].SellAccountNum);
                                    }
                                    else
                                    {
                                        tbInternationalSellAccountNumber.Text = string.Empty;
                                    }

                                    if (Session["IsFleet"] != null)
                                    {
                                        string Isfleet = (string)Session["IsFleet"];

                                        if (Isfleet == "true")
                                        {
                                            if (hdnFleetID.Value != null)
                                            {
                                                //hdnFleetID.Value = Convert.ToString(Session["FleetID"]);
                                                using (FlightPakMasterService.MasterCatalogServiceClient MasterSvc = new MasterCatalogServiceClient())
                                                {
                                                    List<FlightPakMasterService.GetFleet> GetFleetList = new List<GetFleet>();
                                                    var fleetretval = MasterSvc.GetFleet();
                                                    if (fleetretval.ReturnFlag)
                                                    {
                                                        if (hdnFleetID.Value != "0" && hdnFleetID.Value != null)
                                                        {
                                                            GetFleetList = fleetretval.EntityList.Where(x => x.FleetID == Convert.ToInt64(hdnFleetID.Value)).ToList();
                                                            if (GetFleetList != null && GetFleetList.Count > 0)
                                                            {
                                                                if (GetFleetList[0].marginalpercentage != null)
                                                                {
                                                                    tbMarginPercentage.Text = GetFleetList[0].marginalpercentage.ToString();
                                                                }
                                                                else
                                                                {
                                                                    tbMarginPercentage.Text = string.Empty;
                                                                }
                                                            }
                                                            if (Results[0].FleetIsTaxDailyAdj != null && Results[0].FleetIsTaxDailyAdj == true)
                                                            {
                                                                DailyUsage = true;
                                                            }
                                                            if (Results[0].FleetIsTaxLandingFee != null && Results[0].FleetIsTaxLandingFee == true)
                                                            {
                                                                LandingFee = true;
                                                            }
                                                            if (Results[0].FleetMinimumDay != null)
                                                            {
                                                                MinimumDailyUsageHours = Convert.ToString(Math.Round(Convert.ToDecimal(Results[0].FleetMinimumDay), 2));
                                                            }
                                                            else
                                                            {
                                                                MinimumDailyUsageHours = string.Empty;
                                                            }

                                                            if (Results[0].FleetStandardCrewDOM != null)
                                                            {
                                                                DomesticStandardCrew = Results[0].FleetStandardCrewDOM.ToString();
                                                            }
                                                            else
                                                            {
                                                                DomesticStandardCrew = string.Empty;
                                                            }

                                                            if (Results[0].FleetStandardCrewIntl != null)
                                                            {
                                                                InternationalStandardCrew = Results[0].FleetStandardCrewIntl.ToString();
                                                            }
                                                            else
                                                            {
                                                                InternationalStandardCrew = string.Empty;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if (Isfleet == "CQ")
                                        {
                                            if (hdnCqCustomerId.Value != null)
                                            {
                                                //hdnCqCustomerId.Value = Convert.ToString(Session["CQCustomerID"]);
                                                CQCustomer cq = new CQCustomer();
                                                cq.CQCustomerID = Convert.ToInt64(hdnCqCustomerId.Value);
                                                if (Results != null && Results.Count > 0)
                                                {
                                                    if (Results[0].CQCustomerDailyUsageAdjTax != null && Results[0].CQCustomerDailyUsageAdjTax == true)
                                                    {
                                                        DailyUsage = true;
                                                    }
                                                    if (Results[0].CQCustomerLandingFeeTax != null && Results[0].CQCustomerLandingFeeTax == true)
                                                    {
                                                        LandingFee = true;
                                                    }
                                                    if (Results[0].CQCustomerMinimumDayUseHrs != null)
                                                    {
                                                        MinimumDailyUsageHours = Convert.ToString(Math.Round(Convert.ToDecimal(Results[0].CQCustomerMinimumDayUseHrs), 2));
                                                    }
                                                    else
                                                    {
                                                        MinimumDailyUsageHours = string.Empty;
                                                    }

                                                    if (Results[0].CQCustomerDomesticStdCrewNum != null)
                                                    {
                                                        DomesticStandardCrew = Results[0].CQCustomerDomesticStdCrewNum.ToString();
                                                    }
                                                    else
                                                    {
                                                        DomesticStandardCrew = string.Empty;
                                                    }

                                                    if (Results[0].CQCustomerIntlStdCrewNum != null)
                                                    {
                                                        InternationalStandardCrew = Results[0].CQCustomerIntlStdCrewNum.ToString();
                                                    }
                                                    else
                                                    {
                                                        InternationalStandardCrew = string.Empty;
                                                    }
                                                }
                                                using (MasterCatalogServiceClient MasterSvc = new MasterCatalogServiceClient())
                                                {
                                                    List<FlightPakMasterService.CQCustomer> GetCQCustomer = new List<FlightPakMasterService.CQCustomer>();
                                                    var cqcustretval = MasterSvc.GetCQCustomerList();
                                                    if (cqcustretval.ReturnFlag)
                                                    {
                                                        if (hdnCqCustomerId.Value != "0" && hdnCqCustomerId.Value != null)
                                                        {
                                                            GetCQCustomer = cqcustretval.EntityList.Where(x => x.CQCustomerID == Convert.ToInt64(hdnCqCustomerId.Value)).ToList();
                                                            if (GetCQCustomer != null && GetCQCustomer.Count > 0)
                                                            {
                                                                if (GetCQCustomer[0].MarginalPercentage != null)
                                                                {
                                                                    tbMarginPercentage.Text = GetCQCustomer[0].MarginalPercentage.ToString();
                                                                }
                                                                else
                                                                {
                                                                    tbMarginPercentage.Text = string.Empty;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (hdnAircraftID.Value != null)
                                            {
                                                //hdnAircraftID.Value = Convert.ToString(Session["AircraftID"]);
                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                                                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(Convert.ToInt64(hdnAircraftID.Value));

                                                    if (objPowerSetting.ReturnFlag)
                                                    {
                                                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;
                                                        if (AircraftList != null && AircraftList.Count > 0 && AircraftList[0].MarginalPercentage != null)
                                                        {
                                                            tbMarginPercentage.Text = AircraftList[0].MarginalPercentage.ToString();
                                                        }
                                                        else
                                                        {
                                                            tbMarginPercentage.Text = string.Empty;
                                                        }
                                                    }
                                                }

                                                if (Results != null && Results.Count > 0)
                                                {
                                                    if (Results[0].AircraftDailyUsageAdjTax != null && Results[0].AircraftDailyUsageAdjTax == true)
                                                    {
                                                        DailyUsage = true;
                                                    }
                                                    if (Results[0].AircraftLandingFeeTax != null && Results[0].AircraftLandingFeeTax == true)
                                                    {
                                                        LandingFee = true;
                                                    }
                                                    if (Results[0].AircraftMinimumDayUseHrs != null)
                                                    {
                                                        MinimumDailyUsageHours = Convert.ToString(Math.Round(Convert.ToDecimal(Results[0].AircraftMinimumDayUseHrs), 2));
                                                    }
                                                    else
                                                    {
                                                        MinimumDailyUsageHours = string.Empty;
                                                    }

                                                    if (Results[0].AircraftDomesticStdCrewNum != null)
                                                    {
                                                        DomesticStandardCrew = Results[0].AircraftDomesticStdCrewNum.ToString();
                                                    }
                                                    else
                                                    {
                                                        DomesticStandardCrew = string.Empty;
                                                    }

                                                    if (Results[0].AircraftIntlStdCrewNum != null)
                                                    {
                                                        InternationalStandardCrew = Results[0].AircraftIntlStdCrewNum.ToString();
                                                    }
                                                    else
                                                    {
                                                        InternationalStandardCrew = string.Empty;
                                                    }
                                                }
                                            }
                                        }
                                    }


                                    chkTaxDailyUsage.Checked = DailyUsage;
                                    chkTaxLandingFee.Checked = LandingFee;
                                    tbMinimumDailyUsageHours.Text = MinimumDailyUsageHours;
                                    tbDomesticStandardCrew.Text = DomesticStandardCrew;
                                    tbInternationalStandardCrew.Text = InternationalStandardCrew;



                                    if (Results[0].ChargeUnit != null)
                                    {
                                        if (Results[0].ChargeUnit == "Per Flight Hour")
                                        {
                                            rblChargeUnit.SelectedValue = "1";
                                        }
                                        else if (Results[0].ChargeUnit == "Per Nautical Mile")
                                        {
                                            rblChargeUnit.SelectedValue = "2";
                                        }
                                        else if (Results[0].ChargeUnit == "Per Statute Mile")
                                        {
                                            rblChargeUnit.SelectedValue = "3";
                                        }
                                        else if (Results[0].ChargeUnit == "% of Daily Usage")
                                        {
                                            rblChargeUnit.SelectedValue = "4";
                                        }
                                        else if (Results[0].ChargeUnit == "Fixed")
                                        {
                                            rblChargeUnit.SelectedValue = "5";
                                        }
                                        else if (Results[0].ChargeUnit == "Per Hour")
                                        {
                                            rblChargeUnit.SelectedValue = "6";
                                        }
                                        else if (Results[0].ChargeUnit == "Per Leg")
                                        {
                                            rblChargeUnit.SelectedValue = "7";
                                        }
                                        else if (Results[0].ChargeUnit == "Per Kilometre")
                                        {
                                            rblChargeUnit.SelectedValue = "8";
                                        }
                                    }
                                    else
                                    {
                                        rblChargeUnit.SelectedValue = "0";
                                    }

                                }
                            }
                            //if (!IsPopUp)
                            //{
                            lbColumnName1.Text = "Description";
                            lbColumnValue1.Text = System.Web.HttpUtility.HtmlEncode(tbdescription.Text);
                            lbColumnName2.Text = "Charge Unit";
                            lbColumnValue2.Text = rblChargeUnit.SelectedItem.Text;
                            //}
                            //else
                            //{
                            //    //toolbar.Visible = false;
                            //}
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Update Command for Fleet Charge Rate Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFleetNewCharterRate_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckDomesticBuyAccountExist())
                        {
                            cvFixedCostBuyAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDomesticFixedCostBuyAccountNumber.ClientID);
                            IsValidate = false;
                        }
                        if (CheckDomesticSellAccountExist())
                        {
                            cvSellAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDomesticSellAccountNumber.ClientID);
                            IsValidate = false;
                        }
                        if (CheckInternationalFixedAccountExist())
                        {
                            cvDomesticFixedCostBuyAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbInternationalFixedCostBuyAccountNumber.ClientID);
                            IsValidate = false;
                        }
                        if (CheckInternationalSellAccountExist())
                        {
                            cvInternationalSellAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbInternationalSellAccountNumber.ClientID);
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objPayableVendorService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FleetNewCharterRate oVendor = new FlightPakMasterService.FleetNewCharterRate();
                                var Result = objPayableVendorService.UpdateFleetNewCharterRate(GetItems());
                                
                                if (Result.ReturnFlag == true)
                                {
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedFleetNewCharterID"].ToString().Trim()));
                                    }
                                    GridEnable(true, true, true);
                                    dgFleetNewCharterRate.Rebind();
                                    SelectItem();
                                    ReadOnlyForm();
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.FleetNewCharterRate);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

        }



        /// <summary>
        /// Update Command for Fleet Charge Rate Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFleetNewCharterRate_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckDomesticBuyAccountExist())
                        {
                            cvFixedCostBuyAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDomesticFixedCostBuyAccountNumber.ClientID);
                            IsValidate = false;
                        }
                        if (CheckDomesticSellAccountExist())
                        {
                            cvSellAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDomesticSellAccountNumber.ClientID);
                            IsValidate = false;
                        }
                        if (CheckInternationalFixedAccountExist())
                        {
                            cvDomesticFixedCostBuyAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbInternationalFixedCostBuyAccountNumber.ClientID);
                            IsValidate = false;
                        }
                        if (CheckInternationalSellAccountExist())
                        {
                            cvInternationalSellAccountNumber.IsValid = false;
                            RadAjaxManager1.FocusControl(tbInternationalSellAccountNumber.ClientID);
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objPayableVendorCatalogService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FleetNewCharterRate oVendor = new FlightPakMasterService.FleetNewCharterRate();
                                var Result = objPayableVendorCatalogService.AddFleetNewCharterRate(GetItems());
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.FleetNewCharterRate);
                                }
                            }
                            dgFleetNewCharterRate.Rebind();
                            DefaultSelection(false);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFleetNewCharterRate_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedFleetNewCharterID"] != null)
                        {
                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {
                                GridDataItem Item = dgFleetNewCharterRate.SelectedItems[0] as GridDataItem;
                                FlightPakMasterService.FleetNewCharterRate FleetNewCharterRate = new FlightPakMasterService.FleetNewCharterRate();
                                string FleetNewCharterRateID = Session["SelectedFleetNewCharterID"].ToString();
                                FleetNewCharterRate.FleetNewCharterRateID = Convert.ToInt64(FleetNewCharterRateID);
                                FleetNewCharterRate.IsDeleted = true;
                                bool IsValidate = false;                                
                                if (Convert.ToBoolean(Item.GetDataKeyValue("IsUWAData")) == true)
                                {
                                    IsValidate = Convert.ToBoolean(Item.GetDataKeyValue("IsUWAData"));
                                }
                                if (IsValidate == true)
                                {
                                    RadWindowManager1.RadAlert("You can not delete default record", 400, 100, "Alert", "");
                                }
                                else
                                {

                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Crew, Convert.ToInt64(Session["SelectedFleetNewCharterID"].ToString()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            e.Item.Selected = true;
                                            DefaultSelection(false);
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FleetNewCharterRate);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FleetNewCharterRate);
                                            return;
                                        }
                                    }
                                    CrewRosterService.DeleteFleetNewCharter(FleetNewCharterRate);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = false;
                                    Session["SelectedFleetNewCharterID"] = null;
                                    DefaultSelection(false);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FleetNewCharterRate, Convert.ToInt64(Session["SelectedFleetNewCharterID"].ToString()));
                    }
                }
            }

        }
        /// <summary>
        /// Bind Selected Item  for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetNewCharterRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSaveChanges.Visible == false)
                        {
                            GridDataItem item = dgFleetNewCharterRate.SelectedItems[0] as GridDataItem;
                            if (item["FleetNewCharterRateID"].Text.Trim() != "")
                            {
                                Session["SelectedFleetNewCharterID"] = item["FleetNewCharterRateID"].Text;                                
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }
        }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSave.Value = "Save";
                ClearForm();
                hdnOrderNum.Value = null;
                EnableForm(true);
                DisplayDefaultCharterRate();
            }
        }


        private void DisplayDefaultCharterRate()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["IsFleet"] != null)
                {
                    string Isfleet = (string)Session["IsFleet"];
                    if (Isfleet == "true")
                    {
                        if (hdnFleetID.Value != null)
                        {
                            //hdnFleetID.Value = Convert.ToString(Session["FleetID"]);

                            using (FlightPakMasterService.MasterCatalogServiceClient MasterSvc = new MasterCatalogServiceClient())
                            {
                                List<FlightPakMasterService.GetFleet> GetFleetList = new List<GetFleet>();
                                var fleetretval = MasterSvc.GetFleet();
                                if (fleetretval.ReturnFlag)
                                {
                                    if (hdnFleetID.Value != "0" && hdnFleetID.Value != null)
                                    {
                                        var Results = FleetChargeRateService.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID != null && x.FleetID == Convert.ToInt64(hdnFleetID.Value) && x.AircraftTypeID == null && x.CQCustomerID == null).ToList();
                                        if (Results != null && Results.Count > 0)
                                        {                                          
                                            if (Results[0].FleetIsTaxDailyAdj != null && Results[0].FleetIsTaxDailyAdj == true)
                                            {
                                                DailyUsage = true;
                                            }
                                            if (Results[0].FleetIsTaxLandingFee != null && Results[0].FleetIsTaxLandingFee == true)
                                            {
                                                LandingFee = true;
                                            }
                                            if (Results[0].FleetMinimumDay != null)
                                            {
                                                MinimumDailyUsageHours = Convert.ToString(Math.Round(Convert.ToDecimal(Results[0].FleetMinimumDay), 2));
                                            }
                                            else
                                            {
                                                MinimumDailyUsageHours = string.Empty;
                                            }

                                            if (Results[0].FleetStandardCrewDOM != null)
                                            {
                                                DomesticStandardCrew = Results[0].FleetStandardCrewDOM.ToString();
                                            }
                                            else
                                            {
                                                DomesticStandardCrew = string.Empty;
                                            }

                                            if (Results[0].FleetStandardCrewIntl != null)
                                            {
                                                InternationalStandardCrew = Results[0].FleetStandardCrewIntl.ToString();
                                            }
                                            else
                                            {
                                                InternationalStandardCrew = string.Empty;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    chkTaxDailyUsage.Checked = DailyUsage;
                    chkTaxLandingFee.Checked = LandingFee;
                    tbMinimumDailyUsageHours.Text = MinimumDailyUsageHours;
                    tbDomesticStandardCrew.Text = DomesticStandardCrew;
                    tbInternationalStandardCrew.Text = InternationalStandardCrew;

                }
            }
        }








        protected void lbtnCopy_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnMode.Value == "Fleet")
                        {

                        }
                        else if (hdnMode.Value == "CQCD")
                        {

                        }
                        else
                        {


                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

        }

        /// <summary>
        /// Command Event Trigger for Save or Update Fleet Charge Rate Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value.ToString().ToUpper() == "UPDATE")
                        {
                            (dgFleetNewCharterRate.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgFleetNewCharterRate.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                        Session["CQConfirmCharterRate"] = "Changed";

                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

        }
        /// <summary>
        /// Cancel Fleet Charge Rate Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedFleetNewCharterID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.FleetNewCharterRate, Convert.ToInt64(Session["SelectedFleetNewCharterID"].ToString().Trim()));
                            }
                        }
                        GridEnable(true, true, true);
                        DefaultSelection(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

            // tbCode.ReadOnly = true;

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFleetNewCharterRate;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }

        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgFleetNewCharterRate.SelectedItems.Count > 0)
                    {
                        LoadControlData();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// It Select The Default Item In The Grid
        /// </summary>
        protected void DefaultSelection(bool BindDataSwitch)
        {
            if (BindDataSwitch)
            {
                dgFleetNewCharterRate.Rebind();
            }
            if (dgFleetNewCharterRate.MasterTableView.Items.Count > 0)
            {
                //if (!IsPostBack)
                //{
                //    Session["SelectedFleetNewCharterID"] = null;
                //}
                if (Session["SelectedFleetNewCharterID"] == null)
                {
                dgFleetNewCharterRate.SelectedIndexes.Add(0);
                Session["SelectedFleetNewCharterID"] = dgFleetNewCharterRate.Items[0].GetDataKeyValue("FleetNewCharterRateID").ToString();
                }

                if (dgFleetNewCharterRate.SelectedIndexes.Count == 0)
                    dgFleetNewCharterRate.SelectedIndexes.Add(0);

                ReadOnlyForm();
                GridEnable(true, true, true);
            }
            else
            {
                ClearForm();
                EnableForm(false);
            }
        }

        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="delayTypeCode"></param>
        /// <returns></returns>
        private FlightPakMasterService.FleetNewCharterRate GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.FleetNewCharterRate FleetNewCharterRate = new FlightPakMasterService.FleetNewCharterRate();
                decimal marginalpercentage = 0;
                decimal MinimumDailyUsageHours = 0;
                decimal InternationalStandardCrew = 0;
                decimal DomesticStandardCrew = 0;
                if (tbMinimumDailyUsageHours.Text.Trim() != "")
                {
                    MinimumDailyUsageHours = Convert.ToDecimal(tbMinimumDailyUsageHours.Text.Trim());
                }
                if (tbInternationalStandardCrew.Text.Trim() != "")
                {
                    InternationalStandardCrew = Convert.ToDecimal(tbInternationalStandardCrew.Text.Trim());
                }
                if (tbDomesticStandardCrew.Text.Trim() != "")
                {
                    DomesticStandardCrew = Convert.ToDecimal(tbDomesticStandardCrew.Text.Trim());
                }
                if (tbMarginPercentage.Text.Trim() != string.Empty)
                {
                    marginalpercentage = Convert.ToDecimal(tbMarginPercentage.Text.Trim());
                }                
                if (hdnSave.Value == "Update")
                {
                    if (Session["SelectedFleetNewCharterID"] != null)
                    {
                        FleetNewCharterRate.FleetNewCharterRateID = Convert.ToInt64(Session["SelectedFleetNewCharterID"].ToString());                                               
                    }
                }
                if (Session["IsFleet"] != null)
                {
                    string Isfleet = (string)Session["IsFleet"];
                    if (Isfleet == "true")
                    {
                        FleetNewCharterRate.FleetID = Convert.ToInt64(hdnFleetID.Value);
                        FleetNewCharterRate.CQCustomerID = null;
                        FleetNewCharterRate.AircraftTypeID = null;
                        using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FleetChargeRateService.UpdateCharterNewRateTax(Convert.ToInt64(hdnFleetID.Value), Convert.ToInt64("0"), chkTaxDailyUsage.Checked, chkTaxLandingFee.Checked, string.Empty, DateTime.Now, Convert.ToDecimal(MinimumDailyUsageHours), Convert.ToDecimal(InternationalStandardCrew), Convert.ToDecimal(DomesticStandardCrew), Convert.ToInt64("0"), marginalpercentage);
                        }
                    }
                    else if (Isfleet == "CQ")
                    {
                        FleetNewCharterRate.FleetID = null;
                        FleetNewCharterRate.AircraftTypeID = null;
                        FleetNewCharterRate.CQCustomerID = Convert.ToInt64(hdnCqCustomerId.Value);

                        using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FleetChargeRateService.UpdateCharterNewRateTax(Convert.ToInt64("0"), Convert.ToInt64("0"), chkTaxDailyUsage.Checked, chkTaxLandingFee.Checked, string.Empty, DateTime.Now, Convert.ToDecimal(MinimumDailyUsageHours), Convert.ToDecimal(InternationalStandardCrew), Convert.ToDecimal(DomesticStandardCrew), Convert.ToInt64(hdnCqCustomerId.Value), marginalpercentage);
                        }
                    }
                    else
                    {
                        FleetNewCharterRate.FleetID = null;
                        FleetNewCharterRate.CQCustomerID = null;
                        FleetNewCharterRate.AircraftTypeID = Convert.ToInt64(hdnAircraftID.Value);

                        using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {

                            FleetChargeRateService.UpdateCharterNewRateTax(Convert.ToInt64("0"), Convert.ToInt64(hdnAircraftID.Value), chkTaxDailyUsage.Checked, chkTaxLandingFee.Checked, string.Empty, DateTime.Now, MinimumDailyUsageHours, InternationalStandardCrew, DomesticStandardCrew, Convert.ToInt64("0"), marginalpercentage);
                        }
                    }
                }
                FleetNewCharterRate.FleetNewCharterRateDescription = tbdescription.Text.Trim();
                FleetNewCharterRate.ChargeUnit = rblChargeUnit.SelectedItem.Text.Trim();
                FleetNewCharterRate.NegotiatedChgUnit = Convert.ToDecimal(rblChargeUnit.SelectedItem.Value.Trim());
                if (hdnOrderNum.Value != "")
                {
                    FleetNewCharterRate.OrderNum = Convert.ToInt32(hdnOrderNum.Value);
                }
                else
                {
                    FleetNewCharterRate.OrderNum = null;
                }
                if (tbDomesticStandardCrew.Text.Trim() != "")
                {
                    FleetNewCharterRate.StandardCrewDOM = Convert.ToDecimal(DomesticStandardCrew);
                }
                else
                {
                    FleetNewCharterRate.StandardCrewDOM = null;
                }

                if (tbInternationalStandardCrew.Text.Trim() != "")
                {
                    FleetNewCharterRate.StandardCrewIntl = Convert.ToDecimal(InternationalStandardCrew);
                }
                else
                {
                    FleetNewCharterRate.StandardCrewIntl = null;
                }

                if (tbInternationalSell.Text.Trim() != "")
                {
                    FleetNewCharterRate.SellIntl = Convert.ToDecimal(tbInternationalSell.Text.Trim());
                }
                else
                {
                    FleetNewCharterRate.SellIntl = null;
                }

                if (tbInternationalFixedCostBuy.Text.Trim() != "")
                {
                    FleetNewCharterRate.BuyIntl = Convert.ToDecimal(tbInternationalFixedCostBuy.Text.Trim());
                }
                else
                {
                    FleetNewCharterRate.BuyIntl = null;
                }
                if (tbDomesticSell.Text.Trim() != "")
                {
                    FleetNewCharterRate.SellDOM = Convert.ToDecimal(tbDomesticSell.Text.Trim());
                }
                else
                {
                    FleetNewCharterRate.SellDOM = null;
                }
                if (tbDomesticFixedCostBuy.Text.Trim() != "")
                {
                    FleetNewCharterRate.BuyDOM = Convert.ToDecimal(tbDomesticFixedCostBuy.Text.Trim());
                }
                else
                {
                    FleetNewCharterRate.BuyDOM = null;
                }
                if (tbMinimumDailyUsageHours.Text.Trim() != "")
                {
                    FleetNewCharterRate.MinimumDailyRequirement = Convert.ToDecimal(MinimumDailyUsageHours);
                }
                else
                {
                    FleetNewCharterRate.MinimumDailyRequirement = null;
                }
                if (tbInternationalFixedCostBuyAccountNumber.Text.Trim() != "" && hdBuyAccountID.Value != "")
                {
                    FleetNewCharterRate.BuyAircraftFlightIntlID = Convert.ToInt64(hdBuyAccountID.Value);
                }
                else
                {
                    FleetNewCharterRate.BuyAircraftFlightIntlID = null;
                }
                if (tbInternationalSellAccountNumber.Text.Trim() != "" && hdnSellAccountID.Value != "")
                {
                    FleetNewCharterRate.SellAircraftFlightIntlID = Convert.ToInt64(hdnSellAccountID.Value);
                }
                else
                {
                    FleetNewCharterRate.SellAircraftFlightIntlID = null;
                }
                if (tbDomesticFixedCostBuyAccountNumber.Text.Trim() != "" && hdnDBAccountID.Value != "")
                {
                    FleetNewCharterRate.DBAircraftFlightChgID = Convert.ToInt64(hdnDBAccountID.Value);
                }
                else
                {
                    FleetNewCharterRate.DBAircraftFlightChgID = null;
                }
                if (tbDomesticSellAccountNumber.Text.Trim() != "" && hdnDSAccountID.Value != "")
                {
                    FleetNewCharterRate.DSAircraftFlightChgID = Convert.ToInt64(hdnDSAccountID.Value);
                }
                else
                {
                    FleetNewCharterRate.DSAircraftFlightChgID = null;
                }
                FleetNewCharterRate.IsTaxDOM = chkDomesticTax.Checked;
                FleetNewCharterRate.IsTaxIntl = chkInternationalTax.Checked;
                FleetNewCharterRate.IsDiscountDOM = chkDomesticDisc.Checked;
                FleetNewCharterRate.IsDiscountIntl = chkInternationalDisc.Checked;
                FleetNewCharterRate.IsUWAData = Convert.ToBoolean(Session["ISUWAData"]); 
                FleetNewCharterRate.IsDeleted = false;                
                return FleetNewCharterRate;
            }
        }


        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, delCtl, editCtl;
                insertCtl = (LinkButton)dgFleetNewCharterRate.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                delCtl = (LinkButton)dgFleetNewCharterRate.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                editCtl = (LinkButton)dgFleetNewCharterRate.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                if (IsAuthorized(Permission.Database.AddFleetCharterRates))
                {
                    insertCtl.Visible = true;
                    if (add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteFleetCharterRates))
                {
                    delCtl.Visible = true;
                    if (delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = "";
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditFleetCharterRates))
                {
                    editCtl.Visible = true;
                    if (edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = "";
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                dgFleetNewCharterRate.SelectedIndexes.Clear();
                tbdescription.Text = "";
                tbMinimumDailyUsageHours.Text = "0.00";
                tbDomesticStandardCrew.Text = "0";
                tbInternationalStandardCrew.Text = "0";
                tbMarginPercentage.Text = "0.00";
                chkDomesticDisc.Checked = false;
                tbDomesticFixedCostBuy.Text = "0.00";
                tbInternationalFixedCostBuy.Text = "0.00";
                tbDomesticSell.Text = "0.00";
                tbInternationalSell.Text = "0.00";
                chkDomesticTax.Checked = false;
                chkInternationalTax.Checked = false;
                chkDomesticDisc.Checked = false;
                chkInternationalDisc.Checked = false;
                tbDomesticFixedCostBuyAccountNumber.Text = "";
                tbDomesticSellAccountNumber.Text = "";
                tbInternationalFixedCostBuyAccountNumber.Text = "";
                tbInternationalSellAccountNumber.Text = "";
                rblChargeUnit.SelectedValue = "1";
                chkTaxDailyUsage.Checked = DailyUsage;
                chkTaxLandingFee.Checked = LandingFee;
                Session["ISUWAData"] = false;
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                chkDomesticDisc.Enabled = enable;
                tbdescription.Enabled = enable;
                tbMinimumDailyUsageHours.Enabled = enable;
                tbDomesticStandardCrew.Enabled = enable;
                tbInternationalStandardCrew.Enabled = enable;
                tbDomesticFixedCostBuy.Enabled = enable;
                tbInternationalFixedCostBuy.Enabled = enable;
                tbDomesticSell.Enabled = enable;
                tbInternationalSell.Enabled = enable;
                chkDomesticTax.Enabled = enable;
                chkInternationalTax.Enabled = enable;
                chkDomesticDisc.Enabled = enable;
                chkInternationalDisc.Enabled = enable;
                tbDomesticFixedCostBuyAccountNumber.Enabled = enable;
                tbDomesticSellAccountNumber.Enabled = enable;
                tbInternationalFixedCostBuyAccountNumber.Enabled = enable;
                tbInternationalSellAccountNumber.Enabled = enable;
                rblChargeUnit.Enabled = enable;
                btnSaveChanges.Visible = enable;
                btnCancel.Visible = enable;
                chkTaxDailyUsage.Enabled = enable;
                chkTaxLandingFee.Enabled = enable;
                tbMarginPercentage.Enabled = enable;
            }
        }



        /// <summary>
        /// for setting format for date in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetNewCharterRate_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {

                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }


        protected void dgFleetNewCharterRate_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
        {
            //Charter Information
            //if (e.Column.UniqueName == "BuyDOM")
            //{
            //    e.Column.Visible = rbCharterRates.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "SellDOM")
            //{
            //    e.Column.Visible = rbCharterRates.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "IsTaxDOM")
            //{
            //    e.Column.Visible = rbCharterRates.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "IsDiscountDOM")
            //{
            //    e.Column.Visible = rbCharterRates.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "BuyIntl")
            //{
            //    e.Column.Visible = rbCharterRates.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "SellIntl")
            //{
            //    e.Column.Visible = rbCharterRates.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "IsTaxIntl")
            //{
            //    e.Column.Visible = rbCharterRates.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "IsDiscountIntl")
            //{
            //    e.Column.Visible = rbCharterRates.Checked ? true : false;
            //}

            ////Account Information
            //if (e.Column.UniqueName == "DBAircraftFlightChg")
            //{
            //    e.Column.Visible = rbAccountInformation.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "DSAircraftFlightChg")
            //{
            //    e.Column.Visible = rbAccountInformation.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "BuyAircraftFlightIntl")
            //{
            //    e.Column.Visible = rbAccountInformation.Checked ? true : false;
            //}
            //if (e.Column.UniqueName == "IsSellAircraftFlightIntl")
            //{
            //    e.Column.Visible = rbAccountInformation.Checked ? true : false;
            //}

        }

        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DomesticFixedCostBuyAccountNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDomesticFixedCostBuyAccountNumber.Text))
                        {
                            CheckDomesticBuyAccountExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DomesticSellAccountNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDomesticSellAccountNumber.Text))
                        {
                            CheckDomesticSellAccountExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }


        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InternationalFixedCostBuyAccountNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbInternationalFixedCostBuyAccountNumber.Text))
                        {
                            CheckInternationalFixedAccountExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }


        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InternationalSellAccountNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbInternationalSellAccountNumber.Text))
                        {
                            CheckInternationalSellAccountExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <returns></returns>
        private bool CheckDomesticBuyAccountExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDomesticFixedCostBuyAccountNumber.Text != string.Empty) && (tbDomesticFixedCostBuyAccountNumber.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbDomesticFixedCostBuyAccountNumber.Text.ToString().ToUpper().Trim())).ToList();

                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvFixedCostBuyAccountNumber.IsValid = false;
                            return true;
                        }
                        else
                        {
                            hdnDBAccountID.Value = RetValue[0].AccountID.ToString();
                            return false;
                        }
                    }
                }
                return false;
            }
        }

        private bool CheckDomesticSellAccountExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDomesticSellAccountNumber.Text != string.Empty) && (tbDomesticSellAccountNumber.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbDomesticSellAccountNumber.Text.ToString().ToUpper().Trim())).ToList();

                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvSellAccountNumber.IsValid = false;
                            return true;
                        }
                        else
                        {
                            hdnDSAccountID.Value = RetValue[0].AccountID.ToString();
                            return false;
                        }
                    }
                }
                return false;
            }
        }

        private bool CheckInternationalFixedAccountExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbInternationalFixedCostBuyAccountNumber.Text != string.Empty) && (tbInternationalFixedCostBuyAccountNumber.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbInternationalFixedCostBuyAccountNumber.Text.ToString().ToUpper().Trim())).ToList();

                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvDomesticFixedCostBuyAccountNumber.IsValid = false;
                            return true;
                        }
                        else
                        {
                            hdBuyAccountID.Value = RetValue[0].AccountID.ToString();
                            return false;
                        }
                    }
                }
                return false;
            }
        }

        private bool CheckInternationalSellAccountExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbInternationalSellAccountNumber.Text != string.Empty) && (tbInternationalSellAccountNumber.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbInternationalSellAccountNumber.Text.ToString().ToUpper().Trim())).ToList();

                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvInternationalSellAccountNumber.IsValid = false;
                            return true;
                        }
                        else
                        {
                            hdnSellAccountID.Value = RetValue[0].AccountID.ToString();
                            return false;
                        }
                    }
                }
                return false;
            }
        }

        protected void FixedCostBuyAccountNumber_TextChanged(object sender, EventArgs e)
        {

        }

        protected void CharterRates_CheckedChanged(object sender, EventArgs e)
        {
            dgFleetNewCharterRate.Rebind();
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetNewCharterRate);
                }
            }
        }
        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFleetNewCharterRate.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var RateValue = FPKMstService.GetAllFleetNewCharterRate();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, RateValue);
            List<FlightPakMasterService.GetAllFleetNewCharterRate> filteredList = GetFilteredList(RateValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FleetNewCharterRateID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFleetNewCharterRate.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFleetNewCharterRate.CurrentPageIndex = PageNumber;
            dgFleetNewCharterRate.SelectedIndexes.Add(ItemIndex);
        }

        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllFleetNewCharterRate RateValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedFleetNewCharterID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = RateValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FleetNewCharterRateID;
                Session["SelectedFleetNewCharterID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }

        protected void dgFleetNewCharterRate_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.GridFilterSessionName,
                dgFleetNewCharterRate, Page.Session);
        }

        private List<FlightPakMasterService.GetAllFleetNewCharterRate> GetFilteredList(ReturnValueOfGetAllFleetNewCharterRate RateValue)
        {
            List<FlightPakMasterService.GetAllFleetNewCharterRate> filteredList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();

            if (Session["IsFleet"] != null)
            {
                string Isfleet = (string)Session["IsFleet"];

                if (Isfleet == "true" && !string.IsNullOrEmpty(hdnFleetID.Value))
                {
                    filteredList = RateValue.EntityList.Where(x => x.FleetID != null && x.FleetID == Convert.ToInt64(hdnFleetID.Value) && x.AircraftTypeID == null && x.CQCustomerID == null).ToList();
                }
                else if (Isfleet == "CQ" && !string.IsNullOrEmpty(hdnCqCustomerId.Value))
                {
                    filteredList = RateValue.EntityList.Where(x => x.CQCustomerID != null && x.CQCustomerID == Convert.ToInt64(hdnCqCustomerId.Value) && x.FleetID == null && x.AircraftTypeID == null).ToList();
                }
                else if (!string.IsNullOrEmpty(hdnAircraftID.Value))
                {
                    filteredList = RateValue.EntityList.Where(x => x.AircraftTypeID != null && x.AircraftTypeID == Convert.ToInt64(hdnAircraftID.Value) && x.FleetID == null && x.CQCustomerID == null).ToList();
                }
            }

            return filteredList;
        }
    }
}