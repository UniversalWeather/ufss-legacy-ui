﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class CrewRosterAircraftTypePopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            #region "Comments"

            //Defect : 2609(OntimeID) In Crew Roster Add Rating , Aircraft Popup should not display Inactive Aircrafts.
            //Fixed By: Karthikeyan.

            #endregion

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            lbMessage.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAircraftType);
                }
            }

        }

        protected void AircraftType_ItemCreated(object sender, GridItemEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            Button btnSubmit = (e.Item as GridCommandItem).FindControl("btnSubmit") as Button;

                            if (Request.QueryString["AircraftTypeCD"] != null)
                            {
                                btnSubmit.Visible = true;
                                lbtnInsertButton.Visible = false;
                                dgAircraftType.ClientSettings.ClientEvents.OnRowDblClick = "returnToParent";
                            }
                            else
                            {
                                btnSubmit.Visible = false;
                                lbtnInsertButton.Visible = true;
                                dgAircraftType.ClientSettings.ClientEvents.OnRowDblClick = "RowDblClick";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAircraftType);
                }
            }

        }


        /// <summary>
        /// Method to Bind Grid Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AircraftType_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewRosterAircraftTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CrewRosterAircraftType = CrewRosterAircraftTypeService.GetAircraftList();
                            if (CrewRosterAircraftType.ReturnFlag == true)
                            {
                                dgAircraftType.DataSource = CrewRosterAircraftType.EntityList.Where(x => x.IsDeleted == false && (x.IsInActive == null || x.IsInActive == false));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAircraftType);
                }
            }

        }

        /// <summary>
        /// Method to trigger Grid Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AircraftType_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAircraftType);
                }
            }

        }

        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void AircraftType_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewRosterAircraftType);
                }
            }

        }

        /// <summary>
        /// Method to Bind into "CrewNewTypeRating" session, and 
        /// Call "CloseAndRebind" javascript function to Close and
        /// Rebind the selected record into parent page.
        /// </summary>
        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgAircraftType.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgAircraftType.SelectedItems[0];
                        if (CheckIfExists(Item.GetDataKeyValue("AircraftCD").ToString()))
                        {
                            lbMessage.Text = "Warning, Aircraft Type Code Already Exists.";
                            lbMessage.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            bool IsExist = false;
                            List<FlightPakMasterService.GetCrewRating> CrewRatingInfo = (List<FlightPakMasterService.GetCrewRating>)Session["CrewTypeRating"];


                            for (int Index = 0; Index < CrewRatingInfo.Count; Index++)
                            {
                                if (CrewRatingInfo[Index].AircraftTypeCD == Item.GetDataKeyValue("AircraftCD").ToString())
                                {

                                    CrewRatingInfo[Index].IsPilotinCommand = false;
                                    CrewRatingInfo[Index].IsSecondInCommand = false;
                                    CrewRatingInfo[Index].IsQualInType135PIC = false;
                                    CrewRatingInfo[Index].IsQualInType135SIC = false;
                                    CrewRatingInfo[Index].IsEngineer = false;
                                    CrewRatingInfo[Index].IsInstructor = false;
                                    CrewRatingInfo[Index].IsCheckAttendant = false;
                                    CrewRatingInfo[Index].IsAttendantFAR91 = false;
                                    CrewRatingInfo[Index].IsAttendantFAR135 = false;
                                    CrewRatingInfo[Index].IsCheckAirman = false;
                                    CrewRatingInfo[Index].IsInActive = false;
                                    CrewRatingInfo[Index].TotalUpdateAsOfDT = null;
                                    CrewRatingInfo[Index].TotalTimeInTypeHrs = 0;
                                    CrewRatingInfo[Index].TotalDayHrs = 0;
                                    CrewRatingInfo[Index].TotalNightHrs = 0;
                                    CrewRatingInfo[Index].TotalINSTHrs = 0;
                                    CrewRatingInfo[Index].PilotInCommandTypeHrs = 0;
                                    CrewRatingInfo[Index].TPilotinCommandDayHrs = 0;
                                    CrewRatingInfo[Index].TPilotInCommandNightHrs = 0;
                                    CrewRatingInfo[Index].TPilotInCommandINSTHrs = 0;
                                    CrewRatingInfo[Index].SecondInCommandTypeHrs = 0;
                                    CrewRatingInfo[Index].SecondInCommandDayHrs = 0;
                                    CrewRatingInfo[Index].SecondInCommandNightHrs = 0;
                                    CrewRatingInfo[Index].SecondInCommandINSTHrs = 0;
                                    CrewRatingInfo[Index].OtherSimHrs = 0;
                                    CrewRatingInfo[Index].OtherFlightEngHrs = 0;
                                    CrewRatingInfo[Index].OtherFlightInstrutorHrs = 0;
                                    CrewRatingInfo[Index].OtherFlightAttdHrs = 0;
                                    CrewRatingInfo[Index].IsDeleted = false;
                                    IsExist = true;
                                }
                            }
                            Session["CrewTypeRating"] = CrewRatingInfo;


                            if (IsExist == false)
                            {
                                List<FlightPakMasterService.GetCrewRating> GetCrewRatingList = new List<FlightPakMasterService.GetCrewRating>();
                                FlightPakMasterService.GetCrewRating GetCrewRatingDef = new FlightPakMasterService.GetCrewRating();
                                GetCrewRatingDef.AircraftTypeCD = Item.GetDataKeyValue("AircraftCD").ToString().Trim();
                                GetCrewRatingDef.AircraftTypeID = Convert.ToInt64(Item.GetDataKeyValue("AircraftID").ToString().Trim());
                                GetCrewRatingDef.CrewRatingDescription = Item.GetDataKeyValue("AircraftDescription").ToString().Trim();
                                GetCrewRatingDef.IsPilotinCommand = false;
                                GetCrewRatingDef.IsSecondInCommand = false;
                                GetCrewRatingDef.IsQualInType135PIC = false;
                                GetCrewRatingDef.IsQualInType135SIC = false;
                                GetCrewRatingDef.IsEngineer = false;
                                GetCrewRatingDef.IsInstructor = false;
                                GetCrewRatingDef.IsCheckAttendant = false;
                                GetCrewRatingDef.IsAttendantFAR91 = false;
                                GetCrewRatingDef.IsAttendantFAR135 = false;
                                GetCrewRatingDef.IsCheckAirman = false;
                                GetCrewRatingDef.IsInActive = false;
                                GetCrewRatingDef.TotalUpdateAsOfDT = null;
                                GetCrewRatingDef.TotalTimeInTypeHrs = 0;
                                GetCrewRatingDef.TotalDayHrs = 0;
                                GetCrewRatingDef.TotalNightHrs = 0;
                                GetCrewRatingDef.TotalINSTHrs = 0;
                                GetCrewRatingDef.PilotInCommandTypeHrs = 0;
                                GetCrewRatingDef.TPilotinCommandDayHrs = 0;
                                GetCrewRatingDef.TPilotInCommandNightHrs = 0;
                                GetCrewRatingDef.TPilotInCommandINSTHrs = 0;
                                GetCrewRatingDef.SecondInCommandTypeHrs = 0;
                                GetCrewRatingDef.SecondInCommandDayHrs = 0;
                                GetCrewRatingDef.SecondInCommandNightHrs = 0;
                                GetCrewRatingDef.SecondInCommandINSTHrs = 0;
                                GetCrewRatingDef.OtherSimHrs = 0;
                                GetCrewRatingDef.OtherFlightEngHrs = 0;
                                GetCrewRatingDef.OtherFlightInstrutorHrs = 0;
                                GetCrewRatingDef.OtherFlightAttdHrs = 0;
                                GetCrewRatingDef.IsDeleted = false;
                                GetCrewRatingList.Add(GetCrewRatingDef);
                                Session["CrewNewTypeRating"] = GetCrewRatingList;
                            }

                            InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }


        /// <summary>
        /// Function to Check if Crew Roster Aircraft Type Code alerady exists or not, by passing Crew Code
        /// </summary>
        /// <param name="TypeCode">Pass Aircraft Type Code</param>
        /// <returns>Returns True if Exists else Fase</returns>
        private bool CheckIfExists(string TypeCode)
        {
            bool ReturnValue = false;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TypeCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewTypeRating"] != null)
                    {
                        List<GetCrewRating> RatingList = (List<GetCrewRating>)Session["CrewTypeRating"];
                        var CrewRatingValue = (from crew in RatingList
                                               where crew.AircraftTypeCD.ToUpper().Trim() == TypeCode.ToUpper().Trim() &&
                                            crew.IsDeleted == false
                                               select crew);

                        if (CrewRatingValue.Count() > 0)
                        { ReturnValue = true; }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }

        }

        protected void dgAircraftType_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgAircraftType, Page.Session);
        }
    }
}