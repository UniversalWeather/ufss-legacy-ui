﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Data;
using System.Text;
using System.Collections;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetGroupCatalog : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private bool IsEmptyCheck = true;
        bool flag = true;
        public List<string> FleetCodes = new List<string>();
        List<string> tb = new List<string>();
        List<string> tb1 = new List<string>();
        ArrayList TailNum = new ArrayList();
        ArrayList k = new ArrayList();
        string kp;
        ArrayList selected = new ArrayList();
        //ArrayList TailNum = new ArrayList();
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFleetGroup, dgFleetGroup, this.Page.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                        //store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFleetGroup.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewFleetGroupCatalog);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgFleetGroup.Rebind();
                                LoadSelectedList();
                                ReadOnlyForm();
                                EnableForm(false);
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        protected void dgAircraftType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFleetGroup.ClientSettings.Scrolling.ScrollTop = "0";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }

        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {

            if (BindDataSwitch)
            {
                dgFleetGroup.Rebind();
            }
            if (dgFleetGroup.MasterTableView.Items.Count > 0)
            {
                //if (!IsPostBack)
                //{
                //    hdnFleetGroupID.Value = "0";
                //}
                if (string.IsNullOrEmpty(hdnFleetGroupID.Value) || hdnFleetGroupID.Value.Trim() == "0")
                {
                    dgFleetGroup.SelectedIndexes.Add(0);
                    GridDataItem item = (GridDataItem)dgFleetGroup.SelectedItems[0];
                    hdnFleetGroupID.Value = item["FleetGroupID"].Text;
                }

                if (dgFleetGroup.SelectedIndexes.Count == 0)
                    dgFleetGroup.SelectedIndexes.Add(0);

                ReadOnlyForm();
                LoadSelectedList();
                GridEnable(true, true, true);
            }
            else
            {
                dgFleetGroup.DataSource = string.Empty;
                ClearForm();
                EnableForm(false);
            }

        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    //target.Focus();
                    RadAjaxManager1.FocusControl(target);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + target + "');", true);
                    IsEmptyCheck = false;
                }

            }
        }


        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selected, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgFleetGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgFleetGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }    
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    
                    //This if block is implemented because LastUpdTS is not updating when a new Fleet Group is added
                    //It is updating when a Fleet Group is modified
                    if (!_selectLastModified)
                        hdnSave.Value = "";

                    
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selected, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((e.Initiator.ID.IndexOf("btnSaveChanges") > -1))
                        {
                            e.Updated = dgFleetGroup;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        protected void DisplayInsertForm()
        {
            

                //hdnSave.Value = "Save";
                ClearForm();
                if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportICAOCd.Trim() != string.Empty)
                {
                    tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd.ToString().Trim();
                    if (UserPrincipal != null && UserPrincipal.Identity._homeBaseId != 0)
                    {
                        hdnHomeBase.Value = Convert.ToString(UserPrincipal.Identity._homeBaseId);
                    }
                }
                else
                {
                    tbHomeBase.Text = string.Empty;

                }
                EnableForm(true);
            if(hdnSave.Value=="Save")
              ShowHideControls(true);   
            else
               ShowHideControls(false); 
        }

        protected void dgFleetGroup_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selected, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        /// <summary>
        /// Bind Fleet Type Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>_inserbind
        /// 
        protected void dgFleetGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selected, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetFleetGroup();
                            List<FlightPakMasterService.GetAllFleetGroup> lstFleetGroup = new List<FlightPakMasterService.GetAllFleetGroup>();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                               lstFleetGroup= ObjRetVal.EntityList;
                            }
                            dgFleetGroup.DataSource = lstFleetGroup;
                            Session["FleetGroup"] =  lstFleetGroup;
                            if (chkSearchActiveOnly.Checked == true)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        protected void dgFleetGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selected, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;

                               
                                pnlExternalForm.Visible = true;
                                hdnSave.Value = "Update";
                                hdnRedirect.Value = "";
                                DisplayEditForm();
                                GridEnable(false, true, false);
                                SelectItem();
                                //tbDescription.Focus();
                                RadAjaxManager1.FocusControl(tbDescription.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                                tbCode.Enabled = false;
                                DisableLinks();
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                hdnSave.Value = "Save";
                                DisplayInsertForm();
                                BindAvailableList();
                                GridEnable(true, false, false);
                                //lstavailable.Items.Clear();
                                //lstselected.Items.Clear();
                                //tbCode.Focus();
                                RadAjaxManager1.FocusControl(tbCode.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                                DisableLinks();
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        private void BindAvailableList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.GetLsavailableFleet> tb = new List<FlightPakMasterService.GetLsavailableFleet>();
                    tb = AviailableList(0);
                    List<object> tm = new List<object>();

                    RadListBoxItem lstItem;
                    lstavailable.Items.Clear();
                    foreach (FlightPakMasterService.GetLsavailableFleet fm in tb)
                    {
                        lstItem = new RadListBoxItem(fm.TailNum + "-" + "(" + fm.AircraftCD + fm.AircraftDescription + ")", fm.TailNum);
                        lstavailable.Items.Add(lstItem);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        //Method for updating the items
        protected void dgFleetGroup_UpdateCommand(object source, GridCommandEventArgs e)
        {
            e.Canceled = true;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ArrayList sample = new ArrayList();
                        Page.Validate();
                        if (!IsValid)
                        {
                            btnSaveChanges.Visible = true;
                            btnCancel.Visible = true;
                            flag = false;
                            return;
                        }
                        if (IsValid)
                        {
                            if (hdnFleetGroupID.Value.ToString().Trim() != "" && Convert.ToInt64(hdnFleetGroupID.Value) > 0)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient ObjFleettypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = ObjFleettypeService.UpdateFleetType(GetItems(string.Empty));
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        //pnlExternalForm.Visible = false;
                                        insertdata(Convert.ToInt64(hdnFleetGroupID.Value));

                                        foreach (ListItem li in lstselected.Items)
                                        {
                                            string t;
                                            t = li.Value.ToString();
                                            ArrayList temp = new ArrayList(t.Split(new Char[] { '-' }));
                                            sample.Add(temp[0]);
                                        }
                                        
                                        DefaultSelection(false);
                                        SelectItem();
                                        ShowSuccessMessage();
                                        EnableLinks();
                                        ShowHideControls(true);
                                        _selectLastModified = true;
                                    }
                                    else
                                    {                                        
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FleetGroup);                                        
                                    }
                                }
                               
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        //method which gets the data rom the ui
        private FleetGroup GetItems(string FleetGroupCode)
        {
            

                FlightPakMasterService.FleetGroup objFleetType = new FlightPakMasterService.FleetGroup();
                objFleetType.FleetGroupID = Convert.ToInt64(hdnFleetGroupID.Value);
                objFleetType.FleetGroupCD = tbCode.Text.ToUpper();
                objFleetType.FleetGroupDescription = tbDescription.Text.ToUpper();
                if (tbHomeBase.Text.Trim() != "")
                {
                    if (Convert.ToInt64(hdnHomeBase.Value) > 0)
                        objFleetType.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                }
                objFleetType.IsDeleted = false;
                objFleetType.IsInActive = chkFleetInactive.Checked; 
                return objFleetType;
            
        }
        /// <summary>
        /// Function to Check if Fleet Group Code Alerady Exists
        /// </summary>
        /// <returns></returns>
        private Boolean checkAllReadyExist()
        {
            bool returnVal = false;
            using (FlightPakMasterService.MasterCatalogServiceClient objFleetGroupsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objFleetGroupsvc.GetFleetGroup().EntityList.Where(x => x.FleetGroupCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                if (objRetVal.Count() > 0 && objRetVal != null)
                {
                    foreach (FlightPakMasterService.GetAllFleetGroup cm in objRetVal)
                    {
                        cvCode.IsValid = false;
                        //tbCode.Focus();
                        RadAjaxManager1.FocusControl(tbCode.ClientID);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                        return true;
                    }
                }
            }
            return returnVal;
        }

        /// <summary>
        /// Insert Command for Fleet Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFleetGroup_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        ArrayList sample = new ArrayList();
                        Page.Validate();
                        if (checkAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            //tbCode.Focus();
                            RadAjaxManager1.FocusControl(tbCode.ClientID);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                        }
                        if (!IsValid)
                        {
                            btnSaveChanges.Visible = true;
                            btnCancel.Visible = true;
                            flag = false;
                            return;
                        }
                        else if (IsValid)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FleetGroup objFleetGroup = new FlightPakMasterService.FleetGroup();
                                objFleetGroup.FleetGroupCD = tbCode.Text;
                                objFleetGroup.FleetGroupDescription = tbDescription.Text;
                                if (tbHomeBase.Text.Trim() != "")
                                {
                                    if (Convert.ToInt64(hdnHomeBase.Value) > 0)
                                    {
                                        objFleetGroup.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                                    }
                                }
                                objFleetGroup.IsDeleted = false;
                                objFleetGroup.IsInActive = chkFleetInactive.Checked;
                                var objRetVal = objService.AddFleetType(objFleetGroup);
                                if (objRetVal.ReturnFlag == true)
                                {
                                    pnlExternalForm.Visible = false;
                                    GridEnable(true, true, true);

                                    Int64 NewId = 0;
                                    var InsertedFleetGrpID = objService.GetFleetGroupID(tbCode.Text.Trim());
                                    if (InsertedFleetGrpID != null)
                                    {
                                        foreach (FlightPakMasterService.GetFleetGroupID FltId in InsertedFleetGrpID.EntityList)
                                        {
                                            NewId = FltId.FleetGroupID;
                                            hdnFleetGroupID.Value = Convert.ToString(NewId);
                                        }
                                    }
                                    insertdata(Convert.ToInt64(hdnFleetGroupID.Value));
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    ShowHideControls(true);
                                    _selectLastModified = true;
                                }
                                else
                                {                                    
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FleetGroup);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        protected void dgFleetGroup_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (hdnFleetGroupID.Value.ToString().Trim() != "" && Convert.ToInt64(hdnFleetGroupID.Value.ToString()) > 0)
                        {

                            using (FlightPakMasterService.MasterCatalogServiceClient objFleetTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FleetGroup objFleetType = new FlightPakMasterService.FleetGroup();

                                objFleetType.FleetGroupID = Convert.ToInt64(hdnFleetGroupID.Value.ToString());

                                objFleetType.IsDeleted = true;
                                objFleetTypeService.DeleteFleetType(objFleetType);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(false);
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FleetGroup, Convert.ToInt64(hdnFleetGroupID.Value.ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Insert Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnInsert_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selected, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFleetGroup.SelectedIndexes.Clear();
                        DisplayInsertForm();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }


        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selected, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //hdnFleetGroupID.Value = "0";
                        DefaultSelection(false);
                        ShowHideControls(true);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        /// <summary>
        /// Load Selected List Items in the Listbox
        /// </summary>

        protected void LoadSelectedList()
        {
            

                Int64 FleetGroupID;

                FleetGroupID = Convert.ToInt64(hdnFleetGroupID.Value.ToString());

                List<FlightPakMasterService.GetLsavailableFleet> tb = new List<FlightPakMasterService.GetLsavailableFleet>();
                tb = AviailableList(FleetGroupID);
                List<object> tm = new List<object>();

                RadListBoxItem lstItem;                
                lstavailable.Items.Clear();
                foreach (FlightPakMasterService.GetLsavailableFleet fm in tb)
                {
                    tm.Add(fm.TailNum + "-" + "(" + fm.AircraftCD + fm.AircraftDescription + ")");
                    // added for having text field and tailnum as value field
                    //lstItem = new ListItem(fm.TailNum + "-" + "(" + fm.AircraftCD + fm.AircraftDescription + ")", fm.TailNum);
                    lstItem = new RadListBoxItem(fm.TailNum + "-" + "(" + fm.AircraftCD + fm.AircraftDescription + ")", fm.TailNum);
                    lstavailable.Items.Add(lstItem);
                }

                List<FlightPakMasterService.GetLsselectedFleet> tb1 = new List<FlightPakMasterService.GetLsselectedFleet>();
                tb1 = SelectedList(FleetGroupID);
                List<object> tm1 = new List<object>();

                lstselected.Items.Clear();
                foreach (FlightPakMasterService.GetLsselectedFleet fm in tb1)
                {
                    tm1.Add(fm.TailNum + "-" + "(" + fm.AircraftCD + fm.AircraftDescription + ")");
                    // added for having text field and tailnum as value field
                    lstItem = new RadListBoxItem(fm.TailNum + "-" + "(" + fm.AircraftCD + fm.AircraftDescription + ")", fm.TailNum);
                    lstselected.Items.Add(lstItem);
                }

                //Commented for formatting display of text field and value field
                //lstavailable.DataSource = tm;
                //lstavailable.DataBind();
                //lstselected.DataSource = tm1;
                //lstselected.DataBind();
                ReadOnlyForm();
                GridEnable(true, true, true);
          
        }

        protected void dgFleetGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selected, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = (GridDataItem)dgFleetGroup.SelectedItems[0];
                                hdnFleetGroupID.Value = item["FleetGroupID"].Text;
                                Label lblUser;
                                lblUser = (Label)dgFleetGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lblUser.Text = string.Empty;
                                }
                                if (item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                                }
                                //item.Selected = true;
                                ReadOnlyForm();
                                LoadSelectedList();
                                GridEnable(true, true, true);
                                ShowHideControls(true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                    }
                }
            }
        }

        //Method which deletes the item
        protected void deleteitem()
        {
            FlightPakMasterService.MasterCatalogServiceClient objFleetTypeService = new FlightPakMasterService.MasterCatalogServiceClient();
            FlightPakMasterService.FleetGroupOrder objFleetGroupOrderType = new FlightPakMasterService.FleetGroupOrder();
            objFleetGroupOrderType.FleetGroupID = Convert.ToInt64(hdnFleetGroupID.Value);
            objFleetTypeService.DeleteFleetGroupOrderType(objFleetGroupOrderType);
        }
        //method getting the data from the list available
        public List<FlightPakMasterService.GetLsavailableFleet> AviailableList(Int64 FleetGroupID)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objFleetTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                FlightPakMasterService.GetLsavailableFleet objFleetmaster = new FlightPakMasterService.GetLsavailableFleet();
                var ObjRetVal = objFleetTypeService.GetLsavailableList(FleetGroupID);
                return ObjRetVal.EntityList.OrderBy( x=> x.TailNum).ToList();
                //List<FlightPakMasterService.GetLsavailableFleet> ret = new List<FlightPakMasterService.GetLsavailableFleet>();
                //return ret;
            }
        }

        //method getting the data from the list selected
        public List<FlightPakMasterService.GetLsselectedFleet> SelectedList(Int64 FleetGroupID)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objFleetTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                FlightPakMasterService.GetLsselectedFleet objFleetmaster = new FlightPakMasterService.GetLsselectedFleet();
                var ObjRetVal = objFleetTypeService.GetLsselectedList(FleetGroupID);
                //return ObjRetVal.EntityList.OrderBy(x => x.TailNum).ToList();
                return ObjRetVal.EntityList.ToList();                
                //List<FlightPakMasterService.GetLsavailableFleet> ret = new List<FlightPakMasterService.GetLsavailableFleet>();
                //return ret;
            }
        }


        /// <summary>
        /// To Clear the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                tbCode.Text = string.Empty;
                tbDescription.Text = string.Empty;
                tbHomeBase.Text = string.Empty;
                hdnHomeBase.Value = string.Empty;
                lstavailable.Items.Clear();
                lstselected.Items.Clear();
                chkFleetInactive.Checked = false;
            }
        }


        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        ///  /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LoadControlData();
               
                //hdnSave.Value = "Update";
                btnCancel.Visible = true;
                btnSaveChanges.Visible = true;
                // dgFleetGroup.Rebind(); 
                EnableForm(true);
                ShowHideControls(true);
            }
        }

        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                tbCode.Enabled = enable;
                tbDescription.Enabled = enable;
                tbHomeBase.Enabled = enable;
                lstavailable.Enabled = enable;
                lstselected.Enabled = enable;

                btnHomeBase.Enabled = enable;
                btnCancel.Visible = enable;
                btnSaveChanges.Visible = enable;

                //btnNext.Enabled = enable;
                //btnPrevious.Enabled = enable;
                //btnFirst.Enabled = enable;
                //btnLast.Enabled = enable;
            }
        }

        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton lnkInsertCtl = (LinkButton)dgFleetGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                LinkButton lnkDelCtl = (LinkButton)dgFleetGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                LinkButton lnkEditCtl = (LinkButton)dgFleetGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                if (Add)
                    lnkInsertCtl.Enabled = true;
                else
                    lnkInsertCtl.Enabled = false;
                if (Delete)
                {
                    lnkDelCtl.Enabled = true;
                    lnkDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                }
                else
                {
                    lnkDelCtl.Enabled = false;
                    lnkDelCtl.OnClientClick = "";
                }
                if (Edit)
                {
                    lnkEditCtl.Enabled = true;
                    lnkEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                }
                else
                {
                    lnkEditCtl.Enabled = false;
                    lnkEditCtl.OnClientClick = "";
                }

            }

        }


        /// <summary>
        /// To make the form readonly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                pnlExternalForm.Visible = true;

                //DisplayEditForm();
                LoadControlData();
               
                btnCancel.Visible = false;
                btnSaveChanges.Visible = false;
                EnableForm(false);
            }
        }

        protected void insertdata(Int64 FleetGroupID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetGroupID))
            {
                
                    hdnFleetGroupID.Value = FleetGroupID.ToString();
                    deleteitem();

                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        FlightPakMasterService.FleetGroupOrder objFleetGroupOrder = new FlightPakMasterService.FleetGroupOrder();
                        Int64 FleetID = 0;

                        string selValue = string.Empty;
                        string TailNumber = string.Empty;
                        string TailNumberValue = string.Empty;  // for validating tailnum
                        string FleetTailNum = string.Empty;
                        int Indx = 0;

                        FlightPakMasterService.GetLsavailableFleet objFleetTypeService = new FlightPakMasterService.GetLsavailableFleet();
                        var ObjRetVal = objService.GetLsavailableList(0).EntityList;

                        for (int i = 0; i < lstselected.Items.Count; i++)
                        {
                            TailNumber = lstselected.Items[i].Text;
                            TailNumberValue = lstselected.Items[i].Value;  // for validating tailnum+
                            if (TailNumber.IndexOf("-") != -1)
                            {
                                Indx = TailNumber.IndexOf("-");
                                //FleetTailNum = TailNumber.Substring(0, Indx);
                                FleetTailNum = TailNumberValue;
                                FleetID = 0;

                                foreach (FlightPakMasterService.GetLsavailableFleet fm in ObjRetVal)
                                {
                                    if (FleetTailNum.ToUpper() == fm.TailNum.ToUpper())
                                        FleetID = fm.FleetID;
                                }

                                if (FleetID > 0)
                                {
                                    objFleetGroupOrder.FleetID = FleetID;
                                    objFleetGroupOrder.FleetGroupID = FleetGroupID;
                                    objFleetGroupOrder.OrderNum = i;
                                    objFleetGroupOrder.IsDeleted = false;                                    
                                    objService.AddFleetGroupOrderType(objFleetGroupOrder);
                                }
                            }
                        }
                    }
                    dgFleetGroup.Rebind();
                    GridEnable(true, true, true);
                if(hdnSave.Value!="Save")
                    DisplayInsertForm();
            }
        }


        protected void btnNext_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lstavailable.SelectedItem != null)
                        {
                            string listSelected = string.Empty;
                            ArrayList selectedList = new ArrayList();
                            for (int i = lstavailable.Items.Count - 1; i >= 0; i--)
                            {
                                if (lstavailable.Items[i].Selected)
                                {
                                    listSelected = lstavailable.Items[i].ToString();
                                    ArrayList tempSelectedList = new ArrayList(listSelected.Split(new Char[] { '-' }));
                                    selectedList.Add(tempSelectedList[0].ToString());
                                    lstselected.Items.Add(lstavailable.Items[i]);
                                    //lstavailable.Items.RemoveAt(i);
                                    lstavailable.Items.Remove(lstavailable.Items[i]);
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        protected void btnPrevious_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lstselected.SelectedItem != null)
                        {
                            string listSelected = string.Empty;
                            ArrayList selectedList = new ArrayList();
                            for (int i = lstselected.Items.Count - 1; i >= 0; i--)
                            {
                                if (lstselected.Items[i].Selected)
                                {
                                    listSelected = lstselected.Items[i].ToString();
                                    ArrayList tempSelectedList = new ArrayList(listSelected.Split(new Char[] { '-' }));
                                    selectedList.Add(tempSelectedList[0].ToString().Trim());
                                    lstavailable.Items.Add(lstselected.Items[i]);
                                    //lstselected.Items.RemoveAt(i);
                                    lstselected.Items.Remove(lstselected.Items[i]);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }

        }

        protected void btnLast_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ArrayList selectedList = new ArrayList();
                        for (int i = lstavailable.Items.Count - 1; i >= 0; i--)
                        {
                            string listSelected;
                            listSelected = lstavailable.Items[i].ToString();
                            ArrayList tempSelectedList = new ArrayList(listSelected.Split(new Char[] { '-' }));
                            selectedList.Add(tempSelectedList[1].ToString().Trim());
                            lstselected.Items.Add(lstavailable.Items[i]);
                            //lstavailable.Items.RemoveAt(i);
                            lstavailable.Items.Remove(lstavailable.Items[i]);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        protected void btnFirst_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ArrayList selectedList = new ArrayList();
                        for (int i = lstselected.Items.Count - 1; i >= 0; i--)
                        {
                            string listSelected;
                            listSelected = lstselected.Items[i].ToString();
                            ArrayList tempSelectedList = new ArrayList(listSelected.Split(new Char[] { '-' }));
                            selectedList.Add(tempSelectedList[0].ToString().Trim());
                            lstavailable.Items.Add(lstselected.Items[i]);
                            //lstselected.Items.RemoveAt(i);
                            lstselected.Items.Remove(lstselected.Items[i]);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }

        }

        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objService.GetFleetGroup().EntityList.Where(x => x.FleetGroupCD.Trim().ToUpper() == (tbCode.Text.ToString().ToUpper().Trim()));
                                if (objRetVal.Count() > 0 && objRetVal != null)
                                {
                                    cvCode.IsValid = false;
                                    RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();                                    
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                                }
                                else
                                {
                                    RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                                }
                            }                           
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }


        /// <summary>
        /// To check Valid Home Base
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbHomeBase.Text != null && tbHomeBase.Text.Trim() != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = ObjService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.Trim().ToUpper() == (tbHomeBase.Text.Trim().ToUpper()));
                                if (objRetVal.Count() <= 0)
                                {
                                    cvHomeBase.IsValid = false;
                                    //tbHomeBase.Focus();
                                    RadAjaxManager1.FocusControl(tbHomeBase.ClientID);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbHomeBase.ClientID + "');", true);
                                }
                                else
                                {
                                    hdnHomeBase.Value = objRetVal.ToList()[0].HomebaseID.ToString();
                                    tbHomeBase.Text = objRetVal.ToList()[0].HomebaseCD;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetGroup);
                }
            }
        }

        private void ShowHideControls(bool Visibility)
        {
            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Visibility))
            //{
                lstavailable.Visible = Visibility;
                lstselected.Visible = Visibility;
                //btnNext.Visible = Visibility;
                //btnPrevious.Visible = Visibility;
                //btnLast.Visible = Visibility;
                //btnFirst.Visible = Visibility;
                lbOrderDetails.Visible = Visibility;
                lbFleetAvailable.Visible = Visibility;
                lbFleetSelected.Visible = Visibility;
            //}
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllFleetGroup> lstFleetGroup = new List<FlightPakMasterService.GetAllFleetGroup>();
                if (Session["FleetGroup"] != null)
                {
                    lstFleetGroup = (List<FlightPakMasterService.GetAllFleetGroup>)Session["FleetGroup"];
                }
                if (lstFleetGroup.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstFleetGroup = lstFleetGroup.Where(x => x.IsInactive == false).ToList<GetAllFleetGroup>(); }
                    else
                    {
                        lstFleetGroup = lstFleetGroup.ToList<GetAllFleetGroup>();
                    }

                    dgFleetGroup.DataSource = lstFleetGroup;
                    if (IsDataBind)
                    {
                        dgFleetGroup.DataBind();
                    }
                }
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }
        
        private void LoadControlData()
        {
            foreach (GridDataItem Item in dgFleetGroup.MasterTableView.Items)
            {
                if (Item.GetDataKeyValue("FleetGroupID").ToString() == hdnFleetGroupID.Value.Trim())
                {
                    if (Item.GetDataKeyValue("FleetGroupCD").ToString().Trim() != "")
                        tbCode.Text = Item.GetDataKeyValue("FleetGroupCD").ToString().Trim();
                    else
                        tbCode.Text = string.Empty;

                    if (Item.GetDataKeyValue("HomebaseCD") != null)
                    {
                        tbHomeBase.Text = Item.GetDataKeyValue("HomebaseCD").ToString().Trim();
                        hdnHomeBase.Value = Item.GetDataKeyValue("HomebaseID").ToString().Trim();
                    }
                    else
                    {
                        tbHomeBase.Text = string.Empty;
                        hdnHomeBase.Value = "0";
                    }
                    if (Item.GetDataKeyValue("FleetGroupDescription") != null)
                        tbDescription.Text = Item.GetDataKeyValue("FleetGroupDescription").ToString().Trim();
                    else
                        tbDescription.Text = string.Empty;

                    Label lbLastUpdatedUser;
                    lbLastUpdatedUser = (Label)dgFleetGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    if (Item.GetDataKeyValue("IsInActive") != null && Item.GetDataKeyValue("IsInActive").ToString().ToUpper().Trim() == "TRUE")
                    {
                        chkFleetInactive.Checked = true;
                    }
                    else
                    {
                        chkFleetInactive.Checked = false;
                    }
                    lbColumnName1.Text = "Fleet Group Code";
                    lbColumnName2.Text = "Description";
                    lbColumnValue1.Text = Item["FleetGroupCD"].Text;
                    lbColumnValue2.Text = Item["FleetGroupDescription"].Text;
                    break;
                }
            }
        }
        
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!string.IsNullOrEmpty(hdnFleetGroupID.Value) && hdnFleetGroupID.Value.Trim() != "0")
                    {
                        foreach (GridDataItem Item in dgFleetGroup.MasterTableView.Items)
                        {
                            if (Item["FleetGroupID"].Text.Trim() == hdnFleetGroupID.Value.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFleetGroup.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var FleetGroupValue = FPKMstService.GetFleetGroup();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, FleetGroupValue);
            List<FlightPakMasterService.GetAllFleetGroup> filteredList = GetFilteredList(FleetGroupValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FleetGroupID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFleetGroup.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFleetGroup.CurrentPageIndex = PageNumber;
            dgFleetGroup.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllFleetGroup FleetGroupValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    hdnFleetGroupID.Value = Session["SearchItemPrimaryKeyValue"].ToString();
                }
            }
            else
            {
                //This if else block and hdnSave.Value - "" are implemented because LastUpdTS is not updating
                //when a new Fleet Group is added. It is updating when a Fleet Group is modified
                if (hdnSave.Value == "Save")
                {
                    PrimaryKeyValue = FleetGroupValue.EntityList.OrderByDescending(x => x.FleetGroupID).FirstOrDefault().FleetGroupID;
                }
                else
                {
                    PrimaryKeyValue = FleetGroupValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FleetGroupID;
                }

                hdnFleetGroupID.Value = PrimaryKeyValue.ToString();
                hdnSave.Value = "";
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllFleetGroup> GetFilteredList(ReturnValueOfGetAllFleetGroup FleetGroupValue)
        {
            List<FlightPakMasterService.GetAllFleetGroup> filteredList = new List<FlightPakMasterService.GetAllFleetGroup>();

            if (FleetGroupValue.ReturnFlag)
            {
                filteredList = FleetGroupValue.EntityList;
            }

            if (filteredList.Count > 0)
            {
                if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInactive == false).ToList<GetAllFleetGroup>(); }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFleetGroup.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFleetGroup.Rebind();
                    SelectItem();
                }
            }
        }

        protected void dgFleetGroup_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFleetGroup, Page.Session);
        }
    }
}