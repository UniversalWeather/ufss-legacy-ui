﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fleetcopy.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Fleet.Fleetcopy" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Copy Charter Rate</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();


            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function confirmSubmitCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnSubmitAircraftYes.ClientID%>').click();
                }
            }


            function confirmSubmitCallBackFnFleet(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnSubmitFleetYes.ClientID%>').click();
                }
            }


            function confirmSubmitCallBackFnCQ(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnSubmitCQYes.ClientID%>').click();
                }
            }

            function CopySuccessful(arg) {
                var oWnd = GetRadWindow();
                oWnd.close();
            }


            function ConfirmWindowClose() {
                var oWnd = GetRadWindow();
                oWnd.close();
            }




            function AircraftCopy() {
                document.getElementById('<%=btnSubmitAircraft.ClientID%>').click();
            }

            function FleetCopy() {
                document.getElementById('<%=btnSubmitFleet.ClientID%>').click();
            }

            function CQCopy() {
                document.getElementById('<%=btnSubmitCQ.ClientID%>').click();
            }
  
         
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <%--<telerik:AjaxSetting AjaxControlID="dgAircraftType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftType" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgFleetPopup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgFleetPopup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgCharterQuoteCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCharterQuoteCustomer" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <ConfirmTemplate >
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>                    
                </div>
            </div>
        </ConfirmTemplate>
            <Windows>
                <telerik:RadWindow ID="RadWindow1" runat="server" AutoSize="true" Modal="true" Behaviors="None">
                </telerik:RadWindow>
            </Windows>
            <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        </telerik:RadWindowManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table id="AircraftTable" runat="server" cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <telerik:RadGrid ID="dgAircraftType" runat="server" AllowMultiRowSelection="false"
                        OnPageIndexChanged="dgAircraftType_PageIndexChanged" AllowSorting="true" OnNeedDataSource="dgAircraftType_BindData"
                        AutoGenerateColumns="false" Height="330px" AllowPaging="false" Width="500px"
                        PagerStyle-AlwaysVisible="true">
                        <MasterTableView DataKeyNames="AircraftID,AircraftCD,AircraftDescription,BindingDesc"
                            AllowSorting="true" CommandItemDisplay="None" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Type Code" DataField="AircraftCD" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="BindingDesc" HeaderText="Aircraft Name" ShowFilterIcon="false"
                                    Display="false" AutoPostBackOnFilter="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Code" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AircraftDescription" HeaderText="Aircraft Name"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AircraftID" HeaderText="Code" Display="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <button id="btnSubmit" onclick="AircraftCopy(); return false;" class="button">
                                        Ok</button>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="AircraftCopy" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <table id="FleetTable" runat="server" cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <telerik:RadGrid ID="dgFleetPopup" runat="server" AllowMultiRowSelection="false"
                        AllowSorting="true" OnNeedDataSource="dgFleetPopup_BindData" AutoGenerateColumns="false"
                        Height="330px" AllowPaging="false" Width="960px">
                        <MasterTableView DataKeyNames="FleetID,AircraftCD,TailNum,AirCraft_AircraftCD,IsInActive,LastUpdUID,LastUpdTS,IcaoID,PowerSettings1TrueAirSpeed,PowerSettings2TrueAirSpeed,PowerSettings3TrueAirSpeed"
                            CommandItemDisplay="None" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="FleetID" HeaderText="FleetID" DataField="FleetID"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Fleet Code" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" HeaderStyle-Width="80px" FilterControlWidth="60px" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" HeaderStyle-Width="80px" FilterControlWidth="60px" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AirCraft_AircraftCD" HeaderText="Type Code" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TypeDescription" HeaderText="Type Description"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                    HeaderStyle-Width="200px" FilterControlWidth="180px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PowerSettings1TrueAirSpeed" HeaderText="Constant Mach"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="80px" CurrentFilterFunction="Contains"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PowerSettings2TrueAirSpeed" HeaderText="Long Range Cruise"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="80px" CurrentFilterFunction="Contains"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PowerSettings3TrueAirSpeed" HeaderText="High Speed Cruise"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="80px" CurrentFilterFunction="Contains"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="MaximumPassenger" HeaderText="Maximum PAX" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Vendor Code" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IcaoID" HeaderText="Home Base" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomebaseID" HeaderText="Home Base ID" Display="false"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn UniqueName="BoolField" DataField="IsInActive" HeaderText="Active"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="60px" AllowFiltering="false"
                                    CurrentFilterFunction="EqualTo">
                                </telerik:GridCheckBoxColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 20px; text-align: right;">
                                    <button id="btnSubmit" onclick="FleetCopy(); return false;" class="button">
                                        Ok</button>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="FleetCopy" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <table id="CQTable" runat="server" cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <telerik:RadGrid ID="dgCharterQuoteCustomer" runat="server" AllowMultiRowSelection="true"
                        AllowSorting="true" AutoGenerateColumns="false" Height="341px" PageSize="10"
                        OnNeedDataSource="dgCharterQuoteCustomer_BindData" AllowPaging="true" Width="900px"
                        AllowFilteringByColumn="true">
                        <MasterTableView DataKeyNames="CQCustomerID" CommandItemDisplay="Bottom">
                            <Columns>
                                <telerik:GridBoundColumn DataField="CQCustomerCD" HeaderText="Code" CurrentFilterFunction="Contains"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CQCustomerName" HeaderText="Customer Name" CurrentFilterFunction="Contains"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Name" HeaderText="Contact Name" CurrentFilterFunction="Contains"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Active" CurrentFilterFunction="EqualTo"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" CurrentFilterFunction="Contains"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CQCustomerContactID" HeaderText="CQCustomerContactID"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CQCustomerContactCD" HeaderText="CQCustomerContactCD"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="PhoneNum" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FaxNum" HeaderText="FaxNum" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CQCustomerID" HeaderText="CQCustomerID" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Credit" HeaderText="Credit" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" Display="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <button id="btnSubmit" onclick="CQCopy(); return false;" class="button">
                                        Ok</button>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="CQCopy" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdnMode" runat="server" />
        <asp:HiddenField ID="hdnOk" runat="server" />
        <asp:HiddenField ID="hdnOldAircraftID" runat="server" />
        <asp:HiddenField ID="hdnOldAircraftCD" runat="server" />
        <asp:HiddenField ID="hdnOldFleetID" runat="server" />
        <asp:HiddenField ID="hdnOldCQID" runat="server" />
        <asp:HiddenField ID="hdnTempID" runat="server" />
        <asp:Button ID="btnSubmitAircraftYes" runat="server" Text="Button" OnClick="btnSubmitAircraftYes_Click"
            Style="display: none;" />
        <asp:Button ID="btnSubmitFleetYes" runat="server" Text="Button" OnClick="btnSubmitFleetYes_Click"
            Style="display: none;" />
        <asp:Button ID="btnSubmitCQYes" runat="server" Text="Button" OnClick="btnSubmitCQYes_Click"
            Style="display: none;" />
        <asp:Button ID="btnSubmitAircraft" runat="server" Text="Button" OnClick="dgAircraftTypeOk_click"
            Style="display: none;" />
        <asp:Button ID="btnSubmitFleet" runat="server" Text="Button" OnClick="dgFleetPopupOk_Click"
            Style="display: none;" />
        <asp:Button ID="btnSubmitCQ" runat="server" Text="Button" OnClick="dgCharterQuoteCustomerOk_Click"
            Style="display: none;" />
    </div>
    </form>
</body>
</html>
