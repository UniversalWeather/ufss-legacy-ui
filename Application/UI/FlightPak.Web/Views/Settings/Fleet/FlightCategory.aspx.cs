﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.Security;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class FlightCategory : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private bool FlightCategoryPageNavigated = false;
        private string strFlightCategoryCD = "";
        private string strFlightCategoryID = "";
        private List<string> lstflightCategoryCodes = new List<string>();
        private bool _selectLastModified = false;
        #region constants
        private const string ConstIsInactive = "IsInActive";
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewFlightCategoryReport);
                        if (!IsPostBack)
                        {
                            //    // Grid Control could be ajaxified when the page is initially loaded.
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFlightCategories, dgFlightCategories, RadAjaxLoadingPanel1);
                            // Store the clientID of the grid to reference it later on the client
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFlightCategories.ClientID));
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewFlightCategory);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgFlightCategories.Rebind();
                                ReadOnlyForm();
                                EnableForm(false);
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                DefaultSelection(true);
                            }
                        }
                        if (IsPopUp)
                        {
                            dgFlightCategories.AllowPaging = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// To select the selected Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFlightCategoryID"] != null)
                    {
                        string ID = Session["SelectedFlightCategoryID"].ToString();
                        foreach (GridDataItem item in dgFlightCategories.MasterTableView.Items)
                        {
                            if (item["FlightCategoryID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                        //if (dgFlightCategories.SelectedItems.Count == 0)
                        //{
                        //    dgFlightCategories.SelectedIndexes.Add(0);
                        //}
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Datagrid Item Created for Flight Purpose Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!IsPopUp)
                    {
                        if (BindDataSwitch)
                        {
                            dgFlightCategories.Rebind();
                        }
                        if (dgFlightCategories.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedFlightCategoryID"] = null;
                            //}
                            if (Session["SelectedFlightCategoryID"] == null)
                            {
                                dgFlightCategories.SelectedIndexes.Add(0);

                                if (!IsPopUp)
                                {
                                    Session["SelectedFlightCategoryID"] = dgFlightCategories.Items[0].GetDataKeyValue("FlightCategoryID").ToString();
                                }

                                if (dgFlightCategories.SelectedIndexes.Count == 0)
                                    dgFlightCategories.SelectedIndexes.Add(0);

                                ReadOnlyForm();
                            }
                            else
                            {
                                LoadControlData();
                                EnableForm(false);
                            }
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                        }   
                   
                        GridEnable(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Prerender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (FlightCategoryPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFlightCategories, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Created for Flight Category Type Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Code on Tab out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            CheckAllReadyCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// To check unique client Code on Tab out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbClientCode.Text != null)
                        {
                            CheckAllReadyClientCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        private bool CheckAllReadyClientCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                bool ReturnVal = false;
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategorysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        if ((tbClientCode.Text != string.Empty) && (tbClientCode.Text != null))
                        {
                            var objFlightCategoryVal = objFlightCategorysvc.GetClientCodeList().EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbClientCode.Text.ToString().ToUpper().Trim())).ToList();
                            if (objFlightCategoryVal.Count() == 0 || objFlightCategoryVal == null)
                            {
                                cvClientCode.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                                //tbClientCode.Focus();
                                ReturnVal = true;
                            }
                            else
                            {
                                //if (string.IsNullOrEmpty(hdnclientCD.Value))
                                //{

                                hdnclientCD.Value = ((FlightPakMasterService.Client)objFlightCategoryVal[0]).ClientID.ToString().Trim();
                                tbClientCode.Text = ((FlightPakMasterService.Client)objFlightCategoryVal[0]).ClientCD.Trim();
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbForeColor);
                                //tbForeColor.Focus();
                                //}
                                ReturnVal = false;
                                //hdnclientCD.Value = string.Empty;
                            }
                        }
                        else
                        {
                            hdnclientCD.Value = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbForeColor);
                            //tbForeColor.Focus();
                        }
                    }
                    ReturnVal = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnVal;
            }
        }
        ///// <summary>
        ///// To get clientcode based on clientid
        ///// </summary>
        //private void GetClientCode()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        //Handle methods throguh exception manager with return flag
        //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();

        //        exManager.Process(() =>
        //        {
        //            FlightPakMasterService.MasterCatalogServiceClient objFlightCategorysvc = new FlightPakMasterService.MasterCatalogServiceClient();

        //            var objFlightCategoryVal = objFlightCategorysvc.GetClientCodeList().EntityList.Where(x => x.ClientID.ToString().ToUpper().Trim().Equals(hdnclientCD.Value.ToString().ToUpper().Trim())).ToList();
        //            if (objFlightCategoryVal.Count() > 0)
        //            {
        //                tbClientCode.Text = ((FlightPakMasterService.Client)objFlightCategoryVal[0]).ClientCD.ToString().Trim();
        //            }

        //        }, FlightPak.Common.Constants.Policy.UILayer);

        //    }
        //    }

        /// <summary>
        /// Bind Flight Category Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objFlightCategoryVal = objFlightCategoryService.GetAllFlightCategoryList();
                            if (objFlightCategoryVal.ReturnFlag == true)
                            {
                                dgFlightCategories.DataSource = objFlightCategoryVal.EntityList;
                            }
                            Session["flightCategoryCodes"] = objFlightCategoryVal.EntityList.ToList();
                            if (chkSearchActiveOnly != null)
                            {
                                if (!IsPopUp)
                                    SearchAndFilter(false);
                                //SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                    //The exception will be handled, logged and replaced by our custom exception. 
                }
            }
        }
        /// <summary>
        /// To filter Inactive records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllFlightCategories> lstFlightCategory = new List<FlightPakMasterService.GetAllFlightCategories>();
                if (Session["flightCategoryCodes"] != null)
                {
                    lstFlightCategory = (List<FlightPakMasterService.GetAllFlightCategories>)Session["flightCategoryCodes"];
                }
                if (lstFlightCategory.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == false) { lstFlightCategory = lstFlightCategory.Where(x => x.IsInActive == false).ToList<GetAllFlightCategories>(); }
                    else
                        if (chkSearchActiveOnly.Checked == true) { lstFlightCategory = lstFlightCategory.ToList(); }
                    dgFlightCategories.DataSource = lstFlightCategory;
                    if (IsDataBind)
                    {
                        dgFlightCategories.DataBind();
                        dgFlightCategories.DataBind();
                        dgFlightCategories.SelectedIndexes.Add(0);
                        GridDataItem item = dgFlightCategories.SelectedItems[0] as GridDataItem;
                        Session["SelectedFlightCategoryID"] = item.GetDataKeyValue("FlightCategoryID").ToString().Trim();
                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                }
                return false;
            }
        }
        #endregion
        /// <summary>
        /// Datagrid Item Command forFlight Category Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedFlightCategoryID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.FlightCategory, Convert.ToInt64(Session["SelectedFlightCategoryID"].ToString().Trim()));
                                        Session["IsFlightCategoryEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FlightCategory);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FlightCategory);
                                            SelectItem();
                                            return;
                                        }
                                        SelectItem();
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDescription);
                                        //tbDescription.Focus();
                                        SelectItem();
                                        e.Item.OwnerTableView.Rebind();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFlightCategories.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                EnableForm(true);
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCode);
                                // tbCode.Focus();                              
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            
                            //case "RowClick":
                            //    GridDataItem item = dgFlightCategories.SelectedItems[0] as GridDataItem;
                            //    Session["SelectedFlightCategoryID"] = item["FlightCategoryID"].Text;
                            //    var key = item.ItemIndex;
                            //    dgFlightCategories.Rebind();
                            //    dgFlightCategories.SelectedIndexes.Add(key);
                            //    GridEnable(true, true, true);
                            //    ReadOnlyForm();
                            //    break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// Update Command for updating the values of Flight Category in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedFlightCategoryID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objFlightCategory = objFlightCategoryService.UpdateFlightCategory(GetItems());
                                if (objFlightCategory.ReturnFlag == true)
                                {
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    ///////Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.FlightCategory, Convert.ToInt64(Session["SelectedFlightCategoryID"].ToString().Trim()));
                                        GridEnable(true, true, true);
                                    }

                                    ShowSuccessMessage();
                                    SelectItem();
                                    ReadOnlyForm();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objFlightCategory.ErrorMessage, ModuleNameConstants.Database.FlightCategory);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            //Session["SelectedFlightCategoryID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// Save and Update Flight Category Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgFlightCategories.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgFlightCategories.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// Cancel Flight Category Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            //pnlExternalForm.Visible = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedFlightCategoryID"] != null)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.FlightCategory, Convert.ToInt64(Session["SelectedFlightCategoryID"].ToString().Trim()));
                                }
                            }
                        }
                        //Session.Remove("SelectedFlightCategoryID");
                        DefaultSelection(true);
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// Flight Category Insert Command for inserting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_InsertCommand(object source, GridCommandEventArgs e)
        {
            e.Canceled = true;
            bool IsValidate = true;
            if (CheckAllReadyCodeExist())
            {
                cvCode.IsValid = false;
                RadAjaxManager.GetCurrent(Page).FocusControl(tbCode);
                // tbCode.Focus();
                IsValidate = false;
            }
            if (CheckAllReadyClientCodeExist())
            {
                cvClientCode.IsValid = false;
                RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                //tbClientCode.Focus();
                IsValidate = false;
            }
            if (IsValidate)
            {
                using (MasterCatalogServiceClient objFlightCategoryService = new MasterCatalogServiceClient())
                {
                    var objFlightCategory = objFlightCategoryService.AddFlightCategory(GetItems());
                    if (objFlightCategory.ReturnFlag == true)
                    {
                        DefaultSelection(true);

                        ShowSuccessMessage();
                        _selectLastModified = true;
                        
                        //dgFlightCategories.Rebind();
                        //if (dgFlightCategories.MasterTableView.Items.Count > 0)
                        //{
                        //    dgFlightCategories.SelectedIndexes.Add(0);
                        //    Session["SelectedFlightCategoryID"] = dgFlightCategories.Items[0].GetDataKeyValue("FlightCategoryID").ToString();
                        //    ReadOnlyForm();
                        //    hdnGridEnable.Value = "Save";
                        //}
                    }
                    else
                    {
                        //For Data Anotation
                        ProcessErrorMessage(objFlightCategory.ErrorMessage, ModuleNameConstants.Database.FlightCategory);
                    }
                }
            }
            else
            {
                tbForeColor.Text = hdnForeColor.Value;
                tbBackColor.Text = hdnBackColor.Value;
            }

            if (IsPopUp)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
            }

        }
        /// <summary>
        ///  Flight Category Delete Command for deleting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedFlightCategoryID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient FlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FlightCatagory FlightCategory = new FlightPakMasterService.FlightCatagory();
                                string Code = Session["SelectedFlightCategoryID"].ToString();
                                strFlightCategoryCD = "";
                                strFlightCategoryID = "";
                                foreach (GridDataItem Item in dgFlightCategories.MasterTableView.Items)
                                {
                                    if (Item["FlightCategoryID"].Text.Trim() == Code.Trim())
                                    {
                                        strFlightCategoryCD = Item["FlightCatagoryCD"].Text.Trim();
                                        strFlightCategoryID = Item["FlightCategoryID"].Text.Trim();
                                        break;
                                    }
                                }
                                FlightCategory.FlightCatagoryCD = strFlightCategoryCD;
                                FlightCategory.FlightCategoryID = Convert.ToInt64(strFlightCategoryID);
                                FlightCategory.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.FlightCategory, Convert.ToInt64(strFlightCategoryID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FlightCategory);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FlightCategory);
                                        return;
                                    }
                                }
                                FlightCategoryService.DeleteFlightCategory(FlightCategory);

                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(true);
                                hdnSave.Value = "Delete";

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FlightCategory, Convert.ToInt64(strFlightCategoryID));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {

                                GridDataItem item = dgFlightCategories.SelectedItems[0] as GridDataItem;
                                Session["SelectedFlightCategoryID"] = item["FlightCategoryID"].Text;
                                var key = item.ItemIndex;
                                dgFlightCategories.Rebind();
                                dgFlightCategories.SelectedIndexes.Add(key);
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                    }
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFlightCategories;
                            if (hdnSave.Value == "Delete" || hdnGridEnable.Value == "Save")
                            {
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private FlightCatagory GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.FlightCatagory FlightCatagory = new FlightPakMasterService.FlightCatagory();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    FlightCatagory.FlightCatagoryCD = tbCode.Text;
                    FlightCatagory.FlightCatagoryDescription = tbDescription.Text;
                    FlightCatagory.ForeGrndCustomColor = hdnForeColor.Value;
                    FlightCatagory.BackgroundCustomColor = hdnBackColor.Value;
                    FlightCatagory.IsInActive = chkInactive.Checked;
                    FlightCatagory.IsDeleted = false;
                    FlightCatagory.IsDeadorFerryHead = chkDeadferryhead.Checked;
                    if (tbClientCode.Text != string.Empty)
                    {
                        FlightCatagory.ClientID = Convert.ToInt64(hdnclientCD.Value);
                    }
                    else
                    {
                        FlightCatagory.ClientID = null;
                    }
                    if (hdnSave.Value == "Update")
                    {
                        foreach (GridDataItem Item in dgFlightCategories.MasterTableView.Items)
                        {
                            if (Item["FlightCategoryID"].Text.Trim() == Session["SelectedFlightCategoryID"].ToString().Trim())
                            {
                                FlightCatagory.FlightCategoryID = Convert.ToInt64(Item["FlightCategoryID"].Text.Trim());
                                break;
                            }
                        }
                    }
                    return FlightCatagory;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return FlightCatagory;
            }
        }
        /// <summary>
        /// Binding the colors for grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                            dataItem.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ForeGrndCustomColor"), CultureInfo.CurrentCulture));
                            dataItem.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackgroundCustomColor"), CultureInfo.CurrentCulture));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    dgFlightCategories.Rebind();
                    tbCode.ReadOnly = false;
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        ///  /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReadOnlyForm();
                    tbCode.ReadOnly = true;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    //dgFlightCategories.Rebind();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
            {
                if (dgFlightCategories.SelectedItems.Count == 0)
                {
                    dgFlightCategories.SelectedIndexes.Add(0);
                }
                if (dgFlightCategories.SelectedItems.Count > 0)
                {
                    GridDataItem Item = dgFlightCategories.SelectedItems[0] as GridDataItem;
                    Label lbLastUpdatedUser = (Label)dgFlightCategories.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    LoadControlData();
                    EnableForm(false);
                }
            }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Load Selected Item Data into the Form Fields
        /// </summary>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFlightCategoryID"] != null)
                    {
                        strFlightCategoryID = "";
                        System.Drawing.Color foreColor;
                        System.Drawing.Color backColor;
                        foreach (GridDataItem Item in dgFlightCategories.MasterTableView.Items)
                        {
                            if (Item["FlightCategoryID"].Text.Trim() == Session["SelectedFlightCategoryID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("FlightCatagoryCD") != null)
                                {
                                    tbCode.Text = Item["FlightCatagoryCD"].Text.Trim();
                                }
                                else
                                {
                                    tbCode.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("FlightCategoryID") != null)
                                {
                                    strFlightCategoryID = Item["FlightCategoryID"].Text.Trim();
                                }
                                if (Item.GetDataKeyValue("FlightCatagoryDescription") != null)
                                {
                                    tbDescription.Text = Item["FlightCatagoryDescription"].Text.Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ClientID") != null)
                                {
                                    hdnclientCD.Value = Item.GetDataKeyValue("ClientID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("ClientCD") != null)
                                {
                                    tbClientCode.Text = Item.GetDataKeyValue("ClientCD").ToString().Trim();
                                }
                                else
                                {
                                    tbClientCode.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ForeGrndCustomColor") != null)
                                {
                                    tbForeColor.Text = Item.GetDataKeyValue("ForeGrndCustomColor").ToString().Trim();
                                    foreColor = System.Drawing.Color.FromName(Convert.ToString(tbForeColor.Text, CultureInfo.CurrentCulture));
                                    RadColorPickerForeColor.SelectedColor = foreColor;
                                    hdnForeColor.Value = Item.GetDataKeyValue("ForeGrndCustomColor").ToString();
                                }
                                else
                                {
                                    tbForeColor.Text = string.Empty;
                                    RadColorPickerForeColor.SelectedColor = System.Drawing.Color.White;
                                }
                                if (Item.GetDataKeyValue("BackgroundCustomColor") != null)
                                {
                                    tbBackColor.Text = Item.GetDataKeyValue("BackgroundCustomColor").ToString().Trim();
                                    backColor = System.Drawing.Color.FromName(Convert.ToString(tbBackColor.Text, CultureInfo.CurrentCulture));
                                    RadColorPickerBackColor.SelectedColor = backColor;
                                    hdnBackColor.Value = Item.GetDataKeyValue("BackgroundCustomColor").ToString();
                                }
                                else
                                {
                                    tbBackColor.Text = string.Empty;
                                    RadColorPickerBackColor.SelectedColor = System.Drawing.Color.White;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                if (Item.GetDataKeyValue("IsDeadorFerryHead") != null)
                                {
                                    chkDeadferryhead.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsDeadorFerryHead").ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkDeadferryhead.Checked = false;
                                }

                                lbColumnName1.Text = "Flight Category Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["FlightCatagoryCD"].Text;
                                lbColumnValue2.Text = Item["FlightCatagoryDescription"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbCode.Text = string.Empty;
                        tbDescription.Text = string.Empty;
                        tbClientCode.Text = string.Empty;
                        tbForeColor.Text = string.Empty;
                        tbBackColor.Text = string.Empty;
                        chkInactive.Checked = false;
                        chkDeadferryhead.Checked = false;
                        RadColorPickerBackColor.SelectedColor = System.Drawing.Color.White;
                        RadColorPickerForeColor.SelectedColor = System.Drawing.Color.White;
                        hdnBackColor.Value = string.Empty;
                        hdnForeColor.Value = string.Empty;
                        hdnclientCD.Value = string.Empty;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFlightCategories_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFlightCategories.ClientSettings.Scrolling.ScrollTop = "0";
                        FlightCategoryPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Set Logged-in User Name
                        FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                        if (identity != null && identity.Identity._clientId != null && identity.Identity._clientId != 0)
                        {
                            tbClientCode.Enabled = false;
                            //need to change
                            tbClientCode.Text = identity.Identity._clientCd.ToString().Trim();
                            hdnclientCD.Value = identity.Identity._clientId.ToString().Trim();
                            btnHomeBase.Enabled = false;
                        }
                        else
                        {
                            tbClientCode.Enabled = enable;
                            btnHomeBase.Enabled = enable;
                        }
                        if ((hdnSave.Value == "Update") && (enable == true))
                        {
                            tbCode.Enabled = false;
                        }
                        else
                        {
                            tbCode.Enabled = enable;
                        }
                        chkInactive.Enabled = enable;
                        chkDeadferryhead.Enabled = enable;
                        tbDescription.Enabled = enable;
                        tbForeColor.Enabled = enable;
                        tbBackColor.Enabled = enable;
                        RadColorPickerBackColor.Enabled = enable;
                        RadColorPickerForeColor.Enabled = enable;
                        btnSaveChanges.Visible = enable;
                        btnCancel.Visible = enable;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        /// <summary>
        /// To check whether the Enter code is unique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private bool CheckAllReadyCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["flightCategoryCodes"] != null)
                    {
                        List<GetAllFlightCategories> FlightCategoryGroupList = new List<GetAllFlightCategories>();
                        FlightCategoryGroupList = ((List<GetAllFlightCategories>)Session["flightCategoryCodes"]).Where(x => x.FlightCatagoryCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<GetAllFlightCategories>();
                        if (FlightCategoryGroupList.Count != 0)
                        {
                            cvCode.IsValid = false;                            
                            RadAjaxManager1.FocusControl(tbCode.ClientID);//tbCode.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            ReturnValue = true;
                        }
                        else
                        {
                            RadAjaxManager1.FocusControl(tbDescription.ClientID);//tbDescription.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                        }
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }
        }

        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightCategory);
                }
            }
        }
        #endregion
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtninsertCtl, lbtneditCtl, lbtndelCtl;
                    lbtninsertCtl = (LinkButton)dgFlightCategories.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    lbtndelCtl = (LinkButton)dgFlightCategories.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    lbtneditCtl = (LinkButton)dgFlightCategories.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddFlightCategory))
                    {
                        lbtninsertCtl.Visible = true;
                        if (add)
                        {
                            lbtninsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtninsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtninsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteFlightCategory))
                    {
                        lbtndelCtl.Visible = true;
                        if (delete)
                        {
                            lbtndelCtl.Enabled = true;
                            lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtndelCtl.Enabled = false;
                            lbtndelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtndelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditFlightCategory))
                    {
                        lbtneditCtl.Visible = true;
                        if (edit)
                        {
                            lbtneditCtl.Enabled = true;
                            lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtneditCtl.Enabled = false;
                            lbtneditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtneditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = true;
                                table2.Visible = false;
                                table3.Visible = false;
                                dgFlightCategories.Visible = false;
                                if (IsAdd)
                                {
                                    (dgFlightCategories.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgFlightCategories.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFlightCategories.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFlightCategories.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                if(Request.QueryString["SelectedFlightCategoryID"]!=null)
                {
                    Session["SelectedFlightCategoryID"] = Request.QueryString["SelectedFlightCategoryID"].Trim();
                }
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFlightCategories.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var FlightCategoryValue = FPKMstService.GetAllFlightCategoryList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, FlightCategoryValue);
            List<FlightPakMasterService.GetAllFlightCategories> filteredList = GetFilteredList(FlightCategoryValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FlightCategoryID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFlightCategories.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFlightCategories.CurrentPageIndex = PageNumber;
            dgFlightCategories.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllFlightCategories FlightCategoryValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedFlightCategoryID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = FlightCategoryValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FlightCategoryID;
                Session["SelectedFlightCategoryID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllFlightCategories> GetFilteredList(ReturnValueOfGetAllFlightCategories FlightCategoryValue)
        {
            List<FlightPakMasterService.GetAllFlightCategories> filteredList = new List<FlightPakMasterService.GetAllFlightCategories>();

            if (FlightCategoryValue.ReturnFlag)
            {
                filteredList = FlightCategoryValue.EntityList;
            }

            if (!IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (!chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetAllFlightCategories>(); }
                    else if (chkSearchActiveOnly.Checked) { filteredList = filteredList.ToList(); }
                }
            }

            return filteredList;
        }
    }
}