﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class DelayTypeCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private bool DelayTypePageNavigated = false;
        private List<string> lstdelayCodes = new List<string>();
        private string strDelayTypeID = "";
        private string strDelayTypeCD = "";
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgDelayType, dgDelayType, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgDelayType.ClientID));
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewDelayTypeCatalogReport);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewDelayTypeCatalog);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgDelayType.Rebind();
                                ReadOnlyForm();
                                EnableForm(false);
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                        if (IsPopUp)
                        {
                            dgDelayType.AllowPaging = false;
                            chkSearchActiveOnly.Visible = false;
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// To select the first record as default in read only mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgDelayType.Rebind();
                    }
                    if (dgDelayType.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedDelayTypeID"] = null;
                        //}
                        if (Session["SelectedDelayTypeID"] == null)
                        {
                            dgDelayType.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                            {
                                Session["SelectedDelayTypeID"] = dgDelayType.Items[0].GetDataKeyValue("DelayTypeID").ToString();
                            }
                        }

                        if (dgDelayType.SelectedIndexes.Count == 0)
                            dgDelayType.SelectedIndexes.Add(0);

                            ReadOnlyForm();
                            GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the selected item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDelayTypeID"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                        //string code = items.GetDataKeyValue("DelayTypeCD").ToString();
                        string ID = Session["SelectedDelayTypeID"].ToString();
                        foreach (GridDataItem item in dgDelayType.MasterTableView.Items)
                        {
                            if (item["DelayTypeID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDelayType_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton deleteButton = (e.Item as GridCommandItem).FindControl("lnkDelete") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(deleteButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Bind Delay Type Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDelayType_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        using (FlightPakMasterService.MasterCatalogServiceClient objDelayTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objDelVal = objDelayTypeService.GetDelayTypeList();
                            if (objDelVal.ReturnFlag == true)
                            {
                                dgDelayType.DataSource = objDelVal.EntityList;
                            }
                            Session["delayCodes"] = objDelVal.EntityList.ToList();
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// Item Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDelayType_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                hdnRedirect.Value = "";
                                if (Session["SelectedDelayTypeID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.DelayType, Convert.ToInt64(Session["SelectedDelayTypeID"].ToString().Trim()));
                                        Session["IsDelayEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.DelayType);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.DelayType);
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        SelectItem();
                                        DisableLinks();
                                        tbCode.Enabled = false;
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgDelayType.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                tbCode.Focus();
                                break;
                            case "UpdateEdited":
                                dgDelayType_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// Update Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDelayType_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedDelayTypeID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDelayTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDelayTypeService.UpdateDelayType(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    ///////Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.DelayType, Convert.ToInt64(Session["SelectedDelayTypeID"].ToString().Trim()));
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        DefaultSelection(true);

                                    }
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.DelayType);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            // Session["SelectedDelayTypeID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radDelayTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        private bool CheckAllReadyExist()
        {
           
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnFlag = false;
                //Handle methods throguh exception manager with return flag
                //exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //exManager.Process(() =>
                //{

                    //    List<DelayType> DelayCodeList = new List<DelayType>();
                    //   // DelayCodeList = ((List<DelayType>)Session["delayCodes"]).Where(x => x.DelayTypeCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<DelayType>();
                    //    using (FlightPakMasterService.MasterCatalogServiceClient objDelayTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                    //    {
                    //        var objDelVal = objDelayTypeService.GetDelayTypeList();
                    //        if (objDelVal.ReturnFlag == true)
                    //        {
                    //            DelayCodeList = objDelVal.EntityList.Where(x => x.DelayTypeCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<DelayType>();
                    //        }
                    //        if (DelayCodeList.Count != 0)
                    //        {
                    //            cvDelayCode.IsValid = false;
                    //            tbCode.Focus();
                    //            ReturnFlag = true;
                    //        }
                    //    }
                    //    return ReturnFlag;

                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ReturnValue = Service.GetDelayTypeList().EntityList.Where(x => x.DelayTypeCD.Trim().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                        if (ReturnValue.Count() > 0 && ReturnValue != null)
                        {
                            cvDelayCode.IsValid = false;
                            tbCode.Focus();
                            ReturnFlag = true;
                        }
                    }
                //}, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnFlag;
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDelayType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgDelayType.ClientSettings.Scrolling.ScrollTop = "0";
                        DelayTypePageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// Update Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDelayType_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvDelayCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objDelayTypeService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objDelayTypeService.AddDelayType(GetItems());
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    dgDelayType.Rebind();
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.DelayType);
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radDelayTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDelayType_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedDelayTypeID"] != null)
                        {
                            FlightPakMasterService.MasterCatalogServiceClient DelayTypeService = new FlightPakMasterService.MasterCatalogServiceClient();
                            FlightPakMasterService.DelayType DelayType = new FlightPakMasterService.DelayType();
                            string Code = Session["SelectedDelayTypeID"].ToString();
                            strDelayTypeCD = "";
                            strDelayTypeID = "";
                            foreach (GridDataItem Item in dgDelayType.MasterTableView.Items)
                            {
                                if (Item["DelayTypeID"].Text.Trim() == Code.Trim())
                                {
                                    strDelayTypeCD = Item["DelayTypeCD"].Text.Trim();
                                    strDelayTypeID = Item["DelayTypeID"].Text.Trim();
                                    break;
                                }
                            }
                            DelayType.DelayTypeCD = strDelayTypeCD;
                            DelayType.DelayTypeID = Convert.ToInt64(strDelayTypeID);
                            DelayType.IsSiflExcluded = false;
                            DelayType.IsDeleted = true;
                            //Lock will happen from UI
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.DelayType, Convert.ToInt64(strDelayTypeID));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                    if (IsPopUp)
                                        ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.DelayType);
                                    else
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.DelayType);
                                    return;
                                }
                                DelayTypeService.DeleteDelayType(DelayType);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.DelayType, Convert.ToInt64(strDelayTypeID));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDelayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgDelayType.SelectedItems[0] as GridDataItem;
                                Session["SelectedDelayTypeID"] = item["DelayTypeID"].Text;
                                if (btnSaveChanges.Visible == false)
                                {
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);
                                }
                            }

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                    }
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Delay Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            // CheckAllReadyExist();

                            if (CheckAllReadyExist())
                            {
                                cvDelayCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();                                
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();                                
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Delay Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgDelayType.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgDelayType.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// Cancel Delay Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedDelayTypeID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.DelayType, Convert.ToInt64(Session["SelectedDelayTypeID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        EnableLinks();
                        //Session.Remove("SelectedDelayTypeID");
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radDelayTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgDelayType;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private DelayType GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.DelayType DelayServiceType = null;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    DelayServiceType = new FlightPakMasterService.DelayType();
                    DelayServiceType.DelayTypeCD = tbCode.Text;
                    DelayServiceType.DelayTypeDescription = tbDescription.Text;
                    //need to get data
                    DelayServiceType.IsSiflExcluded = false;
                    if (hdnSave.Value == "Update")
                    {
                        //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                        if (Session["SelectedDelayTypeID"] != null)
                        {
                            DelayServiceType.DelayTypeID = Convert.ToInt64(Session["SelectedDelayTypeID"].ToString().Trim());
                        }
                    }
                    else
                    {
                        DelayServiceType.DelayTypeID = 0;
                    }
                    DelayServiceType.IsInActive = chkInactive.Checked;
                    DelayServiceType.IsDeleted = false;
                    return DelayServiceType;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return DelayServiceType;
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDelayTypeID"] != null)
                    {

                        strDelayTypeID = "";

                        foreach (GridDataItem Item in dgDelayType.MasterTableView.Items)
                        {
                            if (Item["DelayTypeID"].Text.Trim() == Session["SelectedDelayTypeID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("DelayTypeCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("DelayTypeCD").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("DelayTypeID") != null)
                                {
                                    strDelayTypeID = Item.GetDataKeyValue("DelayTypeID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("DelayTypeDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("DelayTypeDescription").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                break;
                            }
                        }
                        EnableForm(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtninsertCtl, lbtndelCtl, lbtneditCtl;
                    lbtninsertCtl = (LinkButton)dgDelayType.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    lbtndelCtl = (LinkButton)dgDelayType.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    lbtneditCtl = (LinkButton)dgDelayType.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddDelayTypeCatalog))
                    {
                        lbtninsertCtl.Visible = true;
                        if (add)
                        {
                            lbtninsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtninsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtninsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteDelayTypeCatalog))
                    {
                        lbtndelCtl.Visible = true;
                        if (delete)
                        {
                            lbtndelCtl.Enabled = true;
                            lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtndelCtl.Enabled = false;
                            lbtndelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtndelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditDelayTypeCatalog))
                    {
                        lbtneditCtl.Visible = true;
                        if (edit)
                        {
                            lbtneditCtl.Enabled = true;
                            lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtneditCtl.Enabled = false;
                            lbtneditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtneditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDelayType_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (DelayTypePageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgDelayType, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// To display the value in read only format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    EnableForm(false);

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear  the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbDescription.Enabled = enable;
                    btnSaveChanges.Visible = enable;
                    btnCancel.Visible = enable;
                    chkInactive.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                tableDelayType.Visible = false;
                                dgDelayType.Visible = false;
                                if (IsAdd)
                                {
                                    (dgDelayType.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgDelayType.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgDelayType.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgDelayType.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();


                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDelayTypeID"] != null)
                    {
                        //TimeDisplay();
                        foreach (GridDataItem Item in dgDelayType.MasterTableView.Items)
                        {
                            if (Item["DelayTypeID"].Text.Trim() == Session["SelectedDelayTypeID"].ToString().Trim())
                            {
                                //GridDataItem Item = dgDelayType.SelectedItems[0] as GridDataItem;
                                if (Item.GetDataKeyValue("DelayTypeCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("DelayTypeCD").ToString();
                                }
                                if (Item.GetDataKeyValue("DelayTypeDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("DelayTypeDescription").ToString();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }

                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgDelayType.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Delay Type Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["DelayTypeCD"].Text;
                                lbColumnValue2.Text = Item["DelayTypeDescription"].Text;

                                Item.Selected = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        #endregion

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.DelayType> lstDelayType = new List<FlightPakMasterService.DelayType>();
                if (Session["delayCodes"] != null)
                {
                    lstDelayType = (List<FlightPakMasterService.DelayType>)Session["delayCodes"];
                }
                if (lstDelayType.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstDelayType = lstDelayType.Where(x => x.IsInActive == false).ToList<DelayType>(); }
                    dgDelayType.DataSource = lstDelayType;
                    if (IsDataBind)
                    {
                        dgDelayType.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDelayType_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;

                //Check the formatting condition
                if (dataBoundItem["DelayTypeDescription"].Text.Count() > 75)
                {
                    string strDesc = dataBoundItem["DelayTypeDescription"].Text;
                    dataBoundItem["DelayTypeDescription"].Text = strDesc.Substring(0, 75) + "...";
                    dataBoundItem["DelayTypeDescription"].ToolTip = strDesc;
                }
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgDelayType.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var DelayTypeValue = FPKMstService.GetDelayTypeList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, DelayTypeValue);
            List<FlightPakMasterService.DelayType> filteredList = GetFilteredList(DelayTypeValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.DelayTypeID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgDelayType.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgDelayType.CurrentPageIndex = PageNumber;
            dgDelayType.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfDelayType DelayTypeValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedDelayTypeID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = DelayTypeValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().DelayTypeID;
                Session["SelectedDelayTypeID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.DelayType> GetFilteredList(ReturnValueOfDelayType DelayTypeValue)
        {
            List<FlightPakMasterService.DelayType> filteredList = new List<FlightPakMasterService.DelayType>();

            if (DelayTypeValue.ReturnFlag)
            {
                filteredList = DelayTypeValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<DelayType>(); }
                }
            }

            return filteredList;
        }
    }
}