﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FleetGroupCatalog.aspx.cs"
    MasterPageFile="~/Framework/Masters/Settings.master" Inherits="FlightPak.Web.Views.Settings.Fleet.FleetGroupCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="Server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/ecmascript"></script>
    <script type="text/javascript">
        function CheckTxtBox(control) {


            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);

                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);

                } return false;
            }
        }
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="Server">
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fleet Group</span> <span class="tab-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=FleetGroupHelp"
                        class="help-icon" target="_blank" title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFleetGroup">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgFleetGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgFleetGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFleetGroup">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgFleetGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="cvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbHomeBase">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbHomeBase" />
                    <telerik:AjaxUpdatedControl ControlID="cvHomeBase" />
                    <telerik:AjaxUpdatedControl ControlID="hdnHomeBase" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openWin() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=tbHomeBase.ClientID%>").value, "radCompanyMasterPopup");
            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.HomeBase;
                    document.getElementById("<%=hdnHomeBase.ClientID%>").value = arg.HomebaseID;
                    document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                }
                else {
                    document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                    document.getElementById("<%=hdnHomeBase.ClientID%>").value = "0";
                }

            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx"
                VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgFleetGroup" runat="server" AllowSorting="true" OnItemCreated="dgFleetGroup_ItemCreated"
                OnPageIndexChanged="dgAircraftType_PageIndexChanged" OnNeedDataSource="dgFleetGroup_BindData"
                OnItemCommand="dgFleetGroup_ItemCommand" OnUpdateCommand="dgFleetGroup_UpdateCommand"
                OnInsertCommand="dgFleetGroup_InsertCommand" OnDeleteCommand="dgFleetGroup_DeleteCommand"
                AutoGenerateColumns="false" PageSize="10" Height="341px" AllowPaging="true" OnSelectedIndexChanged="dgFleetGroup_SelectedIndexChanged"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPreRender="dgFleetGroup_PreRender">
                <MasterTableView DataKeyNames="FleetGroupID,FleetGroupCD,FleetGroupDescription,HomebaseID,HomebaseCD,LastUpdUID,LastUpdTS,IsInActive"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="FleetGroupID" HeaderText="FleetGroupID" ShowFilterIcon="false"
                            Display="false" CurrentFilterFunction="EqualTo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FleetGroupCD" HeaderText="Fleet Group Code" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="100px" FilterControlWidth="80px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FleetGroupDescription" HeaderText="Description"
                            HeaderStyle-Width="460px" FilterControlWidth="440px" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="100px" FilterControlWidth="80px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" HeaderStyle-Width="80px">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddFleetGroupCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                Visible='<%# IsAuthorized(Permission.Database.EditFleetGroupCatalog)%>' ToolTip="Edit"
                                CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                Visible='<%# IsAuthorized(Permission.Database.DeleteFleetGroupCatalog)%>' runat="server"
                                CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkFleetInactive" runat="server" Text="Inactive" Font-Size="12px"
                                        ForeColor="Black" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel110" valign="top">
                                    <span class="mnd_text">Fleet Group Code</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCode" AutoPostBack="true" OnTextChanged="Code_TextChanged" runat="server"
                                                    MaxLength="4" CssClass="text50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="Save" ControlToValidate="tbCode"
                                                    Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Fleet Group Code is Required.</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Fleet Group Code is Required"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel110" valign="top">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" runat="server" MaxLength="30" CssClass="text180"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="Save"
                                                    ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Description is Required.</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel110" valign="top">
                                    Home Base
                                </td>
                                <td align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" OnTextChanged="HomeBase_TextChanged"
                                                    AutoPostBack="true" CssClass="text80" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                <asp:HiddenField ID="hdnHomeBase" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnHomeBase" runat="server" OnClientClick="javascript:openWin();return false;"
                                                    CssClass="browse-button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                    ErrorMessage="Invalid Home Base Code." Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel110" valign="top">
                                    <asp:Label ID="lbOrderDetails" runat="server" Text="Order Details" />
                                </td>
                                <td align="left">
                                    <table cellpadding="0">
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="lbFleetAvailable" runat="server" Text="Available:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td valign="top">
                                                <asp:Label ID="lbFleetSelected" runat="server" Text="Selected:" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <telerik:RadListBox ID="lstavailable" SelectionMode="Multiple" runat="server" Width="275px"
                                                    AllowReorder="false" EnableDragAndDrop="true" AllowTransferOnDoubleClick="true"
                                                    AllowTransfer="true" TransferMode="Move" AllowTransferDuplicates="false" TransferToID="lstSelected"
                                                    Height="200px">
                                                    <ButtonSettings ShowDelete="false" ShowReorder="false" />
                                                </telerik:RadListBox>
                                            </td>
                                            <td valign="middle">
                                                <%-- <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnNext" OnClick="btnNext_click" runat="server" CssClass="icon-next"
                                                                ToolTip="Add Selected items" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnLast" runat="server" OnClick="btnLast_click" CssClass="icon-last"
                                                                ToolTip="Add All Items" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnFirst" runat="server" OnClick="btnFirst_click" CssClass="icon-first"
                                                                ToolTip="Remove All Items" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnPrevious" runat="server" OnClick="btnPrevious_click" CssClass="icon-prev"
                                                                ToolTip="Remove Selected Items" />
                                                        </td>
                                                    </tr>
                                                </table>--%>
                                            </td>
                                            <td>
                                                <telerik:RadListBox ID="lstselected" SelectionMode="Multiple" runat="server" Width="275px"
                                                    AllowReorder="true" EnableDragAndDrop="true" AllowTransferOnDoubleClick="true"
                                                    TransferToID="lstAvailable" AllowTransfer="true" TransferMode="Move" AllowTransferDuplicates="false"
                                                    Height="200px">
                                                    <ButtonSettings ShowDelete="false" ShowReorder="false" ShowTransfer="false" ShowTransferAll="false" />
                                                </telerik:RadListBox>
                                            </td>
                                            <td>
                                                <asp:ListBox ID="ls1" SelectionMode="Multiple" runat="server" Visible="false"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveChanges" Text="Save" ValidationGroup="Save" runat="server"
                            OnClick="SaveChanges_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnFleetGroupID" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                        <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                            OnClick="Cancel_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
