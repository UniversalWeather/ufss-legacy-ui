﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using System.Linq;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetProfileAdditionalInfoCatalog : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private bool IsEmptyCheck = true;
        private bool _selectLastModified = false;

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFltAddInfo, dgFltAddInfo, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFltAddInfo.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewFleetProfileAdditionalInfoCatalog);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgFltAddInfo.Rebind();
                                ReadOnlyForm();
                                EnableForm(false);                                
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                DefaultSelection();
                            }
                            btnSaveChanges.Visible = false;
                            btnCancel.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }
        }

        protected void dgAircraftType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFltAddInfo.ClientSettings.Scrolling.ScrollTop = "0";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FeeGroup);
                }
            }
        }

        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection()
        {
            dgFltAddInfo.Rebind();
            
            if (dgFltAddInfo.MasterTableView.Items.Count > 0)
            {
                //if (!IsPostBack)
                //{
                //    Session["FAISelectedItem"] = null;
                //}
                if (Session["FAISelectedItem"] == null)
                {
                    dgFltAddInfo.SelectedIndexes.Add(0);
                    GridDataItem item = (GridDataItem)dgFltAddInfo.SelectedItems[0];
                    Session["FAISelectedItem"] = item["FleetProfileInformationID"].Text;
                }

                if (dgFltAddInfo.SelectedIndexes.Count == 0)
                    dgFltAddInfo.SelectedIndexes.Add(0);

                ReadOnlyForm();
                GridEnable(true, true, true);
            }
            else
            {
                ClearForm();
                EnableForm(false);
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    target.Focus();
                    IsEmptyCheck = false;
                }

            }
        }

        /// <summary>
        /// To check unique Code  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfoCode_TextChanged(object sender, EventArgs e)
        {
            if (tbCode.Text != null)
            {
                checkAllReadyExist();
            }
        }

        /// <summary>
        /// To check whether the Crew already exists 
        /// </summary>
        /// <returns></returns>
        private bool checkAllReadyExist()
        {
            bool returnVal = false;
            using (FlightPakMasterService.MasterCatalogServiceClient objCrewRostersvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objCrewRostersvc.GetFleetProfileInformationList().EntityList.Where(x => x.FleetInfoCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                var objRetVal1 = objCrewRostersvc.GetFleetProfileInformationList().EntityList.Where(x => x.FleetProfileAddInfDescription.ToString().ToUpper().Trim().Equals(tbDescription.Text.ToString().ToUpper().Trim()));
                if (objRetVal.Count() > 0 && objRetVal != null)
                {
                    foreach (FlightPakMasterService.FleetProfileInformation cm in objRetVal)
                    {
                        cvCode.IsValid = false;
                        tbCode.Focus();
                        return true;
                    }
                }
                if (objRetVal1.Count() > 0 && objRetVal1 != null)
                {
                    foreach (FlightPakMasterService.FleetProfileInformation cm in objRetVal)
                    {
                        cvCode.IsValid = false;
                        tbDescription.Focus();
                        return true;
                    }
                }
            }
            return returnVal;
        }
        /// <summary>
        /// Crew Roster Additional Info Item created Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }

        }


        /// <summary>
        /// Crew Roster Additional Info Data Bind event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    string hs;

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCrewRosterAddInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (objCrewRosterAddInfoService.GetFleetProfileInformationList().ReturnFlag == true)
                            {
                                dgFltAddInfo.DataSource = objCrewRosterAddInfoService.GetFleetProfileInformationList().EntityList;
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }

        }

        /// <summary>
        /// Crew Roster Additional Info Item command Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var ReturnValue = CommonService.Lock(EntitySet.Database.FleetProfileInformation, Convert.ToInt64(Session["FAISelectedItem"].ToString().Trim()));
                                    if (!ReturnValue.ReturnFlag)
                                    {
                                        ShowAlert(ReturnValue.LockMessage, ModuleNameConstants.Database.FleetProfileInformation);
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    SelectItem();
                                    tbDescription.Focus();
                                    tbCode.Enabled = false;
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFltAddInfo.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                tbCode.Focus();
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }

        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["FAISelectedItem"] != null)
                {
                    string ID = Session["FAISelectedItem"].ToString();
                    foreach (GridDataItem Item in dgFltAddInfo.MasterTableView.Items)
                    {
                        if (Item["FleetProfileInformationID"].Text.Trim() == ID.Trim())
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    DefaultSelection();
                }
            }
        }

        /// <summary>
        /// Crew Roster Additional Info Update Event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFltAddInfo_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        if (Session["FAISelectedItem"] != null)
                        {
                            Page.Validate();
                            if (IsValid)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objCrewRosterAddInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objCrewRosterAddInfoService.UpdateFleetProfileInformation(GetItems(string.Empty));
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.FleetProfileInformation, Convert.ToInt64(Session["FAISelectedItem"].ToString().Trim()));
                                        }
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        SelectItem();
                                        ReadOnlyForm();
                                        ShowSuccessMessage();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FleetProfileInformation);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }

        }


        /// <summary>
        /// Crew Roster Additional Info Insert Command
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRosterAddInfo_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        e.Canceled = true;

                        Page.Validate();
                        if (IsValid)
                        {  
                            if (checkAllReadyExist())
                            {
                                cvCode.IsValid = false;
                                tbCode.Focus();
                            }
                            else
                            {
                                using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                                {
                                    var objRetVal = objService.AddFleetProfileInformation(GetItems(string.Empty));
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        dgFltAddInfo.Rebind();
                                        DefaultSelection();

                                        ShowSuccessMessage();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FleetProfileInformation);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }

        }
        /// <summary>
        /// <summary>
        /// Crew Roster Additional Info delete command
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFltAddInfo_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["FAISelectedItem"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objCrewRosterAddInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FleetProfileInformation objCrewRosterAddInfo = new FlightPakMasterService.FleetProfileInformation();
                                objCrewRosterAddInfo.FleetProfileInformationID = Convert.ToInt64(Session["FAISelectedItem"]);
                                objCrewRosterAddInfo.IsDeleted = true;

                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var ReturnValue = CommonService.Lock(EntitySet.Database.FleetProfileInformation, Convert.ToInt64(Session["FAISelectedItem"].ToString().Trim()));
                                }
                                objCrewRosterAddInfoService.DeleteFleetProfileInformation(objCrewRosterAddInfo);
                                DefaultSelection();
                                e.Item.OwnerTableView.Rebind();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FleetProfileInformation, Convert.ToInt64(Session["FAISelectedItem"]));
                    }
                }
            }

        }


        /// <summary>
        /// Crew Roster Additional Info Item selected changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFltAddInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {

                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            GridDataItem Item = dgFltAddInfo.SelectedItems[0] as GridDataItem;
                            Session["FAISelectedItem"] = Item["FleetProfileInformationID"].Text;
                            var key = Item.ItemIndex;
                            dgFltAddInfo.Rebind();
                            dgFltAddInfo.SelectedIndexes.Add(key);
                            ReadOnlyForm();
                            GridEnable(true, true, true);
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                    }
                }
            }
        }

        /// <summary>
        /// Method to insert the form Values
        /// </summary>
        protected void DisplayInsertForm()
        {
            dgFltAddInfo.Rebind();
            hdnSave.Value = "Save";
            ClearForm();
            EnableForm(true);
        }

        /// <summary>
        /// Crew Roster Additional Info Save changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgFltAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgFltAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }

        }

        /// <summary>
        /// Crew Roster Additional Info Cancel event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Session.Remove("FAISelectedItem");
                        DefaultSelection();
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        /// <summary>
        /// Rad Ajax creating event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFltAddInfo;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfileInformation);
                }
            }

        }


        /// <summary>
        /// Crew Roster Additional Info Get items method
        /// </summary>
        /// <param name="PassengerAdditionalInfoCode"></param>
        /// <returns></returns>
        private FleetProfileInformation GetItems(string CrewRosterAddInfoCode)
        {
            FlightPakMasterService.FleetProfileInformation objCrewRosterAddInfo = new FlightPakMasterService.FleetProfileInformation();
            if (hdnSave.Value == "Update")
            {
                objCrewRosterAddInfo.FleetProfileInformationID = Convert.ToInt64(Session["FAISelectedItem"]);
            }
            else
                objCrewRosterAddInfo.FleetProfileInformationID = 0;

            objCrewRosterAddInfo.FleetInfoCD = tbCode.Text.Trim().ToUpper();
            objCrewRosterAddInfo.FleetProfileAddInfDescription = tbDescription.Text.Trim().ToUpper();
            objCrewRosterAddInfo.LastUpdTS= DateTime.UtcNow;
            objCrewRosterAddInfo.IsDeleted = false;
            return objCrewRosterAddInfo;
        }

        /// <summary>
        /// Crew Roster Additional Info Edit form
        /// </summary>s
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LoadControlData();
                hdnSave.Value = "Update";
                hdnRedirect.Value = "";
                dgFltAddInfo.Rebind();
                EnableForm(true);
            }
        }

        /// <summary>
        /// Grid enable event
        /// </summary>
        /// <param name="add"></param>
        /// <param name="edit"></param>
        /// <param name="delete"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            LinkButton insertCtl, delCtl, editCtl;
            insertCtl = (LinkButton)dgFltAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
            delCtl = (LinkButton)dgFltAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
            editCtl = (LinkButton)dgFltAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
            if (add)
                insertCtl.Enabled = true;
            else
                insertCtl.Enabled = false;
            if (delete)
            {
                delCtl.Enabled = true;
                delCtl.OnClientClick = "javascript:return ProcessDelete();";
            }
            else
            {
                delCtl.Enabled = false;
                delCtl.OnClientClick = "";
            }
            if (edit)
            {
                editCtl.Enabled = true;
                editCtl.OnClientClick = "javascript:return ProcessUpdate();";
            }
            else
            {
                editCtl.Enabled = false;
                editCtl.OnClientClick = "";
            }
        }

        /// <summary>
        /// Read only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            LoadControlData();
            //hdnSave.Value = "Update";

            EnableForm(false);
        }

        /// <summary>
        /// Clear form
        /// </summary>
        protected void ClearForm()
        {
            tbCode.Text = string.Empty;
            tbDescription.Text = string.Empty;
        }

        /// <summary>
        /// Enable form
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            tbCode.Enabled = enable;
            tbDescription.Enabled = enable;
            btnCancel.Visible = enable;
            btnSaveChanges.Visible = enable;
        }



        /// <summary>
        /// Clear filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnclrFilters_Click(object sender, EventArgs e)
        {
            pnlExternalForm.Visible = false;
            foreach (GridColumn column in dgFltAddInfo.MasterTableView.Columns)
            {
                //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                //column.CurrentFilterValue = string.Empty;
            }
            dgFltAddInfo.MasterTableView.FilterExpression = string.Empty;
            dgFltAddInfo.MasterTableView.Rebind();
        }

        /// <summary>
        /// To assign the data to controls 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadControlData()
        {
            tbCode.Text = "";
            tbDescription.Text = "";
            Label lbLastUpdatedUser;
            lbLastUpdatedUser = (Label)dgFltAddInfo.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");

            using (FlightPakMasterService.MasterCatalogServiceClient objCrewRosterAddInfoService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var retval = objCrewRosterAddInfoService.GetFleetProfileInformationList().EntityList.Where(x => x.FleetProfileInformationID.Equals(Convert.ToInt64(Session["FAISelectedItem"].ToString())));

                foreach (FlightPakMasterService.FleetProfileInformation FltInf in retval)
                {
                    tbCode.Text = FltInf.FleetInfoCD.ToString();
                    if (FltInf.FleetProfileAddInfDescription.ToString().Trim() != "")
                        tbDescription.Text = FltInf.FleetProfileAddInfDescription.ToString();
                    lbLastUpdatedUser.Text = "Last Updated";
                    if (FltInf.LastUpdUID != null && FltInf.LastUpdUID.ToString().Trim() != "")
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " " + " User: " + FltInf.LastUpdUID.ToString().Trim());

                    if (FltInf.LastUpdTS != null && FltInf.LastUpdTS.ToString().Trim() != "")
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(FltInf.LastUpdTS.ToString().Trim())));

                    lbColumnName1.Text = "Additional Info Code";
                    lbColumnName2.Text = "Description";
                    lbColumnValue1.Text = System.Web.HttpUtility.HtmlEncode(FltInf.FleetInfoCD.ToString());
                    lbColumnValue2.Text = System.Web.HttpUtility.HtmlEncode(FltInf.FleetProfileAddInfDescription.ToString());
                    break;
                }
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFltAddInfo.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var FleetAdditionalValue = FPKMstService.GetFleetProfileInformationList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, FleetAdditionalValue);
            List<FlightPakMasterService.FleetProfileInformation> filteredList = GetFilteredList(FleetAdditionalValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FleetProfileInformationID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFltAddInfo.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFltAddInfo.CurrentPageIndex = PageNumber;
            dgFltAddInfo.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfFleetProfileInformation FleetAdditionalValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["FAISelectedItem"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = FleetAdditionalValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FleetProfileInformationID;
                Session["FAISelectedItem"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.FleetProfileInformation> GetFilteredList(ReturnValueOfFleetProfileInformation FleetAdditionalValue)
        {
            List<FlightPakMasterService.FleetProfileInformation> filteredList = new List<FlightPakMasterService.FleetProfileInformation>();

            if (FleetAdditionalValue.ReturnFlag)
            {
                filteredList = FleetAdditionalValue.EntityList.Where(x => x.IsDeleted == false).ToList();
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFltAddInfo.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFltAddInfo.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        protected void dgFltAddInfo_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFltAddInfo, Page.Session);
        }
    }
}







