﻿<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <title>Fleet Profile</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var selectedRowData = null;
        var jqgridTableId = '#gridfleetProfile';
        var deleteFleetId = null;
        var deleteTailNum = null;
        var selectedTailNum = "";
        var isopenlookup = false;
        $(document).ready(function () {
            selectedTailNum = $.trim(unescape(getQuerystring("TailNumber", "")));
            if (selectedTailNum) {
                isopenlookup = true;
            }
            $("#btnSubmit").click(function () {
                var selr = $('#gridfleetProfile').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridfleetProfile').getRowData(selr);
                if (rowData["FleetID"] == undefined) {
                    BootboxAlert("Please select a tail no.");
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            $("#recordAdd").click(function () {
                var originurl = window.location.href;
                var splitURL = originurl.split('//');
                var fleetProfileCatalogURL = splitURL[0] + "//" + window.location.host + "/Views/Settings/Fleet/FleetProfileCatalogBootStrap.aspx?IsPopup=Add&ParentPage=FleetProfilePopupBootStrap";
                self.parent.SetFunction("FleetProfilePopupBootStrap", reloadFleetProfile);
                self.parent.OpenWithBootStrapPopup(fleetProfileCatalogURL, '#ModalFleetProfileCatalog', '#BodyFleetProfileCatalog', 'iFrameFleetProfileCatalog');
                return false;
            });
            $("#recordEdit").click(function () {
                var selr = $('#gridfleetProfile').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridfleetProfile').getRowData(selr);
                var fleetID = rowData['FleetID'];
                var widthDoc = $(document).width();
                if (fleetID != undefined && fleetID != null && fleetID != '' && fleetID != 0) {
                    var originurl = window.location.href;
                    var splitURL = originurl.split('//');
                    var fleetProfileCatalogURL = splitURL[0] + "//" + window.location.host + "/Views/Settings/Fleet/FleetProfileCatalogBootStrap.aspx?IsPopup=&FleetId=" + fleetID + "&ParentPage=FleetProfilePopupBootStrap";
                    self.parent.SetFunction("FleetProfilePopupBootStrap", reloadFleetProfile);
                    self.parent.OpenWithBootStrapPopup(fleetProfileCatalogURL, '#ModalFleetProfileCatalog', '#BodyFleetProfileCatalog', 'iFrameFleetProfileCatalog');
                }
                else {
                    BootboxAlert("Please select a tail no.");
                }
                return false;
            });
            $("#recordDelete").click(function () {
                var selr = $('#gridfleetProfile').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridfleetProfile').getRowData(selr);
                deleteFleetId = rowData['FleetID'];
                deleteTailNum = rowData['TailNum'];
                var widthDoc = $(document).width();
                if (deleteFleetId != undefined && deleteFleetId != null && deleteFleetId != '' && deleteFleetId != 0) {
                    bootbox.dialog({
                        message: '<img src="/Scripts/images/error.gif" /> <span>Are you sure you want to delete this record?</span>',
                        title: '<img src="/Scripts/images/title_icon.gif" /> <span>Universal Weather And Aviation</span>',
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn-success",
                                callback: function () {
                                    confirmCallBackFn();
                                }
                            },
                            danger: {
                                label: "Cancel",
                                className: "btn-danger",
                                callback: function () {

                                }
                            }
                        }
                    });
                }
                else {
                    BootboxAlert("Please select a tail no.");
                }
                return false;
            });
            $("#btnClientCodeFilter").click(function () {
                var originurl = window.location.href;
                var splitURL = originurl.split('//');
                var clientCodePopUpUrl = splitURL[0] + "//" + window.location.host + "/Views/Settings/Company/ClientCodePopupBootStrap.aspx?ClientCD=" + document.getElementById("tbClientCodeFilter").value + "&ParentPage=FleetProfilePopupBootStrap";
                self.parent.SetHtmlControl('FleetProfilePopupBootStrap',document.getElementById("tbClientCodeFilter"));
                self.parent.OpenWithBootStrapPopup(clientCodePopUpUrl, '#ModalClientCode', '#BodyClientCode', 'iFrameClientCode');
                return false;
            });
            function confirmCallBackFn() {
                    $.ajax({
                        type: 'GET',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=FleetProfile&fleetID=' + deleteFleetId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This record is already in use. Deletion is not allowed.";
                            DeleteApiResponseWithBootStrapDialLogBox(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (content.indexOf('404'))
                                BootboxAlert("This record is already in use. Deletion is not allowed.");
                        }
                    });
            }
            function chkformat(cellvalue, options, rowObject) {
                if (cellvalue == "" || cellvalue == "false")
                    return "<input type=checkbox checked=checked value=true offval=no disabled=disabled/>";
                else
                    return "<input type=checkbox value=false offval=no disabled=disabled>";
            }

            jQuery("#gridfleetProfile").jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }

                    var isFixedRotary;
                    var chkRotary = $('#chkRotary').is(':checked') ? true : false;
                    var chkFixed = $('#chkFixedWing').is(':checked') ? true : false;
                    if (chkRotary == true && chkFixed == false) {
                        isFixedRotary = "R";
                    } else if (chkRotary == false && chkFixed == true) {
                        isFixedRotary = "F";
                    } else {
                        isFixedRotary = "B";
                    }
                    var model = {
                        ClientID: $("#hdnClientId").val(),
                        FetchActiveOnly: $('#chkActiveOnly').is(':checked') ? true : false,
                        IsFixedRotary: isFixedRotary,
                        VendorID: 0
                    };
                    postData.Model = model;
                    postData.sendCustomerId = false;
                    postData.sendIcaoId = $('#chkHomeBaseOnly').is(':checked') ? true : false;
                    postData.apiType = 'fss';
                    postData.method = 'fleetprofile';
                    postData.markSelectedRecord = isopenlookup;
                    postData.tailNum = selectedTailNum;
                    isopenlookup = false;
                    return postData;
                },
                height: 300,
                width: 1035,
                viewrecords: true,
                rowNum: $("#rowNumFleetProfile").val(),
                multiselect: false,
                pager: "#pg_gridPager_fleetProfile",
                colNames: ['FleetID', 'Fleet Code', 'Tail No.', 'Type Code', 'Type Description', 'Constant Mach', 'Long Range Cruise', 'High Speed Cruise', 'Maximum Pax', 'Vendor Code', 'Home Base', 'Active', 'IcaoID'],
                colModel: [
                    { name: 'FleetID', index: 'FleetID', key: true, hidden: true, },
                    { name: 'AircraftCD', index: 'AircraftCD', width: 110 },
                    { name: 'TailNum', index: 'TailNum', width: 100 },
                    { name: 'AirCraft_AircraftCD', index: 'AirCraft_AircraftCD', width: 110 },
                    { name: 'TypeDescription', index: 'TypeDescription', width: 160 },
                    { name: 'PowerSettings1TrueAirSpeed', index: 'PowerSettings1TrueAirSpeed', width: 100 },
                    { name: 'PowerSettings2TrueAirSpeed', index: 'PowerSettings2TrueAirSpeed', width: 100 },
                    { name: 'PowerSettings3TrueAirSpeed', index: 'PowerSettings3TrueAirSpeed', width: 100 },
                    { name: 'MaximumPassenger', index: 'MaximumPassenger', width: 80 },
                    {
                        name: 'VendorCD', index: 'VendorCD', width: 80,
                        searchoptions: {
                            dataInit: function (el) {
                                var VendorCode = getQuerystring("VendorCD", "");
                                if (VendorCode != null && VendorCode != "" && VendorCode != undefined) {
                                    var intervalId = setInterval(function () {
                                        var rowCount = $("#gridfleetProfile").getGridParam("reccount");
                                        if (rowCount != undefined && rowCount != null && rowCount != "" && rowCount > 0) {
                                            $(el).val(VendorCode);
                                            $(el).focus().trigger({ type: 'keypress', charCode: 13 });
                                            clearInterval(intervalId);
                                        }
                                    }, 1000);
                                }
                            }
                        }
                    },
                     { name: 'IcaoID', index: 'IcaoID', width: 75 },
                     { name: 'IsInactive', index: 'IsInactive', width: 55, formatter: 'checkbox', align: 'center', search: false, formatter: chkformat },
                    { name: 'HomebaseID', index: 'HomebaseID', hidden: true }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);

                    returnToParent(rowData);
                },
                onSelectRow: function (id) {

                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['AircraftCD'];//replace name with any column
                    selectedTailNum = $.trim(rowData['AircraftCD']);

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                loadComplete: function () {
                    setScrollFleetProfile();
                    
                    var i, count, $grid = $(jqgridTableId);
                    var rowArray = $(jqgridTableId).jqGrid('getDataIDs');
                    for (i = 0, count = rowArray.length; i < count; i += 1) {
                        var rowData = $(jqgridTableId).getRowData(rowArray[i]);
                        if ($.trim(rowData["TailNum"]) == $.trim(selectedTailNum)) {
                            $grid.jqGrid('setSelection', rowArray[i], true);
                            break;
                        }
                    }
                },
                afterInsertRow: function (rowid, rowdata, rowelem) {
                    var lastSel = rowdata['TailNum'];//replace name with any column

                    if ($.trim(selectedTailNum) == $.trim(lastSel)) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        $(jqgridTableId).jqGrid('setSelection', rowid);
                    }
                }
            });
            $("#pg_gridPager_fleetProfile_left").html($("#pagesizebox"));
            $("#gridfleetProfile").jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
        });

        $(window).resize(function () {
            setScrollFleetProfile();
        });
        function reloadFleetProfile() {
            //Reset the scroll
            $('#gbox_gridfleetProfile').find(".jspPane").css("top", 0);
            $(jqgridTableId).jqGrid().trigger('reloadGrid');
        }
        function setScrollFleetProfile() {
            var table_header = $('#gbox_gridfleetProfile').find('.ui-jqgrid-hbox').css("position", "relative");
            $('#gbox_gridfleetProfile').find('.ui-jqgrid-bdiv').bind('jsp-scroll-x', function (event, scrollPositionX, isAtLeft, isAtRight) {
                table_header.css('right', scrollPositionX);
            }).jScrollPane({
                scrollbarWidth: 15,
                scrollbarMargin: 0
            });
        }
        function tbClientCodeFilterChange() {
            $("#hdnClientId").val("");
            var clientCd = $.trim($("#tbClientCodeFilter").val());
            if (!IsNullOrEmpty(clientCd)) {
                CheckClientCodeExistance(clientCd, hdnClientId, errorMsg);
            }
            else {
                $("#hdnClientId").val(0);
                $("#errorMsg").removeClass().addClass('hideDiv');
            }
            reloadFleetProfile();
        }
    </script>
    <script type="text/javascript">
        function returnToParent(rowData) {
            var oArg = new Object();
            oArg.FleetID = rowData["FleetID"];
            oArg.FleetIDs = oArg.FleetID;
            oArg.TailNum = rowData["TailNum"];
            oArg.AircraftTypeCD = rowData["AircraftCD"];
            oArg.AirCraftTypeCode = rowData["AirCraft_AircraftCD"];
            oArg.AirCraftTypeCodes = oArg.AirCraftTypeCode;
            oArg.Arg1 = oArg.TailNum;
            oArg.CallingButton = "TailNum";
            oArg.Arg2 = oArg.FleetID;
            oArg.CallingButton1 = "FleetID";
            oArg.Arg3 = oArg.AirCraftTypeCode;
            oArg.CallingButton2 = "AirCraft_AircraftCD";
            self.parent.Preflight.PreflightMain.Fleet.TailNum(oArg.TailNum);
            self.parent.TailNumValidation();
            self.parent.ManuallyCloseBootstrapPopup("#ModalFleetProfile");
        }
    </script>

    <style type="text/css">
         body {
             height: 463px;
             background:#fff !important;
         }

        #pg_gridPager_fleetProfile_center {
            width: 530px;
        }

        #pg_gridPager_fleetProfile_center > table {
            margin-left: 0;
        }

        .showDiv {
            text-align: right;
            margin-right: 77px;
            color: red;
            visibility: visible !important;
        }

        .hideDiv {
            visibility: hidden;
        }
    </style>

</head>

<body>
    <form id="form1" runat="server">
         <%=Scripts.Render("~/bundles/BootStrapScriptBundle") %>
         <%=Styles.Render("~/bundles/BootStrapPopupCssBundle") %>
         <div class="bootstrap-common fleetprofile_popup">
            <div class="jqgrid row">
            <div class="col-md-12">
                <table class="box1">
                    <tr>
                        <td>
                            <div class="header_inn_wrapper">
                                <div class="header_wrapper_left">
                                    <ul>
                                        <li>
                                            <input type="checkbox" id="chkActiveOnly" checked="checked" onchange="reloadFleetProfile()" />
                                            <label class="left_label">Active Only</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="chkHomeBaseOnly"  onchange="reloadFleetProfile()"/>
                                            <label class="left_label">Home Base Only</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="chkRotary" checked="checked"  onchange="reloadFleetProfile()"/>
                                            <label class="left_label">Rotary Only</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="chkFixedWing" checked="checked"  onchange="reloadFleetProfile()"/>
                                            <label class="left_label">Fixed Wing Only</label>
                                        </li>
                                        <li>
                                            <div class="search_code">
                                                <ul>
                                                    <li>
                                                        <label>Client Code:</label>
                                                    </li>
                                                    <li>
                                                        <input style="width: 48px;"  type="text" id="tbClientCodeFilter" onchange="tbClientCodeFilterChange();" name="tbClientCodeFilter" maxlength="5" class="text50" />
                                                        <input id="hdnClientId" type="hidden" value="" />
                                                        <div class="error_code"><div id="errorMsg" class="hideDiv">Invalid client code.!</div></div>
                                                    </li>
                                                    <li>
                                                        <input type="button" class="browse-button"  data-toggle="modal" data-target="#ModalClientCode" data-bind="showModal: displaySubmitModal()"  id="btnClientCodeFilter" />
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="search_btn"></div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="header_wrapper_right"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridfleetProfile" style="width: 981px !important;" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager_fleetProfile" class="footer_type2"></div>
                                <a title="Add" class="add-icon-grid" data-toggle="modal" data-target="#ModalFleetProfileCatalog" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNumFleetProfile" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize($(jqgridTableId), $('#rowNumFleetProfile')); return false;" />
                                </div>
                            </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        </div>
    </form>
</body>
</html>

