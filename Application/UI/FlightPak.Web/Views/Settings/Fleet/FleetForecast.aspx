﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FleetForecast.aspx.cs"
    MasterPageFile="~/Framework/Masters/Settings.master" Inherits="FlightPak.Web.Views.Settings.Fleet.FleetForecast"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script runat="server">
       
        void QueryStringButton_Click(object sender, EventArgs e)
        {
            string URL = "../Fleet/FleetProfileCatalog.aspx?FleetID=" + hdnFleetID.Value + "&Screen=FleetProfile";
            Response.Redirect(URL);
        }

      
    </script>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">
                        <asp:LinkButton ID="LinkButton1" Text="Fleet Profile " runat="server" OnClick="QueryStringButton_Click"></asp:LinkButton>&nbsp;>
                        Fleet Forecast</span> <span class="tab-nav-icons">
                            <!--<a href="#" class="search-icon"></a>
                            <a href="#" class="save-icon"></a><a href="#" class="print-icon"></a>-->
                            <a href="#" title="Help" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" class="sticky">
        <tr>
            <td class="tdLabel100">
                Tail No.
            </td>
            <td>
                <asp:TextBox ID="tbTailNumber" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="bodyForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgFF" />
                    <telerik:AjaxUpdatedControl ControlID="pnlExternalForm" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="bodyForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="pnlExternalForm" />
                    <telerik:AjaxUpdatedControl ControlID="dgFF" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFF">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="bodyForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbforecastyear" />
                    <telerik:AjaxUpdatedControl ControlID="tbjanuary" />
                    <telerik:AjaxUpdatedControl ControlID="tbfebruary" />
                    <telerik:AjaxUpdatedControl ControlID="tbmarch" />
                    <telerik:AjaxUpdatedControl ControlID="tbapril" />
                    <telerik:AjaxUpdatedControl ControlID="tbmay" />
                    <telerik:AjaxUpdatedControl ControlID="tbjune" />
                    <telerik:AjaxUpdatedControl ControlID="tbjuly" />
                    <telerik:AjaxUpdatedControl ControlID="tbaugust" />
                    <telerik:AjaxUpdatedControl ControlID="tbseptember" />
                    <telerik:AjaxUpdatedControl ControlID="tboctober" />
                    <telerik:AjaxUpdatedControl ControlID="tbnovember" />
                    <telerik:AjaxUpdatedControl ControlID="tbdecember" />
                    <telerik:AjaxUpdatedControl ControlID="hdnFleetForeCastID" />
                    <telerik:AjaxUpdatedControl ControlID="btnSaveChanges" />
                    <telerik:AjaxUpdatedControl ControlID="btnCancel" />
                    <telerik:AjaxUpdatedControl ControlID="dgFF" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbforecastyear">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbforecastyear" />
                    <telerik:AjaxUpdatedControl ControlID="cvYear" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadGrid ID="dgFF" runat="server" AllowSorting="true" OnItemCreated="dgFF_ItemCreated"
        OnPageIndexChanged="dgAircraftType_PageIndexChanged" OnNeedDataSource="dgFF_BindData"
        OnItemCommand="dgFF_ItemCommand" OnInsertCommand="dgFF_InsertCommand" OnItemDataBound="dgFF_ItemDataBound"
        OnDeleteCommand="dgFF_DeleteCommand" OnUpdateCommand="dgFF_UpdateCommand" AutoGenerateColumns="false"
        Height="341px" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgFF_SelectedIndexChanged"
        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPreRender="dgFF_PreRender">
        <MasterTableView DataKeyNames="FleetForeCastID,FleetID,YearMon,YearMonthDate,LastUpdUID,LastUpdTS"
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="FleetForeCastID" HeaderText="Code" ShowFilterIcon="false"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="YearMon" HeaderText="Forecast Year" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                        Visible='<%# IsAuthorized(Permission.Database.AddFleetForecast)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        Visible='<%# IsAuthorized(Permission.Database.EditFleetForecast)%>' ToolTip="Edit"
                        CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                        Visible='<%# IsAuthorized(Permission.Database.DeleteFleetForecast)%>' runat="server"
                        CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                </div>
                <div style="padding: 5px 5px; float: right;">
                    <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false"></GroupingSettings>
    </telerik:RadGrid>
    <div id="bodyForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="True">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <%-- <asp:Panel ID="pnlExternalForm" runat="server" Visible="True">--%>
            <table class="border-box" width="100%">
                <tr>
                    <td class="tdLabel90">
                        <span class="mnd_text">Forecast Year</span>
                    </td>
                    <td>
                        <asp:TextBox ID="tbforecastyear" Style="text-align: right;" OnTextChanged="ForecastYear_TextChanged"
                            MaxLength="4" CssClass="text100" runat="server" TabIndex="9" AutoPostBack="true"
                            onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RequiredFieldValidator ID="rfvforecastyear" runat="server" ValidationGroup="Save"
                            ControlToValidate="tbforecastyear" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Forecast Year Required.</asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvYear" runat="server" ControlToValidate="tbforecastyear"
                            ErrorMessage="Unique Forecast Year is Required" Display="Dynamic" CssClass="alert-text"
                            ValidationGroup="Save"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        January:
                    </td>
                    <td class="tdLabel150">
                        <asp:TextBox ID="tbjanuary" TabIndex="10" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                    <td class="tdLabel100">
                        July:
                    </td>
                    <td>
                        <asp:TextBox ID="tbjuly" TabIndex="16" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        February:
                    </td>
                    <td>
                        <asp:TextBox ID="tbfebruary" TabIndex="11" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        August:
                    </td>
                    <td>
                        <asp:TextBox ID="tbaugust" TabIndex="17" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        March:
                    </td>
                    <td>
                        <asp:TextBox ID="tbmarch" TabIndex="12" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        September:
                    </td>
                    <td>
                        <asp:TextBox ID="tbseptember" TabIndex="18" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        April:
                    </td>
                    <td>
                        <asp:TextBox ID="tbapril" TabIndex="13" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        October:
                    </td>
                    <td>
                        <asp:TextBox ID="tboctober" TabIndex="19" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        May:
                    </td>
                    <td>
                        <asp:TextBox ID="tbmay" TabIndex="14" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        November:
                    </td>
                    <td>
                        <asp:TextBox ID="tbnovember" TabIndex="20" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        June:
                    </td>
                    <td>
                        <asp:TextBox ID="tbjune" TabIndex="15" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        December:
                    </td>
                    <td>
                        <asp:TextBox ID="tbdecember" TabIndex="21" Style="text-align: right;" onKeyPress="return fnAllowNumeric(this, event)"
                            MaxLength="6" CssClass="text100" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveChanges" Text="Save" TabIndex="22" OnClick="SaveChanges_Click"
                            ValidationGroup="Save" runat="server" CssClass="button" />
                    </td>
                    <td align="right">
                        <asp:Button ID="btnCancel" Text="Cancel" TabIndex="23" CausesValidation="false" runat="server"
                            OnClick="Cancel_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnFleetID" runat="server" />
                        <asp:HiddenField ID="hdnFleetForeCastID" runat="server" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                </tr>
            </table>
            <%-- </asp:Panel>--%>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <%--<span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>--%>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
