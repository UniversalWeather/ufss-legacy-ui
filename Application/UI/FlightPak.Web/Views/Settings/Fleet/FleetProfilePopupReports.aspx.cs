﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Fleet
{
    public partial class FleetProfilePopupReports : BaseSecuredPage
    {

        private string TailNum;
        private ExceptionManager exManager;
        public bool showCommandItems = true;
        public bool CRMain = false;
        private string VendorID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string i = Request.QueryString["TailNumber"];

                if (Request.QueryString["TailNumber"] != null || Request.QueryString["FromPage"] != null)
                {
                    if ((Request.QueryString["TailNumber"] != null && Request.QueryString["TailNumber"].ToString() == "1") || (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "CompareCharterRates"))
                    {
                        dgFleetPopup.AllowMultiRowSelection = true;
                    }
                    else
                    {
                        dgFleetPopup.AllowMultiRowSelection = false;
                    }
                }
                else
                {
                    dgFleetPopup.AllowMultiRowSelection = false;
                }
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        showCommandItems = true;
                        if (!IsPostBack)
                        {
                            //Added for Reassign the Selected Value and highlight the specified row in the Grid             
                            if (Request.QueryString["TailNumber"] != null)
                            {
                                TailNum = Request.QueryString["TailNumber"].ToUpper().Trim();
                            }

                            if (Request.QueryString["Vendor"] != null)
                            {
                                VendorID = Request.QueryString["Vendor"].Trim();
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }



        }

        protected void Search_Click(object sender, EventArgs e)
        {
            chkAllActive.Checked = false;
            chkAllInactive.Checked = false;
            foreach (GridDataItem dataItem in dgFleetPopup.MasterTableView.Items)
            {
                (((CheckBox)dataItem["IsSelect"].Controls[0]).Checked) = false;
                dataItem.Selected = false;
            }
        }

        protected void CrewRoster_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridDataItem dataItem in dgFleetPopup.MasterTableView.Items)
            {
                if ((((CheckBox)dataItem["IsSelect"].Controls[0]).Checked) == true)
                {
                    dataItem.Selected = true;
                }
                if (dataItem.Selected)
                {
                    (((CheckBox)dataItem["IsSelect"].Controls[0]).Checked) = true;
                }
            }
        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgFleetPopup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetAllFleetForPopupList();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgFleetPopup.DataSource = ObjRetVal.EntityList.ToList();
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFleetPopup_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["TailNumber"] != null)
                {
                    if (Request.QueryString["TailNumber"].ToString() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }

        protected void dgFleetPopup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgFleetPopup_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFleetPopup, Page.Session);
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgFleetPopup;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

        }

        protected void dgFleetPopup_ItemDatabound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            CheckBox chkSelect = (CheckBox)Item["IsSelect"].Controls[0];

                            if (chkAllActive.Checked)
                            {
                                if (Item.GetDataKeyValue("IsInActive") != null && Convert.ToBoolean(Item.GetDataKeyValue("IsInActive")) == false)
                                {
                                    Item.Selected = true;
                                    chkSelect.Checked = true;
                                }
                            }
                            if (chkAllInactive.Checked)
                            {
                                if (Item.GetDataKeyValue("IsInActive") != null && Convert.ToBoolean(Item.GetDataKeyValue("IsInActive")) == true)
                                {
                                    Item.Selected = true;
                                    chkSelect.Checked = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }



        protected void Crew_ToggleSelectedState(object sender, EventArgs e)
        {

            foreach (GridDataItem dataItem in dgFleetPopup.MasterTableView.Items)
            {
                dataItem.Selected = (((CheckBox)dataItem["IsSelect"].Controls[0]).Checked);
            }
        }


        protected void chkAllActive_CheckedChanged(object sender, EventArgs e)
        {
            dgFleetPopup.Rebind();

            foreach (GridDataItem dataItem in dgFleetPopup.MasterTableView.Items)
            {
                CheckBox chkSelect = (CheckBox)dataItem["IsSelect"].Controls[0];
                dataItem.Selected = chkSelect.Checked;
            }

            //foreach (GridDataItem Item in dgFleetPopup.MasterTableView.Items)
            //{
            //    if (chkAllActive.Checked)
            //    {
            //        if (Item.GetDataKeyValue("IsInActive") != null && Convert.ToBoolean(Item.GetDataKeyValue("IsInActive")) == false)
            //        {
            //            CheckBox checkBox = (CheckBox)Item["ClientSelectColumn"].Controls[0];
            //            checkBox.Checked = true;
            //        }
            //    }
            //    if (chkAllInactive.Checked)
            //    {
            //        if (Item.GetDataKeyValue("IsInActive") != null && Convert.ToBoolean(Item.GetDataKeyValue("IsInActive")) == true)
            //        {
            //            CheckBox checkBox = (CheckBox)Item["ClientSelectColumn"].Controls[0];
            //            checkBox.Checked = true;
            //        }
            //    }
            //}
        }

        //protected void RadGrid1_PreRender(object sender, System.EventArgs e)
        //{
        //    foreach (GridDataItem Item in dgFleetPopup.MasterTableView.Items)
        //    {
        //        if (chkAllActive.Checked)
        //        {
        //            if (Item.GetDataKeyValue("IsInActive") != null && Convert.ToBoolean(Item.GetDataKeyValue("IsInActive")) == false)
        //            {
        //                CheckBox checkBox = (CheckBox)Item["ClientSelectColumn"].Controls[0];
        //                checkBox.Checked = true;
        //            }
        //        }
        //        if (chkAllInactive.Checked)
        //        {
        //            if (Item.GetDataKeyValue("IsInActive") != null && Convert.ToBoolean(Item.GetDataKeyValue("IsInActive")) == true)
        //            {
        //                CheckBox checkBox = (CheckBox)Item["ClientSelectColumn"].Controls[0];
        //                checkBox.Checked = true;
        //            }
        //        }
        //    }
        //}


        protected void chkAllInactive_CheckedChanged(object sender, EventArgs e)
        {
            dgFleetPopup.Rebind();

            //foreach (GridDataItem Item in dgFleetPopup.MasterTableView.Items)
            //{
            //    if (chkAllActive.Checked)
            //    {
            //        if (Item.GetDataKeyValue("IsInActive") != null && Convert.ToBoolean(Item.GetDataKeyValue("IsInActive")) == false)
            //        {
            //            CheckBox checkBox = (CheckBox)Item["ClientSelectColumn"].Controls[0];
            //            checkBox.Checked = true;
            //        }
            //    }
            //    if (chkAllInactive.Checked)
            //    {
            //        if (Item.GetDataKeyValue("IsInActive") != null && Convert.ToBoolean(Item.GetDataKeyValue("IsInActive")) == true)
            //        {
            //            CheckBox checkBox = (CheckBox)Item["ClientSelectColumn"].Controls[0];
            //            checkBox.Checked = true;
            //        }
            //    }
            //}
        }

        protected void dgFleetPopup_DeleteCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                GridDataItem item = dgFleetPopup.SelectedItems[0] as GridDataItem;
                Int64 FleetId = Convert.ToInt64(item["FleetId"].Text);
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        using (FlightPakMasterService.MasterCatalogServiceClient FleetTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FlightPakMasterService.Fleet FleetType = new FlightPakMasterService.Fleet();
                            FleetType.FleetID = Convert.ToInt64(FleetId);
                            FleetType.IsDeleted = true;
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var ReturnValue = CommonService.Lock(EntitySet.Database.FleetProfile, FleetId);
                                if (!ReturnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    ShowLockAlertPopup(ReturnValue.LockMessage, ModuleNameConstants.Database.FleetProfile);
                                    return;
                                }
                                FleetTypeService.DeleteFleetProfileType(FleetType);
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.FleetProfile);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FleetProfile, FleetId);
                    }
                }


            }
        }
    }
}
