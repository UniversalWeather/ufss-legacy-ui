﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Aircraft Types</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var selectedRowData = "";
        var jqgridTableId = '#gridAircraft';
        var aircraftId = null;
        var isopenlookup = false;
        $(document).ready(function () {
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            selectedRowData = decodeURI(getQuerystring("AircraftCD", ""));
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if (selr != null) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData);
                }
                else {
                    showMessageBox('Please select an aircraft.', popupTitle);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/Views/Settings/Fleet/AircraftType.aspx?IsPopup=Add", popupTitle, widthDoc + 534, 700, jqgridTableId);
                $(jqgridTableId).trigger('reloadGrid');
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                aircraftId = rowData['AircraftID'];
                var widthDoc = $(document).width();

                if (aircraftId != undefined && aircraftId != null && aircraftId != '' && aircraftId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select an aircraft type.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        async: true,
                        type: 'Get',
                        crossDomain: true,
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Aircrafts&aircraftId=' + aircraftId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This record is already in use. Deletion is not allowed.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (jqXHR.responseText == 'Conflict')
                                showMessageBox("This record is already in use. Deletion is not allowed.", popupTitle);
                        }
                    });
                }
            }

            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                aircraftId = rowData['AircraftID'];
                var widthDoc = $(document).width();
                if (aircraftId != undefined && aircraftId != null && aircraftId != '' && aircraftId != 0) {
                    popupwindow("/Views/Settings/Fleet/AircraftType.aspx?IsPopup=&AircraftID=" + aircraftId, popupTitle, widthDoc + 534, 700, jqgridTableId);
                }
                else {
                    showMessageBox('Please select an aircraft type.', popupTitle);
                }
                return false;
            });

            jQuery(jqgridTableId).jqGrid({

                url: '/Views/Utilities/ApiCallerwithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }

                    postData.apiType = 'fss';
                    postData.method = 'aircraft';
                    postData.aircraftCD = selectedRowData;
                    postData.aircraftId = 0;
                    postData.IsInActive = $("#chkActiveOnly").is(':checked') ? false : true;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;

                    return postData;
                },
                height: 300,
                width: 550,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: false,
                pager: "#pg_gridPager",
                colNames: ['Type Code', 'Aircraft Name', 'AircraftId'],
                colModel: [
                    { name: 'AircraftCD', index: 'AircraftCD' },
                    { name: 'AircraftDescription', index: 'AircraftDescription' },
                    { name: 'AircraftID', index: 'AircraftID', key: true, hidden: true }

                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);
                },
                onSelectRow: function (id) {
                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['AircraftDescription'];//replace name with any column
                    selectedRowData = $.trim(rowData['AircraftCD']);

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $('#results_table').jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                afterInsertRow: function (rowid, rowObject) {
                    if ($.trim(rowObject.AircraftCD.toLowerCase()) == $.trim(selectedRowData.toLowerCase()) && $.trim(rowObject.AircraftCD) != "") {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        jQuery(jqgridTableId).setSelection(rowid, true);
                    }
                }
            });
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
            $("#pagesizebox").insertBefore('.ui-paging-info');
        });

        function reloadAircraftGrid(aircraftCd) {
            if (aircraftCd != null || aircraftCd != undefined)
            {
                selectedRowData = aircraftCd;
                isopenlookup = true;
            }
            $(jqgridTableId).trigger('reloadGrid');
        }
    </script>
    <script type="text/javascript">

        function returnToParent(rowData) {
            var oArg = new Object();
            oArg.AircraftCD = rowData["AircraftCD"];
            oArg.AircraftDescription = rowData["AircraftDescription"];
            oArg.AircraftID = rowData["AircraftID"];
            oArg.FleetID = rowData["AircraftDescription"] + "(" + rowData["AircraftCD"] + ")";
            oArg.Arg1 = oArg.AircraftCD;
            oArg.CallingButton = "AircraftCD";
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }

    </script>

    <style type="text/css">
        body {
            width: 500px;
            height: 300px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="jqgrid">

            <div>
                <table class="box1">
                    <tr>
                        <td colspan="2">
                            <div>
                                <input type="checkbox" name="Active" value="Active Only" id="chkActiveOnly" checked="checked" />
                                Active Only
                            <input id="btnSearch" class="button" value="Search" type="submit" name="btnSearch" onclick="reloadAircraftGrid(); return false;" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table id="gridAircraft" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                 <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                            </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="Ok"  type="button"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
