﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FleetProfileAdditionalInfo.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Fleet.FleetProfileAdditionalInfo" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Fleet Additional Information</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgPassengerAddInfo.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "FleetInfoCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "FleetProfileAddInfDescription")
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "FleetProfileInformationID")
                    //var cell3 = MasterTable.getCellByColumnUniqueName(row, "AdditionalINFOValue")
                }

                if (selectedRows.length > 0) {
                    oArg.AdditionalINFOCD = cell1.innerHTML;
                    oArg.AdditionalINFODescription = cell2.innerHTML;
                    oArg.AdditionalINFOID = cell3.innerHTML;
                    //oArg.AdditionalINFOValue = cell3.innerHTML;
                }
                else {
                    oArg.AdditionalINFOCD = "";
                    oArg.AdditionalINFODescription = "";
                    oArg.AdditionalINFOID = "";
                    //oArg.AdditionalINFOValue = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
                document.getElementById('<%=btnSub.ClientID%>').click();
            }


            function RowDblClick() {
                var masterTable = $find("<%= dgPassengerAddInfo.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }     
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgPassengerAddInfo" runat="server" AllowSorting="true" Visible="true"
            OnPageIndexChanged="dgAircraftType_PageIndexChanged" OnNeedDataSource="PassengerAddInfo_BindData"
            OnItemCommand="PassengerAddInfo_ItemCommand" OnInsertCommand="PassengerAddInfo_InsertCommand"
            PageSize="10" Width="500px" Height="341px" AllowPaging="true" AllowFilteringByColumn="true"
            PagerStyle-AlwaysVisible="true" AutoGenerateColumns="false" OnPreRender="dgPassengerAddInfo_PreRender">
            <MasterTableView DataKeyNames="FleetProfileInformationID,CustomerID,FleetInfoCD,FleetProfileAddInfDescription,ClientID,LastUpdUID,LastUpdTS,IsDeleted"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="FleetInfoCD" HeaderText="Code" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                        FilterControlWidth="80px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FleetProfileAddInfDescription" HeaderText="Description"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="400px" FilterControlWidth="380px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="AdditionalINFOValue" HeaderText="Additional Information"
                        AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="FleetProfileInformationID" HeaderText="FleetProfileInformationID"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;">
                        <%--                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                        Ok</button>--%>
                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick="RowDblClick" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false"></GroupingSettings>
        </telerik:RadGrid>
        <asp:Label ID="InjectScript" runat="server"></asp:Label>
        <asp:Button ID="btnSub" OnClick="btnSub_Click" Style="display: none" runat="server">
        </asp:Button>
        <br />
        <asp:Label ID="lblMessage" runat="server" CssClass="alert-text" Font-Bold="true"></asp:Label>
    </div>
    </form>
</body>
</html>
