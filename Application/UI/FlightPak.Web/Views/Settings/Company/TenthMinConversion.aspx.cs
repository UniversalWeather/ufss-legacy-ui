﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.Company
{
    public partial class TenthMinConversion : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (Session["ConversionDefaults"] == null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Conversion = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var CompanyData = Conversion.GetConversionTableList().EntityList.Where(x => x.HomebaseID!= null && x.HomebaseID == Convert.ToInt64(Session["HomeBaseID"].ToString())).ToList();
                                    if (CompanyData.Count > 0 && Session["Conversion"] != "Insert")
                                    {

                                        tbTenth0.Text = ((FlightPakMasterService.ConversionTable)CompanyData[0]).Tenths.ToString();
                                        tbStartMin0.Text = ((FlightPakMasterService.ConversionTable)CompanyData[0]).StartMinimum.ToString();
                                        tbEndMin0.Text = ((FlightPakMasterService.ConversionTable)CompanyData[0]).EndMinutes.ToString();

                                        tbTenth1.Text = ((FlightPakMasterService.ConversionTable)CompanyData[1]).Tenths.ToString();
                                        tbStartMin1.Text = ((FlightPakMasterService.ConversionTable)CompanyData[1]).StartMinimum.ToString();
                                        tbEndMin1.Text = ((FlightPakMasterService.ConversionTable)CompanyData[1]).EndMinutes.ToString();

                                        tbTenth2.Text = ((FlightPakMasterService.ConversionTable)CompanyData[2]).Tenths.ToString();
                                        tbStartMin2.Text = ((FlightPakMasterService.ConversionTable)CompanyData[2]).StartMinimum.ToString();
                                        tbEndMin2.Text = ((FlightPakMasterService.ConversionTable)CompanyData[2]).EndMinutes.ToString();

                                        tbTenth3.Text = ((FlightPakMasterService.ConversionTable)CompanyData[3]).Tenths.ToString();
                                        tbStartMin3.Text = ((FlightPakMasterService.ConversionTable)CompanyData[3]).StartMinimum.ToString();
                                        tbEndMin3.Text = ((FlightPakMasterService.ConversionTable)CompanyData[3]).EndMinutes.ToString();

                                        tbTenth4.Text = ((FlightPakMasterService.ConversionTable)CompanyData[4]).Tenths.ToString();
                                        tbStartMin4.Text = ((FlightPakMasterService.ConversionTable)CompanyData[4]).StartMinimum.ToString();
                                        tbEndMin4.Text = ((FlightPakMasterService.ConversionTable)CompanyData[4]).EndMinutes.ToString();

                                        tbTenth5.Text = ((FlightPakMasterService.ConversionTable)CompanyData[5]).Tenths.ToString();
                                        tbStartMin5.Text = ((FlightPakMasterService.ConversionTable)CompanyData[5]).StartMinimum.ToString();
                                        tbEndMin5.Text = ((FlightPakMasterService.ConversionTable)CompanyData[5]).EndMinutes.ToString();

                                        tbTenth6.Text = ((FlightPakMasterService.ConversionTable)CompanyData[6]).Tenths.ToString();
                                        tbStartMin6.Text = ((FlightPakMasterService.ConversionTable)CompanyData[6]).StartMinimum.ToString();
                                        tbEndMin6.Text = ((FlightPakMasterService.ConversionTable)CompanyData[6]).EndMinutes.ToString();

                                        tbTenth7.Text = ((FlightPakMasterService.ConversionTable)CompanyData[7]).Tenths.ToString();
                                        tbStartMin7.Text = ((FlightPakMasterService.ConversionTable)CompanyData[7]).StartMinimum.ToString();
                                        tbEndMin7.Text = ((FlightPakMasterService.ConversionTable)CompanyData[7]).EndMinutes.ToString();

                                        tbTenth8.Text = ((FlightPakMasterService.ConversionTable)CompanyData[8]).Tenths.ToString();
                                        tbStartMin8.Text = ((FlightPakMasterService.ConversionTable)CompanyData[8]).StartMinimum.ToString();
                                        tbEndMin8.Text = ((FlightPakMasterService.ConversionTable)CompanyData[8]).EndMinutes.ToString();

                                        tbTenth9.Text = ((FlightPakMasterService.ConversionTable)CompanyData[9]).Tenths.ToString();
                                        tbStartMin9.Text = ((FlightPakMasterService.ConversionTable)CompanyData[9]).StartMinimum.ToString();
                                        tbEndMin9.Text = ((FlightPakMasterService.ConversionTable)CompanyData[9]).EndMinutes.ToString();

                                    }
                                    else
                                    {
                                        tbTenth0.Text = "0.0";
                                        tbStartMin0.Text = string.Empty;
                                        tbEndMin0.Text = string.Empty;

                                        tbTenth1.Text = "0.1";
                                        tbStartMin1.Text = string.Empty;
                                        tbEndMin1.Text = string.Empty;

                                        tbTenth2.Text = "0.2";
                                        tbStartMin2.Text = string.Empty;
                                        tbEndMin2.Text = string.Empty;

                                        tbTenth3.Text = "0.3";
                                        tbStartMin3.Text = string.Empty;
                                        tbEndMin3.Text = string.Empty;

                                        tbTenth4.Text = "0.4";
                                        tbStartMin4.Text = string.Empty;
                                        tbEndMin4.Text = string.Empty;

                                        tbTenth5.Text = "0.5";
                                        tbStartMin5.Text = string.Empty;
                                        tbEndMin5.Text = string.Empty;

                                        tbTenth6.Text = "0.6";
                                        tbStartMin6.Text = string.Empty;
                                        tbEndMin6.Text = string.Empty;

                                        tbTenth7.Text = "0.7";
                                        tbStartMin7.Text = string.Empty;
                                        tbEndMin7.Text = string.Empty;

                                        tbTenth8.Text = "0.8";
                                        tbStartMin8.Text = string.Empty;
                                        tbEndMin8.Text = string.Empty;

                                        tbTenth9.Text = "0.9";
                                        tbStartMin9.Text = string.Empty;
                                        tbEndMin9.Text = string.Empty;
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<string, string> dictSetConversionDefaults = new Dictionary<string, string>();
                                dictSetConversionDefaults = (Dictionary<string, string>)Session["ConversionDefaults"];
                                tbTenth0.Text = dictSetConversionDefaults["Tenths0"];
                                tbStartMin0.Text = dictSetConversionDefaults["StartMinimum0"];
                                tbEndMin0.Text = dictSetConversionDefaults["EndMinutes0"];
                                tbTenth1.Text = dictSetConversionDefaults["Tenths1"];
                                tbStartMin1.Text = dictSetConversionDefaults["StartMinimum1"];
                                tbEndMin1.Text = dictSetConversionDefaults["EndMinutes1"];
                                tbTenth2.Text = dictSetConversionDefaults["Tenths2"];
                                tbStartMin2.Text = dictSetConversionDefaults["StartMinimum2"];
                                tbEndMin2.Text = dictSetConversionDefaults["EndMinutes2"];
                                tbTenth3.Text = dictSetConversionDefaults["Tenths3"];
                                tbStartMin3.Text = dictSetConversionDefaults["StartMinimum3"];
                                tbEndMin3.Text = dictSetConversionDefaults["EndMinutes3"];
                                tbTenth4.Text = dictSetConversionDefaults["Tenths4"];
                                tbStartMin4.Text = dictSetConversionDefaults["StartMinimum4"];
                                tbEndMin4.Text = dictSetConversionDefaults["EndMinutes4"];
                                tbTenth5.Text = dictSetConversionDefaults["Tenths5"];
                                tbStartMin5.Text = dictSetConversionDefaults["StartMinimum5"];
                                tbEndMin5.Text = dictSetConversionDefaults["EndMinutes5"];
                                tbTenth6.Text = dictSetConversionDefaults["Tenths6"];
                                tbStartMin6.Text = dictSetConversionDefaults["StartMinimum6"];
                                tbEndMin6.Text = dictSetConversionDefaults["EndMinutes6"];
                                tbTenth7.Text = dictSetConversionDefaults["Tenths7"];
                                tbStartMin7.Text = dictSetConversionDefaults["StartMinimum7"];
                                tbEndMin7.Text = dictSetConversionDefaults["EndMinutes7"];
                                tbTenth8.Text = dictSetConversionDefaults["Tenths8"];
                                tbStartMin8.Text = dictSetConversionDefaults["StartMinimum8"];
                                tbEndMin8.Text = dictSetConversionDefaults["EndMinutes8"];

                                tbTenth9.Text = dictSetConversionDefaults["Tenths9"];
                                tbStartMin9.Text = dictSetConversionDefaults["StartMinimum9"];
                                tbEndMin9.Text = dictSetConversionDefaults["EndMinutes9"];
                            }
                            //BindData();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TenthMinConversion);
                }
            }
        }
        //public void BindData()
        //{
        //    FlightPakMasterService.MasterCatalogServiceClient ConversionService = new FlightPakMasterService.MasterCatalogServiceClient();
        //    var Conversionvalue = ConversionService.GetConversionTableList();
        //    if (Conversionvalue.ReturnFlag==true)
        //    {
        //    }
        //}
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //List<string> lstSetConversion = new List<string>();
                        //lstSetConversion.Add(tbTenth0.Text);
                        //lstSetConversion.Add(tbStartMin0.Text);
                        //lstSetConversion.Add(tbEndMin0.Text);
                        Dictionary<string, string> dictSetConversionDefaults = new Dictionary<string, string>();
                        dictSetConversionDefaults.Add("Tenths0", tbTenth0.Text);
                        dictSetConversionDefaults.Add("StartMinimum0", tbStartMin0.Text);
                        dictSetConversionDefaults.Add("EndMinutes0", tbEndMin0.Text);
                        dictSetConversionDefaults.Add("Tenths1", tbTenth1.Text);
                        dictSetConversionDefaults.Add("StartMinimum1", tbStartMin1.Text);
                        dictSetConversionDefaults.Add("EndMinutes1", tbEndMin1.Text);
                        dictSetConversionDefaults.Add("Tenths2", tbTenth2.Text);
                        dictSetConversionDefaults.Add("StartMinimum2", tbStartMin2.Text);
                        dictSetConversionDefaults.Add("EndMinutes2", tbEndMin2.Text);
                        dictSetConversionDefaults.Add("Tenths3", tbTenth3.Text);
                        dictSetConversionDefaults.Add("StartMinimum3", tbStartMin3.Text);
                        dictSetConversionDefaults.Add("EndMinutes3", tbEndMin3.Text);
                        dictSetConversionDefaults.Add("Tenths4", tbTenth4.Text);
                        dictSetConversionDefaults.Add("StartMinimum4", tbStartMin4.Text);
                        dictSetConversionDefaults.Add("EndMinutes4", tbEndMin4.Text);
                        dictSetConversionDefaults.Add("Tenths5", tbTenth5.Text);
                        dictSetConversionDefaults.Add("StartMinimum5", tbStartMin5.Text);
                        dictSetConversionDefaults.Add("EndMinutes5", tbEndMin5.Text);
                        dictSetConversionDefaults.Add("Tenths6", tbTenth6.Text);
                        dictSetConversionDefaults.Add("StartMinimum6", tbStartMin6.Text);
                        dictSetConversionDefaults.Add("EndMinutes6", tbEndMin6.Text);
                        dictSetConversionDefaults.Add("Tenths7", tbTenth7.Text);
                        dictSetConversionDefaults.Add("StartMinimum7", tbStartMin7.Text);
                        dictSetConversionDefaults.Add("EndMinutes7", tbEndMin7.Text);
                        dictSetConversionDefaults.Add("Tenths8", tbTenth8.Text);
                        dictSetConversionDefaults.Add("StartMinimum8", tbStartMin8.Text);
                        dictSetConversionDefaults.Add("EndMinutes8", tbEndMin8.Text);

                        dictSetConversionDefaults.Add("Tenths9", tbTenth9.Text);
                        dictSetConversionDefaults.Add("StartMinimum9", tbStartMin9.Text);
                        dictSetConversionDefaults.Add("EndMinutes9", tbEndMin9.Text);

                        Session.Add("ConversionDefaults", dictSetConversionDefaults);
                        //lbScript.Visible = true;
                        //lbScript.Text = "<script type='text/javascript'>Close()</" + "script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "Close();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TenthMinConversion);
                }
            }
        }
    }
}