﻿<!DOCTYPE  html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head runat="server">
    <title>Department/Authorization</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>

    <script type="text/javascript">

        var selectedRowData = null;
        var departmentId = null;
        var departmentName = null;
        var jqgridTableId = '#gridDepartments';
        var clientId = null;
        var departmentSelected = null;

        $(document).ready(function () {
            clientId = $.trim(unescape(getQuerystring("ClientID", "")));
            
            departmentSelected = $.trim(unescape(getQuerystring("DepartmentCD", "")));
            
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if (selr) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData);
                }
                else
                    showMessageBox('Please select a department.', popupTitle);
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/Views/Settings/Company/DepartmentAuthorizationCatalog.aspx?Catalog=D&IsPopup=Add", popupTitle, 1100, 620, jqgridTableId);
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                departmentId = rowData['DepartmentID'];
                departmentName = rowData['DepartmentName'];
                var isinActive = rowData['IsInactive'];
                var widthDoc = $(document).width();

                if (departmentId != undefined && departmentId != null && departmentId != '' && departmentId != 0) {
                    if (isinActive != 'Yes') {
                        showMessageBox('Active departments cannot be deleted.', popupTitle);
                        return false;
                    }
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a department.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        async: true,
                        type: 'Get',
                        crossDomain: true,
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Department&departmentId=' + departmentId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (content.indexOf('404'))
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                        }
                    });
                }
            }

            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                departmentId = rowData['DepartmentID'];
                var widthDoc = $(document).width();
                if (departmentId != undefined && departmentId != null && departmentId != '' && departmentId != 0) {
                    popupwindow("/Views/Settings/Company/DepartmentAuthorizationCatalog.aspx?Catalog=D&IsPopup=&DepartmentID=" + departmentId, popupTitle, 1100, 620, jqgridTableId);
                    $(jqgridTableId).trigger('reloadGrid');
                }
                else {
                    showMessageBox('Please select a department.', popupTitle);
                }
                return false;
            });

            jQuery("#gridDepartments").jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.showInactive = $("#chkSearchActiveOnly").is(':checked') ? true : false;
                    postData.apiType = 'fss';
                    postData.method = 'departments';
                    postData.clientId = clientId;
                    return postData;
                },

                height: 250,
                width: 750,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                rowNum: 500,
                multiselect: false,
                pager: "#pg_gridPager",
                colNames: ['DepartmentID', 'Department Code', 'Description', 'Inactive', 'Client'],
                colModel: [
                    { name: 'DepartmentID', index: 'DepartmentID', key: true, hidden: true },
                    { name: 'DepartmentCD', index: 'DepartmentCD', width: 50 },
                    { name: 'DepartmentName', index: 'DepartmentName', width: 70 },
                    { name: 'IsInactive', index: 'IsInactive', width: 25, align: "center", editable: true, formatter: "checkbox", formatoptions: { disabled: true }, search: false },
                    { name: 'ClientCD', index: 'ClientCD', width: 50 }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);
                },
                onSelectRow: function (id) {
                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['DepartmentCD'];//replace name with any column
                    
                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $('#results_table').jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                afterInsertRow: function (rowid, rowdata, rowelem) {
                    var lastSel = rowdata['DepartmentCD'];//replace name with any column
                    
                    if ($.trim(departmentSelected) == $.trim(lastSel)) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        $(jqgridTableId).jqGrid('setSelection', rowid);
                    }
                },
                loadComplete:function(rowData){
                    setSelectRow(); 
            }
            });
            $("#pagesizebox").appendTo('#pg_gridPager_left');
            $("#gridDepartments").jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
        });

        function reloadDepartments() {
            $(jqgridTableId).jqGrid().trigger('reloadGrid', [{ page: 1 }]);
        }

        function returnToParent(rowData) {
            var oArg = new Object();
            oArg.DepartmentID = rowData["DepartmentID"];
            oArg.DepartmentCD = rowData["DepartmentCD"];
            oArg.DepartmentName = rowData["DepartmentName"];
            oArg.IsInactive = rowData["IsInactive"];
            oArg.Arg1 = oArg.DepartmentCD;
            oArg.CallingButton = "DepartmentCD";
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }

        function reloadDepartments(id) {
            document.getElementById("hfUpdatedId").value = id;
            var myGrid = $(jqgridTableId);
            nd = id;
            myGrid.trigger('reloadGrid');
        }
        function setSelectRow() {
            if (document.getElementById("hfUpdatedId").value != "") {
                var myGrid = $(jqgridTableId);
                var rowIds = myGrid.jqGrid('getDataIDs');
                for (i = 1; i <= rowIds.length; i++) {
                    var rowData = myGrid.jqGrid('getRowData', rowIds[i]);
                    if (rowData['DepartmentID'] == document.getElementById("hfUpdatedId").value) {
                        myGrid.jqGrid('setSelection', rowIds[i]);
                    }
                }
                document.getElementById("hfUpdatedId").value = "";
            }
        }

    </script>

</head>
<body>
    <form id="form1">
        <div class="jqgrid">
            <div>
                <table class="box1">
                    <tr>
                        <td>
                            <div style="text-align: left; margin-top: -5px">
                                <span>
                                    <input type="checkbox" name="DisplayInactive" value="Display Inactive" id="chkSearchActiveOnly" />
                                    Display Inactive
                                </span>
                                <input id="btnSearch" class="button" value="Search" type="submit" name="btnSearch" onclick="reloadDepartments(); return false;" />
                            </div>
                        </td>
                   </tr>
                   <tr>
                        <td>
                            <table id="gridDepartments" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                 <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                            </div>
                            <div style="text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <input type="hidden" id="hfUpdatedId" />
    </form>
</body>
</html>
