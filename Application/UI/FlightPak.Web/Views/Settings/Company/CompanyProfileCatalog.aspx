﻿<%@ page title="" language="C#" masterpagefile="~/Framework/Masters/Settings.master"
    autoeventwireup="true" codebehind="CompanyProfileCatalog.aspx.cs" inherits="FlightPak.Web.Views.Settings.Company.CompanyProfileCatalog"
    clientidmode="AutoID" maintainscrollpositiononpostback="true" %>

<%@ import namespace="FlightPak.Common.Constants" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">

        var Code = "";
        function validateEmptyNameTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "0";
            }
        }

        function OnClientClick(strPanelToExpand) {
            var PanelBar1 = $find("<%= pnlbargeneralnotes.ClientID %>");
            var PanelBar2 = $find("<%= pnlbarcompanyInformation.ClientID %>");
            var PanelBar3 = $find("<%= pnlbarAPISInterface.ClientID %>");
            var PanelBar4 = $find("<%= pnlbarPreflight.ClientID %>");
            var PanelBar5 = $find("<%=pnlbarDataBase.ClientID %>");
            var PanelBar6 = $find("<%=pnlImage.ClientID %>");
            PanelBar1.get_items().getItem(0).set_expanded(false);
            PanelBar2.get_items().getItem(0).set_expanded(false);
            PanelBar3.get_items().getItem(0).set_expanded(false);
            PanelBar4.get_items().getItem(0).set_expanded(false);
            PanelBar5.get_items().getItem(0).set_expanded(false);
            PanelBar6.get_items().getItem(0).set_expanded(false);
            if (strPanelToExpand == "APIS_Interface") {
                PanelBar3.get_items().getItem(0).set_expanded(true);
            }
            else if (strPanelToExpand == "General_Notes") {
                PanelBar1.get_items().getItem(0).set_expanded(true);
            }
            return false;
        }
        function ValidateEmptyTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "0.00";
            }
        }
        function ValidateEmptyTextboxTime(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "0:00";
            }
        }
        function ValidateEmptyTextbox1(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "W";
            }
        }
        function File_onchange() {
            if (validate())
                __doPostBack('__Page', 'LoadImage');
        }
        function FileCQ_onchange() {
            if (cqvalidate())
                __doPostBack('__Page', 'LoadCQImage');
        }
        function RemoveSpecialCharsNew(elementRef) {
            var checkValue = new String(elementRef.value);
            var newValue = '';
            var isExists = false;
            for (var i = 0; i < checkValue.length; i++) {
                var currentChar = checkValue.charAt(i);
                if ((currentChar == '0') || (currentChar == '1') || (currentChar == '2') || (currentChar == '3') || (currentChar == '4') || (currentChar == '5') ||
                (currentChar == '6') || (currentChar == '7') || (currentChar == '8') || (currentChar == '9') || (currentChar == '!') || (currentChar == '.')) {
                    newValue += currentChar;
                }
                if (checkValue.indexOf(currentChar) > -1) {
                    isExists = true;
                }
            }
            elementRef.value = newValue;
        }
        function validate() {
            var uploadcontrol = document.getElementById('<%=fileUL.ClientID%>').value;
            //var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.[Bb][Mm][Pp])$/;
            var reg = /([^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG])).\2)/gm;
            if (uploadcontrol.length > 0) {
                if (reg.test(uploadcontrol)) {
                    return true;
                }
                else {
                    //alert("Only .bmp files are allowed!");
                    return false;
                }
            }
        }
        function cqvalidate() {
            var uploadcontrol = document.getElementById('<%=fuCQ.ClientID%>').value;
            //var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.[Bb][Mm][Pp])$/;
            var reg = /([^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG])).\2)/gm;
            if (uploadcontrol.length > 0) {
                if (reg.test(uploadcontrol)) {
                    return true;
                }
                else {
                    //alert("Only .bmp files are allowed!");
                    return false;
                }
            }
        }
        function CheckName() {
            var nam = document.getElementById('<%=tbImgName.ClientID%>').value;
            if (nam != "") {
                document.getElementById("<%=fileUL.ClientID %>").disabled = false;
            }
            else {
                document.getElementById("<%=fileUL.ClientID %>").disabled = true;
            }
        }
        function CheckCQName() {
            var nam = document.getElementById('<%=tbCQimgName.ClientID%>').value;
            if (nam != "") {
                document.getElementById("<%=fuCQ.ClientID %>").disabled = false;
            }
            else {
                document.getElementById("<%=fuCQ.ClientID %>").disabled = true;
            }
        }
        function OpenRadWindow() {
            var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
            document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
            oWnd.show();
        }
        function OpenCQRadWindow() {
            var oWnd = $find("<%=RadCQImagePopup.ClientID%>");
            document.getElementById('<%=imgCQPopup.ClientID%>').src = document.getElementById('<%=imgCQ.ClientID%>').src;
            oWnd.show();
        }
        function CloseRadWindow() {
            var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
            document.getElementById('<%=ImgPopup.ClientID%>').src = null;
            oWnd.close();
        }
        function CloseCQRadWindow() {
            var oWnd = $find("<%=RadCQImagePopup.ClientID%>");
            document.getElementById('<%=imgCQPopup.ClientID%>').src = null;
            oWnd.close();
        }
        var typeforClose = '';

        function openWinAccount(type) {
            var url = '';
            typeforClose = type;
            var oManager = $find("<%= RadWindowManager1.ClientID %>");

            if (type == 'federaltax') {
                url = '../Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbFedTaxAccount.ClientID%>').value;

                var oWnd = oManager.open(url, 'radAccountMasterPopup');
                oWnd.add_close(OnClientCloseFederalTaxPopup);

            }
            else if (type == 'statetax') {
                url = '../Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbStateTaxAccount.ClientID%>').value;

                var oWnd = oManager.open(url, 'radAccountMasterPopup');
                oWnd.add_close(OnClientCloseStateTaxPopup);

            }
            else if (type == 'ExchangeRate') {
                url = '../Company/ExchangeRateMasterpopup.aspx?ExchRateCD=' + document.getElementById('<%=tbExchangeRateCode.ClientID%>').value;

                var oWnd = oManager.open(url, 'radExchangePopUp');
                oWnd.add_close(OnClientCloseExchangeRatePopup);

            }
            else {
                url = '../Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbSalesTaxAccount.ClientID%>').value;

                var oWnd = oManager.open(url, 'radAccountMasterPopup');
                oWnd.add_close(OnClientCloseSalesTaxPopup);

            }
}
function openWin(radWin) {
    var url = '';
    if (radWin == "radAirportPopup") {
        url = '../Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbMainBase.ClientID%>').value;
            }
            else if (radWin == "radFederalTaxAccountPopup") {
                url = '../Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbFedTaxAccount.ClientID%>').value;
            }
            else if (radWin == "radStateTaxAccountPopup") {
                url = '../Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbStateTaxAccount.ClientID%>').value;
            }
            else if (radWin == "radSalesTaxAccountPopup") {
                url = '../Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbSalesTaxAccount.ClientID%>').value;
            }
            else if (radWin == "radCrewDutyRulesPopup") {
                url = '../People/CrewDutyRulesPopup.aspx?CrewDutyRuleCD=' + document.getElementById('<%=tbDefaultCrewDutyRules.ClientID%>').value;
            }
            else if (radWin == "RadFlightCategoryPopup") {
                url = '../Fleet/FlightCategoryPopup.aspx?FlightCategoryID=' + document.getElementById('<%=hdnFlightCategoryID.ClientID%>').value;
            }
            else if (radWin == "RadCrewChecklistGroupPopup") {
                url = '../People/CrewChecklistGroupPopup.aspx?CheckGroupCD=' + document.getElementById('<%=tbDefaultChklistGroup.ClientID%>').value;
            }
            else if (radWin == "RadFlightDefaultPurposePopup") {
                url = '../People/FlightPurposePopup.aspx?FDP=Yes&FlightPurposeCD=' + document.getElementById('<%=tbDefaultBlockedSeat.ClientID%>').value;
            }
            else if (radWin == "RadTenthMin") {
                document.getElementById('<%=radOther.ClientID%>').checked = true;
                url = '../Company/TenthMinConversion.aspx';
            }
            else if (radWin == "RadDefaultTripsheetRWGroupPopup") {
                url = '../Company/TripsheetRWGrouppopup.aspx?RWGroupCD=' + document.getElementById('<%=tbDefaultTripsheetRWGroup.ClientID%>').value;
            }
            else if (radWin == "RadDepAuth") {

                url = '../Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=' + document.getElementById("<%=tbCharterDepartment.ClientID%>").value;
            }
            else if (radWin == "RadFlightCategoryPax") {

                url = '/Views/Settings/Fleet/FlightCategoryPopup.aspx?FlightCatagoryCD=' + document.getElementById("<%=tbPax.ClientID%>").value;
            }
            else if (radWin == "RadFlightCategoryNoPax") {
                url = '/Views/Settings/Fleet/FlightCategoryPopup.aspx?FlightCatagoryCD=' + document.getElementById("<%=tbNoPax.ClientID%>").value;
            }

            else if (radWin == "RadCrewDutyrules") {


                url = '/Views/Settings/People/CrewDutyRulesPopup.aspx?CrewDutyRuleCD=' + document.getElementById("<%=tbCQDefaultCrewDutyRules.ClientID%>").value;
            }
            else if (radWin == "RadFEDAccounts") {
                url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById("<%=tbFedAccNum1.ClientID%>").value;
            }
            else if (radWin == "RadFEDAccounts1") {
                url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById("<%=tbFedAccNum2.ClientID%>").value;
            }
            else if (radWin == "RadSegFeeAccounts1") {
                url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById("<%=tbSegFeeAccNum1.ClientID%>").value;
            }
            else if (radWin == "RadSegFeeAccounts2") {
                url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById("<%=tbSegFeeAccNum2.ClientID%>").value;
            }
            else if (radWin == "RadSegFeeAccounts3") {
                url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById("<%=tbSegFeeAccNum3.ClientID%>").value;
            }
            else if (radWin == "RadSegFeeAccounts4") {
                url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById("<%=tbSegFeeAccNum4.ClientID%>").value;
            }

    var oManager = $find("<%= RadWindowManager1.ClientID %>");

            if (radWin == "RadFEDAccounts") {
                Code = "RadFEDAccounts";
                radWin = "radAccountMasterPopup";

                var oWnd = oManager.open(url, radWin);
                oWnd.add_close(OnClientCloseFEDAccountsPopup);
            }
            else if (radWin == "RadFEDAccounts1") {
                Code = "RadFEDAccounts1";
                radWin = "radAccountMasterPopup";

                var oWnd = oManager.open(url, radWin);
                oWnd.add_close(OnClientCloseFEDAccounts1Popup);
            }
            else if (radWin == "RadSegFeeAccounts1") {
                Code = "RadSegFeeAccounts1";
                radWin = "radAccountMasterPopup";

                var oWnd = oManager.open(url, radWin);
                oWnd.add_close(OnClientCloseSegFeeAccounts1Popup);
            }
            else if (radWin == "RadSegFeeAccounts2") {
                Code = "RadSegFeeAccounts2";
                radWin = "radAccountMasterPopup";

                var oWnd = oManager.open(url, radWin);
                oWnd.add_close(OnClientCloseSegFeeAccounts2Popup);
            }
            else if (radWin == "RadSegFeeAccounts3") {
                Code = "RadSegFeeAccounts3";
                radWin = "radAccountMasterPopup";

                var oWnd = oManager.open(url, radWin);
                oWnd.add_close(OnClientCloseSegFeeAccounts3Popup);
            }
            else if (radWin == "RadSegFeeAccounts4") {
                Code = "RadSegFeeAccounts4";
                radWin = "radAccountMasterPopup";

                var oWnd = oManager.open(url, radWin);
                oWnd.add_close(OnClientCloseSegFeeAccounts4Popup);
            }
            else {

                var oWnd = oManager.open(url, radWin);
            }

        }
    </script>
    <script type="text/javascript">
        function fnAllowedChars(myfield, e) {
            var key;
            var keychar;
            var allowedString = "9.!";
            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;
            keychar = String.fromCharCode(key);
            if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                return true;
            else if ((allowedString).indexOf(keychar) > -1)
                return true;
            else
                return false;
        }
        function GenerateExpression() {
            var AccountFormat = document.getElementById('<%=tbAccountFormat.ClientID%>').value;
            var splitdot = AccountFormat.split('');
            var regx = "^";
            for (i = 0; i < splitdot.length; i++) {
                if (splitdot[i] == '9') {
                    regx = regx + "[0-9]";
                }
                if (splitdot[i] == '.') {
                    regx = regx + "[\\. ]";
                }
                if (splitdot[i] == '!') {
                    regx = regx + "[\\! ]";
                }
            }
            regx = regx + "$";
            return regx;
        }
        function ValidateAccountFormat(Control, text) {
            var AccountFormat = document.getElementById('<%=tbAccountFormat.ClientID%>').value;
            if ((AccountFormat != null) && (AccountFormat != undefined) && (AccountFormat != '')) {
                var Expression = GenerateExpression();
                var Pattern = new RegExp(Expression);
                if (Control.value != "") {
                    if (Pattern.exec(Control.value)) {
                        return true;
                    } else {
                        jAlert(text + " No. didn't match the Account format pattern.", "Alert");
                        return false;
                    }
                }
            }
        }
    </script>
    <script type="text/javascript">
        function browserName() {
            var agt = navigator.userAgent.toLowerCase();
            if (agt.indexOf("msie") != -1) return 'Internet Explorer';
            if (agt.indexOf("chrome") != -1) return 'Chrome';
            if (agt.indexOf("opera") != -1) return 'Opera';
            if (agt.indexOf("firefox") != -1) return 'Firefox';
            if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
            if (agt.indexOf("netscape") != -1) return 'Netscape';
            if (agt.indexOf("safari") != -1) return 'Safari';
            if (agt.indexOf("staroffice") != -1) return 'Star Office';
            if (agt.indexOf("webtv") != -1) return 'WebTV';
            if (agt.indexOf("beonex") != -1) return 'Beonex';
            if (agt.indexOf("chimera") != -1) return 'Chimera';
            if (agt.indexOf("netpositive") != -1) return 'NetPositive';
            if (agt.indexOf("phoenix") != -1) return 'Phoenix';
            if (agt.indexOf("skipstone") != -1) return 'SkipStone';
            if (agt.indexOf('\/') != -1) {
                if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                    return navigator.userAgent.substr(0, agt.indexOf('\/'));
                }
                else return 'Netscape';
            } else if (agt.indexOf(' ') != -1)
                return navigator.userAgent.substr(0, agt.indexOf(' '));
            else return navigator.userAgent;
        }
        function GetBrowserName() {
            document.getElementById('<%=hdnBrowserName.ClientID%>').value = browserName();
        }
        function validateHourAndMinute(oSrc, args) {
            var array = args.Value.split(":");
            if (array[0] > 23 && array[1] > 59) {
                args.IsValid = false;
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    oSrc.textContent = "Hour entry must be 0-23; Minute entry must be 0-59";
                } else {
                    oSrc.innerText = "Hour entry must be 0-23; Minute entry must be 0-59";
                }
                if (oSrc.id.indexOf("tbTaxiTimes") != -1) {
                    setTimeout("document.getElementById('<%=rmttbTaxiTimes.ClientID%>').focus()", 0);
                }
                return false;
            }
            else if ((array[0] > 23)) {
                args.IsValid = false;
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    oSrc.textContent = "Hour entry must be 0-23";
                } else {
                    oSrc.innerText = "Hour entry must be 0-23";
                }
                if (oSrc.id.indexOf("tbTaxiTimes") != -1) {
                    setTimeout("document.getElementById('<%=rmttbTaxiTimes.ClientID%>').focus()", 0);
                }
                return false;
            }
            else {
                if (array[1] > 59) {
                    args.IsValid = false;
                    if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                        oSrc.textContent = "Minute entry must be 0-59";
                    } else {
                        oSrc.innerText = "Minute entry must be 0-59";
                    }
                    if (oSrc.id.indexOf("tbTaxiTimes") != -1) {
                        setTimeout("document.getElementById('<%=rmttbTaxiTimes.ClientID%>').focus()", 0);
                    }
                    return false;
                }
            }
    }
    </script>

    <style type="text/css">
        .displayNone {
            display: none !important;
        }
    </style>

</asp:Content>
<asp:content id="Content2" contentplaceholderid="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="divCompanyProfileCatalogForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divCompanyProfileCatalogForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divCompanyProfileCatalogForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }
            function OnClientCloseAirportMasterPopup(oWnd, args) {
                var combo = $find("<%= tbMainBase.ClientID %>");
                //get the transferred argumentss
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbMainBase.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=lbMainBase1.ClientID%>").innerHTML = arg.AirportName;
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=cvMainBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=cvMainBaseCheck.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbMainBase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            //this function is used to display the value of selected Payment code from popup
            function OnClientCloseExchangeRatePopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbExchangeRateCode.ClientID%>").value = arg.ExchangeRateCD;
                        document.getElementById("<%=cvExchangeRateID.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbExchangeRateCode.ClientID%>").value = "";
                        document.getElementById("<%=cvExchangeRateID.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseFederalTaxPopup(oWnd, args) {
                if (typeforClose == 'federaltax') {
                    var combo = $find("<%= tbFedTaxAccount.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbFedTaxAccount.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvFedTaxAccount.ClientID%>").innerHTML = "";
                            ValidateAccountFormat(document.getElementById("<%=tbFedTaxAccount.ClientID%>"), 'Federal Tax Account');
                        }
                        else {
                            document.getElementById("<%=tbFedTaxAccount.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }
            function OnClientCloseCrewDutyRulesPopup(oWnd, args) {
                var combo = $find("<%= tbDefaultCrewDutyRules.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDefaultCrewDutyRules.ClientID%>").value = arg.CrewDutyRuleCD;
                        document.getElementById("<%=hdnCrewDutyRulesID.ClientID%>").value = arg.CrewDutyRulesID;
                        document.getElementById("<%=cvDefaultCrewDutyRules.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbDefaultCrewDutyRules.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseCQCrewDutyrulesPopup(oWnd, args) {
                var combo = $find("<%= tbCQDefaultCrewDutyRules.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCQDefaultCrewDutyRules.ClientID%>").value = arg.CrewDutyRuleCD;
                        document.getElementById("<%=hdnCrewDutyRulesID.ClientID%>").value = arg.CrewDutyRulesID;
                        document.getElementById("<%=cvCqefaultCrewDutyRules.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbDefaultCrewDutyRules.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            } OnClientCloseCQCrewDutyrulesPopup
            function OnClientCloseFlightCategoryPopup(oWnd, args) {
                var combo = $find("<%= tbDefaultBlockedSeat.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDefaultFlightCategory.ClientID%>").value = arg.FlightCatagoryCD;
                        document.getElementById("<%=hdnFlightCategoryID.ClientID%>").value = arg.FlightCategoryID;
                        document.getElementById("<%=cvDefaultFlightCategory.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbDefaultBlockedSeat.ClientID%>").value = "";
                        document.getElementById("<%=hdnFlightCategoryID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseCrewChecklistGroupPopup(oWnd, args) {
                var combo = $find("<%= tbDefaultChklistGroup.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDefaultChklistGroup.ClientID%>").value = arg.CheckGroupCD;
                        document.getElementById("<%=hdnCheckGroupID.ClientID%>").value = arg.CheckGroupID;
                        document.getElementById("<%=cvDefaultChklistGroup.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbDefaultChklistGroup.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseFlightDefaultPurposePopup(oWnd, args) {
                var combo = $find("<%= tbDefaultBlockedSeat.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDefaultBlockedSeat.ClientID%>").value = arg.FlightPurposeCD;
                        document.getElementById("<%=cvDefaultBlockedSeat.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbDefaultBlockedSeat.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseDefaultTripsheetRWGroupPopup(oWnd, args) {
                var combo = $find("<%= tbDefaultTripsheetRWGroup.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDefaultTripsheetRWGroup.ClientID%>").value = arg.ReportID;
                        document.getElementById("<%=cvDefaultTripsheetRWGroup.ClientID%>").innerHTML = "";


                    }
                    else {
                        document.getElementById("<%=tbDefaultTripsheetRWGroup.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnDepAuthClose(oWnd, args) {
                var combo = $find("<%= tbCharterDepartment.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();

                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCharterDepartment.ClientID%>").value = arg.DepartmentCD;
                        document.getElementById("<%=lbCharterDeptInfo.ClientID%>").innerHTML = arg.DepartmentName;
                        document.getElementById("<%=cvDepartmentCode.ClientID%>").innerHTML = "";

                    }
                    else {
                        document.getElementById("<%=tbCharterDepartment.ClientID%>").value = "";
                        document.getElementById("<%=lbCharterDeptInfo.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseFlightCategoryPaxPopup(oWnd, args) {
                var combo = $find("<%= tbPax.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();

                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbPax.ClientID%>").value = arg.FlightCatagoryCD;
                        document.getElementById("<%=tbPax.ClientID%>").value = arg.FlightCatagoryCD;
                        document.getElementById("<%=lbPaxInfo.ClientID%>").innerHTML = arg.FlightCatagoryDescription;
                        document.getElementById("<%=cvtbPax.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbPax.ClientID%>").value = "";
                        document.getElementById("<%=lbPaxInfo.ClientID%>").innerHTML = "";


                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseFlightCategoryNoPaxPopup(oWnd, args) {
                var combo = $find("<%= tbNoPax.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();

                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbNoPax.ClientID%>").value = arg.FlightCatagoryCD;
                        document.getElementById("<%=lbNoPaxInfo.ClientID%>").innerHTML = arg.FlightCatagoryDescription;
                        document.getElementById("<%=cvtbNoPax.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbNoPax.ClientID%>").value = "";
                        document.getElementById("<%=lbNoPaxInfo.ClientID%>").innerHTML = "";


                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseCrewDutyrulesPopup(oWnd, args) {
                var combo = $find("<%= tbDefaultCrewDutyRules.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();

                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDefaultCrewDutyRules.ClientID%>").value = arg.CrewDutyRuleCD;

                    }
                    else {
                        document.getElementById("<%=tbDefaultCrewDutyRules.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseFEDAccountsPopup(oWnd, args) {
                if (Code == "RadFEDAccounts") {
                    var combo = $find("<%= tbFedAccNum1.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();

                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbFedAccNum1.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvFedAccNum1.ClientID%>").innerHTML = "";
                        }
                        else {
                            document.getElementById("<%=tbFedAccNum1.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }
            function OnClientCloseFEDAccounts1Popup(oWnd, args) {
                if (Code == "RadFEDAccounts1") {
                    var combo = $find("<%= tbFedAccNum2.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();

                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbFedAccNum2.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvFedAccNum2.ClientID%>").innerHTML = "";
                        }
                        else {
                            document.getElementById("<%=tbFedAccNum2.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }

            function OnClientCloseSegFeeAccounts1Popup(oWnd, args) {
                if (Code == "RadSegFeeAccounts1") {
                    var combo = $find("<%= tbSegFeeAccNum1.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();

                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbSegFeeAccNum1.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvSegFeeAccNum1.ClientID%>").innerHTML = "";
                        }
                        else {
                            document.getElementById("<%=tbSegFeeAccNum1.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }
            function OnClientCloseSegFeeAccounts2Popup(oWnd, args) {
                if (Code == "RadSegFeeAccounts2") {
                    var combo = $find("<%= tbSegFeeAccNum2.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();

                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbSegFeeAccNum2.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvSegFeeAccNum2.ClientID%>").innerHTML = "";
                        }
                        else {
                            document.getElementById("<%=tbSegFeeAccNum2.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }
            function OnClientCloseSegFeeAccounts3Popup(oWnd, args) {
                if (Code == "RadSegFeeAccounts3") {
                    var combo = $find("<%= tbSegFeeAccNum3.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();

                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbSegFeeAccNum3.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvSegFeeAccNum3.ClientID%>").innerHTML = "";
                        }
                        else {
                            document.getElementById("<%=tbSegFeeAccNum3.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }
            function OnClientCloseSegFeeAccounts4Popup(oWnd, args) {
                if (Code == "RadSegFeeAccounts4") {
                    var combo = $find("<%= tbSegFeeAccNum4.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();

                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbSegFeeAccNum4.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvSegFeeAccNum4.ClientID%>").innerHTML = "";

                        }
                        else {
                            document.getElementById("<%=tbSegFeeAccNum4.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }
            function openWin3() {
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../Company/CrewCurrencyPopup.aspx?EditModeFlag=" + document.getElementById('<%=hdnSaveFlag.ClientID%>').value, "radCrewCurrencyPopup");
            }
            function openWin4() {
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../Company/TripPrivacySettings.aspx?HomeBase=" + document.getElementById('<%=tbMainBase.ClientID%>').value + "&EditModeFlag=" + document.getElementById('<%=hdnSaveFlag.ClientID%>').value, "radTripPrivacySettingPopup");
            }
            function openWin5() {
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                oManager.open("../Company/CustomFlightLog.aspx?HomeBase=" + document.getElementById('<%=tbMainBase.ClientID%>').value + "&EditModeFlag=" + document.getElementById('<%=hdnSaveFlag.ClientID%>').value, "RadCustomFlightLog");

            }
            function openWin6() {
                if (document.getElementById('<%=hdnSaveFlag.ClientID%>').value != "") {

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.open("../Company/CustomPilotLog.aspx?HomeBase=" + document.getElementById('<%=tbMainBase.ClientID%>').value, "RadCustomPilotLog");
                }
            }
            function openWin9() {
                if (document.getElementById('<%=hdnSaveFlag.ClientID%>').value != "") {

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.open("../Company/CompanyQuoteReportDefaults.aspx?HomeBaseID=" + document.getElementById('<%=hdnHmeBaseId.ClientID%>').value, "RadQuoteReport");
                }
            }
            function openWin10() {
                if (document.getElementById('<%=hdnSaveFlag.ClientID%>').value != "") {

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.open("../Company/CompanyInvoiceDefaults.aspx?HomeBaseID=" + document.getElementById('<%=hdnHmeBaseId.ClientID%>').value, "RadInvoiceDefaults");
                }
            }

            function CheckAccountFormat() {
                if (document.getElementById('<%=tbAccountFormat.ClientID%>').value != "") {
                    var ReturnValue = true;
                    if (document.getElementById('<%=tbFedTaxAccount.ClientID%>').value != "") {
                        ReturnValue = ValidateAccountFormat(document.getElementById("<%=tbFedTaxAccount.ClientID%>"), 'Federal Tax Account');
                    }
                    if (ReturnValue == true) {
                        if (document.getElementById('<%=tbStateTaxAccount.ClientID%>').value != "") {
                            ReturnValue = ValidateAccountFormat(document.getElementById("<%=tbStateTaxAccount.ClientID%>"), 'State Tax Account');
                        }
                    }
                    if (ReturnValue == true) {
                        if (document.getElementById('<%=tbSalesTaxAccount.ClientID%>').value != "") {
                            ReturnValue = ValidateAccountFormat(document.getElementById("<%=tbSalesTaxAccount.ClientID%>"), 'Sales Tax Account');
                        }
                    }
                    if (ReturnValue == true) {
                        document.getElementById('<%=btnSaveChanges1.ClientID%>').click();
                    }
                    return false;
                }
                else {
                    document.getElementById('<%=btnSaveChanges1.ClientID%>').click();
                }
            }
            function OnClientCloseStateTaxPopup(oWnd, args) {
                if (typeforClose == 'statetax') {
                    var combo = $find("<%= tbStateTaxAccount.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbStateTaxAccount.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvStateTaxAccount.ClientID%>").innerHTML = "";
                            ValidateAccountFormat(document.getElementById("<%=tbStateTaxAccount.ClientID%>"), 'State Tax Account');
                        }
                        else {
                            document.getElementById("<%=tbStateTaxAccount.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }
            function OnClientCloseSalesTaxPopup(oWnd, args) {
                if (typeforClose == 'salestax') {
                    var combo = $find("<%= tbSalesTaxAccount.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbSalesTaxAccount.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=cvSalesTaxAccount.ClientID%>").innerHTML = "";
                            ValidateAccountFormat(document.getElementById("<%=tbSalesTaxAccount.ClientID%>"), 'Sales Tax Account');
                        }
                        else {
                            document.getElementById("<%=tbSalesTaxAccount.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function lnkTSAProcess_OnClientClick() {
                window.open('TSABatchProcess.aspx', 'TSABatchUploadProcess', 'width=650,height=150,toolbar=no, location=no,directories=no,status=no,menubar=no,scrollbars=no,copyhistory=no,resizable=no,top=' + (screen.height - 150) / 2 + ',left=' + (screen.width - 650) / 2);
                return false;
            }
            function printImage() {
                var image = document.getElementById('<%=ImgPopup.ClientID%>');
                PrintWindow(image);
            }
            function printCQImage() {
                var image = document.getElementById('<%=imgCQPopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlImg = document.getElementById('<%=ddlImg.ClientID%>');
                if (ddlImg.length > 0) {
                    var ImageName = ddlImg.options[ddlImg.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Company Profile");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }
            function ImageCQDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlCQFileName = document.getElementById('<%=ddlCQFileName.ClientID%>');
                if (ddlCQFileName.length > 0) {
                    var ImageName = ddlCQFileName.options[ddlCQFileName.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Company Profile");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }

            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");

                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSaveFlag.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });

            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
            return false;
        }
        function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFederalTaxPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radExchangePopUp" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExchangeRatePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ExchangeRateMasterpopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <%--<Windows>
            <telerik:RadWindow ID="radStateTaxAccountPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseStateTaxPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radSalesTaxAccountPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseSalesTaxPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>--%>
        <Windows>
            <telerik:RadWindow ID="radCrewCurrencyPopup" runat="server" OnClientResizeEnd="GetDimensions"
                CssClass="RadCrewCurrencyPopup" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CrewCurrencyPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radTripPrivacySettingPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Title="Trip Privacy Settings"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/TripPrivacySettings.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCrewDutyRulesPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCrewDutyRulesPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyRulesPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCrewDutyRulesCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFlightCategoryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCrewChecklistGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCrewChecklistGroupPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewChecklistGroupPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCustomFlightLog" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CustomFlightLog.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCustomPilotLog" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Company/CustomPilotLog.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadTenthMin" runat="server" OnClientResizeEnd="GetDimensions"
                Width="400px" AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/TenthMinConversion.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadFlightDefaultPurposePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFlightDefaultPurposePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/FlightPurposePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadDefaultTripsheetRWGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseDefaultTripsheetRWGroupPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/TripsheetRWGrouppopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printImage();return false;" /></div>
                                    <asp:Image ID="ImgPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCQImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Document Image" OnClientClose="CloseCQRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnimgprint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printCQImage();return false;" /></div>
                                    <asp:Image ID="imgCQPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadDepAuth" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnDepAuthClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadFlightCategoryPax" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFlightCategoryPaxPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadFlightCategoryNoPax" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFlightCategoryNoPaxPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCrewDutyrules" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCQCrewDutyrulesPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyRulesPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadFEDAccounts" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFEDAccountsPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadFEDAccounts1" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFEDAccounts1Popup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadSegFeeAccounts1" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseSegFeeAccounts1Popup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadSegFeeAccounts2" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseSegFeeAccounts2Popup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadSegFeeAccounts3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseSegFeeAccounts3Popup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadSegFeeAccounts4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseSegFeeAccounts4Popup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadQuoteReport" runat="server" OnClientResizeEnd="GetDimensions"
                Height="700px" Width="796px" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyQuoteReportDefaults.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadInvoiceDefaults" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Company/CompanyInvoiceDefaults.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Company Profile</span> <span class="tab-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=ConfiguringCompanyProfile"
                        class="help-icon" target="_blank" title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="head-sub-menu">
        <tr>
            <td>
                <div class="tags_select">
                    <asp:LinkButton ID="lnkTSAProcess" runat="server" title="Applies to all Home Bases" Text="TSA Batch Upload" OnClientClick="javascript:return lnkTSAProcess_OnClientClick();" /></div>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="tblspace_10">
                </div>
            </td>
        </tr>
    </table>
    <div id="divCompanyProfileCatalogForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Display Inactive" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadGrid ID="dgCompanyProfileCatalog" runat="server" AllowSorting="true"
                            OnItemCreated="dgCompanyProfileCatalog_ItemCreated" Visible="true" OnNeedDataSource="dgCompanyProfileCatalog_BindData"
                            OnItemCommand="dgCompanyProfileCatalog_ItemCommand" OnUpdateCommand="dgCompanyProfileCatalog_UpdateCommand"
                            OnPageIndexChanged="Budget_PageIndexChanged" OnInsertCommand="dgCompanyProfileCatalog_InsertCommand"
                            OnDeleteCommand="dgCompanyProfileCatalog_DeleteCommand" AutoGenerateColumns="false"
                            Height="341px" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgCompanyProfileCatalog_SelectedIndexChanged"
                            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPreRender="dgCompanyProfileCatalog_PreRender">
                            <MasterTableView DataKeyNames="CustomerID,HomeBaseID,HomeBaseCD,BaseDescription,CrewDutyRuleCD,FlightCatagoryCD,CrewChecklistGroupCD,IsInActive,LastUpdUID,LastUpdTS,HomebaseAirportID"
                                CommandItemDisplay="Bottom">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                                        FilterControlWidth="60px" HeaderStyle-Width="80px" AutoPostBackOnFilter="false"
                                        ShowFilterIcon="false" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BaseDescription" HeaderText="Base Description"
                                        FilterControlWidth="540px" HeaderStyle-Width="560px" AutoPostBackOnFilter="false"
                                        CurrentFilterFunction="StartsWith" ShowFilterIcon="false" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" AllowFiltering="false">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="HomeBaseID" DataType="System.Int64" HeaderText="Home Base"
                                        CurrentFilterFunction="EqualTo" Display="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="LastUpdUID" CurrentFilterFunction="StartsWith"
                                        Display="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="LastUpdTS" CurrentFilterFunction="EqualTo"
                                        Display="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CrewDutyRuleCD" HeaderText="CrewDutyRuleCD" CurrentFilterFunction="StartsWith"
                                        Display="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FlightCatagoryCD" HeaderText="FlightCatagoryCD"
                                        CurrentFilterFunction="StartsWith" Display="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CrewChecklistGroupCD" HeaderText="CrewChecklistGroupCD"
                                        CurrentFilterFunction="StartsWith" Display="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CustomerID" HeaderText="CustomerID" CurrentFilterFunction="EqualTo"
                                        Display="false">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemTemplate>
                                    <div class="grid_icon">
                                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                            CommandName="InitInsert" Visible='<%# IsAuthorized(Permission.Database.AddCompanyProfileCatalog)%>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                            ToolTip="Edit" CssClass="edit-icon-grid" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditCompanyProfileCatalog)%>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnInitDelete" OnClientClick="javascript:return ProcessDelete();"
                                            runat="server" CommandName="DeleteSelected" CssClass="delete-icon-grid" ToolTip="Delete"
                                            Visible='<%# IsAuthorized(Permission.Database.DeleteCompanyProfileCatalog)%>'></asp:LinkButton>
                                    </div>
                                    <div>
                                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                    </div>
                                </CommandItemTemplate>
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlCompanyProfileCatalogForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnAPISInterface" runat="server" CssClass="ui_nav" OnClientClick="javascript:OnClientClick('APIS_Interface'); return false;"
                                        Text="APIS Interface" />
                                </td>
                                <td>
                                    <asp:Button ID="btnGeneralNotes" runat="server" CssClass="ui_nav" OnClientClick=" javascript:OnClientClick('General_Notes'); return false;"
                                        Text="General Notes" />
                                </td>
                                <td>
                                    <asp:Button ID="btndgSave" CssClass="button" runat="server" Text="Save" OnClientClick="return CheckAccountFormat();"
                                        ValidationGroup="Save" TabIndex="135" />
                                </td>
                                <td>
                                    <asp:Button ID="btndgCancel" CssClass="button" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                        TabIndex="136" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadTabStrip ID="TabCompanyProfileCatalog" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                            MultiPageID="RadMultiPage1" SelectedIndex="0" Align="Justify" Width="700px" Style="float: inherit"
                            AutoPostBack="true" CausesValidation="false" ValidationGroup="Save" OnTabClick="TabCompanyProfileCatalog_OnTabClick">
                            <Tabs>
                                <telerik:RadTab Text="Company Information" PageViewID="">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Database">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Preflight">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Postflight">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Charter">                              
                                </telerik:RadTab>
                                <telerik:RadTab Text="Report">
                                </telerik:RadTab>
                            </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="tlk_checkbox">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                <telerik:RadPanelBar ID="pnlbarcompanyInformation" Width="100%" ExpandAnimation-Type="None"
                                    CollapseAnimation-Type="none" runat="server">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                                            <Items>
                                                <telerik:RadPanelItem>
                                                    <ContentTemplate>
                                                        <table class="box1">
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <%--<td class="tdLabel140">
                                                                                <asp:CheckBox ID="chkNoteDisplay" runat="server" Text="Note Display" />
                                                                            </td>--%>
                                                                            <td class="tdLabel150">
                                                                                <asp:CheckBox ID="chkAutoDispatchNumber" runat="server" Text="Auto Dispatch No." />
                                                                            </td>
                                                                            <td class="tdLabel150">
                                                                                <asp:CheckBox ID="chkCanpasspermit" Checked="false" runat="server" AutoPostBack="true"
                                                                                    OnCheckedChanged="chkCanpasspermit_CheckedChanged" Text="CANPASS Permit" />
                                                                            </td>
                                                                            <td class="tdLabel100">
                                                                                <asp:CheckBox ID="chkInactive" runat="server" Text="Inactive" />
                                                                            </td>
                                                                            <td>
                                                                                Default Date Format
                                                                                <asp:DropDownList ID="ddlDefaultDateFormat" runat="server" CssClass="tdLabel120">
                                                                                    <asp:ListItem Text="AMERICAN" Value="AMERICAN"></asp:ListItem>
                                                                                    <asp:ListItem Text="ANSI" Value="ANSI"></asp:ListItem>
                                                                                    <asp:ListItem Text="BRITISH" Value="BRITISH"></asp:ListItem>
                                                                                    <asp:ListItem Text="FRENCH" Value="FRENCH"></asp:ListItem>
                                                                                    <asp:ListItem Text="GERMAN" Value="GERMAN"></asp:ListItem>
                                                                                    <asp:ListItem Text="ITALIAN" Value="ITALIAN"></asp:ListItem>
                                                                                    <asp:ListItem Text="JAPAN" Value="JAPAN"></asp:ListItem>
                                                                                    <asp:ListItem Text="TAIWAN" Value="TAIWAN"></asp:ListItem>
                                                                                    <asp:ListItem Text="MDYY" Value="MDY"></asp:ListItem>
                                                                                    <asp:ListItem Text="DMYY" Value="DMY"></asp:ListItem>
                                                                                    <asp:ListItem Text="YYMD" Value="YMD"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                <asp:CheckBox ID="chkConflictChecking" runat="server" Text="Conflict Checking" />
                                                                            </td>
                                                                            <%--<td class="tdLabel150">
                                                                               <asp:CheckBox ID="chkTextAutotab" runat="server" Text="Text Auto-tab" />
                                                                            </td>--%>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="tblspace_10">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140" valign="top">
                                                                                <span class="mnd_text">Home Base</span>
                                                                            </td>
                                                                            <td class="tdLabel230" valign="top">
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbMainBase" runat="server" CssClass="text80" MaxLength="4"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button ID="btnMainBase" OnClientClick="javascript:openWin('radAirportPopup');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <asp:RequiredFieldValidator ID="rfvMainBase" runat="server" ControlToValidate="tbMainBase"
                                                                                                ValidationGroup="Save" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">MainBase is Required</asp:RequiredFieldValidator>
                                                                                            <asp:Label ID="lbMainBase1" runat="server" CssClass="input_no_bg">
                                                                                            </asp:Label>
                                                                                            <asp:CustomValidator ID="cvMainBase" runat="server" ControlToValidate="tbMainBase"
                                                                                                ErrorMessage="Unique Main Base is Required" Display="Dynamic" CssClass="alert-text"
                                                                                                ValidationGroup="Save" OnServerValidate="cvMainBase_OnServerValidate"></asp:CustomValidator>
                                                                                            <asp:CustomValidator ID="cvMainBaseCheck" runat="server" ControlToValidate="tbMainBase"
                                                                                                ErrorMessage="Invalid ICAO" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                                OnServerValidate="cvMainBaseCheck_OnServerValidate"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td class="tdLabel160" valign="top">
                                                                                <span>Base Description</span>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbBaseDescription" runat="server" CssClass="text180" MaxLength="30"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                Company Name
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbCompanyName" runat="server" CssClass="text180" MaxLength="40"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                Phone
                                                                            </td>
                                                                            <td class="tdLabel230">
                                                                                <asp:TextBox ID="tbPhone" runat="server" CssClass="text180" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel160">
                                                                                Base Fax
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbBaseFax" runat="server" CssClass="text180" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="tblspace_10">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                               <%-- <table width="100%">
                                                                                    <tr>
                                                                                        <td>--%>
                                                                                            Account Format
                                                                                        <%--</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="input_no_bg">
                                                                                            (Only .,!,9 Allowed)
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>--%>
                                                                            </td>
                                                                            <td class="tdLabel230" valign="top" title='(Only ".", "!", and "9" Characters Are Allowed.)'>
                                                                                <asp:TextBox ID="tbAccountFormat" runat="server" CssClass="text180" MaxLength="32"
                                                                                    onBlur="return RemoveSpecialCharsNew(this)" OnKeyPress="return fnAllowedChars(this, event)"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel160">
                                                                                <%--<table width="100%">
                                                                                    <tr>
                                                                                        <td>--%>
                                                                                             Corporate Account Format
                                                                                       <%-- </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="input_no_bg">
                                                                                            (Only .,!,9 Allowed)
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>--%>
                                                                            </td>
                                                                            <td valign="top"  title='(Only ".", "!", and "9" Characters Are Allowed.)'>
                                                                                <asp:TextBox ID="tbCorpAcctFormat" runat="server" CssClass="text180" MaxLength="64"
                                                                                    onBlur="return RemoveSpecialCharsNew(this)" OnKeyPress="return fnAllowedChars(this, event)"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                Federal Tax Account
                                                                            </td>
                                                                            <td class="tdLabel230">
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbFedTaxAccount" runat="server" CssClass="text180" MaxLength="32"
                                                                                                OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')" onblur="javascript:return ValidateAccountFormat(this,'Federal Tax Account');"></asp:TextBox>
                                                                                            <asp:Button ID="btnFedTaxAccount" OnClientClick="javascript:openWinAccount('federaltax');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CustomValidator ID="cvFedTaxAccount" runat="server" ControlToValidate="tbFedTaxAccount"
                                                                                                OnServerValidate="cvFedTaxAccount_OnServerValidate" ErrorMessage="Invalid Account No."
                                                                                                Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td class="tdLabel160">
                                                                                State Tax Account
                                                                            </td>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="tdLabel230">
                                                                                            <asp:TextBox ID="tbStateTaxAccount" runat="server" CssClass="text180" MaxLength="32"
                                                                                                OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')" onblur="javascript:return ValidateAccountFormat(this,'State Tax Account');"></asp:TextBox>
                                                                                            <asp:Button ID="btnStateTaxAccount" OnClientClick="javascript:openWinAccount('statetax');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CustomValidator ID="cvStateTaxAccount" runat="server" ControlToValidate="tbStateTaxAccount"
                                                                                                OnServerValidate="cvStateTaxAccount_OnServerValidate" ErrorMessage="Invalid Account No."
                                                                                                Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                Sales Tax Account
                                                                            </td>
                                                                            <td class="tdLabel230">
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbSalesTaxAccount" runat="server" CssClass="text180" MaxLength="32"
                                                                                                OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')" onblur="javascript:return ValidateAccountFormat(this,'Sales Tax Account');"></asp:TextBox>
                                                                                            <asp:Button ID="btnSalesTaxAccount" OnClientClick="javascript:openWinAccount('salestax');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CustomValidator ID="cvSalesTaxAccount" runat="server" ControlToValidate="tbSalesTaxAccount"
                                                                                                OnServerValidate="cvSalesTaxAccount_OnServerValidate" ErrorMessage="Invalid Account No."
                                                                                                Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td class="tdLabel160">
                                                                                CANPASS Permit No.
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbCanpassPermit" runat="server" Enabled="false" CssClass="text180"
                                                                                    MaxLength="25" OnKeyPress="return fnAllowAlphaNumeric(this,event)"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="tblspace_10">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="60%" style="display: none;">
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        TSA - Cleared File Name
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:FileUpload ID="TSARadClearFile" runat="server" CssClass="text180" OnClientFileSelected="javascript:return TSAClearFile_onchange(this);" />
                                                                                                        <asp:HiddenField ID="hdTSAClear" runat="server" />
                                                                                                        <asp:RegularExpressionValidator ID="rexp" runat="server" ControlToValidate="TSARadClearFile"
                                                                                                            ValidationGroup="Save" CssClass="alert-text" ErrorMessage="Upload Only .CSV file."
                                                                                                            ValidationExpression="(.*\.([Cc][Ss][Vv])$)"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        TSA - No Fly
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:FileUpload ID="TSANoFlyFile" runat="server" CssClass="text180" /><asp:RegularExpressionValidator
                                                                                                            ID="RegularExpressionValidator1" runat="server" ControlToValidate="TSANoFlyFile"
                                                                                                            ValidationGroup="Save" CssClass="alert-text" ErrorMessage="Upload Only .CSV file."
                                                                                                            ValidationExpression="(.*\.([Cc][Ss][Vv])$)"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        TSA - Selectee
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:FileUpload ID="TSASelecteeFile" runat="server" CssClass="text180" /><asp:RegularExpressionValidator
                                                                                                            ID="RegularExpressionValidator2" runat="server" ControlToValidate="TSASelecteeFile"
                                                                                                            ValidationGroup="Save" CssClass="alert-text" ErrorMessage="Upload Only .CSV file."
                                                                                                            ValidationExpression="(.*\.([Cc][Ss][Vv])$)"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td width="40%" valign="top">
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140">
                                                                                            Currency
                                                                                        </td>
                                                                                        <td class="tdLabel230">
                                                                                            <asp:TextBox ID="tbCurrency" runat="server" CssClass="text80" MaxLength="3" OnKeyPress="return fnAllowAlphaNumericAndChar(this, event,'$')"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="tdLabel160">
                                                                                            Distance
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RadioButtonList ID="radlstMilesKilo" runat="server" RepeatDirection="Horizontal"
                                                                                                CellPadding="0" CellSpacing="0">
                                                                                                <asp:ListItem Value="0" Selected="True">Nautical Miles</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Kilometers</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="display: none;">
                                                                                        <td>
                                                                                            Grid Srch
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbGridSrch" runat="server" CssClass="text80" MaxLength="4" onKeyPress="return fnAllowNumeric(this, event,'.')"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkEnableTSA" runat="server" Text="Enable TSA No-Fly Auto Check-in Preflight" />
                                                                            </td>
                                                                            <td>
                                                                                <%--<asp:CheckBox ID="chkIsBlackberrySupport" runat="server" Text="Trip E-mail Notify (Applies to all Home Bases)" />--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:CheckBox ID="chkEnableTSAPAX" runat="server" Text="Enable TSA No-Fly Auto Check-in PAX/Requestor Catalog" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                                Exchange Rate
                                                                            </td>
                                                                            <td class="tdLabel230">
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbExchangeRateCode" runat="server" CssClass="text180" MaxLength="32"></asp:TextBox>
                                                                                            <asp:HiddenField ID="hdnExchangeRateID" runat="server" />
                                                                                            <asp:Button ID="btnExchangeRate" OnClientClick="javascript:openWinAccount('ExchangeRate');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CustomValidator ID="cvExchangeRateID" runat="server" ControlToValidate="tbExchangeRateCode"
                                                                                                OnServerValidate="cvExchangeRateID_OnServerValidate" ErrorMessage="Invalid Exchange Rate Code"
                                                                                                Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td class="tdLabel160">
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                 <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel140">
                                                                               Reset peroid
                                                                            </td>
                                                                            <td class="tdLabel230">
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                           <asp:RadioButton ID="M3" runat="server"   Checked="True" GroupName="peroid"   Text="3 Month" value="3" ></asp:RadioButton>
                                                                                           <asp:RadioButton ID="M6" runat="server"   GroupName="peroid"   Text="6 Month" value="6"></asp:RadioButton>
                                                                                           <asp:RadioButton ID="Y1" runat="server"   GroupName="peroid"   Text="1 Year" value="12"></asp:RadioButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                    
                                                                                </table>
                                                                            </td>
                                                                            <td class="tdLabel160">
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <%--<tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel150">
                                                                                Business Week Start
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="dpBusinessWeekStart" runat="server">
                                                                                    <asp:ListItem Value="0" Selected="True" Text="Select">
                                                                                    </asp:ListItem>
                                                                                    <asp:ListItem Value="1" Text="Sunday">
                                                                                    </asp:ListItem>
                                                                                    <asp:ListItem Value="2" Text="Monday">
                                                                                    </asp:ListItem>
                                                                                    <asp:ListItem Value="3" Text="Tuesday">
                                                                                    </asp:ListItem>
                                                                                    <asp:ListItem Value="4" Text="Wednesday">
                                                                                    </asp:ListItem>
                                                                                    <asp:ListItem Value="5" Text="Thursday">
                                                                                    </asp:ListItem>
                                                                                    <asp:ListItem Value="6" Text="Friday">
                                                                                    </asp:ListItem>
                                                                                    <asp:ListItem Value="7" Text="Saturday">
                                                                                    </asp:ListItem>
                                                                                </asp:DropDownList>--%>
                                                            <%-- <asp:TextBox ID="tbBusinessWeekStart" runat="server" CssClass="text170" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                    MaxLength="1"></asp:TextBox>--%>
                                                            <%-- </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>--%>
                                                        </table>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <telerik:RadPanelBar ID="pnlbarDataBase" Width="100%" ExpandAnimation-Type="None"
                                    CollapseAnimation-Type="none" runat="server">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                                            <Items>
                                                <telerik:RadPanelItem>
                                                    <ContentTemplate>
                                                        <table width="100%" class="box1">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td width="65%">
                                                                                <fieldset>
                                                                                    <legend>PAX/Crew Code Setting</legend>
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td width="50%">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                  <tr> 	
                                                                                                        <td><asp:CheckBox ID="chkAutoPaxcode" AutoPostBack="true" OnCheckedChanged="chkAutoPaxcode_CheckedChanged" runat="server" Text="Auto Paxcode" /></td>
                                                                                                  </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <span class="mnd_text">PAX Name Elements</span>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <span class="mnd_text">Character Selection </span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="tdLabel160">
                                                                                                            <telerik:RadListBox ID="radlstPaxNameElements" runat="server" Width="150px" Height="80px"
                                                                                                                AutoPostBack="true" SelectionMode="Multiple" AllowReorder="true" AutoPostBackOnReorder="true"
                                                                                                                OnReordered="radlstPaxNameElements_Reordered" EnableDragAndDrop="true">
                                                                                                                <Items>
                                                                                                                    <telerik:RadListBoxItem Text="FIRST NAME" Value="FIRST NAME" />
                                                                                                                    <telerik:RadListBoxItem Text="MIDDLE INITIAL" Value="MIDDLE NAME" />
                                                                                                                    <telerik:RadListBoxItem Text="LAST NAME" Value="LAST NAME" />
                                                                                                                </Items>
                                                                                                            </telerik:RadListBox>
                                                                                                        </td>
                                                                                                        <td valign="top" align="left">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbFirstPaxCharacter" runat="server" MaxLength="1" CssClass="text60"
                                                                                                                            AutoPostBack="true" onBlur="return validateEmptyNameTextbox(this, event)" OnTextChanged="tbFirstPaxCharacter_TextChanged"
                                                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbInitialPaxCharacter" runat="server" MaxLength="1" CssClass="text60"
                                                                                                                            AutoPostBack="true" onBlur="return validateEmptyNameTextbox(this, event)" OnTextChanged="tbInitialPaxCharacter_TextChanged"
                                                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbLastPaxCharacter" runat="server" MaxLength="1" CssClass="text60"
                                                                                                                            AutoPostBack="true" onBlur="return validateEmptyNameTextbox(this, event)" OnTextChanged="tbLastPaxCharacter_TextChanged"
                                                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td width="50%">
                                                                                                <table>
                                                                                                       <tr>
                                                                                                            <td><asp:CheckBox ID="chkAutoCrewcode" AutoPostBack="true" OnCheckedChanged="chkAutoCrewcode_CheckedChanged" runat="server" Text="Auto Crewcode" /></td>
                                                                                                        </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <span class="mnd_text">Crew Name Elements</span>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <span class="mnd_text">Character Selection </span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="tdLabel160">
                                                                                                            <telerik:RadListBox ID="radlstCrewNameElement" runat="server" Width="150px" Height="80px"
                                                                                                                SelectionMode="Multiple" AllowReorder="true" AutoPostBackOnReorder="true" EnableDragAndDrop="true"
                                                                                                                OnReordered="radlstCrewNameElement_Reordered">
                                                                                                                <Items>
                                                                                                                    <telerik:RadListBoxItem Text="FIRST NAME" />
                                                                                                                    <telerik:RadListBoxItem Text="MIDDLE INITIAL" />
                                                                                                                    <telerik:RadListBoxItem Text="LAST NAME" />
                                                                                                                </Items>
                                                                                                            </telerik:RadListBox>
                                                                                                        </td>
                                                                                                        <td valign="top" align="left">
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbFirstCrewCharacter" runat="server" MaxLength="1" CssClass="text60"
                                                                                                                            AutoPostBack="true" onBlur="return validateEmptyNameTextbox(this, event)" OnTextChanged="tbFirstCrewCharacter_TextChanged"
                                                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbMiddleCrewCharacter" runat="server" MaxLength="1" CssClass="text60"
                                                                                                                            AutoPostBack="true" onBlur="return validateEmptyNameTextbox(this, event)" OnTextChanged="tbMiddleCrewCharacter_TextChanged"
                                                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbLastCrewCharacter" runat="server" MaxLength="1" CssClass="text60"
                                                                                                                            AutoPostBack="true" onBlur="return validateEmptyNameTextbox(this, event)" OnTextChanged="tbLastCrewCharacter_TextChanged"
                                                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                            <asp:Label ID="lblDays" runat="server" Text="Days" Visible="false"></asp:Label>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="display: none;">
                                                                    <table>
                                                                        <tr>
                                                                            <td class="text166">
                                                                                <asp:Label ID="lbAutoClosureTime" runat="server" Text="Auto Closure Time:"></asp:Label>
                                                                                <telerik:RadMaskedTextBox ID="tbAutoClosureTime" runat="server" SelectionOnFocus="SelectAll"
                                                                                    Mask="<0..23>:<0..59>" CssClass="RadMaskedTextBox50">
                                                                                </telerik:RadMaskedTextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                         <div class="mandatory">
                                                            <span>Note:</span> The Length of PAX/Crew codes can not exceed 5 characters.
                                                          </div>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>                                       
                                    </Items>                                   
                                </telerik:RadPanelBar>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView3" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td>
                                            <telerik:RadPanelBar ID="pnlbarPreflight" Width="100%" ExpandAnimation-Type="None"
                                                CollapseAnimation-Type="none" runat="server">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                                                        <Items>
                                                            <telerik:RadPanelItem>
                                                                <ContentTemplate>
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                                                                                    <tr>
                                                                                        <td class="tags_select">
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <div id="divCrewCurrency" runat="server">
                                                                                                            <asp:LinkButton ID="lnkbtncrew" runat="server" OnClientClick="javascript:openWin3();return false;"
                                                                                                                Text="Crew Currency"></asp:LinkButton>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <div id="divCustom" runat="server">
                                                                                                            <asp:LinkButton ID="lnkCustomFlightLog" runat="server" OnClientClick="javascript:openWin5();return false;"
                                                                                                                Text="Custom Flight Log"></asp:LinkButton>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <div id="divTripPrivacy" runat="server">
                                                                                                            <asp:LinkButton ID="lnkCrewCurrency" runat="server" OnClientClick="javascript:openWin4();return false;"
                                                                                                                Text="Trip Privacy Setting"></asp:LinkButton>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" class="box1">
                                                                                    <tr>
                                                                                        <td valign="top" width="400px">
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td valign="top" class="padtop_10">
                                                                                                        <table width="100%" class="box_tab">
                                                                                                            <%--<tr>
                                                                                                                <td>
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td class="tdLabel140">
                                                                                                                                Flt Cal Timer
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox ID="tbFltCallTimer" runat="server" CssClass="text60" MaxLength="6" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>--%>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td class="tdLabel80">
                                                                                                                                Boeing Winds
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <asp:RadioButtonList ID="radlstBoeingWinds" runat="server" RepeatDirection="Horizontal">
                                                                                                                                    <asp:ListItem Value="1">50%</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="2">75%</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="3" Selected="True">85%</asp:ListItem>
                                                                                                                                </asp:RadioButtonList>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td class="tdLabel80">
                                                                                                                                Ground Time:
                                                                                                                            </td>
                                                                                                                            <td class="tdLabel60">
                                                                                                                                Domestic
                                                                                                                            </td>
                                                                                                                            <td class="tdLabel60">
                                                                                                                                <table cellspacing="0" cellpadding="0">
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <asp:TextBox ID="tbDomestic" runat="server" MaxLength="5" CssClass="text40" onBlur="return ValidateEmptyTextbox(this, event)"
                                                                                                                                                onKeyPress="return fnAllowNumericAndChar(this, event,'.')" Text="0.00"></asp:TextBox>
                                                                                                                                            <asp:TextBox ID="tbDomesticTime" runat="server" MaxLength="5" CssClass="text40" onBlur="return ValidateEmptyTextboxTime(this, event)"
                                                                                                                                                onKeyPress="return fnAllowNumericAndChar(this, event,':')" Text="0:00"></asp:TextBox>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <asp:RegularExpressionValidator ID="revDomestic" runat="server" ControlToValidate="tbDomestic"
                                                                                                                                                EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(00.00)"
                                                                                                                                                Display="Dynamic" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                                                                                                                            <asp:RegularExpressionValidator ID="revDomesticTime" runat="server" ControlToValidate="tbDomesticTime"
                                                                                                                                                EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(00:00)"
                                                                                                                                                Display="Dynamic" ValidationExpression="^(?:[01]?[0-9]|2[0-3]):[0-5][0-9]$"></asp:RegularExpressionValidator>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                            <td class="tdLabel40">
                                                                                                                                Intl
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <asp:TextBox ID="tbIntl" runat="server" MaxLength="5" CssClass="text40" onBlur="return ValidateEmptyTextbox(this, event)"
                                                                                                                                                onKeyPress="return fnAllowNumericAndChar(this, event,'.')" Text="0.00"></asp:TextBox>
                                                                                                                                            <asp:TextBox ID="tbIntlTime" runat="server" MaxLength="5" CssClass="text40" onBlur="return ValidateEmptyTextboxTime(this, event)"
                                                                                                                                                onKeyPress="return fnAllowNumericAndChar(this, event,':')" Text="00:00"></asp:TextBox>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <asp:RegularExpressionValidator ID="revIntl" runat="server" ControlToValidate="tbIntl"
                                                                                                                                                EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(00.00)"
                                                                                                                                                Display="Dynamic" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                                                                                                                            <asp:RegularExpressionValidator ID="revIntlTime" runat="server" ControlToValidate="tbIntlTime"
                                                                                                                                                EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(00:00)"
                                                                                                                                                Display="Dynamic" ValidationExpression="^(?:[01]?[0-9]|2[0-3]):[0-5][0-9]$"></asp:RegularExpressionValidator>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td class="tdLabel80">
                                                                                                                                Time Display
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <asp:RadioButtonList ID="radlstTimeDisplay" runat="server" RepeatDirection="Horizontal"
                                                                                                                                    AutoPostBack="true" OnSelectedIndexChanged="radlstTimeDisplay_CheckedChanged">
                                                                                                                                    <asp:ListItem Value="1" Selected="True">Tenths</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="2">Minutes</asp:ListItem>
                                                                                                                                </asp:RadioButtonList>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td class="tdLabel70">
                                                                                                                                ETE Round
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <asp:RadioButtonList ID="radlstETERound" runat="server" RepeatDirection="Horizontal"
                                                                                                                                    Enabled="false">
                                                                                                                                    <asp:ListItem Value="1">None</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="2">5 M</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="3">10 M</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="4">15 M</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="5">30 M</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="6">1 Hr</asp:ListItem>
                                                                                                                                </asp:RadioButtonList>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td class="tdLabel150">
                                                                                                                                Crew Checklist Alert Days
                                                                                                                            </td>
                                                                                                                            <td class="tdLabel220">
                                                                                                                                <asp:TextBox ID="tbCrewChecklistAlertDays" runat="server" CssClass="text170" MaxLength="3"
                                                                                                                                    OnKeyPress="return  fnAllowNumeric(this,event)"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <fieldset>
                                                                                                            <div style="overflow: auto; height: 262px">
                                                                                                                <table width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:CheckBox ID="chkDeactiveHomeBaseFilter" runat="server" Text="Deactivate Home Base Filter" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkAutoCalcLegTimes" runat="server" Text="Auto Calc Leg Times" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkUseChecklistWarnings" runat="server" Text="Use Checklist Warnings" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkChklistItemsToNewCrewMembers" runat="server" Text="Assign All Checklist Items To New Crew Members" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkDoNotUpdateFuelBrand" runat="server" Text="Do Not Update Fuel Brand" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkAssignAuthDuringRequestorSelection" runat="server" Text="Assign Dept/Auth During Requestor Selection" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkIncludeNonLoggedTripsheetsinCrewHistory" runat="server" Text="Include Non-Logged Tripsheets in Crew History" />
                                                                                                                            <%--<br />
                                                                                                                            <asp:CheckBox ID="chkAutoValidateTripManager" runat="server" Text="Auto Validate Trip" />--%>
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkAutoCreateCrewAvailabilityInfo" runat="server" Text="Auto Create Crew Availability Info" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkShowRequestorsOnly" runat="server" Text="Show Requestors Only" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkAutoPopulateDispatcher" runat="server" Text="Auto Populate Dispatcher" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkAutoRevisionNumber" runat="server" Text="Auto Revision No." />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkHighlightAircraftHavingNotes" runat="server" Text="Highlight Aircraft Having Notes" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkHighlightCrewtHavingNotes" runat="server" Text="Highlight Crew Having Notes and/or Addl Notes" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkHighlightPassengerHavingNotes" runat="server" Text="Highlight Passenger Having Notes and/or PAX Alerts" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkCrewQualificationLegFAR" runat="server" Text="Check Crew Qualification against Leg FAR" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chk24hrTripOverlapforCrew" runat="server" Text="Check 24-Hr Trip Overlap for Crew" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkPassengerOverlap" runat="server" Text="Check Passenger Overlap" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkTailInspectionDue" runat="server" Text="Check Tail Inspection Due" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkPaxPassportExpiry" runat="server" Text="Check if Passenger Passport Expires Within 7 Months" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkCrewPassportExpiry" runat="server" Text="Check if Crew Passport Expires Within 7 Months" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkOpenHistoryOnEditingTrip" runat="server" Text="Open History On Editing Trip" />
                                                                                                                            <br />
                                                                                                                            <asp:CheckBox ID="chkDefaultTripDepartureDate" runat="server" Text="Default Trip Departure Date" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </div>
                                                                                                        </fieldset>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td valign="top">
                                                                                                        <fieldset>
                                                                                                            <legend>Crew Duty </legend>
                                                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td colspan="2">
                                                                                                                        <table width="100%">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    Default Crew Duty Rules
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="tbDefaultCrewDutyRules" runat="server" CssClass="text60"></asp:TextBox>
                                                                                                                                    <asp:Button ID="btnDefaultCrewDutyRules" runat="server" OnClientClick="javascript:openWin('radCrewDutyRulesPopup');return false;"
                                                                                                                                        CssClass="browse-button"></asp:Button>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td colspan="2" align="right">
                                                                                                                                    <asp:CustomValidator ID="cvDefaultCrewDutyRules" runat="server" ControlToValidate="tbDefaultCrewDutyRules"
                                                                                                                                        ErrorMessage="Invalid Crew Duty Rule" Display="Dynamic" CssClass="alert-text"
                                                                                                                                        OnServerValidate="cvDefaultCrewDutyRules_OnServerValidate" ValidationGroup="Save"></asp:CustomValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2">
                                                                                                                        <table width="100%">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    Default FAR Rules
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:RadioButtonList ID="radlstDefaultFAR" runat="server" RepeatDirection="Horizontal">
                                                                                                                                        <asp:ListItem Value="91" Selected="True">91</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="135">135</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="121">121</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="125">125</asp:ListItem>
                                                                                                                                    </asp:RadioButtonList>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </fieldset>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <fieldset>
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        Trip Search Days Back
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbTripManagerSrchDaysBack" runat="server" CssClass="text60" MaxLength="4"
                                                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        Trip Date Warning
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbTripManagerDateWarning" runat="server" CssClass="text60" MaxLength="3"
                                                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        Default Flight Category
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbDefaultFlightCategory" runat="server" CssClass="text60"></asp:TextBox>
                                                                                                                        <asp:Button ID="btnDefaultFlightCategory" runat="server" OnClientClick="javascript:openWin('RadFlightCategoryPopup');return false;"
                                                                                                                            CssClass="browse-button"></asp:Button>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" align="right">
                                                                                                                        <asp:CustomValidator ID="cvDefaultFlightCategory" runat="server" ControlToValidate="tbDefaultFlightCategory"
                                                                                                                            ErrorMessage="Invalid Flight Category" Display="Dynamic" CssClass="alert-text"
                                                                                                                            OnServerValidate="cvDefaultFlightCategory_OnServerValidate" ValidationGroup="Save"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        Default Checklist Group
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbDefaultChklistGroup" runat="server" CssClass="text60"></asp:TextBox>
                                                                                                                        <asp:Button ID="btnDefaultChklistGroup" runat="server" OnClientClick="javascript:openWin('RadCrewChecklistGroupPopup');return false;"
                                                                                                                            CssClass="browse-button"></asp:Button>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" align="right">
                                                                                                                        <asp:CustomValidator ID="cvDefaultChklistGroup" runat="server" ControlToValidate="tbDefaultChklistGroup"
                                                                                                                            ErrorMessage="Invalid Checklist Group" Display="Dynamic" CssClass="alert-text"
                                                                                                                            OnServerValidate="cvDefaultChklistGroup_OnServerValidate" ValidationGroup="Save"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        Trip Status
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="tbTripManagerStatus" runat="server" CssClass="text20" MaxLength="1"
                                                                                                                                        onBlur="return ValidateEmptyTextbox1(this, event)"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:RegularExpressionValidator ID="revTripManagerStatus" runat="server" ControlToValidate="tbTripManagerStatus"
                                                                                                                                        ValidationGroup="Save" ValidationExpression=".*[T]|[W]|[U]|[C]|[H].*" Display="Dynamic"
                                                                                                                                        CssClass="alert-text" ErrorMessage="Should Be 'T''W''U''C''H'"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        Default Tripsheet RW Group
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="tbDefaultTripsheetRWGroup" runat="server" CssClass="text60"></asp:TextBox>
                                                                                                                                    <asp:Button ID="btnDefaultTripsheetRWGroup" runat="server" OnClientClick="javascript:openWin('RadDefaultTripsheetRWGroupPopup');return false;"
                                                                                                                                        CssClass="browse-button"></asp:Button>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:CustomValidator ID="cvDefaultTripsheetRWGroup" runat="server" ControlToValidate="tbDefaultTripsheetRWGroup"
                                                                                                                                        ErrorMessage="Invalid Tripsheet RW Group" Display="Dynamic" CssClass="alert-text"
                                                                                                                                        OnServerValidate="cvDefaultTripsheetRWGroup_OnServerValidate" ValidationGroup="Save"></asp:CustomValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td style="width:100px">
                                                                                                                        GA Desk ID
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbGADeskId" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </fieldset>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <fieldset>
                                                                                                            <legend>Outbound Instruction Labels</legend>
                                                                                                            <table width="280px">
                                                                                                                <tr>
                                                                                                                    <td class="tdLabel10">
                                                                                                                        1
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbOutboundInst1" runat="server" CssClass="text80" MaxLength="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="tdLabel10">
                                                                                                                        2
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbOutboundInst2" runat="server" CssClass="text80" MaxLength="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="tdLabel10">
                                                                                                                        3
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbOutboundInst3" runat="server" CssClass="text80" MaxLength="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdLabel10">
                                                                                                                        4
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbOutboundInst4" runat="server" CssClass="text80" MaxLength="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="tdLabel10">
                                                                                                                        5
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbOutboundInst5" runat="server" CssClass="text80" MaxLength="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="tdLabel10">
                                                                                                                        6
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="tbOutboundInst6" runat="server" CssClass="text80" MaxLength="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        7
                                                                                                                    </td>
                                                                                                                    <td align="left" colspan="5">
                                                                                                                        <asp:TextBox ID="tbOutboundInst7" runat="server" CssClass="text80" MaxLength="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </fieldset>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </telerik:RadPanelItem>
                                                        </Items>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                        </td>
                                    </tr>
                                </table>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td>
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                    runat="server">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Image/logo will print on Tripsheet and Itinerary Reports" CssClass="PanelHeaderStyle">
                                            <ContentTemplate>
                                                <table width="100%" class="box1">
                                                    <tr style="display: none;">
                                                        <td class="tdLabel100">
                                                            Document Name
                                                        </td>
                                                        <td style="vertical-align: top">
                                                            <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text210" ClientIDMode="Static"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="tdLabel370">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:FileUpload ID="fileUL"  runat="server" Enabled="false" ClientIDMode="Static"
                                                                            onchange="javascript:return File_onchange();" />
                                                                        <asp:Label ID="lblError" CssClass="alert-text" runat="server" Style="float: left;">File types must be .jpg, .gif or .bmp</asp:Label>
                                                                        <asp:HiddenField ID="hdnUrl" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="rexpfileUL" runat="server" ControlToValidate="fileUL"
                                                                            ValidationGroup="Save" CssClass="alert-text" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.gif|.GIF|.jpeg|.JPEG|.bmp|.BMP)$"
                                                                            Display="Static"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="tdLabel100">
                                                                                    Logo Position
                                                                                </td>
                                                                                <td>
                                                                                    <asp:RadioButtonList ID="radlstLogoPosition" runat="server" RepeatDirection="Horizontal"
                                                                                        CellPadding="0" CellSpacing="0">
                                                                                        <asp:ListItem Value="0">None</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Center</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Left </asp:ListItem>
                                                                                        <asp:ListItem Value="3">Right </asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" class="tdLabel110">
                                                            <table cellpadding="0px" cellspacing="0px">
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                                                            OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text200"
                                                                            ClientIDMode="Static">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="input_no_bg" style="text-align:justify !important;">
                                                                        <asp:Literal ID="litPFLogoMsg" runat="server"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" class="tdLabel80">
                                                            <asp:Image ID="imgFile" Width="50px" Height="50px" runat="server" OnClick="OpenRadWindow();"
                                                                ClientIDMode="Static" />
                                                        </td>
                                                        <td valign="top" align="right" class="custom_radbutton">
                                                            <telerik:RadButton ID="btncqdeleteImage" runat="server" Text="Delete" Enabled="false"
                                                                OnClientClicking="ImageDeleteConfirm" OnClick="DeleteImage_Click" ClientIDMode="Static" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                                  <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlbargeneralnotes" Width="100%" ExpandAnimation-Type="None"
                            CollapseAnimation-Type="None" runat="server">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="General Notes" CssClass="PanelHeaderStyle">
                                    <Items>
                                        <telerik:RadPanelItem>
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0" class="note-box">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="tbgeneralnotes" runat="server" TextMode="MultiLine" CssClass="textarea-db"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td align="left">
                            <div class="nav-space">
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlbarAPISInterface" Width="100%" ExpandAnimation-Type="None"
                            CollapseAnimation-Type="None" runat="server" CssClass="tlk_checkbox">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="APIS Interface" CssClass="PanelHeaderStyle">
                                    <Items>
                                        <telerik:RadPanelItem>
                                            <ContentTemplate>
                                                <table class="box1">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkEnableAPISInterface" runat="server" Text="Enable APIS Interface" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbURLAddress" Style="display: none" Text="URL/IP Address" runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbURLAddress" Style="display: none" runat="server" CssClass="text180"
                                                                MaxLength="250"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbDomain" Style="display: none" Text="Domain" runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbDomain" Style="display: none" runat="server" CssClass="text180"
                                                                MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkAlertToresubmitTrips" runat="server" Text="Alert to re-submit trips when APIS data is edited" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="tdLabel120">
                                                            Sender ID
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbSenderID" runat="server" MaxLength="15" CssClass="text180">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="tdLabel120">
                                                            CBP User Password
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel190">
                                                                        <asp:TextBox ID="tbCBPUserPassword" runat="server" MaxLength="15" CssClass="text183"
                                                                            TextMode="Password">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnSubmittoUWA" runat="server" Text="Submit to UWA" CssClass="button"
                                                                            OnClick="btnSubmittoUWA_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView4" runat="server">
                                <telerik:RadPanelBar ID="pnlbarPostflight" Width="100%" ExpandAnimation-Type="None"
                                    CollapseAnimation-Type="none" runat="server">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                                            <Items>
                                                <telerik:RadPanelItem>
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                                                            <tr>
                                                                <td>
                                                                    <div class="tags_select" id="divCustompilot" runat="server">
                                                                        <asp:LinkButton ID="lnkbtnCustomPilotLog" runat="server" Text="Custom Pilot Log"
                                                                            OnClientClick="javascript:openWin6();return false;"></asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table width="100%" class="box1">
                                                            <tr>
                                                                <td width="40%">
                                                                    <table width="100%" class="box_tab">
                                                                        <tr>
                                                                            <td class="tdLabel100">
                                                                                Log Fixed Wing
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="radlstLogFixedWing" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Text="Home" Value="1"></asp:ListItem>
                                                                                    <asp:ListItem Text="UTC" Selected="True" Value="2"></asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Log Rotary Wing
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="radlstLogRotaryWing" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Text="Local" Selected="true" Value="1"></asp:ListItem>
                                                                                    <asp:ListItem Text="UTC" Value="2"></asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td width="60%">
                                                                    <table width="100%" class="box_tab">
                                                                        <tr>
                                                                            <td>
                                                                                Postflight Crew Duty Basis
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="radlstPostflightCrewDuty" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Text="Block Out" Selected="True" Value="1"></asp:ListItem>
                                                                                    <asp:ListItem Text="Scheduled Dep." Value="2"></asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Postflight Aircraft Basis
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="radlstPostflightAircraft" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Text="Block Time" Value="1"></asp:ListItem>
                                                                                    <asp:ListItem Text="Flight Time" Selected="true" Value="2"></asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel200">
                                                                                <asp:CheckBox ID="chkAutoPopulateLegTimes" runat="server" Text="Auto Populate Leg Times"
                                                                                    AutoPostBack="true" OnCheckedChanged="chkAutoPopulateLegTimes_checked" />
                                                                            </td>
                                                                            <td class="tdLabel60">
                                                                                Taxi Time
                                                                            </td>
                                                                            <td>
                                                                                <telerik:RadMaskedTextBox ID="rmttbTaxiTimes" runat="server" Enabled="false" SelectionOnFocus="SelectAll"
                                                                                    Mask="<0..99>:<0..99>" class="riTextBox riEnabled">
                                                                                </telerik:RadMaskedTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:CustomValidator ID="reqrmttbTaxiTimes" runat="server" ValidationGroup="Save"
                                                                                    ControlToValidate="rmttbTaxiTimes" CssClass="alert-text" Display="Dynamic" ClientValidationFunction="validateHourAndMinute"
                                                                                    SetFocusOnError="true" ErrorMessage="">
                                                                                </asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>Fuel</legend>
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            Purchase Units
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButtonList ID="radlstFuelPurchaseUnits" runat="server" RepeatDirection="Horizontal">
                                                                                                                <asp:ListItem Text="U.S. Gallons" Selected="true" Value="1"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Liters" Value="2"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Imp. Gallons" Value="3"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Pounds" Value="4"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Kilos" Value="5"></asp:ListItem>
                                                                                                            </asp:RadioButtonList>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            Burn
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButtonList ID="radlstFuelBurn" runat="server" RepeatDirection="Horizontal">
                                                                                                                <asp:ListItem Text="Pounds" Value="1" Selected="true"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Kilos" Value="2"></asp:ListItem>
                                                                                                            </asp:RadioButtonList>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                            <td width="25%" valign="top">
                                                                                <fieldset>
                                                                                    <legend>Fiscal Year</legend>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="tdLabel80">
                                                                                                Start Month
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="ddlStartMonth" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStartMonth_OnSelectedIndexChanged">
                                                                                                    <asp:ListItem Value="0" Selected="True" Text=""></asp:ListItem>
                                                                                                    <asp:ListItem Value="1">Jan</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">Feb</asp:ListItem>
                                                                                                    <asp:ListItem Value="3">Mar</asp:ListItem>
                                                                                                    <asp:ListItem Value="4">Apr</asp:ListItem>
                                                                                                    <asp:ListItem Value="5">May</asp:ListItem>
                                                                                                    <asp:ListItem Value="6">Jun</asp:ListItem>
                                                                                                    <asp:ListItem Value="7">Jul</asp:ListItem>
                                                                                                    <asp:ListItem Value="8">Aug</asp:ListItem>
                                                                                                    <asp:ListItem Value="9">Sep</asp:ListItem>
                                                                                                    <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                                                    <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                                                    <asp:ListItem Value="12">Dec</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                End Month
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="ddlEndMonth" runat="server" Enabled="false">
                                                                                                    <asp:ListItem Value="0" Selected="True" Text=""></asp:ListItem>
                                                                                                    <asp:ListItem Value="1">Jan</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">Feb</asp:ListItem>
                                                                                                    <asp:ListItem Value="3">Mar</asp:ListItem>
                                                                                                    <asp:ListItem Value="4">Apr</asp:ListItem>
                                                                                                    <asp:ListItem Value="5">May</asp:ListItem>
                                                                                                    <asp:ListItem Value="6">Jun</asp:ListItem>
                                                                                                    <asp:ListItem Value="7">Jul</asp:ListItem>
                                                                                                    <asp:ListItem Value="8">Aug</asp:ListItem>
                                                                                                    <asp:ListItem Value="9">Sep</asp:ListItem>
                                                                                                    <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                                                    <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                                                    <asp:ListItem Value="12">Dec</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td width="50%">
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkUseLogsheetWarnings" runat="server" Text="Use Logsheet Warnings" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkLogBlockedSeats" runat="server" Text="Log Blocked Seats" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkRemoveEndDutyMarker" runat="server" Text="Remove End Duty Marker on Last Leg When Adding a New Leg" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkPromptTocalculateSIFL" runat="server" Text="Prompt to Calculate SIFL on Saving Flight Log" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkShowScheduleDateValidation" runat="server" Text="Show Schedule Date Validation" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkAutoFillEngineAirframeHoursandCycles" runat="server" Text="Auto Fill Engine Airframe Hours and Cycles"
                                                                                                OnCheckedChanged="chkAutoFillEngineAirframeHoursandCycles_CheckedChanged" AutoPostBack="true" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkIsPODepartPercentage" runat="server" Text="Postflight Department % Allocation per PAX Leg" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <table width="70%" cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <fieldset>
                                                                                                            <legend>Fill Engine Airframe Hours with</legend>
                                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:RadioButtonList ID="radlstEngineAirframe" runat="server" RepeatDirection="Horizontal">
                                                                                                                            <asp:ListItem Text="Block Hours" Value="1"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="Flight Hours" Value="2"></asp:ListItem>
                                                                                                                        </asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </fieldset>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td valign="top" class="padleft_10 padtop_10" runat="server" id="tdTenthMin">
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <fieldset>
                                                                                                <legend>Tenths-Min Conversion</legend>
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButton ID="radStandard" runat="server" Text="Standard" GroupName="TenMin"
                                                                                                                AutoPostBack="true" ToolTip="Standard:&#xa;0-5=0.0&#xa;6-11=0.1&#xa;12-17=0.2&#xa;18-23=0.3&#xa;24-29=0.4&#xa;30-35=0.5&#xa;36-41=0.6&#xa;42-47=0.7&#xa;48-53=0.8&#xa;54-59=0.9" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButton ID="radFlightPak" runat="server" Text="Flight Scheduling Software" GroupName="TenMin"
                                                                                                                ToolTip="Flight Scheduling Software:&#xa;0-2=0.0&#xa;3-8=0.1&#xa;9-14=0.2&#xa;15-21=0.3&#xa;22-27=0.4&#xa;28-32=0.5&#xa;33-39=0.6&#xa;40-44=0.7&#xa;45-50=0.8&#xa;51-57=0.9&#xa;58-59=1.0" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButton ID="radOther" runat="server" Text="Other" GroupName="TenMin" AutoPostBack="true" />
                                                                                                            <asp:Button ID="btnOther" runat="server" OnClientClick="javascript:openWin('RadTenthMin');return false;"
                                                                                                                CssClass="browse-button"></asp:Button>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </fieldset>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="45%" valign="top">
                                                                                            <table class="padleft_10">
                                                                                                <tr>
                                                                                                    <td class="tdLabel200">
                                                                                                        Default Blocked Seat Flt Purpose
                                                                                                    </td>
                                                                                                    <td class="tdLabel100">
                                                                                                        <asp:TextBox ID="tbDefaultBlockedSeat" CssClass="text40" MaxLength="2" runat="server"></asp:TextBox>
                                                                                                        <asp:Button ID="btnDefaultBlockedSeat" runat="server" OnClientClick="javascript:openWin('RadFlightDefaultPurposePopup');return false;"
                                                                                                            CssClass="browse-button"></asp:Button>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" align="right">
                                                                                                        <asp:CustomValidator ID="cvDefaultBlockedSeat" runat="server" ControlToValidate="tbDefaultBlockedSeat"
                                                                                                            ErrorMessage="Invalid Flight Purpose" Display="Dynamic" CssClass="alert-text"
                                                                                                            OnServerValidate="cvDefaultBlockedSeat_OnServerValidate" ValidationGroup="Save"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        SIFL Layovers Hrs
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbSIFLLayoversHrs" runat="server" CssClass="text40" MaxLength="2"
                                                                                                            OnTextChanged="SIFLLayoversHrs_Textchanged" AutoPostBack="true" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        <asp:CustomValidator ID="cvSIFLLayoversHrs" runat="server" ControlToValidate="tbSIFLLayoversHrs"
                                                                                                            ErrorMessage="Range should between 0 and 24." Display="Dynamic" CssClass="alert-text"
                                                                                                            ValidationGroup="Save"></asp:CustomValidator>
                                                                                                        <%--<asp:RangeValidator ID="rvSIFLLayoversHrs" runat="server" ControlToValidate="tbSIFLLayoversHrs"
                                                                                                            Type="Integer" MinimumValue="1" MaximumValue="24" ValidationGroup="Save" CssClass="alert-text"
                                                                                                            SetFocusOnError="true" ErrorMessage="Range should between 1 and 24." Display="Dynamic" />--%>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        Flight Log Srch Days Back
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbFlightLogSrchDaysBack" runat="server" CssClass="text40" MaxLength="4"
                                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>Crew Log Custom Labels</legend>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="tdLabel60">
                                                                                                Short 1
                                                                                            </td>
                                                                                            <td class="tdLabel110">
                                                                                                <asp:TextBox ID="tbShort1" runat="server" CssClass="text90" MaxLength="10"></asp:TextBox>
                                                                                            </td>
                                                                                            <td class="tdLabel60">
                                                                                                Long 1
                                                                                            </td>
                                                                                            <td colspan="3">
                                                                                                <asp:TextBox ID="tbLong1" runat="server" CssClass="text180" MaxLength="25"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdLabel60">
                                                                                                Short 2
                                                                                            </td>
                                                                                            <td class="tdLabel110">
                                                                                                <asp:TextBox ID="tbShort2" runat="server" CssClass="text90" MaxLength="10"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                Long 2
                                                                                            </td>
                                                                                            <td colspan="3">
                                                                                                <asp:TextBox ID="tbLong2" runat="server" CssClass="text180" MaxLength="25"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                Short 3
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbShort3" runat="server" CssClass="text90" MaxLength="10"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                Long 3
                                                                                            </td>
                                                                                            <td class="tdLabel220">
                                                                                                <asp:TextBox ID="tbLong3" runat="server" CssClass="text180" MaxLength="25"></asp:TextBox>
                                                                                            </td>
                                                                                            <td class="tdLabel40">
                                                                                                Decimal
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbDec1" runat="server" CssClass="text30" placeholder="0" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                                    MaxLength="1"></asp:TextBox>
                                                                                                <asp:RangeValidator ID="rngvDec1" runat="server" ControlToValidate="tbDec1" Type="Integer"
                                                                                                    MinimumValue="0" MaximumValue="3" ValidationGroup="Save" CssClass="alert-text"
                                                                                                    SetFocusOnError="true" ErrorMessage="Range should be between 0 and 3." Display="Dynamic" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                Short 4
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbShort4" runat="server" CssClass="text90" MaxLength="10"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                Long 4
                                                                                            </td>
                                                                                            <td class="tdLabel220">
                                                                                                <asp:TextBox ID="tbLong4" runat="server" CssClass="text180" MaxLength="25"></asp:TextBox>
                                                                                            </td>
                                                                                            <td class="tdLabel40">
                                                                                                Decimal
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbDec2" runat="server" CssClass="text30" placeholder="0" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                                    MaxLength="1"></asp:TextBox>
                                                                                                <asp:RangeValidator ID="rngvDec2" runat="server" ControlToValidate="tbDec2" Type="Integer"
                                                                                                    MinimumValue="0" MaximumValue="3" ValidationGroup="Save" CssClass="alert-text"
                                                                                                    SetFocusOnError="true" ErrorMessage="Range should be between 0 and 3." Display="Dynamic" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="4">
                                                                                            </td>
                                                                                            <td class="srch_category" colspan="2">
                                                                                                Decimal values should be between 0 and 3
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView5" runat="server">
                                <telerik:RadPanelBar ID="pnlbarCharterQuote" Width="100%" ExpandAnimation-Type="None"
                                    CollapseAnimation-Type="none" runat="server">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text=" " CssClass="PanelHeaderStyle">
                                            <Items>
                                                <telerik:RadPanelItem>
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                                                            <tr>
                                                                <td class="tags_select">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <div id="divQuoteInfo" runat="server">
                                                                                    <asp:LinkButton ID="lnkbtnQuoteInfo" runat="server" Text="Quote Info" OnClientClick="javascript:openWin9();return false;"></asp:LinkButton>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div id="divInvoiceInfo" runat="server">
                                                                                    <asp:LinkButton ID="lnkInvoiceInfo" runat="server" Text="Invoice Info" OnClientClick="javascript:openWin10();return false;"></asp:LinkButton>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="box1">
                                                            <tr>
                                                                <td valign="top">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel220">
                                                                                <asp:CheckBox ID="chkAutoRON" runat="server" Text="Activate Auto RON" OnCheckedChanged="chkAutoRON_CheckedChanged"
                                                                                    AutoPostBack="true" />
                                                                            </td>
                                                                            <td class="tdLabel200">
                                                                                <asp:CheckBox ID="chkAutoDateAdjustment" runat="server" Text="Activate Auto Date Adjustment"
                                                                                    AutoPostBack="true" OnCheckedChanged="chkAutoDateAdjustment_CheckedChanged" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkWarningMessage" runat="server" Text="Activate Warning Message" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkDeactivateAutoUpdatingRates" runat="server" Text="Deactivate Auto Updating of Rates" AutoPostBack="true" OnCheckedChanged="chkDeactivateAutoUpdatingRates_CheckedChanged" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkTaxDiscountedTotal" runat="server" Text="Tax Discounted Totals" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkTailNumHomebase" runat="server" Text="Use Tail Home Base for 1st Leg" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td class="tdLabel140">
                                                                                            <asp:Label ID="lbCharterDepartment" runat="server" Text="Charter Department"></asp:Label>
                                                                                        </td>
                                                                                        <td class="tdLabel160">
                                                                                            <asp:TextBox ID="tbCharterDepartment" runat="server" CssClass="text100" MaxLength="8"></asp:TextBox>
                                                                                            <asp:Button ID="btnCharterDepartment" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadDepAuth');return false;" />
                                                                                            <asp:HiddenField ID="hdnDepartmentID" runat="server" />
                                                                                        </td>
                                                                                        <td class="tdLabel190">
                                                                                            <asp:Label ID="lbSearchDaysBack" runat="server" Text="Charter Quote Search Days Back"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbSearchDaysBack" runat="server" CssClass="text40" MaxLength="4"
                                                                                                onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel150">
                                                                                                        <asp:Label ID="lbCharterDeptInfo" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:CustomValidator ID="cvDepartmentCode" runat="server" ControlToValidate="tbCharterDepartment"
                                                                                                            ErrorMessage="Invalid Department Code" Display="Dynamic" CssClass="alert-text"
                                                                                                            OnServerValidate="cvDepartmentCode_OnServerValidate" ValidationGroup="Save"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="lbDefaultCrewDutyRules" runat="server" Text="Default Crew Duty Rule"></asp:Label>
                                                                                            <asp:HiddenField ID="hdnCrewDutyRules" runat="server" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbCQDefaultCrewDutyRules" runat="server" CssClass="text100" MaxLength="4"></asp:TextBox>
                                                                                            <asp:Button ID="btnCqCrewDutyRules" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadCrewDutyrules');return false;" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbMinRONHrs" runat="server" Text="Min RON Hrs"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbMinRONHrs" runat="server" Text="0" CssClass="text40" MaxLength="2"
                                                                                                onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td colspan="3">
                                                                                            <asp:CustomValidator ID="cvCqefaultCrewDutyRules" runat="server" ControlToValidate="tbCQDefaultCrewDutyRules"
                                                                                                ErrorMessage="Invalid Crew Duty Code" Display="Dynamic" CssClass="alert-text"
                                                                                                OnServerValidate="cvCqefaultCrewDutyRules_OnServerValidate" ValidationGroup="Save"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="width: 50%;" valign="top">
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td style="width: 50%;" valign="top">
                                                                                                        <fieldset>
                                                                                                            <legend>Flight Category</legend>
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td class="tdLabel50">
                                                                                                                        <asp:Label ID="lbPax" runat="server" Text="PAX"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td class="tdLabel100">
                                                                                                                        <asp:TextBox ID="tbPax" runat="server" CssClass="text60" MaxLength="4"></asp:TextBox>
                                                                                                                        <asp:Button ID="btnPax" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadFlightCategoryPax');return false;" />
                                                                                                                        <asp:HiddenField ID="hdnPax" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lbPaxInfo" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:CustomValidator ID="cvtbPax" runat="server" ControlToValidate="tbPax" ErrorMessage="Invalid Flight Category Code"
                                                                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" OnServerValidate="cvtbPax_OnServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdLabel50">
                                                                                                                        <asp:Label ID="lbNoPax" runat="server" Text="No PAX"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td class="tdLabel100">
                                                                                                                        <asp:TextBox ID="tbNoPax" runat="server" CssClass="text60" MaxLength="4"></asp:TextBox>
                                                                                                                        <asp:Button ID="btnNoPax" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadFlightCategoryNoPax');return false;" />
                                                                                                                        <asp:HiddenField ID="hdnNoPax" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lbNoPaxInfo" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:CustomValidator ID="cvtbNoPax" runat="server" ControlToValidate="tbNoPax" ErrorMessage="Invalid Flight Category Code"
                                                                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" OnServerValidate="cvtbNoPax_OnServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </fieldset>
                                                                                                    </td>
                                                                                                    <td style="width: 50%;">
                                                                                                        <fieldset>
                                                                                                            <legend>Apply Taxes</legend>
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:RadioButton ID="radAllLegs" runat="server" Text="All Legs" GroupName="checkTax" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:RadioButton ID="radDomesticleg" runat="server" Text="Domestic legs" GroupName="checkTax" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:RadioButton ID="radNone" runat="server" Text="None" GroupName="checkTax" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </fieldset>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width: 50%;" valign="top" align="left">
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td valign="top">
                                                                                                        <div class="radiobuttons-charter company-profile">
                                                                                                            <asp:RadioButton ID="radTaxes" Checked="true" AutoPostBack="true" GroupName="Mode"
                                                                                                                OnCheckedChanged="radTaxes_Checked" runat="server" Text="Taxes" Visible="true">
                                                                                                            </asp:RadioButton>
                                                                                                            <asp:RadioButton ID="radUSSegmentFees" AutoPostBack="true" runat="server" GroupName="Mode"
                                                                                                                OnCheckedChanged="radUSSegmentFees_Checked" Visible="true" Text="U.S. Segment Fees">
                                                                                                            </asp:RadioButton>
                                                                                                        </div>
                                                                                                       
                                                                                                        <div id="DivTaxesForm" runat="server" class="ExternalForm">
                                                                                                            <asp:Panel ID="PanelTaxes" runat="server" Visible="True">
                                                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <table>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbTaxRate" runat="server" Text="Tax Rate"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbAccountNumber" runat="server" Text="Account No."></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbFED" runat="server" Text="Fed/VAT"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <table width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <%--<asp:TextBox ID="tbTaxRate1" runat="server" CssClass="text50" MaxLength="10" AutoPostBack="true"></asp:TextBox>--%>
                                                                                                                                                    <asp:TextBox ID="tbTaxRate1" runat="server" CssClass="text35" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                                                                                        ValidationGroup="Save"></asp:TextBox>
                                                                                                                                                </td>
                                                                                                                                                <td>
                                                                                                                                                    <asp:RegularExpressionValidator ID="revLatitudeMin" runat="server" ControlToValidate="tbTaxRate1"
                                                                                                                                                        ValidationGroup="Save" ErrorMessage="00.00" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"
                                                                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="tbFedAccNum1" runat="server" CssClass="text80" MaxLength="30" OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')"></asp:TextBox>
                                                                                                                                        <asp:Button ID="btnAccNum1" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadFEDAccounts');return false;" />
                                                                                                                                        <asp:HiddenField ID="hdnFedAccNum1" runat="server" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:CustomValidator ID="cvFedAccNum1" runat="server" ControlToValidate="tbFedAccNum1"
                                                                                                                                            ErrorMessage="Invalid Account Number" Display="Dynamic" CssClass="alert-text"
                                                                                                                                            ValidationGroup="Save" OnServerValidate="cvFedAccNum1_OnServerValidate"></asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbRural" runat="server" Text="Rural"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <table width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <%--<asp:TextBox ID="tbTaxRate2" runat="server" CssClass="text50" MaxLength="10" AutoPostBack="true"></asp:TextBox>--%>
                                                                                                                                                    <asp:TextBox ID="tbTaxRate2" runat="server" CssClass="text35" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                                                                                        ValidationGroup="Save"></asp:TextBox>
                                                                                                                                                </td>
                                                                                                                                                <td>
                                                                                                                                                    <asp:RegularExpressionValidator ID="revtbTaxRate2" runat="server" ControlToValidate="tbTaxRate2"
                                                                                                                                                        ValidationGroup="Save" ErrorMessage="00.00" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"
                                                                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="tbFedAccNum2" runat="server" CssClass="text80" MaxLength="30" OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')"></asp:TextBox>
                                                                                                                                        <asp:Button ID="btnAccNum2" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadFEDAccounts1');return false;" />
                                                                                                                                        <asp:HiddenField ID="hdnFedAccNum2" runat="server" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:CustomValidator ID="cvFedAccNum2" runat="server" ControlToValidate="tbFedAccNum2"
                                                                                                                                            ErrorMessage="Invalid Account Number" Display="Dynamic" CssClass="alert-text"
                                                                                                                                            ValidationGroup="Save" OnServerValidate="cvFedAccNum2_OnServerValidate"></asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </asp:Panel>
                                                                                                        </div>
                                                                                                         <div>
                                                                                                            <asp:Label ID="lbUserWarning" runat="server" CssClass="SIFL_Rate_text company-profile" Text="User shall be responsible for providing and maintaining Tax and U.S Segment Fees"></asp:Label>
                                                                                                        </div>
                                                                                                        <div id="DivUSTaxesForm" runat="server" class="ExternalForm">
                                                                                                            <asp:Panel ID="pnlUSTaxesForm" runat="server" Visible="True">
                                                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <table>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbFee" runat="server" Text="Fee"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbSegFeeAccountNumber" runat="server" Text="Account Number"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbDom" runat="server" Text="Dom"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <table width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <%--<asp:TextBox ID="tbFee1" runat="server" CssClass="text50" MaxLength="4" AutoPostBack="true"></asp:TextBox>--%>
                                                                                                                                                    <asp:TextBox ID="tbFee1" runat="server" CssClass="text35" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                                                                                        ValidationGroup="Save"></asp:TextBox>
                                                                                                                                                </td>
                                                                                                                                                <td>
                                                                                                                                                    <asp:RegularExpressionValidator ID="revtbFee1" runat="server" ControlToValidate="tbFee1"
                                                                                                                                                        ValidationGroup="Save" ErrorMessage="00.00" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"
                                                                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="tbSegFeeAccNum1" runat="server" CssClass="text80" MaxLength="30"
                                                                                                                                            OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')" OnTextChanged="tbSegFeeAccNum1_TextChanged"
                                                                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                                                                        <asp:Button ID="btnSegFeeAccNum1" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadSegFeeAccounts1');return false;" />
                                                                                                                                        <asp:HiddenField ID="hdnSegFeeAccNum1" runat="server" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:CustomValidator ID="cvSegFeeAccNum1" runat="server" ControlToValidate="tbSegFeeAccNum1"
                                                                                                                                            ErrorMessage="Invalid Account Number" Display="Dynamic" CssClass="alert-text"
                                                                                                                                            ValidationGroup="Save"></asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbIntl" runat="server" Text="Intl"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <table width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <%--<asp:TextBox ID="tbFee2" runat="server" CssClass="text50" MaxLength="4" AutoPostBack="true"></asp:TextBox>--%>
                                                                                                                                                    <asp:TextBox ID="tbFee2" runat="server" CssClass="text35" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                                                                                        ValidationGroup="Save"></asp:TextBox>
                                                                                                                                                </td>
                                                                                                                                                <td>
                                                                                                                                                    <asp:RegularExpressionValidator ID="revtbFee2" runat="server" ControlToValidate="tbFee2"
                                                                                                                                                        ValidationGroup="Save" ErrorMessage="00.00" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"
                                                                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="tbSegFeeAccNum2" runat="server" CssClass="text80" MaxLength="30"
                                                                                                                                            OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')" OnTextChanged="tbSegFeeAccNum2_TextChanged"
                                                                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                                                                        <asp:Button ID="btnSegFeeAccNum2" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadSegFeeAccounts2');return false;" />
                                                                                                                                        <asp:HiddenField ID="hdnSegFeeAccNum2" runat="server" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:CustomValidator ID="cvSegFeeAccNum2" runat="server" ControlToValidate="tbSegFeeAccNum2"
                                                                                                                                            ErrorMessage="Invalid Account Number" Display="Dynamic" CssClass="alert-text"
                                                                                                                                            ValidationGroup="Save"></asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbAlaska" runat="server" Text="Alaska"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <table width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <%--<asp:TextBox ID="tbFee3" runat="server" CssClass="text50" MaxLength="4" AutoPostBack="true"></asp:TextBox>--%>
                                                                                                                                                    <asp:TextBox ID="tbFee3" runat="server" CssClass="text35" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                                                                                        ValidationGroup="Save"></asp:TextBox>
                                                                                                                                                </td>
                                                                                                                                                <td>
                                                                                                                                                    <asp:RegularExpressionValidator ID="revtbFee3" runat="server" ControlToValidate="tbFee3"
                                                                                                                                                        ValidationGroup="Save" ErrorMessage="00.00" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"
                                                                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="tbSegFeeAccNum3" runat="server" CssClass="text80" MaxLength="30"
                                                                                                                                            OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')" OnTextChanged="tbSegFeeAccNum3_TextChanged"
                                                                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                                                                        <asp:Button ID="btnSegFeeAccNum3" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadSegFeeAccounts3');return false;" />
                                                                                                                                        <asp:HiddenField ID="hdnSegFeeAccNum3" runat="server" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:CustomValidator ID="cvSegFeeAccNum3" runat="server" ControlToValidate="tbSegFeeAccNum3"
                                                                                                                                            ErrorMessage="Invalid Account Number" Display="Dynamic" CssClass="alert-text"
                                                                                                                                            ValidationGroup="Save"></asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="lbHawaii" runat="server" Text="Hawaii"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <table width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <%--<asp:TextBox ID="tbFee4" runat="server" CssClass="text50" MaxLength="4" AutoPostBack="true"></asp:TextBox>--%>
                                                                                                                                                    <asp:TextBox ID="tbFee4" runat="server" CssClass="text35" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                                                                                        ValidationGroup="Save"></asp:TextBox>
                                                                                                                                                </td>
                                                                                                                                                <td>
                                                                                                                                                    <asp:RegularExpressionValidator ID="revtbFee4" runat="server" ControlToValidate="tbFee4"
                                                                                                                                                        ValidationGroup="Save" ErrorMessage="00.00" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"
                                                                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="tbSegFeeAccNum4" runat="server" CssClass="text80" MaxLength="30"
                                                                                                                                            OnKeyPress="return  fnAllowNumericAndChar(this,event,'.,!')" OnTextChanged="tbSegFeeAccNum4_TextChanged"
                                                                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                                                                        <asp:Button ID="btnSegFeeAccNum4" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('RadSegFeeAccounts4');return false;" />
                                                                                                                                        <asp:HiddenField ID="hdnSegFeeAccNum4" runat="server" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:CustomValidator ID="cvSegFeeAccNum4" runat="server" ControlToValidate="tbSegFeeAccNum4"
                                                                                                                                            ErrorMessage="Invalid Account Number" Display="Dynamic" CssClass="alert-text"
                                                                                                                                            ValidationGroup="Save"></asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </asp:Panel>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Image/logo will print on CQRW-Itinerary" CssClass="PanelHeaderStyle">
                                            <ContentTemplate>
                                                <table width="100%" class="box1">
                                                    <tr style="display: none;">
                                                        <td class="tdLabel100">
                                                            Document Name
                                                        </td>
                                                        <td style="vertical-align: top">
                                                            <asp:TextBox ID="tbCQimgName" MaxLength="20" runat="server" CssClass="text210" OnBlur="CheckCQName();"
                                                                ClientIDMode="Static"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="tdLabel370">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:FileUpload ID="fuCQ" runat="server" Enabled="false" ClientIDMode="Static" onchange="javascript:return FileCQ_onchange();" />
                                                                        <asp:Label ID="lbCQDocum" CssClass="alert-text" runat="server" Style="float: left;">File types must be .jpg, .gif or .bmp</asp:Label>
                                                                        <asp:HiddenField ID="hdnCQDocument" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="revCQImg" runat="server" ControlToValidate="fuCQ"
                                                                            ValidationGroup="Save" CssClass="alert-text" ValidationExpression="(.*\.([Gg][Ii][Ff])|.*\.([Jj][Pp][Gg])|.*\.([Bb][Mm][Pp])|.*\.([jJ][pP][eE][gG])$)"
                                                                            Display="Static"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="tdLabel100">
                                                                                    Logo Position
                                                                                </td>
                                                                                <td>
                                                                                    <asp:RadioButtonList ID="RdlstCqPosition" runat="server" RepeatDirection="Horizontal"
                                                                                        CellPadding="0" CellSpacing="0">
                                                                                        <asp:ListItem Value="0">None</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Center</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Left </asp:ListItem>
                                                                                        <asp:ListItem Value="3">Right </asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" class="tdLabel110">
                                                            <table cellpadding="0px" cellspacing="0px">
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCQFileName" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                                                            AutoPostBack="true" CssClass="text200" ClientIDMode="AutoID" OnSelectedIndexChanged="ddlCQFileName_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <td class="input_no_bg" style="text-align:justify !important;">
                                                                    <asp:Literal ID="litCQLogoMsg" runat="server"></asp:Literal>
                                                                </td>
                                                            </table>
                                                        </td>
                                                        <td valign="top" class="tdLabel80">
                                                            <asp:Image ID="imgCQ" Width="50px" Height="50px" runat="server" OnClick="OpenCQRadWindow();"
                                                                AlternateText="" ClientIDMode="Static" />
                                                        </td>
                                                        <td valign="top" align="right" class="custom_radbutton">
                                                            <telerik:RadButton ID="radbtnCQDelete" runat="server" Text="Delete" Enabled="true"
                                                                ClientIDMode="Static" OnClientClicking="ImageCQDeleteConfirm" OnClick="btncqdeleteImage_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </telerik:RadPageView>                           
                            <telerik:RadPageView ID="RadPageView6" runat="server">
                                <telerik:RadPanelBar ID="RadPanelBar1" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="none"
                                    runat="server">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                                            <Items>
                                                <telerik:RadPanelItem>
                                                    <ContentTemplate>
                                                        <table class="box1">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkTimeStampReports" runat="server" Text="Date/Timestamp Reports" />
                                                                </td>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lbPrintFormat" CssClass="mnd_text" runat="server" Text="Print/Format"></asp:Label>
                                                                                <asp:RadioButton ID="rbMonthDay" runat="server" Text="Month/Day" Checked="true" GroupName="Print" />
                                                                                <asp:RadioButton ID="rbDayMonth" runat="server" Text="Day/Month" GroupName="Print" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                Suppress Zero Activity On the Following Reports:
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkAircraft" runat="server" Text="Aircraft" />
                                                                                <asp:CheckBox ID="chkCrew" runat="server" Text="Crew" />
                                                                                <asp:CheckBox ID="chkDept" runat="server" Text="Dept" />
                                                                                <asp:CheckBox ID="chkPax" runat="server" Text="PAX" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td valign="top">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkPrintUWAReports" runat="server" Text="Print UWA Logo on Reports" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="left">
                                                                    Report Message:
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="left">
                                                                    <asp:TextBox ID="tbReportMessage" Text=" *** Your Custom Message Goes Here ***" runat="server"
                                                                        MaxLength="100" CssClass="text400"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
          
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" CssClass="button" runat="server" Text="Save" OnClick="SaveChanges_Click"
                            OnClientClick="return CheckAccountFormat();" ValidationGroup="Save" TabIndex="184" />
                        <asp:Button ID="btnSaveChanges1" CssClass="button" Text="Save" runat="server" Style="display: none;"
                            OnClick="SaveChanges_Click" ValidationGroup="Save" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" CausesValidation="false"
                            runat="server" OnClick="btnCancel_Click" TabIndex="185" />
                        <asp:HiddenField ID="hdnSaveFlag" runat="server" />
                        <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                        <asp:HiddenField ID="hdnFlightCategoryID" runat="server" />
                        <asp:HiddenField ID="hdnCrewDutyRulesID" runat="server" />
                        <asp:HiddenField ID="hdnCheckGroupID" runat="server" />
                        <asp:HiddenField ID="hdnCompanyID" runat="server" />
                        <asp:HiddenField ID="hdnCrewOFirst" runat="server" />
                        <asp:HiddenField ID="hdnCrewOMiddle" runat="server" />
                        <asp:HiddenField ID="hdnCrewOLast" runat="server" />
                        <asp:HiddenField ID="hdnPaxOFirst" runat="server" />
                        <asp:HiddenField ID="hdnPaxOMiddle" runat="server" />
                        <asp:HiddenField ID="hdnPaxOLast" runat="server" />
                        <asp:HiddenField ID="hdnDefaultBlockedSeat" runat="server" />
                        <asp:HiddenField ID="hdnDefaultTripsheetRWGroup" runat="server" />
                        <asp:HiddenField ID="hdnBrowserName" runat="server" />
                        <asp:HiddenField ID="hdnHmeBaseId" runat="server" />
                        <asp:Button ID="btndeafaultCrewduty" runat="server" OnClick="DefaultCrewDutyRules_Click"
                            Style="display: none;" CssClass="button-disable" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:content>
