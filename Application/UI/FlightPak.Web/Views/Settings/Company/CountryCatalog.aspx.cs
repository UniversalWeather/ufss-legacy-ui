﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Drawing;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exceptioon Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class CountryCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private bool CountryCatalogPageNavigated = false;
        private string strCountryId = "";
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCountry, dgCountry, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCountry.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (UserPrincipal.Identity._fpSettings._IsAPISSupport != null)
                        {
                            if (UserPrincipal.Identity._fpSettings._IsAPISSupport == true)
                            {
                                lbISOCode.CssClass = "important-text";
                            }
                            else
                            {
                                lbISOCode.ForeColor = System.Drawing.Color.Black;
                            }
                        }
                        else
                        {
                            lbISOCode.ForeColor = System.Drawing.Color.Black;
                        }
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewCountryCatalog);
                            
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgCountry.Rebind();
                                ReadOnlyForm();                               
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else { DefaultSelection(true); }


                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (BindDataSwitch)
                        {
                            dgCountry.Rebind();
                        }
                        if (Session["SearchItemPrimaryKeyValue"] != null)
                        {
                            ReadOnlyForm();
                        }
                        else if (dgCountry.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedCountryID"] = null;
                            //}
                            if (Session["SelectedCountryID"] == null)
                            {
                                dgCountry.SelectedIndexes.Add(0);
                                Session["SelectedCountryID"] = dgCountry.Items[0].GetDataKeyValue("CountryID").ToString();
                            }

                            if (dgCountry.SelectedIndexes.Count == 0)
                                dgCountry.SelectedIndexes.Add(0);

                            ReadOnlyForm();
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                        }
                        // GridEnable(true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// To select the selected item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCountryID"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                        //string code = items.GetDataKeyValue("DelayTypeCD").ToString();
                        string ID = Session["SelectedCountryID"].ToString();
                        foreach (GridDataItem item in dgCountry.MasterTableView.Items)
                        {
                            if (item.GetDataKeyValue("CountryID").ToString().Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCountry_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCountry.ClientSettings.Scrolling.ScrollTop = "0";
                        CountryCatalogPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CountryCatalogPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCountry, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton lbtnEditButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, divExternalForm, RadAjaxLoadingPanel1);
                            LinkButton lbtnInsertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, divExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Bind Country Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objCountryVal = CountryService.GetCountryMasterList();
                            if (objCountryVal.ReturnFlag == true)
                            {
                                dgCountry.DataSource = objCountryVal.EntityList;
                            }
                            Session["CountryCodes"] = objCountryVal.EntityList.ToList();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// Item Command for Country Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                tbCode.CssClass = "text50 inpt_non_edit";
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                hdnRedirect.Value = "";
                                if (Session["SelectedCountryID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Country, Convert.ToInt64(Session["SelectedCountryID"].ToString().Trim()));
                                        Session["IsCountryEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Country);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        //GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        SelectItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                tbCode.ReadOnly = false;
                                DisplayInsertForm();
                                //GridEnable(true, false, false);
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// Update Command for Country Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCountry_UpdateCommand(object source, GridCommandEventArgs e)
        {
            e.Canceled = true;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedCountryID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objCountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetVal = objCountryService.UpdateCountryMaster(GetItems());
                                ///////Update Method UnLock
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Country, Convert.ToInt64(Session["SelectedCountryID"].ToString().Trim()));
                                }
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                //GridEnable(true, true, true);
                                DefaultSelection(true);
                                if (RetVal.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// Function to Check if Country Type Code Already Exists
        /// </summary>
        /// <returns></returns>
        private Boolean CheckAllReadyExist()
        {
            bool ReturnFlag = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<Country> DelayCodeList = new List<Country>();
                    DelayCodeList = ((List<Country>)Session["CountryCodes"]).Where(x => x.CountryCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<Country>();
                    if (DelayCodeList.Count != 0)
                    {
                        cvCode.IsValid = false;
                        tbCode.Focus();
                        ReturnFlag = true;
                    }
                    return ReturnFlag;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnFlag;
            }
        }
        /// <summary>
        /// To enable the items in the grid
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        ///Commenting this method Since there is no Add,Edit ,Delete functionality for this catalog.Always view.

        //private void GridEnable(bool Add, bool Edit, bool Delete)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
        //    {
        //        //Handle methods throguh exception manager with return flag
        //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //        exManager.Process(() =>
        //        {
        //            LinkButton lbtnInsertCtl = (LinkButton)dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
        //            LinkButton lbtnDelCtl = (LinkButton)dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
        //            LinkButton lbtnEditCtl = (LinkButton)dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
        //            if (IsAuthorized(Permission.Database.AddCountryCatalog))
        //            {
        //                lbtnInsertCtl.Visible = true;
        //                if (Add)
        //                {
        //                    lbtnInsertCtl.Enabled = true;
        //                }
        //                else
        //                {
        //                    lbtnInsertCtl.Enabled = false;
        //                }
        //            }
        //            else
        //            {
        //                lbtnInsertCtl.Visible = false;
        //            }
        //            if (IsAuthorized(Permission.Database.EditCountryCatalog))
        //            {
        //                lbtnEditCtl.Visible = true;
        //                if (Edit)
        //                {
        //                    lbtnEditCtl.Enabled = true;
        //                    lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
        //                }
        //                else
        //                {
        //                    lbtnEditCtl.Enabled = false;
        //                    lbtnEditCtl.OnClientClick = string.Empty;
        //                }
        //            }
        //            else
        //            {
        //                lbtnEditCtl.Visible = false;
        //            }
        //        }, FlightPak.Common.Constants.Policy.UILayer);
        //    }
        //}
        /// <summary>
        /// To Display the read only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    GridDataItem Item = dgCountry.SelectedItems[0] as GridDataItem;
                    tbCode.Text = Convert.ToString(Item.GetDataKeyValue("CountryCD"));//.ToString();
                    if (Item.GetDataKeyValue("CountryName") != null)
                    {
                        tbDescription.Text = Item.GetDataKeyValue("CountryName").ToString();
                    }
                    else
                    {
                        tbDescription.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("ISOCD") != null)
                    {
                        tbIsoCode.Text = Item.GetDataKeyValue("ISOCD").ToString();
                    }
                    else
                    {
                        tbIsoCode.Text = string.Empty;
                    }
                    Label lbLastUpdatedUser = (Label)dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");

                    lbLastUpdatedUser.Text = "Last Updated";
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " " + "User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }

                    if (Item.GetDataKeyValue("LastUpdDT") != null)
                    {
                        lbLastUpdatedUser.Text = HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdDT").ToString().Trim())));
                    }
                    Item.Selected = true;
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        ///Enable the controls in the form
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = Enable;
                    tbDescription.Enabled = Enable;
                    tbIsoCode.Enabled = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChanges.Visible = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Update Command for Country Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCountry_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient CountryTypeService = new MasterCatalogServiceClient())
                            {
                                var RetVal = CountryTypeService.AddCountryMaster(GetItems());
                                dgCountry.Rebind();
                                DefaultSelection(false);
                                if (RetVal.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgCountry.SelectedItems[0] as GridDataItem;
                                Session["SelectedCountryID"] = item.GetDataKeyValue("CountryID");
                                if (btnSaveChanges.Visible == false)
                                {
                                    ReadOnlyForm();
                                }
                            }
                            //  GridEnable(true, true, true);
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                    }
                }
            }
        }
        /// <summary>
        /// Clear the Form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbIsoCode.Text = string.Empty;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert  form when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbCode.ReadOnly = false;
                    tbDescription.ReadOnly = false;
                    tbIsoCode.Enabled = false;
                    tbIsoCode.ReadOnly = true;
                    ClearForm();
                    dgCountry.Rebind();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Country Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// Cancel Country Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedCountryID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Country, Convert.ToInt64(Session["SelectedCountryID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges") > -1)
                        {
                            e.Updated = dgCountry;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private Country GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                FlightPakMasterService.Country CountryType = new FlightPakMasterService.Country();
                exManager.Process(() =>
                {
                    CountryType.CountryName = tbDescription.Text;
                    CountryType.CountryCD = tbCode.Text;
                    string Code = Session["SelectedCountryID"].ToString();
                    if (hdnSave.Value == "Update")
                    {
                        foreach (GridDataItem Item in dgCountry.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("CountryID").ToString().Trim() == Code.Trim())
                            {

                                if (Item.GetDataKeyValue("CountryID") != null)
                                {
                                    CountryType.CountryID = Convert.ToInt64(Item.GetDataKeyValue("CountryID"));
                                }

                                break;
                            }
                        }
                    }
                    CountryType.IsDeleted = false;

                    return CountryType;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return CountryType;
            }
        }
        /// <summary>
        /// To display the edit form
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCountryID"] != null)
                    {
                        strCountryId = "";
                        foreach (GridDataItem Item in dgCountry.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("CountryID").ToString().Trim() == Session["SelectedCountryID"].ToString().Trim())
                            {
                                if ((Item).GetDataKeyValue("CountryCD") != null)
                                {
                                    tbCode.Text = (Item).GetDataKeyValue("CountryCD").ToString();
                                }
                                if ((Item).GetDataKeyValue("CountryName") != null)
                                {
                                    tbDescription.Text = (Item).GetDataKeyValue("CountryName").ToString();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                tbIsoCode.ReadOnly = false;
                                tbIsoCode.Enabled = true;
                                if ((Item).GetDataKeyValue("ISOCD") != null)
                                {
                                    tbIsoCode.Text = (Item).GetDataKeyValue("ISOCD").ToString();
                                }
                                else
                                {
                                    tbIsoCode.Text = string.Empty;
                                }
                                break;
                            }
                        }
                        EnableForm(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetVal = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper() == (tbCode.Text.ToString().ToUpper().Trim()));
                                if (RetVal.Count() > 0 && RetVal != null)
                                {
                                    cvCode.IsValid = false;
                                    tbCode.Focus();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCountry.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var CountryValue = FPKMstService.GetCountryMasterList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CountryValue);
            List<FlightPakMasterService.Country> filteredList = GetFilteredList(CountryValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CountryID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCountry.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCountry.CurrentPageIndex = PageNumber;
            dgCountry.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfCountry CountryValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedCountryID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CountryValue.EntityList.OrderByDescending(x => x.LastUpdDT).FirstOrDefault().CountryID;
                Session["SelectedCountryID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.Country> GetFilteredList(ReturnValueOfCountry CountryValue)
        {
            List<FlightPakMasterService.Country> filteredList = new List<FlightPakMasterService.Country>();

            if (CountryValue.ReturnFlag)
            {
                filteredList = CountryValue.EntityList;
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCountry.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgCountry.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}