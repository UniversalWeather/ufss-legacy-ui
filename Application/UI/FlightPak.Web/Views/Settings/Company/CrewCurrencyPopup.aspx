﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewCurrencyPopup.aspx.cs" ValidateRequest="false"
    ClientIDMode="AutoID" Inherits="FlightPak.Web.Views.Settings.Company.CrewCurrencyPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Crew Currency Defaults</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">


            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function returnToParent() {
                //create the argument that will be returned to the parent page

                oArg = new Object();
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            function OnClientClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg == '' || arg == null) {
                    // No need to refresh RadGrid 
                }
                else {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest(arg);
                }

                //get the transferred arguments
                //                var arg = args.get_argument();
                //                if (arg !== null) {
                //                    if (arg)
                //                        document.getElementById("<%// tbFedTaxAccount.ClientID%>").value = arg.AccountNumber;
                //                    else
                //                        document.getElementById("<%// tbFedTaxAccount.ClientID%>").value = "";
                //                }
            }

            function Close() {
                //alert('clos called');
                // GetRadWindow().BrowserWindow.refreshGrid();
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="CurrencyDefaults">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="CurrencyDefaults" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="CurrencyDefaults" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table id="CurrencyDefaults" class="box1">
            <tr>
                <td class="tdtext150">
                    &nbsp;
                </td>
                <td class="tdLabel80">
                    Days Queried
                </td>
                <td class="tdtext50">
                    Min:
                </td>
                <td class="tdtext50">
                    Max:
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Day Landings:
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbDLDayQueried" runat="server" CssClass="RadMaskedTextBox25" Text="90"
                        onBlur="return RemoveSpecialChars(this)" onKeyPress="return fnAllowNumeric(this, event)"
                        MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDLDayQueried" runat="server" ValidationGroup="save"
                        ControlToValidate="tbDLDayQueried" Text="" Display="Dynamic" ForeColor="Red"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvDLDayQueried" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbDLDayQueried" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbDLMin" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)" ToolTip="Minimum number of day landings."
                        Text="3" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDLMin" runat="server" ValidationGroup="save" ControlToValidate="tbDLMin"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvDLMin" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbDLMin" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Night Landings:
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbNLDayQueried" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="90" MaxLength="3" >
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNLDayQueried" runat="server" ValidationGroup="save"
                        ControlToValidate="tbNLDayQueried" Text="" Display="Dynamic" ForeColor="Red"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvNLDayQueried" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbNLDayQueried" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbNLMinimum" runat="server" CssClass="RadMaskedTextBox25" Text="3" ToolTip="Minimum number of night landings."
                        onKeyPress="return fnAllowNumeric(this, event)" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNLMinimum" runat="server" ValidationGroup="save"
                        ControlToValidate="tbNLMinimum" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvNLMinimum" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbNLMinimum" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Approaches:
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbApproachesDaysQueried" runat="server" CssClass="RadMaskedTextBox25"
                        Text="180" onKeyPress="return fnAllowNumeric(this, event)" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvApproachesDaysQueried" runat="server" ValidationGroup="save"
                        ControlToValidate="tbApproachesDaysQueried" Text="" Display="Dynamic" ForeColor="Red"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvApproachesDaysQueried" runat="server" ValidationGroup="save"
                        ErrorMessage="" ControlToValidate="tbApproachesDaysQueried" MinimumValue="1"
                        MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbApproachesMin" runat="server" CssClass="RadMaskedTextBox25" Text="6" ToolTip="Minimum number approaches."
                        onKeyPress="return fnAllowNumeric(this, event)" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvApproachesMin" runat="server" ValidationGroup="save"
                        ControlToValidate="tbApproachesMin" Text="" Display="Dynamic" ForeColor="Red"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvApproachesMin" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbApproachesMin" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Instrument:
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbInstrumentDaysQueried" runat="server" CssClass="RadMaskedTextBox25"
                        onKeyPress="return fnAllowNumeric(this, event)" Text="180" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInstrumentDaysQueried" runat="server" ValidationGroup="save"
                        ControlToValidate="tbInstrumentDaysQueried" Text="" Display="Dynamic" ForeColor="Red"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvInstrumentDaysQueried" runat="server" ValidationGroup="save"
                        ErrorMessage="" ControlToValidate="tbInstrumentDaysQueried" MinimumValue="1"
                        MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbInstrumentDaysMin" runat="server" CssClass="RadMaskedTextBox25" ToolTip="Minimum number of Instrument hours."
                        onKeyPress="return fnAllowNumeric(this, event)" Text="6" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInstrumentDaysMin" runat="server" ValidationGroup="save"
                        ControlToValidate="tbInstrumentDaysMin" Text="" Display="Dynamic" ForeColor="Red"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvInstrumentDaysMin" runat="server" ValidationGroup="save"
                        ErrorMessage="" ControlToValidate="tbInstrumentDaysMin" MinimumValue="1" MaximumValue="999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Takeoff Days:
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbTdDq" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="90" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTdDq" runat="server" ValidationGroup="save" ControlToValidate="tbTdDq"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvTdDq" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbTdDq" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbTdMin" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)" ToolTip="Minimum number of day takeoffs."
                        Text="3" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTdMin" runat="server" ValidationGroup="save" ControlToValidate="tbTdMin"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvTdMin" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbTdMin" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Takeoff Nights:
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbTnDq" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="90" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTnDq" runat="server" ValidationGroup="save" ControlToValidate="tbTnDq"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvTnDq" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbTnDq" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbTnMin" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)" ToolTip="Minimum number of night takeoffs."
                        Text="3" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTnMin" runat="server" ValidationGroup="save" ControlToValidate="tbTnMin"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvtbTnMin" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbTnMin" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    <asp:Label ID="lbDays" runat="server" Text="7"></asp:Label>
                    Days
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbDaysDQ" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="7" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDaysDQ" runat="server" ValidationGroup="save"
                        ControlToValidate="tbDaysDQ" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvDaysDQ" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbDaysDQ" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbDaysMax" Text="034.000" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in 7 days."
                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revDaysMax" runat="server" Display="Dynamic"
                        ErrorMessage="Invalid Format" ControlToValidate="tbDaysMax" ForeColor="Red" ValidationExpression="^\d+(\.\d\d\d)$"
                        ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvDaysMax" runat="server" ValidationGroup="save"
                        ControlToValidate="tbDaysMax" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvDaysMax" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbDaysDQ" MinimumValue="1.000" MaximumValue="999.999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    <asp:Label ID="lbDays2" runat="server" Text="30"></asp:Label>
                    Days
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbDays2DQ" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="30" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDays2DQ" runat="server" ValidationGroup="save"
                        ControlToValidate="tbDays2DQ" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvDays2DQ" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbDays2DQ" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbDays2Max" Text="120.000" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in 30 days."
                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revDays2Max" runat="server" Display="Dynamic"
                        ErrorMessage="Invalid Format" ControlToValidate="tbDays2Max" ForeColor="Red"
                        ValidationExpression="^\d+(\.\d\d\d)$" ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvDays2Max" runat="server" ValidationGroup="save"
                        ControlToValidate="tbDays2Max" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvDays2Max" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbDays2Max" MinimumValue="1.000" MaximumValue="999.999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Calendar Month:
                </td>
                <td class="tdLabel80">
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbCmDaysMax" Text="120.000" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in 1 calendar month."
                        ValidationGroup="save" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revCmDaysMax" runat="server" Display="Dynamic"
                        ErrorMessage="Invalid Format" ControlToValidate="tbCmDaysMax" ForeColor="Red"
                        ValidationExpression="^\d+(\.\d\d\d)$" ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvCmDaysMax" runat="server" ValidationGroup="save"
                        ControlToValidate="tbCmDaysMax" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCmDaysMax" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbCmDaysMax" MinimumValue="1.000" MaximumValue="999.999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    <asp:Label ID="lbCmDays" runat="server" Text="90"></asp:Label>
                    Days
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbCmDaysDq" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="90" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCmDaysDq" runat="server" ValidationGroup="save"
                        ControlToValidate="tbCmDaysDq" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCmDaysDq" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbCmDaysDq" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbCmDaysMaximum" Text="300.000" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in 90 days."
                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revCmDaysMaximum" runat="server" Display="Dynamic"
                        ErrorMessage="Invalid Format" ControlToValidate="tbCmDaysMaximum" ForeColor="Red"
                        ValidationExpression="^\d+(\.\d\d\d)$" ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvtbCmDaysMaximum" runat="server" ValidationGroup="save"
                        ControlToValidate="tbCmDaysMaximum" Text="" Display="Dynamic" ForeColor="Red"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvtbCmDaysMaximum" runat="server" ValidationGroup="save"
                        ErrorMessage="" Type="Double" ControlToValidate="tbCmDaysMaximum" MinimumValue="1.000"
                        MaximumValue="999.999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Calendar Quarter:
                </td>
                <td class="tdLabel80">
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbCqMax" Text="500.000" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in 1 calendar quarter."
                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revCqMax" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                        ControlToValidate="tbCqMax" ForeColor="Red" ValidationExpression="^\d+(\.\d\d\d)$"
                        ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvCqMax" runat="server" ValidationGroup="save" ControlToValidate="tbCqMax"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCqMax" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbCqMax" MinimumValue="1.000" MaximumValue="999.999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    <asp:Label ID="lbCqMonths" runat="server" Text="6"></asp:Label>
                    Months
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbCqDq" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="6" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCqDq" runat="server" ValidationGroup="save" ControlToValidate="tbCqDq"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCqDq" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbCqDq" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbCqMaximum" Text="800.000" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in 6 months."
                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revCqMaximum" runat="server" Display="Dynamic"
                        ErrorMessage="Invalid Format" ControlToValidate="tbCqMaximum" ForeColor="Red"
                        ValidationExpression="^\d+(\.\d\d\d)$" ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvCqMaximum" runat="server" ValidationGroup="save"
                        ControlToValidate="tbCqMaximum" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCqMaximum" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbCqMaximum" MinimumValue="1.000" MaximumValue="999.999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Previous 2 Quarters:
                </td>
                <td class="tdLabel80">
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbPqMax" Text="800.000" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in the previous quarters."
                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revPqMax" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                        ControlToValidate="tbPqMax" ForeColor="Red" ValidationExpression="^\d+(\.\d\d\d)$"
                        ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvPqMax" runat="server" ValidationGroup="save" ControlToValidate="tbPqMax"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvPqMax" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbPqMax" MinimumValue="1.000" MaximumValue="999.999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    <asp:Label ID="lbPqMonths" runat="server" Text="12"></asp:Label>
                    Months
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbPqDq" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)" 
                        Text="12" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPqDq" runat="server" ValidationGroup="save" ControlToValidate="tbPqDq"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvPqDq" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbPqDq" MinimumValue="1" MaximumValue="999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbPqMaximum" Text="1400.00" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in 12 months."
                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revPqMaximum" runat="server" Display="Dynamic"
                        ErrorMessage="Invalid Format" ControlToValidate="tbPqMaximum" ForeColor="Red"
                        ValidationExpression="^\d+(\.\d+)$" ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvPqMaximum" runat="server" ValidationGroup="save"
                        ControlToValidate="tbPqMaximum" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvPqMaximum" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbPqMaximum" MinimumValue="1.00" MaximumValue="9999.999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Calendar Year :
                </td>
                <td class="tdLabel80">
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbCyMax" Text="1400.00" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in a calendar year."
                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revCyMax" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                        ControlToValidate="tbCyMax" ForeColor="Red" ValidationExpression="^\d+(\.\d+)$"
                        ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvCyMax" runat="server" ValidationGroup="save" ControlToValidate="tbCyMax"
                        Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCyMax" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbCyMax" MinimumValue="1.00" MaximumValue="9999.999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    <asp:Label ID="lbCyDays" runat="server" Text="365"></asp:Label>
                    Days
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbCyDaysDq" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)" 
                        Text="365" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCyDaysDq" runat="server" ValidationGroup="save"
                        ControlToValidate="tbCyDaysDq" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCyDaysDq" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbCyDaysDq" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbCyDaysMaximum" Text="1400.00" MaxLength="7" runat="server" CssClass="tdtext50" ToolTip="Maximum number of hours allowed in 365 days."
                        onKeyPress="return fnAllowNumericAndChar(this,event,'.')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revCyDaysMaximum" runat="server" Display="Dynamic"
                        ErrorMessage="Invalid Format" ControlToValidate="tbCyDaysMaximum" ForeColor="Red"
                        ValidationExpression="^\d+(\.\d+)$" ValidationGroup="save"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvtbCyDaysMaximum" runat="server" ValidationGroup="save"
                        ControlToValidate="tbCyDaysMaximum" Text="" Display="Dynamic" ForeColor="Red"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCyDaysMaximum" runat="server" ValidationGroup="save" ErrorMessage=""
                        Type="Double" ControlToValidate="tbCyDaysMaximum" MinimumValue="1.00" MaximumValue="9999.999"
                        SetFocusOnError="True"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    <asp:Label ID="lbCyDaysRest" runat="server" Text="7"></asp:Label>
                    Day Rest
                </td>
                <td class="tdLabel80">
                    <asp:TextBox ID="tbCyDayRestDq" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="7" MaxLength="3">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCyDayRestDq" runat="server" ValidationGroup="save"
                        ControlToValidate="tbCyDayRestDq" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCyDayRestDq" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbCyDayRestDq" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbCyDayRestMin" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="24" MaxLength="2" ToolTip="Minimum number of consecutive hours rest in a 7 day period.">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCyDayRestMin" runat="server" ValidationGroup="save"
                        ControlToValidate="tbCyDayRestMin" Text="" Display="Dynamic" ForeColor="Red"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCyDayRestMin" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbCyDayRestMin" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
            </tr>
            <tr>
                <td class="tdtext150">
                    Calendar Quarter Rest:
                </td>
                <td class="tdLabel80">
                </td>
                <td class="tdtext50">
                    <asp:TextBox ID="tbCqrMin" runat="server" CssClass="RadMaskedTextBox25" onKeyPress="return fnAllowNumeric(this, event)"
                        Text="13" MaxLength="2" ToolTip="Minimum number of days rest in a calendar quarter.">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCqrMin" runat="server" ValidationGroup="save"
                        ControlToValidate="tbCqrMin" Text="" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCqrMin" runat="server" ValidationGroup="save" ErrorMessage=""
                        ControlToValidate="tbCqrMin" MinimumValue="1" MaximumValue="999" SetFocusOnError="True"></asp:RangeValidator>
                </td>
                <td class="tdtext50">
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div>
                        <asp:Label ID="tbNote" runat="server" CssClass="crew-currency-alert-text" Text="Information: Value should be greater than or equal to 1"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div style="padding: 5px 5px; text-align: right;">
                        <asp:Button ID="btnSubmit" runat="server" Text="Ok" CssClass="button" OnClick="btnSubmit_Click"
                            ValidationGroup="save" /><asp:ValidationSummary ID="vsCrewCurrencyPopup" runat="server"
                                HeaderText="Value should greater than or equal to 1" ShowMessageBox="True" ShowSummary="False"
                                ValidationGroup="save" />
                        <button id="btnCancel" onclick="returnToParent(); return false;" class="button">
                            Cancel</button>
                        <asp:Label ID="lbScript" runat="server" Text="" Visible="false"></asp:Label>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
