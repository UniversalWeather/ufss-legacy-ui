﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class DepartmentAuthorizationCatalog : BaseSecuredPage
    {
        #region "VARIABLES"
        private bool IsValidateCustom = true;
        private ExceptionManager exManager;
        private bool DepartmentAuthorizationPageNavigated = false;
        private bool AuthorizationPageNavigated = false;
        private string strDepartmentCD = "";
        private string strDepartmentID = "";
        private string strauthorizationID = "";
        private bool _selectLastModified = false;
        private long rtnValue = 0;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    
                    // Grid Control could be ajaxified when the page is initially loaded.
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgDepartment, dgDepartment, RadAjaxLoadingPanel1);
                    // Store the clientID of the grid to reference it later on the client
                    RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgDepartment.ClientID));
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAuthorization, dgAuthorization, RadAjaxLoadingPanel1);
                    // Store the clientID of the grid to reference it later on the client
                    RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgAuthorization.ClientID));
                    lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewDepartmentAuthorizationReport);
                    RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                    if (!IsPostBack)
                    {
                        //To check the page level access.
                        CheckAutorization(Permission.Database.ViewDepartmentAuthorizationCatalog);
                         // Method to display first record in read only format   
                        if (Session["SearchItemPrimaryKeyValue"] != null)
                        {
                            dgDepartment.Rebind();
                            ReadDepartmentOnlyForm();
                            EnableDepartmentForm(false);
                            dgAuthorization.Rebind();
                            ReadAuthOnlyForm();
                            EnableAuthForm(false);
                            Session.Remove("SearchItemPrimaryKeyValue");
                        }
                        else
                        {
                            //To check the page level access for Authorization 
                            divAuthorization.Visible = IsAuthorized(Permission.Database.ViewAuthorization);
                            //To select the first row in grid
                            DefaultSelection(true);
                        }

                        if (IsPopUp)
                        {
                            dgAuthorization.AllowPaging = false;
                            dgDepartment.AllowPaging = false;
                            EnableDepartmentForm(false);
                        }
                    }
                    

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the selected Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDepartmentID"] != null)
                    {
                        string ID = Session["SelectedDepartmentID"].ToString();
                        bool itemFound = false;
                        foreach (GridDataItem item in dgDepartment.MasterTableView.Items)
                        {
                            if (item.GetDataKeyValue("DepartmentID").ToString().Trim() == ID)
                            {
                                //keep track wether any of the items got matched
                                itemFound = true;
                                item.Selected = true;
                                dgDepartment.SelectedIndexes.Clear();
                                dgDepartment.SelectedIndexes.Add(item.ItemIndex.ToString());                               
                                break;
                            }
                        }

                        //if no any item got matched then select the default item
                        if(!itemFound)
                            DefaultSelection(false);
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the selected Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectAuthorizationItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedAuthorizationID"] != null)
                    {
                        string ID = Session["SelectedAuthorizationID"].ToString();
                        foreach (GridDataItem item in dgAuthorization.MasterTableView.Items)
                        {
                            if (item.GetDataKeyValue("AuthorizationID").ToString().Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the first record after insert and update
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!IsPopUp)
                    {
                        if (BindDataSwitch)
                        {
                            dgDepartment.Rebind();
                        }
                        if (dgDepartment.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedDepartmentID"] = null;
                            //}
                            if (Session["SelectedDepartmentID"] == null)
                            {
                                dgDepartment.SelectedIndexes.Add(0);
                                if (!IsPopUp)
                                {
                                    Session["SelectedDepartmentID"] = dgDepartment.Items[0].GetDataKeyValue("DepartmentID").ToString();
                                }
                            }

                            ReadDepartmentOnlyForm();
                        }
                        else
                        {
                            ClearDepartmentForm();
                            EnableDepartmentForm(false);
                        }
                        dgAuthorization.Rebind();
                        if (dgAuthorization.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedAuthorizationID"] = null;
                            //}
                            if (Session["SelectedAuthorizationID"] == null)
                            {
                                if (!IsPopUp)
                                {
                                    dgAuthorization.SelectedIndexes.Add(0);
                                    Session["SelectedAuthorizationID"] = dgAuthorization.Items[0].GetDataKeyValue("AuthorizationID").ToString();
                                    ReadAuthOnlyForm();
                                }
                            }
                        }
                        else
                        {
                            dgAuthorization.DataSource = string.Empty;
                            EnableAuthForm(false);
                            ClearAuthForm();
                        }

                        EnableDepartmentForm(false);
                        EnableAuthForm(false);
                        GridAuthEnable(true, true, true);
                        GridDepartmentEnable(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #region Department
        /// <summary>
        /// Prerender method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartment_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (DepartmentAuthorizationPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgDepartment, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// page index changed
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDepartment_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgDepartment.ClientSettings.Scrolling.ScrollTop = "0";
                        DepartmentAuthorizationPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Created for Department Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartment_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnDepEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnDepInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Bind Department Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartment_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var DepartmentValue = DepartmentService.GetAllDepartmentList();
                            if (DepartmentValue.ReturnFlag == true)
                            {
                                dgDepartment.DataSource = DepartmentValue.EntityList;
                            }
                            //To check for unique departcd we save  entity in session
                            Session["DepartVal"] = (List<GetAllDepartments>)DepartmentValue.EntityList.ToList();
                            if (chkSearchActiveOnly != null)
                            {
                                if (!IsPopUp)
                                    SearchAndFilter(false);
                            }

                            if (!String.IsNullOrEmpty(Request.QueryString["DepartmentID"]) && IsPopUp)
                            {
                                long departmentId = Convert.ToInt64(Request.QueryString["DepartmentID"].ToString());

                                var department =
                                   DepartmentService.GetDepartmentByWithFilters("", departmentId, 0, false).EntityList;

                                if (department.Count > 0)
                                {
                                    tbDeptCode.Text = department[0].DepartmentCD;
                                    tbDeptDescription.Text = department[0].DepartmentName;
                                    tbClientCode.Text = department[0].ClientCD;
                                    tbMarginPercentage.Text = department[0].DepartPercentage.HasValue
                                        ? department[0].DepartPercentage.Value.ToString("F1")
                                        : "0.0";

                                    tbDeptCode.Enabled = false;
                                    tbDeptDescription.Enabled = false;
                                    tbClientCode.Enabled = false;
                                    tbMarginPercentage.Enabled = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// To filter Inactive records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllDepartments> lstDepartment = new List<FlightPakMasterService.GetAllDepartments>();
                if (Session["DepartVal"] != null)
                {
                    lstDepartment = (List<FlightPakMasterService.GetAllDepartments>)Session["DepartVal"];
                }
                if (lstDepartment.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == false) { lstDepartment = lstDepartment.Where(x => x.IsInActive == false).ToList<GetAllDepartments>(); }
                    else
                        if (chkSearchActiveOnly.Checked == true) { lstDepartment = lstDepartment.ToList(); }
                    dgDepartment.DataSource = lstDepartment;
                    if (IsDataBind)
                    {
                        dgDepartment.DataBind();
                        SelectFirstItem();
                    }

                }
                return false;
            }
        }
        private void SelectFirstItem()
        {
            if (dgDepartment.SelectedItems.Count <= 0)
            {
                dgDepartment.SelectedIndexes.Add(0);
            }
            GridDataItem item = dgDepartment.SelectedItems[0] as GridDataItem;
            Session["SelectedDepartmentID"] = item.GetDataKeyValue("DepartmentID").ToString().Trim();
            ReadDepartmentOnlyForm();
            dgAuthorization.Rebind();
            dgAuthorization.SelectedIndexes.Add(0);
            GridDepartmentEnable(true, true, true);
            GridAuthEnable(true, true, true);
            if (dgAuthorization.Items.Count > 0 && dgAuthorization.SelectedItems.Count > 0)
            {
                GridDataItem Item = dgAuthorization.SelectedItems[0] as GridDataItem;
                Session["SelectedAuthorizationID"] = Item.GetDataKeyValue("AuthorizationID").ToString().Trim();
                ReadAuthOnlyForm();
            }
        }
        #endregion


        /// <summary>
        /// Datagrid Item Command for Department Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartment_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedDepartmentID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Department, Convert.ToInt64(Session["SelectedDepartmentID"].ToString().Trim()));
                                        Session["IsDepartmentEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.DepartmentAuthorization);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.DepartmentAuthorization);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayDepartmentEditForm();
                                        GridDepartmentEnable(false, true, false);
                                        GridAuthEnable(false, false, false);
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDeptDescription);
                                        //tbDeptDescription.Focus();
                                        hdnDept.Value = "Dept";
                                        SelectItem();
                                        //EnableDepartmentForm(true);
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDeptCode);
                                //tbDeptCode.Focus();
                                dgDepartment.SelectedIndexes.Clear();
                                DisplayDepartmentInsertForm();
                                GridDepartmentEnable(true, false, false);
                                hdnDept.Value = "Dept";
                                EnableDepartmentForm(true);
                                dgAuthorization.DataSource = string.Empty;
                                dgAuthorization.DataBind();
                                ClearAuthForm();
                                GridAuthEnable(false, false, false);
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            case "RowClick":
                                dgAuthorization.Rebind();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayDepartmentInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //tbDeptCode.ReadOnly = false;
                    //tbDeptCode.BackColor = System.Drawing.Color.White;
                    hdnSave.Value = "Save";
                    tbDeptCode.Text = string.Empty;
                    chkDeptInactive.Checked = false;
                    tbDeptDescription.Text = string.Empty;
                    tbClientCode.Text = string.Empty;
                    ClearDepartmentForm();
                    EnableDepartmentForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Update Command for updating the values of Department in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDepartment_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        CheckCustomValidator();
                        if (IsValidateCustom)
                        {
                            if (Session["SelectedDepartmentID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objDepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDepartmentService.UpdateDepartment(GetItems());
                                    //For Data Anotation
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.Department, Convert.ToInt64(Session["SelectedDepartmentID"].ToString().Trim()));
                                        }
                                        GridDepartmentEnable(true, true, true);
                                        GridAuthEnable(true, true, true);
                                        dgDepartment.Rebind();
                                        //DefaultSelection(false);
                                        EnableDepartmentForm(false);
                                        ShowSuccessMessage();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.DepartmentAuthorization);
                                    }
                                }
                            }
                            if (dgAuthorization.Items.Count > 0)
                            {
                                dgDepartment.Items[0].GetDataKeyValue("DepartmentID").ToString();
                                Session["AuthAuthorizationID"] = dgAuthorization.Items[0].GetDataKeyValue("AuthorizationID").ToString();
                                //Session["AuthAuthorizationID"] = (GridDataItem)dgAuthorization.SelectedItems[0];
                            }
                            ReadAuthOnlyForm();
                            GridDepartmentEnable(true, true, true);
                            GridAuthEnable(true, true, true);
                            EnableDepartmentForm(false);
                            //tbDeptCode.ReadOnly = true;
                            tbDeptCode.Enabled = false;
                            hdnDept.Value = string.Empty;

                        }
                        if (Convert.ToString(Session["Catalog"]) == "D")
                        {
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                        }
                        if (Convert.ToString(Session["Catalog"]) == "A")
                        {
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Save and Update Department Authorization Form Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool isValidCheck = true;//To check for authorization required field
                        if (IsPopUp)
                        {
                            if (hdnDept.Value == "Dept")
                            {
                                if (hdnSave.Value == "Update")
                                {
                                    (dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                                }

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.argument='" + rtnValue + "';  oWnd.close();", true);
                            }

                            if (hdnAuth.Value == "Auth")
                            {
                                if (String.IsNullOrEmpty(tbAuthCode.Text.Trim()))
                                {
                                    cvAuthRequired.IsValid = false;
                                    isValidCheck = false;
                                }
                                if (String.IsNullOrEmpty(tbAuthDescription.Text.Trim()))
                                {
                                    cvAuthDescription.IsValid = false;
                                    isValidCheck = false;
                                }
                                if(isValidCheck)
                                {
                                    if(Request.QueryString["IsPopup"] == "Add")
                                    {
                                        if(isValidCheck)
                                        {
                                            (dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                                        }
                                    }
                                    else
                                    {
                                        (dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                    }

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                }
                            }
                        }
                        else
                        {
                            if (hdnDept.Value == "Dept")
                            {
                                if (hdnSave.Value == "Update")
                                {
                                    (dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0] as
                                        GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0] as
                                        GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                                }
                            }
                            if (hdnAuth.Value == "Auth")
                            {
                                if (String.IsNullOrEmpty(tbAuthCode.Text.Trim()))
                                {
                                    cvAuthRequired.IsValid = false;
                                    isValidCheck = false;
                                }
                                if (String.IsNullOrEmpty(tbAuthDescription.Text.Trim()))
                                {
                                    cvAuthDescription.IsValid = false;
                                    isValidCheck = false;
                                }
                                if ((hdnSave.Value == "Update") && (isValidCheck))
                                {
                                    (dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0] as
                                        GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                }
                                else
                                {
                                    if (isValidCheck)
                                    {
                                        (dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0] as
                                            GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName,
                                                string.Empty);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Cancel Department Authorization Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            if (hdnSave.Value == "Update")
            {
                if (Session["SelectedDepartmentID"] != null)
                {
                    //Unlock should happen from Service Layer
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue = CommonService.UnLock(EntitySet.Database.Department, Convert.ToInt64(Session["SelectedDepartmentID"].ToString().Trim()));
                    }
                }
                if (Session["SelectedAuthorizationID"] != null)
                {
                    //Unlock should happen from Service Layer
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue = CommonService.UnLock(EntitySet.Database.DepartmentAuthorization, Convert.ToInt64(Session["SelectedAuthorizationID"].ToString().Trim()));
                    }
                }
                if (Session["Catalog"] == "D")
                {
                    if (IsPopUp)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radDepartmentPopup');", true);
                    }
                }
                if (Session["Catalog"] == "A")
                {
                    if (IsPopUp)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radAuthorizationPopup');", true);
                    }
                }
            }
            DefaultSelection(false);
            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }

            if (IsPopUp)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
        }
        /// <summary>
        /// Department Authorization Insert Command for inserting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDepartment_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckDepartmentAllReadyExist())
                        {
                            if (!(!String.IsNullOrEmpty(Request.QueryString["DepartmentID"]) &&
                                  !String.IsNullOrEmpty(Request.QueryString["IsPopup"])))
                            {
                                cvDeptCode.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDeptCode);
                                //tbDeptCode.Focus();
                            }
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objDepartmentService = new MasterCatalogServiceClient())
                            {
                                Department objdepartment = GetItems();
                                var objRetVal = objDepartmentService.AddDepartment(objdepartment);
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    dgDepartment.Rebind();
                                    dgDepartment.SelectedIndexes.Add(0);
                                    if (dgDepartment.Items.Count > 0)
                                    {
                                        Session["SelectedDepartmentID"] = dgDepartment.Items[0].GetDataKeyValue("DepartmentID").ToString();
                                    }
                                    DefaultSelection(false);
                                    dgAuthorization.Rebind();
                                    dgAuthorization.SelectedIndexes.Add(0);
                                    if (dgAuthorization.Items.Count > 0)
                                    {
                                        Session["SelectedAuthorizationID"] = dgAuthorization.Items[0].GetDataKeyValue("AuthorizationID").ToString();
                                    }
                                    DefaultSelection(false);
                                    //tbDeptCode.ReadOnly = true;
                                    tbDeptCode.Enabled = false;
                                    hdnDept.Value = string.Empty;
                                    GridAuthEnable(true, true, true);
                                    GridDepartmentEnable(true, true, true);
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                    var departmentData = (GetDepartmentByDepartmentCDResult)objDepartmentService.GetDepartmentByDepartmentCD(objdepartment.DepartmentCD).EntityList.OrderByDescending(x => x.DepartmentID).FirstOrDefault();
                                    rtnValue = departmentData.DepartmentID;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.DepartmentAuthorization);
                                }
                                if (Session["Catalog"] == "D")
                                {
                                    if (IsPopUp)
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                    }
                                }
                                if (Session["Catalog"] == "A")
                                {
                                    if (IsPopUp)
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        ///  Department Delete Command for deleting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDepartment_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedDepartmentID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Department DepartmentType = new FlightPakMasterService.Department();
                                string Code = Session["SelectedDepartmentID"].ToString();
                                strDepartmentCD = "";
                                strDepartmentID = "";
                                foreach (GridDataItem Item in dgDepartment.MasterTableView.Items)
                                {
                                    if (Item.GetDataKeyValue("DepartmentID").ToString().Trim() == Code.Trim())
                                    {
                                        strDepartmentCD = Item.GetDataKeyValue("DepartmentCD").ToString().Trim();
                                        strDepartmentID = Item.GetDataKeyValue("DepartmentID").ToString().Trim();
                                        break;
                                    }
                                }
                                DepartmentType.DepartmentCD = strDepartmentCD;
                                DepartmentType.DepartmentID = Convert.ToInt64(strDepartmentID);
                                DepartmentType.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Department, Convert.ToInt64(strDepartmentID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Department);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Department);
                                        return;
                                    }
                                }
                                DepartmentService.DeleteDepartment(DepartmentType);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Department);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Department, Convert.ToInt64(strDepartmentID));
                    }
                }
            }
        }
        /// <summary>
        /// To check unique Department Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbDeptCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckDepartmentAllReadyExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Department);
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update" && hdnDept.Value == "Dept")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                
                                //SelectFirstItem();
                                GridDataItem item = dgDepartment.SelectedItems[0] as GridDataItem;
                                Session["SelectedDepartmentID"] = item.GetDataKeyValue("DepartmentID").ToString().Trim();

                                ReadDepartmentOnlyForm();
                                GridDepartmentEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                    }
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <returns></returns>
        private FlightPakMasterService.Department GetItems()
        {
            FlightPakMasterService.Department DepartmentService = new FlightPakMasterService.Department();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    DepartmentService.DepartmentCD = tbDeptCode.Text.Trim();
                    hdnDeptCode.Value = tbDeptCode.Text.Trim();
                    DepartmentService.DepartmentName = tbDeptDescription.Text.Trim();
                    if (!string.IsNullOrEmpty(tbClientCode.Text))
                    {
                        DepartmentService.ClientID = Convert.ToInt64(hdnclientID.Value);
                    }
                    if (hdnSave.Value == "Update")
                    {
                        //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                        if (Session["SelectedDepartmentID"] != null)
                        {
                            DepartmentService.DepartmentID = Convert.ToInt64(Session["SelectedDepartmentID"].ToString().Trim());
                        }
                    }
                    else
                    {
                        DepartmentService.DepartmentID = 0;
                    }
                    DepartmentService.IsDeleted = false;
                    DepartmentService.IsInActive = chkDeptInactive.Checked;
                    decimal DepartPercentage = 0;
                    if (tbMarginPercentage.Text.Trim() != "")
                    {
                        DepartPercentage = Convert.ToDecimal(tbMarginPercentage.Text);
                    }
                    DepartmentService.DepartPercentage = Convert.ToDecimal(DepartPercentage, CultureInfo.InvariantCulture);
                    return DepartmentService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return DepartmentService;
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgAuthorization;
                            e.Updated = dgDepartment;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayDepartmentEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDepartmentID"] != null)
                    {
                        strDepartmentID = "";
                        hdnSave.Value = "Update";
                        hdnRedirect.Value = "";
                        foreach (GridDataItem Item in dgDepartment.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("DepartmentID").ToString().Trim() == Session["SelectedDepartmentID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("DepartmentCD") != null)
                                {
                                    tbDeptCode.Text = Item.GetDataKeyValue("DepartmentCD").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("DepartmentName") != null)
                                {
                                    tbDeptDescription.Text = Item.GetDataKeyValue("DepartmentName").ToString().Trim();
                                }
                                else
                                {
                                    tbDeptDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ClientCD") != null)
                                {
                                    tbClientCode.Text = Item.GetDataKeyValue("ClientCD").ToString().Trim();
                                    // CheckAllReadyClientCodeExist();
                                }
                                else
                                {
                                    tbClientCode.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkDeptInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkDeptInactive.Checked = false;
                                }
                                break;
                            }
                        }
                        //tbDeptCode.ReadOnly = true;
                        //tbDeptCode.BackColor = System.Drawing.Color.LightGray;
                        EnableDepartmentForm(true);
                        tbDeptCode.Enabled = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        protected void ReadDepartmentOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgDepartment.SelectedItems.Count <= 0)
                {
                    dgDepartment.SelectedIndexes.Add(0);
                }
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    GridDataItem Item = dgDepartment.SelectedItems[0] as GridDataItem;
                    Label lblUser = (Label)dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString().Trim());
                    }
                    else
                    {
                        lblUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("DepartmentID") != null)
                    {
                        hdnDeptId.Value = Item.GetDataKeyValue("DepartmentID").ToString().Trim();
                    }
                    else
                    {
                        hdnDeptId.Value = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    if (Item.GetDataKeyValue("DepartmentCD") != null)
                    {
                        tbDeptCode.Text = Item.GetDataKeyValue("DepartmentCD").ToString().Trim();
                    }
                    else
                    {
                        tbDeptCode.Text = string.Empty;
                    }
                    tbDeptCode.Text = Item.GetDataKeyValue("DepartmentCD").ToString().Trim();
                    if (Item.GetDataKeyValue("DepartmentName") != null)
                    {
                        tbDeptDescription.Text = Item.GetDataKeyValue("DepartmentName").ToString().Trim();
                    }
                    else
                    {
                        tbDeptDescription.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("ClientID") != null)
                    {
                        hdnclientID.Value = Item.GetDataKeyValue("ClientID").ToString().Trim();
                    }
                    else
                    {
                        hdnclientID.Value = string.Empty;
                    }
                    if (Item.GetDataKeyValue("ClientCD") != null)
                    {
                        tbClientCode.Text = Item.GetDataKeyValue("ClientCD").ToString().Trim();
                    }
                    else
                    {
                        tbClientCode.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("IsInActive") != null)
                    {
                        chkDeptInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        chkDeptInactive.Checked = false;
                    }

                    if (Item.GetDataKeyValue("DepartPercentage") != null)
                    {
                        tbMarginPercentage.Text = Convert.ToString(Item.GetDataKeyValue("DepartPercentage").ToString(), CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        tbMarginPercentage.Text = string.Empty;
                    }


                    
                    //tbDeptCode.ReadOnly = true;
                    //tbDeptCode.BackColor = System.Drawing.Color.LightGray;
                    lbColumnName1.Text = "Department Code";
                    lbColumnName2.Text = "Description";
                    lbColumnValue1.Text = Item["DepartmentCD"].Text;
                    lbColumnValue2.Text = Item["DepartmentName"].Text;
                    EnableDepartmentForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        ///  To Clear the form
        /// </summary>        
        protected void ClearDepartmentForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbDeptCode.Text = string.Empty;
                    tbDeptDescription.Text = string.Empty;
                    tbClientCode.Text = string.Empty;
                    chkDeptInactive.Checked = false;
                    hdnclientID.Value = string.Empty;
                    tbMarginPercentage.Text = string.Empty;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To enable the form
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableDepartmentForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Set Logged-in User Name
                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                    if (identity != null && identity.Identity._clientId != null && identity.Identity._clientId != 0)
                    {
                        tbClientCode.Enabled = false;
                        tbClientCode.Text = identity.Identity._clientCd.ToString().Trim();
                        hdnclientID.Value = identity.Identity._clientId.ToString().Trim();
                        btnHomeBase.Enabled = false;
                    }
                    else
                    {
                        tbClientCode.Enabled = Enable;
                        btnHomeBase.Enabled = Enable;
                    }
                    tbDeptCode.Enabled = Enable;
                    tbDeptDescription.Enabled = Enable;
                    chkDeptInactive.Enabled = Enable;

                    if (!IsPopUp)
                    {
                        btnSaveChanges.Visible = Enable;
                        btnCancel.Visible = Enable;
                        tbMarginPercentage.Enabled = Enable;
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check whether the code is unique
        /// </summary>
        /// <returns></returns>                
        private bool CheckDepartmentAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (tbDeptCode.Text != null)
                    {
                        List<GetAllDepartments> DepartmentCodeList = new List<GetAllDepartments>();
                        DepartmentCodeList = ((List<GetAllDepartments>)Session["DepartVal"]).Where(x => x.DepartmentCD != null && x.DepartmentCD.ToString().ToUpper().Trim().Equals(tbDeptCode.Text.ToString().ToUpper().Trim())).ToList<GetAllDepartments>();
                        if (DepartmentCodeList.Count != 0)
                        {
                            cvDeptCode.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDeptCode);
                            //tbDeptCode.Focus();
                            returnVal = true;
                        }
                        else
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDeptDescription);
                            //tbDeptDescription.Focus();
                            returnVal = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridDepartmentEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDepInsert");
                    LinkButton lbtnDeleteCtl = (LinkButton)dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDepDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDepEdit");
                    if (IsAuthorized(Permission.Database.AddDepartmentAuthorizationCatalog))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteDepartmentAuthorizationCatalog))
                    {
                        lbtnDeleteCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDeleteCtl.Enabled = true;
                            lbtnDeleteCtl.OnClientClick = "javascript:return ProcessDeptDelete();";
                        }
                        else
                        {
                            lbtnDeleteCtl.Enabled = false;
                            lbtnDeleteCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDeleteCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditDepartmentAuthorizationCatalog))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessDeptUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {         
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = false;
                                table2.Visible = false;
                                table3.Visible = false;
                                table4.Visible = false;
                                table5.Visible = false;
                                table6.Visible = false;
                                table7.Visible = false;
                                dgAuthorization.Visible = false;
                                dgDepartment.Visible = false;

                                if (Session["Catalog"] != null)
                                {
                                    if ((string)Session["Catalog"] == "D")
                                    {
                                        table2.Visible = true;
                                        rpbAuth.Visible = false;
                                        tbdepartform.Visible = true;
                                        if (IsAdd)
                                        {
                                            (dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                        }
                                        else
                                        {
                                            (dgDepartment.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                        }
                                    }
                                
                                    if ((string)Session["Catalog"] == "A")
                                    {
                                        table5.Visible = true;
                                        rpbAuth.Visible = true;
                                        tbdepartform.Visible = false;

                                        if (IsAdd)
                                        {
                                            tbdepartform.Visible = true;
                                            (dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                        }
                                        else
                                        {
                                            (dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                        }
                                    }
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Department);
                }
            }

            
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgDepartment.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgDepartment.Rebind();
                    SelectItem();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                
                if (Request.QueryString["DepartmentID"] != null)
                {
                    Session["SelectedDepartmentID"] = Request.QueryString["DepartmentID"].Trim();
                }       
                

                if (Request.QueryString["Catalog"] != null)
                {
                    Session["Catalog"] = Request.QueryString["Catalog"].Trim();
                }       


                if (Request.QueryString["AuthorizationID"] != null)
                {
                    Session["SelectedAuthorizationID"] = Request.QueryString["AuthorizationID"].Trim();
                } 

                if(Request.QueryString["DepartmentID"] != null)
                {
                    Session["SelectedDepartmentID"] = Request.QueryString["DepartmentID"].Trim();
                }
      

            }
        }
        #endregion
        #region Authorization
        ///
        /// 
        private void SelectedDepartmentItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["SelectedDepartmentID"] != null)
                {
                    string ID = Session["SelectedDepartmentID"].ToString();
                    foreach (GridDataItem item in dgDepartment.MasterTableView.Items)
                    {
                        if (item.GetDataKeyValue("DepartmentID").ToString().Trim() == ID)
                        {
                            item.Selected = true;
                            Session["SelectedDepartmentID"] = dgDepartment.Items[0].GetDataKeyValue("DepartmentID").ToString();
                            break;
                        }
                    }
                    if (dgDepartment.MasterTableView.Items.Count > 0)
                    {
                        // dgDepartment.SelectedIndexes.Add(0);                     
                        ReadDepartmentOnlyForm();
                    }
                    else
                    {
                        ClearDepartmentForm();
                        EnableDepartmentForm(false);
                    }
                    dgAuthorization.Rebind();
                    if (dgAuthorization.MasterTableView.Items.Count > 0)
                    {
                        dgAuthorization.SelectedIndexes.Add(0);
                        Session["SelectedAuthorizationID"] = dgAuthorization.Items[0].GetDataKeyValue("AuthorizationID").ToString();
                        ReadAuthOnlyForm();
                    }
                    else
                    {
                        dgAuthorization.DataSource = string.Empty;
                        EnableAuthForm(false);
                        ClearAuthForm();
                    }
                    GridAuthEnable(true, true, true);
                    GridDepartmentEnable(true, true, true);
                }
            }
        }
        /// <summary>
        /// Prerender method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAuthorization_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectAuthorizationItem();
                        }
                        else if (AuthorizationPageNavigated)
                        {
                            SelectAuthorizationItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FlightPurpose);
                }
            }
        }
        /// <summary>
        /// page index changed
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAuthorization_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAuthorization.ClientSettings.Scrolling.ScrollTop = "0";
                        AuthorizationPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Created for DepartmentAuthorization Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAuthorization_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEdit = (e.Item as GridCommandItem).FindControl("lbtnInitAuthorizationEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEdit, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsert = (e.Item as GridCommandItem).FindControl("lbtnInitAuthorizationInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsert, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Bind DepartmentAuthorization Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAuthorization_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        using (FlightPakMasterService.MasterCatalogServiceClient AuthorizationService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objAuthorizationValue = AuthorizationService.GetDepartmentAuthorizationList();
                            if (objAuthorizationValue.ReturnFlag == true)
                            {
                                if (IsPopUp && Session["SelectedDepartmentID"] != null)
                                {
                                    hdnDeptId.Value = Session["SelectedDepartmentID"].ToString();
                                }
                                var ObjauthVal = objAuthorizationValue.EntityList.Where(x => x.DepartmentID.ToString().ToUpper().Trim() == (hdnDeptId.Value.ToString().ToUpper()));
                                // Filter grid records only if it is not popup, because we are displaying edit recrods from grid MasterTableView
                                // and in case of inactive record if we remove below condition grid will be bind with active records only.
                                dgAuthorization.DataSource = (chkInactiveAuth.Checked || IsPopUp) ? ObjauthVal.ToList() : ObjauthVal.Where(x => x.IsInActive == false).ToList<DepartmentAuthorization>();
                                
                                Session["AuthorizationVal"] = (List<DepartmentAuthorization>)ObjauthVal.ToList();
                            }
                            else
                            {
                                dgAuthorization.DataSource = string.Empty;
                            }
                            //To check for unique departcd we save  entity in session

                            if (chkInactiveAuth != null)
                            {
                                if (!IsPopUp)
                                    SearchAndFilter(false);
                            }
                            ClearAuthForm();
                            dgAuthorization.SelectedIndexes.Add(0);
                            Session["AuthVal"] = (List<DepartmentAuthorization>)objAuthorizationValue.EntityList.ToList();

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Checkchange event of authorization check box
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FilterBychkInactiveAuth_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilterAuth(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        /// <summary>
        /// To search and filter Authorization
        /// </summary>
        /// <param name="IsDataBind"></param>
        /// <returns></returns>
        private bool SearchAndFilterAuth(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.DepartmentAuthorization> lstAuth = new List<FlightPakMasterService.DepartmentAuthorization>();
                if (Session["AuthorizationVal"] != null)
                {
                    lstAuth = (List<FlightPakMasterService.DepartmentAuthorization>)Session["AuthorizationVal"];
                }
                if (lstAuth.Count != 0)
                {
                    if (chkInactiveAuth.Checked == false) { lstAuth = lstAuth.Where(x => x.IsInActive == false).ToList<DepartmentAuthorization>(); }
                    else
                        if (chkInactiveAuth.Checked == true) { lstAuth = lstAuth.ToList(); }
                    dgAuthorization.DataSource = lstAuth;
                    if (IsDataBind)
                    {
                        dgAuthorization.DataBind();
                        ClearAuthForm();
                        dgAuthorization.SelectedIndexes.Add(0);
                        ReadAuthOnlyForm();
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// Datagrid Item Command for DepartmentAuthorization Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAuthorization_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                //e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (UserPrincipal != null && UserPrincipal.Identity != null && UserPrincipal.Identity._fpSettings != null && UserPrincipal.Identity._fpSettings._DepartmentID != null && UserPrincipal.Identity._fpSettings._DepartmentID != 0 && UserPrincipal.Identity._fpSettings._DepartmentID == Convert.ToInt64(Session["SelectedDepartmentID"]))
                                {
                                    RadWindowManager1.RadAlert("Warning, this Department Code is being used by the Charter Quote Module. The Authorization Codes are here for historical purposes only.  Please use the Customer Catalog To Update Customer records linked to the Charter Quote Department Code.", 400, 100, "FSS", "");
                                }
                                if (Session["SelectedAuthorizationID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.DepartmentAuthorization, Convert.ToInt64(Session["SelectedAuthorizationID"].ToString().Trim()));
                                        Session["IsDepartmentAuthEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.DepartmentAuthorization);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayAuthEditForm();
                                        GridAuthEnable(false, true, false);
                                        GridDepartmentEnable(false, false, false);
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthDescription);
                                        SelectItem();
                                        hdnAuth.Value = "Auth";
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthCode);
                                //tbAuthCode.Focus();
                                //dgDepartment.SelectedIndexes.Clear();
                                if (UserPrincipal != null && UserPrincipal.Identity != null && UserPrincipal.Identity._fpSettings != null && UserPrincipal.Identity._fpSettings._DepartmentID != null && UserPrincipal.Identity._fpSettings._DepartmentID != 0 && UserPrincipal.Identity._fpSettings._DepartmentID == Convert.ToInt64(Session["SelectedDepartmentID"]))
                                {
                                    RadWindowManager1.RadAlert("Warning, this Department Code is being used by the Charter Quote Module. The Authorization Codes are here for historical purposes only.  Please use the Customer Catalog To Update Customer records linked to the Charter Quote Department Code.", 400, 100, "FSS", "");
                                }
                                DisplayAuthInsertForm();
                                hdnAuth.Value = "Auth";
                                GridAuthEnable(true, false, false);
                                GridDepartmentEnable(false, false, false);
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}                                
		                        Pair filterPair = (Pair)e.CommandArgument;
                                Session["gridAuthorizationFilterSession"] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayAuthInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //tbAuthCode.ReadOnly = false;
                    //tbAuthCode.BackColor = System.Drawing.Color.White;
                    hdnSave.Value = "Save";
                    tbAuthDescription.Text = string.Empty;
                    chkAuthInactive.Checked = false;
                    tbAuthPhone.Text = string.Empty;
                    ClearAuthForm();
                    EnableAuthForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Update Command for updating the values of DepartmentAuthorization in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAuthorization_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedAuthorizationID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objAuthorizationService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var AuthorizationCategory = objAuthorizationService.UpdateDepartmentAuthorization(GetAuthItems());
                                //For Data Anotation
                                if (AuthorizationCategory.ReturnFlag == true)
                                {
                                    ///////Update Method UnLock
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.DepartmentAuthorization, Convert.ToInt64(Session["SelectedAuthorizationID"].ToString().Trim()));
                                    }
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    GridAuthEnable(true, true, true);
                                    GridDepartmentEnable(true, true, true);
                                    hdnAuth.Value = string.Empty;
                                    hdnDeptCode.Value = string.Empty;
                                    hdnClientCode.Value = string.Empty;
                                    //DefaultSelection(true);
                                    dgAuthorization.Rebind();
                                    EnableAuthForm(false);
                                    SelectAuthorizationItem();
                                    ReadAuthOnlyForm();

                                    ShowSuccessMessage();
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(AuthorizationCategory.ErrorMessage, ModuleNameConstants.Database.DepartmentAuthorization);
                                }
                                //SelectedDepartmentItem();
                            }
                        }
                        if (Session["Catalog"] == "D")
                        {
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                //Session["SelectedDepartmentID"] = null;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                        }
                        if (Session["Catalog"] == "A")
                        {
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                //Session["AuthAuthorizationID"] = null;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Department Authorization Insert Command for inserting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAuthorization_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAuthAllReadyExist())
                        {
                            cvAuthRequired.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthCode);
                            //tbAuthCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objAuthorizationService = new MasterCatalogServiceClient())
                            {
                                var AuthorizationCategory = objAuthorizationService.AddDepartmentAuthorization(GetAuthItems());
                                //For Data Anotation
                                if (AuthorizationCategory.ReturnFlag == true)
                                {
                                    dgAuthorization.Rebind();
                                    //GridAuthEnable(true, true, true);
                                    //GridDepartmentEnable(true, true, true);
                                    hdnAuth.Value = string.Empty;
                                    hdnDeptCode.Value = string.Empty;
                                    hdnClientCode.Value = string.Empty;
                                    //DefaultSelection(false);
                                    dgAuthorization.Rebind();
                                    EnableAuthForm(false);
                                    if (dgAuthorization.Items.Count > 0)
                                    {
                                        dgAuthorization.SelectedIndexes.Add(0);
                                        ReadAuthOnlyForm();
                                    }

                                    GridAuthEnable(true, true, true);
                                    GridDepartmentEnable(true, true, true);
                                    ShowSuccessMessage();
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(AuthorizationCategory.ErrorMessage, ModuleNameConstants.Database.DepartmentAuthorization);
                                }
                            }
                        }
                        if (Session["Catalog"] == "D")
                        {
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                        }
                        if (Session["Catalog"] == "A")
                        {
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        ///  DepartmentAuthorization Delete Command for deleting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAuthorization_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedAuthorizationID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient DepartmentAuthService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.DepartmentAuthorization AuthorizationPurpose = new FlightPakMasterService.DepartmentAuthorization();
                                string Code = Session["SelectedAuthorizationID"].ToString();
                                strauthorizationID = "";
                                foreach (GridDataItem Item in dgAuthorization.MasterTableView.Items)
                                {
                                    if (Item.GetDataKeyValue("AuthorizationID").ToString().Trim() == Code.Trim())
                                    {
                                        if (Item.GetDataKeyValue("AuthorizationCD") != null)
                                        {
                                            AuthorizationPurpose.AuthorizationCD = Item.GetDataKeyValue("AuthorizationCD").ToString().Trim();
                                        }
                                        if (Item.GetDataKeyValue("AuthorizationID") != null)
                                        {
                                            strauthorizationID = Item.GetDataKeyValue("AuthorizationID").ToString().Trim();
                                        }
                                        if (Item.GetDataKeyValue("DepartmentID") != null)
                                        {
                                            AuthorizationPurpose.DepartmentID = Convert.ToInt64(Item.GetDataKeyValue("DepartmentID").ToString().Trim());
                                        }
                                        break;
                                    }
                                }
                                AuthorizationPurpose.AuthorizationID = Convert.ToInt64(strauthorizationID);
                                AuthorizationPurpose.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.DepartmentAuthorization, Convert.ToInt64(strauthorizationID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.DepartmentAuthorization);
                                        return;
                                    }
                                    DepartmentAuthService.DeleteDepartmentAuthorization(AuthorizationPurpose);
                                    dgAuthorization.Rebind();
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Department);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.DepartmentAuthorization, Convert.ToInt64(strauthorizationID));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAuthorization_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    GridDataItem item = dgAuthorization.SelectedItems[0] as GridDataItem;
                    Session["SelectedAuthorizationID"] = item.GetDataKeyValue("AuthorizationID").ToString().Trim();
                    if (btnSaveChanges.Visible == false)
                    {
                        ReadAuthOnlyForm();
                        GridAuthEnable(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private FlightPakMasterService.DepartmentAuthorization GetAuthItems()
        {
            FlightPakMasterService.DepartmentAuthorization AuthorizationService = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    AuthorizationService = new FlightPakMasterService.DepartmentAuthorization();
                    AuthorizationService.IsInActive = chkAuthInactive.Checked;
                    AuthorizationService.AuthorizationCD = tbAuthCode.Text.Trim();
                    AuthorizationService.AuthorizerPhoneNum = tbAuthPhone.Text.Trim();
                    AuthorizationService.DeptAuthDescription = tbAuthDescription.Text.Trim();
                    AuthorizationService.IsDeleted = false;                    //need to get data
                    if (Session["SelectedDepartmentID"] != null)
                    {
                        AuthorizationService.DepartmentID = Convert.ToInt64(Session["SelectedDepartmentID"].ToString().Trim());
                    }
                    else
                        if (hdnDeptCode.Value != string.Empty)
                        {
                            AuthorizationService.DepartmentID = Convert.ToInt64(hdnDeptCode.Value);
                        }
                    if (hdnSave.Value == "Update")
                    {
                        //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                        if (Session["SelectedAuthorizationID"] != null)
                        {
                            AuthorizationService.AuthorizationID = Convert.ToInt64(Session["SelectedAuthorizationID"].ToString().Trim());
                        }
                    }
                    else
                    {
                        AuthorizationService.AuthorizationID = 0;
                    }
                    if (hdnclientID.Value != string.Empty)
                    {
                        AuthorizationService.ClientID = Convert.ToInt64(hdnclientID.Value);
                    }
                    //else
                    //{
                    //}
                    //if (hdnClientCode.Value != string.Empty)
                    //{
                    //    AuthorizationService.ClientID = Convert.ToInt64(hdnClientCode.Value);
                    //}
                    AuthorizationService.IsInActive = chkAuthInactive.Checked;
                    return AuthorizationService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return AuthorizationService;
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayAuthEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedAuthorizationID"] != null)
                    {
                        strauthorizationID = "";
                        foreach (GridDataItem Item in dgAuthorization.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("AuthorizationID").ToString().Trim() == Session["SelectedAuthorizationID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("AuthorizationCD") != null)
                                {
                                    tbAuthCode.Text = Item.GetDataKeyValue("AuthorizationCD").ToString().Trim();
                                }
                                //if (Item.GetDataKeyValue("DepartmentCD") != null)
                                //{
                                //    tbDeptCode.Text = Item.GetDataKeyValue("DepartmentCD").ToString().Trim();
                                //}
                                if (Item.GetDataKeyValue("DeptAuthDescription") != null)
                                {
                                    tbAuthDescription.Text = Item.GetDataKeyValue("DeptAuthDescription").ToString().Trim();
                                }
                                else
                                {
                                    tbDeptDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("AuthorizerPhoneNum") != null)
                                {
                                    tbAuthPhone.Text = Item.GetDataKeyValue("AuthorizerPhoneNum").ToString().Trim();
                                }
                                else
                                {
                                    tbAuthPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkAuthInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkAuthInactive.Checked = false;
                                }
                                break;
                            }
                        }
                        EnableAuthForm(true);
                        tbAuthCode.Enabled = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        protected void ReadAuthOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgAuthorization.Items.Count != 0 && dgAuthorization.SelectedItems.Count > 0)
                        {
                            hdnSave.Value = "Update";
                            GridDataItem Item = dgAuthorization.SelectedItems[0] as GridDataItem;
                            Label lblUser = (Label)dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedAuthorizationUser");
                            if (Item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString().Trim());
                            }
                            else
                            {
                                lblUser.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                            }
                            tbAuthCode.Text = Item.GetDataKeyValue("AuthorizationCD").ToString().Trim();
                            if (Item.GetDataKeyValue("DeptAuthDescription") != null)
                            {
                                tbAuthDescription.Text = Item.GetDataKeyValue("DeptAuthDescription").ToString().Trim();
                            }
                            else
                            {
                                tbDeptDescription.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("AuthorizerPhoneNum") != null)
                            {
                                tbAuthPhone.Text = Item.GetDataKeyValue("AuthorizerPhoneNum").ToString().Trim();
                            }
                            else
                            {
                                tbAuthPhone.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("IsInActive") != null)
                            {
                                chkAuthInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                chkAuthInactive.Checked = false;
                            }
                            //tbAuthCode.ReadOnly = true;
                            //tbAuthCode.BackColor = System.Drawing.Color.LightGray;
                            EnableAuthForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// To Clear the form
        /// </summary>
        protected void ClearAuthForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbAuthCode.Text = string.Empty;
                    tbAuthDescription.Text = string.Empty;
                    tbAuthPhone.Text = string.Empty;
                    chkAuthInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableAuthForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbAuthCode.Enabled = enable;
                    tbAuthDescription.Enabled = enable;
                    tbAuthPhone.Enabled = enable;
                    chkAuthInactive.Enabled = enable;
                    btnSaveChanges.Visible = enable;
                    btnCancel.Visible = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Authorization Code for Department Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbAuthCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if ((tbAuthCode.Text != null) && (tbDeptCode.Text != null))
                        //{
                        //    //string SelectedDepartmentIDs;
                        //    List<DepartmentAuthorization> DepartmentAuthCodeList = new List<DepartmentAuthorization>();
                        //    if (Session["SelectedDepartmentID"] != null)
                        //    {
                        //        DepartmentAuthCodeList = ((List<DepartmentAuthorization>)Session["AuthVal"]).Where(x => x.DepartmentID.ToString().ToUpper().Trim().Equals(Session["SelectedDepartmentID"].ToString().Trim()) && (x.AuthorizationCD.ToString().ToUpper().Trim().Equals(tbAuthCode.Text.ToString().ToUpper().Trim()))).ToList<DepartmentAuthorization>();
                        //        //SelectedDepartmentIDs = Session["SelectedDepartmentID"].ToString().Trim();
                        //    }
                        //    if (DepartmentAuthCodeList.Count != 0)
                        //    {
                        //        cvAuthorizationCode.IsValid = false;
                        //        tbAuthCode.Focus();
                        //    }
                        //}
                        CheckAuthAllReadyExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// To check whether the code is unique
        /// </summary>
        /// <returns></returns>
        private bool CheckAuthAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!string.IsNullOrEmpty(tbAuthCode.Text))
                    {
                        List<DepartmentAuthorization> DepartmentAuthCodeList = new List<DepartmentAuthorization>();
                        if (Session["SelectedDepartmentID"] != null)
                        {
                            DepartmentAuthCodeList = ((List<DepartmentAuthorization>)Session["AuthVal"]).Where(x => x.DepartmentID.ToString().ToUpper().Trim().Equals(Session["SelectedDepartmentID"].ToString().Trim()) && (x.AuthorizationCD.ToString().ToUpper().Trim().Equals(tbAuthCode.Text.ToString().ToUpper().Trim()))).ToList<DepartmentAuthorization>();
                            //SelectedDepartmentIDs = Session["SelectedDepartmentID"].ToString().Trim();
                        }
                        if (DepartmentAuthCodeList.Count() > 0 && DepartmentAuthCodeList != null)
                        {
                            cvAuthRequired.IsValid = false;
                            cvAuthRequired.Text = "Unique Code is Required.";
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthCode);
                            //tbAuthCode.Focus();
                            ReturnVal = true;
                        }
                        else
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthDescription);
                            //tbAuthDescription.Focus();
                            ReturnVal = false;
                        }
                    }
                    else
                    {
                        cvAuthRequired.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthCode);
                        //tbAuthCode.Focus();
                        ReturnVal = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnVal;
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>        
        private void GridAuthEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnDeletAuthCtl = (LinkButton)dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnAuthorizationDelete");
                    LinkButton lbtnEditAuthCtl = (LinkButton)dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitAuthorizationEdit");
                    LinkButton lbtnInsertAuthCtl = (LinkButton)dgAuthorization.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitAuthorizationInsert");
                    if (IsAuthorized(Permission.Database.AddAuthorization))
                    {
                        lbtnInsertAuthCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertAuthCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertAuthCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertAuthCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteAuthorization))
                    {
                        lbtnDeletAuthCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDeletAuthCtl.Enabled = true;
                            lbtnDeletAuthCtl.OnClientClick = "javascript:return ProcessAuthDelete();";
                        }
                        else
                        {
                            lbtnDeletAuthCtl.Enabled = false;
                            lbtnDeletAuthCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDeletAuthCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditAuthorization))
                    {
                        lbtnEditAuthCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditAuthCtl.Enabled = true;
                            lbtnEditAuthCtl.OnClientClick = "javascript:return ProcessAuthUpdate();";
                        }
                        else
                        {
                            lbtnEditAuthCtl.Enabled = false;
                            lbtnEditAuthCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditAuthCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Country Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyClientCodeExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyClientCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        if ((tbClientCode.Text != null) && (tbClientCode.Text != string.Empty))
                        {
                            //To check for unique Country code
                            var objClientValue = ClientService.GetClientCodeList().EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbClientCode.Text.ToUpper().Trim())).ToList();
                            if (objClientValue.Count() == 0 || objClientValue == null)
                            {
                                cvClientCode.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                                //tbClientCode.Focus();
                                ReturnValue = false;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(hdnclientID.Value))
                                {
                                    hdnclientID.Value = ((FlightPakMasterService.Client)objClientValue[0]).ClientID.ToString().Trim();
                                    tbClientCode.Text = ((FlightPakMasterService.Client)objClientValue[0]).ClientCD;
                                }
                                ReturnValue = false;
                            }
                        }
                        else
                        {
                            hdnclientID.Value = null;
                            //if (hdnclientID.Value != null)
                            //{
                            //    //var objFlightClientVal = ClientService.GetClientCodeList().EntityList.Where(x => x.ClientID.ToString().ToUpper().Trim().Equals(hdnclientID.Value.ToString().ToUpper().Trim()));
                            //    //foreach (FlightPakMasterService.Client ClientCode in objFlightClientVal)
                            //    //{
                            //    //    // To get the client code based on client cd
                            //    //    tbClientCode.Text = ClientCode.ClientCD.ToUpper().Trim();
                            //    //}
                            //}
                        }
                        return ReturnValue;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// Method to check custom Validator
        /// </summary>
        private void CheckCustomValidator()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (CheckAllReadyClientCodeExist())
                    {
                        cvClientCode.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                        //tbClientCode.Focus();
                        IsValidateCustom = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Reports"
        /// <summary>
        /// To Show reports based on report,format,parameter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentAuthorization);
                }
            }
        }
        #endregion

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgDepartment.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var DeptValue = FPKMstService.GetAllDepartmentList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, DeptValue);
            List<FlightPakMasterService.GetAllDepartments> filteredList = GetFilteredList(DeptValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.DepartmentID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgDepartment.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgDepartment.CurrentPageIndex = PageNumber;
            dgDepartment.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllDepartments DeptValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedDepartmentID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = DeptValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().DepartmentID;
                Session["SelectedDepartmentID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllDepartments> GetFilteredList(ReturnValueOfGetAllDepartments DeptValue)
        {
            List<FlightPakMasterService.GetAllDepartments> filteredList = new List<FlightPakMasterService.GetAllDepartments>();

            if (DeptValue.ReturnFlag == true)
            {
                filteredList = DeptValue.EntityList;
            }

            if (filteredList.Count > 0 && !IsPopUp)
            {
                if (chkSearchActiveOnly.Checked == false) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetAllDepartments>(); }
                else if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.ToList(); }
            }

            return filteredList;
        }

        protected void dgAuthorization_PreRender1(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox("gridAuthorizationFilterSession", dgAuthorization, Page.Session);
        }
    }
}