﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class TripPrivacySettings : BaseSecuredPage
    {
        List<string> lstCompany;
        string HomeBase;
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        HomeBase = (Request.QueryString["HomeBase"]);
                        if (string.IsNullOrEmpty(Request.QueryString["EditModeFlag"]))
                        {
                            btnOk.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripPrivacySettings);
                }
            }
        }
        protected void dgTripPrivacy_BindData(object sender, GridNeedDataSourceEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["dgTripPrivacy"] == null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var CompanyData = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD == HomeBase);
                                lstCompany = new List<string>();
                                string[] arrString;
                                System.Collections.Hashtable htTest = new System.Collections.Hashtable();
                                DataTable table = new DataTable();
                                table.Columns.Add("Description", typeof(string));
                                table.Columns.Add("Display", typeof(string));
                                if (CompanyData.Count() > 0)
                                {
                                    foreach (FlightPakMasterService.GetAllCompanyMaster companyEntity in CompanyData)
                                    {
                                        if (companyEntity.PrivateTrip != null)
                                        {
                                            arrString = companyEntity.PrivateTrip.Split(',');
                                            if ((arrString[1].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Arrival/Departure ICAO, City, Airport Name", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Arrival/Departure ICAO, City, Airport Name", "F");
                                            }
                                            if ((arrString[2].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Arrival/Departure Time", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Arrival/Departure Time", "F");
                                            }
                                            if ((arrString[3].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Crew", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Crew", "F");
                                            }
                                            if ((arrString[4].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Flight No.", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Flight No.", "F");
                                            }
                                            if ((arrString[5].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Leg Purpose", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Leg Purpose", "F");
                                            }
                                            if ((arrString[6].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Seats Available", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Seats Available", "F");
                                            }
                                            if ((arrString[7].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("PAX Count", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("PAX Count", "F");
                                            }
                                            if ((arrString[8].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Requestor", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Requestor", "F");
                                            }
                                            if ((arrString[9].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Department", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Department", "F");
                                            }
                                            if ((arrString[10].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Authorization", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Authorization", "F");
                                            }
                                            if ((arrString[11].ToString().Contains("T\n")))
                                            {
                                                table.Rows.Add("Flight Category", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Flight Category", "F");
                                            }
                                            if ((arrString[12].ToString().Contains("T")))
                                            {
                                                table.Rows.Add("Cumulative ETE", "T");
                                            }
                                            else
                                            {
                                                table.Rows.Add("Cumulative ETE", "F");
                                            }
                                            dgTripPrivacy.DataSource = table;// ds.Tables["dtTest"];
                                        }
                                        else
                                        {
                                            table.Rows.Add("Arrival/Departure ICAO, City, Airport Name", "T");
                                            table.Rows.Add("Arrival/Departure Time", "T");
                                            table.Rows.Add("Crew", "T");
                                            table.Rows.Add("Flight No.", "T");
                                            table.Rows.Add("Leg Purpose", "T");
                                            table.Rows.Add("Seats Available", "T");
                                            table.Rows.Add("PAX Count", "T");
                                            table.Rows.Add("Requestor", "T");
                                            table.Rows.Add("Department", "T");
                                            table.Rows.Add("Authorization", "T");
                                            table.Rows.Add("Flight Category", "T");
                                            table.Rows.Add("Cumulative ETE", "T");

                                            dgTripPrivacy.DataSource = table;
                                        }
                                    }
                                }
                                else
                                {
                                    table.Rows.Add("Arrival/Departure ICAO, City, Airport Name", "T");
                                    table.Rows.Add("Arrival/Departure Time", "T");
                                    table.Rows.Add("Crew", "T");
                                    table.Rows.Add("Flight No.", "T");
                                    table.Rows.Add("Leg Purpose", "T");
                                    table.Rows.Add("Seats Available", "T");
                                    table.Rows.Add("PAX Count", "T");
                                    table.Rows.Add("Requestor", "T");
                                    table.Rows.Add("Department", "T");
                                    table.Rows.Add("Authorization", "T");
                                    table.Rows.Add("Flight Category", "T");
                                    table.Rows.Add("Cumulative ETE", "T");

                                    dgTripPrivacy.DataSource = table;
                                }
                            }
                        }
                        else
                        {
                            dgTripPrivacy.DataSource = (DataTable)Session["dgTripPrivacy"];
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripPrivacySettings);
                }
            }

        }

        private void getGridInfo()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    int CurrentPageIndex = dgTripPrivacy.CurrentPageIndex;
                    DataTable dt = new DataTable();
                    DataRow dr;
                    dt.Columns.Add(new System.Data.DataColumn("Description", typeof(String)));
                    dt.Columns.Add(new System.Data.DataColumn("Display", typeof(String)));

                    //for (int pagecount = CurrentPageIndex; pagecount < dgTripPrivacy.PageCount; pagecount++)
                    //{
                    foreach (GridDataItem row in dgTripPrivacy.Items)
                    {
                        Label Description = (Label)row.FindControl("lbDescription");
                        CheckBox Display = (CheckBox)row.FindControl("chkDisplay");
                        dr = dt.NewRow();
                        dr[0] = Description.Text;
                        if (Display.Checked)
                        {
                            dr[1] = "T";
                        }
                        else
                        {
                            dr[1] = "F";
                        }
                        dt.Rows.Add(dr);
                    }
                    //   dgTripPrivacy.CurrentPageIndex = pagecount + 1;
                    //   dgTripPrivacy.Rebind();
                    //}
                    Session["dgTripPrivacy"] = dt;
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void dgTripPrivacy_ItemDataBound(object Sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem Item = (GridDataItem)e.Item;
                Label lbCheckList = (Label)Item.FindControl("lbDisplay");
                CheckBox Display = (CheckBox)Item.FindControl("chkDisplay");
                if (lbCheckList.Text.Trim() == "T")
                {
                    Display.Checked = true;
                }
                else
                {
                    Display.Checked = false;
                }
                if (string.IsNullOrEmpty(Request.QueryString["EditModeFlag"]))
                {
                    Display.Enabled = false;
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgTripPrivacy;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripPrivacySettings);
                }
            }

        }

        protected void Ok_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        getGridInfo();
                        //lbScript.Visible = true;
                        //lbScript.Text = "<script type='text/javascript'>Close()</" + "script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "Close();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripPrivacySettings);
                }
            }

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //lbScript.Visible = true;
                        //lbScript.Text = "<script type='text/javascript'>Close()</" + "script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "Close();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripPrivacySettings);
                }
            }

        }
    }
}