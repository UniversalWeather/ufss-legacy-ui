﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.IO;
namespace FlightPak.Web.Views.Settings.Company
{
    public partial class TSABatchProcess : BaseSecuredPage
    {
        public readonly string roTsaCleared = "";
        public readonly string roTsaNoFly = "";
        public readonly string roTsaSelectee = "";
        public readonly string roFolderPath = "";
        public readonly string roDestinationFile = "";

        public TSABatchProcess()
        {
            string FolderPath = Server.MapPath("~") + "\\" + System.Configuration.ConfigurationManager.AppSettings["TSAUploadDirectory"] + "\\";
            string FileName = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff").Replace(" ", "_").Replace(":", "_").Replace(".", "_") + "_file.csv";
            roFolderPath = FolderPath.Replace("\\\\", "\\");

            roTsaCleared = roFolderPath + "TsaCleared_" + FileName;
            roTsaNoFly = roFolderPath + "TsaNoFly_" + FileName;
            roTsaSelectee = roFolderPath + "TsaSelectee_" + FileName;
        }

        private ExceptionManager exManager;
        string strErrUpload = string.Empty, strErrUploadClear = string.Empty, strErrUploadNoFly = string.Empty, strErrUploadSelect = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewCompanyProfileCatalog);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        //protected void Page_PreRender(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                if (!IsPostBack)
        //                {
        //                }
        //                else
        //                {
        //                    string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
        //                    if (eventArgument == "btnSaveChanges_OnClick")
        //                    {
        //                        btnSaveChanges_OnClick(sender, e);
        //                    }
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
        //        }
        //    }
        //}
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        private void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                if (Enable == false)
                {
                    lbUploadStatus.Text = "File is uploading. Please wait...";
                }
                else
                {
                    lbUploadStatus.Text = string.Empty;
                }
                btnSaveChanges.Enabled = Enable;
                btnCancel.Enabled = Enable;
                btnClose.Enabled = Enable;
            }
        }
        protected void btnSaveChanges_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSABatchUpload();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Save", "btnSaveChanges_OnClientClick(false);", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSARadClearFile = new FileUpload();
                        TSANoFlyFile = new FileUpload();
                        TSASelecteeFile = new FileUpload();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }



        #region "TSA Batch Process"
        private string GetFileUrl(string fileserverpath)
        {
            string pageurl = HttpContext.Current.Request.Url.PathAndQuery;
            string domainname = HttpContext.Current.Request.Url.AbsoluteUri.Replace(pageurl, "/");
            string applicationPath = HttpContext.Current.Server.MapPath("~/");
            string fileurl = domainname + fileserverpath.Substring(applicationPath.Length).Replace('\\', '/');

            return fileurl;
        }
        /// <summary>
        /// TSA Batch upload process
        /// Steps to be followed
        /// * Delete TSANoFly, TSASelectee, TSAClear from Database 
        /// * Save TSANoFly, TSASelectee, TSAClear to Database 
        /// * Check the all the Passenger Records with TSANoFly
        /// * Conditions : 1 - First Name AND Middle Name AND Last Name AND DOB - SET TSASTAT to 'R' 
        /// * Conditions : 2 - First Name AND Middle Name AND Last Name         - SET TSASTAT to 'R'
        /// * Check the all the Passenger Records with TSASelectee
        /// * Conditions : 3 - First Name AND Middle Name AND Last Name AND DOB - SET TSASTAT to 'S'
        /// * Conditions : 4 - First Name AND Middle Name AND Last Name         - SET TSASTAT to 'S'
        /// * Check the all the Passenger Records with TSAClear
        /// * Conditions : 5 - First Name AND Middle Name AND Last Name AND DOB - SET TSASTAT to 'C'
        /// * Conditions : 6 - First Name AND Middle Name AND Last Name         - SET TSASTAT to 'C'
        /// * Conditions : 7 - SET TSASTAT to 'NULL'
        /// </summary>
        private void TSABatchUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool IsFileExist = false;
                FlightPakMasterService.Customer customerTSA = new Customer();
                customerTSA.CustomerName = "null";
                // Read TSANoFly records from CSV file
                if (TSANoFlyFile.HasFile)
                {
                    CopyFile(TSANoFlyFile, roTsaNoFly);

                    using (MasterCatalogServiceClient CompanyService = new MasterCatalogServiceClient())
                    {
                        // Delete and Save TSANoFly records to database
                        FlightPakMasterService.TSANoFly TSANoFlydel = new TSANoFly();
                        CompanyService.DeleteTSANoFly(TSANoFlydel);
                        CompanyService.ReadTSACSVFile(customerTSA, EntitySet.Database.TSANoFly, GetCSVContent(roTsaNoFly));
                    }

                    DeleteFile(roTsaNoFly, null);
                    IsFileExist = true;

                }

                // Read TSASelectee records from CSV file
                if (TSASelecteeFile.HasFile)
                {
                    CopyFile(TSASelecteeFile, roTsaSelectee);

                    using (MasterCatalogServiceClient CompanyService = new MasterCatalogServiceClient())
                    {
                        // Delete and Save TSASelectee records to database
                        FlightPakMasterService.TSASelect TSASelectdel = new TSASelect();
                        CompanyService.DeleteTSASelecte(TSASelectdel);
                        CompanyService.ReadTSACSVFile(customerTSA, EntitySet.Database.TSASelect, GetCSVContent(roTsaSelectee));
                    }

                    DeleteFile(roTsaSelectee, null);
                    IsFileExist = true;
                }

                // Read TSAClear records from CSV file
                if (TSARadClearFile.HasFile)
                {
                    CopyFile(TSARadClearFile, roTsaCleared);

                    using (MasterCatalogServiceClient CompanyService = new MasterCatalogServiceClient())
                    {
                        // Delete and Save TSAClear records to database
                        FlightPakMasterService.TSAClear TSACleardel = new TSAClear();
                        CompanyService.DeleteTSANClear(TSACleardel);
                        CompanyService.ReadTSACSVFile(customerTSA, EntitySet.Database.TSAClear, GetCSVContent(roTsaCleared));
                    }

                    DeleteFile(roTsaCleared, null);
                    IsFileExist = true;
                }

                if (IsFileExist == true)
                {
                    using (MasterCatalogServiceClient CompanyService = new MasterCatalogServiceClient())
                    {
                        // Insert TSANoFly, TSASelectee, TSAClear Via Customer
                        CompanyService.SetTSAstatus(customerTSA);
                    }
                }
            }
        }


        private string CopyFile(FileUpload UploadedFile, string file)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UploadedFile.HasFile)
                {
                    if (!Directory.Exists(roFolderPath))
                    {
                        Directory.CreateDirectory(roFolderPath);
                    }
                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }
                    UploadedFile.SaveAs(file);

                }

                return file;
            }
        }

        private bool DeleteFile(string DestinationFile, string FolderPath)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FolderPath, DestinationFile))
            {
                bool ReturnValue = false;
                if (!string.IsNullOrEmpty(DestinationFile))
                {
                    File.Delete(DestinationFile);
                }
                if (!string.IsNullOrEmpty(FolderPath))
                {
                    FolderPath = FolderPath.Substring(0, FolderPath.LastIndexOf("\\"));
                    if (Directory.GetFiles(FolderPath).Count() == 0)
                    {
                        Directory.Delete(FolderPath);
                        ReturnValue = true;
                    }
                }
                return ReturnValue;
            }
        }

        private string GetCSVContent(string FilePath)
        {
            if (!string.IsNullOrEmpty(FilePath))
            {
                string CSVData = string.Empty;
                using (var rd = new StreamReader(FilePath))
                {
                    while (!rd.EndOfStream)
                    {
                        CSVData = rd.ReadToEnd();
                    }
                }
                return CSVData;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}