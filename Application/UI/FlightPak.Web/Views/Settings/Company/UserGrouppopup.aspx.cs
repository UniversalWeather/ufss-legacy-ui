﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.AdminService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class UserGrouppopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private string UserGroupCD;
        Int64 CustomerId;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["UserGroupCD"]))
                        {
                            UserGroupCD = Request.QueryString["UserGroupCD"].ToUpper().Trim();
                        }
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CustomerID != null)
                        {
                            CustomerId = Convert.ToInt64(UserPrincipal.Identity._fpSettings._CustomerID.ToString().Trim());
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroup);
                }
            }
        }
        /// <summary>
        /// Bind User Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgUserGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AdminService.AdminServiceClient ObjService = new AdminService.AdminServiceClient())
                        {
                            var ObjRetVal = ObjService.GetUserGroupList(CustomerId);
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgUserGroup.DataSource = ObjRetVal.EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserGroup);
                }
            }
        }

        protected void dgUserGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgUserGroup_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgUserGroup, Page.Session);
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgUserGroup;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }
        }
        /// <summary>
        /// To Select Item
        /// </summary>
        protected void SelectItem()
        {
            if (!string.IsNullOrEmpty(UserGroupCD))
            {
                foreach (GridDataItem item in dgUserGroup.MasterTableView.Items)
                {
                    if (item["UserGroupCD"].Text.ToUpper().Trim() == UserGroupCD)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (dgUserGroup.Items.Count > 0)
                {
                    dgUserGroup.SelectedIndexes.Add(0);
                }
            }
        }
    }
}
