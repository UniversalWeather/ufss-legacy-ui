﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    ClientIDMode="AutoID" AutoEventWireup="true" CodeBehind="SiflRateCatalog.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.SiflRateCatalog" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        var currentTextBox = null;
        var currentDatePicker = null;

        //This method is called to handle the onclick and onfocus client side events for the texbox
        function showPopup(sender, e) {
            //this is a reference to the texbox which raised the event
            //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

            currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

            //this gets a reference to the datepicker, which will be shown, to facilitate
            //the selection of a date
            var datePicker = $find("<%= RadDatePicker1.ClientID %>");

            //this variable is used to store a reference to the date picker, which is currently
            //active
            currentDatePicker = datePicker;

            //this method first parses the date, that the user entered or selected, and then
            //sets it as a selected date to the picker
            datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

            //the code lines below show the calendar, which is used to select a date. The showPopup
            //function takes three arguments - the x and y coordinates where to show the calendar, as
            //well as its height, derived from the offsetHeight property of the textbox
            var position = datePicker.getElementPosition(currentTextBox);
            datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
        }

        //this handler is used to set the text of the TextBox to the value of selected from the popup
        function dateSelected(sender, args) {
            if (currentTextBox != null) {
                //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                //value of the picker
                currentTextBox.value = args.get_newValue();
            }
        }

        //this function is used to parse the date entered or selected by the user
        function parseDate(sender, e) {
            if (currentDatePicker != null) {
                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                var dateInput = currentDatePicker.get_dateInput();
                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                sender.value = formattedDate;
            }
        }

        function validateEmptyRadRateTextbox(sender, eventArgs) {
            if (sender.get_textBoxValue().length < 1) {
                sender.set_value('0.0');
            }
        }

        function fncClientFromDate(sender, args) {
            //alert("Hi");
            var DateOfBirth;
            DateOfBirth = new Date(document.getElementById('<%= ucFromDate.FindControl("tbDate").ClientID %>').value);
            //var Today = new Date();
            if (DateOfBirth == null) {
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }

        function fncClientToDate(sender, args) {
            var DateOfBirth;
            DateOfBirth = new Date(document.getElementById('<%= ucToDate.FindControl("tbDate").ClientID %>').value);
            //var Today = new Date();
            if (DateOfBirth == null) {
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }


        //this function is used to display the format if the textbox is empty
        function ValidateEmptyTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "0.0000";
            }
        }            
    </script>
    <style>
        .art-setting-PostContent .RadAjaxPanel .rgHeaderWrapper .rgHeaderDiv {width: 751px !important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgPaymentType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgPaymentType" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgSiflRateCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgSiflRateCatalog">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgSiflRateCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <table style="width: 100%;" cellspacing="0" cellpadding="0">
        <tr style="display: none;">
            <td>
                <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
            </td>
        </tr>
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">SIFL Rate</span> <span class="tab-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=SIFLRateHelp"
                        class="help-icon" target="_blank" title="Help"></a></span><span class="SIFL_Rate_text">
                            User shall be responsible for providing and maintaining such SIFL Rates.</span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgSiflRateCatalog" runat="server" AllowSorting="true" OnItemCreated="dgSiflRateCatalog_ItemCreated"
                Culture="en-US" Visible="true" OnNeedDataSource="dgSiflRateCatalog_BindData"
                OnItemCommand="dgSiflRateCatalog_ItemCommand" OnItemDataBound="dgSiflRateCatalog_ItemDataBound"
                OnPreRender="SiflRateCatalog_PreRender" OnUpdateCommand="dgSiflRateCatalog_UpdateCommand"
                Height="341px" OnInsertCommand="dgSiflRateCatalog_InsertCommand" OnPageIndexChanged="SiflRateCatalog_PageIndexChanged"
                OnDeleteCommand="dgSiflRateCatalog_DeleteCommand" AutoGenerateColumns="false"
                PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgSiflRateCatalog_SelectedIndexChanged"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
                <MasterTableView ClientDataKeyNames="FareLevelID,FareLevelCD,StartDT,EndDT,Rate1,Rate2,Rate3,TerminalCHG"
                    DataKeyNames="FareLevelID,FareLevelCD,StartDT,EndDT,Rate1,Rate2,Rate3,TerminalCHG,LastUpdUID,LastUpdTS,IsDeleted,CustomerID,IsInActive"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridDateTimeColumn DataField="FareLevelCD" UniqueName="FareLevelCD" PickerType="None"
                            ShowFilterIcon="false" Display="false">
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="StartDT" HeaderText="Start Date" SortExpression="StartDT"
                            UniqueName="StartDT" PickerType="None" ShowFilterIcon="false" AllowFiltering="false"
                            ConvertEmptyStringToNull="true" HeaderStyle-Width="120px">
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="EndDT" HeaderText="End Date" SortExpression="EndDT"
                            UniqueName="EndDT" PickerType="None" ShowFilterIcon="false" AllowFiltering="false"
                            ConvertEmptyStringToNull="true" HeaderStyle-Width="120px">
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn DataField="Rate1" HeaderText="Fare Level 1" AutoPostBackOnFilter="false" 
                            CurrentFilterFunction="EqualTo" FilterDelay="500" ShowFilterIcon="false" HeaderStyle-Width="100px"
                            FilterControlWidth="80px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Rate2" HeaderText="Fare Level 2" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="EqualTo" FilterDelay="500" ShowFilterIcon="false" HeaderStyle-Width="100px"
                            FilterControlWidth="80px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Rate3" HeaderText="Fare Level 3" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="EqualTo" FilterDelay="500" ShowFilterIcon="false" HeaderStyle-Width="100px"
                            FilterControlWidth="80px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TerminalCHG" HeaderText="Terminal Charge" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="EqualTo" FilterDelay="500" ShowFilterIcon="false" HeaderStyle-Width="100px"
                            FilterControlWidth="80px">
                        </telerik:GridBoundColumn>
                        <%--<telerik:GridNumericColumn DataField="Rate1" HeaderText="Fare Level 1" AutoPostBackOnFilter="true"
                            NumericType="Currency" DecimalDigits="2" CurrentFilterFunction="EqualTo" FilterDelay="4000"
                            ShowFilterIcon="false" HeaderStyle-Width="100px" FilterControlWidth="80px">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Rate2" HeaderText="Fare Level 2" AutoPostBackOnFilter="true"
                            NumericType="Currency" DecimalDigits="2" CurrentFilterFunction="EqualTo" FilterDelay="4000"
                            ShowFilterIcon="false" HeaderStyle-Width="100px" FilterControlWidth="80px">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Rate3" HeaderText="Fare Level 3" AutoPostBackOnFilter="true"
                            NumericType="Currency" DecimalDigits="2" CurrentFilterFunction="EqualTo" FilterDelay="4000"
                            ShowFilterIcon="false" HeaderStyle-Width="100px" FilterControlWidth="80px">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="TerminalCHG" HeaderText="Terminal Charge" AutoPostBackOnFilter="true"
                            NumericType="Currency" DecimalDigits="2" CurrentFilterFunction="EqualTo" FilterDelay="4000"
                            ShowFilterIcon="false" HeaderStyle-Width="100px" FilterControlWidth="80px">
                        </telerik:GridNumericColumn>--%>
                        <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="LastUpdTS" AutoPostBackOnFilter="false"
                            Display="false" CurrentFilterFunction="EqualTo" FilterDelay="500" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FareLevelID" HeaderText="FareLevelID" AutoPostBackOnFilter="false"
                            UniqueName="FareLevelID" Display="false" CurrentFilterFunction="EqualTo" FilterDelay="500"
                            ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            HeaderStyle-Width="80px" ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddSiflRateCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                Visible='<%# IsAuthorized(Permission.Database.EditSiflRateCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                                Visible='<%# IsAuthorized("Permission.Database.DeleteSiflRateCatalog")%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr style="display: none;">
                                <td>
                                    <uc:DatePicker ID="DatePicker1" runat="server"></uc:DatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel80" valign="top">
                                    <span class="mnd_text">Start Date</span>
                                </td>
                                <td class="tdLabel250" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <uc:DatePicker ID="ucFromDate" runat="server"></uc:DatePicker>
                                                <%--<asp:TextBox ID="tbFromDate" runat="server" CssClass="text70" CausesValidation="true"
                                                    TabIndex="1" onKeyPress="return fnNotAllowKeys(this, event)" ValidationGroup="Save"
                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onBlur="return RemoveSpecialChars(this);"
                                                    MaxLength="10"></asp:TextBox>--%>
                                                <asp:CustomValidator ID="cvFromDate" runat="server" ErrorMessage="Start Date is Required"
                                                    ClientValidationFunction="fncClientFromDate" ValidationGroup="Save" CssClass="alert-text"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%--<asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ValidationGroup="Save"
                                                    ControlToValidate="ucFromDate" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                    ErrorMessage="Start Date is Required"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel100" valign="top">
                                    <span class="mnd_text">End Date</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <uc:DatePicker ID="ucToDate" runat="server"></uc:DatePicker>
                                                <%--<asp:TextBox ID="tbToDate" runat="server" CssClass="text70" onKeyPress="return fnNotAllowKeys(this, event)"
                                                    TabIndex="2" ValidationGroup="Save" onclick="showPopup(this, event);" onfocus="showPopup(this, event);"
                                                    onBlur="return RemoveSpecialChars(this);" MaxLength="10"></asp:TextBox>--%>
                                                <asp:CustomValidator ID="cvToDate" runat="server" ErrorMessage="Start Date is Required"
                                                    ClientValidationFunction="fncClientToDate" ValidationGroup="Save" CssClass="alert-text"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%--<asp:RequiredFieldValidator ID="rfvToEndDate" runat="server" ValidationGroup="Save"
                                                    ControlToValidate="ucToDate" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                    ErrorMessage="End Date is Required"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80" valign="top">
                                    Fare Level 1:
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="pr_radtextbox_75">
                                                <%--<asp:TextBox ID="tbFare1" runat="server" Text="0.0000" MaxLength="8" ValidationGroup="Save"
                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                                    CssClass="text80" TabIndex="9"></asp:TextBox>--%>
                                                <telerik:RadNumericTextBox ID="tbFare1" runat="server" Type="Currency" Culture="en-US"
                                                    ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="8" Value="0.00"
                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    <NumberFormat AllowRounding="true" DecimalDigits="4" />
                                                </telerik:RadNumericTextBox>&nbsp;[0 - 500 Miles]
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revFare1" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                    ControlToValidate="tbFare1" CssClass="alert-text" ValidationExpression="^[0-9]{0,3}(\.[0-9]{1,4})?$"
                                                    ValidationGroup="Save"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80" valign="top">
                                    Fare Level 2:
                                </td>
                                <td class="tdLabel250">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr class="pr_radtextbox_75">
                                            <td>
                                                <%--<asp:TextBox ID="tbFare2" runat="server" Text="0.0000" MaxLength="8" ValidationGroup="Save"
                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                                    CssClass="text80" TabIndex="10"></asp:TextBox>--%>
                                                <telerik:RadNumericTextBox ID="tbFare2" runat="server" Type="Currency" Culture="en-US"
                                                    ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="8" Value="0.00"
                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    <NumberFormat AllowRounding="true" DecimalDigits="4" />
                                                </telerik:RadNumericTextBox>&nbsp;[501 - 1500 Miles]
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revFare2" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                    ControlToValidate="tbFare2" CssClass="alert-text" ValidationExpression="^[0-9]{0,3}(\.[0-9]{1,4})?$"
                                                    ValidationGroup="Save"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel100">
                                    Terminal Charge:
                                </td>
                                <td class="pr_radtextbox_75">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <%--<asp:TextBox ID="tbTerminalCharge" runat="server" MaxLength="8" Text="0.0000" CssClass="text80"
                                                    ValidationGroup="Save" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                    onBlur="return ValidateEmptyTextbox(this, event)" TabIndex="12"></asp:TextBox>--%>
                                                <telerik:RadNumericTextBox ID="tbTerminalCharge" runat="server" Type="Currency" Culture="en-US"
                                                    ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="8" Value="0.00"
                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    <NumberFormat AllowRounding="true" DecimalDigits="4" />
                                                </telerik:RadNumericTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revTerminalCharge" runat="server" Display="Dynamic"
                                                    ErrorMessage="Invalid Format" ControlToValidate="tbTerminalCharge" CssClass="alert-text"
                                                    ValidationGroup="Save" ValidationExpression="^[0-9]{0,3}(\.[0-9]{1,4})?$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80" valign="top">
                                    Fare Level 3:
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="pr_radtextbox_75">
                                                <%--<asp:TextBox ID="tbFare3" runat="server" ValidationGroup="Save" Text="0.0000" MaxLength="8"
                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                                    CssClass="text80" TabIndex="11"></asp:TextBox>--%>
                                                <telerik:RadNumericTextBox ID="tbFare3" runat="server" Type="Currency" Culture="en-US"
                                                    ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="8" Value="0.00"
                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    <NumberFormat AllowRounding="true" DecimalDigits="4" />
                                                </telerik:RadNumericTextBox>&nbsp;[Over 1500 Miles]
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revFare3" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                    onBlur="return ValidateEmptyTextbox(this, event)" SetFocusOnError="true" ControlToValidate="tbFare3"
                                                    CssClass="alert-text" ValidationExpression="^[0-9]{0,3}(\.[0-9]{1,4})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="Save"
                            OnClick="SaveChanges_Click" CssClass="button" TabIndex="13" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnCode" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" CausesValidation="false"
                            OnClick="Cancel_Click" TabIndex="14" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
