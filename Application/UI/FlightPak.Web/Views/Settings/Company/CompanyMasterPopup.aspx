﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <title>Company Profile</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var selectedRowData = "";
        var jqgridTableId = '#gridHomebase';
        var homebaseId = null;
        var homebasecd = null;
        var selectedRowId = [];
        var selectedRowIdWithData = [];
        var ismultiSelect = false;
        var homebases = [];
        var isopenlookup = false;
        
        ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
        
        $(document).ready(function () {
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            selectedRowData = decodeURI(getQuerystring("HomeBase", ""));
            if (ismultiSelect) {
                
                if (selectedRowData.length < document.getElementById("hdnSelectedRows").value.length) {
                    selectedRowData = document.getElementById("hdnSelectedRows").value;
                }
                homebases = selectedRowData.split(',');
            }
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                if (rowData["HomebaseCD"] == undefined) {
                    showMessageBox('Please select a record.', popupTitle);
                    
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/Views/Settings/Company/CompanyProfileCatalog.aspx?IsPopup=Add", popupTitle, 1100, 798, jqgridTableId);
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                homebaseId = rowData['HomebaseID'];
                homebasecd = rowData['HomebaseCD'];
                var widthDoc = $(document).width();
                if (homebaseId != undefined && homebaseId != null && homebaseId != '' && homebaseId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a record.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Homebase&homebaseId=' + homebaseId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            var msg = "This HomeBase (" + homebasecd + ")";
                            
                            if (content == 'PreconditionFailed') {
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                            } else {
                                msg = msg.concat(" does not exist.");
                                if (content.indexOf('404'))
                                    showMessageBox(msg, popupTitle);
                            }
                        }
                    });

                }
            }

            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                homebaseId = rowData['HomebaseID'];

                var widthDoc = $(document).width();
                if (homebaseId != undefined && homebaseId != null && homebaseId != '' && homebaseId != 0) {
                    popupwindow("/Views/Settings/Company/CompanyProfileCatalog.aspx?IsPopup=&HomebaseID=" + homebaseId, popupTitle, 1100, 798, jqgridTableId);
                }
                else {
                    showMessageBox('Please select a record.', popupTitle);
                }
                return false;
            });

            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'homebase';
                    postData.showInactive = $('#chkSearchActiveOnly').is(':checked') ? true : false;
                    postData.homebaseCD = function () {
                        if (ismultiSelect && homebases.length > 0) {
                            return homebases[0];
                        } else {
                            return selectedRowData;
                        }
                    };
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 260,
                width: 650,
                multiselectWidth:100,
                viewrecords: true,
                multiselect: ismultiSelect,
                altRows: true,
                altclass: 'AltRowClass',
                pager: "#pg_gridPager",
                colNames: ['HomebaseID', 'Code', 'Base Description', 'Home Base', 'Inactive'],
                colModel: [
                    { name: 'HomebaseID', index: 'HomebaseID', key: true, hidden: true },
                     { name: 'IcaoId', index: 'IcaoId', width: 50 },
                     { name: 'BaseDescription', index: 'BaseDescription', width: 150 },
                     { name: 'HomebaseCD', index: 'IcaoId', width: 50 },
                     { name: 'IsInActive', index: 'IsInActive', width: 50, formatter: 'checkbox', search: false, align: 'center' }

                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);
                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['HomebaseID'];//replace name with any column
                    maintainMultiselectRows(id, status);
                    if (id !== lastSel) {
                        $(jqgridTableId).find(".selected").removeClass('selected');
                        $(jqgridTableId).jqGrid('resetSelection', lastSel, true);
                        $(jqgridTableId).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                    
                },
                onSelectAll: function(id, status) {
                        for (var i = 0; i < id.length; i++) {
                            maintainMultiselectRows(id[i], status);
                        }
                        homebases = selectedRowData.split(",");
                },
                afterInsertRow: function (rowid, rowObject) {
                    if (ismultiSelect) {
                        if (jQuery.inArray(rowObject.HomebaseCD,homebases)!=-1) {
                            $(jqgridTableId).setSelection(rowid, true);
                        }
                    }
                    if ($.trim(rowObject.HomebaseCD.toLowerCase()) == $.trim(selectedRowData.toLowerCase()))
                    {
                        $(jqgridTableId).setSelection(rowid, true);
                    }
                },
                loadComplete: function (rowData) {

                    if (ismultiSelect) {
                        
                        var gridid = "gridHomebase";
                        $("#jqgh_" + gridid + "_cb").find("b").remove();
                        
                       
                        $("#jqgh_" + gridid + "_cb").append("<b> Select All</b>");
                        $("#jqgh_" + gridid + "_cb").css("height", "32px");
                        $(jqgridTableId + "_cb").css("width", "80px");
                        $(jqgridTableId + " tbody tr").children().first("td").css("width", "80px");
                        
                        if (selectedRowData.length < document.getElementById("hdnSelectedRows").value.length) {
                            selectedRowData = document.getElementById("hdnSelectedRows").value;
                        }
                        homebases = selectedRowData.split(',');
                    }
                }
            });

            $("#pagesizebox").insertBefore('.ui-paging-info');
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

        });

        function maintainMultiselectRows(id,status) {
            if (ismultiSelect) {
                if (status) {
                    if ($.inArray(id, selectedRowId) == -1) {
                        selectedRowId.push(id);
                        selectedRowIdWithData.push($(jqgridTableId).getRowData(id));
                    }
                } else {
                    if ($.inArray(id, selectedRowId) != -1) {
                        var selectid = $.inArray(id, selectedRowId);
                        selectedRowId = jQuery.grep(selectedRowId, function (value) {
                            return value != id;
                        });
                        selectedRowIdWithData[selectid] = undefined;
                        selectedRowIdWithData = jQuery.grep(selectedRowIdWithData, function (value) {
                            return value != undefined;
                        });
                    }
                }

                selectedRowData = "";
                for (var i = 0; i < selectedRowIdWithData.length; i++) {
                    selectedRowData += (eval(selectedRowIdWithData[i])).HomebaseCD + ",";
                }
                selectedRowData.substr(0, selectedRowData.length - 1);
                
                document.getElementById("hdnSelectedRows").value=selectedRowData;
            } 
        }

        function reloadPageSize() {

            
            selectedRowIdWithData = null;
            selectedRowId = null;
            document.getElementById("hdnSelectedRows").value = "";
            homebases = null;
            selectedRowData = "";
            selectedRowData = decodeURI(getQuerystring("HomeBase", ""));
            
            
            var myGrid = $(jqgridTableId);
            myGrid.trigger('reloadGrid');
            
        }

        function returnToParent(rowData) {
            
            var oArg = new Object();

            if (ismultiSelect) {
                oArg.data = [];
                oArg.Arg1 = "";
                for (var i = 0; i < selectedRowIdWithData.length; i++) {
                    oArg.data.push(selectedRowIdWithData[i]);
                    oArg.Arg1 += (eval(selectedRowIdWithData[i])).HomebaseCD + ",";
                }
                oArg.Arg1 = oArg.Arg1.substring(0, oArg.Arg1.length - 1);
            } else {

                oArg.HomebaseID = rowData["HomebaseID"];
                oArg.HomeBase = rowData["HomebaseCD"];
                oArg.HomeBaseCD = rowData["HomebaseCD"];
                oArg.BaseDescription = rowData["BaseDescription"];
                oArg.IsInActive = rowData["IsInActive"];
                oArg.Arg1 = oArg.HomeBaseCD;
                
            }
            oArg.CallingButton = "HomeBaseCD";
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }
        function reloadPageSize() {
            var myGrid = $(jqgridTableId);
            var currentValue = $("#rowNum").val();
            myGrid.setGridParam({ rowNum: currentValue });
            myGrid.trigger('reloadGrid');
        }


    </script>

</head>
<body>
    <form id="form1">
        <input type="hidden" id="hdnSelectedRows"/>
        <div class="jqgrid">

            <div>
                <table class="box1">
                    <tr>
                        <td>
                            <input type="checkbox" name="chkSearchActiveOnly" id="chkSearchActiveOnly" /><label for="chkSearchInActive">Display Inactive</label>
                            <input type="button" class="button" id="btnSearch" value="Search" name="btnSearch" onclick="reloadPageSize(); return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridHomebase" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <span class="Span">Page Size:</span>
                                <input class="PageSize" id="rowNum" type="text" value="20" maxlength="5"/>
                                <input id="btnChange" class="btnChange" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>

            </div>
        </div>

    </form>
</body>
</html>
