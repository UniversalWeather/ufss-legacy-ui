﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head runat="server">
    <title>Authorization</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />

   <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
   <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
   <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <style>
        .Authorizationpop .ui-jqgrid-htable{width:636px!important;}
        .Authorizationpop #gridAuthorization{width:636px!important;}

    </style>

    <script type="text/javascript">
        var isopenlookup = false;
        var selectedRowData = null;
        var authorizationId = null;
        var authorizationcd = null;
        var deptId = null;
        var clientId = null;
        var jqgridTableId = '#gridAuthorization';
        $(document).ready(function () {
            var authorizeSelectedCD = $.trim(unescape(getQuerystring("AuthorizationCD", "")));
            clientId = $.trim(unescape(getQuerystring("ClientID", "")));
            if (authorizeSelectedCD != "") {
                isopenlookup = true;
            }

            deptId = getUrlVars()["deptId"];            
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

             $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if (selr) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    var AuthorizationCD = rowData['AuthorizationCD'];
                    returnToParent(rowData);
                }
                else {
                    showMessageBox('Please select a record.', popupTitle);
                }
                return false;
            });


            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                var left = (screen.width / 2) - (2 / 2);
                var top = (screen.height / 2) - (2 / 2);
                popupwindow("/Views/Settings/Company/DepartmentAuthorizationCatalog.aspx?Catalog=A&IsPopup=Add&DepartmentID=" + deptId, popupTitle, 1100, 450, jqgridTableId);
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                authorizationId = rowData['AuthorizationID'];
                authorizationcd = rowData['AuthorizationCD'];
                var widthDoc = $(document).width();
                if (authorizationId != undefined && authorizationId != null && authorizationId != '' && authorizationId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a record.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=authorization&authorizationId=' + authorizationId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            authorizeSelectedCD = ""; // need to make this blank to make sure grid refresh after delete record
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            var msg = "This AuthorizationCode (" + authorizationcd + ")";
                            
                            if (content == 'PreconditionFailed') {
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                            } else {
                                msg = msg.concat(" does not exist.");
                                if (content.indexOf('404'))
                                    showMessageBox(msg, popupTitle);
                            }
                        }
                    });

                }
            }
                      

            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                authorizationId = rowData['AuthorizationID'];
                var widthDoc = $(document).width();
                if (authorizationId != undefined && authorizationId != null && authorizationId != '' && authorizationId != 0) {                
                    popupwindow("/Views/Settings/Company/DepartmentAuthorizationCatalog.aspx?Catalog=A&IsPopup=&AuthorizationID=" + authorizationId + "&DepartmentID=" + deptId, popupTitle, 1100, 300, jqgridTableId);
                }
                else {
                    showMessageBox('Please select a record.', popupTitle);
                }
                return false;
            });



            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.departmentId = deptId;
                    postData.clientId = clientId;
                    postData.apiType = 'fss';
                    postData.method = 'authorization';
                    postData.showInactive = $('#chkSearchActiveOnly').is(':checked') ? true : false;
                    postData.authorizationCD = authorizeSelectedCD;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 300,
                width: 635,
                viewrecords: true,
                multiselect: false,
                pager: "#pg_gridPager",
                colNames: ['AuthorizationID', 'Code', 'Description', 'Auth Phone','Inactive'],
                colModel: [
                    { name: 'AuthorizationID', index: 'AuthorizationID', key: true, hidden: true },
                    { name: 'AuthorizationCD', index: 'AuthorizationCD', width: 50 },
                    { name: 'DeptAuthDescription', index: 'DeptAuthDescription', width: 150 },
                    { name: 'AuthorizerPhoneNum', index: 'AuthorizerPhoneNum', width: 100 },        
                    { name: 'IsInactive', index: 'IsInactive', width: 50, align: "center", editable: true, formatter: "checkbox", formatoptions: { disabled: true }, search: false }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);
                },
                onSelectRow: function (id) {

                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['AuthorizationID'];//replace name with any column
                    authorizeSelectedCD = $.trim(rowData['AuthorizationCD']);

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $(jqgridTableId).jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                afterInsertRow: function (rowid, rowdata, rowelem) {
                    var lastSel = rowdata['AuthorizationCD'];//replace name with any column

                    if ($.trim(authorizeSelectedCD) == $.trim(lastSel)) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        $(jqgridTableId).jqGrid('setSelection', rowid);
                    }
                },
                gridComplete: function () {
                    var rowIds = $(this).getDataIDs();
                    for (var i = 0; i <= rowIds.length; i++) {
                        var rowData = $(this).getRowData(rowIds[i]);
                        if (rowData['AuthorizationCD'] == authorizeSelectedCD) {
                            $(this).jqGrid('setSelection', rowIds[i]);
                        }
                    }

                }
            });
            $("#pagesizebox").insertBefore('.ui-paging-info');
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
            
        });

        function reloadAuthorization() {
            $(jqgridTableId).jqGrid().trigger('reloadGrid');
        }

        function reloadPageSize() {
            var myGrid = $(jqgridTableId);
            var currentValue = $("#rowNum").val();
            myGrid.setGridParam({ rowNum: currentValue });
            myGrid.trigger('reloadGrid');
        }

  
        function returnToParent(rowData) {
            var oArg = new Object(); 
            oArg.DeptAuthDescription = rowData["DeptAuthDescription"];
            oArg.AuthorizationCD = rowData["AuthorizationCD"];
            oArg.AuthorizationID = rowData["AuthorizationID"];
            
            oArg.Arg1 = oArg.AuthorizationCD;
            oArg.CallingButton = "AuthorizationCD";

            var oWnd = GetRadWindow();
            if (oArg.AuthorizationCD) {
                oWnd.close(oArg);
            } else {
                oWnd.close('');
            }
        }

        function rebindgrid() {
            $(jqgridTableId).trigger('reloadGrid');
        }
    </script>
</head>
<body>
    <form id="form1">


        <div class="jqgrid Authorizationpop">

            <div>
                <table class="box1">
                    <tr>
                <td>
                    <table>
                        <tbody><tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <input type="checkbox" name="chkSearchActiveOnly" id="chkSearchActiveOnly"/><label for="chkSearchActiveOnly">Display Inactive</label></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <input type="button" class="button" id="btnSearch" value="Search" name="btnSearch"  onclick="reloadAuthorization(); return false;" />
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>

                    <tr>
                        <td>
                            <table id="gridAuthorization" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                               <div id="pagesizebox">
                               <span>Size:</span>
                                <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                </div> 
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </form>
</body>
</html>
