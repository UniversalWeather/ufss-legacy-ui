﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
namespace FlightPak.Web.Views.Settings.Company
{
    public partial class ExchangeRatesCatalog : BaseSecuredPage
    {
        private bool ExchangeRatePageNavigated = false;
        bool IsDateCheck = true;
        private ExceptionManager exManager;
        string DateFormat;
        string tbFromDate;
        string tbToDate;
        private bool IsValidateCustom = true;
        DateTime DateStart = new DateTime();
        DateTime DateEnd = new DateTime();
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgExchangeRatesCatalog, dgExchangeRatesCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgExchangeRatesCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                        // Set Logged-in User Name
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }
                        ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewExchangeRatesCatalog);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgExchangeRatesCatalog.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// Code to enable first record on the grid to be selected on page load
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgExchangeRatesCatalog.Rebind();
                    }
                    if (dgExchangeRatesCatalog.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedExchangeRateID"] = null;
                        //}
                        if (Session["SelectedExchangeRateID"] == null)
                        {
                            dgExchangeRatesCatalog.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                                Session["SelectedExchangeRateID"] = dgExchangeRatesCatalog.Items[0].GetDataKeyValue("ExchangeRateID").ToString();
                        }

                        if (dgExchangeRatesCatalog.SelectedIndexes.Count == 0)
                            dgExchangeRatesCatalog.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Grid Command Item for Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEdit = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEdit, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsert = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsert, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// To select the selected item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedExchangeRateID"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                        //string code = items.GetDataKeyValue("DelayTypeCD").ToString();
                        string ID = Session["SelectedExchangeRateID"].ToString();
                        foreach (GridDataItem item in dgExchangeRatesCatalog.MasterTableView.Items)
                        {
                            if (item.GetDataKeyValue("ExchangeRateID").ToString().Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgExchangeRatesCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                        ExchangeRatePageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (ExchangeRatePageNavigated)
                        {
                            SelectItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }

        


        /// <summary>
        /// Binds the data to the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ExchangeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ExchangeRateValue = ExchangeRateService.GetExchangeRate();
                            List<FlightPakMasterService.ExchangeRate> ExchangeRateList = new List<FlightPakMasterService.ExchangeRate>();
                            if (ExchangeRateValue.ReturnFlag == true)
                            {
                                ExchangeRateList = ExchangeRateValue.EntityList;
                            }
                            dgExchangeRatesCatalog.DataSource = ExchangeRateList;
                            Session["ExchangeRateList"] = ExchangeRateList;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// Item Command for ExchangeRate Catalog Grid, fires on click of Add, Edit or Delete button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                //tbCode.CssClass = "text40 inpt_non_edit";
                                e.Item.Selected = true;
                                hdnSaveFlag.Value = "Update";
                                if (Session["SelectedExchangeRateID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.ExchangeRate, Convert.ToInt64(Session["SelectedExchangeRateID"].ToString().Trim()));
                                        Session["IsExchangeEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.ExchangeRate);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbCode.Enabled = false;
                                        tbDescription.Focus();
                                        SelectItem();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                tbCode.CssClass = "text40";
                                dgExchangeRatesCatalog.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                tbCode.Focus();
                                DisableLinks();
                                break;
                            case "UpdateEdited":
                                dgExchangeRatesCatalog_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// Update Command for ExchangeRate Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedExchangeRateID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objExchangeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objExchangeRateService.UpdateExchangeRate(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    ///////Update Method UnLock
                                    if (Session["SelectedExchangeRateID"] != null)
                                    {
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.ExchangeRate, Convert.ToInt64(Session["SelectedExchangeRateID"].ToString().Trim()));
                                        }
                                    }
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    GridEnable(true, true, true);
                                    DefaultSelection(true);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.ExchangeRate);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// Insert Command for ExchangeRate Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        // CheckAllReadyExist();
                        //if (IsValidateCustom)
                        //{
                        using (MasterCatalogServiceClient ExchangeRateService = new MasterCatalogServiceClient())
                        {
                            var objRetVal = ExchangeRateService.AddExchangeRate(GetItems());
                            if (objRetVal.ReturnFlag == true)
                            {
                                dgExchangeRatesCatalog.Rebind();
                                DefaultSelection(false);

                                ShowSuccessMessage();
                                EnableLinks();
                                _selectLastModified = true;
                            }
                            else
                            {
                                //For Data Anotation
                                ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.ExchangeRate);
                            }
                        }
                        //}

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string strExchangeRateID = "";
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedExchangeRateID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ExchangeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.ExchangeRate ExchangeRateType = new FlightPakMasterService.ExchangeRate();
                                string Code = Session["SelectedExchangeRateID"].ToString();
                                foreach (GridDataItem Item in dgExchangeRatesCatalog.MasterTableView.Items)
                                {
                                    if (Item.GetDataKeyValue("ExchangeRateID").ToString().Trim() == Code.Trim())
                                    {
                                        if (Item.GetDataKeyValue("ExchangeRateID") != null)
                                        {
                                            strExchangeRateID = Item.GetDataKeyValue("ExchangeRateID").ToString().Trim();
                                        }
                                        if (Item.GetDataKeyValue("FromDT") != null)
                                        {
                                            ExchangeRateType.FromDT = Convert.ToDateTime(Item.GetDataKeyValue("FromDT"));
                                        }
                                        if (Item.GetDataKeyValue("ToDT") != null)
                                        {
                                            ExchangeRateType.ToDT = Convert.ToDateTime(Item.GetDataKeyValue("ToDT"));
                                        }
                                        break;
                                    }
                                }
                                ExchangeRateType.ExchangeRateID = Convert.ToInt64(strExchangeRateID);
                                ExchangeRateType.ExchRateCD = tbCode.Text;
                                ExchangeRateType.ExchRateDescription = tbDescription.Text;
                                ExchangeRateType.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.ExchangeRate, Convert.ToInt64(strExchangeRateID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.ExchangeRate);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.ExchangeRate);
                                        return;
                                    }
                                    ExchangeRateService.DeleteExchangeRate(ExchangeRateType);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.ExchangeRate, Convert.ToInt64(strExchangeRateID));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSaveFlag.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgExchangeRatesCatalog.SelectedItems[0] as GridDataItem;
                                Session["SelectedExchangeRateID"] = item.GetDataKeyValue("ExchangeRateID").ToString().Trim();
                                if (btnSaveChanges.Visible == false)
                                {
                                    ReadOnlyForm();
                                }
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                    }
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgExchangeRatesCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbCode.Enabled = true;
                    hdnSaveFlag.Value = "Save";
                    // dgExchangeRatesCatalog.Rebind();
                    tbDescription.Text = string.Empty;
                    (((TextBox)ucFromDat.FindControl("tbDate")).Text) = string.Empty;
                    (((TextBox)ucToDate.FindControl("tbDate")).Text) = string.Empty;
                    tbExchangeRate.Text = "0000.00000";
                    //ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// ClearForm after Adding, editing & Deleting a Record
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbExchangeRate.Text = "0000.00000";
                    (((TextBox)ucFromDat.FindControl("tbDate")).Text) = string.Empty;
                    (((TextBox)ucToDate.FindControl("tbDate")).Text) = string.Empty;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Enable form for editing
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //tbCode.Enabled = Enable;
                        tbDescription.Enabled = Enable;
                        TextBox tbFromDate = (TextBox)ucFromDat.FindControl("tbDate");
                        tbFromDate.Enabled = Enable;
                        TextBox tbToDate = (TextBox)ucToDate.FindControl("tbDate");
                        tbToDate.Enabled = Enable;
                        tbExchangeRate.Enabled = Enable;
                        btnSaveChanges.Visible = Enable;
                        btnCancel.Visible = Enable;
                        chkInactive.Enabled = Enable;    
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedExchangeRateID"] != null)
                    {
                        foreach (GridDataItem Item in dgExchangeRatesCatalog.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("ExchangeRateID").ToString().Trim() == Session["SelectedExchangeRateID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("ExchRateCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("ExchRateCD").ToString();
                                }
                                else
                                {
                                    tbCode.Text = string.Empty;
                                }
                                tbCode.Enabled = false;
                                if (Item.GetDataKeyValue("ExchRateDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("ExchRateDescription").ToString();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("FromDT") != null)
                                {
                                    string FromDt = Item.GetDataKeyValue("FromDT").ToString().Trim();
                                    TextBox tbStartDate = (TextBox)ucFromDat.FindControl("tbDate");
                                    tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(FromDt)).Trim();
                                    // tbStartDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", FromDt).Substring(0, 10);
                                    //string FromDt = Item.GetDataKeyValue("FromDT").ToString().Trim();
                                    //tbFromDate.Text = Convert.ToString(Convert.ToDateTime(FromDt).ToString("MM/dd/yyyy"));
                                }
                                else
                                {
                                    TextBox tbStartDate = (TextBox)ucFromDat.FindControl("tbDate");
                                    tbStartDate.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ToDT") != null)
                                {
                                    string ToDt = Item.GetDataKeyValue("ToDT").ToString().Trim();
                                    TextBox tbToDate = (TextBox)ucToDate.FindControl("tbDate");
                                    tbToDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(ToDt)).Trim();
                                    //tbToDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ToDt).Substring(0, 10);
                                    //tbToDate.Text = Convert.ToString(Convert.ToDateTime(ToDt).ToString("MM/dd/yyyy"));
                                }
                                else
                                {
                                    TextBox tbToDate = (TextBox)ucToDate.FindControl("tbDate");
                                    tbToDate.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ExchRate") != null)
                                {
                                    tbExchangeRate.Text = Item.GetDataKeyValue("ExchRate").ToString();
                                }
                                else
                                {
                                    tbExchangeRate.Text = "0000.00000";
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                hdnSaveFlag.Value = "Update";
                                hdnRedirect.Value = "";
                                break;
                            }
                        }
                        EnableForm(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// for setting format for date in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgExchangeRatesCatalog_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem DataItem = e.Item as GridDataItem;
                            GridColumn Column = dgExchangeRatesCatalog.MasterTableView.GetColumn("FromDT");
                            string UwaValue = DataItem["FromDT"].Text.Trim();
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["FromDT"];
                            TableCell cell1 = (TableCell)Item["ToDT"];
                            if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                            {
                                cell.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(cell.Text.Trim())));
                            }
                            if ((!string.IsNullOrEmpty(cell1.Text)) && (cell1.Text != "&nbsp;"))
                            {
                                cell1.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(cell1.Text.Trim())));
                            }


                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {

                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// To display the controls in Read Only mode
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgExchangeRatesCatalog.SelectedItems.Count > 0)
                    {
                    GridDataItem Item = dgExchangeRatesCatalog.SelectedItems[0] as GridDataItem;
                    if (Item.GetDataKeyValue("ExchRateCD") != null)
                    {
                        tbCode.Text = Item.GetDataKeyValue("ExchRateCD").ToString();
                    }
                    else
                    {
                        tbCode.Text = string.Empty;
                    }
                    tbCode.Enabled = false;
                    if (Item.GetDataKeyValue("ExchRateDescription") != null)
                    {
                        tbDescription.Text = Item.GetDataKeyValue("ExchRateDescription").ToString();
                    }
                    else
                    {
                        tbDescription.Text = string.Empty;
                    }

                    if (Item.GetDataKeyValue("FromDT") != null)
                    {
                        if (DateFormat != null)
                        {
                            ((TextBox)ucFromDat.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("FromDT").ToString().Trim()));
                        }
                        else
                        {
                            ((TextBox)ucFromDat.FindControl("tbDate")).Text = Item.GetDataKeyValue("FromDT").ToString().Trim();
                        }
                    }
                    else
                    {
                        ((TextBox)ucFromDat.FindControl("tbDate")).Text = string.Empty;
                    }


                    //if (Item.GetDataKeyValue("FromDT") != null)
                    //{
                    //    string FromDt = Item.GetDataKeyValue("FromDT").ToString();
                    //    TextBox tbStartDate = (TextBox)ucFromDat.FindControl("tbDate");
                    //    tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(FromDt)).Substring(0, 11);
                    //    // tbStartDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", FromDt).Substring(0, 10);
                    //    //string FromDt = Item.GetDataKeyValue("FromDT").ToString().Trim();
                    //    //tbFromDate.Text = Convert.ToString(Convert.ToDateTime(FromDt).ToString("MM/dd/yyyy"));
                    //}
                    //else
                    //{
                    //    TextBox tbStartDate = (TextBox)ucFromDat.FindControl("tbDate");
                    //    tbStartDate.Text = string.Empty;
                    //}

                    //if (Item.GetDataKeyValue("ToDT") != null)
                    //{
                    //    string ToDt = Item.GetDataKeyValue("ToDT").ToString().Trim();
                    //    TextBox tbToDate = (TextBox)ucToDate.FindControl("tbDate");
                    //    tbToDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(ToDt)).Substring(0, 11);
                    //    // tbToDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ToDt).Substring(0, 10);
                    //    //tbToDate.Text = Convert.ToString(Convert.ToDateTime(ToDt).ToString("MM/dd/yyyy"));
                    //}
                    //else
                    //{
                    //    TextBox tbToDate = (TextBox)ucToDate.FindControl("tbDate");
                    //    tbToDate.Text = string.Empty;
                    //}

                    if (Item.GetDataKeyValue("ToDT") != null)
                    {
                        if (DateFormat != null)
                        {
                            ((TextBox)ucToDate.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("ToDT").ToString().Trim()));
                        }
                        else
                        {
                            ((TextBox)ucToDate.FindControl("tbDate")).Text = Item.GetDataKeyValue("ToDT").ToString().Trim();
                        }
                    }
                    else
                    {
                        ((TextBox)ucToDate.FindControl("tbDate")).Text = string.Empty;
                    }


                    if (Item.GetDataKeyValue("ExchRate") != null)
                    {
                        tbExchangeRate.Text = Item.GetDataKeyValue("ExchRate").ToString();
                    }
                    else
                    {
                        tbExchangeRate.Text = "0000.00000";
                    }
                    Label lbLastUpdatedUser = (Label)dgExchangeRatesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    if (Item.GetDataKeyValue("IsInActive") != null)
                    {
                        chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        chkInactive.Checked = false;
                    }
                    lbColumnName1.Text = "Rate Code";
                    lbColumnName2.Text = "Description";
                    lbColumnValue1.Text = Item["ExchRateCD"].Text;
                    lbColumnValue2.Text = Item["ExchRateDescription"].Text;
                    Item.Selected = true;
                    EnableForm(false);
                    }
                    else
                    {
                        DefaultSelection(true);
                    }

                    
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update ExchangeRate Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ValidateExchangeRateDateFormat();
                        string FromDT = string.Empty;
                        string ToDT = string.Empty;
                        string FromDate = (((TextBox)ucFromDat.FindControl("tbDate")).Text);
                        string ToDate = (((TextBox)ucToDate.FindControl("tbDate")).Text);
                        if (FromDate.Trim() != "" && ToDate.Trim() != "")
                        {
                            if ((!string.IsNullOrEmpty(FromDate)) && (!string.IsNullOrEmpty(ToDate)))
                            {
                                FromDT = String.Format("{0:" + DateFormat + "}", FromDate).Substring(0, 10);
                                ToDT = String.Format("{0:" + DateFormat + "}", ToDate).Substring(0, 10);
                            }
                            List<ExchangeRate> ExchangeRateCodeList = (List<ExchangeRate>)Session["ExchangeRateList"];
                            if (hdnSaveFlag.Value == "Update")
                            {
                                ExchangeRateCodeList = ExchangeRateCodeList.Where(x => x.ExchangeRateID != Convert.ToInt64(Session["SelectedExchangeRateID"])).ToList<ExchangeRate>();
                            }
                            FlightPakMasterService.ExchangeRate ExchangeRateService = new FlightPakMasterService.ExchangeRate();

                            var FromDTResults = (from code in ExchangeRateCodeList
                                                 where (code.ExchRateCD.ToString().Trim().ToUpper() == tbCode.Text.ToString().Trim().ToUpper())
                                                && ((Convert.ToDateTime(code.FromDT.ToString()) <= Convert.ToDateTime(FormatDate(FromDT.ToString(), DateFormat))) && (Convert.ToDateTime(code.ToDT.ToString()) >= Convert.ToDateTime(FormatDate(FromDT.ToString(), DateFormat))))
                                                 select code);

                            var ToDTResults = (from code in ExchangeRateCodeList
                                               where (code.ExchRateCD.ToString().Trim().ToUpper() == tbCode.Text.ToString().Trim().ToUpper())
                                              && ((Convert.ToDateTime(code.FromDT.ToString()) <= Convert.ToDateTime(FormatDate(ToDT.ToString(), DateFormat))) && (Convert.ToDateTime(code.ToDT.ToString()) >= Convert.ToDateTime(FormatDate(ToDT.ToString(), DateFormat))))
                                               select code);
                            if (ToDTResults.Count() > 0 && FromDTResults.Count() > 0)
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Code already exists for same Date Range', 360, 50, 'Exchange Rate');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            }
                            else if (FromDTResults.Count() > 0)
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Exchange rate already exist for the date FROM DATE.', 360, 50, 'Exchange Rate');";
                                //string AlertMsg = "radalert('From Date should be different for same Code', 360, 50, 'Exchange Rate');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            }
                            else if (ToDTResults.Count() > 0)
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Exchange rate already exist for the date TO DATE.', 360, 50, 'Exchange Rate');";
                                //string AlertMsg = "radalert('To Date should be different for same Code', 360, 50, 'Exchange Rate');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            }
                            else
                            {
                                if (DateStart > DateEnd)
                                {
                                    
                                    string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                        + @" oManager.radalert('From Date cannot be earlier than To Date', 360, 50, 'Exchange Rate');";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                }
                                else
                                {
                                    double ExchangeRate = Convert.ToDouble(tbExchangeRate.Text);
                                    if (ExchangeRate <= 0000.0000)
                                    {
                                        //cvExchangeRate.IsValid = false;
                                        tbExchangeRate.Focus();
                                    }
                                    else if (hdnSaveFlag.Value == "Update")
                                    {
                                        (dgExchangeRatesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                    }
                                    else
                                    {
                                        (dgExchangeRatesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                                    }
                                }
                            }
                        }
                        else if (FromDate.Trim() == "" && ToDate.Trim() != "")
                        {
                            
                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('From Date cannot be blank', 360, 50, 'Exchange Rate');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        }
                        else if (FromDate.Trim() != "" && ToDate.Trim() == "")
                        {
                            
                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('To Date cannot be blank', 360, 50, 'Exchange Rate');";
                            //string AlertMsg = "radalert('From Date should be different for same Code', 360, 50, 'Exchange Rate');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);

                        }
                        else
                        {
                            
                            string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('From Date and To Date cannot be blank', 360, 50, 'Exchange Rate');";
                            //string AlertMsg = "radalert('From Date should be different for same Code', 360, 50, 'Exchange Rate');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSaveFlag.Value = "";

                    if (IsPopUp)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('RadExchangeMasterPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                    }

                    else if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// Cancel ExchangeRate catalog form information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            if (Session["SelectedExchangeRateID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.ExchangeRate, Convert.ToInt64(Session["SelectedExchangeRateID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        //Session.Remove("SelectedExchangeRateID");
                        EnableLinks();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radAddExchangeRate');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }

            hdnSaveFlag.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// Filter the items in the Grid in ExchangeRate Catalog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnclrFilters_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        foreach (GridColumn Column in dgExchangeRatesCatalog.MasterTableView.Columns)
                        {
                            //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                            //column.CurrentFilterValue = string.Empty;
                        }
                        dgExchangeRatesCatalog.MasterTableView.FilterExpression = string.Empty;
                        dgExchangeRatesCatalog.MasterTableView.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// To enable or disable the icons in the grid for insert, edit or delete
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgExchangeRatesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDeleteCtl = (LinkButton)dgExchangeRatesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgExchangeRatesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddExchangeRatesCatalog))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteExchangeRatesCatalog))
                    {
                        lbtnDeleteCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDeleteCtl.Enabled = true;
                            lbtnDeleteCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtnDeleteCtl.Enabled = false;
                            lbtnDeleteCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDeleteCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditExchangeRatesCatalog))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Get the Items in the ExcahngeRate catalog
        /// </summary>
        /// <param name="ExchangeRateCode"></param>
        /// <returns></returns>
        private ExchangeRate GetItems()
        {
            FlightPakMasterService.ExchangeRate ExchangeRateService = new FlightPakMasterService.ExchangeRate();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            if (Session["SelectedExchangeRateID"] != null)
                            {
                                string Code = Session["SelectedExchangeRateID"].ToString().Trim();
                                ExchangeRateService.ExchangeRateID = Convert.ToInt64(Code);
                            }
                        }
                        ExchangeRateService.ExchRateCD = tbCode.Text;
                        ExchangeRateService.ExchRateDescription = tbDescription.Text;
                        tbFromDate = (((TextBox)ucFromDat.FindControl("tbDate")).Text);
                        tbToDate = (((TextBox)ucToDate.FindControl("tbDate")).Text);

                        if (!string.IsNullOrEmpty(tbFromDate))
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                ExchangeRateService.FromDT = Convert.ToDateTime(FormatDate(tbFromDate, DateFormat));
                            }
                            else
                            {
                                ExchangeRateService.FromDT = Convert.ToDateTime(tbFromDate);
                            }
                        }


                        if (!string.IsNullOrEmpty(tbToDate))
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                ExchangeRateService.ToDT = Convert.ToDateTime(FormatDate(tbToDate, DateFormat));
                            }
                            else
                            {
                                ExchangeRateService.ToDT = Convert.ToDateTime(tbToDate);
                            }
                        }

                        //if (!string.IsNullOrEmpty(tbFromDate))
                        //{
                        //    ExchangeRateService.FromDT = Convert.ToDateTime(tbFromDate);
                        //}

                        //if (!string.IsNullOrEmpty(tbToDate))
                        //{
                        //    //ExchangeRateService.ToDT = DateTime.ParseExact(tbToDate.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        //    ExchangeRateService.ToDT = Convert.ToDateTime(tbToDate);
                        //}
                        if (!string.IsNullOrEmpty(tbExchangeRate.Text))
                        {
                            ExchangeRateService.ExchRate = Convert.ToDecimal(tbExchangeRate.Text, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            ExchangeRateService.ExchRate = Convert.ToDecimal("0000.00000", CultureInfo.CurrentCulture);
                        }
                        ExchangeRateService.IsInActive = chkInactive.Checked;
                        ExchangeRateService.IsDeleted = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
            return ExchangeRateService;
        }
        /// <summary>
        /// To Check whether the code is available for the same FromDate and Todate
        /// </summary>
        /// <returns></returns>
        private string CheckAllReadyExist()
        {
            string ReturnValue = "";
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string FromDT = string.Empty;
                        string ToDT = string.Empty;
                        if (Session["ExchangeRateList"] != null)
                        {
                            string FromDate = (((TextBox)ucFromDat.FindControl("tbDate")).Text);
                            string ToDate = (((TextBox)ucToDate.FindControl("tbDate")).Text);
                            if ((!string.IsNullOrEmpty(FromDate)) && (!string.IsNullOrEmpty(ToDate)))
                            {
                                FromDT = String.Format("{0:" + DateFormat + "}", FromDate).Substring(0, 10);
                                ToDT = String.Format("{0:" + DateFormat + "}", ToDate).Substring(0, 10);
                            }
                            List<ExchangeRate> ExchangeRateCodeList = (List<ExchangeRate>)Session["ExchangeRateList"];
                            FlightPakMasterService.ExchangeRate ExchangeRateService = new FlightPakMasterService.ExchangeRate();

                            var FromDTResults = (from code in ExchangeRateCodeList
                                                 where (code.ExchRateCD.ToString().Trim().ToUpper() == tbCode.Text.ToString().Trim().ToUpper())
                                                && ((Convert.ToDateTime(code.FromDT.ToString()) >= Convert.ToDateTime(FromDT.ToString())) || (Convert.ToDateTime(code.ToDT.ToString()) <= Convert.ToDateTime(FromDT.ToString())))
                                                 select code);

                            var ToDTResults = (from code in ExchangeRateCodeList
                                               where (code.ExchRateCD.ToString().Trim().ToUpper() == tbCode.Text.ToString().Trim().ToUpper())
                                              && ((Convert.ToDateTime(code.FromDT.ToString()) >= Convert.ToDateTime(ToDT.ToString())) || (Convert.ToDateTime(code.ToDT.ToString()) <= Convert.ToDateTime(ToDT.ToString())))
                                               select code);

                            if (FromDTResults.Count() > 0 && FromDTResults != null)
                            {
                                cvFromDate.IsValid = false;
                                cvToDate.ErrorMessage = "From Date should be different for same Code";
                                IsValidateCustom = false;
                            }
                            else if (ToDTResults.Count() > 0 && ToDTResults != null)
                            {
                                cvToDate.IsValid = false;
                                cvToDate.ErrorMessage = "To Date should be different for same Code";
                                IsValidateCustom = false;
                            }
                            else if (ToDTResults.Count() > 0 && FromDTResults.Count() != null)
                            {
                                cvToDate.IsValid = false;
                                cvToDate.ErrorMessage = "Code already exists for same Date Range";
                                IsValidateCustom = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
            return ReturnValue;
        }
        private void ValidateExchangeRateDateFormat()
        {
            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
            string StartDate = ((TextBox)ucFromDat.FindControl("tbDate")).Text;
            string EndDate = ((TextBox)ucToDate.FindControl("tbDate")).Text;

            if (StartDate.Trim() != "" && EndDate.Trim() != "")
            {
                //if (string.IsNullOrEmpty(StartDate))
                //{
                //    cvFromDate.IsValid = false;
                //    cvFromDate.ErrorMessage = "From Date is Required";
                //    IsDateCheck = false;
                //}
                //if (string.IsNullOrEmpty(EndDate))
                //{
                //    cvToDate.IsValid = false;
                //    cvToDate.ErrorMessage = "To Date is Required";
                //    IsDateCheck = false;
                //}
                //if (IsDateCheck)
                //{
                if (!string.IsNullOrEmpty(StartDate))
                {
                    if (DateFormat != null)
                    {
                        DateStart = Convert.ToDateTime(FormatDate(StartDate, DateFormat));
                    }
                    else
                    {
                        DateStart = Convert.ToDateTime(StartDate);
                    }
                }
                if (!string.IsNullOrEmpty(EndDate))
                {
                    if (DateFormat != null)
                    {
                        DateEnd = Convert.ToDateTime(FormatDate(EndDate, DateFormat));
                    }
                    else
                    {
                        DateEnd = Convert.ToDateTime(EndDate);
                    }
                }
                //}
            }

        }
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                //IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.ExchangeRate> lstExchangeRate = new List<FlightPakMasterService.ExchangeRate>();
                if (Session["ExchangeRateList"] != null)
                {
                    lstExchangeRate = (List<FlightPakMasterService.ExchangeRate>)Session["ExchangeRateList"];
                }
                if (lstExchangeRate.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstExchangeRate = lstExchangeRate.Where(x => x.IsInActive == false).ToList<ExchangeRate>(); }
                    dgExchangeRatesCatalog.DataSource = lstExchangeRate;
                    if (IsDataBind)
                    {
                        dgExchangeRatesCatalog.DataBind();
                    }
                }
               
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgExchangeRatesCatalog.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var ExchangeRateValue = FPKMstService.GetExchangeRate();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, ExchangeRateValue);
            List<FlightPakMasterService.ExchangeRate> filteredList = GetFilteredList(ExchangeRateValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.ExchangeRateID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgExchangeRatesCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgExchangeRatesCatalog.CurrentPageIndex = PageNumber;
            dgExchangeRatesCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfExchangeRate ExchangeRateValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedExchangeRateID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = ExchangeRateValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().ExchangeRateID;
                Session["SelectedExchangeRateID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.ExchangeRate> GetFilteredList(ReturnValueOfExchangeRate ExchangeRateValue)
        {
            List<FlightPakMasterService.ExchangeRate> filteredList = new List<FlightPakMasterService.ExchangeRate>();

            if (ExchangeRateValue.ReturnFlag)
            {
                filteredList = ExchangeRateValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<ExchangeRate>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            if (!IsPostBack && IsPopUp)
            {
                //Hide Controls
                dgExchangeRatesCatalog.Visible = false;
                chkSearchActiveOnly.Visible = false;
                if (IsAdd)
                {
                    (dgExchangeRatesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                }
                else
                {
                    (dgExchangeRatesCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                }
            }
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgExchangeRatesCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgExchangeRatesCatalog.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        protected void dgExchangeRatesCatalog_PreRender1(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgExchangeRatesCatalog, Page.Session);
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
    }
}
