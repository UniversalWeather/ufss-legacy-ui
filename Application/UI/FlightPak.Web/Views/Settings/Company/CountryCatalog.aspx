﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="CountryCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Company.CountryCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="Server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/ecmascript"></script>
    <script type="text/javascript">
        function deleterecord() {

            var masterTable = $find('<%= dgCountry.ClientID %>').get_masterTableView();
            if (masterTable.get_selectedItems().length > 0) {
                var cell1 = masterTable.getCellByColumnUniqueName(masterTable.get_selectedItems()[0], "chkUWAID");
                if (cell1.innerHTML.substring(0, 3) == "UWA") {
                    alert('You Cant Delete UWA Record');
                    return false;
                }
                else {

                    if (confirm('Are you sure you want to delete this record?'))
                        return true;
                    else
                        return false;
                }
            }
            else {
                alert('Please select a record from the above table.');
                return false;

            }

        }

    </script>
    <script type="text/javascript">
        function CheckTxtBox(control) {


            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);
                    //document.getElementById('tbCode').setFocus();
                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);
                    //document.getElementById('tbDescription').setFocus();
                } return false;
            }
        }
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>

    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td class="headerText">
                <div class="tab-nav-top">
                    <span class="head-title">Country</span> <span class="tab-nav-icons">
                    
                    <a href="../../Help/ViewHelp.aspx?Screen=CountryHelp" target="_blank" title="Help"
                        class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCountry">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="cvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadGrid ID="dgCountry" runat="server" AllowSorting="true" OnItemCreated="dgCountry_ItemCreated"
        OnNeedDataSource="dgCountry_BindData" OnItemCommand="dgCountry_ItemCommand" OnUpdateCommand="dgCountry_UpdateCommand"
        OnInsertCommand="dgCountry_InsertCommand" OnPreRender="dgCountry_PreRender" OnPageIndexChanged="dgCountry_PageIndexChanged"
        AutoGenerateColumns="false" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgCountry_SelectedIndexChanged"
        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="341px">
        <MasterTableView DataKeyNames="CountryID,CountryCD,CountryName,ISOCD,LastUpdUID,LastUpdDT"
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Country Code" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px" FilterDelay="500"
                    FilterControlWidth="100px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CountryName" HeaderText="Description" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="520px" FilterDelay="500"
                    FilterControlWidth="500px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ISOCD" HeaderText="ISO Code" AutoPostBackOnFilter="false"
                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px" FilterDelay="500"
                    FilterControlWidth="100px">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                        Visible="false"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        ToolTip="Edit" CommandName="Edit" Visible="false"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                </div>
                <div>
                    <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
    </telerik:RadGrid>
    <div id="divExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td valign="top" class="tdLabel100">
                        <span class="mnd_text">Country Code</span>
                    </td>
                    <td class="tdLabel200" valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbCode" AutoPostBack="true" OnTextChanged="Code_TextChanged" runat="server"
                                        MaxLength="2" CssClass="text50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="Save" ControlToValidate="tbCode"
                                        Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Country Code is Required.</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Country Code is Required"
                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" class="tdLabel80">
                        <asp:Label ID="lbISOCode" Text="ISO Code" runat="server"></asp:Label></span>
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="tbIsoCode" runat="server" MaxLength="3" CssClass="text50 inpt_non_edit"
                            onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                        <asp:HiddenField ID="hdnHomeBase" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="tdLabel80">
                        <span class="mnd_text">Description</span>
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbDescription" runat="server" MaxLength="60" CssClass="text180"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="Save"
                                        ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Description is Required.</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" ValidationGroup="Save" runat="server"
                            OnClick="SaveChanges_Click" CssClass="button" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                            OnClick="Cancel_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
