﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.Company
{
    public partial class CustomFlightLog : BaseSecuredPage
    {
        int x;
        string HomeBase;
        Int64 HomeBaseID;
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        HomeBase = (Request.QueryString["HomeBase"]);
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewCompanyProfileCatalog);
                            if (!string.IsNullOrEmpty(HomeBase))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var CompanyData = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD == HomeBase);
                                    tbBaseDescription.Enabled = false;
                                    tbMainBase.Enabled = false;
                                    foreach (FlightPakMasterService.GetAllCompanyMaster companyEntity in CompanyData)
                                    {
                                        if (companyEntity.HomebaseID != null)
                                        {
                                            hdnHomeBaseID.Value = companyEntity.HomebaseID.ToString();
                                        }
                                        if (companyEntity.HomebaseCD != null)
                                        {
                                            tbMainBase.Text = companyEntity.HomebaseCD;
                                        }
                                        if (companyEntity.BaseDescription != null)
                                        {
                                            tbBaseDescription.Text = companyEntity.BaseDescription;
                                        }
                                    }
                                }
                            }

                            SetDefaultSortOrder();
                            if (string.IsNullOrEmpty(Request.QueryString["EditModeFlag"]))
                            {
                                LinkButton insertCtl, editCtl, delCtl;
                                insertCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                                delCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                                editCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                                insertCtl.Visible = delCtl.Visible = editCtl.Visible = btnReset.Visible = false;

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }

        private void SetDefaultSortOrder()
        {
            GridSortExpression sortExpr = new GridSortExpression();
            sortExpr.FieldName = "OriginalDescription";
            sortExpr.SortOrder = GridSortOrder.Ascending;
            dgCustomFlight.MasterTableView.AllowNaturalSort = false;
            dgCustomFlight.MasterTableView.SortExpressions.AddSortExpression(sortExpr);
            dgCustomFlight.Rebind();
        }

        protected void dgCountry_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCustomFlight.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }

        protected void dgCustomFlight_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSFlightLog oTSFlightLog = new TSFlightLog();
                        oTSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetAllFlightLogList(oTSFlightLog);
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgCustomFlight.DataSource = ObjRetVal.EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }
        protected void CustomFlight_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem Item = (GridDataItem)e.Item;
                if ((Item["desc"] != null) && ((Label)Item["desc"].FindControl("lbDesc")).Text.Trim() != string.Empty)
                {
                    if (!((Label)Item["desc"].FindControl("lbDesc")).Text.Trim().ToUpper().Contains("CREW LOG CUSTOM LABEL"))
                        ((Label)Item["desc"].FindControl("lbDesc")).Text = ((Label)Item["desc"].FindControl("lbDesc")).Text.ToUpper().Trim();
                }

                if (hdnSave.Value == "Save")
                {
                    if ((Item.GetDataKeyValue("TSFlightLogID") != null) && (Convert.ToInt64(Item.GetDataKeyValue("TSFlightLogID")) != 0))
                    {
                        ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = false;
                        ((RadNumericTextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = false;
                        ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = false;
                    }
                    else
                    {
                        ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = true;
                        ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Focus();
                        ((RadNumericTextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = true;
                        ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = true;
                    }
                }
                else if (hdnSave.Value == "Update")
                {
                    ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = true;
                    ((RadNumericTextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = true;
                    ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = true;
                }
                else if (hdnSave.Value == "Delete")
                {
                    ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = false;
                    ((RadNumericTextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = false;
                    ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = false;
                }
                else if (string.IsNullOrEmpty(hdnSave.Value))
                {
                    ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = false;
                    ((RadNumericTextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = false;
                    ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = false;
                }

            }
        }
        protected void dgCustomFlight_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            //Apply custom sorting

            GridSortExpression sortExpr = new GridSortExpression();
            switch (e.OldSortOrder)
            {
                case GridSortOrder.None:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
               case GridSortOrder.Ascending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Descending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Descending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;

            }
            e.Canceled = true;
            dgCustomFlight.Rebind();

        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCustomFlight;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }
        protected void lbtnInitInsert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = "Save";
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<Int64> OverallValue = new List<Int64>();
                            List<int> OverallMaxOrderValue = new List<int>();
                            List<GetAllFlightLog> lstTSFlightLog = new List<GetAllFlightLog>();
                            List<GetAllFlightLog> OrderCount = new List<GetAllFlightLog>();
                            GetAllFlightLog TSFlightLogdef = new GetAllFlightLog();
                            TSFlightLogdef.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                            TSFlightLog oTSFlightLog = new TSFlightLog();
                            oTSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                            var objFlightLog = Service.GetAllFlightLogList(oTSFlightLog);
                            if (objFlightLog.ReturnFlag == true)
                            {
                                lstTSFlightLog = objFlightLog.EntityList.Where(x => x.OriginalDescription.ToLower().Trim().Contains("crew log custom label")).ToList();
                                OrderCount = objFlightLog.EntityList.ToList();
                            }
                            Int64 CCLCount = 0;
                            int OrderNumber = 0;
                            if (lstTSFlightLog.Count != 0)
                            {
                                //CCLCount = Convert.ToInt64(lstTSFlightLog[lstTSFlightLog.Count - 1].OriginalDescription.ToUpper().Trim().Replace("CREWLOG CUSTOM LABEL", ""));
                                foreach (GetAllFlightLog Item in lstTSFlightLog)
                                {
                                    OverallValue.Add(Convert.ToInt64(Item.OriginalDescription.ToLower().Trim().Replace("crew log custom label", "")));
                                }
                                CCLCount = OverallValue.Max();
                                foreach (GetAllFlightLog Item in OrderCount)
                                {
                                    OverallMaxOrderValue.Add(Convert.ToInt32(Item.SequenceOrder));
                                }
                                OrderNumber = OverallMaxOrderValue.Max() + 1;
                            }
                            List<TSFlightLog> FinalList = new List<TSFlightLog>();
                            List<TSFlightLog> RetainList = new List<TSFlightLog>();
                            List<TSFlightLog> NewList = new List<TSFlightLog>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainCrewVisaGrid(dgCustomFlight);
                            FinalList.AddRange(RetainList);
                            // Bind Selected New Item and add into Final List
                            TSFlightLog TSFlightLog = new TSFlightLog();
                            if (CCLCount != 2)
                            {
                                TSFlightLog.OriginalDescription = "Crew Log Custom Label " + (CCLCount + 1);
                                TSFlightLog.CustomDescription = "Crew Log Custom Label " + (CCLCount + 1);
                            }
                            else if (CCLCount == 2)
                            {
                                TSFlightLog.OriginalDescription = "Crew Log Custom Label " + (CCLCount + 3);
                                TSFlightLog.CustomDescription = "Crew Log Custom Label " + (CCLCount + 3);
                            }
                            TSFlightLog.SequenceOrder = OrderNumber;
                            TSFlightLog.IsPrint = true;
                            TSFlightLog.IsDeleted = false;
                            NewList.Add(TSFlightLog);
                            FinalList.AddRange(NewList);
                            // Bind final list into Session
                            Session["CrewVisa"] = FinalList;
                            dgCustomFlight.DataSource = FinalList;
                            dgCustomFlight.Rebind();
                            LinkButton insertCtl, editCtl, delCtl;
                            insertCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                            delCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                            editCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                            insertCtl.Enabled = false;
                            delCtl.Enabled = false;
                            editCtl.Enabled = false;
                            //dgCustomFlight.Enabled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }
        protected void lbtnInitEdit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = "Update";
                        List<TSFlightLog> RetainList = new List<TSFlightLog>();
                        RetainList = RetainCrewVisaGrid(dgCustomFlight);
                        dgCustomFlight.DataSource = RetainList;
                        dgCustomFlight.Rebind();
                        if (Session["OriginalDescription"] != null)
                        {
                            foreach (GridDataItem item in dgCustomFlight.MasterTableView.Items)
                            {
                                if (item["desc"].Text.Trim() == Session["OriginalDescription"].ToString().Trim())
                                {
                                    item.Selected = true;
                                    break;
                                }
                            }
                        }
                        //dgCustomFlight.Enabled = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }
        protected void lbtnDelete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = "Delete";
                        GridDataItem Item = (GridDataItem)dgCustomFlight.SelectedItems[0];
                        TSDefinitionLog TSFlightLog = new TSDefinitionLog();
                        TSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                        TSFlightLog.IsDeleted = true;
                        TSFlightLog.TSDefinitionLogID = Convert.ToInt64(Item.GetDataKeyValue("TSFlightLogID"));
                        using (MasterCatalogServiceClient TsFlightService = new MasterCatalogServiceClient())
                        {
                            TsFlightService.DeleteTSDefinitionLog(TSFlightLog);
                        }
                        dgCustomFlight.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = string.Empty;
                        foreach (GridDataItem Item in dgCustomFlight.MasterTableView.Items)
                        {
                            TSDefinitionLog TSFlightLog = new TSDefinitionLog();
                            TSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                            TSFlightLog.PrimeKey = x;// Hardcoded until DB is ready
                            if (!string.IsNullOrEmpty(((Label)Item["desc"].FindControl("lbDesc")).Text))
                            {
                                TSFlightLog.OriginalDescription = ((Label)Item["desc"].FindControl("lbDesc")).Text;
                            }
                            if (!string.IsNullOrEmpty(((TextBox)Item["custdesc"].FindControl("tbCustDesc")).Text))
                            {
                                TSFlightLog.CustomDescription = ((TextBox)Item["custdesc"].FindControl("tbCustDesc")).Text;
                            }
                            if (!string.IsNullOrEmpty(((RadNumericTextBox)Item["Order"].FindControl("tbOrder")).Value.ToString()))
                            {
                                TSFlightLog.SequenceOrder = Convert.ToInt32(((RadNumericTextBox)Item["Order"].FindControl("tbOrder")).Value.ToString());
                            }
                            if ((((CheckBox)Item["IsPrint"].FindControl("chkIsPrint")).Checked))
                            {
                                TSFlightLog.IsPrint = true;
                            }
                            else
                            {
                                TSFlightLog.IsPrint = false;
                            }
                            if (Item.GetDataKeyValue("TSFlightLogID") != null)
                            {
                                TSFlightLog.TSDefinitionLogID = Convert.ToInt64(Item.GetDataKeyValue("TSFlightLogID"));
                            }
                            else
                            {
                                TSFlightLog.TSDefinitionLogID = 0;
                            }
                            TSFlightLog.IsDeleted = false;
                            TSFlightLog.Category = "Flight Log";
                            using (MasterCatalogServiceClient TsFlightService = new MasterCatalogServiceClient())
                            {
                                var Result = TsFlightService.AddTSDefinitionLog(TSFlightLog);
                                if (Result.ReturnFlag == true)
                                {
                                    //ShowSuccessMessage();
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowSuccessMessage", "ShowSuccessMessage();", true);
                                }
                            }
                        }
                        List<TSFlightLog> FinalList = new List<TSFlightLog>();
                        List<TSFlightLog> RetainList = new List<TSFlightLog>();
                        List<TSFlightLog> NewList = new List<TSFlightLog>();
                        // Retain Grid Items and bind into List and add into Final List
                        //RetainList = RetainCrewVisaGrid(dgCustomFlight);
                        //FinalList.AddRange(RetainList);
                        //dgCustomFlight.DataSource = FinalList;
                        //dgCustomFlight.Rebind();
                        TSFlightLog oTSFlightLog = new TSFlightLog();
                        oTSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetAllFlightLogList(oTSFlightLog);
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgCustomFlight.DataSource = ObjRetVal.EntityList;
                            }
                            dgCustomFlight.Rebind();
                            LinkButton insertCtl, editCtl, delCtl;
                            insertCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                            delCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                            editCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                            insertCtl.Enabled = true;
                            delCtl.Enabled = true;
                            editCtl.Enabled = true;
                            //dgCustomFlight.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = string.Empty;
                        LinkButton insertCtl, editCtl, delCtl;
                        insertCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        delCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        editCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        insertCtl.Enabled = true;
                        delCtl.Enabled = true;
                        editCtl.Enabled = true;
                        dgCustomFlight.Rebind();
                        //dgCustomFlight.Enabled = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }
        protected void Reset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<TSFlightLog> lstTSFlightLog = new List<TSFlightLog>();
                            TSFlightLog oTSFlightLog = new TSFlightLog();
                            oTSFlightLog.TSFlightLogID = 0;
                            oTSFlightLog.CustomerID = 0;
                            oTSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                            oTSFlightLog.Category = "Flight Log";
                            oTSFlightLog.IsDeleted = false;
                            if (hdnSave.Value == "ResetYes")
                            {
                                oTSFlightLog.IsPrint = true;
                            }
                            else if (hdnSave.Value == "ResetNo")
                            {
                                oTSFlightLog.IsPrint = false;
                            }
                            var ObjRetVal = ObjService.ResetFlightLog(oTSFlightLog);
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgCustomFlight.DataSource = ObjRetVal.EntityList;
                            }
                            hdnSave.Value = string.Empty;
                            dgCustomFlight.Rebind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }
        private List<TSFlightLog> RetainCrewVisaGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<TSFlightLog> TSFlightLogList = new List<TSFlightLog>();
                foreach (GridDataItem Item in grid.MasterTableView.Items)
                {
                    TSFlightLog TSFlightLog = new TSFlightLog();
                    if (Item.GetDataKeyValue("TSFlightLogID") != null)
                    {
                        TSFlightLog.TSFlightLogID = Convert.ToInt64(Item.GetDataKeyValue("TSFlightLogID").ToString());
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["desc"].FindControl("lbDesc")).Text))
                    {
                        TSFlightLog.OriginalDescription = ((Label)Item["desc"].FindControl("lbDesc")).Text;
                    }
                    if (!string.IsNullOrEmpty(((TextBox)Item["custdesc"].FindControl("tbCustDesc")).Text))
                    {
                        TSFlightLog.CustomDescription = ((TextBox)Item["custdesc"].FindControl("tbCustDesc")).Text;
                    }
                    if (!string.IsNullOrEmpty(((RadNumericTextBox)Item["Order"].FindControl("tbOrder")).Value.ToString()))
                    {
                        TSFlightLog.SequenceOrder = Convert.ToInt32(((RadNumericTextBox)Item["Order"].FindControl("tbOrder")).Value.ToString());
                    }
                    if ((((CheckBox)Item["IsPrint"].FindControl("chkIsPrint")).Checked))
                    {
                        TSFlightLog.IsPrint = true;
                    }
                    else
                    {
                        TSFlightLog.IsPrint = false;
                    }
                    TSFlightLog.IsDeleted = false;
                    TSFlightLogList.Add(TSFlightLog);
                }
                return TSFlightLogList;
            }
        }

        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, editCtl, delCtl;
                insertCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                delCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                editCtl = (LinkButton)dgCustomFlight.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                if (IsAuthorized(Permission.Database.AddCompanyProfileCatalog))
                {
                    insertCtl.Visible = true;
                    if (add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteCompanyProfileCatalog))
                {
                    delCtl.Visible = true;
                    if (delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditCompanyProfileCatalog))
                {
                    editCtl.Visible = true;
                    if (edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
            }
        }
        private void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //btnSaveChanges.Enabled = Enable;
                //btnCancel.Enabled = Enable;
                //btnReset.Enabled = Enable;
            }
        }

        protected void dgCustomFlight_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem Item = dgCustomFlight.SelectedItems[0] as GridDataItem;
                        Session["OriginalDescription"] = Item["desc"].Text;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }
    }
}