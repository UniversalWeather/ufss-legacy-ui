﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class CrewCurrencyPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (Session["CrewCurrencyDefaults"] != null)
                            {
                                Dictionary<string, string> dictRetrieveCrewCurrency = new Dictionary<string, string>();
                                dictRetrieveCrewCurrency = (Dictionary<string, string>)Session["CrewCurrencyDefaults"];

                                tbDLDayQueried.Text = dictRetrieveCrewCurrency["DLDayQueried"];
                                tbDLMin.Text = dictRetrieveCrewCurrency["DLMin"];
                                tbNLDayQueried.Text = dictRetrieveCrewCurrency["NLDayQueried"];
                                tbNLMinimum.Text = dictRetrieveCrewCurrency["NLMinimum"];
                                tbApproachesDaysQueried.Text = dictRetrieveCrewCurrency["ApproachesDaysQueried"];
                                tbInstrumentDaysQueried.Text = dictRetrieveCrewCurrency["InstrumentDaysQueried"];
                                tbApproachesMin.Text = dictRetrieveCrewCurrency["ApproachesMin"];
                                tbInstrumentDaysMin.Text = dictRetrieveCrewCurrency["InstrumentMin"];
                                tbTdDq.Text = dictRetrieveCrewCurrency["TdDq"];
                                tbTdMin.Text = dictRetrieveCrewCurrency["TdMin"];
                                tbTnDq.Text = dictRetrieveCrewCurrency["TnDq"];
                                tbTnMin.Text = dictRetrieveCrewCurrency["TnMin"];
                                tbDaysDQ.Text = dictRetrieveCrewCurrency["DaysDQ"];
                                tbDaysMax.Text = dictRetrieveCrewCurrency["DaysMax"];
                                tbDays2DQ.Text = dictRetrieveCrewCurrency["Days2DQ"];
                                tbDays2Max.Text = dictRetrieveCrewCurrency["Days2Max"];
                                tbCmDaysMax.Text = dictRetrieveCrewCurrency["CmDaysMax"];
                                tbCmDaysDq.Text = dictRetrieveCrewCurrency["CmDaysDq"];
                                tbCmDaysMaximum.Text = dictRetrieveCrewCurrency["CmDaysMaximum"];
                                tbCqMax.Text = dictRetrieveCrewCurrency["CqMax"];
                                tbCqDq.Text = dictRetrieveCrewCurrency["CqDq"];
                                tbCqMaximum.Text = dictRetrieveCrewCurrency["CqMaximum"];
                                tbPqMax.Text = dictRetrieveCrewCurrency["PqMax"];
                                tbPqDq.Text = dictRetrieveCrewCurrency["PqDq"];
                                tbPqMaximum.Text = dictRetrieveCrewCurrency["PqMaximum"];
                                tbCyMax.Text = dictRetrieveCrewCurrency["CyMax"];
                                tbCyDaysDq.Text = dictRetrieveCrewCurrency["CyDaysDq"];
                                tbCyDaysMaximum.Text = dictRetrieveCrewCurrency["CyDaysMaximum"];
                                tbCyDayRestDq.Text = dictRetrieveCrewCurrency["CyDayRestDq"];
                                tbCyDayRestMin.Text = dictRetrieveCrewCurrency["CyDayRestMin"];
                                tbCqrMin.Text = dictRetrieveCrewCurrency["CqrMin"];
                                lbDays.Text = System.Web.HttpUtility.HtmlEncode(tbDaysDQ.Text);
                                lbDays2.Text = System.Web.HttpUtility.HtmlEncode(tbDays2DQ.Text);
                                lbCmDays.Text = System.Web.HttpUtility.HtmlEncode(tbCmDaysDq.Text);
                                lbCqMonths.Text = System.Web.HttpUtility.HtmlEncode(tbCqDq.Text);
                                lbCyDays.Text = System.Web.HttpUtility.HtmlEncode(tbCyDaysDq.Text);
                                lbPqMonths.Text = System.Web.HttpUtility.HtmlEncode(tbPqDq.Text);
                                lbCyDaysRest.Text = System.Web.HttpUtility.HtmlEncode(tbCyDayRestDq.Text);
                                if (string.IsNullOrEmpty(Request.QueryString["EditModeFlag"]))
                                {
                                    DisableControl(false);
                                }
                            }
                            else
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    if (Session["HomeBaseID"] != null)
                                    {
                                        var CompanyData = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseID == Convert.ToInt64(Session["HomeBaseID"].ToString())).ToList();

                                        if (CompanyData.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(CompanyData[0].DayLanding.ToString()))
                                                tbDLDayQueried.Text = CompanyData[0].DayLanding.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].DayLandingMinimum.ToString()))
                                                tbDLMin.Text = CompanyData[0].DayLandingMinimum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].NightLanding.ToString()))
                                                tbNLDayQueried.Text = CompanyData[0].NightLanding.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].NightllandingMinimum.ToString()))
                                                tbNLMinimum.Text = CompanyData[0].NightllandingMinimum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].Approach.ToString()))
                                                tbApproachesDaysQueried.Text = CompanyData[0].Approach.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].Instrument.ToString()))
                                                tbInstrumentDaysQueried.Text = CompanyData[0].Instrument.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].ApproachMinimum.ToString()))
                                                tbApproachesMin.Text = CompanyData[0].ApproachMinimum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].InstrumentMinimum.ToString()))
                                                tbInstrumentDaysMin.Text = CompanyData[0].InstrumentMinimum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].TakeoffDay.ToString()))
                                                tbTdDq.Text = CompanyData[0].TakeoffDay.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].TakeoffDayMinimum.ToString()))
                                                tbTdMin.Text = CompanyData[0].TakeoffDayMinimum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].TakeoffNight.ToString()))
                                                tbTnDq.Text = CompanyData[0].TakeoffNight.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].TakeofNightMinimum.ToString()))
                                                tbTnMin.Text = CompanyData[0].TakeofNightMinimum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].Day7.ToString()))
                                                tbDaysDQ.Text = CompanyData[0].Day7.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].DayMaximum7.ToString()))
                                                tbDaysMax.Text = CompanyData[0].DayMaximum7.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].Day30.ToString()))
                                                tbDays2DQ.Text = CompanyData[0].Day30.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].DayMaximum30.ToString()))
                                                tbDays2Max.Text = CompanyData[0].DayMaximum30.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].CalendarMonthMaximum.ToString()))
                                                tbCmDaysMax.Text = CompanyData[0].CalendarMonthMaximum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].Day90.ToString()))
                                                tbCmDaysDq.Text = CompanyData[0].Day90.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].DayMaximum90.ToString()))
                                                tbCmDaysMaximum.Text = CompanyData[0].DayMaximum90.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].CalendarQTRMaximum.ToString()))
                                                tbCqMax.Text = CompanyData[0].CalendarQTRMaximum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].Month6.ToString()))
                                                tbCqDq.Text = CompanyData[0].Month6.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].MonthMaximum6.ToString()))
                                                tbCqMaximum.Text = CompanyData[0].MonthMaximum6.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].RestDaysMinimum.ToString()))
                                                tbCyDayRestMin.Text = CompanyData[0].RestDaysMinimum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].RestCalendarQTRMinimum.ToString()))
                                                tbCqrMin.Text = CompanyData[0].RestCalendarQTRMinimum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].Previous2QTRMaximum.ToString()))
                                                tbPqMax.Text = CompanyData[0].Previous2QTRMaximum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].Month12.ToString()))
                                                tbPqDq.Text = CompanyData[0].Month12.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].MonthMaximum12.ToString()))
                                                tbPqMaximum.Text = CompanyData[0].MonthMaximum12.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].CalendarYearMaximum.ToString()))
                                                tbCyMax.Text = CompanyData[0].CalendarYearMaximum.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].Day365.ToString()))
                                                tbCyDaysDq.Text = CompanyData[0].Day365.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].DayMaximum365.ToString()))
                                                tbCyDaysMaximum.Text = CompanyData[0].DayMaximum365.ToString();

                                            if (!string.IsNullOrEmpty(CompanyData[0].RestDays.ToString()))
                                                tbCyDayRestDq.Text = CompanyData[0].RestDays.ToString();

                                            lbDays.Text = System.Web.HttpUtility.HtmlEncode(tbDaysDQ.Text);
                                            lbDays2.Text = System.Web.HttpUtility.HtmlEncode(tbDays2DQ.Text);
                                            lbCmDays.Text = System.Web.HttpUtility.HtmlEncode(tbCmDaysDq.Text);
                                            lbCqMonths.Text = System.Web.HttpUtility.HtmlEncode(tbCqDq.Text);
                                            lbCyDays.Text = System.Web.HttpUtility.HtmlEncode(tbCyDaysDq.Text);
                                            lbPqMonths.Text = System.Web.HttpUtility.HtmlEncode(tbPqDq.Text);
                                            lbCyDaysRest.Text = System.Web.HttpUtility.HtmlEncode(tbCyDayRestDq.Text);

                                            if (string.IsNullOrEmpty(Request.QueryString["EditModeFlag"]))
                                            {
                                                    DisableControl(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCurrency);
                }
            }

        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, string> dictSetCrewCurrencyDefaults = new Dictionary<string, string>();
                        dictSetCrewCurrencyDefaults.Add("DLDayQueried", tbDLDayQueried.Text);
                        dictSetCrewCurrencyDefaults.Add("DLMin", tbDLMin.Text);
                        dictSetCrewCurrencyDefaults.Add("NLDayQueried", tbNLDayQueried.Text);
                        dictSetCrewCurrencyDefaults.Add("NLMinimum", tbNLMinimum.Text);
                        dictSetCrewCurrencyDefaults.Add("ApproachesDaysQueried", tbApproachesDaysQueried.Text);
                        dictSetCrewCurrencyDefaults.Add("InstrumentDaysQueried", tbInstrumentDaysQueried.Text);
                        dictSetCrewCurrencyDefaults.Add("ApproachesMin", tbApproachesMin.Text);
                        dictSetCrewCurrencyDefaults.Add("InstrumentMin", tbInstrumentDaysMin.Text);
                        dictSetCrewCurrencyDefaults.Add("TdDq", tbTdDq.Text);
                        dictSetCrewCurrencyDefaults.Add("TdMin", tbTdMin.Text);
                        dictSetCrewCurrencyDefaults.Add("TnDq", tbTnDq.Text);
                        dictSetCrewCurrencyDefaults.Add("TnMin", tbTnMin.Text);
                        dictSetCrewCurrencyDefaults.Add("DaysDQ", tbDaysDQ.Text);
                        dictSetCrewCurrencyDefaults.Add("DaysMax", tbDaysMax.Text);
                        dictSetCrewCurrencyDefaults.Add("Days2DQ", tbDays2DQ.Text);
                        dictSetCrewCurrencyDefaults.Add("Days2Max", tbDays2Max.Text);
                        dictSetCrewCurrencyDefaults.Add("CmDaysMax", tbCmDaysMax.Text);
                        dictSetCrewCurrencyDefaults.Add("CmDaysDq", tbCmDaysDq.Text);
                        dictSetCrewCurrencyDefaults.Add("CmDaysMaximum", tbCmDaysMaximum.Text);
                        dictSetCrewCurrencyDefaults.Add("CqMax", tbCqMax.Text);
                        dictSetCrewCurrencyDefaults.Add("CqDq", tbCqDq.Text);
                        dictSetCrewCurrencyDefaults.Add("CqMaximum", tbCqMaximum.Text);
                        dictSetCrewCurrencyDefaults.Add("PqMax", tbPqMax.Text);
                        dictSetCrewCurrencyDefaults.Add("PqDq", tbPqDq.Text);
                        dictSetCrewCurrencyDefaults.Add("PqMaximum", tbPqMaximum.Text);
                        dictSetCrewCurrencyDefaults.Add("CyMax", tbCyMax.Text);
                        dictSetCrewCurrencyDefaults.Add("CyDaysDq", tbCyDaysDq.Text);
                        dictSetCrewCurrencyDefaults.Add("CyDaysMaximum", tbCyDaysMaximum.Text);
                        dictSetCrewCurrencyDefaults.Add("CyDayRestDq", tbCyDayRestDq.Text);
                        dictSetCrewCurrencyDefaults.Add("CyDayRestMin", tbCyDayRestMin.Text);
                        dictSetCrewCurrencyDefaults.Add("CqrMin", tbCqrMin.Text);
                        Session.Add("CrewCurrencyDefaults", dictSetCrewCurrencyDefaults);
                        //lbScript.Visible = true;
                        //lbScript.Text = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode("<script type='text/javascript'>Close()</" + "script>"));
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "Close();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCurrency);
                }
            }

        }

        protected void DisableControl(bool enable)
        {

            tbDLDayQueried.Enabled =  tbDLMin.Enabled =  tbNLDayQueried.Enabled = 
            tbNLMinimum.Enabled =tbApproachesDaysQueried.Enabled = tbInstrumentDaysQueried.Enabled = tbApproachesMin.Enabled = tbInstrumentDaysMin.Enabled = 
            tbTdDq.Enabled = tbTdMin.Enabled = tbTnDq.Enabled = tbTnMin.Enabled = tbDaysDQ.Enabled = tbDaysMax.Enabled = tbDays2DQ.Enabled =
            tbDays2Max.Enabled = tbCmDaysMax.Enabled = tbCmDaysDq.Enabled = tbCmDaysMaximum.Enabled = tbCqMax.Enabled = tbCqDq.Enabled =
            tbCqMaximum.Enabled = tbCyDayRestMin.Enabled = tbCqrMin.Enabled = tbPqMax.Enabled = tbPqDq.Enabled = tbPqMaximum.Enabled = tbCyMax.Enabled =
            tbCyDaysDq.Enabled = tbCyDaysMaximum.Enabled = tbCyDayRestDq.Enabled = btnSubmit.Visible=enable;
            
        }
    }
}