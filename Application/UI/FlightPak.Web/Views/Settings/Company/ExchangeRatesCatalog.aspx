﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="ExchangeRatesCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Company.ExchangeRatesCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">            // This method is used for Tabout message

            //This method is used for numeric textbox
            function ValidateEmptyTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0000.00000";
                }
            }
            
        </script>
    </telerik:RadCodeBlock>
    <script type="text/javascript">
        function CheckTxtBox(control) {
            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;
            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);
                } return false;
            }
            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);
                } return false;
            }
        }
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSaveFlag.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgExchangeRatesCatalog">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgExchangeRatesCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgExchangeRatesCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgExchangeRatesCatalog">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgExchangeRatesCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbDescription">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbDescription" />
                    <telerik:AjaxUpdatedControl ControlID="rfvDescription" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="rfvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <%-- <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" runat="server">
        </DateInput>
    </telerik:RadDatePicker>--%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%;" cellspacing="0" cellpadding="0">
        <tr style="display: none;">
            <td>
                <uc:DatePicker ID="ucDatePicker" runat="server" re></uc:DatePicker>
            </td>
        </tr>
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Exchange Rate</span> <span class="tab-nav-icons">
                        <!--<a href="#"
                        class="search-icon"></a><a href="#" class="save-icon"></a><a href="#" class="print-icon">
                        </a>-->
                        <a href="../../Help/ViewHelp.aspx?Screen=ExchangeRateHelp" target="_blank" title="Help"
                            class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgExchangeRatesCatalog" runat="server" AllowSorting="true" OnItemCreated="dgExchangeRatesCatalog_ItemCreated"
                OnNeedDataSource="dgExchangeRatesCatalog_BindData" OnItemCommand="dgExchangeRatesCatalog_ItemCommand"
                OnPageIndexChanged="dgExchangeRatesCatalog_PageIndexChanged" OnUpdateCommand="dgExchangeRatesCatalog_UpdateCommand"
                OnInsertCommand="dgExchangeRatesCatalog_InsertCommand" OnDeleteCommand="dgExchangeRatesCatalog_DeleteCommand"
                AutoGenerateColumns="false" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgExchangeRatesCatalog_SelectedIndexChanged"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="341px"
                OnItemDataBound="dgExchangeRatesCatalog_ItemDataBound" OnPreRender="dgExchangeRatesCatalog_PreRender1">
                <MasterTableView DataKeyNames="ExchangeRateID,ExchRateCD,ExchRateDescription,FromDT,ToDT,ExchRate,LastUpdTS,LastUpdUID,CurrencyID,IsDeleted,IsInActive"
                    CommandItemDisplay="Bottom" Width="100%">
                    <Columns>
                        <telerik:GridBoundColumn DataField="ExchRateCD" HeaderText="Exchange Rate Code" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="140px" FilterControlWidth="120px" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ExchRateDescription" HeaderText="Description"
                            CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="240px"
                            FilterControlWidth="220px" AutoPostBackOnFilter="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FromDT" HeaderText="From Date" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="100px" AllowFiltering="false" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" UniqueName="FromDT" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ToDT" HeaderText="To Date" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="100px" AllowFiltering="false" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" UniqueName="ToDT" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ExchRate" HeaderText="Exchange Rate" CurrentFilterFunction="EqualTo"
                            HeaderStyle-Width="100px" ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            HeaderStyle-Width="80px" ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left;">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddExchangeRatesCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                Visible='<%# IsAuthorized(Permission.Database.EditExchangeRatesCatalog)%>' OnClientClick="javascript:return ProcessUpdate();"><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                                runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeleteExchangeRatesCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box" width="100%">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td valign="top" class="tdLabel100">
                                    <span class="mnd_text">Rate Code</span>
                                </td>
                                <td class="tdLabel230">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCode" runat="server" MaxLength="6" CssClass="text40" onKeyPress="return fnAllowAlpha(this, event,'.')"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="tbCode"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"
                                                    ErrorMessage="Rate Code is Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" class="tdLabel100">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" runat="server" MaxLength="30" CssClass="text180"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="tbDescription"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"
                                                    ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top" class="tdLabel100">
                                    <span class="mnd_text">From Date</span>
                                </td>
                                <td valign="top" class="tdLabel230">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <uc:DatePicker ID="ucFromDat" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <%--   <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="ucFromDat"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true" 
                                                    ErrorMessage="From Date is Required"></asp:RequiredFieldValidator>--%>
                                                            <%--  <asp:CustomValidator ID="cvFromDate2" runat="server" ErrorMessage="Date Of Birth should not be future date."
                                                                            ClientValidationFunction="fncClientCheckDate" ValidationGroup="save" CssClass="alert-text"></asp:CustomValidator>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CustomValidator ID="cvFromDate" runat="server" ErrorMessage="" Display="Dynamic"
                                                                CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" class="tdLabel100">
                                    <span class="mnd_text">To Date</span>
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <uc:DatePicker ID="ucToDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <%-- <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="ucToDate"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"
                                                    ErrorMessage="To Date is Required"></asp:RequiredFieldValidator>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CustomValidator ID="cvToDate" runat="server" ErrorMessage="" Display="Dynamic"
                                                                CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top" class="tdLabel100">
                                    <span class="mnd_text">Exchange Rate</span>
                                </td>
                                <td class="pr_radtextbox_100">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <%--<asp:TextBox ID="tbExchangeRate" runat="server" MaxLength="10" CssClass="text120"
                                                    ValidationGroup="save" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                    onBlur="return ValidateEmptyTextbox(this, event),RemoveSpecialChars(this);"></asp:TextBox>--%>
                                                <telerik:RadNumericTextBox ID="tbExchangeRate" runat="server" Type="Currency" Culture="en-US"
                                                    MaxLength="5" Value="0.0000" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                </telerik:RadNumericTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvExchangeRate" runat="server" ControlToValidate="tbExchangeRate"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"
                                                    ErrorMessage="Exchange Rate is Required"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revExchangeRate" runat="server" ControlToValidate="tbExchangeRate"
                                                    EnableClientScript="True" CssClass="alert-text" ValidationGroup="save" ErrorMessage="Expected Format(0000.00000)"
                                                    Display="Dynamic" ValidationExpression="^[0-9]{0,4}(\.[0-9]{1,5})?$"></asp:RegularExpressionValidator>
                                                <asp:CustomValidator ID="cvExchangeRate" runat="server" ControlToValidate="tbExchangeRate"
                                                    ErrorMessage="Exchange rate cannot be negative or zero" Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td style="text-align: right">
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="save"
                            OnClick="SaveChanges_Click" CssClass="button" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClick="Cancel_Click" />
                        <asp:HiddenField ID="hdnSaveFlag" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
