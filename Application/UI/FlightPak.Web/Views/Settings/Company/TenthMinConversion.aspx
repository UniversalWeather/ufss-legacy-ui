﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TenthMinConversion.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.TenthMinConversion" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tenths-Min Conversion</title>
     <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function returnToParent() {
                //create the argument that will be returned to the parent page

                oArg = new Object();
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            function OnClientClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg == '' || arg == null) {
                    // No need to refresh RadGrid 
                }
                else {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest(arg);
                }

            }

            function Close() {
                //alert('clos called');
                // GetRadWindow().BrowserWindow.refreshGrid();
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <div>
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="TenthMinConversion">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="TenthMinConversion" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="TenthMinConversion" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table width="210px" style="background: #F6F6F6; padding: 5px;">
            <tr>
                <td class="tdLabel60">
                    Tenth
                </td>
                <td class="tdLabel80">
                    Start Min
                </td>
                <td class="tdLabel80">
                    End Min
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth0" runat="server" CssClass="RadMaskedTextBox25" Text="0.0"
                        Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin0" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin0" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth1" runat="server" CssClass="RadMaskedTextBox25" Text="0.1"
                        MaxLength="2" Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin1" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin1" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth2" runat="server" CssClass="RadMaskedTextBox25" Text="0.2"
                        MaxLength="2" Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin2" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin2" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth3" runat="server" CssClass="RadMaskedTextBox25" Text="0.3"
                        MaxLength="2" Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin3" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin3" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth4" runat="server" CssClass="RadMaskedTextBox25" Text="0.4"
                        MaxLength="2" Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin4" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin4" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth5" runat="server" CssClass="RadMaskedTextBox25" Text="0.5"
                        MaxLength="2" Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin5" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin5" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth6" runat="server" CssClass="RadMaskedTextBox25" Text="0.6"
                        MaxLength="2" Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin6" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin6" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth7" runat="server" CssClass="RadMaskedTextBox25" Text="0.7"
                        MaxLength="2" Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin7" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin7" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth8" runat="server" CssClass="RadMaskedTextBox25" Text="0.8"
                        MaxLength="2" Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin8" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin8" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbTenth9" runat="server" CssClass="RadMaskedTextBox25" Text="0.9"
                        MaxLength="2" Enabled="false">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbStartMin9" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="tbEndMin9" runat="server" CssClass="RadMaskedTextBox25" Text="0"
                        MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="3">
                    <table cellpadding="0" cellspacing="0" class="margin0">
                        <tr>
                            <td>
                                <asp:Button ID="btnSaveChanges" Text="Ok" runat="server" ValidationGroup="Save" OnClick="SaveChanges_Click"
                                    CssClass="button-tenth" />
                                <asp:Label ID="lbScript" runat="server" Text="" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
