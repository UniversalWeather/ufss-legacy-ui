﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common;
using FlightPak.Web.FlightPakMasterService;
using System.IO;
using System.Text.RegularExpressions;

//security
using Microsoft.Security.Application;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class CompanyInvoiceDefaults : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public Int64 HomeBaseID;
        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"]; }
            set { Session["CqInvoiceDicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["CqInvoiceImgDelete"]; }
            set { Session["CqInvoiceImgDelete"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["HomeBaseID"]))
                        {
                            HomeBaseID = Convert.ToInt64(Request.QueryString["HomeBaseID"].Trim());
                        }

                        if (HomeBaseID != null)
                        {
                            ltlHomeBase.Text = "Invoice Defaults";
                            using (MasterCatalogServiceClient objMaster = new MasterCatalogServiceClient())
                            {
                                var objCompany = objMaster.GetListInfoByHomeBaseId(HomeBaseID).EntityList;
                                if (objCompany != null && objCompany.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(objCompany[0].HomebaseCD))
                                    {
                                        ltlHomeBase.Text = System.Web.HttpUtility.HtmlEncode( "Invoice Defaults " + "- " + objCompany[0].HomebaseCD + " - " + objCompany[0].BaseDescription);
                                    }
                                    else
                                    {
                                        ltlHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Invoice Defaults");
                                    }
                                }
                                else
                                {
                                    ltlHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Invoice Defaults");
                                }
                            }
                        }
                        if (!IsPostBack)
                        {
                            ClearFields();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                            fileUL.Enabled = true;
                            ddlImg.Enabled = true;
                            btndeleteImage.Enabled = true;
                            LoadImages();
                            //MR15042013
                            if (Session["CompanyInsert"] != "Insert" && Session["CQInvoiceFooterList"] == null)
                            {
                                LoadFooter();
                            }
                            else
                            {
                                ddlFooterInfo.Items.Add(new ListItem("None", "0"));
                                ddlFooterInfo.DataBind();
                            }
                            if (Session["CQInvoiceFooterList"] != null)
                            {
                                List<FlightPakMasterService.CQMessage> FooterListInvoice = new List<FlightPakMasterService.CQMessage>();
                                FooterListInvoice = (List<FlightPakMasterService.CQMessage>)Session["CQInvoiceFooterList"];
                                ddlFooterInfo.DataSource = FooterListInvoice;
                                ddlFooterInfo.DataTextField = "MessageDescription";
                                ddlFooterInfo.DataValueField = "MessageLine";
                                ddlFooterInfo.DataBind();
                                if (Session["CQInvoiceFooter"] != null)
                                {
                                    string Item = Session["CQInvoiceFooter"].ToString();
                                    if (ddlFooterInfo.Items.FindByText(Item) != null)
                                    {
                                        ddlFooterInfo.Items.FindByText(Item).Selected = true;
                                        tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text;
                                    }
                                }
                            }
                            if (HomeBaseID != null && HomeBaseID != 0)
                            {
                                HomeBaseID = Convert.ToInt64(Request.QueryString["HomeBaseID"].Trim());
                                if ((Session["CqCompanyStringInvoiceReport"] == null) && (Session["CqCompanyCheckInvoiceReport"] == null) && (Session["CqCompanyRadInvoiceReport"] == null))
                                {
                                    LoadQuoteInvoiceDefaults();
                                }
                                else
                                    AssignInvoiceReport();
                            }
                            else
                            {
                                if ((Session["CqCompanyStringInvoiceReport"] == null) && (Session["CqCompanyCheckInvoiceReport"] == null) && (Session["CqCompanyRadInvoiceReport"] == null))
                                {
                                    LoadSummaryDescription();

                                }
                                else
                                {
                                    AssignInvoiceReport();

                                }
                            }
                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                            else if (eventArgument == "DeleteImage_Click")
                            {
                                DeleteImage_Click(sender, e);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        private void LoadSummaryDescription()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbDailyDesc.Text = "DAILY USAGE ADJUSTMENT:";
                tbAdditionalDesc.Text = "ADDITIONAL FEES:";
                tbLandingDesc.Text = "LANDING FEES:";
                tbSegmentDesc.Text = "SEGMENT FEES:";
                tbRonCrewDesc.Text = "RON CREW CHARGE:";
                tbCrewAddCrewDesc.Text = "ADDITIONAL CREW CHARGE:";
                tbAddCrewRonDesc.Text = "ADDITIONAL CREW RON CHARGE:";
                tbWaitDesc.Text = "WAIT CHARGE:";
                tbFlightDesc.Text = "FLIGHT CHARGE:";
                tbSubtotalDesc.Text = "SUBTOTAL:";
                tbDiscountDesc.Text = "DISCOUNT AMOUNT:";
                tbTaxesDesc.Text = "TAXES:";
                tbTotalInvoiceDesc.Text = "TOTAL INVOICE:";
                tbPrepayDesc.Text = "TOTAL PREPAY:";
                tbRemainingInvoiceDesc.Text = "TOTAL REMAINING INVOICE:";
            }
        }

        private void LoadFooter()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var RetValue = objMasterService.GetCQMessage("I", HomeBaseID).EntityList.ToList();
                    if (RetValue != null && RetValue.Count() > 0)
                    {
                        ddlFooterInfo.Items.Add(new ListItem("None", "0"));
                        ddlFooterInfo.DataSource = RetValue;
                        ddlFooterInfo.DataTextField = "MessageDescription";
                        ddlFooterInfo.DataValueField = "MessageCD";
                        ddlFooterInfo.DataBind();
                        if (Session["CQInvoiceFooter"] != null)
                        {
                            string Item = Session["CQInvoiceFooter"].ToString();
                            if (ddlFooterInfo.Items.FindByText(Item) != null)
                            {
                                ddlFooterInfo.Items.FindByText(Item).Selected = true;
                                tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text;
                            }
                        }
                    }
                    else
                    {
                        ddlFooterInfo.Items.Add(new ListItem("None", "0"));
                        ddlFooterInfo.DataBind();
                    }
                }
            }
        }

        private void ClearFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCharterCompInfo.Text = string.Empty;
                tbTopOffset.Text = string.Empty;
                chkCityDetail.Checked = false;
                chkAirportName.Checked = false;
                chkIcaoId.Checked = false;
                tbCustomHeaderInfo.Text = string.Empty;
                tbCustomFooterInfo.Text = string.Empty;
                fileUL.Enabled = false;
                ddlImg.Enabled = false;
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                //ImgPopup.ImageUrl = null;
                ddlImg.Items.Clear();
                ddlImg.Text = "";
                tbImgName.Text = string.Empty;
                radlstLogoPosition.SelectedIndex = 0;
                chkPrintFirstPage.Checked = false;
                chkDailyUsage.Checked = false;
                tbDailyDesc.Text = string.Empty;
                chkAdditionalPrint.Checked = false;
                tbAdditionalDesc.Text = string.Empty;
                chkLandingPrint.Checked = false;
                tbLandingDesc.Text = string.Empty;
                chkSegmentPrint.Checked = false;
                tbSegmentDesc.Text = string.Empty;
                chkRonCrewPrint.Checked = false;
                tbRonCrewDesc.Text = string.Empty;
                chkAddCrewPrint.Checked = false;
                tbCrewAddCrewDesc.Text = string.Empty;
                chkAddCrewRonPrint.Checked = false;
                tbAddCrewRonDesc.Text = string.Empty;
                chkWaitPrint.Checked = false;
                tbWaitDesc.Text = string.Empty;
                chkFlightPrint.Checked = false;
                tbFlightDesc.Text = string.Empty;
                chkSubtotalPrint.Checked = false;
                tbSubtotalDesc.Text = string.Empty;
                chkDiscountPrint.Checked = false;
                tbDiscountDesc.Text = string.Empty;
                chkTaxesAmt.Checked = false;
                tbTaxesDesc.Text = string.Empty;
                chkTotalInvoicePrint.Checked = false;
                tbTotalInvoiceDesc.Text = string.Empty;
                chkTotalPrepayPrint.Checked = false;
                tbPrepayDesc.Text = string.Empty;
                chkRemainingInvoicePrint.Checked = false;
                tbRemainingInvoiceDesc.Text = string.Empty;
                tbReportTitle.Text = string.Empty;
                chkDetail.Checked = false;
                chkSubTotal.Checked = false;
                chkAdditionalFees.Checked = false;
                chkInvoiceSummary.Checked = false;
                chkAdditionalSummary.Checked = false;
                chkLegNum.Checked = false;
                chkDepartDate.Checked = false;
                chkFromDescription.Checked = false;
                chkToDescription.Checked = false;
                chkInvoiceAmount.Checked = false;
                chkFlightHours.Checked = false;
                chkMiles.Checked = false;
                chkPassengerCount.Checked = false;
                chkInvoiceFeeDescription.Checked = false;
                chkInvoicedAmt.Checked = false;
                chkChargeUnit.Checked = false;
                chkQuantity.Checked = false;
                chkRate.Checked = false;
                chkCharterCompanyInfo.Checked = false;
                chkCustomerInfo.Checked = false;
                chkDescription.Checked = false;
                chkTailNumber.Checked = false;
                chkAircraftTypeCode.Checked = false;
                tbCustomFooterInfo.Text = string.Empty;
                tbCustomFooterInfo.Enabled = false;
                chkIsInvoiceRptArrivalDate.Checked = false;
                chkIsInvoiceRptQuoteDate.Checked = false;

                //radSalesLogoPosition.SelectedIndex = 0;
            }
        }

        private void LoadQuoteInvoiceDefaults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(HomeBaseID).EntityList.ToList();
                    if (CompanyList.Count > 0)
                    {
                        GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                        if (CompanyCatalogEntity.ChtQuoteCompanyNameINV != null)
                        {
                            tbCharterCompInfo.Text = CompanyCatalogEntity.ChtQuoteCompanyNameINV;
                        }
                        if (CompanyCatalogEntity.QuotePageOffsettingInvoice != null)
                        {
                            tbTopOffset.Text = CompanyCatalogEntity.QuotePageOffsettingInvoice.ToString();
                        }
                        if (CompanyCatalogEntity.InvoiceTitle != null)
                        {
                            tbReportTitle.Text = CompanyCatalogEntity.InvoiceTitle.ToString();
                        }
                        if (CompanyCatalogEntity.IsCityDescription != null)
                        {
                            chkCityDetail.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCityDescription);
                        }
                        if (CompanyCatalogEntity.IsAirportDescription != null)
                        {
                            chkAirportName.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAirportDescription);
                        }
                        if (CompanyCatalogEntity.IsQuoteICAODescription != null)
                        {
                            chkIcaoId.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteICAODescription);
                        }

                        if (CompanyCatalogEntity.IsInvoiceRptArrivalDate != null)
                        {
                            chkIsInvoiceRptArrivalDate.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptArrivalDate);
                        }
                        else
                        {
                            chkIsInvoiceRptArrivalDate.Checked = false;
                        }


                        if (CompanyCatalogEntity.IsInvoiceRptQuoteDate != null)
                        {
                            chkIsInvoiceRptQuoteDate.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptQuoteDate);
                        }
                        else
                        {
                            chkIsInvoiceRptQuoteDate.Checked = false;
                        }


                        if (CompanyCatalogEntity.ChtQuoteHeaderINV != null)
                        {
                            tbCustomHeaderInfo.Text = CompanyCatalogEntity.ChtQuoteHeaderINV;
                        }
                        //if (CompanyCatalogEntity.ChtQouteFooterDetailRpt != null)
                        //{
                        //    tbCustomFooterInfo.Text = CompanyCatalogEntity.ChtQouteFooterDetailRpt;
                        //}
                        if (CompanyCatalogEntity.IFirstPG != null)
                        {
                            chkPrintFirstPage.Checked = Convert.ToBoolean(CompanyCatalogEntity.IFirstPG);
                        }
                        if (CompanyCatalogEntity.IlogoPOS != null)
                        {
                            radlstLogoPosition.SelectedIndex = Convert.ToInt32(CompanyCatalogEntity.IlogoPOS);
                        }

                        if (CompanyCatalogEntity.InvoiceSalesLogoPosition != null)
                        {
                            //radSalesLogoPosition.SelectedIndex = Convert.ToInt32(CompanyCatalogEntity.InvoiceSalesLogoPosition);
                        }

                        #region Information tab2

                        if (CompanyCatalogEntity.IsInvoiceDTsystemDT != null)
                        {
                            chkDetail.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceDTsystemDT);
                        }
                        if (CompanyCatalogEntity.IsQuotePrintSubtotal != null)
                        {
                            chkSubTotal.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrintSubtotal);
                        }
                        if (CompanyCatalogEntity.IsQuoteDetailPrint != null)
                        {
                            chkAdditionalFees.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailPrint);
                        }
                        if (CompanyCatalogEntity.IsQuotePrintSum != null)
                        {
                            chkInvoiceSummary.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrintSum);
                        }
                        if (CompanyCatalogEntity.isQuotePrintFees != null)
                        {
                            chkAdditionalSummary.Checked = Convert.ToBoolean(CompanyCatalogEntity.isQuotePrintFees);
                        }
                        if (CompanyCatalogEntity.IsQuotePrepaidMinimUsageFeeAmt != null)
                        {
                            chkDailyUsage.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidMinimUsageFeeAmt);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteMinimumUseFeeDepositAmt))
                        {
                            tbDailyDesc.Text = CompanyCatalogEntity.QuoteMinimumUseFeeDepositAmt.ToString();
                        }
                        else
                        {
                            tbDailyDesc.Text = "DAILY USAGE ADJUSTMENT:";
                        }
                        if (CompanyCatalogEntity.IsQuotePrintAdditionalFee != null)
                        {
                            chkAdditionalPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrintAdditionalFee);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAdditionalFeeDefault))
                        {
                            tbAdditionalDesc.Text = CompanyCatalogEntity.QuoteAdditionalFeeDefault.ToString();
                        }
                        else
                        {
                            tbAdditionalDesc.Text = "ADDITIONAL FEES:";
                        }
                        if (CompanyCatalogEntity.IsQuoteFlightLandingFeePrint != null)
                        {
                            chkLandingPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFlightLandingFeePrint);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteLandingFeeDefault))
                        {
                            tbLandingDesc.Text = CompanyCatalogEntity.QuoteLandingFeeDefault.ToString();
                        }
                        else
                        {
                            tbLandingDesc.Text = "LANDING FEES:";
                        }
                        if (CompanyCatalogEntity.IsQuotePrepaidSegementFee != null)
                        {
                            chkSegmentPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidSegementFee);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteSegmentFeeDeposit))
                        {
                            tbSegmentDesc.Text = CompanyCatalogEntity.QuoteSegmentFeeDeposit.ToString();
                        }
                        else
                        {
                            tbSegmentDesc.Text = "SEGMENT FEES:";
                        }
                        if (CompanyCatalogEntity.IsQuoteStdCrewRonPrepaid != null)
                        {
                            chkRonCrewPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteStdCrewRonPrepaid);
                        }
                        if (CompanyCatalogEntity.QuoteStdCrewRONDeposit != null)
                        {
                            tbRonCrewDesc.Text = CompanyCatalogEntity.QuoteStdCrewRONDeposit.ToString();
                        }
                        else
                        {
                            tbRonCrewDesc.Text = "RON CREW CHARGE:";
                        }
                        if (CompanyCatalogEntity.IsQuoteAdditionalCrewPrepaid != null)
                        {
                            chkAddCrewPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditionalCrewPrepaid);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAdditonCrewDeposit))
                        {
                            tbCrewAddCrewDesc.Text = CompanyCatalogEntity.QuoteAdditonCrewDeposit;
                        }
                        else
                        {
                            tbCrewAddCrewDesc.Text = "ADDITIONAL CREW CHARGE:";
                        }
                        if (CompanyCatalogEntity.IsQuoteAdditonalCrewRON != null)
                        {
                            chkAddCrewRonPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditonalCrewRON);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAdditionCrewRON))
                        {
                            tbAddCrewRonDesc.Text = CompanyCatalogEntity.QuoteAdditionCrewRON;
                        }
                        else
                        {
                            tbAddCrewRonDesc.Text = "ADDITIONAL CREW RON CHARGE:";
                        }
                        if (CompanyCatalogEntity.IsQuoteWaitingChargePrepaid != null)
                        {
                            chkWaitPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteWaitingChargePrepaid);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteWaitingCharge))
                        {
                            tbWaitDesc.Text = CompanyCatalogEntity.QuoteWaitingCharge;
                        }
                        else
                        {
                            tbWaitDesc.Text = "WAIT CHARGE:";
                        }
                        if (CompanyCatalogEntity.IsQuoteFlightChargePrepaid != null)
                        {
                            chkFlightPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFlightChargePrepaid);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.DepostFlightCharge))
                        {
                            tbFlightDesc.Text = CompanyCatalogEntity.DepostFlightCharge;
                        }
                        else
                        {
                            tbFlightDesc.Text = "FLIGHT CHARGE:";
                        }
                        if (CompanyCatalogEntity.IsQuoteSubtotalPrint != null)
                        {
                            chkSubtotalPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteSubtotalPrint);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteSubtotal))
                        {
                            tbSubtotalDesc.Text = CompanyCatalogEntity.QuoteSubtotal;
                        }
                        else
                        {
                            tbSubtotalDesc.Text = "SUBTOTAL:";
                        }
                        if (CompanyCatalogEntity.IsQuoteDispcountAmtPrint != null)
                        {
                            chkDiscountPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDispcountAmtPrint);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.DiscountAmountDefault))
                        {
                            tbDiscountDesc.Text = CompanyCatalogEntity.DiscountAmountDefault;
                        }
                        else
                        {
                            tbDiscountDesc.Text = "DISCOUNT AMOUNT:";
                        }
                        if (CompanyCatalogEntity.IsQuoteTaxesPrint != null)
                        {
                            chkTaxesAmt.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteTaxesPrint);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteTaxesDefault))
                        {
                            tbTaxesDesc.Text = CompanyCatalogEntity.QuoteTaxesDefault;
                        }
                        else
                        {
                            tbTaxesDesc.Text = "TAXES:";
                        }
                        if (CompanyCatalogEntity.IsQuoteInvoicePrint != null)
                        {
                            chkTotalInvoicePrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteInvoicePrint);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.DefaultInvoice))
                        {
                            tbTotalInvoiceDesc.Text = CompanyCatalogEntity.DefaultInvoice;
                        }
                        else
                        {
                            tbTotalInvoiceDesc.Text = "TOTAL INVOICE:";
                        }
                        if (CompanyCatalogEntity.IsQuotePrepayPrint != null)
                        {
                            chkTotalPrepayPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepayPrint);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuotePrepayDefault))
                        {
                            tbPrepayDesc.Text = CompanyCatalogEntity.QuotePrepayDefault;
                        }
                        else
                        {
                            tbPrepayDesc.Text = "TOTAL PREPAY:";
                        }
                        if (CompanyCatalogEntity.IsQuoteRemainingAmtPrint != null)
                        {
                            chkRemainingInvoicePrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteRemainingAmtPrint);
                        }
                        if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteRemainingAmountDefault))
                        {
                            tbRemainingInvoiceDesc.Text = CompanyCatalogEntity.QuoteRemainingAmountDefault;
                        }
                        else
                        {
                            tbRemainingInvoiceDesc.Text = "TOTAL REMAINING INVOICE:";
                        }
                        #endregion
                        #region QuoteDetailColumnsToPrint
                        if (CompanyCatalogEntity.IsInvoiceRptLegNum != null)
                        {
                            chkLegNum.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptLegNum);
                        }
                        if (CompanyCatalogEntity.IsInvoiceRptDetailDT != null)
                        {
                            chkDepartDate.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptDetailDT);
                        }
                        if (CompanyCatalogEntity.IsInvoiceRptFromDescription != null)
                        {
                            chkFromDescription.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptFromDescription);
                        }
                        if (CompanyCatalogEntity.IsQuoteDetailRptToDescription != null)
                        {
                            chkToDescription.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailRptToDescription);
                        }
                        if (CompanyCatalogEntity.IsQuoteDetailInvoiceAmt != null)
                        {
                            chkInvoiceAmount.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailInvoiceAmt);
                        }
                        if (CompanyCatalogEntity.IsQuoteDetailFlightHours != null)
                        {
                            chkFlightHours.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailFlightHours);
                        }
                        if (CompanyCatalogEntity.IsQuoteDetailMiles != null)
                        {
                            chkMiles.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailMiles);
                        }
                        if (CompanyCatalogEntity.IsQuoteDetailPassengerCnt != null)
                        {
                            chkPassengerCount.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailPassengerCnt);
                        }
                        #endregion
                        #region QuoteFeeColumn
                        if (CompanyCatalogEntity.IsQuoteFeelColumnDescription != null)
                        {
                            chkInvoiceFeeDescription.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFeelColumnDescription);
                        }
                        if (CompanyCatalogEntity.IsQuoteFeelColumnInvoiceAmt != null)
                        {
                            chkInvoicedAmt.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFeelColumnInvoiceAmt);
                        }
                        if (CompanyCatalogEntity.IsCOLFee3 != null)
                        {
                            chkChargeUnit.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCOLFee3);
                        }
                        if (CompanyCatalogEntity.IsCOLFee4 != null)
                        {
                            chkQuantity.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCOLFee4);
                        }
                        if (CompanyCatalogEntity.IsCOLFee5 != null)
                        {
                            chkRate.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCOLFee5);
                        }
                        #endregion
                        #region QuoteInformationToPrint
                        if (CompanyCatalogEntity.IsQuoteInformation != null)
                        {
                            chkCharterCompanyInfo.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteInformation);
                        }
                        if (CompanyCatalogEntity.IsCompanyName != null)
                        {
                            chkCustomerInfo.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCompanyName);
                        }
                        if (CompanyCatalogEntity.IsQuoteDispatch != null)
                        {
                            chkDescription.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDispatch);
                        }
                        if (CompanyCatalogEntity.IsTailNumber != null)
                        {
                            chkTailNumber.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsTailNumber);
                        }
                        if (CompanyCatalogEntity.IsAircraftTypeCode != null)
                        {
                            chkAircraftTypeCode.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAircraftTypeCode);
                        }
                        #endregion
                        #region Get Image from Database
                        CreateDictionayForImgUpload();
                        imgFile.ImageUrl = "";
                        ImgPopup.ImageUrl = null;
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetImg = ObjImgService.GetFileWarehouseList("CqCompanyInvoice", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            int i = 0;
                            foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                            {
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"];
                                ddlImg.Items.Insert(i, new ListItem(Encoder.HtmlEncode(fwh.UWAFileName), Encoder.HtmlEncode(fwh.UWAFileName)));
                                dicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                                if (i == 0)
                                {
                                    byte[] picture = fwh.UWAFilePath;

                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(fwh.UWAFileName, picture);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                    //}
                                    //else
                                    //{
                                    //    Session["CqInvoiceReportBase64"] = picture;
                                    //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?CqInvoiceReportBase64=CqInvoiceReportBase64&cd=" + Guid.NewGuid());
                                    //}
                                    ////end of modification for image issue
                                }
                                i = i + 1;
                            }
                            if (ddlImg.Items.Count > 0)
                            {
                                ddlImg.SelectedIndex = 0;
                            }
                            else
                            {
                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            }
                        }
                        #endregion
                    }
                }
            }
        }

        protected void btnMainOk_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetItems();
                        Dictionary<string, string> dictSetInvoiceReportDefaultsString = new Dictionary<string, string>();
                        dictSetInvoiceReportDefaultsString.Add("ChtQuoteCompanyNameINV", tbCharterCompInfo.Text);

                        dictSetInvoiceReportDefaultsString.Add("InvoiceTitle", tbReportTitle.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuotePageOffsettingInvoice", tbTopOffset.Text);
                        dictSetInvoiceReportDefaultsString.Add("ChtQuoteHeaderINV", tbCustomHeaderInfo.Text);
                        //dictSetInvoiceReportDefaultsString.Add("ChtQouteFooterDetailRpt", tbCustomFooterInfo.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteMinimumUseFeeDepositAmt", tbDailyDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteAdditionalFeeDefault", tbAdditionalDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteLandingFeeDefault", tbLandingDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteSegmentFeeDeposit", tbSegmentDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteStdCrewRONDeposit", tbRonCrewDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteAdditonCrewDeposit", tbCrewAddCrewDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteAdditionCrewRON", tbAddCrewRonDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteWaitingCharge", tbWaitDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("DepostFlightCharge", tbFlightDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteSubtotal", tbSubtotalDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("DiscountAmountDefault", tbDiscountDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteTaxesDefault", tbTaxesDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("DefaultInvoice", tbTotalInvoiceDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuotePrepayDefault", tbPrepayDesc.Text);
                        dictSetInvoiceReportDefaultsString.Add("QuoteRemainingAmountDefault", tbRemainingInvoiceDesc.Text);
                        Session.Add("CqCompanyStringInvoiceReport", dictSetInvoiceReportDefaultsString);
                        Dictionary<string, bool> dictSetInvoiceReportDefaultsCheck = new Dictionary<string, bool>();
                        dictSetInvoiceReportDefaultsCheck.Add("IsCityDescription", chkCityDetail.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsAirportDescription", chkAirportName.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteICAODescription", chkIcaoId.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IFirstPG", chkPrintFirstPage.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceDTsystemDT", chkDetail.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrintSubtotal", chkSubTotal.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailPrint", chkAdditionalFees.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrintSum", chkInvoiceSummary.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("isQuotePrintFees", chkAdditionalSummary.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrintAdditionalFee", chkAdditionalPrint.Checked);
                        //chkAdditionalPrint.Checked = dictSetInvoiceReportDefaultsCheck[""];              
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrepaidMinimUsageFeeAmt", chkDailyUsage.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFlightLandingFeePrint", chkLandingPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrepaidSegementFee", chkSegmentPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteAdditionalCrewPrepaid", chkAddCrewPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteStdCrewRonPrepaid", chkRonCrewPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteAdditonalCrewRON", chkAddCrewRonPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteWaitingChargePrepaid", chkWaitPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFlightChargePrepaid", chkFlightPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteSubtotalPrint", chkSubtotalPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDispcountAmtPrint", chkDiscountPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteTaxesPrint", chkTaxesAmt.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteInvoicePrint", chkTotalInvoicePrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrepayPrint", chkTotalPrepayPrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteRemainingAmtPrint", chkRemainingInvoicePrint.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptLegNum", chkLegNum.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptDetailDT", chkDepartDate.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptFromDescription", chkFromDescription.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailRptToDescription", chkToDescription.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailInvoiceAmt", chkInvoiceAmount.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailFlightHours", chkFlightHours.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailMiles", chkMiles.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailPassengerCnt", chkPassengerCount.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFeelColumnDescription", chkInvoiceFeeDescription.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFeelColumnInvoiceAmt", chkInvoicedAmt.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsCOLFee3", chkChargeUnit.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsCOLFee4", chkQuantity.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsCOLFee5", chkRate.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteInformation", chkCharterCompanyInfo.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsCompanyName", chkCustomerInfo.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDispatch", chkDescription.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsTailNumber", chkTailNumber.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsAircraftTypeCode", chkAircraftTypeCode.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptArrivalDate", chkIsInvoiceRptArrivalDate.Checked);
                        dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptQuoteDate", chkIsInvoiceRptQuoteDate.Checked);

                        Session.Add("CqCompanyCheckInvoiceReport", dictSetInvoiceReportDefaultsCheck);
                        Dictionary<string, int> dictSetQuoteInvoiceDefaultsRad = new Dictionary<string, int>();
                        dictSetQuoteInvoiceDefaultsRad.Add("IlogoPOS", radlstLogoPosition.SelectedIndex);
                        //dictSetQuoteInvoiceDefaultsRad.Add("InvoiceSalesLogoPosition", radSalesLogoPosition.SelectedIndex);
                        Session.Add("CqCompanyRadInvoiceReport", dictSetQuoteInvoiceDefaultsRad);
                        if (ddlFooterInfo.SelectedItem != null)
                            Session["CQInvoiceFooter"] = ddlFooterInfo.SelectedItem.Text;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "Close();", true);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }


        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    var items = ddlFooterInfo.Items;

                    List<FlightPakMasterService.CQMessage> FooterList = new List<FlightPakMasterService.CQMessage>();

                    if (items != null)
                    {
                        //foreach (var item in items.Cast<ListItem>().Where(x => x.Value != "-1"))
                        Int64 i = 0;
                        foreach (var item in items.Cast<ListItem>())
                        {
                            i = i + 1;

                            FlightPakMasterService.CQMessage objMessage = new FlightPakMasterService.CQMessage();
                            objMessage.HomebaseID = HomeBaseID;
                            objMessage.MessageTYPE = "I";
                            objMessage.MessageCD = i;
                            objMessage.MessageDescription = item.Text;
                            objMessage.MessageLine = item.Text;
                            objMessage.IsDeleted = false;

                            FooterList.Add(objMessage);
                        }
                    }
                    Session["CQInvoiceFooterList"] = FooterList;
                    //return CQServiceType;
                }, FlightPak.Common.Constants.Policy.UILayer);
                //return CQServiceType;
            }
        }

        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                trAddMessage.Visible = true;
                lbtxt.Text = System.Web.HttpUtility.HtmlEncode("Add Message");
                tbAddMessage.Text = string.Empty;
                hdnEdit.Value = string.Empty;
                btnSaveChanges.Visible = false;
                btnEditChanges.Visible = false;
                btnDelete.Visible = false;
            }
        }

        protected void btnEditChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (ddlFooterInfo.Items.Count > 0)
                {
                    if (ddlFooterInfo.Items.Count == 1)
                    {
                        RadWindowManager1.RadAlert("The default message cannot be Edited", 400, 100, ModuleNameConstants.Database.Company, "", null);
                    }
                    else
                    {
                        trAddMessage.Visible = true;
                        lbtxt.Text = System.Web.HttpUtility.HtmlEncode("Edit Message");
                        btnSaveChanges.Visible = false;
                        btnEditChanges.Visible = false;
                        btnDelete.Visible = false;
                        hdnEdit.Value = "true";
                        if (ddlFooterInfo.Items.Count > 0)
                        {
                            tbAddMessage.Text = ddlFooterInfo.SelectedItem.Text.ToString();
                            tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text.ToString();
                        }
                    }
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (ddlFooterInfo.Items.Count > 0)
                {
                    if (ddlFooterInfo.Items.Count == 1)
                    {
                        RadWindowManager1.RadAlert("The default message cannot be deleted", 400, 100, ModuleNameConstants.Database.Company, "", null);
                    }
                    else
                    {
                        ddlFooterInfo.Items.Remove(ddlFooterInfo.SelectedItem);
                    }
                }
            }
        }

        protected void btnSaveMsg_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                trAddMessage.Visible = false;
                btnSaveChanges.Visible = true;
                btnEditChanges.Visible = true;
                btnDelete.Visible = true;
                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var RetValue = objMasterService.GetCQMessage("I", HomeBaseID).EntityList.ToList().Where(x => x.MessageDescription != null && x.MessageDescription.ToUpper().Equals(tbAddMessage.Text.ToUpper()));
                    if (RetValue != null && RetValue.Count() > 0)
                    {
                        RadWindowManager1.RadAlert("Custom Footer Information must be Unique", 500, 50, ModuleNameConstants.Database.Company, "");

                    }
                    else
                    {
                        if (hdnEdit.Value == "true")
                        {
                            ddlFooterInfo.Items.Remove(ddlFooterInfo.SelectedItem);
                        }
                        ddlFooterInfo.Items.Add(tbAddMessage.Text);
                        ddlFooterInfo.DataBind();
                        ddlFooterInfo.SelectedValue = tbAddMessage.Text;
                        tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text.ToString();
                    }
                }
            }
        }

        protected void btnCancelMsg_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                trAddMessage.Visible = false;
                btnSaveChanges.Visible = true;
                btnEditChanges.Visible = true;
                btnDelete.Visible = true;
            }
        }
        protected void ddlFooterInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text.ToString();
            }
        }
        private void AssignInvoiceReport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CqCompanyStringInvoiceReport"] != null)
                        {
                            Dictionary<string, string> dictSetInvoiceReportDefaultsString = (Dictionary<string, string>)Session["CqCompanyStringInvoiceReport"];
                            tbCharterCompInfo.Text = dictSetInvoiceReportDefaultsString["ChtQuoteCompanyNameINV"];
                            tbTopOffset.Text = dictSetInvoiceReportDefaultsString["QuotePageOffsettingInvoice"];
                            tbCustomHeaderInfo.Text = dictSetInvoiceReportDefaultsString["ChtQuoteHeaderINV"];
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteMinimumUseFeeDepositAmt"]))
                            {
                                tbDailyDesc.Text = dictSetInvoiceReportDefaultsString["QuoteMinimumUseFeeDepositAmt"];
                            }
                            else
                            {
                                tbDailyDesc.Text = "DAILY USAGE ADJUSTMENT:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteAdditionalFeeDefault"]))
                            {
                                tbAdditionalDesc.Text = dictSetInvoiceReportDefaultsString["QuoteAdditionalFeeDefault"];
                            }
                            else
                            {
                                tbAdditionalDesc.Text = "ADDITIONAL FEES:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteLandingFeeDefault"]))
                            {
                                tbLandingDesc.Text = dictSetInvoiceReportDefaultsString["QuoteLandingFeeDefault"];
                            }
                            else
                            {
                                tbLandingDesc.Text = "LANDING FEES:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteSegmentFeeDeposit"]))
                            {
                                tbSegmentDesc.Text = dictSetInvoiceReportDefaultsString["QuoteSegmentFeeDeposit"];
                            }
                            else
                            {
                                tbSegmentDesc.Text = "SEGMENT FEES:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteStdCrewRONDeposit"]))
                            {
                                tbRonCrewDesc.Text = dictSetInvoiceReportDefaultsString["QuoteStdCrewRONDeposit"];
                            }
                            else
                            {
                                tbRonCrewDesc.Text = "RON CREW CHARGE:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteAdditonCrewDeposit"]))
                            {
                                tbCrewAddCrewDesc.Text = dictSetInvoiceReportDefaultsString["QuoteAdditonCrewDeposit"];
                            }
                            else
                            {
                                tbCrewAddCrewDesc.Text = "ADDITIONAL CREW CHARGE:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteAdditionCrewRON"]))
                            {
                                tbAddCrewRonDesc.Text = dictSetInvoiceReportDefaultsString["QuoteAdditionCrewRON"];
                            }
                            else
                            {
                                tbAddCrewRonDesc.Text = "ADDITIONAL CREW RON CHARGE:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteWaitingCharge"]))
                            {
                                tbWaitDesc.Text = dictSetInvoiceReportDefaultsString["QuoteWaitingCharge"];
                            }
                            else
                            {
                                tbWaitDesc.Text = "WAIT CHARGE:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["DepostFlightCharge"]))
                            {
                                tbFlightDesc.Text = dictSetInvoiceReportDefaultsString["DepostFlightCharge"];
                            }
                            else
                            {
                                tbFlightDesc.Text = "FLIGHT CHARGE:";
                            }
                            //dictSetInvoiceReportDefaultsString.Add("DepostFlightCharge", tbFlightDesc.Text);
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteSubtotal"]))
                            {
                                tbSubtotalDesc.Text = dictSetInvoiceReportDefaultsString["QuoteSubtotal"];
                            }
                            else
                            {
                                tbSubtotalDesc.Text = "SUBTOTAL:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["DiscountAmountDefault"]))
                            {
                                tbDiscountDesc.Text = dictSetInvoiceReportDefaultsString["DiscountAmountDefault"];
                            }
                            else
                            {
                                tbDiscountDesc.Text = "DISCOUNT AMOUNT:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteTaxesDefault"]))
                            {
                                tbTaxesDesc.Text = dictSetInvoiceReportDefaultsString["QuoteTaxesDefault"];
                            }
                            else
                            {
                                tbTaxesDesc.Text = "TAXES:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["DefaultInvoice"]))
                            {
                                tbTotalInvoiceDesc.Text = dictSetInvoiceReportDefaultsString["DefaultInvoice"];
                            }
                            else
                            {
                                tbTotalInvoiceDesc.Text = "TOTAL INVOICE:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuotePrepayDefault"]))
                            {
                                tbPrepayDesc.Text = dictSetInvoiceReportDefaultsString["QuotePrepayDefault"];
                            }
                            else
                            {
                                tbPrepayDesc.Text = "TOTAL PREPAY:";
                            }
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuoteRemainingAmountDefault"]))
                            {
                                tbRemainingInvoiceDesc.Text = dictSetInvoiceReportDefaultsString["QuoteRemainingAmountDefault"];
                            }
                            else
                            {
                                tbRemainingInvoiceDesc.Text = "TOTAL REMAINING INVOICE:";
                            }


                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["InvoiceTitle"]))
                            {
                                tbReportTitle.Text = dictSetInvoiceReportDefaultsString["InvoiceTitle"];
                            }
                            else
                            {
                                tbReportTitle.Text = "";
                            }


                        }
                        if (Session["CqCompanyCheckInvoiceReport"] != null)
                        {
                            Dictionary<string, bool> dictSetInvoiceReportDefaultsCheck = (Dictionary<string, bool>)Session["CqCompanyCheckInvoiceReport"];
                            chkCityDetail.Checked = dictSetInvoiceReportDefaultsCheck["IsCityDescription"];
                            chkAirportName.Checked = dictSetInvoiceReportDefaultsCheck["IsAirportDescription"];
                            chkIcaoId.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteICAODescription"];
                            chkPrintFirstPage.Checked = dictSetInvoiceReportDefaultsCheck["IFirstPG"];
                            chkDetail.Checked = dictSetInvoiceReportDefaultsCheck["IsInvoiceDTsystemDT"];
                            chkSubTotal.Checked = dictSetInvoiceReportDefaultsCheck["IsQuotePrintSubtotal"];
                            chkAdditionalFees.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailPrint"];
                            chkInvoiceSummary.Checked = dictSetInvoiceReportDefaultsCheck["IsQuotePrintSum"];
                            chkAdditionalSummary.Checked = dictSetInvoiceReportDefaultsCheck["isQuotePrintFees"];
                            //if (CompanyCatalogEntity.IsQuotePrintSum != null)
                            //{
                            //    chkInvoiceSummary.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrintSum);
                            //}
                            //if (CompanyCatalogEntity.QuoteMinimumUseFeeDepositAmt != null)
                            //{
                            //    chkAdditionalSummary.Checked = Convert.ToBoolean(CompanyCatalogEntity.QuoteMinimumUseFeeDepositAmt);
                            //}
                            //CompanyData.IsQuotePrepaidMinimumUsage2FeeAdj", chkAdditionalSummary.Checked);
                            chkAdditionalPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuotePrintAdditionalFee"];
                            chkDailyUsage.Checked = dictSetInvoiceReportDefaultsCheck["IsQuotePrepaidMinimUsageFeeAmt"];
                            chkLandingPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteFlightLandingFeePrint"];
                            chkSegmentPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuotePrepaidSegementFee"];
                            chkAddCrewPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteAdditionalCrewPrepaid"];
                            chkAddCrewRonPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteAdditonalCrewRON"];
                            chkRonCrewPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteStdCrewRonPrepaid"];
                            chkWaitPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteWaitingChargePrepaid"];
                            chkFlightPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteFlightChargePrepaid"];
                            chkSubtotalPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteSubtotalPrint"];
                            chkDiscountPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteDispcountAmtPrint"];
                            chkTaxesAmt.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteTaxesPrint"];
                            chkTotalInvoicePrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteInvoicePrint"];
                            chkTotalPrepayPrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuotePrepayPrint"];
                            chkRemainingInvoicePrint.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteRemainingAmtPrint"];
                            chkLegNum.Checked = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptLegNum"];
                            chkDepartDate.Checked = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptDetailDT"];
                            chkFromDescription.Checked = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptFromDescription"];
                            chkToDescription.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailRptToDescription"];
                            chkInvoiceAmount.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailInvoiceAmt"];
                            chkFlightHours.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailFlightHours"];
                            chkMiles.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailMiles"];
                            chkPassengerCount.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailPassengerCnt"];
                            chkInvoiceFeeDescription.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteFeelColumnDescription"];
                            chkInvoicedAmt.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteFeelColumnInvoiceAmt"];
                            chkChargeUnit.Checked = dictSetInvoiceReportDefaultsCheck["IsCOLFee3"];
                            chkQuantity.Checked = dictSetInvoiceReportDefaultsCheck["IsCOLFee4"];
                            chkRate.Checked = dictSetInvoiceReportDefaultsCheck["IsCOLFee5"];
                            chkCharterCompanyInfo.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteInformation"];
                            chkCustomerInfo.Checked = dictSetInvoiceReportDefaultsCheck["IsCompanyName"];
                            chkDescription.Checked = dictSetInvoiceReportDefaultsCheck["IsQuoteDispatch"];
                            chkTailNumber.Checked = dictSetInvoiceReportDefaultsCheck["IsTailNumber"];
                            chkAircraftTypeCode.Checked = dictSetInvoiceReportDefaultsCheck["IsAircraftTypeCode"];
                            chkIsInvoiceRptArrivalDate.Checked = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptArrivalDate"];
                            chkIsInvoiceRptQuoteDate.Checked = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptQuoteDate"];
                        }
                        if (Session["CqCompanyRadInvoiceReport"] != null)
                        {
                            Dictionary<string, int> dictSetQuoteInvoiceDefaultsRad = (Dictionary<string, int>)Session["CqCompanyRadInvoiceReport"];
                            if (dictSetQuoteInvoiceDefaultsRad["IlogoPOS"] != null)
                                radlstLogoPosition.SelectedIndex = Convert.ToInt16(dictSetQuoteInvoiceDefaultsRad["IlogoPOS"]);
                            //if (dictSetQuoteInvoiceDefaultsRad["InvoiceSalesLogoPosition"] != null)
                                //radSalesLogoPosition.SelectedIndex = Convert.ToInt16(dictSetQuoteInvoiceDefaultsRad["InvoiceSalesLogoPosition"]);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    // ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        #region Image
        ///// <summary>
        ///// ImageToBinary
        ///// </summary>
        ///// <param name="imagePath"></param>
        ///// <returns></returns>
        //public static byte[] ImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int)fileStream.Length);
        //        fileStream.Close();
        //        return buffer;
        //    }
        //}
        /// <summary>
        /// To Load Image
        /// </summary>
        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream myStream;
                Int32 fileLen;
                Session["CqInvoiceReportBase64"] = null;   //added for image issue
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        if (ddlImg.Items.Count >= 0)
                        {
                            string FileName = fileUL.FileName;
                            fileLen = fileUL.PostedFile.ContentLength;
                            Byte[] Input = new Byte[fileLen];
                            myStream = fileUL.FileContent;
                            myStream.Read(Input, 0, fileLen);

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);

                            ////start of modification for image issue
                            //if (hdnBrowserName.Value == "Chrome")
                            //{
                            //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                            //}
                            //else
                            //{
                            //    Session["CqInvoiceReportBase64"] = Input;
                            //    imgFile.ImageUrl = "../ImageHandler.ashx?CqInvoiceReportBase64=CqInvoiceReportBase64&cd=" + Guid.NewGuid();
                            //}
                            ////end of modification for image issue

                            Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                            
                            if (Session["CqInvoiceDicImg"] == null)
                                Session["CqInvoiceDicImg"] = new Dictionary<string, byte[]>();
                            dicImg = (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"];
                            //Commented for removing document name
                            //if (tbImgName.Text.Trim() != "")
                            //    FileName = tbImgName.Text.Trim();
                            int count = dicImg.Count(D => D.Key.Equals(FileName));
                            if (count > 0)
                            {
                                dicImg[FileName] = Input;
                            }
                            else
                            {
                                DeleteOtherImage();
                                dicImg.Add(FileName, Input);
                                ddlImg.Items.Clear();
                                ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(Encoder.HtmlEncode(FileName), Encoder.HtmlEncode(FileName)));
                               

                                //Ramesh: Set the URL for image file or link based on the file type
                                SetURL(FileName, Input);

                                ////start of modification for image issue
                                //if (hdnBrowserName.Value == "Chrome")
                                //{
                                //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                                //}
                                //else
                                //{
                                //    Session["CqInvoiceReportBase64"] = Input;
                                //    imgFile.ImageUrl = "../ImageHandler.ashx?CqInvoiceReportBase64=CqInvoiceReportBase64&cd=" + Guid.NewGuid();
                                //}
                                ////end of modification for image issue
                            }
                            tbImgName.Text = "";
                            btndeleteImage.Enabled = true;
                        }
                    }
                }
            }
        }
        private void LoadImages()
        {
            #region Get Image from Database
            if (Session["CqInvoiceDicImg"] == null && Session["CompanyInsert"].ToString() != "Insert")
            {
                CreateDictionayForImgUpload();
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlDecode("../../../App_Themes/Default/images/noimage.jpg");
                //ImgPopup.ImageUrl = null;
                using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ObjRetImg = ObjImgService.GetFileWarehouseList("CqCompanyInvoice", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList;
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    Session["CqInvoiceReportBase64"] = null;   //added for image issue
                    int i = 0;
                    foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                    {
                        Dictionary<string, byte[]> CqQuoteDicImg = (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"];
                        ddlImg.Items.Insert(i, new ListItem(Encoder.HtmlEncode(fwh.UWAFileName), Encoder.HtmlEncode(fwh.UWAFileName)));
                        CqQuoteDicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                        if (i == 0)
                        {
                            byte[] picture = fwh.UWAFilePath;

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(fwh.UWAFileName, picture);

                            ////start of modification for image issue
                            //if (hdnBrowserName.Value == "Chrome")
                            //{
                            //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                            //}
                            //else
                            //{
                            //    Session["CqInvoiceReportBase64"] = picture;
                            //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?CqInvoiceReportBase64=CqInvoiceReportBase64&cd=" + Guid.NewGuid());
                            //}
                            ////end of modification for image issue
                        }
                        i = i + 1;
                    }
                    if (ddlImg.Items.Count <= 0)
                    {
                        ddlImg.SelectedIndex = 0;
                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                        ImgPopup.ImageUrl = null;
                    }
                }
            }
            else
            {
                Dictionary<string, byte[]> dicImgs = new Dictionary<string, byte[]>();
                if (Session["CqInvoiceDicImg"] == null)
                    Session["CqInvoiceDicImg"] = new Dictionary<string, byte[]>();
                dicImgs = (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"];
                string FileName = string.Empty;
                Byte[] Input = new byte[0];
                foreach (var DicItem in dicImgs)
                {
                    FileName = DicItem.Key;
                    Input = DicItem.Value;
                }
                ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(FileName, FileName));
                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                }
                else
                {
                    Session["CqInvoiceReportBase64"] = Input;
                    imgFile.ImageUrl = "../ImageHandler.ashx?CqInvoiceReportBase64=CqInvoiceReportBase64&cd=" + Guid.NewGuid();
                }
                if (ddlImg.Items.Count > 0)
                {
                    ddlImg.SelectedIndex = 0;
                    btndeleteImage.Enabled = true;
                }
                if (dicImgs.Count() <= 0)
                {
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    ImgPopup.ImageUrl = null;
                }
            }
            #endregion
        }
        /// <summary>
        /// To Delete Other Image
        /// </summary>
        private void DeleteOtherImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();

                if (Session["CqInvoiceDicImg"] == null)
                    Session["CqInvoiceDicImg"] = new Dictionary<string, byte[]>();
                dicImg = (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"];
                int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                if (count > 0)
                {
                    dicImg.Remove(ddlImg.Text.Trim());
                    Session["CqInvoiceDicImg"] = dicImg;
                    Dictionary<string, string> dicImgDelete;
                    if (Session["CqInvoiceImgDelete"] == null)
                        Session["CqInvoiceImgDelete"] = new Dictionary<string, string>();
                    dicImgDelete = (Dictionary<string, string>)Session["CqInvoiceImgDelete"];
                    //Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["CqInvoiceImgDelete"];
                    dicImgDelete.Add(ddlImg.Text.Trim(), "");
                    Session["CqInvoiceImgDelete"] = dicImgDelete;
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbImgName.Text = "";
                    int i = 0;
                    foreach (var DicItem in dicImg)
                    {
                        ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                        i = i + 1;
                    }
                    imgFile.ImageUrl = "";
                    ImgPopup.ImageUrl = null;
                }
            }
        }
        /// <summary>
        /// Dropdown for image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool imgFound = false;
                            imgFile.ImageUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ObjRetImg = ObjImgService.GetFileWarehouseList("CqCompanyInvoice", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());
                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                {
                                    imgFound = true;
                                    byte[] picture = fwh.UWAFilePath;

                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(fwh.UWAFileName, picture);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                    //}
                                    //else
                                    //{
                                    //    Session["CqInvoiceReportBase64"] = picture;
                                    //    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CqInvoiceReportBase64=CqInvoiceReportBase64&cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                                tbImgName.Text = ddlImg.Text.Trim();
                                if (!imgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"];
                                    int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (count > 0)
                                    {

                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        //}
                                        //else
                                        //{
                                        //    Session["CqInvoiceReportBase64"] = dicImg[ddlImg.Text.Trim()];
                                        //    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CqInvoiceReportBase64=CqInvoiceReportBase64&cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To delete image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                        if (Session["CqInvoiceDicImg"] != null)
                            Session["CqInvoiceDicImg"] = new Dictionary<string, byte[]>();
                        dicImg = (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"];
                        int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            dicImg.Remove(ddlImg.Text.Trim());
                            Session["CqInvoiceImgDelete"] = dicImg;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["CqInvoiceImgDelete"];
                            if (Session["CqInvoiceImgDelete"] != null)
                                if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                                {
                                    dicImgDelete.Add(ddlImg.Text.Trim(), "");
                                }
                            Session["CqInvoiceImgDelete"] = dicImgDelete;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in dicImg)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            ImgPopup.ImageUrl = null;
                        }
                        else
                        {                            
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["CqInvoiceImgDelete"];
                            if (Session["CqInvoiceImgDelete"] != null)
                                if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                                {
                                    dicImgDelete.Add(ddlImg.Text.Trim(), "");
                                }
                            Session["CqInvoiceImgDelete"] = dicImgDelete;

                            dicImg.Remove(ddlImg.Text.Trim());
                            Session["CqInvoiceDicImg"] = dicImg;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    // ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Create dictioanry for image upload
        /// </summary>
        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                Session["CqInvoiceDicImg"] = dicImg;
                Dictionary<string, string> dicImgDelete = new Dictionary<string, string>();
                Session["CqInvoiceImgDelete"] = dicImgDelete;
            }
        }
        /// <summary>
        /// Rad Ajax Seeting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = DivExternalForm;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        #endregion

        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            if (Request.UserAgent.Contains("Chrome"))
            {
                hdnBrowserName.Value = "Chrome";
            }
            //Ramesh: Code to allow any file type for upload control.
            int iIndex = filename.Trim().IndexOf('.');
            //string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);  
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            if (regMatch.Success)
            {
                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    //lnkFileName.NavigateUrl = "";
                    //lnkFileName.Visible = false;
                }
                else
                {
                    Session["CqInvoiceReportBase64"] = filedata;
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CqInvoiceReportBase64=CqInvoiceReportBase64&cd=" + Guid.NewGuid();
                    //lnkFileName.NavigateUrl = "";
                    //lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgFile.Visible = false;
                //lnkFileName.Visible = true;
                Session["CqInvoiceReportBase64"] = filedata;
                //lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();
            }
        }
    }
}