﻿<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head runat="server">
    <title>Payment Types</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
   <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
   <link href="/Scripts/jquery.alerts.css" rel="stylesheet" media="all" />
    
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
   <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.alerts.js"></script>
    <script type="text/javascript">
        var jqgridTableId = '#gridPaymentTypes';
        var paymentSelectedCd = "";
        $(document).ready(function () {

            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });
            paymentSelectedCd = $.trim(unescape(getQuerystring("PaymentCD", "")));

            var uireports = $.trim(unescape(getQuerystring("IsUIReports", "")));
            var ismultiselect = false;
            if (uireports == "1")
                ismultiselect = true;
            $(jqgridTableId).jqGrid({
                url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/GetPaymentTypes',
                mtype: 'POST',
                datatype: "json",
                async: true,
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false, async: false },
                serializeGridData: function (postData) {
                    postData.showActiveOnly = $("#chkDisplayActiveOnly").is(':checked') ? true : false;
                    return JSON.stringify(postData);
                },
                height: 'auto',
                width: 356,
                loadonce: true,
                ignoreCase: true,
                autowidth: false,
                shrinkToFit: false,
                multiselect: ismultiselect,
                pgbuttons: false,
                pginput: false,
                colNames: ['PaymentTypeID', 'Code', 'Description', 'Active'],
                colModel: [
                        { name: 'PaymentTypeID', index: 'PaymentTypeID', hidden: true, key: true },
                        { name: 'PaymentTypeCD', index: 'PaymentTypeCD', width: 80, fixed: true },
                        { name: 'PaymentTypeDescription', index: 'PaymentTypeDescription', width: 250, fixed: true },
                        {
                            name: 'IsInActive', index: 'IsInActive', width: 40, search: false, align: 'center',fixed: true , width: 40, formatter: function (cellvalue, options, rowObject) {
                                var check = cellvalue == false ? "checked='checked'" : "";
                                return "<input type='checkbox' style='width:100%' disabled='disabled' " + check + " readonly/>";
                            }
                        }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);
                },
                loadComplete: function () {
                    var ids = $(this).jqGrid("getDataIDs");
                    for (i = 1; i <= ids.length; i++) {
                        rowData = $(this).jqGrid('getRowData', ids[i]);
                        if (rowData["PaymentTypeCD"] == paymentSelectedCd) {
                            $(this).jqGrid('setSelection', ids[i]);
                            break;
                        }
                    }
                },
                onSelectRow: function (id) {
                    var rowNumber = $(this).jqGrid('getCell', id, 'PaymentTypeID');
                    var rowData = $(jqgridTableId).jqGrid('getRowData', rowNumber);
                    var lastSel = rowData['PaymentTypeID'];//replace name with any column
                    paymentSelectedCd = $.trim(rowData['PaymentTypeCD']);

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $(jqgridTableId).jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                }
            });

            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });


            $("#btnSubmitPaymentType").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                if (rowData["PaymentTypeCD"] == undefined) {
                    BootboxAlert("Please select a record.");
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            $("#chkDisplayActiveOnly").change(function () {
                rebindgrid();
            });
            $("#recordAdd").click(function () {
                openWin2("/Views/Settings/Company/PaymentType.aspx?IsPopup=Add", 'radPaymentTypeCRUDPopup');
                return false;
            });

            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                var paymentSelectedId = rowData['PaymentTypeID'];
                var widthDoc = $(document).width();
                if (!IsNullOrEmptyOrUndefined(paymentSelectedId) && paymentSelectedId != 0) {
                    openWin2("/Views/Settings/Company/PaymentType.aspx?IsPopup=&SelectedPaymentID=" + paymentSelectedId, 'radPaymentTypeCRUDPopup');
                }
                else {
                    jAlert('Please select a Payment Type.', popupTitle);
                }
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                paymentIdToDelete = rowData['PaymentTypeID'];
                if (!IsNullOrEmptyOrUndefined(paymentIdToDelete) && paymentIdToDelete != 0) {
                    jConfirm('Are you sure you want to delete this record?', 'Confirmation', confirmCallBackFn);
                }
                else {
                    jAlert('Please select a record.', popupTitle);
                }
                return false;
            });

        });
        function confirmCallBackFn(arg) {
            if (arg) {
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/DeletePaymentType',
                    data: JSON.stringify({ 'paymentIdToDelete': paymentIdToDelete }),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var returnResult = verifyReturnedResult(data);
                        if (returnResult == false) {
                            var message = "This record is either not available or already in use. Deletion is not allowed.";
                            jAlert(message, "Payment Type");
                        }
                        else {
                            jAlert('Payment Type deleted successfully!', "Payment Type");
                        }
                        rebindgrid();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reportPostflightError(jqXHR, textStatus, errorThrown);
                    }
                });
            }
        }

        function rebindgrid()
        {
            $(jqgridTableId).setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
        }
        function returnToParent(rowDataSelected) {
            var oArg = new Object();
            oArg.PaymentTypeCD = "";
            oArg.PaymentTypeDescription = "";
            oArg.PaymentTypeID = "";
            if (rowDataSelected != undefined && rowDataSelected != null && !IsNullOrEmptyOrUndefined(rowDataSelected["PaymentTypeID"])) {
                oArg.PaymentTypeCD = rowDataSelected["PaymentTypeCD"] ;
                oArg.PaymentTypeDescription = rowDataSelected["PaymentTypeDescription"];
                oArg.PaymentTypeID = rowDataSelected["PaymentTypeID"];
            }
            else {
                var ids = $(jqgridTableId).jqGrid('getGridParam', 'selarrrow');
                for (i = 0; i < ids.length; i++) {
                    var rowData = $(jqgridTableId).jqGrid('getRowData', ids[i]);
                    oArg.PaymentTypeCD += rowData["PaymentTypeCD"] + ",";
                    oArg.PaymentTypeDescription += rowData["PaymentTypeDescription"];
                    oArg.PaymentTypeID += rowData["PaymentTypeID"];
                }
                if (oArg.PaymentTypeCD != "") {
                    oArg.PaymentTypeCD = oArg.PaymentTypeCD.substring(0, oArg.PaymentTypeCD.length - 1)
                }
                else {
                    oArg.PaymentTypeCD = "";
                }
                oArg.Arg1 = oArg.PaymentTypeCD;
                oArg.CallingButton = "PaymentTypeCD";
            }
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }

        }
    </script>
    <style type="text/css">
        body {
            background: none !important;
            overflow-x:hidden;
            height:100%; 
        }
    </style>
</head>
<body>
    <form id="form1">
  
       <div class="search_popup">
        <table class="box1">
                    <tr>
                        <td>
                             <input type="checkbox" id="chkDisplayActiveOnly" name="chkDisplayActiveOnly"/>Active Only
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridPaymentTypes" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager_paymentType"></div>                              
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <div id="jqgridOverflow" style="color: red; display: none;">Loading...</div>
                                <div class="grid_icon">
                                    <a class="add-icon-grid" href="#" id="recordAdd"></a>
                                    <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                    <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                </div>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <input id="btnSubmitPaymentType" class="button okButton" value="OK"  type="button"/>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
        </div>
    </form>
</body>
</html>
