﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyQuoteReportDefaults.aspx.cs"
    ValidateRequest="false" ClientIDMode="AutoID" Inherits="FlightPak.Web.Views.Settings.Company.CompanyQuoteReportDefaults" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Literal ID="ltlHomeBase" runat="server" Text=""></asp:Literal>
    </title>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function Close() { //alert('clos called'); //
                // GetRadWindow().BrowserWindow.refreshGrid(); 
                GetRadWindow().Close();
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function browserName() {
                var agt = navigator.userAgent.toLowerCase();
                if (agt.indexOf("msie") != -1) return 'Internet Explorer';
                if (agt.indexOf("chrome") != -1) return 'Chrome';
                if (agt.indexOf("opera") != -1) return 'Opera';
                if (agt.indexOf("firefox") != -1) return 'Firefox';
                if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
                if (agt.indexOf("netscape") != -1) return 'Netscape';
                if (agt.indexOf("safari") != -1) return 'Safari';
                if (agt.indexOf("staroffice") != -1) return 'Star Office';
                if (agt.indexOf("webtv") != -1) return 'WebTV';
                if (agt.indexOf("beonex") != -1) return 'Beonex';
                if (agt.indexOf("chimera") != -1) return 'Chimera';
                if (agt.indexOf("netpositive") != -1) return 'NetPositive';
                if (agt.indexOf("phoenix") != -1) return 'Phoenix';
                if (agt.indexOf("skipstone") != -1) return 'SkipStone';
                if (agt.indexOf('\/') != -1) {
                    if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                        return navigator.userAgent.substr(0, agt.indexOf('\/'));
                    }
                    else return 'Netscape';
                } else if (agt.indexOf(' ') != -1)
                    return navigator.userAgent.substr(0, agt.indexOf(' '));
                else return navigator.userAgent;
            }
            function GetBrowserName() {
                document.getElementById('<%=hdnBrowserName.ClientID%>').value = browserName();
            }
            function OpenRadWindow() {
                var oWnd = $find("<%=RadwindowQuoteImagePopup.ClientID%>");
                document.getElementById('<%=ImgQuotePopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
                oWnd.show();

            }
            function CloseRadWindow() {
                var oWnd = $find("<%=RadwindowQuoteImagePopup.ClientID%>");
                document.getElementById('<%=ImgQuotePopup.ClientID%>').src = null;
                oWnd.close();
            }
            function File_onchange() {
                if (validate())
                    __doPostBack('__Page', 'LoadImage');
            }
            function printImage() {
                var image = document.getElementById('<%=ImgQuotePopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlImg = document.getElementById('<%=ddlImg.ClientID%>');
                if (ddlImg.length > 0) {
                    var ImageName = ddlImg.options[ddlImg.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });
                    radconfirm(Msg, callBackFunction, 400, 100, null, "Company Profile");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }
            function validate() {
                var uploadcontrol = document.getElementById('<%=fileUL.ClientID%>').value;
                //var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.[Bb][Mm][Pp])$/;
                var reg = /([^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG])).\2)/gm;
                if (uploadcontrol.length > 0) {
                    if (reg.test(uploadcontrol)) {
                        return true;
                    }
                    else {
                        //alert("Only .bmp files are allowed!");
                        return false;
                    }
                }
            }
        </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rtsQuotReportDefault">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadwindowQuoteImagePopup" runat="server" VisibleOnPageLoad="false"
                KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false" Height="600px"
                Width="750px" Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_Quoteprinticon">
                                        <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printImage();return false;" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="ImgQuotePopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="box1" width="100%">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <telerik:RadTabStrip ID="rtsQuotReportDefault" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                                        AutoPostBack="true" MultiPageID="RadMultiPage1" SelectedIndex="0" Align="Justify"
                                        Style="float: inherit">
                                        <Tabs>
                                            <telerik:RadTab Value="InformationPage1" Text="Information Page 1" Selected="true">
                                            </telerik:RadTab>
                                            <telerik:RadTab Value="InformationPage2" Text="Information Page 2">
                                            </telerik:RadTab>
                                            <telerik:RadTab Value="InformationPage3" Text="Information Page 3">
                                            </telerik:RadTab>
                                        </Tabs>
                                    </telerik:RadTabStrip>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="tblspace_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 50%;">
                                                    <table>
                                                        <tr>
                                                            <td class="tdLabel100">
                                                                <asp:Label ID="lbCharterCompInfo" runat="server" Text="Charter Company Information"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel100">
                                                                <asp:TextBox ID="tbCharterCompInfo" runat="server" TextMode="MultiLine" CssClass="textarea350x75"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 50%;">
                                                    <table>
                                                        <tr>
                                                            <td class="tdLabel130">
                                                                <asp:Label ID="lbTopOffset" runat="server" Text="Top of Page Row Offset"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbTopOffset" runat="server" CssClass="text20" MaxLength="3" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:CheckBox ID="chkCityDetail" runat="server" Text="Include City In Detail" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:CheckBox ID="chkAirportName" runat="server" Text="Include Airport Name In Detail" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:CheckBox ID="chkIcaoId" runat="server" Text="Include ICAO ID In Detail" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td class="tblspace_5">
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ldCustomHeaderInfo" runat="server" Text="Custom Header Information"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbCustomFooterInfo" runat="server" Text="Custom Footer Information"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbCustomHeaderInfo" runat="server" TextMode="MultiLine" CssClass="textarea350x75"> </asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbCustomFooterInfo" runat="server" TextMode="MultiLine" CssClass="textarea350x75"> </asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td class="tblspace_5">
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td class="tblspace_5">
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td>
                                                    Custom Footer
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="ddlFooterInfo" runat="server" AutoPostBack="true" Width="770px"
                                                        Height="20px" OnSelectedIndexChanged="ddlFooterInfo_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tblspace_5">
                                                </td>
                                            </tr>
                                            <tr id="trAddMessage" runat="server" visible="false">
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbtxt" CssClass="alert-text" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbAddMessage" runat="server" CssClass="tdLabel620" TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="tblButtonArea">
                                                            <td>
                                                                <asp:Button ID="btnSaveMsg" Text="Save" runat="server" CssClass="button" ValidationGroup="save"
                                                                    OnClick="btnSaveMsg_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnCancelMsg" Text="Cancel" runat="server" CssClass="button" OnClick="btnCancelMsg_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr align="left">
                                                <td align="right">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnSaveChanges" Text="Add" runat="server" CssClass="button" ValidationGroup="save"
                                                                    OnClick="btnSaveChanges_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnEditChanges" Text="Edit" runat="server" CssClass="button" CausesValidation="false"
                                                                    OnClick="btnEditChanges_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnDelete" Text="Delete" runat="server" CssClass="button" CausesValidation="false"
                                                                    OnClick="btnDelete_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td class="tblspace_5">
                                                </td>
                                            </tr>
                                        </table>
                                        <fieldset>
                                            <legend>Image/logo will print on Quote Report</legend>
                                            <table width="100%">
                                                <tr>
                                                    <td width="50%">
                                                        <table width="100%">
                                                            <tr style="display: none;">
                                                                <td class="tdLabel100">
                                                                    Document Name
                                                                </td>
                                                                <td style="vertical-align: top">
                                                                    <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text210" ClientIDMode="Static"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: top">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:FileUpload ID="fileUL" runat="server" Enabled="false" ClientIDMode="Static"
                                                                                    onchange="javascript:return File_onchange();" />
                                                                                <asp:Label ID="lblError" CssClass="alert-text" runat="server" Style="float: left;">File types must be .jpg, .gif or .bmp</asp:Label>
                                                                                <asp:HiddenField ID="hdnUrl" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RegularExpressionValidator ID="rexpfileUL" runat="server" ControlToValidate="fileUL"
                                                                                    ValidationGroup="Save" CssClass="alert-text" ValidationExpression="(.*\.([Gg][Ii][Ff])|.*\.([Jj][Pp][Gg])|.*\.([Bb][Mm][Pp])|.*\.([jJ][pP][eE][gG])$)"
                                                                                    Display="Static"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td width="50%" align="right" valign="top">
                                                                    <table width="70%">
                                                                        <tr>
                                                                            <td style="vertical-align: top">
                                                                                <table cellpadding="0px" cellspacing="0px">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                                                                                OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text200"
                                                                                                ClientIDMode="Static">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <td class="input_no_bg">
                                                                                        Standard logo size is 3.98" X 1.25"
                                                                                    </td>
                                                                                </table>
                                                                            </td>
                                                                            <td style="vertical-align: top">
                                                                                <asp:Image ID="imgFile" Width="50px" Height="50px" runat="server" ClientIDMode="Static"
                                                                                    OnClick="OpenRadWindow();" />
                                                                            </td>
                                                                            <td style="vertical-align: top" class="custom_radbutton">
                                                                                <telerik:RadButton ID="btndeleteImage" runat="server" Text="Delete" Enabled="false"
                                                                                    OnClientClicking="ImageDeleteConfirm" OnClick="DeleteImage_Click" ClientIDMode="Static" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <table>
                                                                <tr>
                                                                    <td class="tblspace_5">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <td style="width: 70%;">
                                                                    <table width="100%">
                                                                        <%-- <tr>
                                                                            <td colspan="2">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel170">
                                                                                            Salesperson Logo Position
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RadioButtonList ID="radSalesLogoPosition" runat="server" RepeatDirection="Horizontal"
                                                                                                CellPadding="0" CellSpacing="0">
                                                                                                <asp:ListItem Value="0">None</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Center</asp:ListItem>
                                                                                                <asp:ListItem Value="2">Left </asp:ListItem>
                                                                                                <asp:ListItem Value="3">Right </asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>--%>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel170">
                                                                                            Company Profile Logo Position
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RadioButtonList ID="radlstLogoPosition" runat="server" RepeatDirection="Horizontal"
                                                                                                CellPadding="0" CellSpacing="0">
                                                                                                <asp:ListItem Value="0">None</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Center</asp:ListItem>
                                                                                                <asp:ListItem Value="2">Left </asp:ListItem>
                                                                                                <asp:ListItem Value="3">Right </asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel170">
                                                                                            Aircraft Image Position
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RadioButtonList ID="RadioImagePosition" runat="server" RepeatDirection="Horizontal"
                                                                                                CellPadding="0" CellSpacing="0">
                                                                                                <asp:ListItem Value="0">None</asp:ListItem>
                                                                                                <asp:ListItem Value="1">End Of Report</asp:ListItem>
                                                                                                <asp:ListItem Value="2">After Tail No. </asp:ListItem>
                                                                                                <asp:ListItem Value="3">Separate Page </asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="width: 30%;">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkPrintFirstPage" runat="server" Text="Print On First Page Only" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radAllLegs" runat="server" Text="Print One Aircraft Image" GroupName="Print" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radTwoimages" runat="server" Text="Print Two Aircraft Images"
                                                                                    GroupName="Print" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radthreeimages" runat="server" Text="Print Three Aircraft Images"
                                                                                    GroupName="Print" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radfourimages" runat="server" Text="Print Four Aircraft Images"
                                                                                    GroupName="Print" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </table>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table width="100%" class="box1">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" Text="Default Quote Summary Description"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <fieldset>
                                                        <table>
                                                            <tr>
                                                                <td class="tdLabel200">
                                                                    <asp:CheckBox ID="chkDetail" runat="server" Text="Print Quote Detail" />
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBox ID="chkSubTotal" runat="server" Text="Print Quote Subtotal" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel200">
                                                                    <asp:CheckBox ID="chkAdditionalFees" runat="server" Text="Print Additional Fees" />
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBox ID="chkQuoteSummary" runat="server" Text="Print Quote Summary" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tblspace_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr class="GridViewHeaderStyle">
                                                            <th>
                                                                Print
                                                            </th>
                                                            <th>
                                                                Description
                                                            </th>
                                                            <th>
                                                                Units
                                                            </th>
                                                        </tr>
                                                        <tr class="GridViewRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkDailyPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbDailyDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkDailyUnits" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewAlternatingRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkAdditionalPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbAdditionalDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkLandingPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbLandingDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewAlternatingRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkSegmentPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbSegmentDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkRonCrewPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbRonCrewDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkRonCrewUnits" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewAlternatingRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkAddCrewPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbCrewAddCrewDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkAddCrewUnits" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkAddCrewRonPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbAddCrewRonDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkAddCrewRonUnits" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewAlternatingRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkWaitPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbWaitDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkWaitUnits" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkFlightPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbFlightDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkFlightUnits" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewAlternatingRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkSubtotalPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbSubtotalDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkDiscountPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbDiscountDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkDiscountUnits" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewAlternatingRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkTaxesAmt" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbTaxesDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkTotalQuotePrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbTotalQuoteDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewAlternatingRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkPrepaidPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbPrepaidDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="GridViewRowStyle">
                                                            <td>
                                                                <asp:CheckBox ID="chkRemainingAmtPrint" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbRemainingAmtDesc" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView3" runat="server">
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table width="100%" cellpadding="0" cellspacing="0" class="box1">
                                            <tr>
                                                <td valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                <fieldset>
                                                                    <legend>Quote Detail Columns To Print</legend>
                                                                    <table>
                                                                        <tr>
                                                                            <td class="tdLabel250">
                                                                                <asp:CheckBox ID="chkLegNum" runat="server" Text="Leg No." />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkDepartDate" runat="server" Text="Departure Date" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkIsRptQuoteDetailArrivalDate" runat="server" Text="Arrival Date" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkIsRptQuoteDetailQuoteDate" runat="server" Text="Quote Date" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkFromDescription" runat="server" Text="From Description" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkToDescription" runat="server" Text="To Description" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkQuotedAmount" runat="server" Text="Quoted Amount" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkFlightHours" runat="server" Text="Flight Hours" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkMiles" runat="server" Text="Miles" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkPassengerCount" runat="server" Text="Passenger Count" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblspace_1">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>Quote ID Printing & Ediiting</legend>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td class="tdLabel150">
                                                                                <asp:RadioButton ID="radNone" runat="server" Text="None" GroupName="Printing" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tdLabel150">
                                                                                <asp:RadioButton ID="radFileQNumber" runat="server" Text="File-Quote No." GroupName="Printing" />
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:CheckBox ID="chkAllowEdit" runat="server" Text="Allow Edit" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblspace_20">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <fieldset>
                                                                                <legend>Quote Fee Columns To Print</legend>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td class="tdLabel250">
                                                                                            <asp:CheckBox ID="chkQuoteFeeDescription" runat="server" Text="Description" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkQuotedAmt" runat="server" Text="Quoted Amount" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkChargeUnit" runat="server" Text="Charge Unit" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkQuantity" runat="server" Text="Quantity" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkRate" runat="server" Text="Rate" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <fieldset>
                                                                                <legend>Quote Information To Print</legend>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td class="tdLabel250">
                                                                                            <asp:CheckBox ID="chkCharterCompanyInfo" runat="server" Text="Charter Company Info" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkCustomerInfo" runat="server" Text="Customer Info" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkDescription" runat="server" Text="Description" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkSalesPerson" runat="server" Text="Salesperson" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkTailNum" runat="server" Text="Tail No." />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkAircraftType" runat="server" Text="Aircraft Type Code" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                Quote Report Title
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%-- <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" Width="575px"
                                                                    Height="20px">
                                                                    <asp:ListItem Text="TES" Selected="True"></asp:ListItem>
                                                                </asp:DropDownList>--%>
                                                                <asp:TextBox ID="tbReportTitle" runat="server" MaxLength="60" Width="700px" TextMode="SingleLine"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" class="box1">
                            <tr>
                                <td align="right">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnOK" Text="Ok" runat="server" CssClass="button" ValidationGroup="save"
                                                    OnClick="btnMainOk_Click" />
                                                <asp:HiddenField ID="hdnEdit" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" CausesValidation="false"
                                                    Style="display: none;" OnClick="btnMainCancel_Click" />
                                                <asp:HiddenField ID="hdnBrowserName" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
