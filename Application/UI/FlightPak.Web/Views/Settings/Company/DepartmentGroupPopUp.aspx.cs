﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class DepartmentGroupPopUp : BaseSecuredPage
    {
        private string departmentgroupCode;
        private ExceptionManager exManager;
        private string departmentgroupCD;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["DepartmentGroupCD"] != null)
            {
                if (Request.QueryString["DepartmentGroupCD"].ToString() == "1")
                {
                    dgDepartmentGroup.AllowMultiRowSelection = true;
                }
                else
                {
                    dgDepartmentGroup.AllowMultiRowSelection = false;
                }
            }
            else
            {
                dgDepartmentGroup.AllowMultiRowSelection = false;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["DepartmentGroupCD"]))
                        {
                            departmentgroupCD = Request.QueryString["DepartmentGroupCD"].ToUpper().Trim();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgDepartmentGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetDeptGroupInfo();
                            List<FlightPakMasterService.GetAllDepartmentGroup> lstDepartmentGroup = new List<FlightPakMasterService.GetAllDepartmentGroup>();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                               lstDepartmentGroup = ObjRetVal.EntityList;
                            }
                            dgDepartmentGroup.DataSource = lstDepartmentGroup;
                            Session["DepartmentGroup"] = lstDepartmentGroup;
                            if (chkSearchActiveOnly.Checked == true)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgDepartmentGroup;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        protected void SelectItem()
        {
            if (!string.IsNullOrEmpty(departmentgroupCD))
            {
                foreach (GridDataItem item in dgDepartmentGroup.MasterTableView.Items)
                {
                    if (item["DepartmentGroupCD"].Text.Trim().ToUpper() == departmentgroupCD)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (dgDepartmentGroup.Items.Count > 0)
                {
                    dgDepartmentGroup.SelectedIndexes.Add(0);
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllDepartmentGroup> lstDepartmentGroup = new List<FlightPakMasterService.GetAllDepartmentGroup>();
                if (Session["DepartmentGroup"] != null)
                {
                    lstDepartmentGroup = (List<FlightPakMasterService.GetAllDepartmentGroup>)Session["DepartmentGroup"];
                }
                if (lstDepartmentGroup.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstDepartmentGroup = lstDepartmentGroup.Where(x => x.IsInActive == false).ToList<GetAllDepartmentGroup>(); }
                    dgDepartmentGroup.DataSource = lstDepartmentGroup;
                    if (IsDataBind)
                    {
                        dgDepartmentGroup.DataBind();
                    }
                }
                return false;
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            SearchAndFilter(true);
        }
        #endregion

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartmentGroup_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["DepartmentGroupCD"] != null)
                {
                    if (Request.QueryString["DepartmentGroupCD"].ToString() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }

        protected void dgDepartmentGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgDepartmentGroup_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgDepartmentGroup, Page.Session);
        }


    }
}