﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="ClientCodeCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.ClientCodeCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <!--[if IE]>
<style>
    .top-bar span.last-span {
  height: 14px;
  min-width: 100px;
  float:left;
}
</style>
<![endif]-->
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgClientCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgClientCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgClientCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSaveChangesTop">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgClientCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgClientCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelTop">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgClientCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgClientCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUserAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUserAddAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUserRemoveAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUserRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFleetAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFleetAddAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFleetRemoveAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFleetRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCrewAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCrewAddAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCrewRemoveAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCrewRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnPaxAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnPaxAddAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnPaxRemoveAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnPaxRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDeptAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDeptAddAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDeptRemoveAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDeptRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFlightCategoryAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFlightCategoryAddAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFlightCategoryRemoveAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFlightCategoryRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFlightPurposeAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFlightPurposeAddAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFlightPurposeRemoveAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFlightPurposeRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFuelLocatorAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFuelLocatorAddAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFuelLocatorRemoveAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnFuelLocatorRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;
                        
                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
   
              <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Client Code</span> <span class="tab-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=ClientHelp"
                        title="Help" target="_blank" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
                <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
            <telerik:RadGrid ID="dgClientCode" runat="server" AllowSorting="true" OnItemCreated="dgClientCode_ItemCreated"
                Visible="true" OnNeedDataSource="dgClientCode_BindData" OnItemCommand="dgClientCode_ItemCommand" OnPageIndexChanged="Budget_PageIndexChanged"
                OnUpdateCommand="dgClientCode_UpdateCommand" OnInsertCommand="dgClientCode_InsertCommand"
                OnDeleteCommand="dgClientCode_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
                AllowPaging="true" OnSelectedIndexChanged="dgClientCode_SelectedIndexChanged" OnPreRender="dgClientCode_PreRender"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="341px">
                <MasterTableView DataKeyNames="ClientID,ClientCD,ClientDescription,CustomerID,LastUpdUID,LastUpdTS,IsDeleted,IsInActive"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="ClientCD" HeaderText="Client Code" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="120px" FilterDelay="500"
                            FilterControlWidth="100px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ClientDescription" HeaderText="Description" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="560px" FilterDelay="500"
                            FilterControlWidth="540px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ClientID" HeaderText="ClientID" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" HeaderStyle-Width="80px">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div>
                            <div style="padding: 5px 5px; float: left; clear: both;">
                                <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                    Visible='<%# IsAuthorized(Permission.Database.AddClientCode)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                    Visible='<%# IsAuthorized(Permission.Database.EditClientCode)%>' ToolTip="Edit"
                                    CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                    Visible='<%# IsAuthorized(Permission.Database.DeleteClientCode)%>' runat="server"
                                    CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                            </div>
                            <div>
                                <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                            </div>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChangesTop" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            CssClass="button" ValidationGroup="save" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancelTop" Text="Cancel" runat="server" OnClick="Cancel_Click"
                            CssClass="button" CausesValidation="false" />
                    </td>
                </tr>
            </table>
            </div>
           

          
                     <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr valign="top">
                    <td valign="top" class="tdLabel150">
                        <span class="mnd_text">Client Code</span>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbCode" runat="server" MaxLength="5" ValidationGroup="save" OnTextChanged="Code_TextChanged"
                                        AutoPostBack="true" CssClass="text40"></asp:TextBox>
                                    <asp:HiddenField ID="hdnClientID" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Code is Required"
                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="save" ControlToValidate="tbCode"
                                        Display="Dynamic" CssClass="alert-text" SetFocusOnError="true" ErrorMessage="Code is Required"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr valign="top">
                    <td valign="top">
                        <span class="mnd_text">Description</span>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbDescription" runat="server" MaxLength="30" CssClass="text225"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                        ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-family: Arial; font-size: 12px; color: Black;">
                        Assign User
                    </td>
                    <td align="left">
                        <table cellpadding="0">
                            <tr>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Available:
                                </td>
                                <td>
                                </td>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Selected:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstUserAvailable" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px"></asp:ListBox>
                                </td>
                                <td valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnUserAdd" runat="server" CssClass="icon-next" OnClick="btnUserAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnUserAddAll" runat="server" CssClass="icon-last" OnClick="btnUserAddAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnUserRemoveAll" runat="server" CssClass="icon-first" OnClick="btnUserRemoveAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnUserRemove" runat="server" CssClass="icon-prev" OnClick="btnUserRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:ListBox ID="lstUserSelected" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-family: Arial; font-size: 12px; color: Black;">
                        Assign Fleet
                    </td>
                    <td align="left">
                        <table cellpadding="0">
                            <tr>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Available:
                                </td>
                                <td>
                                </td>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Selected:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstFleetAvailable" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px" AutoPostBack="false"></asp:ListBox>
                                </td>
                                <td valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFleetAdd" runat="server" CssClass="icon-next" OnClick="btnFleetAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFleetAddAll" runat="server" CssClass="icon-last" OnClick="btnFleetAddAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFleetRemoveAll" runat="server" CssClass="icon-first" OnClick="btnFleetRemoveAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFleetRemove" runat="server" CssClass="icon-prev" OnClick="btnFleetRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:ListBox ID="lstFleetSelected" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px" AutoPostBack="false"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-family: Arial; font-size: 12px; color: Black;">
                        Assign Crew
                    </td>
                    <td align="left">
                        <table cellpadding="0">
                            <tr>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Available:
                                </td>
                                <td>
                                </td>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Selected:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstCrewAvailable" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px"></asp:ListBox>
                                </td>
                                <td valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCrewAdd" runat="server" CssClass="icon-next" OnClick="btnCrewAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCrewAddAll" runat="server" CssClass="icon-last" OnClick="btnCrewAddAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCrewRemoveAll" runat="server" CssClass="icon-first" OnClick="btnCrewRemoveAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCrewRemove" runat="server" CssClass="icon-prev" OnClick="btnCrewRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:ListBox ID="lstCrewSelected" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-family: Arial; font-size: 12px; color: Black;">
                        Assign PAX
                    </td>
                    <td align="left">
                        <table cellpadding="0">
                            <tr>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Available:
                                </td>
                                <td>
                                </td>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Selected:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstPaxAvailable" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px"></asp:ListBox>
                                </td>
                                <td valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnPaxAdd" runat="server" CssClass="icon-next" OnClick="btnPaxAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnPaxAddAll" runat="server" CssClass="icon-last" OnClick="btnPaxAddAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnPaxRemoveAll" runat="server" CssClass="icon-first" OnClick="btnPaxRemoveAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnPaxRemove" runat="server" CssClass="icon-prev" OnClick="btnPaxRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:ListBox ID="lstPaxSelected" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-family: Arial; font-size: 12px; color: Black;">
                        Assign Department
                    </td>
                    <td align="left">
                        <table cellpadding="0">
                            <tr>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Available:
                                </td>
                                <td>
                                </td>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Selected:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstDeptAvailable" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px"></asp:ListBox>
                                </td>
                                <td valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnDeptAdd" runat="server" CssClass="icon-next" OnClick="btnDeptAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnDeptAddAll" runat="server" CssClass="icon-last" OnClick="btnDeptAddAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnDeptRemoveAll" runat="server" CssClass="icon-first" OnClick="btnDeptRemoveAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnDeptRemove" runat="server" CssClass="icon-prev" OnClick="btnDeptRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:ListBox ID="lstDeptSelected" runat="server" SelectionMode="Multiple" Width="200px"
                                        Height="200px"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-family: Arial; font-size: 12px; color: Black;">
                        Assign Flight Category
                    </td>
                    <td align="left">
                        <table cellpadding="0">
                            <tr>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Available:
                                </td>
                                <td>
                                </td>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Selected:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstFlightCategoryAvailable" runat="server" SelectionMode="Multiple"
                                        Width="200px" Height="200px"></asp:ListBox>
                                </td>
                                <td valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFlightCategoryAdd" runat="server" CssClass="icon-next" OnClick="btnFlightCategoryAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFlightCategoryAddAll" runat="server" CssClass="icon-last" OnClick="btnFlightCategoryAddAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFlightCategoryRemoveAll" runat="server" CssClass="icon-first"
                                                    OnClick="btnFlightCategoryRemoveAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFlightCategoryRemove" runat="server" CssClass="icon-prev" OnClick="btnFlightCategoryRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:ListBox ID="lstFlightCategorySelected" runat="server" SelectionMode="Multiple"
                                        Width="200px" Height="200px"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-family: Arial; font-size: 12px; color: Black;">
                        Assign Flight Purpose
                    </td>
                    <td align="left">
                        <table cellpadding="0">
                            <tr>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Available:
                                </td>
                                <td>
                                </td>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Selected:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstFlightPurposeAvailable" runat="server" SelectionMode="Multiple"
                                        Width="200px" Height="200px"></asp:ListBox>
                                </td>
                                <td valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFlightPurposeAdd" runat="server" CssClass="icon-next" OnClick="btnFlightPurposeAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFlightPurposeAddAll" runat="server" CssClass="icon-last" OnClick="btnFlightPurposeAddAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFlightPurposeRemoveAll" runat="server" CssClass="icon-first" OnClick="btnFlightPurposeRemoveAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFlightPurposeRemove" runat="server" CssClass="icon-prev" OnClick="btnFlightPurposeRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:ListBox ID="lstFlightPurposeSelected" runat="server" SelectionMode="Multiple"
                                        Width="200px" Height="200px"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-family: Arial; font-size: 12px; color: Black;">
                        Assign Fuel Locator
                    </td>
                    <td align="left">
                        <table cellpadding="0">
                            <tr>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Available:
                                </td>
                                <td>
                                </td>
                                <td style="font-family: Arial; font-size: 12px; color: Black;">
                                    Selected:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstFuelLocatorAvailable" runat="server" SelectionMode="Multiple"
                                        Width="200px" Height="200px"></asp:ListBox>
                                </td>
                                <td valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFuelLocatorAdd" runat="server" CssClass="icon-next" OnClick="btnFuelLocatorAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFuelLocatorAddAll" runat="server" CssClass="icon-last" OnClick="btnFuelLocatorAddAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFuelLocatorRemoveAll" runat="server" CssClass="icon-first" OnClick="btnFuelLocatorRemoveAll_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFuelLocatorRemove" runat="server" CssClass="icon-prev" OnClick="btnFuelLocatorRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:ListBox ID="lstFuelLocatorSelected" runat="server" SelectionMode="Multiple"
                                        Width="200px" Height="200px"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
               
            

            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            CssClass="button" ValidationGroup="save" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click" CssClass="button"
                            CausesValidation="false" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db top-bar">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo last-span" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
