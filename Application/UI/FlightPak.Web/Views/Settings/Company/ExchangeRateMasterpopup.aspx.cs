﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class ExchangeRateMasterpopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private string ExchRateCD;
        string DateFormat = "MM/dd/yyyy";
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }
                        //Added for Reassign the Selected Value and highlight the specified row in the Grid            
                        if (!string.IsNullOrEmpty(Request.QueryString["ExchRateCD"]))
                        {
                            ExchRateCD = Request.QueryString["ExchRateCD"].ToUpper().Trim();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }

        protected void dgExchangeRate_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["FromDT"];
                            TableCell cell1 = (TableCell)Item["ToDT"];
                            if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                            {
                                cell.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(cell.Text)));
                            }
                            if ((!string.IsNullOrEmpty(cell1.Text)) && (cell1.Text != "&nbsp;"))
                            {
                                cell1.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(cell1.Text)));
                            }


                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {

                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgExchangeRate_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetExchangeRate();
                            List<FlightPakMasterService.ExchangeRate> ExchangeRateList = new List<FlightPakMasterService.ExchangeRate>();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                ExchangeRateList = ObjRetVal.EntityList;
                            }
                            dgExchangeRate.DataSource = ExchangeRateList;
                            Session["ExchangeRate"] = ExchangeRateList;
                            if (chkSearchActiveOnly.Checked == true)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgExchangeRate;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }
        protected void SelectItem()
        {
            if (!string.IsNullOrEmpty(ExchRateCD))
            {
                foreach (GridDataItem item in dgExchangeRate.MasterTableView.Items)
                {
                    if (item["ExchRateCD"].Text.ToUpper().Trim() == ExchRateCD)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (dgExchangeRate.Items.Count > 0)
                {
                    dgExchangeRate.SelectedIndexes.Add(0);
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.ExchangeRate> lstExchangeRate = new List<FlightPakMasterService.ExchangeRate>();
                if (Session["ExchangeRate"] != null)
                {
                    lstExchangeRate = (List<FlightPakMasterService.ExchangeRate>)Session["ExchangeRate"];
                }
                if (lstExchangeRate.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstExchangeRate = lstExchangeRate.Where(x => x.IsInActive == false).ToList<ExchangeRate>(); }
                    dgExchangeRate.DataSource = lstExchangeRate;
                    if (IsDataBind)
                    {
                        dgExchangeRate.DataBind();
                    }
                }
                return false;
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            SearchAndFilter(true);
        }
        #endregion

        protected void dgExchangeRate_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    string resolvedurl = string.Empty;
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                GridDataItem item = dgExchangeRate.SelectedItems[0] as GridDataItem;
                                Session["SelectedExchangeRateID"] = item.GetDataKeyValue("ExchangeRateID").ToString().Trim();

                                if (Session["SelectedExchangeRateID"] != null)
                                {
                                    Session["SelectedExchangeRateID"] = item["ExchangeRateID"].Text;
                                    TryResolveUrl("/Views/Settings/Company/ExchangeRatesCatalog.aspx?IsPopup=", out resolvedurl);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "','radAddExchangeRate');", true);
                                }

                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/Company/ExchangeRatesCatalog.aspx?IsPopup=Add", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "','radAddExchangeRate');", true);
                                break;

                            case "Filter":

                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
            }
        }

        protected void dgExchangeRate_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgExchangeRate, Page.Session);
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgExchangeRate_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string strExchangeRateID = "";
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = dgExchangeRate.SelectedItems[0] as GridDataItem;
                        Session["SelectedExchangeRateID"] = item.GetDataKeyValue("ExchangeRateID").ToString().Trim();

                        if (Session["SelectedExchangeRateID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ExchangeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.ExchangeRate ExchangeRateType = new FlightPakMasterService.ExchangeRate();
                                string Code = Session["SelectedExchangeRateID"].ToString();
                                foreach (GridDataItem Item in dgExchangeRate.MasterTableView.Items)
                                {
                                    if (Item.GetDataKeyValue("ExchangeRateID").ToString().Trim() == Code.Trim())
                                    {
                                        if (Item.GetDataKeyValue("ExchangeRateID") != null)
                                        {
                                            strExchangeRateID = Item.GetDataKeyValue("ExchangeRateID").ToString().Trim();
                                        }
                                        if (Item.GetDataKeyValue("FromDT") != null)
                                        {
                                            ExchangeRateType.FromDT = Convert.ToDateTime(Item.GetDataKeyValue("FromDT"));
                                        }
                                        if (Item.GetDataKeyValue("ToDT") != null)
                                        {
                                            ExchangeRateType.ToDT = Convert.ToDateTime(Item.GetDataKeyValue("ToDT"));
                                        }
                                        if (Item.GetDataKeyValue("ExchRateDescription") != null)
                                        {
                                            ExchangeRateType.ExchRateCD = Item.GetDataKeyValue("ExchRateDescription").ToString().Trim();
                                        }
                                        if (Item.GetDataKeyValue("ExchRateCD") != null)
                                        {
                                            ExchangeRateType.ExchRateDescription = Item.GetDataKeyValue("ExchRateCD").ToString().Trim();
                                        }
                                        break;
                                    }
                                }
                                ExchangeRateType.ExchangeRateID = Convert.ToInt64(strExchangeRateID);
                                ExchangeRateType.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.ExchangeRate, Convert.ToInt64(strExchangeRateID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.ExchangeRate);
                                        return;
                                    }
                                    ExchangeRateService.DeleteExchangeRate(ExchangeRateType);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.ExchangeRate);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.ExchangeRate, Convert.ToInt64(strExchangeRateID));
                    }
                }
            }
        }
    }
}
