﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TripManagerPopUp.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.TripManagerPopUp" ClientIDMode="AutoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Preflight</title>
    <link href="../../../Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="../../../Scripts/jqgrid/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../../Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <link href="../../../Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="../../../Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="../../../Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../../Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="../../../Scripts/knockout/knockout.js"></script>
    <script type="text/javascript" src="../../../Scripts/knockout/knockout.mapping.js"></script>
    <script type="text/javascript" src="../../../Scripts/knockout/moment.min.js"></script>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
   <script type="text/javascript" src="../../../Scripts/Preflight/PreflightTripManagerCheckListGroup.js"></script>
 
     <script type="text/javascript">
         var selectedItems = [];
         var ismultiSelect = false;
         var selectedRowData = null;
         var jqgridTableId = '#dgTripManager';

         function GetRadWindow() {
             var oWindow = null;
             if (window.radWindow) oWindow = window.radWindow;
             else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
             return oWindow;
         }

         function returnToParent() {
             var oArg = new Object();
             var selectedrows = $('#dgTripManager').jqGrid('getGridParam', 'selrow');
             if (selectedrows == null)
             { showMessageBox('Please select a checklist', popupTitle); return false;}
             if (selectedrows.length > 0) {
                 var rowData = $('#dgTripManager').getRowData(selectedrows);
                 oArg.CheckGroupCD = rowData['CheckGroupCD'];
                 oArg.CheckGroupDescription = rowData['CheckGroupDescription'];
                 oArg.CheckGroupID = rowData['CheckGroupID'];
             }
             else {
                 oArg.CheckGroupCD = "";
                 oArg.CheckGroupDescription = "";
                 oArg.CheckGroupID = "";
             }

             var oWnd = GetRadWindow();
             if (oArg) {
                 oWnd.close(oArg);
             }
         }

         function Close() {
             GetRadWindow().Close();
         }
        </script>
</head>
<body>
    <form id="form1" runat="server">
   
    <div class="OuterDiv">  
            <input id="hdCheckGroupCD" runat="server" type="hidden" />
            <input id="hdCheckGroupDescription" runat="server" type="hidden" />
            <input id="hdnCheckGroupID" runat="server" type="hidden" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <input id="chkSearchActiveOnly" type="checkbox"/>Active Only
                                    </span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                            <input id="btnSearch" type="button" data-bind="event: { click: btnSearch_Click }" value="Search" class="button" />                           
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="dgTripManager" class="table table-striped table-hover table-bordered"></table>                   
                </td>
            </tr>
            <tr>
                    <td>
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            <tr>
                <td align="right" style="text-align:right!important">
                <input id="btnSubmitButton" type="button"  class="button" value="Ok" onclick="returnToParent(); return false;"/>
                 </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
