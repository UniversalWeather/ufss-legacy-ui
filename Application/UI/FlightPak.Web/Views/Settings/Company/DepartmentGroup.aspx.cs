﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class DepartmentGroup : BaseSecuredPage
    {
        #region "VARIABLES"
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private bool DepartmentGroupPageNavigated = false;
        private string strDepartmentGroupID = "";
        private string strDepartmentGroupCD = "";
        private bool IsValidate = true;
        Int64 customerID;
        private bool _selectLastModified = false;

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgDepartmentGroup, dgDepartmentGroup, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgDepartmentGroup.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewDepartmentGroup);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgDepartmentGroup.Rebind();
                                ReadOnlyForm();
                                LoadSelectedList();
                                DisableLinks();
                                EnableForm(false);
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// To select the selected Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDepartmentGroupID"] != null)
                    {
                        Boolean flag = false;
                        string ID = Session["SelectedDepartmentGroupID"].ToString();
                        foreach (GridDataItem item in dgDepartmentGroup.MasterTableView.Items)
                        {
                            if (item["DepartmentGroupID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                flag = true;
                                break;
                            }
                        }

                        if (flag == false)
                        {
                            DefaultSelection(false);
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Prerender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartmentGroup_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (DepartmentGroupPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgDepartmentGroup, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>                
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgDepartmentGroup.Rebind();
                    }
                    if (dgDepartmentGroup.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedDepartmentGroupID"] = null;
                        //}
                        if (Session["SelectedDepartmentGroupID"] == null)
                        {
                        dgDepartmentGroup.SelectedIndexes.Add(0);
                        Session["SelectedDepartmentGroupID"] = dgDepartmentGroup.Items[0].GetDataKeyValue("DepartmentGroupID").ToString();
                        }

                        if (dgDepartmentGroup.SelectedIndexes.Count == 0)
                            dgDepartmentGroup.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        LoadSelectedList();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnDelete = (LinkButton)dgDepartmentGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1 || e.Initiator.ID.IndexOf("lbtnDelete", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgDepartmentGroup;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartmentGroup_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                //target.Focus();  
                                RadAjaxManager1.FocusControl(target);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + target + "');", true);
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// Bind Department Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgDepartmentGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjDepartmentGroupVal = DepartmentService.GetDeptGroupInfo();
                            List<FlightPakMasterService.GetAllDepartmentGroup> DepartmentGroupList = new List<FlightPakMasterService.GetAllDepartmentGroup>();
                            if (ObjDepartmentGroupVal.ReturnFlag == true)
                            {
                                DepartmentGroupList = ObjDepartmentGroupVal.EntityList;
                            }
                            dgDepartmentGroup.DataSource = DepartmentGroupList;
                            Session["DeptGroupCodes"] = DepartmentGroupList;

                            if (DepartmentService.GetDeptGroupInfo().ReturnFlag == true)
                            {
                                dgDepartmentGroup.DataSource = DepartmentService.GetDeptGroupInfo().EntityList;
                            }
                            Session["GroupCodes"] = ObjDepartmentGroupVal.EntityList.ToList();

                            if (chkSearchActiveOnly.Checked == true)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// Item Command for Department Group Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartmentGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:

                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnHomeBase.Value = string.Empty;
                                hdnSave.Value = "Update";
                                if (Session["SelectedDepartmentGroupID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.DepartmentGroup, Convert.ToInt64(Session["SelectedDepartmentGroupID"].ToString().Trim()));
                                        Session["IsDepartmentGroupEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.DepartmentGroup);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();                                        
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                                        SelectItem();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgDepartmentGroup.SelectedIndexes.Clear();
                                hdnHomeBase.Value = string.Empty;
                                //tbCode.ReadOnly = false;
                                //tbCode.BackColor = System.Drawing.Color.White;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                lstSelected.Items.Clear();
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                                DisableLinks();
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// Update Command for Department Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDepartmentGroup_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        CheckHomebaseExist();
                        if (IsValidate)
                        {
                            if (Session["SelectedDepartmentGroupID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objDepartmentGroupService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDepartmentGroupService.UpdateDeptGroup(GetFormItems());
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.DepartmentGroup, Convert.ToInt64(Session["SelectedDepartmentGroupID"].ToString().Trim()));
                                            SaveDeptOrders(Convert.ToInt64(Session["SelectedDepartmentGroupID"].ToString().Trim()));
                                            LoadSelectedList();
                                            GridEnable(true, true, true);
                                            //DefaultSelection(true);
                                            SelectItem();
                                            ReadOnlyForm();
                                        }

                                        ShowSuccessMessage();
                                        EnableLinks();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.DepartmentGroup);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDepartmentGroup_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgDepartmentGroup.ClientSettings.Scrolling.ScrollTop = "0";
                        DepartmentGroupPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewGroup);
                }
            }
        }
        /// <summary>
        /// Insert Command for Department Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDepartmentGroup_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        if (CheckAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            IsValidate = false;
                        }
                        CheckHomebaseExist();
                        if (IsValidate)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.DepartmentGroup DepartmentGroup = new FlightPakMasterService.DepartmentGroup();
                                DepartmentGroup.DepartmentGroupCD = tbCode.Text;
                                DepartmentGroup.DepartmentGroupDescription = tbDescription.Text;
                                if (!string.IsNullOrEmpty(hdnHomeBase.Value))
                                {
                                    DepartmentGroup.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                                }
                                DepartmentGroup.IsDeleted = false;
                                DepartmentGroup.IsInActive = chkDepartmentInactive.Checked;
                                var objRetVal = DepartmentService.AddDeptGroup(DepartmentGroup);
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    DefaultSelection(true);
                                    // dgDepartmentGroup.Rebind();
                                    //if(dgDepartmentGroup.Items.Count>0)
                                    // dgDepartmentGroup.SelectedIndexes.Add(0);                               
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.DepartmentGroup);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDepartmentGroup_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedDepartmentGroupID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient DepartmentGroupTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.DepartmentGroup DepartmentGroup = new FlightPakMasterService.DepartmentGroup();
                                string Code = Session["SelectedDepartmentGroupID"].ToString();
                                strDepartmentGroupCD = "";
                                strDepartmentGroupID = "";
                                foreach (GridDataItem Item in dgDepartmentGroup.MasterTableView.Items)
                                {
                                    if (Item["DepartmentGroupID"].Text.Trim() == Code.Trim())
                                    {
                                        strDepartmentGroupCD = Item["DepartmentGroupCD"].Text.Trim();
                                        strDepartmentGroupID = Item["DepartmentGroupID"].Text.Trim();
                                        break;
                                    }
                                }
                                DepartmentGroup.DepartmentGroupCD = strDepartmentGroupCD;
                                DepartmentGroup.DepartmentGroupID = Convert.ToInt64(strDepartmentGroupID);
                                DepartmentGroup.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.DepartmentGroup, Convert.ToInt64(strDepartmentGroupID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.DepartmentGroup);
                                        return;
                                    }
                                    DepartmentGroupTypeService.DeleteDeptGroup(DepartmentGroup);

                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(true);
                                    _selectLastModified = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        if (Session["SelectedDepartmentGroupID"] != null)
                        {
                            var returnValue1 = CommonService.UnLock(EntitySet.Database.DepartmentGroup, Convert.ToInt64(Session["SelectedDepartmentGroupID"]));
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDepartmentGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgDepartmentGroup.SelectedItems[0] as GridDataItem;
                                Session["SelectedDepartmentGroupID"] = item["DepartmentGroupID"].Text;

                                ReadOnlyForm();
                                LoadSelectedList();
                                GridEnable(true, true, true);
                                ShowHideControls(true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                    }
                }
            }
        }
        /// <summary>
        /// Load Selected List Items in the Listbox
        /// </summary>
        protected void LoadSelectedList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    foreach (GridDataItem item in dgDepartmentGroup.MasterTableView.Items)
                    {
                        if (item.Selected)
                        {
                            Session["SelectedDepartmentGroupID"] = item["DepartmentGroupID"].Text;
                            item.Selected = true;
                        }
                    }
                    lstAvailable.Items.Clear();
                    lstSelected.Items.Clear();
                    List<FlightPakMasterService.Department> DeptAvailableList = new List<FlightPakMasterService.Department>();
                    List<FlightPakMasterService.Department> DeptSelectedList = new List<FlightPakMasterService.Department>();
                    List<object> TempAvlList = new List<object>();
                    List<object> TempSelList = new List<object>();
                    ArrayList deptGroupArray = new ArrayList();
                    RadListBoxItem lstItem;
                    DeptAvailableList = GetAvailableListByCode(customerID, Convert.ToInt64(Session["SelectedDepartmentGroupID"]));
                    if (DeptAvailableList != null)
                    {
                        foreach (FlightPakMasterService.Department deptList in DeptAvailableList)
                        {
                            lstItem = new RadListBoxItem();
                            lstItem.Text = deptList.DepartmentName.ToString().Trim() + " - " + "(" + deptList.DepartmentCD.ToString().Trim() + ")";
                            lstItem.Value = deptList.DepartmentID.ToString().Trim();
                            lstAvailable.Items.Add(lstItem);
                        }
                    }
                    Session["DeptGroupArray"] = deptGroupArray;
                    DeptSelectedList = GetSelectedListByCode(customerID, Convert.ToInt64(Session["SelectedDepartmentGroupID"]));
                    if (DeptSelectedList != null)
                    {
                        foreach (FlightPakMasterService.Department dSelList in DeptSelectedList)
                        {
                            lstItem = new RadListBoxItem();
                            lstItem.Text = dSelList.DepartmentName.ToString().Trim() + " - " + "(" + dSelList.DepartmentCD.ToString().Trim() + ")";
                            lstItem.Value = dSelList.DepartmentID.ToString().Trim();
                            lstSelected.Items.Add(lstItem);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportICAOCd.Trim() != string.Empty)
                    {
                        tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd.ToString().Trim();
                        if (UserPrincipal != null && UserPrincipal.Identity._homeBaseId != 0)
                        {
                            hdnHomeBase.Value = Convert.ToString(UserPrincipal.Identity._homeBaseId);
                        }
                    }
                    else
                    {
                        tbHomeBase.Text = string.Empty;

                    }
                    EnableForm(true);
                    ShowHideControls(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Edit Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    //tbCode.ReadOnly = true;
                    //tbCode.BackColor = System.Drawing.Color.LightGray;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    tbCode.Enabled = false;
                    ShowHideControls(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Department Group Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgDepartmentGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgDepartmentGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            ShowHideControls(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    
                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// Cancel Department Group Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedDepartmentGroupID"] != null)
                            {
                                //Unlock should happen from Service Layer                                
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.DepartmentGroup, Convert.ToInt64(Session["SelectedDepartmentGroupID"].ToString().Trim()));
                                }
                                //Session.Remove("SelectedDepartmentGroupID");
                                DefaultSelection(false);
                            }
                        }
                        //Session["SelectedItem"] = null;
                        DefaultSelection(false);
                        ShowHideControls(true);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
            
            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// Function to Check if Passenger Group Code Alerady Exists
        /// </summary>
        /// <returns></returns>
        private Boolean CheckAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["GroupCodes"] != null)
                    {
                        List<GetAllDepartmentGroup> DepartmentGroupList = new List<GetAllDepartmentGroup>();
                        DepartmentGroupList = ((List<GetAllDepartmentGroup>)Session["GroupCodes"]).Where(x => x.DepartmentGroupCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<GetAllDepartmentGroup>();
                        if (DepartmentGroupList.Count != 0)
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID);  //tbCode.Focus();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            ReturnValue = true;
                        }
                    }
                    else
                    {
                        RadAjaxManager1.FocusControl(tbHomeBase.ClientID);  //tbHomeBase.Focus();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbHomeBase.ClientID + "');", true);
                        ReturnValue = false;
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgDepartmentGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDelCtl = (LinkButton)dgDepartmentGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgDepartmentGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddDepartmentGroup))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteDepartmentGroup))
                    {
                        lbtnDelCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDelCtl.Enabled = true;
                            lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtnDelCtl.Enabled = false;
                            lbtnDelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditDepartmentGroup))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Function to Enable / Diable the form fields
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //if (Enable == false)
                    //{
                    //    tbCode.ReadOnly = false;                      
                    //}
                    tbCode.Enabled = Enable;
                    tbDescription.Enabled = Enable;
                    tbHomeBase.Enabled = Enable;
                    lstAvailable.Enabled = Enable;
                    lstAvailable.EnableDragAndDrop = Enable;
                    tbAvailableFilter.Enabled = Enable;
                    lstSelected.Enabled = Enable;
                    lstSelected.EnableDragAndDrop = Enable;
                    btnHomeBase.Enabled = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChanges.Visible = Enable;
                    chkDepartmentInactive.Enabled = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Function to Get Form Items
        /// </summary>
        /// <returns></returns>
        private FlightPakMasterService.DepartmentGroup GetFormItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.DepartmentGroup DepartmentGroup = new FlightPakMasterService.DepartmentGroup();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    DepartmentGroup.DepartmentGroupCD = tbCode.Text;
                    DepartmentGroup.DepartmentGroupDescription = tbDescription.Text;
                    if (!string.IsNullOrEmpty(hdnHomeBase.Value))
                    {
                        DepartmentGroup.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                    }
                    foreach (GridDataItem Item in dgDepartmentGroup.MasterTableView.Items)
                    {
                        if (Item["DepartmentGroupID"].Text.Trim() == Session["SelectedDepartmentGroupID"].ToString().Trim())
                        {
                            DepartmentGroup.DepartmentGroupID = Convert.ToInt64(Item["DepartmentGroupID"].Text.Trim());
                            break;
                        }
                    }
                    DepartmentGroup.IsDeleted = false;
                    DepartmentGroup.IsInActive = chkDepartmentInactive.Checked;
                    return DepartmentGroup;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return DepartmentGroup;
            }
        }
        /// <summary>
        /// Delete from DepartmentGroupOrder Table, based on DeptCode and DeptGroupCode
        /// </summary>
        /// <param name="DeptCode">Pass Department Code</param>
        /// <param name="DeptGroupCode">Pass Department Group Code</param>
        protected void DeleteSeletedItem(string DeptCode, string DeptGroupCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DeptCode, DeptGroupCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        FlightPakMasterService.DepartmentGroupOrder DeptGroupOrder = new FlightPakMasterService.DepartmentGroupOrder();
                        if (DeptGroupCode != null && DeptGroupCode != string.Empty)
                        {
                            DeptGroupOrder.DepartmentGroupOrderID = Convert.ToInt64(DeptGroupCode);
                        }
                        DeptGroupOrder.DepartmentGroupID = Convert.ToInt64(DeptCode);
                        DepartmentService.DeleteDeptGroupOrder(DeptGroupOrder);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Get Available List by DeptCode
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <param name="DeptCode"></param>
        /// <returns></returns>
        public List<FlightPakMasterService.Department> GetAvailableListByCode(Int64 CustomerID, Int64 DeptCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, DeptCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ReturnValue = DepartmentService.GetDeptAvailableList(CustomerID, DeptCode);
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ReturnValue != null)
                        {
                            return ReturnValue.EntityList.ToList();
                        }
                        return null;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    return ReturnValue.EntityList.ToList();
                }
            }
        }
        /// <summary>
        /// To Get Selected List by DeptCode
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <param name="DeptCode"></param>
        /// <returns></returns>
        public List<FlightPakMasterService.Department> GetSelectedListByCode(Int64 CustomerID, Int64 DeptCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, DeptCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ReturnValue = DepartmentService.GetDeptSelectedList(CustomerID, DeptCode);
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ReturnValue != null)
                        {
                            return ReturnValue.EntityList.ToList();
                        }
                        return null;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    return ReturnValue.EntityList.ToList();
                }
            }
        }
        /// <summary>
        /// To save the selected list for DeptGroupCode
        /// </summary>
        /// <param name="DeptGroupCode"></param>
        protected void SaveDeptOrders(Int64 DeptGroupID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DeptGroupID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        FlightPakMasterService.DepartmentGroupOrder DeptGroupOrder = new FlightPakMasterService.DepartmentGroupOrder();
                        DeleteSeletedItem(Session["SelectedDepartmentGroupID"].ToString().Trim(), string.Empty);
                        string SelectedValue = string.Empty;
                        for (int Index = 0; Index < lstSelected.Items.Count; Index++)
                        {
                            DeptGroupOrder.DepartmentGroupID = Convert.ToInt64(DeptGroupID.ToString());
                            DeptGroupOrder.OrderNum = Index;
                            DeptGroupOrder.DepartmentID = Convert.ToInt64(lstSelected.Items[Index].Value);
                            var ReturnValue = DepartmentService.AddDeptGroupOrder(DeptGroupOrder);
                        }
                        dgDepartmentGroup.Rebind();
                        GridEnable(true, true, true);
                        DisplayInsertForm();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>        
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgDepartmentGroup.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = dgDepartmentGroup.SelectedItems[0] as GridDataItem;
                        Label lbLastUpdatedUser = (Label)dgDepartmentGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                        }
                        LoadControlData();
                        EnableForm(false);
                    }
                    else
                    {
                        DefaultSelection(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Load Selected Item Data into the Form Fields
        /// </summary>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDepartmentGroupID"] != null)
                    {
                        strDepartmentGroupID = "";
                        foreach (GridDataItem Item in dgDepartmentGroup.MasterTableView.Items)
                        {
                            if (Item["DepartmentGroupID"].Text.Trim() == Session["SelectedDepartmentGroupID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("DepartmentGroupCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("DepartmentGroupCD").ToString().Trim();
                                }
                                else
                                {
                                    tbCode.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("DepartmentGroupID") != null)
                                {
                                    strDepartmentGroupID = Item.GetDataKeyValue("DepartmentGroupID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("DepartmentGroupDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("DepartmentGroupDescription").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HomeBaseCD") != null)
                                {
                                    tbHomeBase.Text = Item.GetDataKeyValue("HomeBaseCD").ToString().Trim();
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HomebaseID") != null)
                                {
                                    hdnHomeBase.Value = Item["HomebaseID"].Text.Trim();
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null && Item.GetDataKeyValue("IsInActive").ToString().ToUpper().Trim() == "TRUE")
                                {
                                    chkDepartmentInactive.Checked = true;
                                }
                                else
                                {
                                    chkDepartmentInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Group Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["DepartmentGroupCD"].Text;
                                lbColumnValue2.Text = Item["DepartmentGroupDescription"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To move a single record from available list to selected list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNext_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lstAvailable.SelectedItem != null)
                        {
                            string SelectedValue = string.Empty;
                            ArrayList SelectedArrayList = new ArrayList();
                            for (int Index = lstAvailable.Items.Count - 1; Index >= 0; Index--)
                            {
                                if (lstAvailable.Items[Index].Selected)
                                {
                                    SelectedValue = lstAvailable.Items[Index].Text;
                                    ArrayList TempSelectedList = new ArrayList(SelectedValue.Split(new System.Char[] { '-' }));
                                    SelectedArrayList.Add(TempSelectedList[1].ToString().Trim().Replace("(", string.Empty).Replace(")", string.Empty));
                                    lstSelected.Items.Add(lstAvailable.Items[Index]);
                                    lstAvailable.Items.Remove(lstAvailable.Items[Index]);
                                }
                            }
                            lstSelected.SelectedIndex = -1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// To move a single record from selected list to available list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrevious_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lstSelected.SelectedItem != null)
                        {
                            string SelectedValue = string.Empty;
                            ArrayList SelectedArrayList = new ArrayList();
                            for (int Index = lstSelected.Items.Count - 1; Index >= 0; Index--)
                            {
                                if (lstSelected.Items[Index].Selected)
                                {
                                    SelectedValue = lstSelected.Items[Index].Text;
                                    ArrayList TempSelectedList = new ArrayList(SelectedValue.Split(new System.Char[] { '-' }));
                                    SelectedArrayList.Add(TempSelectedList[1].ToString().Trim().Replace("(", string.Empty).Replace(")", string.Empty));
                                    lstAvailable.Items.Add(lstSelected.Items[Index]);
                                    lstSelected.Items.Remove(lstSelected.Items[Index]);
                                }
                            }
                            lstAvailable.SelectedIndex = -1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// To move all records from available list to selected list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLast_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ArrayList SelectedArrayList = new ArrayList();
                        for (int Index = lstAvailable.Items.Count - 1; Index >= 0; Index--)
                        {
                            string SelectedValue = lstAvailable.Items[Index].Text;
                            ArrayList TempSelectedArrayList = new ArrayList(SelectedValue.Split(new System.Char[] { '-' }));
                            SelectedArrayList.Add(TempSelectedArrayList[1].ToString().Trim().Replace("(", string.Empty).Replace(")", string.Empty));

                            foreach (RadListBoxItem item in SelectedArrayList)
                            {
                                lstAvailable.Items.Remove(item);
                                lstSelected.Items.Add(item);
                            }
                        }
                        lstSelected.SelectedIndex = -1;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// To move all records from selected list to available list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFirst_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ArrayList SelectedArrayList = new ArrayList();
                        for (int Index = lstSelected.Items.Count - 1; Index >= 0; Index--)
                        {
                            string SelectedValue = lstSelected.Items[Index].Text;
                            ArrayList TempSelectedArrayList = new ArrayList(SelectedValue.Split(new System.Char[] { '-' }));
                            SelectedArrayList.Add(TempSelectedArrayList[1].ToString().Trim().Replace("(", string.Empty).Replace(")", string.Empty));
                            lstAvailable.Items.Add(lstSelected.Items[Index]);
                            lstSelected.Items.Remove(lstSelected.Items[Index]);
                        }
                        lstAvailable.SelectedIndex = -1;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// To Clear the Form
        /// </summary>        
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    hdnHomeBase.Value = string.Empty;
                    lstAvailable.Items.Clear();
                    tbAvailableFilter.Text = string.Empty;
                    lstSelected.Items.Clear();
                    chkDepartmentInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
                        {
                            if (CheckAllReadyExist())
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        /// <summary>
        /// To check Valid Home Base
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckHomebaseExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DepartmentGroup);
                }
            }
        }
        private void CheckHomebaseExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (tbHomeBase.Text != null && tbHomeBase.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ReturnValue = DepartmentService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.Trim().ToUpper() == (tbHomeBase.Text.Trim().ToUpper())).ToList();
                            if (ReturnValue.Count() > 0 && ReturnValue != null)
                            {
                                hdnHomeBase.Value = ((FlightPakMasterService.GetAllCompanyMaster)ReturnValue[0]).HomebaseID.ToString().Trim();
                                tbHomeBase.Text = ((FlightPakMasterService.GetAllCompanyMaster)ReturnValue[0]).HomebaseCD;
                            }
                            else
                            {
                                cvHomeBase.IsValid = false;
                                //tbHomeBase.Focus();
                                RadAjaxManager1.FocusControl(tbHomeBase.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbHomeBase.ClientID + "');", true);
                                IsValidate = false;
                            }
                        }
                    }
                    else
                    {
                        hdnHomeBase.Value = string.Empty;

                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void ShowHideControls(bool Visibility)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Visibility))
            {
                lstAvailable.Visible = Visibility;
                tbAvailableFilter.Visible = Visibility;
                lstSelected.Visible = Visibility;
                lbOrderDetails.Visible = Visibility;
                lbDeptAvailable.Visible = Visibility;
                lbDeptSelected.Visible = Visibility;
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                        _selectLastModified = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllDepartmentGroup> lstDepartmentGroup = new List<FlightPakMasterService.GetAllDepartmentGroup>();
                if (Session["DeptGroupCodes"] != null)
                {
                    lstDepartmentGroup = (List<FlightPakMasterService.GetAllDepartmentGroup>)Session["DeptGroupCodes"];
                }
                if (lstDepartmentGroup.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstDepartmentGroup = lstDepartmentGroup.Where(x => x.IsInActive == false).ToList<GetAllDepartmentGroup>(); }
                    else
                    {
                        lstDepartmentGroup = lstDepartmentGroup.ToList<GetAllDepartmentGroup>();
                    }

                    dgDepartmentGroup.DataSource = lstDepartmentGroup;
                    if (IsDataBind)
                    {
                        dgDepartmentGroup.DataBind();
                    }
                }
              
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }
        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {

                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgDepartmentGroup.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }

        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var DepartmentValue = FPKMstService.GetDeptGroupInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, DepartmentValue);
            List<FlightPakMasterService.GetAllDepartmentGroup> filteredList = GetFilteredList(DepartmentValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.DepartmentGroupID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgDepartmentGroup.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgDepartmentGroup.CurrentPageIndex = PageNumber;
            dgDepartmentGroup.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllDepartmentGroup DepartmentValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedDepartmentGroupID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = DepartmentValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().DepartmentGroupID;
                Session["SelectedDepartmentGroupID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllDepartmentGroup> GetFilteredList(ReturnValueOfGetAllDepartmentGroup DepartmentValue)
        {
            List<FlightPakMasterService.GetAllDepartmentGroup> filteredList = new List<FlightPakMasterService.GetAllDepartmentGroup>();

            if (DepartmentValue.ReturnFlag == true)
            {
                filteredList = DepartmentValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked == true)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetAllDepartmentGroup>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgDepartmentGroup.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgDepartmentGroup.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}
