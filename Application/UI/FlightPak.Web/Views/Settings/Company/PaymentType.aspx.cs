﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class PaymentType : BaseSecuredPage
    {
        private List<string> lstPaymentTypeCode = new List<string>();
        private bool PaymentTypePageNavigated = false;
        private ExceptionManager exManager;
        private string strPaymentCD = "";
        private string strPaymentID = "";
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        {

                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPaymentType, dgPaymentType, RadAjaxLoadingPanel1);
                            // Store the clientID of the grid to reference it later on the client
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format("window['gridId'] = '{0}';", dgPaymentType.ClientID));
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                            // Grid Control could be ajaxified when the page is initially loaded.

                            if (!IsPostBack)
                            {
                                //To check the page level access.
                                CheckAutorization(Permission.Database.ViewPaymentType);
                                // Method to display first record in read only format   
                                if (Session["SearchItemPrimaryKeyValue"] != null)
                                {
                                    dgPaymentType.Rebind();
                                    ReadOnlyForm();
                                    EnableForm(false);
                                    DisableLinks();
                                    Session.Remove("SearchItemPrimaryKeyValue");
                                }
                                else
                                {
                                    chkSearchActiveOnly.Checked = true;
                                    DefaultSelection(true);
                                }
                                if (IsPopUp)
                                {
                                    dgPaymentType.Visible = false;
                                    chkSearchActiveOnly.Visible = false;
                                    // Show Insert Form
                                    if (IsAdd)
                                    {
                                        (dgPaymentType.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                    }
                                    else
                                    {
                                        Session["SelectedPaymentID"] = Request.QueryString["SelectedPaymentID"];
                                        (dgPaymentType.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }

        }

        protected void DefaultSelection(bool BindDataSwitch)
        {
            if (IsPopUp)
                dgPaymentType.AllowPaging = false;

            if (BindDataSwitch)
            {
                dgPaymentType.Rebind();
            }
            if (dgPaymentType.MasterTableView.Items.Count > 0)
            {
                //if (!IsPostBack)
                //{
                //    Session["SelectedPaymentID"] = null;
                //}
                if (Session["SelectedPaymentID"] == null)
                {
                    dgPaymentType.SelectedIndexes.Add(0);
                    if (!IsPopUp)
                    {
                        Session["SelectedPaymentID"] = dgPaymentType.Items[0].GetDataKeyValue("PaymentTypeID").ToString();
                    }
                }
                
                if (dgPaymentType.SelectedIndexes.Count == 0)
                    dgPaymentType.SelectedIndexes.Add(0);

                ReadOnlyForm();
                GridEnable(true, true, true);
            }
            else
            {
                ClearForm();
                EnableForm(false);
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PaymentType_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }
        }

        protected void PaymentType_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            var PaymentType = Service.GetPaymentType();
                            List<FlightPakMasterService.PaymentType> PaymentTypelst = new List<FlightPakMasterService.PaymentType>();
                            if (PaymentType.ReturnFlag == true)
                            {
                                PaymentTypelst = PaymentType.EntityList;
                            }
                            dgPaymentType.DataSource = PaymentTypelst;
                            Session["PaymentCD"] = PaymentTypelst;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                            //Session.Remove("PaymentCD");
                            //lstPaymentTypeCode = new List<string>();
                            //foreach (FlightPakMasterService.PaymentType PaymentTypeEntity in PaymentType.EntityList)
                            //{
                            //    lstPaymentTypeCode.Add(PaymentTypeEntity.PaymentTypeCD.ToLower().Trim());
                            //}
                            //Session["PaymentCD"] = lstPaymentTypeCode;                            
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                    }
                }
            }
        }
        /// <summary>
        /// Item Command for Payment Type Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PaymentType_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                if (Session["SelectedPaymentID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.PaymentType, Convert.ToInt64(Session["SelectedPaymentID"].ToString().Trim()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PaymentType);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PaymentType);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        //SelectItem();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                //tbCode.ReadOnly = false;
                                //tbCode.BackColor = System.Drawing.Color.White;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                dgPaymentType.SelectedIndexes.Clear();
                                tbCode.Focus();
                                DisableLinks();
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }
        }

        /// <summary>
        /// Update Command for Payment Type Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void PaymentType_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedPaymentID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient PaymentService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ReturnValue = PaymentService.UpdatePaymentType(GetItems());
                                if (ReturnValue.ReturnFlag == true)
                                {
                                    //Unlock should happen from Service Layer?
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.PaymentType, Convert.ToInt64(Session["SelectedPaymentID"].ToString().Trim()));
                                    }
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    //dgPaymentType.Rebind();
                                    SelectItem();
                                    ReadOnlyForm();

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.PaymentType);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            // Session["SelectedPaymentID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaymentTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }
        }
        /// <summary>
        /// Update Command for Payment Type Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void PaymentType_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (checkAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient PaymentService = new MasterCatalogServiceClient())
                            {
                                var ReturnValue = PaymentService.AddPaymentType(GetItems());
                                if (ReturnValue.ReturnFlag == true)
                                {
                                    dgPaymentType.Rebind();
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.PaymentType);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaymentTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }

        }


        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            checkAllReadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }

        }



        private void SelectItem()
        {
            if (Session["SelectedPaymentID"] != null)
            {
                string ID = Session["SelectedPaymentID"].ToString();
                foreach (GridDataItem Item in dgPaymentType.MasterTableView.Items)
                {
                    if (Item["PaymentTypeID"].Text.Trim() == ID.Trim())
                    {
                        Item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                DefaultSelection(false);
            }
        }


        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void PaymentType_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["SelectedPaymentID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient PaymentService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.PaymentType PaymentType = new FlightPakMasterService.PaymentType();
                                    string Code = Session["SelectedPaymentID"].ToString();
                                    strPaymentCD = "";
                                    strPaymentID = "";
                                    foreach (GridDataItem Item in dgPaymentType.MasterTableView.Items)
                                    {
                                        if (Item["PaymentTypeID"].Text.Trim() == Code.Trim())
                                        {
                                            strPaymentCD = Item["PaymentTypeCD"].Text.Trim();
                                            strPaymentID = Item["PaymentTypeID"].Text.Trim();
                                            break;
                                        }
                                    }
                                    PaymentType.PaymentTypeCD = strPaymentCD;
                                    PaymentType.PaymentTypeID = Convert.ToInt64(strPaymentID);
                                    PaymentType.IsDeleted = true;
                                    //Lock will happen from UI                                  
                                    var returnValue = CommonService.Lock(EntitySet.Database.PaymentType, Convert.ToInt64(strPaymentID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PaymentType);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PaymentType);
                                        return;
                                    }
                                    PaymentService.DeletePaymentType(PaymentType);
                                    //Unlock should happen from Service Layer
                                    var returnValue1 = CommonService.UnLock(EntitySet.Database.PaymentType, Convert.ToInt64(strPaymentID));
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgPaymentType.Rebind();
                                    DefaultSelection(false);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                    }
                    finally
                    {
                        // For Positive and negative cases need to unlock the record
                        // The same thing should be done for Edit
                        if (Session["SelectedPaymentID"] != null)
                        {
                            var returnValue1 = CommonService.UnLock(EntitySet.Database.PaymentType, Convert.ToInt64(Session["SelectedPaymentID"]));
                        }
                    }
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (hdnSaveFlag.Value != "Save")
            {
                SelectItem();
            }
            
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgPaymentType.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgPaymentType.Rebind();
                    SelectItem();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSaveFlag.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgPaymentType.SelectedItems[0] as GridDataItem;
                                Session["SelectedPaymentID"] = item["PaymentTypeID"].Text;
                                if (btnSaveChanges.Visible == false)
                                {
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                    }
                }
            }
        }

        protected void PaymentType_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgPaymentType.ClientSettings.Scrolling.ScrollTop = "0";
            PaymentTypePageNavigated = true;
        }

        protected void PaymentType_PreRender(object sender, EventArgs e)
        {
            GridEnable(true, true, true);
            if (hdnSaveFlag.Value != "Save")
            {
                SelectItem();
            }
            else if (PaymentTypePageNavigated)
            {
                SelectItem();
            }
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPaymentType, Page.Session);
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgPaymentType;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }

        }

        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSaveFlag.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbDescription.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;
                    chkInactive.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    //tbCode.ReadOnly = true;
                    //tbCode.BackColor = System.Drawing.Color.LightGray;
                    pnlExternalForm.Visible = true;
                    hdnSaveFlag.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    tbCode.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgPaymentType.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = dgPaymentType.SelectedItems[0] as GridDataItem;

                        Label lbLastUpdatedUser = (Label)dgPaymentType.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");

                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            //To retrieve back the UTC date in Homebase format
                            //lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDate(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));
                        }
                        LoadControlData();
                        EnableForm(false);
                    }
                    else
                    {
                        DefaultSelection(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);


            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Session["SelectedPaymentID"] != null)
                        {
                            strPaymentCD = "";
                            strPaymentID = "";
                            string strPaymentTypeDescription = "";
                            foreach (GridDataItem Item in dgPaymentType.MasterTableView.Items)
                            {
                                if (Item["PaymentTypeID"].Text.Trim() == Session["SelectedPaymentID"].ToString().Trim())
                                {
                                    strPaymentCD = Item["PaymentTypeCD"].Text.Trim();
                                    strPaymentID = Item["PaymentTypeID"].Text.Trim();
                                    strPaymentTypeDescription = Item["PaymentTypeDescription"].Text.Trim();
                                    if (Item.GetDataKeyValue("IsInActive") != null)
                                    {
                                        chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    else
                                    {
                                        chkInactive.Checked = false;
                                    }
                                    lbColumnName1.Text = "Payment Code";
                                    lbColumnName2.Text = "Description";
                                    lbColumnValue1.Text = Item["PaymentTypeCD"].Text;
                                    lbColumnValue2.Text = Item["PaymentTypeDescription"].Text;
                                    break;
                                }
                            }
                            tbCode.Text = strPaymentCD;
                            if (strPaymentTypeDescription != null)
                            {
                                tbDescription.Text = strPaymentTypeDescription;
                            }
                            else
                            {
                                tbDescription.Text = string.Empty;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Payment Type Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            (dgPaymentType.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgPaymentType.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                        GridEnable(true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSaveFlag.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }

        }

        /// <summary>
        /// Cancel Payment Type Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            if (Session["SelectedPaymentID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.PaymentType, Convert.ToInt64(Session["SelectedPaymentID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        EnableLinks();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaymentTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }

            hdnSaveFlag.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgPaymentType.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgPaymentType.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgPaymentType.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        if (IsAuthorized(Permission.Database.AddPaymentType))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                            {
                                lbtnInsertCtl.Enabled = true;
                            }
                            else
                            {
                                lbtnInsertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.DeletePaymentType))
                        {
                            lbtnDelCtl.Visible = true;
                            if (Delete)
                            {
                                lbtnDelCtl.Enabled = true;
                                lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                            }
                            else
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.EditPaymentType))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                            {
                                lbtnEditCtl.Enabled = true;
                                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnEditCtl.Enabled = false;
                                lbtnEditCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
            }
        }

        private FlightPakMasterService.PaymentType GetItems()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.PaymentType PaymentType = null;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PaymentType = new FlightPakMasterService.PaymentType();
                        if (hdnSaveFlag.Value == "Update")
                        {
                            if (Session["SelectedPaymentID"] != null)
                            {
                                PaymentType.PaymentTypeID = Convert.ToInt64(Session["SelectedPaymentID"].ToString().Trim());
                            }
                        }
                        else
                        {
                            PaymentType.PaymentTypeID = 0;
                        }
                        PaymentType.PaymentTypeCD = tbCode.Text.Trim();
                        PaymentType.PaymentTypeDescription = tbDescription.Text.Trim();
                        PaymentType.IsDeleted = false;
                        PaymentType.IsInActive = chkInactive.Checked;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
                return PaymentType;
            }

        }

        private bool checkAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        using (FlightPakMasterService.MasterCatalogServiceClient objPaymentTypesvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objPaymentTypesvc.GetPaymentType().EntityList.Where(x => x.PaymentTypeCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                            if (objRetVal.Count() > 0 && objRetVal != null)
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID);
                                returnVal = true;
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PaymentType);
                }
                return returnVal;
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.PaymentType> lstPaymentType = new List<FlightPakMasterService.PaymentType>();
                if (Session["PaymentCD"] != null)
                {
                    lstPaymentType = (List<FlightPakMasterService.PaymentType>)Session["PaymentCD"];
                }
                if (lstPaymentType.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstPaymentType = lstPaymentType.Where(x => x.IsInActive == false).ToList<FlightPakMasterService.PaymentType>(); }
                    dgPaymentType.DataSource = lstPaymentType;
                    if (IsDataBind)
                    {
                        dgPaymentType.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgPaymentType.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    chkInactive.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var PaymentValue = FPKMstService.GetPaymentType();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, PaymentValue);
            List<FlightPakMasterService.PaymentType> filteredList = GetFilteredList(PaymentValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.PaymentTypeID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgPaymentType.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgPaymentType.CurrentPageIndex = PageNumber;
            dgPaymentType.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfPaymentType PaymentValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedPaymentID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = PaymentValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().PaymentTypeID;
                Session["SelectedPaymentID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.PaymentType> GetFilteredList(ReturnValueOfPaymentType PaymentValue)
        {
            List<FlightPakMasterService.PaymentType> filteredList = new List<FlightPakMasterService.PaymentType>();

            if (PaymentValue.ReturnFlag == true)
            {
                filteredList = PaymentValue.EntityList;
            }

            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<FlightPakMasterService.PaymentType>(); }
                }
            }

            return filteredList;
        }
    }
}