﻿#region "Using Section"
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using System.Text;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Collections;
using FlightPak.Web.Framework.Helpers;
#endregion


namespace FlightPak.Web.Views.Settings.Company
{
    public partial class CompanyProfileCatalog : BaseSecuredPage
    {
        /*
         * Code Modification Details
         * ---------------------------------------------------------------------------
         *  Date        Developer   Task/Bug ID       Comments
         * ---------------------------------------------------------------------------
         *  17-05-2013  Mohanraja                   - Added methods LoadInvoiceDefaults, LoadQuoteDefaults for
         *                                              maintaing session state for Quote and Invoice report info, if
         *                                              the Quote and Invoice popup is not opened while saving.
         * 
         *  18-06-2013  Mohanraja                   - Updated code for GET/SET IsBlackberrySupport property.
         * 
         *  30-01-2014  Karthikeyan                 - Fixed 2483,2235.
         * */

        #region "Public Variables"
        bool IsValid = true;
        private ExceptionManager exManager;
        // To Store Image
        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }
        String PrivateTrip = "";
        private bool CompanyPageNavigated = false;
        private bool IsEmptyCheck = true;
        private bool TenthMin = false;
        private bool IsTimeDisplay = false;
        private bool IsCrewOName = true;
        private bool IsPaxOName = true;
        private bool IstbPaxFirst = true;
        private bool IstbPaxSecond = true;
        private bool IstbPaxThird = true;
        private bool IstbCrewFirst = true;
        private bool IstbCrewSecond = true;
        private bool IstbCrewThird = true;
        List<string> lstCompany = new List<string>();
        string strErrUploadNoFly = string.Empty;
        string strErrUploadSelect = string.Empty;
        string strErrUploadClear = string.Empty;
        string strErrUpload = string.Empty;
        StringBuilder strToolTipTaxes = new System.Text.StringBuilder();
        StringBuilder strToolTipUS = new System.Text.StringBuilder();
        //FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient();
        //long CustomerID = 10000;
        //string CustomerName = "UC";
        public const string constvalue = "value";
        private bool _selectLastModified = false;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCompanyProfileCatalog, dgCompanyProfileCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCompanyProfileCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (UserPrincipal != null && UserPrincipal.Identity._isAPIS != null)
                        {
                            if (UserPrincipal.Identity._isAPIS == true)
                            {
                                //chkEnableAPISInterface.Visible = true;
                                //lbURLAddress.Visible = true;
                                //tbDomain.Visible = true; 
                                //tbURLAddress.Visible = true;
                                //lbDomain.Visible = true;
                                pnlbarAPISInterface.Visible = true;
                                btnAPISInterface.Visible = true;
                            }
                            else
                            {
                                //chkEnableAPISInterface.Visible = false;
                                //lbURLAddress.Visible = false;
                                //lbDomain.Visible = false;
                                //tbDomain.Visible = false;
                                //tbURLAddress.Visible = false;
                                pnlbarAPISInterface.Visible = false;
                                btnAPISInterface.Visible = false;
                            }
                        }
                        else
                        {
                            //chkEnableAPISInterface.Visible = false;
                            //lbDomain.Visible = false;
                            //lbDomain.Visible = false;
                            //tbDomain.Visible = false;
                            //tbURLAddress.Visible = false;
                            pnlbarAPISInterface.Visible = false;
                            btnAPISInterface.Visible = false;
                        }
                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewCompanyProfileCatalog);
                            // Method to display first record in read only format                              

                            GetPostBackEventReference(this);
                            dgCompanyProfileCatalog.Rebind();

                            Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                            CreateDictionayForImgUpload();
                            SelectedItem();
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            
                            litCQLogoMsg.Text = DataValidation.ReportLogoMessage;
                            litPFLogoMsg.Text = DataValidation.ReportLogoMessage;
                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                            else if (eventArgument == "DeleteImage_Click")
                            {
                                DeleteImage_Click(sender, e);
                            }
                            else if (eventArgument == "LoadCQImage")
                            {
                                LoadCQImage();
                            }
                        }
                        if (IsPopUp)
                        {
                            dgCompanyProfileCatalog.AllowPaging = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        protected void Budget_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgCompanyProfileCatalog.ClientSettings.Scrolling.ScrollTop = "0";
        }

        #region Image
        //veracode fix for 2543
        ///// <summary>
        ///// ImageToBinary
        ///// </summary>
        ///// <param name="imagePath"></param>
        ///// <returns></returns>
        //public static byte[] ImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int)fileStream.Length);
        //        fileStream.Close();
        //        return buffer;
        //    }
        //}
        /// <summary>
        /// To Load Image
        /// </summary>
        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream myStream;
                Int32 fileLen;
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        if (ddlImg.Items.Count >= 0)
                        {
                            string FileName = fileUL.FileName;
                            fileLen = fileUL.PostedFile.ContentLength;
                            Byte[] Input = new Byte[fileLen];
                            myStream = fileUL.FileContent;
                            myStream.Read(Input, 0, fileLen);
                            //start of modification for image issue
                            if (hdnBrowserName.Value == "Chrome")
                            {
                                imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                            }
                            else
                            {
                                Session["Base64"] = Input;
                                imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                            }
                            //end of modification for image issue
                            Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                            //Commented for removing document name
                            //if (tbImgName.Text.Trim() != "")
                            //    FileName = tbImgName.Text.Trim();
                            int count = dicImg.Count(D => D.Key.Equals(FileName));
                            if (count > 0)
                            {
                                dicImg[FileName] = Input;
                            }
                            else
                            {
                                DeleteOtherImage();
                                dicImg.Add(FileName, Input);
                                string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FileName);
                                ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(encodedFileName, encodedFileName));
                                //start of modification for image issue
                                if (hdnBrowserName.Value == "Chrome")
                                {
                                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                                }
                                else
                                {
                                    Session["Base64"] = Input;
                                    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                }
                                //end of modification for image issue
                            }
                            tbImgName.Text = "";
                            radbtnCQDelete.Enabled = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// To Delete Other Image
        /// </summary>
        private void DeleteOtherImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                if (count > 0)
                {
                    dicImg.Remove(ddlImg.Text.Trim());
                    Session["DicImg"] = dicImg;
                    Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                    dicImgDelete.Add(ddlImg.Text.Trim(), "");
                    Session["DicImgDelete"] = dicImgDelete;
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbImgName.Text = "";
                    int i = 0;
                    foreach (var DicItem in dicImg)
                    {
                        ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                        i = i + 1;
                    }
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    ImgPopup.ImageUrl = null;
                }
            }
        }
        /// <summary>
        /// Dropdown for image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool imgFound = false;
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ObjRetImg = ObjImgService.GetFileWarehouseList("Company", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());
                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                {
                                    imgFound = true;
                                    byte[] picture = fwh.UWAFilePath;
                                    //start of modification for image issue
                                    if (hdnBrowserName.Value == "Chrome")
                                    {
                                        imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                    }
                                    else
                                    {
                                        Session["Base64"] = picture;
                                        imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    }
                                    //end of modification for image issue
                                }
                                tbImgName.Text = ddlImg.Text.Trim();
                                if (!imgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (count > 0)
                                    {
                                        //start of modification for image issue
                                        if (hdnBrowserName.Value == "Chrome")
                                        {
                                            imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        }
                                        else
                                        {
                                            Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                            Session["Base641"] = dicImg[ddlImg.Text.Trim()];
                                            imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        }
                                        //end of modification for image issue
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To delete image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                        int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            dicImg.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = dicImg;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                            {
                                dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            }
                            Session["DicImgDelete"] = dicImgDelete;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in dicImg)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Create dictioanry for image upload
        /// </summary>
        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                Session["DicImg"] = dicImg;
                Dictionary<string, string> dicImgDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = dicImgDelete;
            }
        }
        #endregion

        #region CQImage
        //Veracode fix for 2551
        ///// <summary>
        ///// ImageToBinary
        ///// </summary>
        ///// <param name="imagePath"></param>
        ///// <returns></returns>
        //public static byte[] CQImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int)fileStream.Length);
        //        fileStream.Close();
        //        return buffer;
        //    }
        //}
        /// <summary>
        /// To Load Image
        /// </summary>
        private void LoadCQImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream myStream;
                Int32 fileLen;
                if (fuCQ.PostedFile != null)
                {
                    if (fuCQ.PostedFile.ContentLength > 0)
                    {
                        if (ddlCQFileName.Items.Count >= 0)
                        {
                            string FileName = fuCQ.FileName;
                            fileLen = fuCQ.PostedFile.ContentLength;
                            Byte[] Input = new Byte[fileLen];
                            myStream = fuCQ.FileContent;
                            myStream.Read(Input, 0, fileLen);
                            //start of modification for image issue
                            if (hdnBrowserName.Value == "Chrome")
                            {
                                imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                            }
                            else
                            {
                                Session["CQBase64"] = Input;
                                imgCQ.ImageUrl = "../ImageHandler.ashx?CQBase64=CQBase64&cd=" + Guid.NewGuid();
                            }
                            //end of modification for image issue
                            Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CQDicImg"];
                            //Commented for removing document name
                            //if (tbImgName.Text.Trim() != "")
                            //    FileName = tbImgName.Text.Trim();
                            int count = dicImg.Count(D => D.Key.Equals(FileName));
                            if (count > 0)
                            {
                                dicImg[FileName] = Input;
                            }
                            else
                            {
                                //DeleteCQOtherImage();
                                dicImg.Add(FileName, Input);
                                // ddlImg.Items.Clear();
                                string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FileName);
                                ddlCQFileName.Items.Insert(ddlCQFileName.Items.Count, new ListItem(encodedFileName, encodedFileName));
                                //start of modification for image issue
                                if (hdnBrowserName.Value == "Chrome")
                                {
                                    imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                                }
                                else
                                {
                                    Session["CQBase64"] = Input;
                                    imgCQ.ImageUrl = "../ImageHandler.ashx?CQBase64=CQBase64&cd=" + Guid.NewGuid();
                                }
                                if (ddlCQFileName.Items.Count != 0)
                                {
                                    //ddlImg.SelectedIndex = ddlImg.Items.Count - 1;
                                    ddlCQFileName.SelectedValue = FileName;
                                }
                                //end of modification for image issue
                            }
                            tbCQimgName.Text = "";
                            radbtnCQDelete.Enabled = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// To Delete Other Image
        /// </summary>
        private void DeleteCQOtherImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                if (Session["CQDicImg"] == null)
                    Session["CQDicImg"] = new Dictionary<string, byte[]>();
                dicImg = (Dictionary<string, byte[]>)Session["CQDicImg"];
                int count = dicImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));
                if (count > 0)
                {
                    dicImg.Remove(ddlCQFileName.Text.Trim());
                    Session["CQDicImg"] = dicImg;
                    Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["CQDicImgDelete"];
                    dicImgDelete.Add(ddlCQFileName.Text.Trim(), "");
                    Session["CQDicImgDelete"] = dicImgDelete;
                    ddlCQFileName.Items.Clear();
                    ddlCQFileName.Text = "";
                    tbCQimgName.Text = "";
                    int i = 0;
                    foreach (var DicItem in dicImg)
                    {
                        ddlCQFileName.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                        i = i + 1;
                    }
                    imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    ImgPopup.ImageUrl = null;
                }
            }
        }
        /// <summary>
        /// <summary>
        /// Dropdown for image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCQFileName_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlCQFileName.Text.Trim() == "")
                        {
                            imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                        }
                        if (ddlCQFileName.SelectedItem != null)
                        {
                            bool imgFound = false;
                            imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ObjRetImg = ObjImgService.GetFileWarehouseList("CQCompany", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlCQFileName.Text.Trim());
                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                {
                                    imgFound = true;
                                    byte[] picture = fwh.UWAFilePath;
                                    //start of modification for image issue
                                    if (hdnBrowserName.Value == "Chrome")
                                    {
                                        imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                    }
                                    else
                                    {
                                        Session["CQBase64"] = picture;
                                        imgCQ.ImageUrl = "../ImageHandler.ashx?CQBase64=CQBase64&cd=" + Guid.NewGuid();
                                    }
                                    //end of modification for image issue
                                }
                                tbCQimgName.Text = ddlCQFileName.Text.Trim();
                                if (!imgFound)
                                {
                                    //Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CQDicCqImg"];
                                    Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                                    if (Session["CQDicImg"] == null)
                                        Session["CQDicImg"] = new Dictionary<string, byte[]>();
                                    dicImg = (Dictionary<string, byte[]>)Session["CQDicImg"];
                                    int count = dicImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));
                                    if (count > 0)
                                    {
                                        //start of modification for image issue
                                        if (hdnBrowserName.Value == "Chrome")
                                        {
                                            imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlCQFileName.Text.Trim()]);
                                        }
                                        else
                                        {

                                            Session["CQBase64"] = dicImg[ddlCQFileName.Text.Trim()];
                                            Session["CQBase641"] = dicImg[ddlCQFileName.Text.Trim()];
                                            imgCQ.ImageUrl = "../ImageHandler.ashx?CQBase64=CQBase64&cd=" + Guid.NewGuid();
                                        }
                                        //end of modification for image issue
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To delete image For Cq
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btncqdeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> dicCqImg = new Dictionary<string, byte[]>();
                        if (Session["CQDicImg"] != null)
                            //Session["CQQuoteDicImg"] = new Dictionary<string, byte[]>();
                            dicCqImg = (Dictionary<string, byte[]>)Session["CQDicImg"];
                        int count = dicCqImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));
                        if (count > 0)
                        {
                            dicCqImg.Remove(ddlCQFileName.Text.Trim());
                            Session["CQDicImg"] = dicCqImg;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["CQDicImgDelete"];
                            if (dicImgDelete.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim())) == 0)
                            {
                                dicImgDelete.Add(ddlCQFileName.Text.Trim(), "");
                            }
                            Session["CQDicImgDelete"] = dicImgDelete;
                            ddlCQFileName.Items.Clear();
                            ddlCQFileName.Text = "";
                            tbCQimgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in dicCqImg)
                            {
                                ddlCQFileName.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            if (ddlCQFileName.Items.Count > 0)
                            {
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CQDicImg"];
                                int Count = dicImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));
                                if (Count > 0)
                                {
                                    //start of modification for image issue
                                    if (hdnBrowserName.Value == "Chrome")
                                    {
                                        imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlCQFileName.Text.Trim()]);
                                    }
                                    else
                                    {
                                        Session["CQBase64"] = dicImg[ddlCQFileName.Text.Trim()];
                                        imgCQ.ImageUrl = "../ImageHandler.ashx?CQBase64=CQBase64&cd=" + Guid.NewGuid();
                                    }
                                    //end of modification for image issue
                                }
                            }
                            else
                            {
                                imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            }
                            ImgPopup.ImageUrl = null;
                            //imgCQ.ImageUrl = "../../../App_Themes/Default/images/noimage.jpg";
                            //imgCQPopup.ImageUrl = null;
                        }
                        else
                        {
                            dicCqImg.Remove(ddlImg.Text.Trim());
                            Session["CQDicImg"] = dicCqImg;
                            ddlCQFileName.Items.Clear();
                            ddlCQFileName.Text = "";
                            tbCQimgName.Text = "";
                            imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            imgCQPopup.ImageUrl = null;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicCQImgDelete"];
                            if (Session["DicCQImgDelete"] != null)
                                if (dicImgDelete.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim())) == 0)
                                {
                                    dicImgDelete.Add(ddlCQFileName.Text.Trim(), "");
                                }
                            Session["DicCQImgDelete"] = dicImgDelete;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Create dictioanry for image upload for CQ
        /// </summary>
        private void CreateCqDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> cqDicImg = new Dictionary<string, byte[]>();
                Session["CQDicImg"] = cqDicImg;
                Dictionary<string, string> cqDicImgDelete = new Dictionary<string, string>();
                Session["CQDicImgDelete"] = cqDicImgDelete;
            }
        }
        #endregion

        #region Events
        protected void radTaxes_Checked(object sender, EventArgs e)
        {
            PanelTaxes.Visible = true;
            pnlUSTaxesForm.Visible = false;
        }
        protected void radUSSegmentFees_Checked(object sender, EventArgs e)
        {
            pnlUSTaxesForm.Visible = true;
            PanelTaxes.Visible = false;
        }
        protected void tbCharterDepartment_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbCharterDepartment.Text))
            {
                CheckAlreadyDepartmentsExist();
            }
            else
            {
                hdnDepartmentID.Value = string.Empty;
                lbCharterDeptInfo.Text = string.Empty;
            }
        }
        protected void FlightCat_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbPax.Text))
            {
                CheckAlreadyFlightCategoryPaxExist();
            }
        }
        protected void FlightCat1_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbNoPax.Text))
            {
                CheckAlreadyFlightCategoryNoPaxExist();
            }
        }
        protected void tbCrewDutyRules_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbCQDefaultCrewDutyRules.Text))
            {
                CheckAlreadyCrewDutyRulesExist();
            }
        }
        protected void tbFedAccNum1_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbFedAccNum1.Text))
            {
                CheckAlreadyFederalTax1Exist();
            }
        }
        protected void tbFedAccNum2_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbFedAccNum2.Text))
            {
                CheckAlreadyFederalTax2Exist();
            }
        }
        protected void tbSegFeeAccNum1_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbSegFeeAccNum1.Text))
            {
                CheckAlreadySegAccountExist();
            }
        }
        protected void tbSegFeeAccNum2_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbSegFeeAccNum2.Text))
            {
                CheckAlreadySegAccount1Exist();
            }
        }
        protected void tbSegFeeAccNum3_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbSegFeeAccNum3.Text))
            {
                CheckAlreadySegAccount2Exist();
            }
        }
        protected void tbSegFeeAccNum4_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbSegFeeAccNum4.Text))
            {
                CheckAlreadySegAccount3Exist();
            }
        }
        /// <summary>
        /// PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CompanyProfileCatalog_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CompanyPageNavigated)
                        {
                            SelectItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Item Created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCompanyProfileCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, divCompanyProfileCatalogForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, divCompanyProfileCatalogForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CompanyProfileCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CompanyPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// For ItemComamnd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCompanyProfileCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                TabCompanyProfileCatalog.CausesValidation = true;
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Company, Convert.ToInt64(Session["HomeBaseID"].ToString().Trim()));
                                    Session["IsEditLockCompany"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Company);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Company);
                                        SelectItem();
                                        return;
                                    }
                                }
                                DisplayEditForm();
                                GridEnable(false, true, false);
                                EnableLinks();
                                divCustom.Visible = true;
                                divCustompilot.Visible = true;
                                EnableDisableAutoPaxCode(chkAutoPaxcode.Checked);
 	                            EnableDisableAutoCrewCode(chkAutoCrewcode.Checked);
                                RadAjaxManager1.FocusControl(tbBaseDescription.ClientID);
                                // tbBaseDescription.Focus();
                                Session["Conversion"] = "Edit";
                                Session["CompanyInsert"] = "Edit";
                                break;
                            case RadGrid.InitInsertCommandName:
                                TabCompanyProfileCatalog.SelectedIndex = 0;
                                TabCompanyProfileCatalog.CausesValidation = true;
                                RadPageView1.Selected = true;
                                e.Canceled = true;
                                dgCompanyProfileCatalog.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                EnableForm(true);
                                GridEnable(true, false, false);
                                RadAjaxManager1.FocusControl(tbMainBase.ClientID);
                                // tbMainBase.Focus();
                                EnableLinks();
                                divCustom.Visible = false;
                                divCustompilot.Visible = false;
                                 EnableDisableAutoPaxCode(chkAutoPaxcode.Checked);
                                EnableDisableAutoCrewCode(chkAutoCrewcode.Checked);
                                Session["Conversion"] = "Insert";
                                Session["CompanyInsert"] = "Insert";
                                break;
                            case "UpdateEdited":
                                dgCompanyProfileCatalog_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Enable Links on Add or Edit Mode
        /// </summary>
        private void EnableLinks()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UserPrincipal != null && UserPrincipal.Identity._isSysAdmin != false)
                        {
                            divTripPrivacy.Visible = true;
                        }
                        else
                        {
                            divTripPrivacy.Visible = false;
                        }
                        divCrewCurrency.Visible = true;
                        divQuoteInfo.Visible = true;
                        divInvoiceInfo.Visible = true;
                        divCustom.Visible = true;
                        divCustompilot.Visible = true;
                        lnkbtnCustomPilotLog.Enabled = true;
                        lnkbtncrew.Enabled = true;
                        if (UserPrincipal != null && UserPrincipal.Identity._isSysAdmin != false)
                        {
                            lnkCrewCurrency.Enabled = true;
                        }
                        lnkCustomFlightLog.Enabled = true;
                        divQuoteInfo.Visible = true;
                        divInvoiceInfo.Visible = true;
                        //lnkbtncrew.CssClass = "linktext_enabled";
                        //lnkbtnCustomPilotLog.CssClass = "linktext_enabled";
                        //lnkCrewCurrency.CssClass = "linktext_enabled";
                        //lnkCustomFlightLog.CssClass = "linktext_enabled";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// For insert command
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCompanyProfileCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        CheckCrewCharacter();
                        if (!IsCrewOName)
                        {
                            IsValidate = false;
                        }
                        CheckPaxCharacter();
                        if (!IsPaxOName)
                        {
                            IsValidate = false;
                        }
                        //string text = "HH:MM";
                        //string expression = "^[0-9]{0,2}(:([0-5]{0,1}[0-9]{0,1}))?$";
                        //if (ValidateText(text, expression))
                        //{
                        //    TabCompanyProfileCatalog.SelectedIndex = 5;
                        //    RadPageView4.Selected = true;
                        //    cvtbCorpReqStartTM.IsValid = false;
                        //    pnlbarcompanyInformation.Items[0].Expanded = true;
                        //    RadAjaxManager1.FocusControl(tbCorpReqStartTM.ClientID);
                        //    IsValidate = false;
                        //}
                        if (CheckAlreadyMainBaseExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvMainBase.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbMainBase.ClientID);
                            // tbMainBase.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyFedTaxAccountExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvFedTaxAccount.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbFedTaxAccount.ClientID);
                            //tbFedTaxAccount.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyExchangeRateExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvExchangeRateID.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbExchangeRateCode.ClientID);
                            IsValidate = false;
                        }
                        if (CheckAlreadySalesTaxAccountExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvSalesTaxAccount.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbSalesTaxAccount.ClientID);
                            //tbSalesTaxAccount.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyStateTaxAccountExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvStateTaxAccount.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbStateTaxAccount.ClientID);
                            // tbStateTaxAccount.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyExchangeRateExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvExchangeRateID.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbExchangeRateCode.ClientID);
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultBlockedSeatExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 3;
                            RadPageView4.Selected = true;
                            cvDefaultBlockedSeat.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultBlockedSeat.ClientID);
                            // tbDefaultBlockedSeat.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultChklistGroupExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView3.Selected = true;
                            cvDefaultChklistGroup.IsValid = false;
                            pnlbarPreflight.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultChklistGroup.ClientID);
                            // tbDefaultChklistGroup.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultCrewDutyRulesExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView3.Selected = true;
                            cvDefaultCrewDutyRules.IsValid = false;
                            pnlbarPreflight.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultCrewDutyRules.ClientID);
                            // tbDefaultCrewDutyRules.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultFlightCategoryExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView3.Selected = true;
                            cvDefaultFlightCategory.IsValid = false;
                            pnlbarPreflight.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultFlightCategory.ClientID);
                            //tbDefaultFlightCategory.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultTripsheetRWGroup())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView3.Selected = true;
                            cvDefaultTripsheetRWGroup.IsValid = false;
                            pnlbarPreflight.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultTripsheetRWGroup.ClientID);
                            //tbDefaultTripsheetRWGroup.Focus();
                            IsValidate = false;
                        }
                        if (tbSIFLLayoversHrs.Text.Trim() != "")
                        {
                            int value = Convert.ToInt32(tbSIFLLayoversHrs.Text.Trim());
                            if (value < 0 || value > 24)
                            {
                                TabCompanyProfileCatalog.SelectedIndex = 3;
                                RadPageView4.Selected = true;
                                cvSIFLLayoversHrs.IsValid = false;
                                pnlbarcompanyInformation.Items[0].Expanded = true;
                                RadAjaxManager1.FocusControl(tbSIFLLayoversHrs.ClientID);
                                IsValidate = false;
                            }
                        }
                        if (CheckAlreadyDepartmentsExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvDepartmentCode.IsValid = false;
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbCharterDepartment.ClientID);
                            IsValidate = false;
                        }
                        if (CheckAlreadyFlightCategoryPaxExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvtbPax.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbPax);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadyFlightCategoryNoPaxExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvtbNoPax.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbNoPax);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadyCrewDutyRulesExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvDefaultCrewDutyRules.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDefaultCrewDutyRules);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadyFederalTax1Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvFedAccNum1.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbFedAccNum1);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadyFederalTax2Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvFedAccNum2.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbFedAccNum2);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadySegAccountExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvSegFeeAccNum1.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum1);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadySegAccount1Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvSegFeeAccNum2.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum2);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadySegAccount2Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvSegFeeAccNum3.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum3);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadySegAccount3Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView5.Selected = true;
                            cvSegFeeAccNum4.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum4);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient CompanyService = new MasterCatalogServiceClient())
                            {
                                if (strErrUpload == string.Empty)
                                {
                                    string str = hdTSAClear.Value;
                                    var ReturnValue = CompanyService.AddCompanyMaster(GetItems());
                                    if (ReturnValue.ReturnFlag == true)
                                    {
                                        FlightPakMasterService.Company CompanyItems = new FlightPakMasterService.Company();
                                        AssignTenthMinConv(CompanyItems);
                                        #region Image Upload in New Mode
                                        var InsertedCompanyID = CompanyService.GetCompanyID();
                                        //Int64 NewCrewID = 0;
                                        if (InsertedCompanyID != null)
                                        {
                                            foreach (FlightPakMasterService.GetCompanyID FltId in InsertedCompanyID.EntityList)
                                            {
                                                hdnCompanyID.Value = FltId.HomebaseID.ToString();
                                            }
                                        }
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        foreach (var DicItem in dicImg)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "Company";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(hdnCompanyID.Value);
                                                Service.UWAWebpageName = "CompanyProfileCatalog.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                objService.AddFWHType(Service);
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in CQ New Mode
                                        //Int64 NewCrewID = 0;
                                        if (InsertedCompanyID != null)
                                        {
                                            foreach (FlightPakMasterService.GetCompanyID FltId in InsertedCompanyID.EntityList)
                                            {
                                                hdnCompanyID.Value = FltId.HomebaseID.ToString();
                                            }

                                            #region Insert/Update custom flightlog list.
                                            //updating custom flightlog list.
                                            //This will insert custom flightlog records to the tsflightlog table. 
                                            //same process happens when the customflightlog screen loads
                                            TSFlightLog oTSFlightLog = new TSFlightLog();  
                                            oTSFlightLog.HomebaseID = Convert.ToInt64(hdnCompanyID.Value);
                                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var ObjRetVal = ObjService.GetAllFlightLogList(oTSFlightLog);
                                            }
                                            #endregion Insert/Update custom flightlog list.

                                        }
                                        Dictionary<string, byte[]> cqdicImg = (Dictionary<string, byte[]>)Session["CQDicImg"];
                                        foreach (var DicItem in cqdicImg)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "CQCompany";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(hdnCompanyID.Value);
                                                Service.UWAWebpageName = "CompanyProfileCatalog.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                objService.AddFWHType(Service);
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in New Mode CQCompanyQuoteReport
                                        //Int64 NewCrewID = 0;
                                        if (InsertedCompanyID != null)
                                        {
                                            foreach (FlightPakMasterService.GetCompanyID FltId in InsertedCompanyID.EntityList)
                                            {
                                                hdnCompanyID.Value = FltId.HomebaseID.ToString();
                                            }
                                        }
                                        if (Session["CQQuoteDicImg"] != null)
                                        {
                                            Dictionary<string, byte[]> CqQuoteDicImg = (Dictionary<string, byte[]>)Session["CQQuoteDicImg"];
                                            foreach (var DicItem in CqQuoteDicImg)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                    Service.FileWarehouseID = 0;
                                                    Service.RecordType = "CqCompanyQuote";
                                                    Service.UWAFileName = DicItem.Key;
                                                    Service.RecordID = Convert.ToInt64(hdnCompanyID.Value);
                                                    Service.UWAWebpageName = "CompanyProfileCatalog.aspx";
                                                    Service.UWAFilePath = DicItem.Value;
                                                    Service.IsDeleted = false;
                                                    Service.FileWarehouseID = 0;
                                                    objService.AddFWHType(Service);
                                                }
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in New Mode CQCompanyInvoiceReport
                                        //Int64 NewCrewID = 0;
                                        if (InsertedCompanyID != null)
                                        {
                                            foreach (FlightPakMasterService.GetCompanyID FltId in InsertedCompanyID.EntityList)
                                            {
                                                hdnCompanyID.Value = FltId.HomebaseID.ToString();
                                            }
                                        }
                                        if (Session["CqInvoiceDicImg"] != null)
                                        {
                                            Dictionary<string, byte[]> CqInvoiceDicImg = (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"];
                                            foreach (var DicItem in CqInvoiceDicImg)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                    Service.FileWarehouseID = 0;
                                                    Service.RecordType = "CqCompanyInvoice";
                                                    Service.UWAFileName = DicItem.Key;
                                                    Service.RecordID = Convert.ToInt64(hdnCompanyID.Value);
                                                    Service.UWAWebpageName = "CompanyProfileCatalog.aspx";
                                                    Service.UWAFilePath = DicItem.Value;
                                                    Service.IsDeleted = false;
                                                    Service.FileWarehouseID = 0;
                                                    objService.AddFWHType(Service);
                                                }
                                            }
                                        }
                                        #endregion
                                        #region "QuoteFooter"
                                        List<FlightPakMasterService.CQMessage> FooterListQuote = new List<FlightPakMasterService.CQMessage>();
                                        if (Session["CQQuoteFooterList"] != null)
                                        {
                                            using (MasterCatalogServiceClient objQuoteTypeService = new MasterCatalogServiceClient())
                                            {
                                                objQuoteTypeService.DeleteCQMessage("Q", Convert.ToInt64(hdnCompanyID.Value));
                                            }
                                            FooterListQuote = (List<FlightPakMasterService.CQMessage>)Session["CQQuoteFooterList"];
                                            Int64 i = 0;
                                            foreach (var DicItem in FooterListQuote)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.CQMessage CQServiceType = new FlightPakMasterService.CQMessage();
                                                    i = i + 1;
                                                    //CQServiceType.CQMessageID = 0;
                                                    CQServiceType.HomebaseID = Convert.ToInt64(hdnCompanyID.Value);
                                                    //need to get data
                                                    CQServiceType.MessageTYPE = "Q";
                                                    CQServiceType.MessageCD = i;
                                                    CQServiceType.MessageDescription = DicItem.MessageDescription;
                                                    CQServiceType.MessageLine = DicItem.MessageDescription;
                                                    CQServiceType.IsDeleted = false;
                                                    using (MasterCatalogServiceClient objQuoteTypeService = new MasterCatalogServiceClient())
                                                    {
                                                        var objRetVal = objQuoteTypeService.AddCqMessage(CQServiceType);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        #region "QuoteInvoice"
                                        List<FlightPakMasterService.CQMessage> FooterListInvoice = new List<FlightPakMasterService.CQMessage>();
                                        if (Session["CQInvoiceFooterList"] != null)
                                        {
                                            //MR15042013
                                            using (MasterCatalogServiceClient objQuoteTypeService = new MasterCatalogServiceClient())
                                            {
                                                objQuoteTypeService.DeleteCQMessage("I", Convert.ToInt64(hdnCompanyID.Value));
                                            }

                                            FooterListInvoice = (List<FlightPakMasterService.CQMessage>)Session["CQInvoiceFooterList"];
                                            Int64 n = 0;
                                            foreach (var DicItem in FooterListInvoice)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.CQMessage CQServiceType = new FlightPakMasterService.CQMessage();
                                                    n = n + 1;
                                                    //CQServiceType.CQMessageID = 0;
                                                    CQServiceType.HomebaseID = Convert.ToInt64(hdnCompanyID.Value);
                                                    //need to get data
                                                    CQServiceType.MessageTYPE = "I";
                                                    CQServiceType.MessageCD = n;
                                                    CQServiceType.MessageDescription = DicItem.MessageDescription;
                                                    CQServiceType.MessageLine = DicItem.MessageDescription;
                                                    CQServiceType.IsDeleted = false;
                                                    using (MasterCatalogServiceClient objQuoteTypeService = new MasterCatalogServiceClient())
                                                    {
                                                        var objRetVal = objQuoteTypeService.AddCqMessage(CQServiceType);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        //AddUpdateTSA();
                                        //string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                        //    + " oManager.radalert('You may need to re login to see the changes.', 360, 50, 'Company Profile');";
                                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                        dgCompanyProfileCatalog.Rebind();
                                        GridEnable(true, true, true);
                                        Session.Remove("CrewCurrencyDefaults");
                                        DefaultSelection();
                                        ShowSuccessMessage();
                                        _selectLastModified = true;

                                        //MR15042013
                                        Session.Remove("CQQuoteFooterList");
                                        Session.Remove("CQQuoteFooter");
                                        Session.Remove("CQInvoiceFooterList");
                                        Session.Remove("CQInvoiceFooter");

                                        refreshUserPrincipal();

                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.Company);
                                    }
                                }
                                else
                                {
                                    RadWindowManager1.RadConfirm(strErrUpload, "confirmCallBackFn", 500, 30, null, "My Confirm");
                                }
                                //DefaultSelection();
                                hdnSaveFlag.Value = "";
                            }
                        }
                        TabCompanyProfileCatalog.CausesValidation = false;
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To select Item based on Id
        /// </summary>
        private void SelectedItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["HomeBaseID"] != null)
                {
                    if (Session["SearchItemPrimaryKeyValue"] == null)
                    {
                        string ID = Session["HomeBaseID"].ToString();
                        Session["SelectedCompanyID"] = Session["HomeBaseID"].ToString();
                        bool FoundItem = false;
                        int CurrentPageIndex = dgCompanyProfileCatalog.CurrentPageIndex; // the current page index of the Grid
                        //loop through the items of all item page from the current to the last
                        for (int i = CurrentPageIndex; i < dgCompanyProfileCatalog.PageCount; i++)
                        {
                            foreach (GridDataItem Item in dgCompanyProfileCatalog.Items)
                            {
                                if (Item["HomeBaseID"].Text.Trim() == ID.Trim())
                                {
                                    Item.Selected = true;
                                    GridDataItem item = dgCompanyProfileCatalog.SelectedItems[0] as GridDataItem;
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);
                                    FoundItem = true;
                                    break;
                                }
                            }
                            if (FoundItem)
                            {
                                //in order selection to be applied
                                break;
                            }
                            else
                            {
                                dgCompanyProfileCatalog.CurrentPageIndex = i + 1;
                                dgCompanyProfileCatalog.Rebind();
                            }
                        }
                    }
                }
                else
                {
                    DefaultSelection();
                }
            }
            if (dgCompanyProfileCatalog.Items.Count > 0 && dgCompanyProfileCatalog.SelectedItems.Count == 0)
            {
                DefaultSelection();
            }
        }
        /// <summary>
        /// For update 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCompanyProfileCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        CheckCrewCharacter();
                        if (!IsCrewOName)
                        {
                            IsValidate = false;
                        }
                        CheckPaxCharacter();
                        if (!IsPaxOName)
                        {
                            IsValidate = false;
                        }
                        //string text = "HH:MM";
                        //string expression = "^[0-9]{0,2}(:([0-5]{0,1}[0-9]{0,1}))?$";
                        //if (ValidateText(text, expression))
                        //{
                        //    TabCompanyProfileCatalog.SelectedIndex = 5;
                        //    RadPageView4.Selected = true;
                        //    cvtbCorpReqStartTM.IsValid = false;
                        //    pnlbarcompanyInformation.Items[0].Expanded = true;
                        //    RadAjaxManager1.FocusControl(tbCorpReqStartTM.ClientID);
                        //    IsValidate = false;
                        //}
                        if (CheckAlreadyFedTaxAccountExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvFedTaxAccount.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbFedTaxAccount.ClientID);
                            //tbFedTaxAccount.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadySalesTaxAccountExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvSalesTaxAccount.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbSalesTaxAccount.ClientID);
                            // tbSalesTaxAccount.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyStateTaxAccountExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvStateTaxAccount.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbStateTaxAccount.ClientID);
                            // tbStateTaxAccount.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultBlockedSeatExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 0;
                            RadPageView1.Selected = true;
                            cvDefaultBlockedSeat.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultBlockedSeat.ClientID);
                            //tbDefaultBlockedSeat.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultChklistGroupExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView3.Selected = true;
                            cvDefaultChklistGroup.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultChklistGroup.ClientID);
                            // tbDefaultChklistGroup.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultCrewDutyRulesExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView3.Selected = true;
                            cvDefaultCrewDutyRules.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultCrewDutyRules.ClientID);
                            // tbDefaultCrewDutyRules.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultFlightCategoryExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView3.Selected = true;
                            cvDefaultFlightCategory.IsValid = false;
                            pnlbarcompanyInformation.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultFlightCategory.ClientID);
                            // tbDefaultFlightCategory.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyDefaultTripsheetRWGroup())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 2;
                            RadPageView3.Selected = true;
                            cvDefaultTripsheetRWGroup.IsValid = false;
                            pnlbarPreflight.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbDefaultTripsheetRWGroup.ClientID);
                            // tbDefaultTripsheetRWGroup.Focus();
                            IsValidate = false;
                        }
                        if (tbSIFLLayoversHrs.Text.Trim() != "")
                        {
                            int value = Convert.ToInt32(tbSIFLLayoversHrs.Text.Trim());
                            if (value < 0 || value > 24)
                            {
                                TabCompanyProfileCatalog.SelectedIndex = 3;
                                RadPageView4.Selected = true;
                                cvSIFLLayoversHrs.IsValid = false;
                                pnlbarcompanyInformation.Items[0].Expanded = true;
                                RadAjaxManager1.FocusControl(tbSIFLLayoversHrs.ClientID);
                                IsValidate = false;
                            }
                        }
                        if (CheckAlreadyDepartmentsExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvDepartmentCode.IsValid = false;
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            RadAjaxManager1.FocusControl(tbCharterDepartment.ClientID);
                            IsValidate = false;
                        }
                        if (CheckAlreadyFlightCategoryPaxExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvtbPax.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbPax);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadyFlightCategoryNoPaxExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvtbNoPax.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbNoPax);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadyCrewDutyRulesExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvDefaultCrewDutyRules.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDefaultCrewDutyRules);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadyFederalTax1Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvFedAccNum1.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbFedAccNum1);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadyFederalTax2Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvFedAccNum2.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbFedAccNum2);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadySegAccountExist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvSegFeeAccNum1.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum1);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadySegAccount1Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvSegFeeAccNum2.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum2);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadySegAccount2Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvSegFeeAccNum3.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum3);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (CheckAlreadySegAccount3Exist())
                        {
                            TabCompanyProfileCatalog.SelectedIndex = 4;
                            RadPageView5.Selected = true;
                            cvSegFeeAccNum4.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum4);
                            pnlbarCharterQuote.Items[0].Expanded = true;
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient CompanyService = new MasterCatalogServiceClient())
                            {
                                //CopyFile(TSAClearedFile);
                                //CopyFile(TSANoFlyFile);
                                //CopyFile(TSASelecteeFile);

                                
                                var CompanyItem = GetItems();
                                #region UpdateUsersPeriod
                                if (Session["PasswordValidDays"] != null)
                                {
                                    decimal? ValidMonths = (decimal?)Session["PasswordValidDays"];
                                    if (CompanyItem.PasswordValidDays != ValidMonths)
                                    {
                                        // Membership.Provider.ResetPassword(userName, string.Empty);
                                        CompanyItem.PasswordValidDays = CompanyItem.PasswordValidDays.Value + 1;
                                    }
                                }
                                #endregion
                                var ReturnValue = CompanyService.UpdateCompanyMaster(CompanyItem);
                                if (ReturnValue.ReturnFlag == true)
                                {
                                    //Unlock should happen from Service Layer?
                                    //To Ramesh: Check whether the record is locked by same user...
                                    List<ConversionTable> ConversionTableList = new List<ConversionTable>();
                                    ConversionTable ConversionTable = new ConversionTable();
                                    //ConversionTable.HomebaseID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                    //ConversionTable.CustomerID = Convert.ToInt64(Session["CompanySelectedCustomerID"].ToString());
                                    //var CrewRosterServiceValues = CompanyService.GetConversionTableList().EntityList.Where(x => x.HomebaseID == Convert.ToInt64(Session["HomeBaseID"].ToString())).ToList();
                                    //if (CrewRosterServiceValues.Count > 0)
                                    //{
                                    FlightPakMasterService.Company CompanyItems = new FlightPakMasterService.Company();
                                    AssignTenthMinConv(CompanyItems);
                                    //}
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        //var returnValue = CommonService.UnLock(EntitySet.Database.Metro, Convert.ToInt64(Item.GetDataKeyValue("MetroID").ToString().Trim()));
                                        var returnValue = CommonService.UnLock(EntitySet.Database.Company, Convert.ToInt64(Session["HomeBaseID"].ToString().Trim()));
                                        Session["IsEditLockCompany"] = "False";
                                        // AddUpdateTSA();
                                        #region Image Upload in Edit Mode
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        foreach (var DicItem in dicImg)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "Company";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                Service.UWAWebpageName = "CompanyProfileCatalog.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                objService.AddFWHType(Service);
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in Delete
                                        using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                            foreach (var DicItem in dicImgDelete)
                                            {
                                                objFWHType.UWAFileName = DicItem.Key;
                                                objFWHType.RecordID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                objFWHType.RecordType = "Company";
                                                objFWHType.IsDeleted = true;
                                                objFWHType.IsDeleted = true;
                                                objFWHTypeService.DeleteFWHType(objFWHType);
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in CQ Edit Mode
                                        Dictionary<string, byte[]> cqdicImg = (Dictionary<string, byte[]>)Session["CQDicImg"];
                                        foreach (var DicItem in cqdicImg)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "CQCompany";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                Service.UWAWebpageName = "CompanyProfileCatalog.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                objService.AddFWHType(Service);
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in CQ Delete
                                        using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                            Dictionary<string, string> cqdicImgDelete = (Dictionary<string, string>)Session["CQDicImgDelete"];
                                            foreach (var DicItem in cqdicImgDelete)
                                            {
                                                objFWHType.UWAFileName = DicItem.Key;
                                                objFWHType.RecordID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                objFWHType.RecordType = "CQCompany";
                                                objFWHType.IsDeleted = true;
                                                objFWHType.IsDeleted = true;
                                                objFWHTypeService.DeleteFWHType(objFWHType);
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in Edit Mode for CqQuoteReport
                                        Dictionary<string, byte[]> CqQuoteDicImg = (Dictionary<string, byte[]>)Session["CQQuoteDicImg"];
                                        if (CqQuoteDicImg != null)
                                        {
                                            foreach (var DicItem in CqQuoteDicImg)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                    Service.FileWarehouseID = 0;
                                                    Service.RecordType = "CqCompanyQuote";
                                                    Service.UWAFileName = DicItem.Key;
                                                    Service.RecordID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                    Service.UWAWebpageName = "CompanyProfileCatalog.aspx";
                                                    Service.UWAFilePath = DicItem.Value;
                                                    Service.IsDeleted = false;
                                                    Service.FileWarehouseID = 0;
                                                    objService.AddFWHType(Service);
                                                }
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in Delete for CqQuoteReport
                                        using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                            Dictionary<string, string> CqQuoteDicImgDelete = (Dictionary<string, string>)Session["DicCQQuoteImgDelete"];
                                            if (CqQuoteDicImgDelete != null)
                                            {
                                                foreach (var DicItem in CqQuoteDicImgDelete)
                                                {
                                                    objFWHType.UWAFileName = DicItem.Key;
                                                    objFWHType.RecordID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                    objFWHType.RecordType = "CqCompanyQuote";
                                                    objFWHType.IsDeleted = true;
                                                    objFWHTypeService.DeleteFWHType(objFWHType);
                                                }
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in Edit Mode for CqInvoiceReport
                                        Dictionary<string, byte[]> CqInvoiceDicImg = (Dictionary<string, byte[]>)Session["CqInvoiceDicImg"];
                                        if (CqInvoiceDicImg != null)
                                        {
                                            foreach (var DicItem in CqInvoiceDicImg)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                    Service.FileWarehouseID = 0;
                                                    Service.RecordType = "CqCompanyInvoice";
                                                    Service.UWAFileName = DicItem.Key;
                                                    Service.RecordID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                    Service.UWAWebpageName = "CompanyProfileCatalog.aspx";
                                                    Service.UWAFilePath = DicItem.Value;
                                                    Service.IsDeleted = false;
                                                    Service.FileWarehouseID = 0;
                                                    objService.AddFWHType(Service);
                                                }
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in Delete for CqInvoiceReport
                                        using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                            Dictionary<string, string> CqInvoiceDicImgDelete = (Dictionary<string, string>)Session["CqInvoiceImgDelete"];
                                            if (CqInvoiceDicImgDelete != null)
                                            {
                                                foreach (var DicItem in CqInvoiceDicImgDelete)
                                                {
                                                    objFWHType.UWAFileName = DicItem.Key;
                                                    objFWHType.RecordID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                    objFWHType.RecordType = "CqCompanyInvoice";
                                                    objFWHType.IsDeleted = true;
                                                    objFWHTypeService.DeleteFWHType(objFWHType);
                                                }
                                            }
                                        }
                                        #endregion

                                        #region "QuoteFooter"
                                        List<FlightPakMasterService.CQMessage> FooterListQuote = new List<FlightPakMasterService.CQMessage>();
                                        if (Session["CQQuoteFooterList"] != null)
                                        {
                                            FooterListQuote = (List<FlightPakMasterService.CQMessage>)Session["CQQuoteFooterList"];
                                            using (MasterCatalogServiceClient objQuoteTypeService = new MasterCatalogServiceClient())
                                            {
                                                objQuoteTypeService.DeleteCQMessage("Q", Convert.ToInt64(Session["HomeBaseID"].ToString()));
                                            }
                                            foreach (var DicItem in FooterListQuote)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.CQMessage CQServiceType = new FlightPakMasterService.CQMessage();

                                                    //CQServiceType.CQMessageID = 0;
                                                    CQServiceType.HomebaseID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                    //need to get data
                                                    CQServiceType.MessageTYPE = "Q";
                                                    CQServiceType.MessageCD = DicItem.MessageCD;
                                                    CQServiceType.MessageDescription = DicItem.MessageDescription;
                                                    CQServiceType.MessageLine = DicItem.MessageDescription;
                                                    CQServiceType.IsDeleted = false;
                                                    using (MasterCatalogServiceClient objQuoteTypeService = new MasterCatalogServiceClient())
                                                    {
                                                        var objRetVal = objQuoteTypeService.AddCqMessage(CQServiceType);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        //MR15042013
                                        #region "Invoice Footer"
                                        List<FlightPakMasterService.CQMessage> InvoiceFooterList = new List<FlightPakMasterService.CQMessage>();
                                        if (Session["CQInvoiceFooterList"] != null)
                                        {
                                            InvoiceFooterList = (List<FlightPakMasterService.CQMessage>)Session["CQInvoiceFooterList"];
                                            using (MasterCatalogServiceClient objQuoteTypeService = new MasterCatalogServiceClient())
                                            {
                                                objQuoteTypeService.DeleteCQMessage("I", Convert.ToInt64(Session["HomeBaseID"].ToString()));
                                            }
                                            foreach (var DicItem in InvoiceFooterList)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.CQMessage CQServiceType = new FlightPakMasterService.CQMessage();

                                                    CQServiceType.HomebaseID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                                    CQServiceType.MessageTYPE = "I";
                                                    CQServiceType.MessageCD = DicItem.MessageCD;
                                                    CQServiceType.MessageDescription = DicItem.MessageDescription;
                                                    CQServiceType.MessageLine = DicItem.MessageDescription;
                                                    CQServiceType.IsDeleted = false;
                                                    using (MasterCatalogServiceClient objQuoteTypeService = new MasterCatalogServiceClient())
                                                    {
                                                        var objRetVal = objQuoteTypeService.AddCqMessage(CQServiceType);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        // Update Session when there is a change in UserPrincipal
                                        RefreshSession();
                                        GridEnable(true, true, true);
                                        //string AlertMsg = "radalert('You may need to re login to see the changes.', 360, 50, 'Company Profile');";
                                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        SelectedItem();
                                        hdnSaveFlag.Value = "";
                                        //lnkbtncrew.CssClass = "linktext_disabled";
                                        //lnkbtnCustomPilotLog.CssClass = "linktext_disabled";
                                        //lnkCrewCurrency.CssClass = "linktext_disabled";
                                        //lnkCustomFlightLog.CssClass = "linktext_disabled";
                                        lnkbtncrew.Enabled = false;
                                        lnkbtnCustomPilotLog.Enabled = false;
                                        if (UserPrincipal != null && UserPrincipal.Identity._isSysAdmin != false)
                                        {
                                            lnkCrewCurrency.Enabled = false;
                                        }
                                        lnkCustomFlightLog.Enabled = false;
                                        ShowSuccessMessage();
                                        EnableForm(false);
                                        _selectLastModified = true;

                                        //MR15042013
                                        Session.Remove("CQQuoteFooterList");
                                        Session.Remove("CQQuoteFooter");
                                        Session.Remove("CQInvoiceFooterList");
                                        Session.Remove("CQInvoiceFooter");

                                        Session.Remove("CqInvoiceImgDelete");
                                        Session.Remove("DicCQQuoteImgDelete");
                                        Session.Remove("CqInvoiceDicImg");
                                        Session.Remove("CQQuoteDicImg");
                                    }

                                    refreshUserPrincipal();

                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.Company);
                                }
                            }
                        }
                        TabCompanyProfileCatalog.CausesValidation = false;

                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// For Delete
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCompanyProfileCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            FlightPakMasterService.Company objCompanytype = new FlightPakMasterService.Company();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["HomeBaseID"] != null)
                        {
                            string Code = Session["HomeBaseID"].ToString();
                            string IsInActive = "";
                            foreach (GridDataItem Item in dgCompanyProfileCatalog.MasterTableView.Items)
                            {
                                if (Item["HomeBaseID"].Text.Trim() == Code.Trim())
                                {
                                    IsInActive = Item["IsInActive"].Text.Trim();
                                    break;
                                }
                            }
                            using (FlightPakMasterService.MasterCatalogServiceClient objCompanytypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                objCompanytype.HomebaseID = Convert.ToInt64(Code);
                                objCompanytype.IsDeleted = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Company, Convert.ToInt64(objCompanytype.HomebaseID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        EnableForm(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                                        return;
                                    }
                                }
                                objCompanytypeService.DeleteCompanyMaster(objCompanytype);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Company, Convert.ToInt64(objCompanytype.HomebaseID));
                    }
                }
            }
        }
        /// <summary>
        /// Selected Index change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCompanyProfileCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSaveFlag.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            // Session["dgTripPrivacy"] = null;
                            if (btnSaveChanges.Visible == false)
                            {
                                Session.Remove("CrewCurrencyDefaults");
                                ClearForm();
                                GridDataItem item = dgCompanyProfileCatalog.SelectedItems[0] as GridDataItem;
                                Session["HomeBaseID"] = item["HomeBaseID"].Text;
                                Session["CompanySelectedCustomerID"] = item["CustomerID"].Text;
                                Session["SelectedCompanyID"] = item["HomeBaseID"].Text;
                                hdnSaveFlag.Value = string.Empty;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                                DisableLinks();
                            }
                            else
                            {
                                SelectItem();
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                    }
                }
            }
        }
        /// <summary>
        /// Item data bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCompanyProfileCatalog_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Rad Ajax Seeting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCompanyProfileCatalog;
                        }
                        else if (e.Initiator.ID.IndexOf("btndgSave", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCompanyProfileCatalog;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        protected void SaveChanges1_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                     
                        if (IsEmptyCheck)
                        {                           
                            if (hdnSaveFlag.Value == "Update")
                            {
                                (dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Save Changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {                        
                        if (IsEmptyCheck)
                        {                           
                            if (hdnSaveFlag.Value == "Update")
                            {
                                (dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    
                    hdnSaveFlag.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// DeafultcrewDutyrules
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultCrewDutyRules_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAlreadyDefaultCrewDutyRulesExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Cancel Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            if (Session["HomeBaseID"] != null)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Company, Convert.ToInt64(Session["HomeBaseID"].ToString().Trim()));
                                }
                            }
                        }
                        TabCompanyProfileCatalog.CausesValidation = false;
                        SelectItem();
                        ReadOnlyForm();
                        DefaultSelection();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
            
            hdnSaveFlag.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// FirstCrewCharacter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbFirstCrewCharacter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbFirstCrewCharacter.Text))
                        {
                            IstbCrewFirst = false;
                            CheckCrewCharacter();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        protected void SIFLLayoversHrs_Textchanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbSIFLLayoversHrs.Text))
                        {
                            int value = Convert.ToInt32(tbSIFLLayoversHrs.Text.Trim());
                            if (value < 0 || value > 24)
                            {
                                TabCompanyProfileCatalog.SelectedIndex = 3;
                                RadPageView4.Selected = true;
                                cvSIFLLayoversHrs.IsValid = false;
                                pnlbarcompanyInformation.Items[0].Expanded = true;
                                RadAjaxManager1.FocusControl(tbSIFLLayoversHrs.ClientID);
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbFlightLogSrchDaysBack.ClientID);
                            }
                        }
                        else
                        {
                            RadAjaxManager1.FocusControl(tbFlightLogSrchDaysBack.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// MiddleCrewCharacter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbMiddleCrewCharacter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbMiddleCrewCharacter.Text))
                        {
                            IstbCrewSecond = false;
                            CheckCrewCharacter();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// LastCrewCharacter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbLastCrewCharacter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbLastCrewCharacter.Text))
                        {
                            IstbCrewThird = false;
                            CheckCrewCharacter();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// FirstPaxCharacter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbFirstPaxCharacter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbLastCrewCharacter.Text))
                        {
                            IstbPaxFirst = false;
                            CheckPaxCharacter();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// InitialPaxCharacter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbInitialPaxCharacter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbLastCrewCharacter.Text))
                        {
                            IstbPaxSecond = false;
                            CheckPaxCharacter();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// LastPaxCharacter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbLastPaxCharacter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbLastCrewCharacter.Text))
                        {
                            IstbPaxThird = false;
                            CheckPaxCharacter();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Check main base on text change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MainBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbMainBase.Text != null)
                        {
                            CheckAlreadyMainBaseExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }
        /// <summary>
        /// Binding Data for company
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCompanyProfileCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CompanyData = CompanyService.GetCompanyMasterListInfo();
                            if (CompanyData.ReturnFlag == true)
                            {
                                dgCompanyProfileCatalog.DataSource = CompanyData.EntityList;
                            }
                            Session["Homeval"] = (List<FlightPakMasterService.GetAllCompanyMaster>)CompanyData.EntityList.ToList();
                            if (Session["HomeBaseID"]==null && UserPrincipal != null && UserPrincipal.Identity._homeBaseId != null && UserPrincipal.Identity._homeBaseId != 0)
                            {
                                Session["HomeBaseID"] = Convert.ToString(UserPrincipal.Identity._homeBaseId);
                            }
                            if (Session["HomeBaseID"] != null && Session["HomeBaseID"].ToString() == Convert.ToString(UserPrincipal.Identity._homeBaseId))
                            {
                                List<FlightPakMasterService.GetAllCompanyMaster> updatedHomebaseList = new List<GetAllCompanyMaster>();
                                //Set Homebase record to First Index in List
                                List<FlightPakMasterService.GetAllCompanyMaster> homebaseList = (List<FlightPakMasterService.GetAllCompanyMaster>)Session["Homeval"];
                                if (homebaseList != null)
                                {
                                    long homebaseID=Convert.ToInt64(Session["HomeBaseID"]);
                                    var homebaseObj = homebaseList.Where(h => h.HomebaseID == homebaseID).FirstOrDefault();
                                    if (homebaseObj != null)
                                    {
                                        updatedHomebaseList.Add(homebaseObj);
                                        updatedHomebaseList.AddRange(homebaseList.Where(h => h.HomebaseID != homebaseID).ToList());
                                        Session["Homeval"] = updatedHomebaseList;
                                        dgCompanyProfileCatalog.DataSource = CompanyData.EntityList;
                                    }
                                }
                            }


                            if (chkSearchActiveOnly != null)
                            {
                                if (!IsPopUp)
                                    SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To filter Inactive records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #region "Search and Filter"

       

        protected void CRLogisticsCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>  {}, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }


        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllCompanyMaster> lstCompany = new List<FlightPakMasterService.GetAllCompanyMaster>();
                if (Session["Homeval"] != null)
                {
                    lstCompany = (List<FlightPakMasterService.GetAllCompanyMaster>)Session["Homeval"];
                }
                if (lstCompany.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == false) { lstCompany = lstCompany.Where(x => x.IsInActive == false).ToList<GetAllCompanyMaster>(); }
                    else
                        if (chkSearchActiveOnly.Checked == true) { lstCompany = lstCompany.ToList(); }
                    dgCompanyProfileCatalog.DataSource = lstCompany;
                    if (IsDataBind)
                    {
                        dgCompanyProfileCatalog.DataBind();
                        dgCompanyProfileCatalog.SelectedIndexes.Add(0);
                        GridDataItem item = dgCompanyProfileCatalog.SelectedItems[0] as GridDataItem;
                        Session["HomeBaseID"] = item.GetDataKeyValue("HomeBaseID").ToString().Trim();
                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    if (!Page.IsPostBack)
                    {
                        if (UserPrincipal != null && UserPrincipal.Identity._homeBaseId != null && UserPrincipal.Identity._homeBaseId != 0 && Session["SearchItemPrimaryKeyValue"] == null)
                        {
                            Session["HomeBaseID"] = Convert.ToString(UserPrincipal.Identity._homeBaseId);
                        }
                    }
                }
                return false;
            }
        }
        #endregion
        /// <summary>
        /// For Filter click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnclrFilters_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        pnlCompanyProfileCatalogForm.Visible = false;
                        foreach (GridColumn Column in dgCompanyProfileCatalog.MasterTableView.Columns)
                        {
                            //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                            //column.CurrentFilterValue = string.Empty;
                        }
                        dgCompanyProfileCatalog.MasterTableView.FilterExpression = string.Empty;
                        dgCompanyProfileCatalog.MasterTableView.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To check AutoFillEngineAirframehourscycle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkAutoFillEngineAirframeHoursandCycles_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAutoFillEngineAirframeHoursandCycles.Checked == true)
                radlstEngineAirframe.Enabled = true;
            else
                radlstEngineAirframe.Enabled = false;
        }
        /// <summary>
        /// o enable  Canpasspermit based on  checkchange
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkCanpasspermit_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkCanpasspermit.Checked == true)
                            tbCanpassPermit.Enabled = true;
                        else
                        {
                            tbCanpassPermit.Text = string.Empty;
                            tbCanpassPermit.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To enable and disable rmttbTaxiTimes based on chkautopopulate checkchange
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkAutoPopulateLegTimes_checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkAutoPopulateLegTimes.Checked == true)
                            rmttbTaxiTimes.Enabled = true;
                        else
                        {
                            rmttbTaxiTimes.Text = "00:00";
                            rmttbTaxiTimes.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// StartMonth selected index for month
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlStartMonth_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlStartMonth.SelectedIndex != 0)
                        {
                            if (ddlStartMonth.SelectedIndex == 1)
                            {
                                ddlEndMonth.SelectedIndex = 12;
                                ddlEndMonth.Items[12].Selected = true;
                            }
                            else
                            {
                                int n = ddlStartMonth.SelectedIndex;
                                ddlEndMonth.SelectedIndex = n - 1;
                                ddlEndMonth.Items[n - 1].Selected = true;
                            }
                        }
                        else
                        {
                            ddlEndMonth.SelectedIndex = 0;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To enable and disable radlstETERound based on radlsttimedisplay
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void radlstTimeDisplay_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (radlstTimeDisplay.SelectedValue == "2")
                        {
                            radlstETERound.Enabled = true;
                            tbDomestic.CssClass = "text40 displayNone";
                            revDomestic.Enabled = false;
                            tbDomesticTime.CssClass = "text40";
                            revDomesticTime.Enabled = true;
                            tbIntl.CssClass = "text40 displayNone";
                            revIntl.Enabled = false;
                            tbIntlTime.CssClass = "text40";
                            revIntlTime.Enabled = true;
                            if (string.IsNullOrEmpty(tbDomestic.Text))
                            tbDomestic.Text = "0.00";
                            if (string.IsNullOrEmpty(tbIntl.Text))
                            tbIntl.Text = "0.00";
                            tbDomesticTime.Text = TimeSpan.FromHours(Convert.ToDouble(tbDomestic.Text)).ToString("h\\:mm");
                            tbIntlTime.Text = TimeSpan.FromHours(Convert.ToDouble(tbIntl.Text)).ToString("h\\:mm");
                        }
                        else
                        {
                            radlstETERound.Enabled = false;
                            tbDomestic.CssClass = "text40";
                            revDomestic.Enabled = true;
                            tbDomesticTime.CssClass = "text40 displayNone";
                            revDomesticTime.Enabled = false;
                            tbIntl.CssClass = "text40";
                            revIntl.Enabled = true;
                            tbIntlTime.CssClass = "text40 displayNone";
                            revIntlTime.Enabled = false;
                            if (string.IsNullOrEmpty(tbDomesticTime.Text))
                                tbDomesticTime.Text = "00:00";
                            if(string.IsNullOrEmpty(tbIntlTime.Text))
                                tbIntlTime.Text = "00:00";
                            tbDomestic.Text = ConvertTimeToDecimal(tbDomesticTime.Text);
                            tbIntl.Text = ConvertTimeToDecimal(tbIntlTime.Text);
                            
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FedTaxAccount_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbFedTaxAccount.Text))
                        {
                            CheckAlreadyFedTaxAccountExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExchangeRateCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbExchangeRateCode.Text))
                        {
                            CheckAlreadyExchangeRateExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Validate TripSheetRWGroup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultTripsheetRWGroup_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultTripsheetRWGroup.Text))
                        {
                            CheckAlreadyDefaultTripsheetRWGroup();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SalesTaxAccount_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbSalesTaxAccount.Text))
                        {
                            CheckAlreadySalesTaxAccountExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Validate Block Seat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultBlockedSeat_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultBlockedSeat.Text))
                        {
                            CheckAlreadyDefaultBlockedSeatExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Validate CheckListGroup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultChklistGroup_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultChklistGroup.Text))
                        {
                            CheckAlreadyDefaultChklistGroupExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To ValidateCrew Duty Rules
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void DefaultCrewDutyRules_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultCrewDutyRules.Text))
                        {
                            CheckAlreadyDefaultCrewDutyRulesExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Validate Flight Category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultFlightCategory_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultFlightCategory.Text))
                        {
                            CheckAlreadyDefaultFlightCategoryExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// APSIS 
        /// </summary>
        protected void btnSubmittoUWA_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    Int64 HomeBaseID = 0;
                    string Password = string.Empty;
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCompanytypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if ((!string.IsNullOrEmpty(tbSenderID.Text)) && (!string.IsNullOrEmpty(tbCBPUserPassword.Text)))
                            {
                                Password = tbCBPUserPassword.Text;
                                var ReturnValue = objCompanytypeService.CompanyEAPISSubmit(tbSenderID.Text, tbCBPUserPassword.Text);
                                if (Session["HomeBaseID"] != null)
                                    HomeBaseID = Convert.ToInt64(Session["HomeBaseID"].ToString().Trim());
                                if (ReturnValue != null)
                                {
                                    if (ReturnValue.ReturnFlag == true)
                                    {
                                        if (tbSenderID.Text != null && tbCBPUserPassword.Text != null && tbDomain.Text != null && tbURLAddress.Text != null && HomeBaseID != null)
                                            objCompanytypeService.UpdateAPISInterface(HomeBaseID, tbSenderID.Text, tbCBPUserPassword.Text, tbDomain.Text, tbURLAddress.Text, chkEnableAPISInterface.Checked, chkAlertToresubmitTrips.Checked);
                                        string msgToDisplay = System.Web.HttpUtility.HtmlEncode( "Update successful");
                                        RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Company Profile!", "");
                                    }
                                    else
                                    {
                                        string msgToDisplay = System.Web.HttpUtility.HtmlEncode( "Unable to update data");
                                        RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Company Profile!", "");
                                    }
                                }
                                else
                                {
                                    string msgToDisplay = System.Web.HttpUtility.HtmlEncode( "Unable to update data");
                                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Company Profile!", "");
                                }
                            }
                            //string encodedConstvalue = Microsoft.Security.Application.Encoder.HtmlEncode(constvalue);
                            //string encodedPassword = Microsoft.Security.Application.Encoder.HtmlEncode(Password);
                            //tbCBPUserPassword.Attributes.Add(encodedConstvalue, encodedPassword);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To call during reoreder of crewname
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void radlstCrewNameElement_Reordered(object sender, RadListBoxEventArgs e)
        {
            CheckCrewCharacter();
        }
        /// <summary>
        /// to call the function during reordering of pax elements
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void radlstPaxNameElements_Reordered(object sender, RadListBoxEventArgs e)
        {
            CheckPaxCharacter();
        }


        protected void chkAutoDateAdjustment_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkAutoDateAdjustment.Checked == true)
                        {
                            chkWarningMessage.Checked = true;
                            chkWarningMessage.Enabled = true;

                        }
                        else
                        {
                            chkWarningMessage.Checked = false;
                            chkWarningMessage.Enabled = false;

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// o enable  CanAutoRun based on  checkchange
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkAutoRON_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkAutoRON.Checked == true)
                        {
                            chkAutoDateAdjustment.Enabled = true;
                            tbMinRONHrs.Enabled = true;
                        }
                        else
                        {
                            chkAutoDateAdjustment.Checked = false;  // 2483
                            chkWarningMessage.Checked = false; // 2483

                            chkAutoDateAdjustment.Enabled = false;
                            chkWarningMessage.Enabled = false;
                            tbMinRONHrs.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// To select default grid
        /// </summary>
        private void DefaultSelectGrid()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCompanyProfileCatalog.Rebind();
                        dgCompanyProfileCatalog.SelectedIndexes.Add(0);
                        if (dgCompanyProfileCatalog.MasterTableView.Items.Count > 0)
                        {
                            Session["HomeBaseID"] = dgCompanyProfileCatalog.Items[0].GetDataKeyValue("HomeBaseID").ToString();
                            ReadOnlyForm();
                            GridEnable(true, true, true);
                            DisableLinks();
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Disable Links on View Mode
        /// </summary>
        /// 
        private void DisableLinks()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //lnkbtncrew.CssClass = "linktext_disabled";
                        //lnkbtnCustomPilotLog.CssClass = "linktext_disabled";
                        //lnkCrewCurrency.CssClass = "linktext_disabled";
                        //lnkCustomFlightLog.CssClass = "linktext_disabled";
                        lnkbtncrew.Enabled = false;
                        lnkbtnCustomPilotLog.Enabled = false;
                        if (UserPrincipal != null && UserPrincipal.Identity._isSysAdmin != false)
                        {
                            lnkCrewCurrency.Enabled = false;
                        }
                        lnkCustomFlightLog.Enabled = false;
                        divCrewCurrency.Visible = true;
                        divQuoteInfo.Visible = true;
                        divInvoiceInfo.Visible = true;
                        if (UserPrincipal != null && UserPrincipal.Identity._isSysAdmin != false)
                        {
                            divTripPrivacy.Visible = true;
                        }
                        else
                        {
                            divTripPrivacy.Visible = false;
                        }
                        divCustom.Visible = true;
                        divCustompilot.Visible = true;
                        hdnSaveFlag.Value = string.Empty;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Select item based on companyid
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedCompanyID"] != null)
                        {
                            string ID = Session["SelectedCompanyID"].ToString();
                            foreach (GridDataItem Item in dgCompanyProfileCatalog.MasterTableView.Items)
                            {
                                if (Item["HomeBaseID"].Text.Trim() == ID.Trim())
                                {
                                    Item.Selected = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            EnableForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To display Controls on Insert Mode
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ClearForm();
                        //tbMainBase.ReadOnly = false;
                        //tbMainBase.BackColor = System.Drawing.Color.White;
                        //lbMainBase1.ReadOnly = true;
                        //lbMainBase1.BackColor = System.Drawing.Color.White;
                        pnlCompanyProfileCatalogForm.Visible = true;
                        hdnSaveFlag.Value = "Save";
                        dgCompanyProfileCatalog.Rebind();
                        tbMainBase.Text = string.Empty;
                        hdnHomeBaseID.Value = string.Empty;
                        tbBaseDescription.Text = string.Empty;
                        tbCompanyName.Text = string.Empty;
                        tbPhone.Text = string.Empty;
                        tbBaseFax.Text = string.Empty;
                        tbCanpassPermit.Text = string.Empty;
                        tbAccountFormat.Text = string.Empty;
                        tbCorpAcctFormat.Text = string.Empty;
                        tbFedTaxAccount.Text = string.Empty;
                        tbStateTaxAccount.Text = string.Empty;
                        tbSalesTaxAccount.Text = string.Empty;
                        tbCurrency.Text = "$";
                        //  tbGridSrch.Text = string.Empty;
                        tbgeneralnotes.Text = string.Empty;
                        //chkNoteDisplay.Checked = false; //*2483
                        chkAutoDispatchNumber.Checked = false;
                        chkInactive.Checked = false;
                        chkConflictChecking.Checked = false;
                        //chkTextAutotab.Checked = false; //*2483
                        chkCanpasspermit.Checked = false;
                        ddlDefaultDateFormat.SelectedIndex = 0;
                        //APIS
                        chkEnableAPISInterface.Checked = false;
                        chkAlertToresubmitTrips.Checked = false;
                        tbURLAddress.Text = string.Empty;
                        tbDomain.Text = string.Empty;
                        //fuXMLFilesLocation.FileName;
                        //Blackberry
                        //chkEnableBlackBerrysupport.Checked = false;
                        //chkSendEmailtoMaintCrew.Checked = false;
                        //chkSMTPPortisBlocked.Checked = false;
                        //tbServerName.Text = string.Empty;
                        //tbPort.Text = string.Empty;
                        //tbUserName.Text = string.Empty;
                        //tbPassword.Text = string.Empty;
                        //tbEmailId.Text = string.Empty;
                        //chkApplySettingtoallMainbases.Checked = false;
                        //chkUseSSL.Checked = false;
                        //radlstBlackBerry.SelectedIndex = 0;
                        //preflight
                        chkDeactiveHomeBaseFilter.Checked = false;
                        chkAutoCalcLegTimes.Checked = false;
                        chkUseChecklistWarnings.Checked = false;
                        chkChklistItemsToNewCrewMembers.Checked = false;
                        chkDoNotUpdateFuelBrand.Checked = false;
                        chkAssignAuthDuringRequestorSelection.Checked = false;
                        chkIncludeNonLoggedTripsheetsinCrewHistory.Checked = false;
                        //chkAutoValidateTripManager.Checked = false;
                        chkAutoCreateCrewAvailabilityInfo.Checked = false;
                        chkShowRequestorsOnly.Checked = false;
                        chkAutoPopulateDispatcher.Checked = false;
                        chkAutoRevisionNumber.Checked = false;
                        chkHighlightAircraftHavingNotes.Checked = false;
                        chkHighlightCrewtHavingNotes.Checked = false;
                        chkHighlightPassengerHavingNotes.Checked = false;
                        chkOpenHistoryOnEditingTrip.Checked = false;
                        chkDefaultTripDepartureDate.Checked = true;
                        chkCrewQualificationLegFAR.Checked = false;
                        chk24hrTripOverlapforCrew.Checked = false;                        
                        chkPassengerOverlap.Checked = false;
                        chkTailInspectionDue.Checked = false;
                        //tbFltCallTimer.Text = string.Empty;
                        tbDomestic.Text = string.Empty;
                        tbDomesticTime.Text = string.Empty;
                        tbIntl.Text = string.Empty;
                        tbIntlTime.Text = string.Empty;
                        tbDefaultCrewDutyRules.Text = string.Empty;
                        tbTripManagerSrchDaysBack.Text = "0";
                        tbTripManagerDateWarning.Text = string.Empty;
                        tbDefaultFlightCategory.Text = string.Empty;
                        tbDefaultChklistGroup.Text = string.Empty;
                        tbTripManagerStatus.Text = "W";
                        tbDefaultTripsheetRWGroup.Text = string.Empty;
                        tbGADeskId.Text = string.Empty;
                        //Commented as this is not valid anymore in new application
                        //tbTravelSenseLocation.Text = string.Empty;
                        radlstTimeDisplay.SelectedIndex = 0;
                        radlstBoeingWinds.SelectedIndex = 2;
                        radlstDefaultFAR.SelectedIndex = 0;
                        radlstETERound.SelectedIndex = 0;
                        radlstLogoPosition.SelectedIndex = 0;
                        tbOutboundInst1.Text = string.Empty;
                        tbOutboundInst2.Text = string.Empty;
                        tbOutboundInst3.Text = string.Empty;
                        tbOutboundInst4.Text = string.Empty;
                        tbOutboundInst5.Text = string.Empty;
                        tbOutboundInst6.Text = string.Empty;
                        tbOutboundInst7.Text = string.Empty;
                        //PostFlight
                        radlstLogFixedWing.SelectedIndex = 1;
                        radlstLogRotaryWing.SelectedIndex = 0;
                        radlstPostflightCrewDuty.SelectedIndex = 0;
                        radlstPostflightAircraft.SelectedIndex = 1;
                        radlstFuelPurchaseUnits.SelectedIndex = 0;
                        radlstFuelBurn.SelectedIndex = 0;
                        radlstEngineAirframe.SelectedIndex = 0;
                        radOther.Checked = false;
                        radFlightPak.Checked = false;
                        radStandard.Checked = false;
                        ddlEndMonth.SelectedIndex = 0;
                        ddlStartMonth.SelectedIndex = 0;
                        chkLogBlockedSeats.Checked = false;
                        chkUseLogsheetWarnings.Checked = false;
                        chkPromptTocalculateSIFL.Checked = false;
                        chkAutoFillEngineAirframeHoursandCycles.Checked = false;
                        chkIsPODepartPercentage.Checked = false;
                        chkShowScheduleDateValidation.Checked = false;
                        chkRemoveEndDutyMarker.Checked = false;
                        chkAutoPopulateLegTimes.Checked = false;
                        rmttbTaxiTimes.Text = "00:00";
                        tbFlightLogSrchDaysBack.Text = string.Empty;
                        tbSIFLLayoversHrs.Text = string.Empty;
                        tbDefaultBlockedSeat.Text = string.Empty;
                        tbShort3.Text = string.Empty;
                        tbShort4.Text = string.Empty;
                        tbLong3.Text = string.Empty;
                        tbLong4.Text = string.Empty;
                        tbDec1.Text = string.Empty;
                        tbDec2.Text = string.Empty;
                        tbShort1.Text = string.Empty;
                        tbShort2.Text = string.Empty;
                        tbLong1.Text = string.Empty;
                        tbLong2.Text = string.Empty;
                        radFlightPak.Checked = true;
                       
                        //DataBase
                        chkEnableTSA.Checked = false;
                        chkEnableTSAPAX.Checked = false;                       
                        tbAutoClosureTime.Text = "00.00";

                        M3.Checked = true;
                        //0pax
                        tbFirstPaxCharacter.Text = "1";
                        tbFirstCrewCharacter.Text = "1";
                        tbLastCrewCharacter.Text = "1";
                        tbLastPaxCharacter.Text = "1";
                        tbMiddleCrewCharacter.Text = "1";
                        tbInitialPaxCharacter.Text = "1";
                        EnableForm(true);
                        fileUL.Enabled = true;
                        Session.Remove("ConversionDefaults");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To display Controls on Edit Mode
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove("CQQuoteDicImg");
                        Session.Remove("CqQuoteReportBase64");
                        Session.Remove("DicCQQuoteImgDelete");
                        Session.Remove("CqInvoiceImgDelete");
                        Session.Remove("CqInvoiceDicImg");
                        Session.Remove("CqInvoiceReportBase64");
                        Session.Remove("ConversionDefaults");
                        Session.Remove("CqCompanyStringInvoiceReport");
                        Session.Remove("CqCompanyCheckInvoiceReport");
                        Session.Remove("CqCompanyRadInvoiceReport");
                        Session.Remove("CqCompanyStringQuoteReport");
                        Session.Remove("CqCompanyCheckQuoteReport");
                        Session.Remove("CqCompanyRadQuoteReport");
                        Session.Remove("CQQuoteFooter");
                        Session.Remove("CQInvoiceFooter");
                        LoadControlData();
                        pnlCompanyProfileCatalogForm.Visible = true;
                        hdnSaveFlag.Value = "Update";
                        hdnRedirect.Value = "";
                        dgCompanyProfileCatalog.Rebind();
                        string ID = Session["HomeBaseID"].ToString();
                        foreach (GridDataItem Item in dgCompanyProfileCatalog.MasterTableView.Items)
                        {
                            if (Item["HomeBaseID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        EnableForm(true);
                        fileUL.Enabled = true;
                        tbMainBase.Enabled = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Disable controls
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LoadControlData();
                        EnableForm(false);
                        fileUL.Enabled = false;
                        //GridEnable(true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        private void GetDepartmentCd()
        {
            if (!string.IsNullOrEmpty(hdnDepartmentID.Value))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var DepartmentValue = DepartmentService.GetAllDeptAuthorizationList().EntityList.Where(x => x.DepartmentID.ToString().Trim().Equals(hdnDepartmentID.Value.ToString().Trim()));
                        if (DepartmentValue != null && DepartmentValue.Count() > 0)
                        {
                            foreach (FlightPakMasterService.GetAllDeptAuth cm in DepartmentValue)
                            {
                                tbCharterDepartment.Text = cm.DepartmentCD.ToString();
                                lbCharterDeptInfo.Text = System.Web.HttpUtility.HtmlEncode(cm.DepartmentName.ToString());
                            }
                        }
                    }
                }
            }
        }
        private bool CheckAlreadyDepartmentsExist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbCharterDepartment.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var DepartmentValue = DepartmentService.GetAllDeptAuthorizationList().EntityList.Where(x => x.DepartmentCD.Trim().ToUpper().Equals(tbCharterDepartment.Text.ToString().ToUpper().Trim()));
                        if (DepartmentValue.Count() == 0 || DepartmentValue == null)
                        {
                            cvDepartmentCode.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCharterDepartment);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.GetAllDeptAuth cm in DepartmentValue)
                            {
                                hdnDepartmentID.Value = cm.DepartmentID.ToString();
                                tbCharterDepartment.Text = cm.DepartmentCD;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        private bool CheckAlreadyFlightCategoryPaxExist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbPax.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient FltCatService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var FltCatValue = FltCatService.GetFlightCategoryList().EntityList.Where(x => x.FlightCatagoryCD.Trim().ToUpper().Equals(tbPax.Text.ToString().ToUpper().Trim()));
                        if (FltCatValue.Count() == 0 || FltCatValue == null)
                        {
                            cvtbPax.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbPax);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.FlightCatagory cm in FltCatValue)
                            {
                                hdnPax.Value = cm.FlightCategoryID.ToString();
                                lbPaxInfo.Text = System.Web.HttpUtility.HtmlEncode(cm.FlightCatagoryDescription);
                                tbPax.Text = cm.FlightCatagoryCD;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        private bool CheckAlreadyFlightCategoryNoPaxExist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbNoPax.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient FltCatService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var FltCatValue = FltCatService.GetFlightCategoryList().EntityList.Where(x => x.FlightCatagoryCD.Trim().ToUpper().Equals(tbNoPax.Text.ToString().ToUpper().Trim()));
                        if (FltCatValue.Count() == 0 || FltCatValue == null)
                        {
                            cvtbNoPax.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbNoPax);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.FlightCatagory cm in FltCatValue)
                            {
                                hdnNoPax.Value = cm.FlightCategoryID.ToString();
                                lbNoPaxInfo.Text = System.Web.HttpUtility.HtmlEncode( cm.FlightCatagoryDescription);
                                tbNoPax.Text = cm.FlightCatagoryCD;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        private bool CheckAlreadyCrewDutyRulesExist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbCQDefaultCrewDutyRules.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient CrewRulesService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CrewDutyRulesValue = CrewRulesService.GetCrewDutyRuleList().EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper().Equals(tbCQDefaultCrewDutyRules.Text.ToString().ToUpper().Trim()));
                        if (CrewDutyRulesValue.Count() == 0 || CrewDutyRulesValue == null)
                        {
                            cvCqefaultCrewDutyRules.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDefaultCrewDutyRules);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.CrewDutyRules cm in CrewDutyRulesValue)
                            {
                                hdnCrewDutyRules.Value = cm.CrewDutyRulesID.ToString();
                                tbCQDefaultCrewDutyRules.Text = cm.CrewDutyRuleCD;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        private bool CheckAlreadyFederalTax1Exist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbFedAccNum1.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient AccountsService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var AccountsValue = AccountsService.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbFedAccNum1.Text.ToString().ToUpper().Trim()));
                        if (AccountsValue.Count() == 0 || AccountsValue == null)
                        {
                            cvFedAccNum1.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbFedAccNum1);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.Account cm in AccountsValue)
                            {
                                hdnFedAccNum1.Value = cm.AccountID.ToString();
                                tbFedAccNum1.Text = cm.AccountNum;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        private bool CheckAlreadyFederalTax2Exist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbFedAccNum2.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient AccountsService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var AccountsValue = AccountsService.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbFedAccNum2.Text.ToString().ToUpper().Trim()));
                        if (AccountsValue.Count() == 0 || AccountsValue == null)
                        {
                            cvFedAccNum2.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbFedAccNum2);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.Account cm in AccountsValue)
                            {
                                hdnFedAccNum2.Value = cm.AccountID.ToString();
                                tbFedAccNum2.Text = cm.AccountNum;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        private bool CheckAlreadySegAccountExist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbSegFeeAccNum1.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient AccountsService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var AccountsValue = AccountsService.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbSegFeeAccNum1.Text.ToString().ToUpper().Trim()));
                        if (AccountsValue.Count() == 0 || AccountsValue == null)
                        {
                            cvSegFeeAccNum1.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum1);
                            //tbSegFeeAccNum1.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.Account cm in AccountsValue)
                            {
                                hdnSegFeeAccNum1.Value = cm.AccountID.ToString();
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        private bool CheckAlreadySegAccount1Exist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbSegFeeAccNum2.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient AccountsService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var AccountsValue = AccountsService.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbSegFeeAccNum2.Text.ToString().ToUpper().Trim()));
                        if (AccountsValue.Count() == 0 || AccountsValue == null)
                        {
                            cvSegFeeAccNum2.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum2);
                            // tbSegFeeAccNum2.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.Account cm in AccountsValue)
                            {
                                hdnSegFeeAccNum2.Value = cm.AccountID.ToString();
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        private bool CheckAlreadySegAccount2Exist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbSegFeeAccNum3.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient AccountsService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var AccountsValue = AccountsService.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbSegFeeAccNum3.Text.ToString().ToUpper().Trim()));
                        if (AccountsValue.Count() == 0 || AccountsValue == null)
                        {
                            cvSegFeeAccNum3.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum3);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.Account cm in AccountsValue)
                            {
                                hdnSegFeeAccNum3.Value = cm.AccountID.ToString();
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        private bool CheckAlreadySegAccount3Exist()
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbSegFeeAccNum4.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient AccountsService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var AccountsValue = AccountsService.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbSegFeeAccNum4.Text.ToString().ToUpper().Trim()));
                        if (AccountsValue.Count() == 0 || AccountsValue == null)
                        {
                            cvSegFeeAccNum4.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbSegFeeAccNum4);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.Account cm in AccountsValue)
                            {
                                hdnSegFeeAccNum4.Value = cm.AccountID.ToString();
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadySalesTaxAccountExist()
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbSalesTaxAccount.Text != string.Empty) && (tbSalesTaxAccount.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbSalesTaxAccount.Text.ToString().ToUpper().Trim()));
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvSalesTaxAccount.IsValid = false;
                            RadAjaxManager1.FocusControl(tbSalesTaxAccount.ClientID);
                            //tbSalesTaxAccount.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            if (chkCanpasspermit.Checked == true)
                            {
                                RadAjaxManager1.FocusControl(tbCanpassPermit.ClientID);
                                // tbCanpassPermit.Focus();
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbCurrency.ClientID);
                                // tbCurrency.Focus();
                            }
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyExchangeRateExist()
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbExchangeRateCode.Text != string.Empty) && (tbExchangeRateCode.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetExchangeRate().EntityList.Where(x => x.ExchRateCD.Trim().ToUpper() == (tbExchangeRateCode.Text.Trim().ToUpper())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvExchangeRateID.IsValid = false;
                            returnValue = true;
                        }
                        else
                        {
                            hdnExchangeRateID.Value = RetValue[0].ExchangeRateID.ToString();
                            tbExchangeRateCode.Text = RetValue[0].ExchRateCD;
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        /// <summary>
        /// To Validate Account Number
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyStateTaxAccountExist()
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbStateTaxAccount.Text != string.Empty) && (tbStateTaxAccount.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbStateTaxAccount.Text.ToString().ToUpper().Trim()));
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvStateTaxAccount.IsValid = false;
                            RadAjaxManager1.FocusControl(tbStateTaxAccount.ClientID);
                            // tbStateTaxAccount.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            RadAjaxManager1.FocusControl(tbSalesTaxAccount.ClientID);
                            //tbSalesTaxAccount.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        /// <summary>
        /// To Validate Blocked Seat
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyDefaultBlockedSeatExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultBlockedSeat.Text != string.Empty) && (tbDefaultBlockedSeat.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeCD.Trim().ToUpper().Equals(tbDefaultBlockedSeat.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvDefaultBlockedSeat.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultBlockedSeat.ClientID);
                            // tbDefaultBlockedSeat.Focus();
                            return true;
                        }
                        else
                        {
                            hdnDefaultBlockedSeat.Value = ((FlightPakMasterService.FlightPurpose)RetValue[0]).FlightPurposeID.ToString().Trim();
                            tbDefaultBlockedSeat.Text = ((FlightPakMasterService.FlightPurpose)RetValue[0]).FlightPurposeCD;
                            RadAjaxManager1.FocusControl(tbSIFLLayoversHrs.ClientID);
                            // tbSIFLLayoversHrs.Focus();
                            return false;
                        }
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// To Validate Checklist Group 
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyDefaultChklistGroupExist()
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultChklistGroup.Text != string.Empty) && (tbDefaultChklistGroup.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetTripManagerChecklistGroupList().EntityList.Where(x => x.CheckGroupCD.Trim().ToUpper().Equals(tbDefaultChklistGroup.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvDefaultChklistGroup.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultChklistGroup.ClientID);
                            //tbDefaultChklistGroup.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            hdnCheckGroupID.Value = ((FlightPakMasterService.TripManagerCheckListGroup)RetValue[0]).CheckGroupID.ToString().Trim();
                            tbDefaultChklistGroup.Text = ((FlightPakMasterService.TripManagerCheckListGroup)RetValue[0]).CheckGroupCD;
                            RadAjaxManager1.FocusControl(tbTripManagerStatus.ClientID);
                            // tbTripManagerStatus.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        /// <summary>
        /// To Validate Crew Duty Rules
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyDefaultCrewDutyRulesExist()
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultCrewDutyRules.Text != string.Empty) && (tbDefaultCrewDutyRules.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetCrewDutyRuleList().EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper().Equals(tbDefaultCrewDutyRules.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvDefaultCrewDutyRules.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultCrewDutyRules.ClientID);
                            //tbDefaultCrewDutyRules.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            hdnCrewDutyRulesID.Value = ((FlightPakMasterService.CrewDutyRules)RetValue[0]).CrewDutyRulesID.ToString().Trim();
                            if (!string.IsNullOrEmpty(RetValue[0].FedAviatRegNum))
                                radlstDefaultFAR.SelectedValue = RetValue[0].FedAviatRegNum.Trim();
                            tbDefaultCrewDutyRules.Text = RetValue[0].CrewDutyRuleCD;
                            RadAjaxManager1.FocusControl(radlstDefaultFAR.ClientID);
                            // radlstDefaultFAR.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        /// <summary>
        /// To Validate Flight Category
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyDefaultFlightCategoryExist()
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultFlightCategory.Text != string.Empty) && (tbDefaultFlightCategory.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetFlightCategoryList().EntityList.Where(x => x.FlightCatagoryCD.Trim().ToUpper().Equals(tbDefaultFlightCategory.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvDefaultFlightCategory.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultFlightCategory.ClientID);
                            //tbDefaultFlightCategory.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            hdnFlightCategoryID.Value = ((FlightPakMasterService.FlightCatagory)RetValue[0]).FlightCategoryID.ToString().Trim();
                            tbDefaultFlightCategory.Text = ((FlightPakMasterService.FlightCatagory)RetValue[0]).FlightCatagoryCD;
                            RadAjaxManager1.FocusControl(tbDefaultChklistGroup.ClientID);
                            //tbDefaultChklistGroup.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        /// <summary>
        /// To Validate Tripsheet RW Group 
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyDefaultTripsheetRWGroup()
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultTripsheetRWGroup.Text != string.Empty) && (tbDefaultTripsheetRWGroup.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetTripSheetReportHeaderList().EntityList.Where(x => x.ReportID != null && x.ReportID.Trim().ToUpper().Equals(tbDefaultTripsheetRWGroup.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvDefaultTripsheetRWGroup.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultTripsheetRWGroup.ClientID);
                            //tbDefaultTripsheetRWGroup.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            hdnDefaultTripsheetRWGroup.Value = ((FlightPakMasterService.GetTripSheetReportHeader)RetValue[0]).TripSheetReportHeaderID.ToString().Trim();
                            tbDefaultTripsheetRWGroup.Text = ((FlightPakMasterService.GetTripSheetReportHeader)RetValue[0]).ReportID;
                            RadAjaxManager1.FocusControl(tbGADeskId.ClientID);
                            //tbGADeskId.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        /// <summary>
        /// To Check Crew Character
        /// </summary>
        /// <returns></returns>
        private void CheckCrewCharacter()
        {
            StringBuilder CrewInfo = new StringBuilder();
            string strNextLine = "<br/>";
            bool IsError = true;
            bool IsErrors = true;
            int CodeCount = 0;
            for (int Index = 0; Index <= 2; Index++)
            {
                if (Index == 0)
                {
                    if (!string.IsNullOrEmpty(tbFirstCrewCharacter.Text))
                    {
                        CodeCount = CodeCount + Convert.ToInt32(tbFirstCrewCharacter.Text);
                        if (radlstCrewNameElement.Items[Index].Text == "FIRST NAME")
                        {
                            if ((Convert.ToInt32(tbFirstCrewCharacter.Text)) > 5)
                            {
                                CrewInfo.Append(" FIRST name can not exceed 5 characters " + strNextLine);
                                IsCrewOName = false;
                                IsError = false;
                                IstbCrewFirst = true;
                            }
                        }
                        if (radlstCrewNameElement.Items[Index].Text == "MIDDLE INITIAL")
                        {
                            if ((Convert.ToInt32(tbFirstCrewCharacter.Text)) > 1)
                            {
                                CrewInfo.Append("  MIDDLE initial can not exceed 1 character " + strNextLine);
                                IsError = false;
                                IsCrewOName = false;
                                IstbCrewFirst = true;
                            }
                        }
                        if (radlstCrewNameElement.Items[Index].Text == "LAST NAME")
                        {
                            if ((Convert.ToInt32(tbFirstCrewCharacter.Text)) > 5)
                            {
                                CrewInfo.Append(" LAST name can not exceed 5 characters " + strNextLine);
                                IsError = false;
                                IsCrewOName = false;
                                IstbCrewFirst = true;
                            }
                        }
                        if (((Convert.ToInt32(tbFirstCrewCharacter.Text) + Convert.ToInt32(tbMiddleCrewCharacter.Text) + Convert.ToInt32(tbLastCrewCharacter.Text)) > 5) && (radlstCrewNameElement.Items[Index].Index == 0) && (!IsError))
                        {
                            CrewInfo.Append(" The Length of Crewcode can not exceed 5 characters " + strNextLine);
                            tbFirstCrewCharacter.Text = "0";
                            IsCrewOName = false;
                            IsErrors = false;
                        }
                        if (((Convert.ToInt32(tbFirstCrewCharacter.Text) + Convert.ToInt32(tbMiddleCrewCharacter.Text) + Convert.ToInt32(tbLastCrewCharacter.Text)) < 6) && (radlstCrewNameElement.Items[Index].Index == 0) && (!IsError) && (IsErrors))
                        {
                            tbFirstCrewCharacter.Text = "0";
                            IsErrors = false;
                            IsCrewOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstCrewCharacter.Text) + Convert.ToInt32(tbMiddleCrewCharacter.Text) + Convert.ToInt32(tbLastCrewCharacter.Text)) > 5) && (radlstCrewNameElement.Items[Index].Index == 0) && (IsError) && (!IstbCrewFirst))
                        {
                            CrewInfo.Append(" The Length of Crewcode can not exceed 5 characters " + strNextLine);
                            tbFirstCrewCharacter.Text = "0";
                            IsCrewOName = false;
                            IsErrors = false;
                        }
                    }
                }
                if (Index == 1)
                {
                    if (!string.IsNullOrEmpty(tbMiddleCrewCharacter.Text))
                    {
                        CodeCount = CodeCount + Convert.ToInt32(tbMiddleCrewCharacter.Text);
                        if (radlstCrewNameElement.Items[Index].Text == "FIRST NAME")
                        {
                            if ((Convert.ToInt32(tbMiddleCrewCharacter.Text)) > 5)
                            {
                                CrewInfo.Append("  FIRST name can not exceed 5 characters " + strNextLine);
                                IsError = false;
                                IsCrewOName = false;
                                IstbCrewSecond = true;
                            }
                        }
                        if (radlstCrewNameElement.Items[Index].Text == "MIDDLE INITIAL")
                        {
                            if ((Convert.ToInt32(tbMiddleCrewCharacter.Text)) > 1)
                            {
                                CrewInfo.Append(" MIDDLE initial can not exceed 1 character " + strNextLine);
                                IsError = false;
                                IsCrewOName = false;
                                IstbCrewSecond = true;
                            }
                        }
                        if (radlstCrewNameElement.Items[Index].Text == "LAST NAME")
                        {
                            if ((Convert.ToInt32(tbMiddleCrewCharacter.Text)) > 5)
                            {
                                CrewInfo.Append("  LAST name can not exceed 5 characters " + strNextLine);
                                IsError = false;
                                IsCrewOName = false;
                                IstbCrewSecond = true;
                            }
                        }
                        if (((Convert.ToInt32(tbFirstCrewCharacter.Text) + Convert.ToInt32(tbMiddleCrewCharacter.Text) + Convert.ToInt32(tbLastCrewCharacter.Text)) > 5) && (radlstCrewNameElement.Items[Index].Index == 1) && (!IsError))
                        {
                            CrewInfo.Append(" The Length of Crewcode can not exceed 5 characters " + strNextLine);
                            tbMiddleCrewCharacter.Text = "0";
                            IsErrors = false;
                            IsCrewOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstCrewCharacter.Text) + Convert.ToInt32(tbMiddleCrewCharacter.Text) + Convert.ToInt32(tbLastCrewCharacter.Text)) > 5) && (radlstCrewNameElement.Items[Index].Index == 1) && (IsError) && (!IstbCrewSecond))
                        {
                            CrewInfo.Append(" The Length of Crewcode can not exceed 5 characters " + strNextLine);
                            tbMiddleCrewCharacter.Text = "0";
                            IsErrors = false;
                            IsCrewOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstCrewCharacter.Text) + Convert.ToInt32(tbMiddleCrewCharacter.Text) + Convert.ToInt32(tbLastCrewCharacter.Text)) < 6) && (radlstCrewNameElement.Items[Index].Index == 1) && (!IsError) && (IsErrors))
                        {
                            tbMiddleCrewCharacter.Text = "0";
                            IsErrors = false;
                            IsCrewOName = false;
                        }
                    }
                }
                if (Index == 2)
                {
                    if (!string.IsNullOrEmpty(tbLastCrewCharacter.Text))
                    {
                        CodeCount = CodeCount + Convert.ToInt32(tbLastCrewCharacter.Text);
                        if (radlstCrewNameElement.Items[Index].Text == "FIRST NAME")
                        {
                            if ((Convert.ToInt32(tbLastCrewCharacter.Text)) > 5)
                            {
                                CrewInfo.Append(" FIRST name can not exceed 5 characters " + strNextLine);
                                IsError = false;
                                IsCrewOName = false;
                                IstbCrewThird = true;
                            }
                        }
                        if (radlstCrewNameElement.Items[Index].Text == "MIDDLE INITIAL")
                        {
                            if ((Convert.ToInt32(tbLastCrewCharacter.Text)) > 1)
                            {
                                CrewInfo.Append(" MIDDLE initial can not exceed 1 character " + strNextLine);
                                IsError = false;
                                IsCrewOName = false;
                                IstbCrewThird = true;
                            }
                        }
                        if (radlstCrewNameElement.Items[Index].Text == "LAST NAME")
                        {
                            if ((Convert.ToInt32(tbLastCrewCharacter.Text)) > 5)
                            {
                                CrewInfo.Append(" LAST name can not exceed 5 characters " + strNextLine);
                                IsError = false;
                                IsCrewOName = false;
                                IstbCrewThird = true;
                            }
                        }
                        if (((Convert.ToInt32(tbFirstCrewCharacter.Text) + Convert.ToInt32(tbMiddleCrewCharacter.Text) + Convert.ToInt32(tbLastCrewCharacter.Text)) > 5) && (radlstCrewNameElement.Items[Index].Index == 2) && (!IsError))
                        {
                            CrewInfo.Append("The Length of Crewcode can not exceed 5 characters " + strNextLine);
                            tbLastCrewCharacter.Text = "0";
                            IsErrors = false;
                            IsCrewOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstCrewCharacter.Text) + Convert.ToInt32(tbMiddleCrewCharacter.Text) + Convert.ToInt32(tbLastCrewCharacter.Text)) > 5) && (radlstCrewNameElement.Items[Index].Index == 2) && (IsError) && (!IstbCrewThird))
                        {
                            CrewInfo.Append("The Length of Crewcode can not exceed 5 characters " + strNextLine);
                            tbLastCrewCharacter.Text = "0";
                            IsErrors = false;
                            IsCrewOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstCrewCharacter.Text) + Convert.ToInt32(tbMiddleCrewCharacter.Text) + Convert.ToInt32(tbLastCrewCharacter.Text)) < 6) && (radlstCrewNameElement.Items[Index].Index == 2) && (!IsError) && (IsErrors))
                        {
                            tbLastCrewCharacter.Text = "0";
                            IsErrors = false;
                            IsCrewOName = false;
                        }
                    }
                }
            }
            if (CodeCount < 2)
            {
                IsError = true;
                IsCrewOName = false;
                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                    + @" oManager.radalert('Crew Code cannot be less than two characters.', 400, 100, 'Company Profile!');";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                return;
            }
            if ((!IsError) || (!IsErrors))
            {
                string msgToDisplay = CrewInfo.ToString().Trim();
                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Company Profile!", "");
            }
        }
        /// <summary>
        /// To Check Pax Character
        /// </summary>
        /// <returns></returns>
        private void CheckPaxCharacter()
        {
            StringBuilder PaxInfo = new StringBuilder();
            string strNextLine = "<br/>";
            bool IsError = true;
            bool IsErrors = true;
            int CodeCount = 0;
            for (int Index = 0; Index <= 2; Index++)
            {
                if (Index == 0)
                {
                    if (!string.IsNullOrEmpty(tbFirstPaxCharacter.Text))
                    {
                        CodeCount = CodeCount + Convert.ToInt32(tbFirstPaxCharacter.Text);
                        if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                        {
                            if ((Convert.ToInt32(tbFirstPaxCharacter.Text)) > 5)
                            {
                                PaxInfo.Append("FIRST name can not exceed 5 characters. " + strNextLine);
                                IsError = false;
                                IsPaxOName = false;
                                IstbPaxFirst = true;
                            }
                        }
                        if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                        {
                            if ((Convert.ToInt32(tbFirstPaxCharacter.Text)) > 1)
                            {
                                PaxInfo.Append("MIDDLE initial can not exceed 1 character. " + strNextLine);
                                IsError = false;
                                IsPaxOName = false;
                                IstbPaxFirst = true;
                            }
                        }
                        if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                        {
                            hdnPaxOFirst.Value = tbFirstPaxCharacter.Text;
                            if ((Convert.ToInt32(tbFirstPaxCharacter.Text)) > 5)
                            {
                                PaxInfo.Append("LAST name can not exceed 5 characters. " + strNextLine);
                                IsError = false;
                                IsPaxOName = false;
                                IstbPaxFirst = true;
                            }
                        }
                        if (((Convert.ToInt32(tbFirstPaxCharacter.Text) + Convert.ToInt32(tbInitialPaxCharacter.Text) + Convert.ToInt32(tbLastPaxCharacter.Text)) > 5) && (radlstPaxNameElements.Items[Index].Index == 0) && (!IsError))
                        {
                            PaxInfo.Append("The Length of Paxcode can not exceed 5 characters. " + strNextLine);
                            tbFirstPaxCharacter.Text = "0";
                            IsErrors = false;
                            IsPaxOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstPaxCharacter.Text) + Convert.ToInt32(tbInitialPaxCharacter.Text) + Convert.ToInt32(tbLastPaxCharacter.Text)) > 5) && (radlstPaxNameElements.Items[Index].Index == 0) && (IsError) && (!IstbPaxFirst))
                        {
                            PaxInfo.Append("The Length of Paxcode can not exceed 5 characters. " + strNextLine);
                            tbFirstPaxCharacter.Text = "0";
                            IsErrors = false;
                            IsPaxOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstPaxCharacter.Text) + Convert.ToInt32(tbInitialPaxCharacter.Text) + Convert.ToInt32(tbLastPaxCharacter.Text)) < 6) && (radlstPaxNameElements.Items[Index].Index == 0) && (!IsError) && (IsErrors))
                        {
                            tbFirstPaxCharacter.Text = "0";
                            IsErrors = false;
                            IsPaxOName = false;
                        }
                    }
                }
                if (Index == 1)
                {
                    if (!string.IsNullOrEmpty(tbInitialPaxCharacter.Text))
                    {
                        CodeCount = CodeCount + Convert.ToInt32(tbInitialPaxCharacter.Text);
                        if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                        {
                            if ((Convert.ToInt32(tbInitialPaxCharacter.Text)) > 5)
                            {
                                PaxInfo.Append("FIRST name can not exceed 5 characters. " + strNextLine);
                                IsError = false;
                                IsPaxOName = false;
                                IstbPaxSecond = true;
                            }
                        }
                        if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                        {
                            hdnPaxOMiddle.Value = tbInitialPaxCharacter.Text;
                            if ((Convert.ToInt32(tbInitialPaxCharacter.Text)) > 1)
                            {
                                PaxInfo.Append("MIDDLE initial can not exceed 1 character. " + strNextLine);
                                IsError = false;
                                IsPaxOName = false;
                                IstbPaxSecond = true;
                            }
                        }
                        if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                        {
                            hdnCrewOLast.Value = tbInitialPaxCharacter.Text;
                            if ((Convert.ToInt32(tbInitialPaxCharacter.Text)) > 5)
                            {
                                PaxInfo.Append("LAST name can not exceed 5 characters. " + strNextLine);
                                IsError = false;
                                IsPaxOName = false;
                                IstbPaxSecond = true;
                            }
                        }
                        if (((Convert.ToInt32(tbFirstPaxCharacter.Text) + Convert.ToInt32(tbInitialPaxCharacter.Text) + Convert.ToInt32(tbLastPaxCharacter.Text)) > 5) && (radlstPaxNameElements.Items[Index].Index == 1) && (!IsError))
                        {
                            PaxInfo.Append("The Length of Paxcode can not exceed 5 characters. " + strNextLine);
                            tbInitialPaxCharacter.Text = "0";
                            IsErrors = false;
                            IsPaxOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstPaxCharacter.Text) + Convert.ToInt32(tbInitialPaxCharacter.Text) + Convert.ToInt32(tbLastPaxCharacter.Text)) > 5) && (radlstPaxNameElements.Items[Index].Index == 1) && (IsError) && (!IstbPaxSecond))
                        {
                            PaxInfo.Append("The Length of Paxcode can not exceed 5 characters. " + strNextLine);
                            tbInitialPaxCharacter.Text = "0";
                            IsErrors = false;
                            IsPaxOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstPaxCharacter.Text) + Convert.ToInt32(tbInitialPaxCharacter.Text) + Convert.ToInt32(tbLastPaxCharacter.Text)) < 6) && (radlstPaxNameElements.Items[Index].Index == 1) && (!IsError) && (IsErrors))
                        {
                            tbInitialPaxCharacter.Text = "0";
                            IsErrors = false;
                            IsPaxOName = false;
                        }
                    }
                }
                if (Index == 2)
                {
                    if (!string.IsNullOrEmpty(tbLastPaxCharacter.Text))
                    {
                        CodeCount = CodeCount + Convert.ToInt32(tbLastPaxCharacter.Text);
                        if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                        {
                            if ((Convert.ToInt32(tbLastPaxCharacter.Text)) > 5)
                            {
                                PaxInfo.Append("FIRST name can not exceed 5 characters. " + strNextLine);
                                IsError = false;
                                IsPaxOName = false;
                                IstbPaxThird = true;
                            }
                        }
                        if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                        {
                            hdnCrewOMiddle.Value = tbLastPaxCharacter.Text;
                            if ((Convert.ToInt32(tbLastPaxCharacter.Text)) > 1)
                            {
                                PaxInfo.Append("MIDDLE initial can not exceed 1 character. " + strNextLine);
                                IsError = false;
                                IsPaxOName = false;
                                IstbPaxThird = true;
                            }
                        }
                        if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                        {
                            hdnCrewOLast.Value = tbLastPaxCharacter.Text;
                            if ((Convert.ToInt32(tbLastPaxCharacter.Text)) > 5)
                            {
                                PaxInfo.Append("LAST name can not exceed 5 characters. " + strNextLine);
                                IsError = false;
                                IsPaxOName = false;
                                IstbPaxThird = true;
                            }
                        }
                        if (((Convert.ToInt32(tbFirstPaxCharacter.Text) + Convert.ToInt32(tbInitialPaxCharacter.Text) + Convert.ToInt32(tbLastPaxCharacter.Text)) > 5) && (radlstPaxNameElements.Items[Index].Index == 2) && (!IsError))
                        {
                            PaxInfo.Append("The Length of Paxcode can not exceed 5 characters. " + strNextLine);
                            tbLastPaxCharacter.Text = "0";
                            IsErrors = false;
                            IsPaxOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstPaxCharacter.Text) + Convert.ToInt32(tbInitialPaxCharacter.Text) + Convert.ToInt32(tbLastPaxCharacter.Text)) > 5) && (radlstPaxNameElements.Items[Index].Index == 2) && (IsError) && (!IstbPaxThird))
                        {
                            PaxInfo.Append("The Length of Paxcode can not exceed 5 characters. " + strNextLine);
                            tbLastPaxCharacter.Text = "0";
                            IsErrors = false;
                            IsPaxOName = false;
                        }
                        if (((Convert.ToInt32(tbFirstPaxCharacter.Text) + Convert.ToInt32(tbInitialPaxCharacter.Text) + Convert.ToInt32(tbLastPaxCharacter.Text)) < 6) && (radlstPaxNameElements.Items[Index].Index == 2) && (!IsError) && (IsErrors))
                        {
                            tbLastPaxCharacter.Text = "0";
                            IsErrors = false;
                            IsPaxOName = false;
                        }
                    }
                }
            }
            if (CodeCount < 2)
            {
                IsPaxOName = false;
                IsError = true;
                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                    + @" oManager.radalert('Pax Code cannot be less than two characters.', 400, 100, 'Company Profile!');";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                return;
            }
            if ((!IsError) || (!IsErrors))
            {
                string msgToDisplay = PaxInfo.ToString().Trim();
                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "Company Profile!", "");
            }
        }
        /// <summary>
        /// To Default Selection of first record
        /// </summary>
        private void DefaultSelection()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCompanyProfileCatalog.Rebind();
                        dgCompanyProfileCatalog.SelectedIndexes.Add(0);
                        if (dgCompanyProfileCatalog.MasterTableView.Items.Count > 0)
                        {
                            if (!IsPopUp)
                            {
                                Session["HomeBaseID"] = dgCompanyProfileCatalog.Items[0].GetDataKeyValue("HomeBaseID").ToString();
                                ReadOnlyForm();
                                DisableLinks();
                            }
                        }
                        else
                        {
                            EnableForm(false);
                        }
                        GridEnable(true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        protected void StateTaxAccount_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbStateTaxAccount.Text))
                        {
                            CheckAlreadyStateTaxAccountExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Clear values of control
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbMainBase.Text = string.Empty;
                        hdnHomeBaseID.Value = string.Empty;
                        lbMainBase1.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbBaseDescription.Text = string.Empty;
                        tbCompanyName.Text = string.Empty;
                        tbPhone.Text = string.Empty;
                        tbBaseFax.Text = string.Empty;
                        tbCanpassPermit.Text = string.Empty;
                        tbAccountFormat.Text = string.Empty;
                        tbCorpAcctFormat.Text = string.Empty;
                        tbFedTaxAccount.Text = string.Empty;
                        tbStateTaxAccount.Text = string.Empty;
                        tbSalesTaxAccount.Text = string.Empty;
                        tbExchangeRateCode.Text = string.Empty;
                        //dpBusinessWeekStart.SelectedValue = "0";
                        //tbBusinessWeekStart.Text = string.Empty;
                        tbCrewChecklistAlertDays.Text = string.Empty;
                        tbCurrency.Text = string.Empty;
                        //tbGridSrch.Text = string.Empty;
                        //chkNoteDisplay.Checked = false; //*2483
                        chkAutoDispatchNumber.Checked = false;
                        chkInactive.Checked = false;
                        chkConflictChecking.Checked = false;
                        //chkTextAutotab.Checked = false; //*2483
                        chkCanpasspermit.Checked = false;
                        ddlDefaultDateFormat.SelectedIndex = 0;
                        tbgeneralnotes.Text = string.Empty;
                        //APIS
                        chkEnableAPISInterface.Checked = false;
                        chkAlertToresubmitTrips.Checked = false;
                        tbURLAddress.Text = string.Empty;
                        tbDomain.Text = string.Empty;
                        tbCBPUserPassword.Attributes.Add(constvalue, string.Empty);
                        tbSenderID.Text = string.Empty;
                        hdnCheckGroupID.Value = "";
                        hdnFlightCategoryID.Value = "";
                        hdnCrewDutyRulesID.Value = "";
                        //Reports
                        tbReportMessage.Text = string.Empty;
                        chkTimeStampReports.Checked = false;
                        chkAircraft.Checked = false;
                        chkPrintUWAReports.Checked = false;
                        chkPax.Checked = false;
                        chkCrew.Checked = false;
                        chkDept.Checked = false;
                        rbMonthDay.Checked = true;
                        rbDayMonth.Checked = false;
                        //preflight
                        chkDeactiveHomeBaseFilter.Checked = false;
                        chkAutoCalcLegTimes.Checked = false;
                        chkUseChecklistWarnings.Checked = false;
                        chkChklistItemsToNewCrewMembers.Checked = false;
                        chkDoNotUpdateFuelBrand.Checked = false;
                        chkAssignAuthDuringRequestorSelection.Checked = false;
                        chkIncludeNonLoggedTripsheetsinCrewHistory.Checked = false;
                        //chkAutoValidateTripManager.Checked = false;
                        chkAutoCreateCrewAvailabilityInfo.Checked = false;
                        chkShowRequestorsOnly.Checked = false;
                        chkAutoPopulateDispatcher.Checked = false;
                        chkAutoRevisionNumber.Checked = false;
                        chkHighlightAircraftHavingNotes.Checked = false;
                        chkHighlightCrewtHavingNotes.Checked = false;
                        chkHighlightPassengerHavingNotes.Checked = false;
                        chkOpenHistoryOnEditingTrip.Checked = false;
                        chkDefaultTripDepartureDate.Checked = false;
                        chkCrewQualificationLegFAR.Checked = false;
                        chk24hrTripOverlapforCrew.Checked = false;
                        chkPassengerOverlap.Checked = false;
                        chkTailInspectionDue.Checked = false;
                        chkCrewPassportExpiry.Checked = false;                       
                        chkPaxPassportExpiry.Checked = false;
                        //need now
                        radlstMilesKilo.ClearSelection();
                        radlstMilesKilo.SelectedValue = "0";    //added this line temporarily for default selection as miles
                        //tbFltCallTimer.Text = string.Empty;
                        tbDomestic.Text = "0.00";
                        tbIntl.Text = "0.00";
                        tbDomesticTime.Text = "00:00";
                        tbIntlTime.Text = "00:00";
                        tbDefaultCrewDutyRules.Text = string.Empty;
                        tbTripManagerSrchDaysBack.Text = "0";
                        tbTripManagerDateWarning.Text = string.Empty;
                        tbDefaultFlightCategory.Text = string.Empty;
                        tbDefaultChklistGroup.Text = string.Empty;
                        tbTripManagerStatus.Text = "W";
                        tbDefaultTripsheetRWGroup.Text = string.Empty;
                        tbGADeskId.Text = string.Empty;
                        //Commented as this is not valid anymore in new application
                        //tbTravelSenseLocation.Text = string.Empty;
                        radlstTimeDisplay.SelectedIndex = 0;
                        radlstBoeingWinds.SelectedIndex = 0;
                        radlstDefaultFAR.SelectedIndex = 0;
                        radlstETERound.SelectedIndex = 0;
                        radlstLogoPosition.SelectedIndex = 0;
                        radlstPaxNameElements.SelectedIndex = 0;
                        radlstCrewNameElement.SelectedIndex = 0;
                        tbOutboundInst1.Text = string.Empty;
                        tbOutboundInst2.Text = string.Empty;
                        tbOutboundInst3.Text = string.Empty;
                        tbOutboundInst4.Text = string.Empty;
                        tbOutboundInst5.Text = string.Empty;
                        tbOutboundInst6.Text = string.Empty;
                        tbOutboundInst7.Text = string.Empty;
                        //PostFlight
                        radlstLogFixedWing.SelectedIndex = 1;
                        radlstLogRotaryWing.SelectedIndex = 0;
                        radlstPostflightCrewDuty.SelectedIndex = 0;
                        radlstPostflightAircraft.SelectedIndex = 1;
                        radlstFuelPurchaseUnits.SelectedIndex = 0;
                        radlstFuelBurn.SelectedIndex = 0;
                        radlstEngineAirframe.SelectedIndex = 0;
                        radOther.Checked = false;
                        radFlightPak.Checked = false;
                        radStandard.Checked = false;
                        ddlEndMonth.SelectedIndex = 0;
                        ddlStartMonth.SelectedIndex = 0;
                        chkLogBlockedSeats.Checked = false;
                        chkUseLogsheetWarnings.Checked = false;
                        chkPromptTocalculateSIFL.Checked = false;
                        chkAutoFillEngineAirframeHoursandCycles.Checked = false;
                        chkIsPODepartPercentage.Checked = false;
                        chkShowScheduleDateValidation.Checked = false;
                        chkRemoveEndDutyMarker.Checked = false;
                        chkAutoPopulateLegTimes.Checked = false;
                        rmttbTaxiTimes.Text = "00:00";
                        tbFlightLogSrchDaysBack.Text = string.Empty;
                        tbSIFLLayoversHrs.Text = string.Empty;
                        tbDefaultBlockedSeat.Text = string.Empty;
                        tbShort3.Text = string.Empty;
                        tbShort4.Text = string.Empty;
                        tbLong3.Text = string.Empty;
                        tbLong4.Text = string.Empty;
                        tbDec1.Text = string.Empty;
                        tbDec2.Text = string.Empty;
                        tbShort1.Text = string.Empty;
                        tbShort2.Text = string.Empty;
                        tbLong1.Text = string.Empty;
                        tbLong2.Text = string.Empty;
                        //DataBase
                        chkEnableTSA.Checked = false;
                        M3.Checked = true;
                        //chkIsBlackberrySupport.Checked = false;
                        chkEnableTSAPAX.Checked = false;
                        tbAutoClosureTime.Text = "00.00";
                        hdnCheckGroupID.Value = string.Empty;
                        tbFirstPaxCharacter.Text = "0";
                        tbFirstCrewCharacter.Text = "0";
                        tbLastCrewCharacter.Text = "0";
                        tbLastPaxCharacter.Text = "0";
                        tbMiddleCrewCharacter.Text = "0";
                        tbInitialPaxCharacter.Text = "0";
                       
                        //Clear Image related data
                        CreateDictionayForImgUpload();
                        fileUL.Enabled = false;
                        ddlImg.Enabled = false;
                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                        ImgPopup.ImageUrl = null;
                        ddlImg.Items.Clear();
                        ddlImg.Text = "";
                        tbImgName.Text = "";
                        chkAutoRON.Checked = false;
                        chkWarningMessage.Checked = false;
                        chkAutoDateAdjustment.Checked = false;
                        chkDeactivateAutoUpdatingRates.Checked = false;
                        chkTaxDiscountedTotal.Checked = false;
                        chkTailNumHomebase.Checked = false;
                        tbCharterDepartment.Text = string.Empty;
                        hdnDepartmentID.Value = string.Empty;
                        tbPax.Text = string.Empty;
                        hdnPax.Value = string.Empty;
                        tbNoPax.Text = string.Empty;
                        hdnNoPax.Value = string.Empty;
                        radAllLegs.Checked = true;
                        radDomesticleg.Checked = false;
                        radNone.Checked = false;
                        radTaxes.Checked = false;
                        radUSSegmentFees.Checked = false;
                        tbTaxRate1.Text = string.Empty;
                        tbFedAccNum1.Text = string.Empty;
                        hdnFedAccNum1.Value = string.Empty;
                        tbTaxRate2.Text = string.Empty;
                        tbFedAccNum2.Text = string.Empty;
                        hdnFedAccNum2.Value = string.Empty;
                        tbFee1.Text = string.Empty;
                        tbSegFeeAccNum1.Text = string.Empty;
                        hdnSegFeeAccNum1.Value = string.Empty;
                        tbFee2.Text = string.Empty;
                        tbSegFeeAccNum2.Text = string.Empty;
                        hdnSegFeeAccNum2.Value = string.Empty;
                        tbFee3.Text = string.Empty;
                        tbSegFeeAccNum3.Text = string.Empty;
                        tbFee4.Text = string.Empty;
                        tbSegFeeAccNum4.Text = string.Empty;
                        hdnSegFeeAccNum4.Value = string.Empty;
                        tbMinRONHrs.Text = "0";
                        tbSearchDaysBack.Text = "0";
                        RdlstCqPosition.SelectedIndex = 0;
                        hdnCrewDutyRules.Value = string.Empty;
                        tbCQDefaultCrewDutyRules.Text = string.Empty;
                        //Clear Image related data
                        CreateCqDictionayForImgUpload();
                        fuCQ.Enabled = false;
                        ddlCQFileName.Enabled = false;
                        imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                        ImgPopup.ImageUrl = null;
                        ddlCQFileName.Items.Clear();
                        ddlCQFileName.Text = string.Empty;
                        tbCQimgName.Text = string.Empty;
                        Session.Remove("ConversionDefaults");
                        Session.Remove("CqCompanyStringInvoiceReport");
                        Session.Remove("CqCompanyCheckInvoiceReport");
                        Session.Remove("CqCompanyRadInvoiceReport");
                        Session.Remove("CqCompanyStringQuoteReport");
                        Session.Remove("CqCompanyCheckQuoteReport");
                        Session.Remove("CqCompanyRadQuoteReport");
                        Session.Remove("CQQuoteDicImg");
                        Session.Remove("CqQuoteReportBase64");
                        Session.Remove("DicCQQuoteImgDelete");
                        Session.Remove("CqInvoiceImgDelete");
                        Session.Remove("CqInvoiceDicImg");
                        Session.Remove("CqInvoiceReportBase64");
                        Session.Remove("CQQuoteFooterList");
                        Session.Remove("CQInvoiceFooterList");
                        Session.Remove("CQQuoteFooter");
                        Session.Remove("CQInvoiceFooter");
                        lbCharterDeptInfo.Text = string.Empty;
                        lbPaxInfo.Text = string.Empty;
                        lbNoPaxInfo.Text = string.Empty;
                        hdnHmeBaseId.Value = string.Empty;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Enable or Disable Controls
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        M3.Enabled = Enable;
                        M6.Enabled = Enable;
                        Y1.Enabled = Enable;
                        tbMainBase.Enabled = Enable;
                        tbBaseDescription.Enabled = Enable;
                        tbCompanyName.Enabled = Enable;
                        tbPhone.Enabled = Enable;
                        tbBaseFax.Enabled = Enable;
                        tbAccountFormat.Enabled = Enable;
                        tbCorpAcctFormat.Enabled = Enable;
                        tbFedTaxAccount.Enabled = Enable;
                        tbStateTaxAccount.Enabled = Enable;
                        tbSalesTaxAccount.Enabled = Enable;
                        tbCurrency.Enabled = Enable;
                        //tbGridSrch.Enabled = Enable;
                        //chkNoteDisplay.Enabled = Enable; //*2483
                        chkAutoDispatchNumber.Enabled = Enable;
                        chkInactive.Enabled = Enable;
                        chkConflictChecking.Enabled = Enable;
                        //chkTextAutotab.Enabled = Enable;  //*2483
                        chkCanpasspermit.Enabled = Enable;
                        tbExchangeRateCode.Enabled = Enable;
                        //tbBusinessWeekStart.Enabled = Enable;
                        //dpBusinessWeekStart.Enabled = Enable;
                        tbCrewChecklistAlertDays.Enabled = Enable;
                        if (chkCanpasspermit.Checked == true && Enable == true)
                        {
                            tbCanpassPermit.Enabled = true;
                        }
                        else
                        {
                            if (Enable == false)
                                tbCanpassPermit.Enabled = false;
                        }
                        tbgeneralnotes.Enabled = Enable;
                        ddlDefaultDateFormat.Enabled = Enable;
                        if ((hdnSaveFlag.Value != "Update") && (Enable == true))
                        {
                            btnMainBase.Enabled = Enable;
                        }
                        else
                        {
                            btnMainBase.Enabled = false;
                        }
                        btnFedTaxAccount.Enabled = Enable;
                        btnStateTaxAccount.Enabled = Enable;
                        btnSalesTaxAccount.Enabled = Enable;
                        TSARadClearFile.Enabled = Enable;
                        TSASelecteeFile.Enabled = Enable;
                        TSANoFlyFile.Enabled = Enable;
                        //APIS
                        chkEnableAPISInterface.Enabled = Enable;
                        chkAlertToresubmitTrips.Enabled = Enable;
                        tbURLAddress.Enabled = Enable;
                        tbDomain.Enabled = Enable;
                        tbSenderID.Enabled = Enable;
                        tbCBPUserPassword.Enabled = Enable;
                        //if (hdnSaveFlag.Value == "Update")
                        //{
                        //    btnSubmittoUWA.Enabled = true;
                        //}
                        //else
                        //{
                        //    btnSubmittoUWA.Enabled = false;
                        //}
                        //  
                        //  fuXMLFilesLocation.Enabled = Enable;
                        btnSubmittoUWA.Enabled = Enable;
                        //Reports
                        tbReportMessage.Enabled = Enable;
                        chkTimeStampReports.Enabled = Enable;
                        chkAircraft.Enabled = Enable;
                        chkPrintUWAReports.Enabled = Enable;
                        chkPax.Enabled = Enable;
                        chkCrew.Enabled = Enable;
                        chkDept.Enabled = Enable;
                        rbMonthDay.Enabled = Enable;
                        rbDayMonth.Enabled = Enable;
                        //Blackberry
                        //chkEnableBlackBerrysupport.Enabled = enable;
                        //chkSendEmailtoMaintCrew.Enabled = enable;
                        //chkSMTPPortisBlocked.Enabled = enable;
                        //tbServerName.Enabled = enable;
                        //tbPort.Enabled = enable;
                        //tbUserName.Enabled = false;
                        //tbPassword.Enabled = false;
                        //tbEmailId.Enabled = enable;
                        //chkApplySettingtoallMainbases.Enabled = enable;
                        //chkUseSSL.Enabled = enable;
                        //radlstBlackBerry.Enabled = enable;
                        //preflight
                        chkDeactiveHomeBaseFilter.Enabled = Enable;
                        chkAutoCalcLegTimes.Enabled = Enable;
                        chkUseChecklistWarnings.Enabled = Enable;
                        chkChklistItemsToNewCrewMembers.Enabled = Enable;
                        chkDoNotUpdateFuelBrand.Enabled = Enable;
                        chkAssignAuthDuringRequestorSelection.Enabled = Enable;
                        chkIncludeNonLoggedTripsheetsinCrewHistory.Enabled = Enable;
                        //chkAutoValidateTripManager.Enabled = Enable;
                        chkAutoCreateCrewAvailabilityInfo.Enabled = Enable;
                        chkShowRequestorsOnly.Enabled = Enable;
                        chkAutoPopulateDispatcher.Enabled = Enable;
                        chkAutoRevisionNumber.Enabled = Enable;
                        chkHighlightAircraftHavingNotes.Enabled = Enable;
                        chkHighlightCrewtHavingNotes.Enabled = Enable;
                        chkHighlightPassengerHavingNotes.Enabled = Enable;
                        chkOpenHistoryOnEditingTrip.Enabled = Enable;
                        chkDefaultTripDepartureDate.Enabled = Enable;
                        chkCrewQualificationLegFAR.Enabled = Enable;
                        chk24hrTripOverlapforCrew.Enabled = Enable;
                        chkPassengerOverlap.Enabled = Enable;
                        chkTailInspectionDue.Enabled = Enable;
                        chkPaxPassportExpiry.Enabled = Enable;
                        chkCrewPassportExpiry.Enabled = Enable;
                        //need now
                        radlstMilesKilo.Enabled = Enable;
                        //tbFltCallTimer.Enabled = Enable;
                        tbDomestic.Enabled = Enable;
                        tbDomesticTime.Enabled = Enable;
                        tbIntl.Enabled = Enable;
                        tbIntlTime.Enabled = Enable;
                        tbDefaultCrewDutyRules.Enabled = Enable;
                        tbTripManagerSrchDaysBack.Enabled = Enable;                       
                        tbTripManagerDateWarning.Enabled = Enable;
                        tbDefaultFlightCategory.Enabled = Enable;
                        tbDefaultChklistGroup.Enabled = Enable;
                        tbTripManagerStatus.Enabled = Enable;
                        tbDefaultTripsheetRWGroup.Enabled = Enable;
                        tbGADeskId.Enabled = Enable;
                        //Commented as this is not valid anymore in new application
                        //tbTravelSenseLocation.Enabled = Enable;
                        if (IsTimeDisplay == false)
                        {
                            radlstETERound.Enabled = false;
                        }
                        else
                        {
                            if (Enable == true)
                                radlstETERound.Enabled = true;
                            else
                                radlstETERound.Enabled = false;
                        }
                        radlstTimeDisplay.Enabled = Enable;
                        radlstBoeingWinds.Enabled = Enable;
                        radlstDefaultFAR.Enabled = Enable;
                        //radlstETERound.Enabled = enable;
                        btncqdeleteImage.Enabled = Enable;
                        btncqdeleteImage.Visible = Enable;
                        radlstLogoPosition.Enabled = Enable;
                        chkAutoPaxcode.Enabled = Enable;
                        chkAutoCrewcode.Enabled = Enable;
                        radlstPaxNameElements.Enabled = Enable;
                        radlstCrewNameElement.Enabled = Enable;
                        tbOutboundInst1.Enabled = Enable;
                        tbOutboundInst2.Enabled = Enable;
                        tbOutboundInst3.Enabled = Enable;
                        tbOutboundInst4.Enabled = Enable;
                        tbOutboundInst5.Enabled = Enable;
                        tbOutboundInst6.Enabled = Enable;
                        tbOutboundInst7.Enabled = Enable;
                        tbImgName.Enabled = Enable;
                        //DataBase
                        chkEnableTSA.Enabled = Enable;
                        chkEnableTSAPAX.Enabled = Enable;
                       
                        //chkIsBlackberrySupport.Enabled = Enable;
                        //radtbPasswordExpires.Enabled = Enable;
                        // radtbPasswordRemembered.Enabled = Enable;
                        tbAutoClosureTime.Enabled = Enable;
                        //0pax
                        tbFirstPaxCharacter.Enabled = Enable;
                        tbFirstCrewCharacter.Enabled = Enable;
                        tbLastCrewCharacter.Enabled = Enable;
                        tbLastPaxCharacter.Enabled = Enable;
                        tbMiddleCrewCharacter.Enabled = Enable;
                        tbInitialPaxCharacter.Enabled = Enable;
                       
                        //PostFlight
                        radlstLogFixedWing.Enabled = Enable;
                        radlstLogRotaryWing.Enabled = Enable;
                        radlstPostflightCrewDuty.Enabled = Enable;
                        radlstPostflightAircraft.Enabled = Enable;
                        radlstFuelPurchaseUnits.Enabled = Enable;
                        radlstFuelBurn.Enabled = Enable;
                        // radlstEngineAirframe.Enabled = Enable;
                        radStandard.Enabled = Enable;
                        radFlightPak.Enabled = Enable;
                        radOther.Enabled = Enable;
                        ddlStartMonth.Enabled = Enable;
                        chkLogBlockedSeats.Enabled = Enable;
                        chkUseLogsheetWarnings.Enabled = Enable;
                        chkPromptTocalculateSIFL.Enabled = Enable;
                        chkAutoFillEngineAirframeHoursandCycles.Enabled = Enable;
                        chkIsPODepartPercentage.Enabled = Enable;
                        if (chkAutoFillEngineAirframeHoursandCycles.Checked == true && Enable == true)
                            radlstEngineAirframe.Enabled = true;
                        else
                            radlstEngineAirframe.Enabled = false;
                        if (chkAutoPopulateLegTimes.Checked == true && Enable == true)
                            rmttbTaxiTimes.Enabled = true;
                        else
                            rmttbTaxiTimes.Enabled = false;
                        chkShowScheduleDateValidation.Enabled = Enable;
                        chkRemoveEndDutyMarker.Enabled = Enable;
                        chkAutoPopulateLegTimes.Enabled = Enable;
                        tbFlightLogSrchDaysBack.Enabled = Enable;
                        tbSIFLLayoversHrs.Enabled = Enable;
                        tbDefaultBlockedSeat.Enabled = Enable;
                        tbShort3.Enabled = Enable;
                        tbShort4.Enabled = Enable;
                        tbLong3.Enabled = Enable;
                        tbLong4.Enabled = Enable;
                        tbDec1.Enabled = Enable;
                        tbDec2.Enabled = Enable;
                        tbShort1.Enabled = Enable;
                        tbShort2.Enabled = Enable;
                        tbLong1.Enabled = Enable;
                        tbLong2.Enabled = Enable;
                        btndgSave.Visible = Enable;
                        btndgCancel.Visible = Enable;
                        btnSaveChanges.Visible = Enable;
                        btnCancel.Visible = Enable;
                        btnDefaultBlockedSeat.Enabled = Enable;
                        btnDefaultChklistGroup.Enabled = Enable;
                        btnDefaultCrewDutyRules.Enabled = Enable;
                        btnDefaultFlightCategory.Enabled = Enable;
                        btnOther.Enabled = Enable;
                        btnDefaultTripsheetRWGroup.Enabled = Enable;                                   
                        radbtnCQDelete.Enabled = Enable;
                        radbtnCQDelete.Visible = Enable;
                        ddlImg.Enabled = Enable;      
                         
                      
                       
                        if (hdnSaveFlag.Value == "Save" && Enable == true)
                        {
                            btnSubmittoUWA.Enabled = false;
                        }
                        else
                        {
                            btnSubmittoUWA.Enabled = Enable;
                        }
                        if ((hdnSaveFlag.Value == "Update" || hdnSaveFlag.Value == "Save") && (Enable == true))
                        {
                            chkSearchActiveOnly.Enabled = false;
                        }
                        else
                        {
                            chkSearchActiveOnly.Enabled = true;
                        }
                        if (chkAutoRON.Checked == true && Enable == true)
                        {

                            chkAutoDateAdjustment.Enabled = true;
                            tbMinRONHrs.Enabled = true;
                        }
                        else
                        {

                            chkAutoDateAdjustment.Enabled = false;
                            tbMinRONHrs.Enabled = false;

                        }
                        if (chkAutoDateAdjustment.Checked == true && Enable == true)
                        {
                            chkWarningMessage.Enabled = true;
                        }
                        else
                        {

                            chkWarningMessage.Enabled = false;

                        }
                        chkAutoRON.Enabled = Enable;
                        chkDeactivateAutoUpdatingRates.Enabled = Enable;
                        chkTaxDiscountedTotal.Enabled = Enable;
                        chkTailNumHomebase.Enabled = Enable;
                        tbCharterDepartment.Enabled = Enable;
                        tbPax.Enabled = Enable;
                        tbNoPax.Enabled = Enable;
                        radAllLegs.Enabled = Enable;
                        radDomesticleg.Enabled = Enable;
                        radNone.Enabled = Enable;
                        radTaxes.Enabled = Enable;
                        radUSSegmentFees.Enabled = Enable;
                        if ((radTaxes.Checked == true && radUSSegmentFees.Checked == false) || (radTaxes.Checked == false && radUSSegmentFees.Checked == false))
                        {
                            radTaxes.Checked = true;
                            PanelTaxes.Visible = true;
                            pnlUSTaxesForm.Visible = false;
                        }
                        else
                        {
                            radUSSegmentFees.Checked = true;
                            PanelTaxes.Visible = false;
                            pnlUSTaxesForm.Visible = true;
                        }
                        tbTaxRate1.Enabled = Enable;
                        tbFedAccNum1.Enabled = Enable;
                        tbTaxRate2.Enabled = Enable;
                        tbFedAccNum2.Enabled = Enable;
                        tbFee1.Enabled = Enable;
                        tbSegFeeAccNum1.Enabled = Enable;
                        tbFee2.Enabled = Enable;
                        tbSegFeeAccNum2.Enabled = Enable;
                        tbFee3.Enabled = Enable;
                        tbSegFeeAccNum3.Enabled = Enable;
                        tbFee4.Enabled = Enable;
                        tbSegFeeAccNum4.Enabled = Enable;
                        //tbMinRONHrs.Enabled = Enable;
                        tbSearchDaysBack.Enabled = Enable;
                        tbCQDefaultCrewDutyRules.Enabled = Enable;
                        tbCQimgName.Enabled = Enable;
                        ddlCQFileName.Enabled = Enable;
                        fuCQ.Enabled = Enable;
                        radbtnCQDelete.Enabled = Enable;
                        btnCqCrewDutyRules.Enabled = Enable;
                        btnSegFeeAccNum4.Enabled = Enable;
                        btnSegFeeAccNum3.Enabled = Enable;
                        btnSegFeeAccNum2.Enabled = Enable;
                        btnSegFeeAccNum1.Enabled = Enable;
                        btnAccNum2.Enabled = Enable;
                        btnAccNum1.Enabled = Enable;
                        btnNoPax.Enabled = Enable;
                        btnPax.Enabled = Enable;
                        btnCharterDepartment.Enabled = Enable;
                        RdlstCqPosition.Enabled = Enable;
                        lbCharterDeptInfo.Enabled = false;
                        lbPaxInfo.Enabled = false;
                        lbNoPaxInfo.Enabled = false;
                        if (Enable == false)
                        {
                            lnkbtnQuoteInfo.ToolTip = "Click Edit To Display";
                            lnkInvoiceInfo.ToolTip = "Click Edit To Display";
                            lnkbtnCustomPilotLog.ToolTip = "Click Edit To Display";
                            lnkbtncrew.ToolTip = "Click Edit To Display";
                            lnkCustomFlightLog.ToolTip = "Click Edit To Display";
                            lnkCrewCurrency.ToolTip = "Click Edit To Display";

                        }
                        else
                        {
                            lnkbtnQuoteInfo.ToolTip = "Display Default Quote Information";
                            lnkInvoiceInfo.ToolTip = "Display Default Invoice Information";
                            lnkbtnCustomPilotLog.ToolTip = "Display Custom Pilot Log Information";
                            lnkbtncrew.ToolTip = "Access Crew Currency Selections";
                            lnkCustomFlightLog.ToolTip = "Display Custom Flight Log Information";
                            lnkCrewCurrency.ToolTip = "Trip Privacy Setting";

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To check already exist of Mainbase
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyMainBaseExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbMainBase.Text != string.Empty) && (tbMainBase.Text != null))
                    {
                        if (CheckIcaoExist())
                        {
                            cvMainBaseCheck.IsValid = false;
                            lbMainBase1.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            RadAjaxManager1.FocusControl(tbMainBase.ClientID);
                            //tbMainBase.Focus();
                        }
                        else
                        {
                            //List<GetAllCompanyMaster> CompanyList = new List<GetAllCompanyMaster>();
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var CompanyList = Service.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD != null && x.IsDeleted == false && x.HomebaseCD.ToUpper().Trim() == (tbMainBase.Text.ToString().ToUpper().Trim())).ToList();
                                // CompanyList = ((List<GetAllCompanyMaster>)Session["Homeval"]).Where(x => x.HomebaseCD != null && x.isdeleted == false && x.HomebaseCD.ToUpper().Trim() == (tbMainBase.Text.ToString().ToUpper().Trim())).ToList<GetAllCompanyMaster>();
                                if (CompanyList.Count() > 0)
                                {
                                    cvMainBase.IsValid = false;
                                    RadAjaxManager1.FocusControl(tbMainBase.ClientID);
                                    //tbMainBase.Focus();
                                    RetVal = true;
                                }
                                else
                                {
                                    TabCompanyProfileCatalog.SelectedIndex = 0;
                                    RadPageView1.Selected = true;
                                    var RetValue = Service.GetAirportByAirportICaoID(tbMainBase.Text).EntityList;
                                    if (RetValue.Count() != 0)
                                    {
                                        if (!string.IsNullOrEmpty(RetValue[0].AirportName))
                                        {
                                            lbMainBase1.Text = System.Web.HttpUtility.HtmlEncode(RetValue[0].AirportName.ToString());
                                            hdnHomeBaseID.Value = RetValue[0].AirportID.ToString();
                                            tbMainBase.Text = RetValue[0].IcaoID;
                                        }
                                    }
                                    RadAjaxManager1.FocusControl(tbBaseDescription.ClientID);
                                    //tbBaseDescription.Focus();
                                }
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        /// <summary>
        /// To check icao
        /// </summary>
        /// <returns></returns>
        private bool CheckIcaoExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbMainBase.Text != string.Empty) && (tbMainBase.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetAirportByAirportICaoID(tbMainBase.Text).EntityList;
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                RetVal = true;
                            }
                            else
                            {
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        /// <summary>
        /// To Enable and disable buttons of insert,edit and delete
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        if (IsAuthorized(Permission.Database.AddCompanyProfileCatalog))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                            {
                                lbtnInsertCtl.Enabled = true;
                            }
                            else
                            {
                                lbtnInsertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.DeleteCompanyProfileCatalog))
                        {
                            lbtnDelCtl.Visible = true;
                            if (Delete)
                            {
                                lbtnDelCtl.Enabled = true;
                                lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                            }
                            else
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.EditCompanyProfileCatalog))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                            {
                                lbtnEditCtl.Enabled = true;
                                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnEditCtl.Enabled = false;
                                lbtnEditCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Assign Tenth Minth Conversion
        /// </summary>
        /// <param name="CompanyData"></param>
        private void AssignTenthMinConv(FlightPakMasterService.Company CompanyData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["ConversionDefaults"] != null)
                        {
                            Dictionary<string, string> SetConversionDefaultsDict = (Dictionary<string, string>)Session["ConversionDefaults"];
                            for (int i = 0; i <= 9; i++)
                            {
                                FlightPakMasterService.ConversionTable Conversion = new FlightPakMasterService.ConversionTable();
                                Conversion.HomebaseID = Convert.ToInt64(Session["HomeBaseID"].ToString());
                                if (!string.IsNullOrEmpty(SetConversionDefaultsDict["Tenths" + i]))
                                    Conversion.Tenths = Convert.ToDecimal(SetConversionDefaultsDict["Tenths" + i]);
                                if (!string.IsNullOrEmpty(SetConversionDefaultsDict["StartMinimum" + i]))
                                    Conversion.StartMinimum = Convert.ToDecimal(SetConversionDefaultsDict["StartMinimum" + i]);
                                if (!string.IsNullOrEmpty(SetConversionDefaultsDict["EndMinutes" + i]))
                                    Conversion.EndMinutes = Convert.ToDecimal(SetConversionDefaultsDict["EndMinutes" + i]);
                                using (FlightPakMasterService.MasterCatalogServiceClient ConversionService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    if (hdnSaveFlag.Value == "Update")
                                    {
                                        Conversion.IsDeleted = false;
                                        var Conversionvalue = ConversionService.UpdateConversionTable(Conversion);
                                    }
                                    else
                                    {
                                        var Conversionvalue = ConversionService.AddConversionTable(Conversion);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        private void AssignQuoteReport(FlightPakMasterService.Company CompanyData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CqCompanyStringQuoteReport"] != null)
                        {
                            Dictionary<string, string> dictSetQuoteReportDefaultsString = (Dictionary<string, string>)Session["CqCompanyStringQuoteReport"];
                            CompanyData.ChtQuoteCompany = dictSetQuoteReportDefaultsString["ChtQuoteCompany"];
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QuotePageOff"]))
                                CompanyData.QuotePageOff = Convert.ToDecimal(dictSetQuoteReportDefaultsString["QuotePageOff"]);
                            CompanyData.ChtQouteHeaderDetailRpt = dictSetQuoteReportDefaultsString["ChtQouteHeaderDetailRpt"];
                            CompanyData.ChtQouteFooterDetailRpt = dictSetQuoteReportDefaultsString["ChtQouteFooterDetailRpt"];
                            CompanyData.QuoteMinimumUse2FeeDepositAdj = dictSetQuoteReportDefaultsString["QuoteMinimumUse2FeeDepositAdj"];
                            CompanyData.uote2LandingFeeDefault = dictSetQuoteReportDefaultsString["uote2LandingFeeDefault"];
                            CompanyData.QouteStdCrew2RONDeposit = dictSetQuoteReportDefaultsString["QouteStdCrew2RONDeposit"];
                            CompanyData.QuoteAddition2CrewRON = dictSetQuoteReportDefaultsString["QuoteAddition2CrewRON"];
                            CompanyData.QuoteWaiting2Charge = dictSetQuoteReportDefaultsString["QuoteWaiting2Charge"];
                            CompanyData.Deposit2FlightCharge = dictSetQuoteReportDefaultsString["Deposit2FlightCharge"];
                            CompanyData.Discount2AmountDeposit = dictSetQuoteReportDefaultsString["Discount2AmountDeposit"];
                            CompanyData.QuoteSegment2FeeDeposit = dictSetQuoteReportDefaultsString["QuoteSegment2FeeDeposit"];
                            CompanyData.QuoteAddition2CrewDeposit = dictSetQuoteReportDefaultsString["QuoteAddition2CrewDeposit"];
                            CompanyData.Quote2Subtotal = dictSetQuoteReportDefaultsString["Quote2Subtotal"];
                            CompanyData.Quote2TaxesDefault = dictSetQuoteReportDefaultsString["Quote2TaxesDefault"];
                            CompanyData.QuoteInvoice2Default = dictSetQuoteReportDefaultsString["QuoteInvoice2Default"];
                            CompanyData.QuotePrepay2Default = dictSetQuoteReportDefaultsString["QuotePrepay2Default"];
                            CompanyData.QuoteRemaining2AmountDefault = dictSetQuoteReportDefaultsString["QuoteRemaining2AmountDefault"];
                            CompanyData.Additional2FeeDefault = dictSetQuoteReportDefaultsString["Additional2FeeDefault"];
                            CompanyData.QuoteTitle = dictSetQuoteReportDefaultsString["QuoteTitle"];
                        }
                        if (Session["CqCompanyCheckQuoteReport"] != null)
                        {
                            Dictionary<string, bool> dictSetQuoteReportDefaultsCheck = (Dictionary<string, bool>)Session["CqCompanyCheckQuoteReport"];
                            CompanyData.IsChangeQueueCity = dictSetQuoteReportDefaultsCheck["IsChangeQueueCity"];
                            CompanyData.IsChangeQueueAirport = dictSetQuoteReportDefaultsCheck["IsChangeQueueAirport"];
                            CompanyData.IsQuoteChangeQueueICAO = dictSetQuoteReportDefaultsCheck["IsQuoteChangeQueueICAO"];
                            CompanyData.IsQuoteFirstPage = dictSetQuoteReportDefaultsCheck["IsQuoteFirstPage"];
                            // CompanyData.ImageCnt = Convert.ToInt32(dictSetQuoteReportDefaultsCheck["ImageCnt"]);
                            CompanyData.IsChangeQueueDetail = dictSetQuoteReportDefaultsCheck["IsChangeQueueDetail"];
                            CompanyData.IsQuoteChangeQueueSubtotal = dictSetQuoteReportDefaultsCheck["IsQuoteChangeQueueSubtotal"];
                            CompanyData.IsQuoteChangeQueueFees = dictSetQuoteReportDefaultsCheck["IsQuoteChangeQueueFees"];
                            CompanyData.IsQuoteAdditional2FeePrint = dictSetQuoteReportDefaultsCheck["IsQuoteAdditional2FeePrint"];
                            CompanyData.IsQuoteUnpaidFlight2Chg = dictSetQuoteReportDefaultsCheck["IsQuoteUnpaidFlight2Chg"];
                            CompanyData.IsQuoteChangeQueueSum = dictSetQuoteReportDefaultsCheck["IsQuoteChangeQueueSum"];
                            CompanyData.IsQuotePrepaidMinimumUsage2FeeAdj = dictSetQuoteReportDefaultsCheck["IsQuotePrepaidMinimumUsage2FeeAdj"];
                            CompanyData.IsQuotePrepaidMin2UsageFeeAdj = dictSetQuoteReportDefaultsCheck["IsQuotePrepaidMin2UsageFeeAdj"];
                            CompanyData.IsQuoteFlight2LandingFeePrint = dictSetQuoteReportDefaultsCheck["IsQuoteFlight2LandingFeePrint"];
                            CompanyData.IsQuotePrepaidSegement2Fee = dictSetQuoteReportDefaultsCheck["IsQuotePrepaidSegement2Fee"];
                            CompanyData.IsQuoteStdCrew2RONPrepaid = dictSetQuoteReportDefaultsCheck["IsQuoteStdCrew2RONPrepaid"];
                            CompanyData.IsQuoteStdCrew2RONPrepaid = dictSetQuoteReportDefaultsCheck["IsQuoteStdCrew2RONPrepaid"];
                            CompanyData.IsQuoteAdditional2CrewRON = dictSetQuoteReportDefaultsCheck["IsQuoteAdditional2CrewRON"];
                            CompanyData.IsQuoteUpaidAdditionl2CrewRON = dictSetQuoteReportDefaultsCheck["IsQuoteUpaidAdditionl2CrewRON"];
                            CompanyData.IsQuoteWaiting2ChargePrepaid = dictSetQuoteReportDefaultsCheck["IsQuoteWaiting2ChargePrepaid"];
                            CompanyData.IsQuoteUpaid2WaitingChg = dictSetQuoteReportDefaultsCheck["IsQuoteUpaid2WaitingChg"];
                            CompanyData.IsQuoteFlight2ChargePrepaid = dictSetQuoteReportDefaultsCheck["IsQuoteFlight2ChargePrepaid"];
                            CompanyData.IsQuoteDiscount2AmountPrepaid = dictSetQuoteReportDefaultsCheck["IsQuoteDiscount2AmountPrepaid"];
                            CompanyData.IsQuoteUnpaid2DiscountAmt = dictSetQuoteReportDefaultsCheck["IsQuoteUnpaid2DiscountAmt"];
                            CompanyData.IsQuoteUnpaidStd2CrewRON = dictSetQuoteReportDefaultsCheck["IsQuoteUnpaidStd2CrewRON"];
                            CompanyData.IsQuoteAdditional2CrewPrepaid = dictSetQuoteReportDefaultsCheck["IsQuoteAdditional2CrewPrepaid"];
                            CompanyData.IsQuoteUnpaidAdditional2Crew = dictSetQuoteReportDefaultsCheck["IsQuoteUnpaidAdditional2Crew"];
                            CompanyData.IsQuoteSubtotal2Print = dictSetQuoteReportDefaultsCheck["IsQuoteSubtotal2Print"];
                            CompanyData.IsQuoteTaxes2Print = dictSetQuoteReportDefaultsCheck["IsQuoteTaxes2Print"];
                            CompanyData.IsQuoteInvoice2Print = dictSetQuoteReportDefaultsCheck["IsQuoteInvoice2Print"];
                            CompanyData.IsQuote2PrepayPrint = dictSetQuoteReportDefaultsCheck["IsQuote2PrepayPrint"];
                            CompanyData.IsQuoteRemaining2AmtPrint = dictSetQuoteReportDefaultsCheck["IsQuoteRemaining2AmtPrint"];
                            CompanyData.IsRptQuoteDetailLegNum = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailLegNum"];
                            CompanyData.IsRptQuoteDetailDepartureDT = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailDepartureDT"];
                            CompanyData.IsRptQuoteDetailFromDescription = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailFromDescription"];
                            CompanyData.IsRptQuoteDetailToDescription = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailToDescription"];
                            CompanyData.IsRptQuoteDetailQuotedAmt = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailQuotedAmt"];
                            CompanyData.IsRptQuoteDetailFlightHours = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailFlightHours"];
                            CompanyData.IsRptQuoteDetailMiles = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailMiles"];
                            CompanyData.IsRptQuoteDetailPassengerCnt = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailPassengerCnt"];
                            CompanyData.IsRptQuoteFeeDescription = dictSetQuoteReportDefaultsCheck["IsRptQuoteFeeDescription"];
                            CompanyData.IsRptQuoteFeeQuoteAmt = dictSetQuoteReportDefaultsCheck["IsRptQuoteFeeQuoteAmt"];
                            CompanyData.IsCharterQuoteFee3 = dictSetQuoteReportDefaultsCheck["IsCharterQuoteFee3"];
                            CompanyData.IsCharterQuoteFee4 = dictSetQuoteReportDefaultsCheck["IsCharterQuoteFee4"];
                            CompanyData.IsCharterQuote5 = dictSetQuoteReportDefaultsCheck["IsCharterQuote5"];
                            CompanyData.IsQuoteInformation2 = dictSetQuoteReportDefaultsCheck["IsQuoteInformation2"];
                            CompanyData.IsCompanyName2 = dictSetQuoteReportDefaultsCheck["IsCompanyName2"];
                            CompanyData.QuoteDescription = dictSetQuoteReportDefaultsCheck["QuoteDescription"];
                            CompanyData.IsQuoteSales = dictSetQuoteReportDefaultsCheck["IsQuoteSales"];
                            CompanyData.IsQuoteTailNum = dictSetQuoteReportDefaultsCheck["IsQuoteTailNum"];
                            CompanyData.IsQuoteTypeCD = dictSetQuoteReportDefaultsCheck["IsQuoteTypeCD"];
                            CompanyData.IsQuoteEdit = dictSetQuoteReportDefaultsCheck["IsQuoteEdit"];
                            CompanyData.IsRptQuoteDetailArrivalDate = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailArrivalDate"];
                            CompanyData.IsRptQuoteDetailQuoteDate = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailQuoteDate"];
                        }
                        if (Session["CqCompanyRadQuoteReport"] != null)
                        {
                            Dictionary<string, int> dictSetQuoteReportDefaultsRad = (Dictionary<string, int>)Session["CqCompanyRadQuoteReport"];
                            CompanyData.QuoteLogoPosition = dictSetQuoteReportDefaultsRad["QuoteLogoPosition"];
                            CompanyData.ImagePosition = dictSetQuoteReportDefaultsRad["ImagePosition"];
                            //CompanyData.QuoteSalesLogoPosition = dictSetQuoteReportDefaultsRad["QuoteSalesLogoPosition"];
                            
                            CompanyData.ImageCnt = dictSetQuoteReportDefaultsRad["ImageCnt"];
                            CompanyData.QuoteIDTYPE = Convert.ToDecimal(dictSetQuoteReportDefaultsRad["QuoteIDTYPE"]);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        private void AssignInvoiceReport(FlightPakMasterService.Company CompanyData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CqCompanyStringInvoiceReport"] != null)
                        {
                            Dictionary<string, string> dictSetInvoiceReportDefaultsString = (Dictionary<string, string>)Session["CqCompanyStringInvoiceReport"];
                            CompanyData.ChtQuoteCompanyNameINV = dictSetInvoiceReportDefaultsString["ChtQuoteCompanyNameINV"];
                            if (!string.IsNullOrEmpty(dictSetInvoiceReportDefaultsString["QuotePageOffsettingInvoice"]))
                                CompanyData.QuotePageOffsettingInvoice = Convert.ToDecimal(dictSetInvoiceReportDefaultsString["QuotePageOffsettingInvoice"]);
                            CompanyData.ChtQuoteHeaderINV = dictSetInvoiceReportDefaultsString["ChtQuoteHeaderINV"];
                            //dictSetInvoiceReportDefaultsString.Add("ChtQouteFooterDetailRpt", tbCustomFooterInfo.Text);
                            CompanyData.QuoteMinimumUseFeeDepositAmt = dictSetInvoiceReportDefaultsString["QuoteMinimumUseFeeDepositAmt"];
                            CompanyData.QuoteAdditionalFeeDefault = dictSetInvoiceReportDefaultsString["QuoteAdditionalFeeDefault"];
                            CompanyData.QuoteLandingFeeDefault = dictSetInvoiceReportDefaultsString["QuoteLandingFeeDefault"];
                            CompanyData.QuoteSegmentFeeDeposit = dictSetInvoiceReportDefaultsString["QuoteSegmentFeeDeposit"];
                            CompanyData.QuoteStdCrewRONDeposit = dictSetInvoiceReportDefaultsString["QuoteStdCrewRONDeposit"];
                            CompanyData.QuoteAdditonCrewDeposit = dictSetInvoiceReportDefaultsString["QuoteAdditonCrewDeposit"];
                            CompanyData.QuoteAdditionCrewRON = dictSetInvoiceReportDefaultsString["QuoteAdditionCrewRON"];
                            CompanyData.QuoteWaitingCharge = dictSetInvoiceReportDefaultsString["QuoteWaitingCharge"];
                            CompanyData.DepostFlightCharge = dictSetInvoiceReportDefaultsString["DepostFlightCharge"];
                            //dictSetInvoiceReportDefaultsString.Add("DepostFlightCharge", lbFlightDesc.Text);
                            CompanyData.QuoteSubtotal = dictSetInvoiceReportDefaultsString["QuoteSubtotal"];
                            CompanyData.DiscountAmountDefault = dictSetInvoiceReportDefaultsString["DiscountAmountDefault"];
                            CompanyData.QuoteTaxesDefault = dictSetInvoiceReportDefaultsString["QuoteTaxesDefault"];
                            CompanyData.DefaultInvoice = dictSetInvoiceReportDefaultsString["DefaultInvoice"];
                            CompanyData.QuotePrepayDefault = dictSetInvoiceReportDefaultsString["QuotePrepayDefault"];
                            CompanyData.QuoteRemainingAmountDefault = dictSetInvoiceReportDefaultsString["QuoteRemainingAmountDefault"];
                            CompanyData.InvoiceTitle = dictSetInvoiceReportDefaultsString["InvoiceTitle"];

                        }
                        if (Session["CqCompanyCheckInvoiceReport"] != null)
                        {
                            Dictionary<string, bool> dictSetInvoiceReportDefaultsCheck = (Dictionary<string, bool>)Session["CqCompanyCheckInvoiceReport"];
                            CompanyData.IsCityDescription = dictSetInvoiceReportDefaultsCheck["IsCityDescription"];
                            CompanyData.IsAirportDescription = dictSetInvoiceReportDefaultsCheck["IsAirportDescription"];
                            CompanyData.IsQuoteICAODescription = dictSetInvoiceReportDefaultsCheck["IsQuoteICAODescription"];
                            CompanyData.IFirstPG = dictSetInvoiceReportDefaultsCheck["IFirstPG"];
                            CompanyData.IsInvoiceDTsystemDT = dictSetInvoiceReportDefaultsCheck["IsInvoiceDTsystemDT"];
                            CompanyData.isQuotePrintFees = dictSetInvoiceReportDefaultsCheck["isQuotePrintFees"];
                            CompanyData.IsQuotePrintSubtotal = dictSetInvoiceReportDefaultsCheck["IsQuotePrintSubtotal"];
                            CompanyData.IsQuoteInvoicePrint = dictSetInvoiceReportDefaultsCheck["IsQuoteInvoicePrint"];
                            CompanyData.IsQuoteStdCrewRonPrepaid = dictSetInvoiceReportDefaultsCheck["IsQuoteStdCrewRonPrepaid"];
                            CompanyData.IsQuotePrintAdditionalFee = dictSetInvoiceReportDefaultsCheck["IsQuotePrintAdditionalFee"];
                            CompanyData.IsQuotePrintSum = dictSetInvoiceReportDefaultsCheck["IsQuotePrintSum"];
                            CompanyData.IsQuoteDetailPrint = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailPrint"];
                            CompanyData.IsQuotePrepaidMinimUsageFeeAmt = dictSetInvoiceReportDefaultsCheck["IsQuotePrepaidMinimUsageFeeAmt"];
                            CompanyData.IsQuoteFlightLandingFeePrint = dictSetInvoiceReportDefaultsCheck["IsQuoteFlightLandingFeePrint"];
                            CompanyData.IsQuotePrepaidSegementFee = dictSetInvoiceReportDefaultsCheck["IsQuotePrepaidSegementFee"];
                            CompanyData.IsQuoteAdditionalCrewPrepaid = dictSetInvoiceReportDefaultsCheck["IsQuoteAdditionalCrewPrepaid"];
                            CompanyData.IsQuoteAdditonalCrewRON = dictSetInvoiceReportDefaultsCheck["IsQuoteAdditonalCrewRON"];
                            CompanyData.IsQuoteWaitingChargePrepaid = dictSetInvoiceReportDefaultsCheck["IsQuoteWaitingChargePrepaid"];
                            CompanyData.IsQuoteFlightChargePrepaid = dictSetInvoiceReportDefaultsCheck["IsQuoteFlightChargePrepaid"];
                            CompanyData.IsQuoteSubtotalPrint = dictSetInvoiceReportDefaultsCheck["IsQuoteSubtotalPrint"];
                            CompanyData.IsQuoteDispcountAmtPrint = dictSetInvoiceReportDefaultsCheck["IsQuoteDispcountAmtPrint"];
                            CompanyData.IsQuoteTaxesPrint = dictSetInvoiceReportDefaultsCheck["IsQuoteTaxesPrint"];
                            // CompanyData.IsQuoteInvoicePrint = dictSetInvoiceReportDefaultsCheck["IsQuoteInvoicePrint"];
                            CompanyData.IsQuotePrepayPrint = dictSetInvoiceReportDefaultsCheck["IsQuotePrepayPrint"];
                            CompanyData.IsQuoteRemainingAmtPrint = dictSetInvoiceReportDefaultsCheck["IsQuoteRemainingAmtPrint"];
                            CompanyData.IsInvoiceRptLegNum = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptLegNum"];
                            CompanyData.IsInvoiceRptDetailDT = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptDetailDT"];
                            CompanyData.IsInvoiceRptFromDescription = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptFromDescription"];
                            CompanyData.IsQuoteDetailRptToDescription = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailRptToDescription"];
                            CompanyData.IsQuoteDetailInvoiceAmt = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailInvoiceAmt"];
                            CompanyData.IsQuoteDetailFlightHours = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailFlightHours"];
                            CompanyData.IsQuoteDetailMiles = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailMiles"];
                            CompanyData.IsQuoteDetailPassengerCnt = dictSetInvoiceReportDefaultsCheck["IsQuoteDetailPassengerCnt"];
                            CompanyData.IsQuoteFeelColumnDescription = dictSetInvoiceReportDefaultsCheck["IsQuoteFeelColumnDescription"];
                            CompanyData.IsQuoteFeelColumnInvoiceAmt = dictSetInvoiceReportDefaultsCheck["IsQuoteFeelColumnInvoiceAmt"];
                            CompanyData.IsCOLFee3 = dictSetInvoiceReportDefaultsCheck["IsCOLFee3"];
                            CompanyData.IsCOLFee4 = dictSetInvoiceReportDefaultsCheck["IsCOLFee4"];
                            CompanyData.IsCOLFee5 = dictSetInvoiceReportDefaultsCheck["IsCOLFee5"];
                            CompanyData.IsQuoteInformation = dictSetInvoiceReportDefaultsCheck["IsQuoteInformation"];
                            CompanyData.IsCompanyName = dictSetInvoiceReportDefaultsCheck["IsCompanyName"];
                            CompanyData.IsQuoteDispatch = dictSetInvoiceReportDefaultsCheck["IsQuoteDispatch"];
                            CompanyData.IsInvoiceRptArrivalDate = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptArrivalDate"];
                            CompanyData.IsInvoiceRptQuoteDate = dictSetInvoiceReportDefaultsCheck["IsInvoiceRptQuoteDate"];
                            CompanyData.IsTailNumber = dictSetInvoiceReportDefaultsCheck["IsTailNumber"];
                            CompanyData.IsAircraftTypeCode = dictSetInvoiceReportDefaultsCheck["IsAircraftTypeCode"];
                        }

                        if (Session["CqCompanyRadInvoiceReport"] != null)
                        {
                            Dictionary<string, int> dictSetQuoteInvoiceDefaultsRad = (Dictionary<string, int>)Session["CqCompanyRadInvoiceReport"];
                            CompanyData.IlogoPOS = dictSetQuoteInvoiceDefaultsRad["IlogoPOS"];
                            //CompanyData.InvoiceSalesLogoPosition = dictSetQuoteInvoiceDefaultsRad["InvoiceSalesLogoPosition"];

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Crew Currency Details
        /// </summary>
        /// <param name="CompanyData"></param>
        private void AssignCrewCurrencyDefaults(FlightPakMasterService.Company CompanyData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CrewCurrencyDefaults"] != null)
                        {
                            Dictionary<string, string> SetCrewCurrencyDefaultsDict = (Dictionary<string, string>)Session["CrewCurrencyDefaults"];
                            CompanyData.DayLanding = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["DLDayQueried"]);
                            CompanyData.DayLandingMinimum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["DLMin"]);
                            CompanyData.NightLanding = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["NLDayQueried"]);
                            CompanyData.NightllandingMinimum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["NLMinimum"]);
                            CompanyData.Approach = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["ApproachesDaysQueried"]);
                            CompanyData.Instrument = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["InstrumentDaysQueried"]);
                            CompanyData.ApproachMinimum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["ApproachesMin"]);
                            CompanyData.InstrumentMinimum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["InstrumentMin"]);
                            CompanyData.TakeoffDay = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["TdDq"]);
                            CompanyData.TakeoffDayMinimum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["TdMin"]);
                            CompanyData.TakeoffNight = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["TnDq"]);
                            CompanyData.TakeofNightMinimum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["TnMin"]);
                            CompanyData.Day7 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["DaysDQ"]);
                            CompanyData.DayMaximum7 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["DaysMax"]);
                            CompanyData.Day30 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["Days2DQ"]);
                            CompanyData.DayMaximum30 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["Days2Max"]);
                            CompanyData.CalendarMonthMaximum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CmDaysMax"]);
                            CompanyData.Day90 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CmDaysDq"]);
                            CompanyData.DayMaximum90 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CmDaysMaximum"]);
                            CompanyData.CalendarQTRMaximum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CqMax"]);
                            CompanyData.Month6 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CqDq"]);
                            CompanyData.MonthMaximum6 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CqMaximum"]);
                            CompanyData.Previous2QTRMaximum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["PqMax"]);
                            CompanyData.Month12 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["PqDq"]);
                            CompanyData.MonthMaximum12 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["PqMaximum"]);
                            CompanyData.CalendarYearMaximum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CyMax"]);
                            CompanyData.Day365 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CyDaysDq"]);
                            CompanyData.DayMaximum365 = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CyDaysMaximum"]);
                            CompanyData.RestDays = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CyDayRestDq"]);
                            CompanyData.RestDaysMinimum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CyDayRestMin"]);
                            CompanyData.RestCalendarQTRMinimum = Convert.ToDecimal(SetCrewCurrencyDefaultsDict["CqrMin"]);
                        }
                        else
                        {
                            CompanyData.DayLanding = 90;
                            CompanyData.DayLandingMinimum = 3;
                            CompanyData.NightLanding = 90;
                            CompanyData.NightllandingMinimum = 3;
                            CompanyData.Approach = 180;
                            CompanyData.Instrument = 180;
                            CompanyData.ApproachMinimum = 6;
                            CompanyData.InstrumentMinimum = 6;
                            CompanyData.TakeoffDay = 90;
                            CompanyData.TakeoffDayMinimum = 3;
                            CompanyData.TakeoffNight = 90;
                            CompanyData.TakeofNightMinimum = 3;
                            CompanyData.Day7 = 7;
                            CompanyData.DayMaximum7 = Convert.ToDecimal(034.000);
                            CompanyData.Day30 = 30;
                            CompanyData.DayMaximum30 = Convert.ToDecimal(120.000);
                            CompanyData.CalendarMonthMaximum = Convert.ToDecimal(120.000);
                            CompanyData.Day90 = 90;
                            CompanyData.DayMaximum90 = Convert.ToDecimal(300.000);
                            CompanyData.CalendarQTRMaximum = Convert.ToDecimal(500.000);
                            CompanyData.Month6 = 6;
                            CompanyData.MonthMaximum6 = Convert.ToDecimal(800.000);
                            CompanyData.Previous2QTRMaximum = Convert.ToDecimal(800.000);
                            CompanyData.Month12 = 12;
                            CompanyData.MonthMaximum12 = Convert.ToDecimal(1400.00);
                            CompanyData.CalendarYearMaximum = Convert.ToDecimal(1400.00);
                            CompanyData.Day365 = 365;
                            CompanyData.DayMaximum365 = Convert.ToDecimal(1400.00);
                            CompanyData.RestDays = 7;
                            CompanyData.RestDaysMinimum = 24;
                            CompanyData.RestCalendarQTRMinimum = 13;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Save Trip privacy Setting
        /// </summary>
        private void SaveTripPrivacySetting()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        System.Data.DataTable dt = (System.Data.DataTable)Session["dgTripPrivacy"];
                        System.Data.DataRow dr;
                        //dt.Columns.Add(new System.Data.DataColumn("Description", typeof(String)));
                        //dt.Columns.Add(new System.Data.DataColumn("Display", typeof(Boolean)));
                        foreach (DataRow row in dt.Rows)
                        {
                            PrivateTrip += row[0] + "," + row[1] + "\n";
                        }
                        PrivateTrip = PrivateTrip.Remove(PrivateTrip.LastIndexOf("\n"));
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Get the values of controls 
        /// </summary>
        /// <returns></returns>
        private FlightPakMasterService.Company GetItems()
        {
            FlightPakMasterService.Company CompanyItems = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CompanyItems = new FlightPakMasterService.Company();
                        if (hdnSaveFlag.Value == "Update")
                        {
                            string Code = "";
                            if (Session["HomeBaseID"] != null)
                            {
                                Code = Session["HomeBaseID"].ToString();
                            }
                            string HomeBaseID = "";
                            foreach (GridDataItem Item in dgCompanyProfileCatalog.MasterTableView.Items)
                            {
                                if (Item["HomeBaseID"].Text.Trim() == Code.Trim())
                                {
                                    HomeBaseID = Item["HomeBaseID"].Text.Trim();
                                    break;
                                }
                            }
                            CompanyItems.HomebaseID = Convert.ToInt64(HomeBaseID);
                        }
                        //CompanyItems.HomebaseCD = tbMainBase.Text;
                        if (hdnHomeBaseID.Value != null)
                        {
                            CompanyItems.HomebaseAirportID = Convert.ToInt64(hdnHomeBaseID.Value);
                        }
                        CompanyItems.BaseDescription = tbBaseDescription.Text;
                        CompanyItems.CompanyName = tbCompanyName.Text;
                        //CompanyItems.IsDisplayNoteFlag = chkNoteDisplay.Checked; //*2483
                        CompanyItems.IsAutomaticDispatchNum = chkAutoDispatchNumber.Checked;
                        CompanyItems.IsInActive = chkInactive.Checked;
                        CompanyItems.IsConflictCheck = chkConflictChecking.Checked;
                        //CompanyItems.IsTextAutoTab = chkTextAutotab.Checked; //*2483
                        CompanyItems.IsCanPass = chkCanpasspermit.Checked;
                        if (radlstMilesKilo.SelectedValue != null)
                        {
                            if (radlstMilesKilo.SelectedValue == "1")
                            {
                                CompanyItems.IsKilometer = true;
                            }
                            else
                            {
                                CompanyItems.IsKilometer = false;
                            }
                            if (radlstMilesKilo.SelectedValue == "0")
                            {
                                CompanyItems.IsMiles = true;
                            }
                            else
                            {
                                CompanyItems.IsMiles = false;
                            }
                            //CompanyItems.IsMiles = true;    //added temporarily as default
                        }
                        if (rbMonthDay.Checked == true)
                        {
                            CompanyItems.DayMonthFormat = 2;
                        }
                        else
                        {
                            CompanyItems.DayMonthFormat = 1;
                        }
                        if (ddlDefaultDateFormat.SelectedValue != null)
                        {
                            CompanyItems.ApplicationDateFormat = ddlDefaultDateFormat.SelectedValue;
                        }
                        CompanyItems.HomebasePhoneNUM = tbPhone.Text;
                        CompanyItems.BaseFax = tbBaseFax.Text;
                        CompanyItems.CanPassNum = tbCanpassPermit.Text;
                        CompanyItems.AccountFormat = tbAccountFormat.Text;
                        CompanyItems.CorpAccountFormat = tbCorpAcctFormat.Text;
                        CompanyItems.FederalTax = tbFedTaxAccount.Text;
                        CompanyItems.StateTax = tbStateTaxAccount.Text;
                        CompanyItems.SaleTax = tbSalesTaxAccount.Text;
                        CompanyItems.CurrencySymbol = tbCurrency.Text;
                        //generalnotes
                        CompanyItems.ApplicationMessage = tbgeneralnotes.Text;
                        //APIS           
                        CompanyItems.IsAPISSupport = chkEnableAPISInterface.Checked;
                        CompanyItems.IsAPISAlert = chkAlertToresubmitTrips.Checked;
                        CompanyItems.APISUrl = tbURLAddress.Text;
                        CompanyItems.Domain = tbDomain.Text;
                        //Encrypt and decrypt logic implemented for Password and Id
                        CompanyItems.UserNameCBP = tbSenderID.Text;
                        CompanyItems.UserPasswordCBP = tbCBPUserPassword.Text;
                        //preflight
                        CompanyItems.IsDeactivateHomeBaseFilter = chkDeactiveHomeBaseFilter.Checked;
                        CompanyItems.IsAutomaticCalcLegTimes = chkAutoCalcLegTimes.Checked;
                        CompanyItems.IsChecklistWarning = chkUseChecklistWarnings.Checked;
                        CompanyItems.IsChecklistAssign = chkChklistItemsToNewCrewMembers.Checked;
                        CompanyItems.IsLogoUpload = chkDoNotUpdateFuelBrand.Checked;
                        CompanyItems.IsDepartAuthDuringReqSelection = chkAssignAuthDuringRequestorSelection.Checked;
                        CompanyItems.IsNonLogTripSheetIncludeCrewHIST = chkIncludeNonLoggedTripsheetsinCrewHistory.Checked;
                        //CompanyItems.IsAutoValidateTripManager = chkAutoValidateTripManager.Checked;
                        CompanyItems.IsAutoCreateCrewAvailInfo = chkAutoCreateCrewAvailabilityInfo.Checked;
                        CompanyItems.IsReqOnly = chkShowRequestorsOnly.Checked;
                        if (Session["dgTripPrivacy"] != null)
                        {
                            SaveTripPrivacySetting();
                            CompanyItems.PrivateTrip = PrivateTrip;
                            Session.Remove("dgTripPrivacy");
                        }
                        CompanyItems.IsAutoDispat = chkAutoPopulateDispatcher.Checked;
                        CompanyItems.IsAutoRevisionNum = chkAutoRevisionNumber.Checked;
                        CompanyItems.IsANotes = chkHighlightAircraftHavingNotes.Checked;
                        CompanyItems.IsCNotes = chkHighlightCrewtHavingNotes.Checked;
                        CompanyItems.IsPNotes = chkHighlightPassengerHavingNotes.Checked;
                        CompanyItems.IsOpenHistory = chkOpenHistoryOnEditingTrip.Checked;
                        CompanyItems.IsDefaultTripDepartureDate = chkDefaultTripDepartureDate.Checked;
                        CompanyItems.IsCrewQaulifiedFAR = chkCrewQualificationLegFAR.Checked;
                        CompanyItems.IsCrewOlap = chk24hrTripOverlapforCrew.Checked;
                        CompanyItems.IsPassengerOlap = chkPassengerOverlap.Checked;
                        CompanyItems.IsTailInsuranceDue = chkTailInspectionDue.Checked;
                        CompanyItems.IsAutoCrew = chkAutoCrewcode.Checked;
                        CompanyItems.IsAutoPassenger = chkAutoPaxcode.Checked;
                        //if (!string.IsNullOrEmpty(tbFltCallTimer.Text))
                        //{
                        //    CompanyItems.FleetCalendar5DayTimer = Convert.ToDecimal(tbFltCallTimer.Text, CultureInfo.CurrentCulture);
                        //}
                        if (!string.IsNullOrEmpty(tbDomestic.Text))
                        {
                            if (radlstTimeDisplay.SelectedValue == "2" && !string.IsNullOrEmpty(tbDomesticTime.Text))
                            {
                                tbDomestic.Text = ConvertTimeToDecimal(tbDomesticTime.Text);
                            }
                            CompanyItems.GroundTM = Convert.ToDecimal(tbDomestic.Text, CultureInfo.CurrentCulture);
                        }
                        if (!string.IsNullOrEmpty(tbIntl.Text))
                        {
                            if (radlstTimeDisplay.SelectedValue == "2" && !string.IsNullOrEmpty(tbIntlTime.Text))
                            {
                                tbIntl.Text = ConvertTimeToDecimal(tbIntlTime.Text);
                            }
                            CompanyItems.GroundTMIntl = Convert.ToDecimal(tbIntl.Text, CultureInfo.CurrentCulture);
                        }
                        if (tbDefaultCrewDutyRules.Text.Trim() != "")
                        {
                            CompanyItems.CrewDutyID = Convert.ToInt64(hdnCrewDutyRulesID.Value);
                        }
                        if (!string.IsNullOrEmpty(tbTripManagerSrchDaysBack.Text))
                        {
                            CompanyItems.TripsheetSearchBack = Convert.ToInt32(tbTripManagerSrchDaysBack.Text, CultureInfo.CurrentCulture);
                        }
                        //CompanyItems.BusinessWeekStart = Convert.ToDecimal(dpBusinessWeekStart.SelectedItem.Value, CultureInfo.CurrentCulture);
                        if (!string.IsNullOrEmpty(tbCrewChecklistAlertDays.Text))
                        {
                            CompanyItems.CrewChecklistAlertDays = Convert.ToDecimal(tbCrewChecklistAlertDays.Text, CultureInfo.CurrentCulture);
                        }
                        if (!string.IsNullOrEmpty(tbTripManagerDateWarning.Text))
                        {
                            CompanyItems.TripsheetDTWarning = Convert.ToInt32(tbTripManagerDateWarning.Text, CultureInfo.CurrentCulture);
                        }
                        if (hdnFlightCategoryID.Value != "" && !string.IsNullOrEmpty(tbDefaultFlightCategory.Text))
                        {
                            CompanyItems.DefaultFlightCatID = Convert.ToInt64(hdnFlightCategoryID.Value);
                        }
                        if (hdnExchangeRateID.Value != "")
                        {
                            CompanyItems.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                        }
                        if (hdnCheckGroupID.Value != "" && !string.IsNullOrEmpty(tbDefaultChklistGroup.Text))
                        {
                            CompanyItems.DefaultCheckListGroupID = Convert.ToInt64(hdnCheckGroupID.Value);
                        }
                        if (!string.IsNullOrEmpty(tbTripManagerStatus.Text))
                        {
                            CompanyItems.TripMGRDefaultStatus = tbTripManagerStatus.Text;
                        }
                        if (!string.IsNullOrEmpty(tbDefaultTripsheetRWGroup.Text))
                        {
                            CompanyItems.TripsheetRptWriteGroup = tbDefaultTripsheetRWGroup.Text;
                        }
                        if (!string.IsNullOrEmpty(tbGADeskId.Text))
                        {
                            CompanyItems.GeneralAviationDesk = tbGADeskId.Text;
                        }
                        //Commented as this is not valid anymore in new application
                        //CompanyItems.Tsense = tbTravelSenseLocation.Text;
                        if (radlstTimeDisplay.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstTimeDisplay.SelectedItem.Value))
                            {
                                string TimeDisplay = radlstTimeDisplay.SelectedItem.Value.ToString();
                                CompanyItems.TimeDisplayTenMin = Convert.ToDecimal(TimeDisplay);
                            }
                        }
                        if (radlstBoeingWinds.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstBoeingWinds.SelectedItem.Value))
                            {
                                string WindReliability = radlstBoeingWinds.SelectedItem.Value.ToString();
                                CompanyItems.WindReliability = Convert.ToInt32(WindReliability);
                            }
                        }
                        if (radlstDefaultFAR.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstDefaultFAR.SelectedItem.Value))
                            {
                                string FedAviation = radlstDefaultFAR.SelectedItem.Value.ToString();
                                CompanyItems.FedAviationRegNum = FedAviation;
                            }
                        }
                        if (radlstETERound.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstETERound.SelectedItem.Value))
                            {
                                string ElapseTmroundind = radlstETERound.SelectedItem.Value.ToString();
                                CompanyItems.ElapseTMRounding = Convert.ToInt32(ElapseTmroundind);
                            }
                        }
                        if (radlstLogoPosition.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstLogoPosition.SelectedItem.Value))
                            {
                                string Trips = radlstLogoPosition.SelectedItem.Value.ToString();
                                CompanyItems.TripS = Convert.ToInt32(Trips);
                            }
                        }
                        if (!string.IsNullOrEmpty(tbOutboundInst1.Text))
                        {
                            CompanyItems.RptTabOutbdInstLab1 = tbOutboundInst1.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOutboundInst2.Text))
                        {
                            CompanyItems.RptTabOutbdInstLab2 = tbOutboundInst2.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOutboundInst3.Text))
                        {
                            CompanyItems.RptTabOutbdInstLab3 = tbOutboundInst3.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOutboundInst4.Text))
                        {
                            CompanyItems.RptTabOutbdInstLab4 = tbOutboundInst4.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOutboundInst5.Text))
                        {
                            CompanyItems.RptTabOutbdInstLab5 = tbOutboundInst5.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOutboundInst6.Text))
                        {
                            CompanyItems.RptTabOutbdInstLab6 = tbOutboundInst6.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOutboundInst7.Text))
                        {
                            CompanyItems.RptTabOutbdInstLab7 = tbOutboundInst7.Text;
                        }
                        //Reports
                        CompanyItems.IsTimeStampRpt = chkTimeStampReports.Checked;
                        if (!string.IsNullOrEmpty(tbReportMessage.Text))
                        {
                            CompanyItems.CustomRptMessage = tbReportMessage.Text;
                        } CompanyItems.IsZeroSuppressActivityAircftRpt = chkAircraft.Checked;
                        CompanyItems.IsPrintUWAReports = chkPrintUWAReports.Checked;
                        CompanyItems.IsZeroSuppressActivityDeptRpt = chkDept.Checked;
                        CompanyItems.IsZeroSuppressActivityCrewRpt = chkCrew.Checked;
                        CompanyItems.IsZeroSuppressActivityPassengerRpt = chkPax.Checked;
                       
                        //DataBase
                        var item = radlstPaxNameElements.FindItemByText("FIRST NAME");
                        CompanyItems.PassengerOFullName = item.Index + 1;
                        var itema = radlstPaxNameElements.FindItemByText("LAST NAME");
                        CompanyItems.PassengerOLast = itema.Index + 1;
                        var itemb = radlstPaxNameElements.FindItemByText("MIDDLE INITIAL");
                        CompanyItems.PassengerOMiddle = itemb.Index + 1;
                        var itemc = radlstCrewNameElement.FindItemByText("FIRST NAME");
                        CompanyItems.CrewOFullName = itemc.Index + 1;
                        var itemd = radlstCrewNameElement.FindItemByText("LAST NAME");
                        CompanyItems.CrewOLast = itemd.Index + 1;
                        var iteme = radlstCrewNameElement.FindItemByText("MIDDLE INITIAL");
                        CompanyItems.CrewOMiddle = iteme.Index + 1;
                        if (!string.IsNullOrEmpty(tbFirstPaxCharacter.Text))
                        {
                            CompanyItems.PassengerOLastFirst = Convert.ToInt32(tbFirstPaxCharacter.Text);
                        }
                        if (!string.IsNullOrEmpty(tbInitialPaxCharacter.Text))
                        {
                            CompanyItems.PassengerOLastMiddle = Convert.ToInt32(tbInitialPaxCharacter.Text);
                        }
                        if (!string.IsNullOrEmpty(tbLastPaxCharacter.Text))
                        {
                            CompanyItems.PassengerOLastLast = Convert.ToInt32(tbLastPaxCharacter.Text);
                        }
                        for (int Index = 0; Index <= 2; Index++)
                        {
                            if (Index == 0)
                            {
                                if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                {
                                    CompanyItems.PassengerOLastFirst = Convert.ToInt32(tbFirstPaxCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                {
                                    CompanyItems.PassengerOLastMiddle = Convert.ToInt32(tbFirstPaxCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                {
                                    CompanyItems.PassengerOLastLast = Convert.ToInt32(tbFirstPaxCharacter.Text);
                                }
                            }
                            if (Index == 1)
                            {
                                if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                {
                                    CompanyItems.PassengerOLastFirst = Convert.ToInt32(tbInitialPaxCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                {
                                    CompanyItems.PassengerOLastMiddle = Convert.ToInt32(tbInitialPaxCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                {
                                    CompanyItems.PassengerOLastLast = Convert.ToInt32(tbInitialPaxCharacter.Text);
                                }
                            }
                            if (Index == 2)
                            {
                                if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                {
                                    CompanyItems.PassengerOLastFirst = Convert.ToInt32(tbLastPaxCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                {
                                    CompanyItems.PassengerOLastMiddle = Convert.ToInt32(tbLastPaxCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                {
                                    CompanyItems.PassengerOLastLast = Convert.ToInt32(tbLastPaxCharacter.Text);
                                }
                            }
                        }
                        CompanyItems.CrewOLastFirst = Convert.ToInt32(tbFirstCrewCharacter.Text);
                        CompanyItems.CrewOLastMiddle = Convert.ToInt32(tbMiddleCrewCharacter.Text);
                        CompanyItems.CrewOLastLast = Convert.ToInt32(tbLastCrewCharacter.Text);
                        for (int Index = 0; Index <= 2; Index++)
                        {
                            if (Index == 0)
                            {
                                if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                {
                                    CompanyItems.CrewOLastFirst = Convert.ToInt32(tbFirstCrewCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                {
                                    CompanyItems.CrewOLastMiddle = Convert.ToInt32(tbFirstCrewCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                {
                                    CompanyItems.CrewOLastLast = Convert.ToInt32(tbFirstCrewCharacter.Text);
                                }
                            }
                            if (Index == 1)
                            {
                                if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                {
                                    CompanyItems.CrewOLastFirst = Convert.ToInt32(tbMiddleCrewCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                {
                                    CompanyItems.CrewOLastMiddle = Convert.ToInt32(tbMiddleCrewCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                {
                                    CompanyItems.CrewOLastLast = Convert.ToInt32(tbMiddleCrewCharacter.Text);
                                }
                            }
                            if (Index == 2)
                            {
                                if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                {
                                    CompanyItems.CrewOLastFirst = Convert.ToInt32(tbLastCrewCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                {
                                    CompanyItems.CrewOLastMiddle = Convert.ToInt32(tbLastCrewCharacter.Text);
                                }
                                if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                {
                                    CompanyItems.CrewOLastLast = Convert.ToInt32(tbLastCrewCharacter.Text);
                                }
                            }
                        }
                        CompanyItems.IsEnableTSA = chkEnableTSA.Checked;
                        CompanyItems.IsEnableTSAPX = chkEnableTSAPAX.Checked;
                        decimal? PasswordValidDays = Convert.ToDecimal("3");
                        if (M6.Checked)
                        {
                            PasswordValidDays = Convert.ToDecimal("6");
                        }
                        else if(Y1.Checked)
                        {
                            PasswordValidDays = Convert.ToDecimal("12");
                        }
                        CompanyItems.PasswordValidDays = PasswordValidDays;
                        //string PasswordHistoryNum = radtbPasswordRemembered.Text.ToString();
                        //if (!string.IsNullOrEmpty(PasswordHistoryNum))
                        //{
                        //    CompanyItems.PasswordHistoryNum = Convert.ToDecimal(PasswordHistoryNum);
                        //}
                        //else
                        //{
                        //    CompanyItems.PasswordHistoryNum = 0;
                        //}
                        if (!string.IsNullOrEmpty(tbAutoClosureTime.Text))
                        {
                            CompanyItems.AutoClimbTime = tbAutoClosureTime.Text;
                        }
                        //Postflight
                        if (radlstLogFixedWing.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstLogFixedWing.SelectedItem.Value))
                            {
                                string Logfixed = radlstLogFixedWing.SelectedItem.Value.ToString();
                                if (!string.IsNullOrEmpty(Logfixed))
                                    CompanyItems.LogFixed = Convert.ToInt32(Logfixed);
                            }
                        }
                        if (radlstLogRotaryWing.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstLogRotaryWing.SelectedItem.Value))
                            {
                                string LogRotary = radlstLogRotaryWing.SelectedItem.Value.ToString();
                                if (!string.IsNullOrEmpty(LogRotary))
                                    CompanyItems.LogRotary = Convert.ToInt32(LogRotary);
                            }
                        }
                        if (radlstPostflightCrewDuty.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstPostflightCrewDuty.SelectedItem.Value))
                            {
                                string DutyBasis = radlstPostflightCrewDuty.SelectedItem.Value.ToString();
                                if (!string.IsNullOrEmpty(DutyBasis))
                                    CompanyItems.DutyBasis = Convert.ToInt32(DutyBasis);
                            }
                        }
                        if (radlstPostflightAircraft.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstPostflightAircraft.SelectedItem.Value))
                            {
                                string AircraftBasis = radlstPostflightAircraft.SelectedItem.Value.ToString();
                                if (!string.IsNullOrEmpty(AircraftBasis))
                                    CompanyItems.AircraftBasis = Convert.ToInt32(AircraftBasis);
                            }
                        }
                        if (radlstFuelPurchaseUnits.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstFuelPurchaseUnits.SelectedItem.Value))
                            {
                                string FuelPurchase = radlstFuelPurchaseUnits.SelectedItem.Value.ToString();
                                if (!string.IsNullOrEmpty(FuelPurchase))
                                    CompanyItems.FuelPurchase = Convert.ToInt32(FuelPurchase);
                            }
                        }
                        if (radlstFuelBurn.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstFuelBurn.SelectedItem.Value))
                            {
                                string FuelBurn = radlstFuelBurn.SelectedItem.Value.ToString();
                                if (!string.IsNullOrEmpty(FuelBurn))
                                    CompanyItems.FuelBurnKiloLBSGallon = Convert.ToInt32(FuelBurn);
                            }
                        }
                        if (radlstEngineAirframe.SelectedItem != null)
                        {
                            if (!string.IsNullOrEmpty(radlstEngineAirframe.SelectedItem.Value))
                            {
                                string AircraftBlockFlight = radlstEngineAirframe.SelectedItem.Value.ToString();
                                if (!string.IsNullOrEmpty(AircraftBlockFlight))
                                    CompanyItems.AircraftBlockFlight = Convert.ToInt32(AircraftBlockFlight);
                            }
                        }
                        if (radStandard != null)
                        {
                            if (radStandard.Checked == true)
                            {
                                CompanyItems.HoursMinutesCONV = 1;
                            }
                        }
                        if (radFlightPak != null)
                        {
                            if (radFlightPak.Checked == true)
                            {
                                CompanyItems.HoursMinutesCONV = 2;
                            }
                        }
                        if (radOther != null)
                        {
                            if (radOther.Checked == true)
                            {
                                CompanyItems.HoursMinutesCONV = 3;
                                TenthMin = true;
                                // AssignTenthMinConv(CompanyItems);
                            }
                        }
                        if (ddlEndMonth.SelectedIndex != null)
                        {
                            CompanyItems.FiscalYREnd = Convert.ToDecimal(ddlEndMonth.SelectedIndex);
                        }
                        //if (CompanyItems.FiscalYREnd != null)
                        //{
                        //    CompanyItems.FiscalYREnd = Convert.ToDecimal(ddlEndMonth.Text);
                        //}
                        if (ddlEndMonth.SelectedIndex != null)
                        {
                            CompanyItems.FiscalYRStart = Convert.ToDecimal(ddlStartMonth.SelectedIndex);
                        }
                        if (chkLogBlockedSeats != null)
                            CompanyItems.IsBlockSeats = chkLogBlockedSeats.Checked;
                        ////if (chkIsBlackberrySupport != null)
                        ////    CompanyItems.IsBlackberrySupport = chkIsBlackberrySupport.Checked;
                        if (chkUseLogsheetWarnings != null)
                            CompanyItems.IsLogsheetWarning = chkUseLogsheetWarnings.Checked;
                        if (chkPromptTocalculateSIFL != null)
                            CompanyItems.IsSIFLCalMessage = chkPromptTocalculateSIFL.Checked;
                        if (chkAutoFillEngineAirframeHoursandCycles != null)
                            CompanyItems.IsAutoFillAF = chkAutoFillEngineAirframeHoursandCycles.Checked;
                        if (chkIsPODepartPercentage != null)
                            CompanyItems.IsPODepartPercentage = chkIsPODepartPercentage.Checked;
                        if (chkShowScheduleDateValidation != null)
                            CompanyItems.IsScheduleDTTM = chkShowScheduleDateValidation.Checked;
                        if (chkRemoveEndDutyMarker != null)
                            CompanyItems.IsEndDutyClient = chkRemoveEndDutyMarker.Checked;
                        if (chkAutoPopulateLegTimes != null)
                            CompanyItems.IsAutoPopulated = chkAutoPopulateLegTimes.Checked;
                        if (rmttbTaxiTimes != null)
                            CompanyItems.TaxiTime = rmttbTaxiTimes.TextWithLiterals;
                        if (!string.IsNullOrEmpty(tbFlightLogSrchDaysBack.Text))
                        {
                            CompanyItems.Searchack = Convert.ToInt32(tbFlightLogSrchDaysBack.Text);
                        }
                        if (!string.IsNullOrEmpty(tbSIFLLayoversHrs.Text))
                        {
                            CompanyItems.LayoverHrsSIFL = Convert.ToInt32(tbSIFLLayoversHrs.Text);
                        }
                        if (!string.IsNullOrEmpty(tbDefaultBlockedSeat.Text))
                        {
                            CompanyItems.FlightPurpose = tbDefaultBlockedSeat.Text;
                        }
                        if (!string.IsNullOrEmpty(tbShort3.Text))
                        {
                            CompanyItems.SpecificationShort3 = tbShort3.Text;
                        }
                        if (!string.IsNullOrEmpty(tbShort4.Text))
                        {
                            CompanyItems.SpecificationShort4 = tbShort4.Text;
                        }
                        if (!string.IsNullOrEmpty(tbLong3.Text))
                        {
                            CompanyItems.SpecificationLong3 = tbLong3.Text;
                        }
                        if (!string.IsNullOrEmpty(tbLong4.Text))
                        {
                            CompanyItems.SpecificationLong4 = tbLong4.Text;
                        }
                        if (!string.IsNullOrEmpty(tbDec1.Text))
                        {
                            CompanyItems.Specdec3 = Convert.ToInt32(tbDec1.Text);
                        }
                        if (!string.IsNullOrEmpty(tbDec2.Text))
                        {
                            CompanyItems.Specdec4 = Convert.ToInt32(tbDec2.Text);
                        }
                        if (!string.IsNullOrEmpty(tbShort1.Text))
                        {
                            CompanyItems.CrewLogCustomLBLShort1 = tbShort1.Text;
                        }
                        if (!string.IsNullOrEmpty(tbShort2.Text))
                        {
                            CompanyItems.CrewLogCustomLBLShort2 = tbShort2.Text;
                        }
                        if (!string.IsNullOrEmpty(tbLong1.Text))
                        {
                            CompanyItems.CrewLogCustomLBLLong1 = tbLong1.Text;
                        }
                        if (!string.IsNullOrEmpty(tbLong2.Text))
                        {
                            CompanyItems.CrewLogCustomLBLLong2 = tbLong2.Text;
                        }
                       
                       //Charter quote
                        if (!string.IsNullOrEmpty(tbCharterDepartment.Text.Trim()))
                        {
                            CompanyItems.DepartmentID = Convert.ToInt64(hdnDepartmentID.Value);
                        }
                        if (!string.IsNullOrEmpty(tbPax.Text))
                        {
                            CompanyItems.FlightCatagoryPassenger = tbPax.Text;
                        }
                        if (!string.IsNullOrEmpty(tbNoPax.Text))
                        {
                            CompanyItems.FlightCatagoryPassengerNum = tbNoPax.Text;
                        }
                        CompanyItems.IsQuoteDeactivateAutoUpdateRates = chkDeactivateAutoUpdatingRates.Checked;
                        CompanyItems.IsQuoteTaxDiscount = chkTaxDiscountedTotal.Checked;
                        CompanyItems.IsQuoteTailNum = chkTailNumHomebase.Checked;
                        CompanyItems.IsAutoRON = chkAutoRON.Checked;
                        CompanyItems.IsQuoteWarningMessage = chkWarningMessage.Checked;
                        CompanyItems.IsAutoRollover = chkAutoDateAdjustment.Checked;
                        CompanyItems.MinuteHoursRON = 0;
                        if (!string.IsNullOrEmpty(tbMinRONHrs.Text))
                        {
                            CompanyItems.MinuteHoursRON = Convert.ToDecimal(tbMinRONHrs.Text);
                        }
                        if (!string.IsNullOrEmpty(tbCQDefaultCrewDutyRules.Text))
                        {
                            CompanyItems.CQCrewDuty = tbCQDefaultCrewDutyRules.Text;
                        }
                        if (!string.IsNullOrEmpty(tbSearchDaysBack.Text))
                        {
                            CompanyItems.SearchBack = Convert.ToInt32(tbSearchDaysBack.Text);
                        }
                        if (radAllLegs.Checked == true)
                        {
                            CompanyItems.IsAppliedTax = 1;
                        }
                        else if (radDomesticleg.Checked == true)
                        {
                            CompanyItems.IsAppliedTax = 2;
                        }
                        else
                        {
                            if (radNone.Checked == true)
                                CompanyItems.IsAppliedTax = 3;
                        }
                        if (!string.IsNullOrEmpty(tbTaxRate1.Text))
                        {
                            CompanyItems.CQFederalTax = Convert.ToDecimal(tbTaxRate1.Text);
                        }
                        if (!string.IsNullOrEmpty(tbFedAccNum1.Text))
                        {
                            CompanyItems.FederalACCT = tbFedAccNum1.Text;
                        }
                        if (!string.IsNullOrEmpty(tbTaxRate2.Text))
                        {
                            CompanyItems.RuralTax = Convert.ToDecimal(tbTaxRate2.Text);
                        }
                        if (!string.IsNullOrEmpty(tbFedAccNum2.Text))
                        {
                            CompanyItems.RuralAccountNum = tbFedAccNum2.Text;
                        }
                        if (!string.IsNullOrEmpty(tbFee1.Text))
                        {
                            CompanyItems.ChtQouteDOMSegCHG = Convert.ToDecimal(tbFee1.Text);
                        }
                        if (!string.IsNullOrEmpty(tbSegFeeAccNum1.Text))
                        {
                            CompanyItems.ChtQouteDOMSegCHGACCT = tbSegFeeAccNum1.Text;
                        }
                        if (!string.IsNullOrEmpty(tbFee2.Text))
                        {
                            CompanyItems.ChtQouteIntlSegCHG = Convert.ToDecimal(tbFee2.Text);
                        }
                        if (!string.IsNullOrEmpty(tbSegFeeAccNum2.Text))
                        {
                            CompanyItems.ChtQouteIntlSegCHGACCT = tbSegFeeAccNum2.Text;
                        }
                        if (!string.IsNullOrEmpty(tbFee3.Text))
                        {
                            CompanyItems.SegmentFeeAlaska = Convert.ToDecimal(tbFee3.Text);
                        }
                        if (!string.IsNullOrEmpty(tbSegFeeAccNum3.Text))
                        {
                            CompanyItems.SeqmentAircraftAlaska = tbSegFeeAccNum3.Text;
                        }
                        if (!string.IsNullOrEmpty(tbFee4.Text))
                        {
                            CompanyItems.SegementFeeHawaii = Convert.ToDecimal(tbFee4.Text);
                        }
                        if (!string.IsNullOrEmpty(tbSegFeeAccNum4.Text))
                        {
                            CompanyItems.SegementAircraftHawaii = tbSegFeeAccNum4.Text;
                        }
                        CompanyItems.ReportWriteLogoPosition = RdlstCqPosition.SelectedIndex;
                        if (Session["CQQuoteFooter"] != null)
                        {
                            CompanyItems.ChtQouteFooterDetailRpt = Session["CQQuoteFooter"].ToString();
                        }
                        if (Session["CQInvoiceFooter"] != null)
                        {
                            CompanyItems.ChtQuoteFooterINV = Session["CQInvoiceFooter"].ToString();
                        }
                        //doubt sujitha CQ company logo
                        //pnlExternalForm.Visible = false;
                        AssignCrewCurrencyDefaults(CompanyItems);
                        AssignQuoteReport(CompanyItems);
                        AssignInvoiceReport(CompanyItems);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
            return CompanyItems;
        }
        /// <summary>
        ///  Get the values of controls based on Company id
        /// </summary>
        protected void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string LastUpdUID = string.Empty, LastUpdTS = string.Empty, HomeBaseCD = string.Empty;
                        //tbMainBase.ReadOnly = true;
                        //tbMainBase.BackColor = System.Drawing.Color.LightGray;
                        // hdnSaveFlag.Value = "Update";
                        Int64 Code = 0;
                        if (Session["HomeBaseID"] != null)
                        {
                            Code = Convert.ToInt64(Session["HomeBaseID"].ToString().Trim());
                            hdnHmeBaseId.Value = Code.ToString();
                        }
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(Code).EntityList.ToList();
                            if (CompanyList.Count > 0)
                            {
                                GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                if (CompanyCatalogEntity.LastUpdUID != null)
                                {
                                    LastUpdUID = CompanyCatalogEntity.LastUpdUID.ToString().Trim().Replace("&nbsp;", "");
                                }
                                if (CompanyCatalogEntity.LastUpdTS != null)
                                {
                                    LastUpdTS = CompanyCatalogEntity.LastUpdTS.ToString().Trim().Replace("&nbsp;", "");
                                }
                                if (CompanyCatalogEntity.CrewDutyRuleCD != null)
                                {
                                    tbDefaultCrewDutyRules.Text = CompanyCatalogEntity.CrewDutyRuleCD.ToString().Trim();
                                }
                                else
                                {
                                    tbDefaultCrewDutyRules.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.FlightCatagoryCD != null)
                                {
                                    tbDefaultFlightCategory.Text = CompanyCatalogEntity.FlightCatagoryCD.ToString().Trim();
                                }
                                else
                                {
                                    tbDefaultFlightCategory.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.CrewChecklistGroupCD != null)
                                {
                                    tbDefaultChklistGroup.Text = CompanyCatalogEntity.CrewChecklistGroupCD.ToString().Trim().Replace("&nbsp;", "");
                                }
                                else
                                {
                                    tbDefaultChklistGroup.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.HomebaseAirportID != null)
                                {
                                    hdnHomeBaseID.Value = CompanyCatalogEntity.HomebaseAirportID.ToString().Trim();
                                }
                                if (CompanyCatalogEntity.HomebaseCD != null)
                                {
                                    tbMainBase.Text = CompanyCatalogEntity.HomebaseCD.Trim();
                                }
                                else
                                {
                                    tbMainBase.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.BaseDescription != null)
                                {
                                    tbBaseDescription.Text = CompanyCatalogEntity.BaseDescription.Trim();
                                }
                                else
                                {
                                    tbBaseDescription.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.CompanyName != null)
                                {
                                    tbCompanyName.Text = CompanyCatalogEntity.CompanyName.Trim();
                                }
                                else
                                {
                                    tbCompanyName.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(tbMainBase.Text))
                                {
                                    var ArrivalVal = FPKMasterService.GetAirportByAirportICaoID(tbMainBase.Text.ToUpper().Trim()).EntityList.ToList();
                                    if (ArrivalVal != null && ArrivalVal.Count > 0)
                                    {
                                        if (((FlightPakMasterService.GetAllAirport)ArrivalVal[0]).AirportName != null)
                                        {
                                            lbMainBase1.Text = System.Web.HttpUtility.HtmlEncode(((FlightPakMasterService.GetAllAirport)ArrivalVal[0]).AirportName.ToUpper());
                                        }
                                    }
                                }
                                //if (CompanyCatalogEntity.IsDisplayNoteFlag != null)  //*2483
                                //{
                                //    //chkNoteDisplay.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsDisplayNoteFlag.ToString(), CultureInfo.CurrentCulture);
                                //}
                                if (CompanyCatalogEntity.IsAutomaticDispatchNum != null)
                                {
                                    chkAutoDispatchNumber.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutomaticDispatchNum.ToString(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsInActive != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsInActive.ToString(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsConflictCheck != null)
                                {
                                    chkConflictChecking.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsConflictCheck.ToString(), CultureInfo.CurrentCulture);
                                }
                                //if (CompanyCatalogEntity.IsTextAutoTab != null) //*2483
                                //{
                                //    chkTextAutotab.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsTextAutoTab.ToString(), CultureInfo.CurrentCulture);
                                //}
                                if (CompanyCatalogEntity.IsCanPass != null)
                                {
                                    chkCanpasspermit.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCanPass.ToString(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.HomebasePhoneNUM != null)
                                {
                                    tbPhone.Text = CompanyCatalogEntity.HomebasePhoneNUM.Trim();
                                }
                                else
                                {
                                    tbPhone.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.BaseFax != null)
                                {
                                    tbBaseFax.Text = CompanyCatalogEntity.BaseFax.Trim();
                                }
                                else
                                {
                                    tbBaseFax.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.CanPassNum != null)
                                {
                                    tbCanpassPermit.Text = CompanyCatalogEntity.CanPassNum.Trim();
                                }
                                else
                                {
                                    tbCanpassPermit.Text = string.Empty;
                                }
                                //if (CompanyCatalogEntity.BusinessWeekStart != null)
                                //{
                                //    if (CompanyCatalogEntity.BusinessWeekStart < 8)
                                //    {
                                //        dpBusinessWeekStart.SelectedValue = CompanyCatalogEntity.BusinessWeekStart.ToString();
                                //    }
                                //    else
                                //    {
                                //        dpBusinessWeekStart.SelectedValue = "0";
                                //    }
                                //}
                                //else
                                //{
                                //    dpBusinessWeekStart.SelectedValue = "0";
                                //}
                                if (CompanyCatalogEntity.CrewChecklistAlertDays != null)
                                {
                                    tbCrewChecklistAlertDays.Text = Convert.ToString(CompanyCatalogEntity.CrewChecklistAlertDays);
                                }
                                else
                                {
                                    tbCrewChecklistAlertDays.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.ExchangeRateCD != null)
                                {
                                    tbExchangeRateCode.Text = Convert.ToString(CompanyCatalogEntity.ExchangeRateCD);
                                }
                                else
                                {
                                    tbExchangeRateCode.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.AccountFormat != null)
                                {
                                    tbAccountFormat.Text = CompanyCatalogEntity.AccountFormat.Trim();
                                }
                                else
                                {
                                    tbAccountFormat.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.CorpAccountFormat != null)
                                {
                                    tbCorpAcctFormat.Text = CompanyCatalogEntity.CorpAccountFormat.Trim();
                                }
                                else
                                {
                                    tbCorpAcctFormat.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.FederalTax != null)
                                {
                                    tbFedTaxAccount.Text = CompanyCatalogEntity.FederalTax.Trim();
                                }
                                else
                                {
                                    tbFedTaxAccount.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.StateTax != null)
                                {
                                    tbStateTaxAccount.Text = CompanyCatalogEntity.StateTax.Trim();
                                }
                                else
                                {
                                    tbStateTaxAccount.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.SaleTax != null)
                                {
                                    tbSalesTaxAccount.Text = CompanyCatalogEntity.SaleTax.Trim();
                                }
                                else
                                {
                                    tbSalesTaxAccount.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.CurrencySymbol != null)
                                {
                                    tbCurrency.Text = CompanyCatalogEntity.CurrencySymbol.Trim();
                                }
                                else
                                {
                                    tbCurrency.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.ApplicationMessage != null)
                                {
                                    tbgeneralnotes.Text = CompanyCatalogEntity.ApplicationMessage.Trim();
                                }
                                else
                                {
                                    tbgeneralnotes.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.ApplicationDateFormat != null)
                                {
                                    ddlDefaultDateFormat.SelectedValue = CompanyCatalogEntity.ApplicationDateFormat.Trim();
                                }
                                //Reports
                                if (CompanyCatalogEntity.IsTimeStampRpt != null)
                                {
                                    chkTimeStampReports.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsTimeStampRpt.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsZeroSuppressActivityAircftRpt != null)
                                {
                                    chkAircraft.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsZeroSuppressActivityAircftRpt.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsPrintUWAReports != null)
                                {
                                    chkPrintUWAReports.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsPrintUWAReports.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsZeroSuppressActivityCrewRpt != null)
                                {
                                    chkCrew.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsZeroSuppressActivityCrewRpt.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsZeroSuppressActivityDeptRpt != null)
                                {
                                    chkDept.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsZeroSuppressActivityDeptRpt.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsZeroSuppressActivityPassengerRpt != null)
                                {
                                    chkPax.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsZeroSuppressActivityPassengerRpt.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.CustomRptMessage != null)
                                {
                                    tbReportMessage.Text = CompanyCatalogEntity.CustomRptMessage.Trim();
                                }
                                else
                                {
                                    tbReportMessage.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.DayMonthFormat != null)
                                {
                                    if (CompanyCatalogEntity.DayMonthFormat == 1)
                                    {
                                        rbDayMonth.Checked = true;
                                    }
                                    else
                                    {
                                        rbMonthDay.Checked = true;
                                    }
                                }
                                //APIS
                                if (CompanyCatalogEntity.IsAPISSupport != null)
                                {
                                    chkEnableAPISInterface.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAPISSupport.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.APISUrl != null)
                                {
                                    tbURLAddress.Text = CompanyCatalogEntity.APISUrl.Trim();
                                }
                                if (CompanyCatalogEntity.IsAPISAlert != null)
                                {
                                    chkAlertToresubmitTrips.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAPISAlert.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.Domain != null)
                                {
                                    tbDomain.Text = CompanyCatalogEntity.Domain.Trim();
                                }
                                else
                                {
                                    tbDomain.Text = string.Empty;
                                }
                                //  Encrypt and decrypt implemented for password and id
                                if (CompanyCatalogEntity.UserNameCBP != null)
                                {
                                    tbSenderID.Text = CompanyCatalogEntity.UserNameCBP.Trim();
                                }
                                else
                                {
                                    tbSenderID.Text = string.Empty;
                                }
                                //if (CompanyCatalogEntity.UserPasswordCBP != null)
                                //{

                                //    tbCBPUserPassword.Text = CompanyCatalogEntity.UserPasswordCBP.Trim();
                                //    string encodedConstvalue = Microsoft.Security.Application.Encoder.HtmlEncode(constvalue);
                                //    string encodedPassword = Microsoft.Security.Application.Encoder.HtmlEncode(tbCBPUserPassword.Text);

                                //    tbCBPUserPassword.Attributes.Add(encodedConstvalue, encodedPassword);
                                //}
                                // Preflight
                                if (CompanyCatalogEntity.IsDeactivateHomeBaseFilter != null)
                                {
                                    chkDeactiveHomeBaseFilter.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsDeactivateHomeBaseFilter.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsAutomaticCalcLegTimes != null)
                                {
                                    chkAutoCalcLegTimes.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutomaticCalcLegTimes.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsChecklistWarning != null)
                                {
                                    chkUseChecklistWarnings.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsChecklistWarning.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsChecklistAssign != null)
                                {
                                    chkChklistItemsToNewCrewMembers.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsChecklistAssign.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsLogoUpload != null)
                                {
                                    chkDoNotUpdateFuelBrand.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsLogoUpload.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsAutoCrew != null)
                                {
                                    chkCrewPassportExpiry.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutoCrew.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsAutoPassenger != null)
                                {
                                    chkPaxPassportExpiry.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutoPassenger.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsDepartAuthDuringReqSelection != null)
                                {
                                    chkAssignAuthDuringRequestorSelection.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsDepartAuthDuringReqSelection.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsNonLogTripSheetIncludeCrewHIST != null)
                                {
                                    chkIncludeNonLoggedTripsheetsinCrewHistory.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsNonLogTripSheetIncludeCrewHIST.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                //if (CompanyCatalogEntity.IsAutoValidateTripManager != null)
                                //{
                                //    chkAutoValidateTripManager.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutoValidateTripManager.ToString().Trim(), CultureInfo.CurrentCulture);
                                //}
                                if (CompanyCatalogEntity.IsAutoCreateCrewAvailInfo != null)
                                {
                                    chkAutoCreateCrewAvailabilityInfo.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutoCreateCrewAvailInfo.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsReqOnly != null)
                                {
                                    chkShowRequestorsOnly.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsReqOnly.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsAutoDispat != null)
                                {
                                    chkAutoPopulateDispatcher.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutoDispat.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsAutoRevisionNum != null)
                                {
                                    chkAutoRevisionNumber.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutoRevisionNum.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsANotes != null)
                                {
                                    chkHighlightAircraftHavingNotes.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsANotes.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsCNotes != null)
                                {
                                    chkHighlightCrewtHavingNotes.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCNotes.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsPNotes != null)
                                {
                                    chkHighlightPassengerHavingNotes.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsPNotes.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsOpenHistory != null)
                                {
                                    chkOpenHistoryOnEditingTrip.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsOpenHistory.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsDefaultTripDepartureDate != null)
                                {
                                    chkDefaultTripDepartureDate.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsDefaultTripDepartureDate.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkDefaultTripDepartureDate.Checked = true;
                                }
                                if (CompanyCatalogEntity.IsCrewQaulifiedFAR != null)
                                {
                                    chkCrewQualificationLegFAR.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCrewQaulifiedFAR.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsCrewOlap != null)
                                {
                                    chk24hrTripOverlapforCrew.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCrewOlap.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsPassengerOlap != null)
                                {
                                    chkPassengerOverlap.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsPassengerOlap.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsTailInsuranceDue != null)
                                {
                                    chkTailInspectionDue.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsTailInsuranceDue.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                radlstMilesKilo.ClearSelection();
                                // Commented below codes temporariily for default selection Start
                                //if (CompanyCatalogEntity.IsKilometer != null)
                                //{
                                //    if (CompanyCatalogEntity.IsKilometer.ToString().ToUpper() == "TRUE")
                                //    {
                                //        radlstMilesKilo.SelectedValue = "1";
                                //    }
                                //    //chkKilometers.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsKilometer.ToString().Trim(), CultureInfo.CurrentCulture);
                                //}
                                if (CompanyCatalogEntity.IsMiles != null)
                                {
                                    if (CompanyCatalogEntity.IsMiles.ToString().ToUpper() == "TRUE")
                                    {
                                        radlstMilesKilo.SelectedValue = "0";
                                    }
                                }
                                if (CompanyCatalogEntity.IsKilometer != null)
                                {
                                    if (CompanyCatalogEntity.IsKilometer.ToString().ToUpper() == "TRUE")
                                    {
                                        radlstMilesKilo.SelectedValue = "1";
                                    }
                                }
                                // Commented below codes temporariily for default selection End
                                //if (CompanyCatalogEntity.FleetCalendar5DayTimer != null)
                                //{
                                //    tbFltCallTimer.Text = Convert.ToString(CompanyCatalogEntity.FleetCalendar5DayTimer, CultureInfo.CurrentCulture);
                                //}
                                //else
                                //{
                                //    tbFltCallTimer.Text = string.Empty;
                                //}
                                if (CompanyCatalogEntity.GroundTM != null)
                                {
                                    tbDomestic.Text = Convert.ToString(CompanyCatalogEntity.GroundTM, CultureInfo.CurrentCulture);
                                    tbDomesticTime.Text = TimeSpan.FromHours((double)CompanyCatalogEntity.GroundTM).ToString("h\\:mm");
                                }
                                else
                                {
                                    tbDomestic.Text = string.Empty;
                                    tbDomesticTime.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.GroundTMIntl != null)
                                {
                                    tbIntl.Text = Convert.ToString(CompanyCatalogEntity.GroundTMIntl, CultureInfo.CurrentCulture);
                                    tbIntlTime.Text = TimeSpan.FromHours((double)CompanyCatalogEntity.GroundTMIntl).ToString("h\\:mm");
                                }
                                if (CompanyCatalogEntity.TripsheetSearchBack != null)
                                {
                                    tbTripManagerSrchDaysBack.Text = Convert.ToString(CompanyCatalogEntity.TripsheetSearchBack, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.TripsheetDTWarning != null)
                                {
                                    tbTripManagerDateWarning.Text = Convert.ToString(CompanyCatalogEntity.TripsheetDTWarning, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.TripMGRDefaultStatus != null)
                                {
                                    tbTripManagerStatus.Text = CompanyCatalogEntity.TripMGRDefaultStatus.Trim();
                                }
                                else
                                {
                                    tbTripManagerStatus.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.TripsheetRptWriteGroup != null)
                                {
                                    tbDefaultTripsheetRWGroup.Text = CompanyCatalogEntity.TripsheetRptWriteGroup.Trim();
                                }
                                if (CompanyCatalogEntity.GeneralAviationDesk != null)
                                {
                                    tbGADeskId.Text = CompanyCatalogEntity.GeneralAviationDesk.Trim();
                                }
                                else
                                {
                                    tbGADeskId.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.TimeDisplayTenMin != null)
                                {
                                    radlstTimeDisplay.SelectedValue = Convert.ToString(CompanyCatalogEntity.TimeDisplayTenMin, CultureInfo.CurrentCulture);
                                    if (radlstTimeDisplay.SelectedValue == "2")
                                        IsTimeDisplay = true;

                                    radlstTimeDisplay_CheckedChanged(null, null);
                                }
                                if (CompanyCatalogEntity.WindReliability != null)
                                {
                                    radlstBoeingWinds.SelectedValue = Convert.ToString(CompanyCatalogEntity.WindReliability, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.FedAviationRegNum != null)
                                {
                                    radlstDefaultFAR.Text = CompanyCatalogEntity.FedAviationRegNum.Trim();
                                }
                                if (CompanyCatalogEntity.ElapseTMRounding != null)
                                {
                                    radlstETERound.SelectedValue = Convert.ToString(CompanyCatalogEntity.ElapseTMRounding, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.TripS != null)
                                {
                                    radlstLogoPosition.SelectedValue = Convert.ToString(CompanyCatalogEntity.TripS, CultureInfo.CurrentCulture);
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.RptTabOutbdInstLab1))
                                {
                                    tbOutboundInst1.Text = CompanyCatalogEntity.RptTabOutbdInstLab1.Trim();
                                }
                                else
                                {
                                    tbOutboundInst1.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.RptTabOutbdInstLab2))
                                {
                                    tbOutboundInst2.Text = CompanyCatalogEntity.RptTabOutbdInstLab2.Trim();
                                }
                                else
                                {
                                    tbOutboundInst2.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.RptTabOutbdInstLab3))
                                {
                                    tbOutboundInst3.Text = CompanyCatalogEntity.RptTabOutbdInstLab3.Trim();
                                }
                                else
                                {
                                    tbOutboundInst3.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.RptTabOutbdInstLab4))
                                {
                                    tbOutboundInst4.Text = CompanyCatalogEntity.RptTabOutbdInstLab4.Trim();
                                }
                                else
                                {
                                    tbOutboundInst4.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.RptTabOutbdInstLab5))
                                {
                                    tbOutboundInst5.Text = CompanyCatalogEntity.RptTabOutbdInstLab5.Trim();
                                }
                                else
                                {
                                    tbOutboundInst5.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.RptTabOutbdInstLab6))
                                {
                                    tbOutboundInst6.Text = CompanyCatalogEntity.RptTabOutbdInstLab6.Trim();
                                }
                                else
                                {
                                    tbOutboundInst6.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.RptTabOutbdInstLab7))
                                {
                                    tbOutboundInst7.Text = CompanyCatalogEntity.RptTabOutbdInstLab7.Trim();
                                }
                                else
                                {
                                    tbOutboundInst7.Text = string.Empty;
                                }

                                   if (CompanyCatalogEntity.IsAutoPassenger != null)
 	
                                  {
                                    chkAutoPaxcode.Checked = CompanyCatalogEntity.IsAutoPassenger.Value;                                    
                                  }
                                   else
                                   {
                                       chkAutoPaxcode.Checked = false;
                                   }
                                if (CompanyCatalogEntity.IsAutoCrew != null)
                                {
                                    chkAutoCrewcode.Checked = CompanyCatalogEntity.IsAutoCrew.Value;
                                }
                                else
                                {
                                    chkAutoCrewcode.Checked = false;                                    
                                }

                                EnableDisableAutoPaxCode(chkAutoPaxcode.Checked);

                                EnableDisableAutoCrewCode(chkAutoCrewcode.Checked);
                                 
                                                                                             
                               
                                if ((CompanyCatalogEntity.PassengerOMiddle != null) && (CompanyCatalogEntity.PassengerOLast != null) && (CompanyCatalogEntity.PassengerOFullName != null))
                                {
                                    radlstPaxNameElements.Items.Clear();
                                    RadListBoxItem radLstPaxBx;
                                    for (int pax = 1; pax <= 3; pax++)
                                    {
                                        radLstPaxBx = new RadListBoxItem();
                                        radLstPaxBx.Value = pax.ToString();
                                        if (CompanyCatalogEntity.PassengerOMiddle == pax)
                                        {
                                            radLstPaxBx.Text = "MIDDLE INITIAL";
                                        }
                                        else if (CompanyCatalogEntity.PassengerOLast == pax)
                                        {
                                            radLstPaxBx.Text = "LAST NAME";
                                        }
                                        else if (CompanyCatalogEntity.PassengerOFullName == pax)
                                        {
                                            radLstPaxBx.Text = "FIRST NAME";
                                        }
                                        radlstPaxNameElements.Items.Add(radLstPaxBx);
                                    }
                                }
                                if ((CompanyCatalogEntity.PassengerOLastMiddle != null) && (CompanyCatalogEntity.PassengerOLastMiddle != null) && (CompanyCatalogEntity.PassengerOFullName != null))
                                {
                                    for (int Index = 0; Index <= 2; Index++)
                                    {
                                        if (Index == 0)
                                        {
                                            if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                            {
                                                tbFirstPaxCharacter.Text = CompanyCatalogEntity.PassengerOLastFirst.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                            {
                                                tbFirstPaxCharacter.Text = CompanyCatalogEntity.PassengerOLastMiddle.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                            {
                                                tbFirstPaxCharacter.Text = CompanyCatalogEntity.PassengerOLastLast.ToString();
                                            }
                                        }
                                        if (Index == 1)
                                        {
                                            if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                            {
                                                tbInitialPaxCharacter.Text = CompanyCatalogEntity.PassengerOLastFirst.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                            {
                                                tbInitialPaxCharacter.Text = CompanyCatalogEntity.PassengerOLastMiddle.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                            {
                                                tbInitialPaxCharacter.Text = CompanyCatalogEntity.PassengerOLastLast.ToString();
                                            }
                                        }
                                        if (Index == 2)
                                        {
                                            if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                            {
                                                tbLastPaxCharacter.Text = CompanyCatalogEntity.PassengerOLastFirst.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                            {
                                                tbLastPaxCharacter.Text = CompanyCatalogEntity.PassengerOLastMiddle.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                            {
                                                tbLastPaxCharacter.Text = CompanyCatalogEntity.PassengerOLastLast.ToString();
                                            }
                                        }
                                    }
                                }
                                if ((CompanyCatalogEntity.CrewOFullName != null) && (CompanyCatalogEntity.CrewOMiddle != null) && (CompanyCatalogEntity.CrewOLast != null))
                                {
                                    radlstCrewNameElement.Items.Clear();
                                    RadListBoxItem radlstCrewBx;
                                    for (int crew = 1; crew <= 3; crew++)
                                    {
                                        radlstCrewBx = new RadListBoxItem();
                                        radlstCrewBx.Value = crew.ToString();
                                        if (CompanyCatalogEntity.CrewOFullName == crew)
                                        {
                                            radlstCrewBx.Text = "FIRST NAME";
                                        }
                                        if (CompanyCatalogEntity.CrewOMiddle == crew)
                                        {
                                            radlstCrewBx.Text = "MIDDLE INITIAL";
                                        }
                                        if (CompanyCatalogEntity.CrewOLast == crew)
                                        {
                                            radlstCrewBx.Text = "LAST NAME";
                                        }
                                        radlstCrewNameElement.Items.Add(radlstCrewBx);
                                    }
                                }
                                if ((CompanyCatalogEntity.CrewOLastFirst != null) && (CompanyCatalogEntity.CrewOLastMiddle != null) && (CompanyCatalogEntity.CrewOLastLast != null))
                                {
                                    for (int Index = 0; Index <= 2; Index++)
                                    {
                                        if (Index == 0)
                                        {
                                            if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                            {
                                                tbFirstCrewCharacter.Text = CompanyCatalogEntity.CrewOLastFirst.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                            {
                                                tbFirstCrewCharacter.Text = CompanyCatalogEntity.CrewOLastMiddle.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                            {
                                                tbFirstCrewCharacter.Text = CompanyCatalogEntity.CrewOLastLast.ToString();
                                            }
                                        }
                                        if (Index == 1)
                                        {
                                            if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                            {
                                                tbMiddleCrewCharacter.Text = CompanyCatalogEntity.CrewOLastFirst.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                            {
                                                tbMiddleCrewCharacter.Text = CompanyCatalogEntity.CrewOLastMiddle.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                            {
                                                tbMiddleCrewCharacter.Text = CompanyCatalogEntity.CrewOLastLast.ToString();
                                            }
                                        }
                                        if (Index == 2)
                                        {
                                            if (radlstPaxNameElements.Items[Index].Text == "FIRST NAME")
                                            {
                                                tbLastCrewCharacter.Text = CompanyCatalogEntity.CrewOLastFirst.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "MIDDLE INITIAL")
                                            {
                                                tbLastCrewCharacter.Text = CompanyCatalogEntity.CrewOLastMiddle.ToString();
                                            }
                                            if (radlstPaxNameElements.Items[Index].Text == "LAST NAME")
                                            {
                                                tbLastCrewCharacter.Text = CompanyCatalogEntity.CrewOLastLast.ToString();
                                            }
                                        }
                                    }
                                }
                                if (CompanyCatalogEntity.IsEnableTSA != null)
                                {
                                    chkEnableTSA.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsEnableTSA.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsEnableTSAPX != null)
                                {
                                    chkEnableTSAPAX.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsEnableTSAPX.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.PasswordValidDays != null)
                                {
                                    if (CompanyCatalogEntity.PasswordValidDays == 6)
                                        M6.Checked = true;
                                    else if (CompanyCatalogEntity.PasswordValidDays == 12)
                                        Y1.Checked = true;
                                    else if (CompanyCatalogEntity.PasswordValidDays == 3)
                                        M3.Checked = true;
                                    else
                                    {
                                        M3.Checked = false;
                                        M6.Checked = false;
                                        Y1.Checked = false;
                                    }
                                }
                                else
                                {
                                    M3.Checked = false;
                                    M6.Checked = false;
                                    Y1.Checked = false;
                                }
                                Session["PasswordValidDays"] = CompanyCatalogEntity.PasswordValidDays;

                                if (CompanyCatalogEntity.AutoClimbTime != null)
                                {
                                    tbAutoClosureTime.Text = Convert.ToString(CompanyCatalogEntity.AutoClimbTime, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    tbAutoClosureTime.Text = string.Empty;
                                }

                                //Postflight
                                if (CompanyCatalogEntity.LogFixed != null)
                                {
                                    radlstLogFixedWing.SelectedValue = Convert.ToString(CompanyCatalogEntity.LogFixed, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.LogRotary != null)
                                {
                                    radlstLogRotaryWing.SelectedValue = Convert.ToString(CompanyCatalogEntity.LogRotary, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.DutyBasis != null)
                                {
                                    radlstPostflightCrewDuty.SelectedValue = Convert.ToString(CompanyCatalogEntity.DutyBasis, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.AircraftBasis != null)
                                {
                                    radlstPostflightAircraft.SelectedValue = Convert.ToString(CompanyCatalogEntity.AircraftBasis, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.FuelPurchase != null)
                                {
                                    radlstFuelPurchaseUnits.SelectedValue = Convert.ToString(CompanyCatalogEntity.FuelPurchase, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.FuelBurnKiloLBSGallon != null)
                                {
                                    radlstFuelBurn.SelectedValue = Convert.ToString(CompanyCatalogEntity.FuelBurnKiloLBSGallon, CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.AircraftBlockFlight != null)
                                    if (CompanyCatalogEntity.AircraftBlockFlight != 0)
                                    {
                                        radlstEngineAirframe.SelectedValue = Convert.ToString(CompanyCatalogEntity.AircraftBlockFlight, CultureInfo.CurrentCulture);
                                    }
                                if (CompanyCatalogEntity.IsBlockSeats != null)
                                {
                                    chkLogBlockedSeats.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsBlockSeats.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                //if (CompanyCatalogEntity.IsBlackberrySupport != null)
                                //{
                                //    chkIsBlackberrySupport.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsBlackberrySupport.ToString().Trim(), CultureInfo.CurrentCulture);
                                //}
                                if (CompanyCatalogEntity.IsLogsheetWarning != null)
                                {
                                    chkUseLogsheetWarnings.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsLogsheetWarning.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsSIFLCalMessage != null)
                                {
                                    chkPromptTocalculateSIFL.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsSIFLCalMessage.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsAutoFillAF != null)
                                {
                                    chkAutoFillEngineAirframeHoursandCycles.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutoFillAF.ToString().Trim(), CultureInfo.CurrentCulture);
                                }

                                if (CompanyCatalogEntity.IsPODepartPercentage != null)
                                {
                                    chkIsPODepartPercentage.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsPODepartPercentage.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsScheduleDTTM != null)
                                {
                                    chkShowScheduleDateValidation.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsScheduleDTTM.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsEndDutyClient != null)
                                {
                                    chkRemoveEndDutyMarker.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsEndDutyClient.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.IsAutoPopulated != null)
                                {
                                    chkAutoPopulateLegTimes.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsAutoPopulated.ToString().Trim(), CultureInfo.CurrentCulture);
                                }
                                if (CompanyCatalogEntity.TaxiTime != null)
                                {
                                    rmttbTaxiTimes.Text = CompanyCatalogEntity.TaxiTime.Trim();
                                }
                                if (CompanyCatalogEntity.Searchack != null)
                                {
                                    tbFlightLogSrchDaysBack.Text = Convert.ToString(CompanyCatalogEntity.Searchack);
                                }
                                else
                                {
                                    tbFlightLogSrchDaysBack.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.LayoverHrsSIFL != null)
                                {
                                    tbSIFLLayoversHrs.Text = Convert.ToString(CompanyCatalogEntity.LayoverHrsSIFL);
                                }
                                else
                                {
                                    tbSIFLLayoversHrs.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.SpecificationShort3))
                                {
                                    tbShort3.Text = CompanyCatalogEntity.SpecificationShort3.Trim();
                                }
                                else
                                {
                                    tbShort3.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.SpecificationShort4))
                                {
                                    tbShort4.Text = CompanyCatalogEntity.SpecificationShort4.Trim();
                                }
                                else
                                {
                                    tbShort4.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.SpecificationLong3))
                                {
                                    tbLong3.Text = CompanyCatalogEntity.SpecificationLong3.Trim();
                                }
                                else
                                {
                                    tbLong3.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.SpecificationLong4))
                                {
                                    tbLong4.Text = CompanyCatalogEntity.SpecificationLong4.Trim();
                                }
                                else
                                {
                                    tbLong4.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.CrewLogCustomLBLShort1))
                                {
                                    tbShort1.Text = CompanyCatalogEntity.CrewLogCustomLBLShort1.Trim();
                                }
                                else
                                {
                                    tbShort1.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.CrewLogCustomLBLShort2))
                                {
                                    tbShort2.Text = CompanyCatalogEntity.CrewLogCustomLBLShort2.Trim();
                                }
                                else
                                {
                                    tbShort2.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.CrewLogCustomLBLLong1))
                                {
                                    tbLong1.Text = CompanyCatalogEntity.CrewLogCustomLBLLong1.Trim();
                                }
                                else
                                {
                                    tbLong1.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.CrewLogCustomLBLLong2))
                                {
                                    tbLong2.Text = CompanyCatalogEntity.CrewLogCustomLBLLong2.Trim();
                                }
                                else
                                {
                                    tbLong2.Text = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(CompanyCatalogEntity.FlightPurpose))
                                {
                                    tbDefaultBlockedSeat.Text = CompanyCatalogEntity.FlightPurpose.Trim();
                                }
                                else
                                {
                                    tbDefaultBlockedSeat.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.Specdec3 != null)
                                {
                                    tbDec1.Text = Convert.ToString(CompanyCatalogEntity.Specdec3);
                                }
                                else
                                {
                                    tbDec1.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.Specdec4 != null)
                                {
                                    tbDec2.Text = Convert.ToString(CompanyCatalogEntity.Specdec4);
                                }
                                else
                                {
                                    tbDec2.Text = string.Empty;
                                }
                                if (CompanyCatalogEntity.FiscalYREnd != null)
                                {
                                    ddlEndMonth.SelectedIndex = Convert.ToInt16(CompanyCatalogEntity.FiscalYREnd);
                                }
                                if (CompanyCatalogEntity.FiscalYRStart != null)
                                {
                                    ddlStartMonth.SelectedIndex = Convert.ToInt16(CompanyCatalogEntity.FiscalYRStart);
                                }
                                if (CompanyCatalogEntity.HoursMinutesCONV == 1)
                                {
                                    radStandard.Checked = true;
                                    radFlightPak.Checked = false;
                                    radOther.Checked = false;
                                }
                                else if (CompanyCatalogEntity.HoursMinutesCONV == 2)
                                {
                                    radFlightPak.Checked = true;
                                    radStandard.Checked = false;
                                    radOther.Checked = false;
                                }
                                else if (CompanyCatalogEntity.HoursMinutesCONV == 3)
                                {
                                    radOther.Checked = true;
                                    radStandard.Checked = false;
                                    radFlightPak.Checked = false;
                                }

                                //Charter Quote
                                if (CompanyCatalogEntity.DepartmentID != null)
                                {
                                    hdnDepartmentID.Value = Convert.ToInt64(CompanyCatalogEntity.DepartmentID).ToString();
                                    GetDepartmentCd();
                                }
                                if (CompanyCatalogEntity.FlightCatagoryPassenger != null)
                                {
                                    tbPax.Text = CompanyCatalogEntity.FlightCatagoryPassenger;
                                    CheckAlreadyFlightCategoryPaxExist();
                                }
                                if (CompanyCatalogEntity.FlightCatagoryPassengerNum != null)
                                {
                                    tbNoPax.Text = CompanyCatalogEntity.FlightCatagoryPassengerNum;
                                    CheckAlreadyFlightCategoryNoPaxExist();
                                }
                                if (CompanyCatalogEntity.IsQuoteDeactivateAutoUpdateRates != null)
                                    chkDeactivateAutoUpdatingRates.Checked = (bool)CompanyCatalogEntity.IsQuoteDeactivateAutoUpdateRates;
                                if (CompanyCatalogEntity.IsQuoteDeactivateAutoUpdateRates != null)
                                    chkTaxDiscountedTotal.Checked = (bool)CompanyCatalogEntity.IsQuoteTaxDiscount;
                                if (CompanyCatalogEntity.IsQuoteDeactivateAutoUpdateRates != null)
                                    chkTailNumHomebase.Checked = (bool)CompanyCatalogEntity.IsQuoteDeactivateAutoUpdateRates;
                                if (CompanyCatalogEntity.IsAutoRollover != null)
                                {
                                    chkAutoDateAdjustment.Checked = (bool)CompanyCatalogEntity.IsAutoRollover;
                                }
                                if (CompanyCatalogEntity.IsAutoRON != null)
                                    chkAutoRON.Checked = (bool)CompanyCatalogEntity.IsAutoRON;
                                if (CompanyCatalogEntity.IsQuoteWarningMessage != null)
                                    chkWarningMessage.Checked = (bool)CompanyCatalogEntity.IsQuoteWarningMessage;
                                //doubt sujitha CQ CompanyItems.FlightCatagoryPassengerNum = chkAutoDateAdjustment.Checked;
                                if ((CompanyCatalogEntity.MinuteHoursRON) != null)
                                    tbMinRONHrs.Text = CompanyCatalogEntity.MinuteHoursRON.ToString();
                                else
                                    tbMinRONHrs.Text = "0";
                                if (CompanyCatalogEntity.CQCrewDuty != null)
                                    tbCQDefaultCrewDutyRules.Text = CompanyCatalogEntity.CQCrewDuty.Trim();
                                if (CompanyCatalogEntity.SearchBack != null)
                                    tbSearchDaysBack.Text = CompanyCatalogEntity.SearchBack.ToString();
                                if (CompanyCatalogEntity.IsAppliedTax != null)
                                {
                                    if (CompanyCatalogEntity.IsAppliedTax == 1)
                                    {
                                        radAllLegs.Checked = true;
                                    }
                                    else if (CompanyCatalogEntity.IsAppliedTax == 2)
                                    {
                                        radDomesticleg.Checked = true;
                                    }
                                    else
                                    {
                                        if (CompanyCatalogEntity.IsAppliedTax == 3)
                                        {
                                            radNone.Checked = true;
                                        }
                                    }
                                }
                                
                                if (CompanyCatalogEntity.CQFederalTax != null)
                                {
                                    tbTaxRate1.Text = CompanyCatalogEntity.CQFederalTax.ToString();
                                }
                                

                                if (CompanyCatalogEntity.FederalACCT != null)
                                {
                                    tbFedAccNum1.Text = CompanyCatalogEntity.FederalACCT.ToString();
                                }
                                if (CompanyCatalogEntity.RuralTax != null)
                                {
                                    tbTaxRate2.Text = CompanyCatalogEntity.RuralTax.ToString();
                                }
                                if (CompanyCatalogEntity.RuralAccountNum != null)
                                {
                                    tbFedAccNum2.Text = CompanyCatalogEntity.RuralAccountNum;
                                }
                                if (CompanyCatalogEntity.ChtQouteDOMSegCHG != null)
                                {
                                    tbFee1.Text = CompanyCatalogEntity.ChtQouteDOMSegCHG.ToString();
                                }
                                if (CompanyCatalogEntity.ChtQouteDOMSegCHGACCT != null)
                                {
                                    tbSegFeeAccNum1.Text = CompanyCatalogEntity.ChtQouteDOMSegCHGACCT;
                                }
                                if (CompanyCatalogEntity.ChtQouteIntlSegCHG != null)
                                {
                                    tbFee2.Text = CompanyCatalogEntity.ChtQouteIntlSegCHG.ToString();
                                }
                                if (CompanyCatalogEntity.ChtQouteIntlSegCHGACCT != null)
                                {
                                    tbSegFeeAccNum2.Text = CompanyCatalogEntity.ChtQouteIntlSegCHGACCT.ToString();
                                }
                                if (CompanyCatalogEntity.SegmentFeeAlaska != null)
                                {
                                    tbFee3.Text = CompanyCatalogEntity.SegmentFeeAlaska.ToString();
                                }
                                if (CompanyCatalogEntity.SeqmentAircraftAlaska != null)
                                {
                                    tbSegFeeAccNum3.Text = CompanyCatalogEntity.SeqmentAircraftAlaska.ToString();
                                }
                                if (CompanyCatalogEntity.SegementFeeHawaii != null)
                                {
                                    tbFee4.Text = CompanyCatalogEntity.SegementFeeHawaii.ToString();
                                }
                                if (CompanyCatalogEntity.SegementAircraftHawaii != null)
                                {
                                    tbSegFeeAccNum4.Text = CompanyCatalogEntity.SegementAircraftHawaii.ToString();
                                }
                                //if (CompanyCatalogEntity.ImagePosition != null)
                                //{
                                //    radlstLogoPosition.SelectedIndex = Convert.ToInt32(CompanyCatalogEntity.ImagePosition);
                                //}
                                if (CompanyCatalogEntity.ReportWriteLogoPosition != null)
                                {
                                    RdlstCqPosition.SelectedIndex = Convert.ToInt32(CompanyCatalogEntity.ReportWriteLogoPosition);
                                }
                                if (CompanyCatalogEntity.ChtQouteFooterDetailRpt != null)
                                {
                                    Session["CQQuoteFooter"] = CompanyCatalogEntity.ChtQouteFooterDetailRpt.ToString();
                                }
                                if (CompanyCatalogEntity.ChtQuoteFooterINV != null)
                                {
                                    Session["CQInvoiceFooter"] = CompanyCatalogEntity.ChtQuoteFooterINV.ToString();
                                }

                                //Taxes
                                strToolTipTaxes.AppendLine(string.Format("              Tax Rate    Account No."));
                                strToolTipTaxes.AppendLine(string.Format("Fed/VAT     : {0}         {1}", (CompanyCatalogEntity.CQFederalTax != null ? CompanyCatalogEntity.CQFederalTax.ToString() : "0"),(CompanyCatalogEntity.FederalACCT != null ? CompanyCatalogEntity.FederalACCT.ToString() : "Nil")));
                                strToolTipTaxes.AppendLine(string.Format("Rural       : {0}         {1}", (CompanyCatalogEntity.RuralTax != null ? CompanyCatalogEntity.RuralTax.ToString() : "0"),(CompanyCatalogEntity.RuralAccountNum != null ? CompanyCatalogEntity.RuralAccountNum.ToString() : "Nil")));


                                //Taxes
                                strToolTipUS.AppendLine(string.Format("                Tax Rate    Account No."));
                                strToolTipUS.AppendLine(string.Format("Domestic        : {0}         {1}", (CompanyCatalogEntity.ChtQouteDOMSegCHG != null ? CompanyCatalogEntity.ChtQouteDOMSegCHG.ToString() : "0"), (CompanyCatalogEntity.ChtQouteDOMSegCHGACCT != null ? CompanyCatalogEntity.ChtQouteDOMSegCHGACCT.ToString() : "Nil")));
                                strToolTipUS.AppendLine(string.Format("International     : {0}         {1}", (CompanyCatalogEntity.ChtQouteIntlSegCHG != null ? CompanyCatalogEntity.ChtQouteIntlSegCHG.ToString() : "0"), (CompanyCatalogEntity.ChtQouteIntlSegCHGACCT != null ? CompanyCatalogEntity.ChtQouteIntlSegCHGACCT.ToString() : "Nil")));
                                strToolTipUS.AppendLine(string.Format("Alaska          : {0}         {1}", (CompanyCatalogEntity.SegmentFeeAlaska != null ? CompanyCatalogEntity.SegmentFeeAlaska.ToString() : "0"), (CompanyCatalogEntity.SeqmentAircraftAlaska != null ? CompanyCatalogEntity.SeqmentAircraftAlaska.ToString() : "Nil")));
                                strToolTipUS.AppendLine(string.Format("Hawaii          : {0}         {1}", (CompanyCatalogEntity.SegementFeeHawaii != null ? CompanyCatalogEntity.SegementFeeHawaii.ToString() : "0"), (CompanyCatalogEntity.SegementAircraftHawaii != null ? CompanyCatalogEntity.SegementAircraftHawaii.ToString() : "Nil")));



                                radTaxes.ToolTip = strToolTipTaxes.ToString();
                                radUSSegmentFees.ToolTip = strToolTipUS.ToString();
                                
                                ////U.S Segment Fees
                                //strToolTipUS.AppendLine(string.Format("Domestic Tax Rate    : {0}", (CompanyCatalogEntity.ChtQouteDOMSegCHG != null ? CompanyCatalogEntity.ChtQouteDOMSegCHG.ToString() : "0")));
                                //strToolTipUS.AppendLine(string.Format("Domestic Account No. : {0}", (CompanyCatalogEntity.ChtQouteDOMSegCHGACCT != null ? CompanyCatalogEntity.ChtQouteDOMSegCHGACCT.ToString() : "Nil")));

                                //strToolTipUS.AppendLine(string.Format("Fed/VAT Tax Rate    : {0}", (CompanyCatalogEntity.CQFederalTax != null ? CompanyCatalogEntity.CQFederalTax.ToString() : "0")));
                                //strToolTipUS.AppendLine(string.Format("Fed/VAT Tax Rate    : {0}", (CompanyCatalogEntity.CQFederalTax != null ? CompanyCatalogEntity.CQFederalTax.ToString() : "0")));
                                //strToolTipUS.AppendLine(string.Format("Fed/VAT Account No. : {0}", (CompanyCatalogEntity.FederalACCT != null ? CompanyCatalogEntity.FederalACCT.ToString() : "Nil")));
                                //strToolTipUS.AppendLine(string.Format("Rural Tax Rate    : {0}", (CompanyCatalogEntity.RuralTax != null ? CompanyCatalogEntity.RuralTax.ToString() : "0")));


                                ////Taxes
                                //strToolTipTaxes.AppendLine(string.Format("Fed/VAT Tax Rate    : {0}", (CompanyCatalogEntity.CQFederalTax != null ? CompanyCatalogEntity.CQFederalTax.ToString() : "0")));
                                //strToolTipTaxes.AppendLine(string.Format("Fed/VAT Account No. : {0}", (CompanyCatalogEntity.FederalACCT != null ? CompanyCatalogEntity.FederalACCT.ToString() : "Nil")));
                                //strToolTipTaxes.AppendLine(string.Format("Rural Tax Rate    : {0}", (CompanyCatalogEntity.RuralTax != null ? CompanyCatalogEntity.RuralTax.ToString() : "0")));
                                //strToolTipTaxes.AppendLine(string.Format("Rural Account No. : {0}", (CompanyCatalogEntity.RuralAccountNum != null ? CompanyCatalogEntity.RuralAccountNum.ToString() : "Nil")));

                                ////U.S Segment Fees
                                //strToolTipUS.AppendLine(string.Format("Domestic Tax Rate    : {0}", (CompanyCatalogEntity.ChtQouteDOMSegCHG != null ? CompanyCatalogEntity.ChtQouteDOMSegCHG.ToString() : "0")));
                                //strToolTipUS.AppendLine(string.Format("Domestic Account No. : {0}", (CompanyCatalogEntity.ChtQouteDOMSegCHGACCT != null ? CompanyCatalogEntity.ChtQouteDOMSegCHGACCT.ToString() : "Nil")));

                                //strToolTipUS.AppendLine(string.Format("Fed/VAT Tax Rate    : {0}", (CompanyCatalogEntity.CQFederalTax != null ? CompanyCatalogEntity.CQFederalTax.ToString() : "0")));
                                //strToolTipUS.AppendLine(string.Format("Fed/VAT Tax Rate    : {0}", (CompanyCatalogEntity.CQFederalTax != null ? CompanyCatalogEntity.CQFederalTax.ToString() : "0")));
                                //strToolTipUS.AppendLine(string.Format("Fed/VAT Account No. : {0}", (CompanyCatalogEntity.FederalACCT != null ? CompanyCatalogEntity.FederalACCT.ToString() : "Nil")));
                                //strToolTipUS.AppendLine(string.Format("Rural Tax Rate    : {0}", (CompanyCatalogEntity.RuralTax != null ? CompanyCatalogEntity.RuralTax.ToString() : "0")));
                                









                                #region Get Image from Database
                                CreateDictionayForImgUpload();
                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                ImgPopup.ImageUrl = null;
                                using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var ObjRetImg = ObjImgService.GetFileWarehouseList("Company", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList;
                                    ddlImg.Items.Clear();
                                    ddlImg.Text = "";
                                    int i = 0;
                                    foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                    {
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.UWAFileName);
                                        ddlImg.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                                        dicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                                        if (i == 0)
                                        {
                                            byte[] picture = fwh.UWAFilePath;
                                            //start of modification for image issue
                                            if (hdnBrowserName.Value == "Chrome")
                                            {
                                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                            }
                                            else
                                            {
                                                Session["Base64"] = picture;
                                                Session["Base641"] = picture;
                                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?cd=" + Guid.NewGuid());
                                            }
                                            //end of modification for image issue
                                        }
                                        i = i + 1;
                                    }
                                    if (ddlImg.Items.Count > 0)
                                    {
                                        ddlImg.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                    }
                                }
                                #endregion
                                #region Get CQ Image from Database
                                CreateCqDictionayForImgUpload();
                                imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                imgCQPopup.ImageUrl = null;
                                using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var ObjRetImg = ObjImgService.GetFileWarehouseList("CQCompany", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList;
                                    ddlCQFileName.Items.Clear();
                                    ddlCQFileName.Text = "";
                                    int i = 0;
                                    foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                    {
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CQDicImg"];
                                        string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.UWAFileName);
                                        ddlCQFileName.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                                        dicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                                        if (i == 0)
                                        {
                                            byte[] picture = fwh.UWAFilePath;
                                            //start of modification for image issue
                                            if (hdnBrowserName.Value == "Chrome")
                                            {
                                                imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                            }
                                            else
                                            {
                                                Session["CQBase64"] = picture;
                                                Session["CQBase641"] = picture;
                                                imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?CQBase64=CQBase64&cd=" + Guid.NewGuid());
                                            }
                                            //end of modification for image issue
                                        }
                                        i = i + 1;
                                    }
                                    if (ddlCQFileName.Items.Count > 0)
                                    {
                                        ddlCQFileName.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                    }
                                }
                                #endregion
                                #region "TenthMinConversion"
                                Dictionary<string, string> SetConversionDefaultsDict = new Dictionary<string, string>();
                                var conversionList = FPKMasterService.GetConversionTableList().EntityList.Where(x => x.HomebaseID == Convert.ToInt64(Session["HomeBaseID"].ToString())).ToList();
                                if (conversionList.Count > 0)
                                {
                                    ConversionTable conversionEntity = conversionList[0];
                                    int ii = 0;
                                    foreach (var l in conversionList)
                                    {
                                        if (ii <= 9)
                                        {
                                            SetConversionDefaultsDict["Tenths" + ii] = l.Tenths.ToString();
                                            SetConversionDefaultsDict["StartMinimum" + ii] = l.StartMinimum.ToString();
                                            SetConversionDefaultsDict["EndMinutes" + ii] = l.EndMinutes.ToString();
                                            Session.Add("ConversionDefaults", SetConversionDefaultsDict);
                                            ii++;
                                        }
                                    }
                                }
                                #endregion
                                lbColumnName1.Text = System.Web.HttpUtility.HtmlEncode("Home Base");
                                lbColumnName2.Text = System.Web.HttpUtility.HtmlEncode("Base Description");
                                lbColumnValue1.Text = System.Web.HttpUtility.HtmlEncode(CompanyCatalogEntity.HomebaseCD);
                                lbColumnValue2.Text = System.Web.HttpUtility.HtmlEncode(CompanyCatalogEntity.BaseDescription);
                            }
                            Label lbUser = (Label)dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (!string.IsNullOrEmpty(LastUpdUID))
                            {
                                lbUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + LastUpdUID);
                            }
                            else
                            {
                                lbUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            }
                            if (!string.IsNullOrEmpty(LastUpdTS))
                            {
                                lbUser.Text = System.Web.HttpUtility.HtmlEncode(lbUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(LastUpdTS)));
                            }

                            #region Load Quote and Invoice Default Values

                            LoadQuoteDefaults();

                            LoadInvoiceDefaults();

                            #endregion
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        /// <summary>
        /// Validate Account Number
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyFedTaxAccountExist()
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbFedTaxAccount.Text != string.Empty) && (tbFedTaxAccount.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbFedTaxAccount.Text.ToString().ToUpper().Trim()));
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvFedTaxAccount.IsValid = false;
                            RadAjaxManager1.FocusControl(tbFedTaxAccount.ClientID);
                            // tbFedTaxAccount.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            RadAjaxManager1.FocusControl(tbStateTaxAccount.ClientID);
                            //tbStateTaxAccount.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (_selectLastModified)
                            {
                                dgCompanyProfileCatalog.SelectedIndexes.Clear();
                                PreSelectItem(FPKMstService, false);
                                dgCompanyProfileCatalog.Rebind();
                                SelectItem();
                            }
                        }

                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                lnkTSAProcess.Visible = false;
                                pnlExternalForm.Visible = false;
                                dgCompanyProfileCatalog.Visible = false;
                                if (IsAdd)
                                {
                                    (dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgCompanyProfileCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

                if (Request.QueryString["HomebaseID"] != null)
                {
                    Session["HomebaseID"] = Request.QueryString["HomebaseID"].Trim();
                }

            }
        }
       

        /// <summary>
        /// Method to Load Charter Quote Invoice Report Details into Session
        /// </summary>
        private void LoadInvoiceDefaults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    if (!string.IsNullOrEmpty(hdnHmeBaseId.Value))
                    {
                        var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(Convert.ToInt64(hdnHmeBaseId.Value)).EntityList.ToList();
                        if (CompanyList != null && CompanyList.Count > 0)
                        {
                            GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];

                            Dictionary<string, string> dictSetInvoiceReportDefaultsString = new Dictionary<string, string>();
                            dictSetInvoiceReportDefaultsString.Add("ChtQuoteCompanyNameINV", CompanyCatalogEntity.ChtQuoteCompanyNameINV);
                            dictSetInvoiceReportDefaultsString.Add("InvoiceTitle", CompanyCatalogEntity.InvoiceTitle);
                            if (CompanyCatalogEntity.QuotePageOffsettingInvoice != null)
                                dictSetInvoiceReportDefaultsString.Add("QuotePageOffsettingInvoice", CompanyCatalogEntity.QuotePageOffsettingInvoice.ToString());
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuotePageOffsettingInvoice", "0");

                            dictSetInvoiceReportDefaultsString.Add("ChtQuoteHeaderINV", CompanyCatalogEntity.ChtQuoteHeaderINV);
                            //dictSetInvoiceReportDefaultsString.Add("ChtQouteFooterDetailRpt", CompanyCatalogEntity.ChtQouteFooterDetailRpt);

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteMinimumUseFeeDepositAmt))
                                dictSetInvoiceReportDefaultsString.Add("QuoteMinimumUseFeeDepositAmt", CompanyCatalogEntity.QuoteMinimumUseFeeDepositAmt);
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteMinimumUseFeeDepositAmt", "DAILY USAGE ADJUSTMENT:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAdditionalFeeDefault))
                                dictSetInvoiceReportDefaultsString.Add("QuoteAdditionalFeeDefault", CompanyCatalogEntity.QuoteAdditionalFeeDefault.ToString());
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteAdditionalFeeDefault", "ADDITIONAL FEES:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteLandingFeeDefault))
                                dictSetInvoiceReportDefaultsString.Add("QuoteLandingFeeDefault", CompanyCatalogEntity.QuoteLandingFeeDefault.ToString());
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteLandingFeeDefault", "LANDING FEES:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteSegmentFeeDeposit))
                                dictSetInvoiceReportDefaultsString.Add("QuoteSegmentFeeDeposit", CompanyCatalogEntity.QuoteSegmentFeeDeposit.ToString());
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteSegmentFeeDeposit", "SEGMENT FEES:");

                            if (CompanyCatalogEntity.QuoteStdCrewRONDeposit != null)
                                dictSetInvoiceReportDefaultsString.Add("QuoteStdCrewRONDeposit", CompanyCatalogEntity.QuoteStdCrewRONDeposit.ToString());
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteStdCrewRONDeposit", "RON CREW CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAdditonCrewDeposit))
                                dictSetInvoiceReportDefaultsString.Add("QuoteAdditonCrewDeposit", CompanyCatalogEntity.QuoteAdditonCrewDeposit);
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteAdditonCrewDeposit", "ADDITIONAL CREW CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAdditionCrewRON))
                                dictSetInvoiceReportDefaultsString.Add("QuoteAdditionCrewRON", CompanyCatalogEntity.QuoteAdditionCrewRON);
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteAdditionCrewRON", "ADDITIONAL CREW RON CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteWaitingCharge))
                                dictSetInvoiceReportDefaultsString.Add("QuoteWaitingCharge", CompanyCatalogEntity.QuoteWaitingCharge);
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteWaitingCharge", "WAIT CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.DepostFlightCharge))
                                dictSetInvoiceReportDefaultsString.Add("DepostFlightCharge", CompanyCatalogEntity.DepostFlightCharge);
                            else
                                dictSetInvoiceReportDefaultsString.Add("DepostFlightCharge", "FLIGHT CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteSubtotal))
                                dictSetInvoiceReportDefaultsString.Add("QuoteSubtotal", CompanyCatalogEntity.QuoteSubtotal);
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteSubtotal", "SUBTOTAL:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.DiscountAmountDefault))
                                dictSetInvoiceReportDefaultsString.Add("DiscountAmountDefault", CompanyCatalogEntity.DiscountAmountDefault);
                            else
                                dictSetInvoiceReportDefaultsString.Add("DiscountAmountDefault", "DISCOUNT AMOUNT:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteTaxesDefault))
                                dictSetInvoiceReportDefaultsString.Add("QuoteTaxesDefault", CompanyCatalogEntity.QuoteTaxesDefault);
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteTaxesDefault", "TAXES:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.DefaultInvoice))
                                dictSetInvoiceReportDefaultsString.Add("DefaultInvoice", CompanyCatalogEntity.DefaultInvoice);
                            else
                                dictSetInvoiceReportDefaultsString.Add("DefaultInvoice", "TOTAL INVOICE:");

                            


                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuotePrepayDefault))
                                dictSetInvoiceReportDefaultsString.Add("QuotePrepayDefault", CompanyCatalogEntity.QuotePrepayDefault);
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuotePrepayDefault", "TOTAL PREPAY:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteRemainingAmountDefault))
                                dictSetInvoiceReportDefaultsString.Add("QuoteRemainingAmountDefault", CompanyCatalogEntity.QuoteRemainingAmountDefault);
                            else
                                dictSetInvoiceReportDefaultsString.Add("QuoteRemainingAmountDefault", "TOTAL REMAINING INVOICE:");
                            Session.Add("CqCompanyStringInvoiceReport", dictSetInvoiceReportDefaultsString);

                            Dictionary<string, bool> dictSetInvoiceReportDefaultsCheck = new Dictionary<string, bool>();

                            if (CompanyCatalogEntity.IsCityDescription != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsCityDescription", Convert.ToBoolean(CompanyCatalogEntity.IsCityDescription));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsCityDescription", false);

                            if (CompanyCatalogEntity.IsAirportDescription != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsAirportDescription", Convert.ToBoolean(CompanyCatalogEntity.IsAirportDescription));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsAirportDescription", false);

                            if (CompanyCatalogEntity.IsQuoteICAODescription != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteICAODescription", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteICAODescription));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteICAODescription", false);

                            if (CompanyCatalogEntity.IsInvoiceDTsystemDT != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceDTsystemDT", Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceDTsystemDT));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceDTsystemDT", false);

                            if (CompanyCatalogEntity.IsQuotePrintSubtotal != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrintSubtotal", Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrintSubtotal));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrintSubtotal", false);

                            if (CompanyCatalogEntity.IsQuoteDetailPrint != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailPrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailPrint));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailPrint", false);

                            if (CompanyCatalogEntity.IsQuotePrintSum != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrintSum", Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrintSum));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrintSum", false);

                            if (CompanyCatalogEntity.isQuotePrintFees != null)
                                dictSetInvoiceReportDefaultsCheck.Add("isQuotePrintFees", Convert.ToBoolean(CompanyCatalogEntity.isQuotePrintFees));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("isQuotePrintFees", false);

                            if (CompanyCatalogEntity.IsQuotePrintAdditionalFee != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrintAdditionalFee", Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrintAdditionalFee));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrintAdditionalFee", false);

                            if (CompanyCatalogEntity.IsQuotePrepaidMinimUsageFeeAmt != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrepaidMinimUsageFeeAmt", Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidMinimUsageFeeAmt));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrepaidMinimUsageFeeAmt", false);

                            if (CompanyCatalogEntity.IsQuoteFlightLandingFeePrint != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFlightLandingFeePrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFlightLandingFeePrint));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFlightLandingFeePrint", false);

                            if (CompanyCatalogEntity.IsQuotePrepaidSegementFee != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrepaidSegementFee", Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidSegementFee));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrepaidSegementFee", false);

                            if (CompanyCatalogEntity.IsQuoteAdditionalCrewPrepaid != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteAdditionalCrewPrepaid", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditionalCrewPrepaid));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteAdditionalCrewPrepaid", false);

                            if (CompanyCatalogEntity.IsQuoteStdCrewRonPrepaid != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteStdCrewRonPrepaid", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteStdCrewRonPrepaid));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteStdCrewRonPrepaid", false);

                            if (CompanyCatalogEntity.IsQuoteAdditonalCrewRON != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteAdditonalCrewRON", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditonalCrewRON));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteAdditonalCrewRON", false);

                            if (CompanyCatalogEntity.IsQuoteWaitingChargePrepaid != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteWaitingChargePrepaid", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteWaitingChargePrepaid));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteWaitingChargePrepaid", false);

                            if (CompanyCatalogEntity.IsQuoteFlightChargePrepaid != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFlightChargePrepaid", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFlightChargePrepaid));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFlightChargePrepaid", false);

                            if (CompanyCatalogEntity.IsQuoteSubtotalPrint != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteSubtotalPrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteSubtotalPrint));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteSubtotalPrint", false);

                            if (CompanyCatalogEntity.IsQuoteDispcountAmtPrint != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDispcountAmtPrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDispcountAmtPrint));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDispcountAmtPrint", false);

                            if (CompanyCatalogEntity.IsQuoteTaxesPrint != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteTaxesPrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteTaxesPrint));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteTaxesPrint", false);

                            if (CompanyCatalogEntity.IsQuoteInvoicePrint != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteInvoicePrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteInvoicePrint));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteInvoicePrint", false);

                            if (CompanyCatalogEntity.IsQuotePrepayPrint != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrepayPrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepayPrint));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuotePrepayPrint", false);

                            if (CompanyCatalogEntity.IsQuoteRemainingAmtPrint != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteRemainingAmtPrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteRemainingAmtPrint));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteRemainingAmtPrint", false);

                            if (CompanyCatalogEntity.IsInvoiceRptLegNum != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptLegNum", Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptLegNum));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptLegNum", false);

                            if (CompanyCatalogEntity.IsInvoiceRptDetailDT != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptDetailDT", Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptDetailDT));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptDetailDT", false);

                            if (CompanyCatalogEntity.IsInvoiceRptFromDescription != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptFromDescription", Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptFromDescription));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptFromDescription", false);

                            if (CompanyCatalogEntity.IsQuoteDetailRptToDescription != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailRptToDescription", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailRptToDescription));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailRptToDescription", false);

                            if (CompanyCatalogEntity.IsQuoteDetailInvoiceAmt != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailInvoiceAmt", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailInvoiceAmt));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailInvoiceAmt", false);

                            if (CompanyCatalogEntity.IsQuoteDetailFlightHours != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailFlightHours", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailFlightHours));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailFlightHours", false);

                            if (CompanyCatalogEntity.IsQuoteDetailMiles != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailMiles", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailMiles));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailMiles", false);

                            if (CompanyCatalogEntity.IsQuoteDetailPassengerCnt != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailPassengerCnt", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDetailPassengerCnt));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDetailPassengerCnt", false);

                            if (CompanyCatalogEntity.IsQuoteFeelColumnDescription != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFeelColumnDescription", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFeelColumnDescription));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFeelColumnDescription", false);

                            if (CompanyCatalogEntity.IsQuoteFeelColumnInvoiceAmt != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFeelColumnInvoiceAmt", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFeelColumnInvoiceAmt));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteFeelColumnInvoiceAmt", false);

                            if (CompanyCatalogEntity.IsCOLFee3 != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsCOLFee3", Convert.ToBoolean(CompanyCatalogEntity.IsCOLFee3));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsCOLFee3", false);

                            if (CompanyCatalogEntity.IsCOLFee4 != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsCOLFee4", Convert.ToBoolean(CompanyCatalogEntity.IsCOLFee4));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsCOLFee4", false);

                            if (CompanyCatalogEntity.IsCOLFee5 != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsCOLFee5", Convert.ToBoolean(CompanyCatalogEntity.IsCOLFee5));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsCOLFee5", false);

                            if (CompanyCatalogEntity.IsQuoteInformation != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteInformation", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteInformation));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteInformation", false);

                            if (CompanyCatalogEntity.IsCompanyName != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsCompanyName", Convert.ToBoolean(CompanyCatalogEntity.IsCompanyName));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsCompanyName", false);

                            if (CompanyCatalogEntity.IsQuoteDispatch != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDispatch", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDispatch));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsQuoteDispatch", false);

                            if (CompanyCatalogEntity.IsTailNumber != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsTailNumber", Convert.ToBoolean(CompanyCatalogEntity.IsTailNumber));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsTailNumber", false);

                            if (CompanyCatalogEntity.IsAircraftTypeCode != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsAircraftTypeCode", Convert.ToBoolean(CompanyCatalogEntity.IsAircraftTypeCode));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsAircraftTypeCode", false);

                            if (CompanyCatalogEntity.IsInvoiceRptArrivalDate != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptArrivalDate", Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptArrivalDate));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptArrivalDate", false);

                            if (CompanyCatalogEntity.IsInvoiceRptQuoteDate != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptQuoteDate", Convert.ToBoolean(CompanyCatalogEntity.IsInvoiceRptQuoteDate));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IsInvoiceRptQuoteDate", false);

                            if (CompanyCatalogEntity.IFirstPG != null)
                                dictSetInvoiceReportDefaultsCheck.Add("IFirstPG", Convert.ToBoolean(CompanyCatalogEntity.IFirstPG));
                            else
                                dictSetInvoiceReportDefaultsCheck.Add("IFirstPG", false);

                            Session.Add("CqCompanyCheckInvoiceReport", dictSetInvoiceReportDefaultsCheck);

                            Dictionary<string, int> dictSetQuoteInvoiceDefaultsRad = new Dictionary<string, int>();
                            if (CompanyCatalogEntity.IlogoPOS != null)
                                dictSetQuoteInvoiceDefaultsRad.Add("IlogoPOS", Convert.ToInt32(CompanyCatalogEntity.IlogoPOS));
                            else
                                dictSetQuoteInvoiceDefaultsRad.Add("IlogoPOS", 0);

                            if (CompanyCatalogEntity != null)
                                dictSetQuoteInvoiceDefaultsRad.Add("InvoiceSalesLogoPosition", Convert.ToInt32(CompanyCatalogEntity.InvoiceSalesLogoPosition));
                            else
                                dictSetQuoteInvoiceDefaultsRad.Add("InvoiceSalesLogoPosition", 0);


                            Session.Add("CqCompanyRadInvoiceReport", dictSetQuoteInvoiceDefaultsRad);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method to Load Charter Quote Quote Report Details into Session
        /// </summary>
        private void LoadQuoteDefaults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    if (!string.IsNullOrEmpty(hdnHmeBaseId.Value))
                    {
                        var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(Convert.ToInt64(hdnHmeBaseId.Value)).EntityList.ToList();
                        if (CompanyList != null && CompanyList.Count > 0)
                        {
                            GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];

                            Dictionary<string, string> dictSetQuoteReportDefaultsString = new Dictionary<string, string>();

                            if (CompanyCatalogEntity.ChtQuoteCompany != null)
                                dictSetQuoteReportDefaultsString.Add("ChtQuoteCompany", CompanyCatalogEntity.ChtQuoteCompany);
                            else
                                dictSetQuoteReportDefaultsString.Add("ChtQuoteCompany", string.Empty);

                            if (CompanyCatalogEntity.QuotePageOff != null)
                                dictSetQuoteReportDefaultsString.Add("QuotePageOff", CompanyCatalogEntity.QuotePageOff.ToString());
                            else
                                dictSetQuoteReportDefaultsString.Add("QuotePageOff", string.Empty);

                            if (CompanyCatalogEntity.ChtQouteHeaderDetailRpt != null)
                                dictSetQuoteReportDefaultsString.Add("ChtQouteHeaderDetailRpt", CompanyCatalogEntity.ChtQouteHeaderDetailRpt);
                            else
                                dictSetQuoteReportDefaultsString.Add("ChtQouteHeaderDetailRpt", string.Empty);

                            if (CompanyCatalogEntity.ChtQouteFooterDetailRpt != null)
                                dictSetQuoteReportDefaultsString.Add("ChtQouteFooterDetailRpt", CompanyCatalogEntity.ChtQouteFooterDetailRpt);
                            else
                                dictSetQuoteReportDefaultsString.Add("ChtQouteFooterDetailRpt", string.Empty);

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteMinimumUse2FeeDepositAdj))
                                dictSetQuoteReportDefaultsString.Add("QuoteMinimumUse2FeeDepositAdj", CompanyCatalogEntity.QuoteMinimumUse2FeeDepositAdj.ToString());
                            else
                                dictSetQuoteReportDefaultsString.Add("QuoteMinimumUse2FeeDepositAdj", "DAILY USAGE ADJUSTMENT:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.Additional2FeeDefault))
                                dictSetQuoteReportDefaultsString.Add("Additional2FeeDefault", CompanyCatalogEntity.Additional2FeeDefault);
                            else
                                dictSetQuoteReportDefaultsString.Add("Additional2FeeDefault", "ADDITIONAL FEES:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.uote2LandingFeeDefault))
                                dictSetQuoteReportDefaultsString.Add("uote2LandingFeeDefault", CompanyCatalogEntity.uote2LandingFeeDefault);
                            else
                                dictSetQuoteReportDefaultsString.Add("uote2LandingFeeDefault", "LANDING FEES:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteSegment2FeeDeposit))
                                dictSetQuoteReportDefaultsString.Add("QuoteSegment2FeeDeposit", CompanyCatalogEntity.QuoteSegment2FeeDeposit.ToString());
                            else
                                dictSetQuoteReportDefaultsString.Add("QuoteSegment2FeeDeposit", "SEGMENT FEES:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QouteStdCrew2RONDeposit))
                                dictSetQuoteReportDefaultsString.Add("QouteStdCrew2RONDeposit", CompanyCatalogEntity.QouteStdCrew2RONDeposit);
                            else
                                dictSetQuoteReportDefaultsString.Add("QouteStdCrew2RONDeposit", "RON CREW CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAddition2CrewDeposit))
                                dictSetQuoteReportDefaultsString.Add("QuoteAddition2CrewDeposit", CompanyCatalogEntity.QuoteAddition2CrewDeposit);
                            else
                                dictSetQuoteReportDefaultsString.Add("QuoteAddition2CrewDeposit", "ADDITIONAL CREW CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAddition2CrewRON))
                                dictSetQuoteReportDefaultsString.Add("QuoteAddition2CrewRON", CompanyCatalogEntity.QuoteAddition2CrewRON);
                            else
                                dictSetQuoteReportDefaultsString.Add("QuoteAddition2CrewRON", "ADDITIONAL CREW RON CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteWaiting2Charge))
                                dictSetQuoteReportDefaultsString.Add("QuoteWaiting2Charge", CompanyCatalogEntity.QuoteWaiting2Charge);
                            else
                                dictSetQuoteReportDefaultsString.Add("QuoteWaiting2Charge", "WAIT CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.Deposit2FlightCharge))
                                dictSetQuoteReportDefaultsString.Add("Deposit2FlightCharge", CompanyCatalogEntity.Deposit2FlightCharge);
                            else
                                dictSetQuoteReportDefaultsString.Add("Deposit2FlightCharge", "FLIGHT CHARGE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.Quote2Subtotal))
                                dictSetQuoteReportDefaultsString.Add("Quote2Subtotal", CompanyCatalogEntity.Quote2Subtotal);
                            else
                                dictSetQuoteReportDefaultsString.Add("Quote2Subtotal", "SUBTOTAL:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.Discount2AmountDeposit))
                                dictSetQuoteReportDefaultsString.Add("Discount2AmountDeposit", CompanyCatalogEntity.Discount2AmountDeposit);
                            else
                                dictSetQuoteReportDefaultsString.Add("Discount2AmountDeposit", "DISCOUNT AMOUNT:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.Quote2TaxesDefault))
                                dictSetQuoteReportDefaultsString.Add("Quote2TaxesDefault", CompanyCatalogEntity.Quote2TaxesDefault);
                            else
                                dictSetQuoteReportDefaultsString.Add("Quote2TaxesDefault", "TAXES");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteInvoice2Default))
                                dictSetQuoteReportDefaultsString.Add("QuoteInvoice2Default", CompanyCatalogEntity.QuoteInvoice2Default);
                            else
                                dictSetQuoteReportDefaultsString.Add("QuoteInvoice2Default", "TOTAL QUOTE");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuotePrepay2Default))
                                dictSetQuoteReportDefaultsString.Add("QuotePrepay2Default", CompanyCatalogEntity.QuotePrepay2Default);
                            else
                                dictSetQuoteReportDefaultsString.Add("QuotePrepay2Default", "PREPAID");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteRemaining2AmountDefault))
                                dictSetQuoteReportDefaultsString.Add("QuoteRemaining2AmountDefault", CompanyCatalogEntity.QuoteRemaining2AmountDefault);
                            else
                                dictSetQuoteReportDefaultsString.Add("QuoteRemaining2AmountDefault", "REMAINING AMOUNT DUE:");

                            if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteTitle))
                                dictSetQuoteReportDefaultsString.Add("QuoteTitle", CompanyCatalogEntity.QuoteTitle);
                            else
                                dictSetQuoteReportDefaultsString.Add("QuoteTitle", string.Empty);

                            Session.Add("CqCompanyStringQuoteReport", dictSetQuoteReportDefaultsString);

                            Dictionary<string, bool> dictSetQuoteReportDefaultsCheck = new Dictionary<string, bool>();

                            if (CompanyCatalogEntity.IsChangeQueueCity != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsChangeQueueCity", Convert.ToBoolean(CompanyCatalogEntity.IsChangeQueueCity));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsChangeQueueCity", false);

                            if (CompanyCatalogEntity.IsChangeQueueAirport != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsChangeQueueAirport", Convert.ToBoolean(CompanyCatalogEntity.IsChangeQueueAirport));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsChangeQueueAirport", false);

                            if (CompanyCatalogEntity.IsQuoteChangeQueueICAO != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueICAO", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteChangeQueueICAO));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueICAO", false);

                            if (CompanyCatalogEntity.IsQuoteFirstPage != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteFirstPage", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFirstPage));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteFirstPage", false);

                            if (CompanyCatalogEntity.IsChangeQueueDetail != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsChangeQueueDetail", Convert.ToBoolean(CompanyCatalogEntity.IsChangeQueueDetail));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsChangeQueueDetail", false);

                            if (CompanyCatalogEntity.IsQuoteChangeQueueSubtotal != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueSubtotal", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteChangeQueueSubtotal));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueSubtotal", false);

                            if (CompanyCatalogEntity.IsQuoteChangeQueueFees != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueFees", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteChangeQueueFees));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueFees", false);

                            if (CompanyCatalogEntity.IsQuoteChangeQueueSum != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueSum", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteChangeQueueSum));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueSum", false);

                            if (CompanyCatalogEntity.IsQuotePrepaidMinimumUsage2FeeAdj != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuotePrepaidMinimumUsage2FeeAdj", Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidMinimumUsage2FeeAdj));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuotePrepaidMinimumUsage2FeeAdj", false);

                            if (CompanyCatalogEntity.IsQuoteAdditional2FeePrint != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteAdditional2FeePrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditional2FeePrint));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteAdditional2FeePrint", false);

                            if (CompanyCatalogEntity.IsQuotePrepaidMin2UsageFeeAdj != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuotePrepaidMin2UsageFeeAdj", Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidMin2UsageFeeAdj));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuotePrepaidMin2UsageFeeAdj", false);

                            if (CompanyCatalogEntity.IsQuoteFlight2LandingFeePrint != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteFlight2LandingFeePrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFlight2LandingFeePrint));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteFlight2LandingFeePrint", false);

                            if (CompanyCatalogEntity.IsQuotePrepaidSegement2Fee != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuotePrepaidSegement2Fee", Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidSegement2Fee));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuotePrepaidSegement2Fee", false);

                            if (CompanyCatalogEntity.IsQuoteStdCrew2RONPrepaid != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteStdCrew2RONPrepaid", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteStdCrew2RONPrepaid));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteStdCrew2RONPrepaid", false);

                            if (CompanyCatalogEntity.IsQuoteUnpaidStd2CrewRON != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaidStd2CrewRON", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUnpaidStd2CrewRON));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaidStd2CrewRON", false);

                            if (CompanyCatalogEntity.IsQuoteAdditional2CrewPrepaid != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteAdditional2CrewPrepaid", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditional2CrewPrepaid));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteAdditional2CrewPrepaid", false);

                            if (CompanyCatalogEntity.IsQuoteUnpaidAdditional2Crew != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaidAdditional2Crew", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUnpaidAdditional2Crew));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaidAdditional2Crew", false);

                            if (CompanyCatalogEntity.IsQuoteAdditional2CrewRON != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteAdditional2CrewRON", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditional2CrewRON));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteAdditional2CrewRON", false);

                            if (CompanyCatalogEntity.IsQuoteUpaidAdditionl2CrewRON != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUpaidAdditionl2CrewRON", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUpaidAdditionl2CrewRON));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUpaidAdditionl2CrewRON", false);

                            if (CompanyCatalogEntity.IsQuoteWaiting2ChargePrepaid != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteWaiting2ChargePrepaid", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteWaiting2ChargePrepaid));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteWaiting2ChargePrepaid", false);

                            if (CompanyCatalogEntity.IsQuoteUpaid2WaitingChg != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUpaid2WaitingChg", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUpaid2WaitingChg));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUpaid2WaitingChg", false);

                            if (CompanyCatalogEntity.IsQuoteFlight2ChargePrepaid != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteFlight2ChargePrepaid", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFlight2ChargePrepaid));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteFlight2ChargePrepaid", false);

                            if (CompanyCatalogEntity.IsQuoteUnpaidFlight2Chg != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaidFlight2Chg", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUnpaidFlight2Chg));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaidFlight2Chg", false);

                            if (CompanyCatalogEntity.IsQuoteSubtotal2Print != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteSubtotal2Print", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteSubtotal2Print));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteSubtotal2Print", false);

                            if (CompanyCatalogEntity.IsQuoteTaxes2Print != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteTaxes2Print", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteTaxes2Print));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteTaxes2Print", false);

                            if (CompanyCatalogEntity.IsQuoteDiscount2AmountPrepaid != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteDiscount2AmountPrepaid", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDiscount2AmountPrepaid));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteDiscount2AmountPrepaid", false);

                            if (CompanyCatalogEntity.IsQuoteUnpaid2DiscountAmt != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaid2DiscountAmt", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUnpaid2DiscountAmt));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaid2DiscountAmt", false);

                            if (CompanyCatalogEntity.IsQuoteInvoice2Print != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteInvoice2Print", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteInvoice2Print));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteInvoice2Print", false);

                            if (CompanyCatalogEntity.IsQuote2PrepayPrint != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuote2PrepayPrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuote2PrepayPrint));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuote2PrepayPrint", false);

                            if (CompanyCatalogEntity.IsQuoteRemaining2AmtPrint != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteRemaining2AmtPrint", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteRemaining2AmtPrint));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteRemaining2AmtPrint", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailLegNum != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailLegNum", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailLegNum));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailLegNum", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailDepartureDT != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailDepartureDT", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailDepartureDT));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailDepartureDT", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailFromDescription != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailFromDescription", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailFromDescription));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailFromDescription", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailToDescription != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailToDescription", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailToDescription));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailToDescription", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailQuotedAmt != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailQuotedAmt", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailQuotedAmt));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailQuotedAmt", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailFlightHours != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailFlightHours", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailFlightHours));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailFlightHours", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailMiles != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailMiles", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailMiles));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailMiles", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailPassengerCnt != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailPassengerCnt", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailPassengerCnt));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailPassengerCnt", false);

                            if (CompanyCatalogEntity.IsRptQuoteFeeDescription != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteFeeDescription", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteFeeDescription));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteFeeDescription", false);

                            if (CompanyCatalogEntity.IsRptQuoteFeeQuoteAmt != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteFeeQuoteAmt", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteFeeQuoteAmt));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteFeeQuoteAmt", false);

                            if (CompanyCatalogEntity.IsCharterQuoteFee3 != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsCharterQuoteFee3", Convert.ToBoolean(CompanyCatalogEntity.IsCharterQuoteFee3));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsCharterQuoteFee3", false);

                            if (CompanyCatalogEntity.IsCharterQuoteFee4 != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsCharterQuoteFee4", Convert.ToBoolean(CompanyCatalogEntity.IsCharterQuoteFee4));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsCharterQuoteFee4", false);

                            if (CompanyCatalogEntity.IsCharterQuote5 != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsCharterQuote5", Convert.ToBoolean(CompanyCatalogEntity.IsCharterQuote5));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsCharterQuote5", false);

                            if (CompanyCatalogEntity.IsQuoteInformation2 != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteInformation2", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteInformation2));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteInformation2", false);

                            if (CompanyCatalogEntity.IsCompanyName2 != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsCompanyName2", Convert.ToBoolean(CompanyCatalogEntity.IsCompanyName2));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsCompanyName2", false);

                            if (CompanyCatalogEntity.QuoteDescription != null)
                                dictSetQuoteReportDefaultsCheck.Add("QuoteDescription", Convert.ToBoolean(CompanyCatalogEntity.QuoteDescription));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("QuoteDescription", false);

                            if (CompanyCatalogEntity.IsQuoteSales != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteSales", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteSales));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteSales", false);

                            if (CompanyCatalogEntity.IsQuoteTailNum != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteTailNum", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteTailNum));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteTailNum", false);

                            if (CompanyCatalogEntity.IsQuoteTypeCD != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteTypeCD", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteTypeCD));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteTypeCD", false);

                            if (CompanyCatalogEntity.IsQuoteEdit != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteEdit", Convert.ToBoolean(CompanyCatalogEntity.IsQuoteEdit));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsQuoteEdit", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailArrivalDate != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailArrivalDate", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailArrivalDate));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailArrivalDate", false);

                            if (CompanyCatalogEntity.IsRptQuoteDetailQuoteDate != null)
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailQuoteDate", Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailQuoteDate));
                            else
                                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailQuoteDate", false);

                            Session.Add("CqCompanyCheckQuoteReport", dictSetQuoteReportDefaultsCheck);

                            Dictionary<string, int> dictSetQuoteReportDefaultsRad = new Dictionary<string, int>();

                            if (CompanyCatalogEntity.QuoteLogoPosition != null)
                                dictSetQuoteReportDefaultsRad.Add("QuoteLogoPosition", Convert.ToInt32(CompanyCatalogEntity.QuoteLogoPosition));
                            else
                                dictSetQuoteReportDefaultsRad.Add("QuoteLogoPosition", 0);

                            if (CompanyCatalogEntity != null)
                                dictSetQuoteReportDefaultsRad.Add("QuoteSalesLogoPosition", Convert.ToInt32(CompanyCatalogEntity.QuoteSalesLogoPosition));
                            else
                                dictSetQuoteReportDefaultsRad.Add("QuoteSalesLogoPosition", 0);

                            if (CompanyCatalogEntity.ImagePosition != null)
                                dictSetQuoteReportDefaultsRad.Add("ImagePosition", Convert.ToInt32(CompanyCatalogEntity.ImagePosition));
                            else
                                dictSetQuoteReportDefaultsRad.Add("ImagePosition", 0);

                            if (CompanyCatalogEntity.ImageCnt != null)
                                dictSetQuoteReportDefaultsRad.Add("ImageCnt", Convert.ToInt32(CompanyCatalogEntity.ImageCnt));
                            else
                                dictSetQuoteReportDefaultsRad.Add("ImageCnt", 0);

                            if (CompanyCatalogEntity.QuoteIDTYPE != null)
                                dictSetQuoteReportDefaultsRad.Add("QuoteIDTYPE", Convert.ToInt32(CompanyCatalogEntity.QuoteIDTYPE));
                            else
                                dictSetQuoteReportDefaultsRad.Add("QuoteIDTYPE", 0);

                            Session.Add("CqCompanyRadQuoteReport", dictSetQuoteReportDefaultsRad);
                        }
                    }
                }
            }
        }
        #endregion

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {

                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCompanyProfileCatalog.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = true;
                    chkInactive.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }

        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var CompanyValue = FPKMstService.GetCompanyMasterListInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CompanyValue);
            List<FlightPakMasterService.GetAllCompanyMaster> filteredList = GetFilteredList(CompanyValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.HomebaseID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCompanyProfileCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCompanyProfileCatalog.CurrentPageIndex = PageNumber;
            dgCompanyProfileCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllCompanyMaster CompanyValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["HomeBaseID"] = Session["SearchItemPrimaryKeyValue"];
                    Session["SelectedCompanyID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CompanyValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().HomebaseID;
                Session["HomeBaseID"] = PrimaryKeyValue;
                Session["SelectedCompanyID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllCompanyMaster> GetFilteredList(ReturnValueOfGetAllCompanyMaster CompanyValue)
        {
            List<FlightPakMasterService.GetAllCompanyMaster> filteredList = new List<FlightPakMasterService.GetAllCompanyMaster>();

            if (CompanyValue.EntityList.Count > 0)
            {
                if (chkSearchActiveOnly.Checked == false) { filteredList = CompanyValue.EntityList.Where(x => x.IsInActive == false).ToList<GetAllCompanyMaster>(); }
                else if (chkSearchActiveOnly.Checked == true) { filteredList = CompanyValue.EntityList.ToList(); }
            }

            return filteredList;
        }

        #region Lookup - Server Validation
        #region Tab Company Information - Look Up Home Base
        protected void cvMainBase_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbMainBase.Text != null && tbMainBase.Enabled)
                        {
                            CheckUniqueMainBase(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }
        protected void cvMainBaseCheck_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbMainBase.Text != null)
                        {
                            CheckIcaoExist(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        private void CheckUniqueMainBase(object sender, ServerValidateEventArgs e)
        {
            if ((tbMainBase.Text != string.Empty) && (tbMainBase.Text != null))
            {
                //List<GetAllCompanyMaster> CompanyList = new List<GetAllCompanyMaster>();
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var CompanyList = Service.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD != null && x.IsDeleted == false && x.HomebaseCD.ToUpper().Trim() == (tbMainBase.Text.ToString().ToUpper().Trim())).ToList();
                    // CompanyList = ((List<GetAllCompanyMaster>)Session["Homeval"]).Where(x => x.HomebaseCD != null && x.isdeleted == false && x.HomebaseCD.ToUpper().Trim() == (tbMainBase.Text.ToString().ToUpper().Trim())).ToList<GetAllCompanyMaster>();
                    if (CompanyList.Count() > 0)
                    {
                        e.IsValid = false;

                        //Commented Out By Vishwa
                        //RadAjaxManager1.FocusControl(tbMainBase.ClientID);
                        //tbMainBase.Focus();
                    }
                    else
                    {

                        var RetValue = Service.GetAirportByAirportICaoID(tbMainBase.Text).EntityList;
                        if (RetValue.Count() != 0)
                        {
                            if (!string.IsNullOrEmpty(RetValue[0].AirportName))
                            {
                                lbMainBase1.Text = System.Web.HttpUtility.HtmlEncode(RetValue[0].AirportName.ToString());
                                hdnHomeBaseID.Value = RetValue[0].AirportID.ToString();
                                tbMainBase.Text = RetValue[0].IcaoID;
                            }
                        }
                        //Commented Out By Vishwa
                        //RadAjaxManager1.FocusControl(tbBaseDescription.ClientID);
                        //tbBaseDescription.Focus();
                    }
                }
            }
        }
        private void CheckIcaoExist(object sender, ServerValidateEventArgs e)
        {
            if ((tbMainBase.Text != string.Empty) && (tbMainBase.Text != null))
            {
                if (CheckIcaoExist())
                {
                    e.IsValid = false;
                    lbMainBase1.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    //Commented Out By Vishwa
                    //RadAjaxManager1.FocusControl(tbMainBase.ClientID);
                    //tbMainBase.Focus();
                }
            }
        }
        #endregion
        #region Tab Company Information - Look Up Federal Tax Account
        protected void cvFedTaxAccount_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbFedTaxAccount.Text))
                        {
                            CheckAlreadyFedTaxAccountExist(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }
        private bool CheckAlreadyFedTaxAccountExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbFedTaxAccount.Text != string.Empty) && (tbFedTaxAccount.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbFedTaxAccount.Text.ToString().ToUpper().Trim()));
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            e.IsValid = false;
                            //RadAjaxManager1.FocusControl(tbFedTaxAccount.ClientID);
                            // tbFedTaxAccount.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            //RadAjaxManager1.FocusControl(tbStateTaxAccount.ClientID);
                            //tbStateTaxAccount.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        #endregion

        #region Tab Company Information - Look Up Sales Tax Account
        protected void cvSalesTaxAccount_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbSalesTaxAccount.Text))
                        {
                            CheckAlreadySalesTaxAccountExist(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }
        private bool CheckAlreadySalesTaxAccountExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbSalesTaxAccount.Text != string.Empty) && (tbSalesTaxAccount.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbSalesTaxAccount.Text.ToString().ToUpper().Trim()));
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager1.FocusControl(tbSalesTaxAccount.ClientID);
                            //tbSalesTaxAccount.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            if (chkCanpasspermit.Checked == true)
                            {
                                RadAjaxManager1.FocusControl(tbCanpassPermit.ClientID);
                                // tbCanpassPermit.Focus();
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbCurrency.ClientID);
                                // tbCurrency.Focus();
                            }
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        #endregion

        #region Tab Company Information - Look Up Exchange Rate
        protected void cvExchangeRateID_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbExchangeRateCode.Text))
                        {
                            CheckAlreadyExchangeRateExist(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }
        private bool CheckAlreadyExchangeRateExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbExchangeRateCode.Text != string.Empty) && (tbExchangeRateCode.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetExchangeRate().EntityList.Where(x => x.ExchRateCD.Trim().ToUpper() == (tbExchangeRateCode.Text.Trim().ToUpper())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            e.IsValid = false;
                            returnValue = true;
                        }
                        else
                        {
                            hdnExchangeRateID.Value = RetValue[0].ExchangeRateID.ToString();
                            tbExchangeRateCode.Text = RetValue[0].ExchRateCD;
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        #endregion

        #region Tab Company Information - Look Up Salary Tax Account
        protected void cvStateTaxAccount_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbStateTaxAccount.Text))
                        {
                            CheckAlreadyStateTaxAccountExist(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomAddress);
                }
            }
        }

        private bool CheckAlreadyStateTaxAccountExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbStateTaxAccount.Text != string.Empty) && (tbStateTaxAccount.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbStateTaxAccount.Text.ToString().ToUpper().Trim()));
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager1.FocusControl(tbStateTaxAccount.ClientID);
                            // tbStateTaxAccount.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            RadAjaxManager1.FocusControl(tbSalesTaxAccount.ClientID);
                            //tbSalesTaxAccount.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        #endregion

        #region Tab Preflight - Look Up Default Crew Duty Rules
        protected void cvDefaultCrewDutyRules_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultCrewDutyRules.Text))
                        {
                            CheckAlreadyDefaultCrewDutyRulesExist(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        private bool CheckAlreadyDefaultCrewDutyRulesExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultCrewDutyRules.Text != string.Empty) && (tbDefaultCrewDutyRules.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetCrewDutyRuleList().EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper().Equals(tbDefaultCrewDutyRules.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultCrewDutyRules.ClientID);
                            //tbDefaultCrewDutyRules.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            hdnCrewDutyRulesID.Value = ((FlightPakMasterService.CrewDutyRules)RetValue[0]).CrewDutyRulesID.ToString().Trim();
                            if (!string.IsNullOrEmpty(RetValue[0].FedAviatRegNum))
                                radlstDefaultFAR.SelectedValue = RetValue[0].FedAviatRegNum.Trim();
                            tbDefaultCrewDutyRules.Text = RetValue[0].CrewDutyRuleCD;
                            RadAjaxManager1.FocusControl(radlstDefaultFAR.ClientID);
                            // radlstDefaultFAR.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        #endregion

        #region Tab Preflight - Look Up Default Flight Category
        protected void cvDefaultFlightCategory_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultFlightCategory.Text))
                        {
                            CheckAlreadyDefaultFlightCategoryExist(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        private bool CheckAlreadyDefaultFlightCategoryExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultFlightCategory.Text != string.Empty) && (tbDefaultFlightCategory.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetFlightCategoryList().EntityList.Where(x => x.FlightCatagoryCD.Trim().ToUpper().Equals(tbDefaultFlightCategory.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultFlightCategory.ClientID);
                            //tbDefaultFlightCategory.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            hdnFlightCategoryID.Value = ((FlightPakMasterService.FlightCatagory)RetValue[0]).FlightCategoryID.ToString().Trim();
                            tbDefaultFlightCategory.Text = ((FlightPakMasterService.FlightCatagory)RetValue[0]).FlightCatagoryCD;
                            RadAjaxManager1.FocusControl(tbDefaultChklistGroup.ClientID);
                            //tbDefaultChklistGroup.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        #endregion

        #region Tab Preflight - Look Up Default Check List Group
        protected void cvDefaultChklistGroup_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultChklistGroup.Text))
                        {
                            CheckAlreadyDefaultChklistGroupExist(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        private bool CheckAlreadyDefaultChklistGroupExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultChklistGroup.Text != string.Empty) && (tbDefaultChklistGroup.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetTripManagerChecklistGroupList().EntityList.Where(x => x.CheckGroupCD.Trim().ToUpper().Equals(tbDefaultChklistGroup.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultChklistGroup.ClientID);
                            //tbDefaultChklistGroup.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            hdnCheckGroupID.Value = ((FlightPakMasterService.TripManagerCheckListGroup)RetValue[0]).CheckGroupID.ToString().Trim();
                            tbDefaultChklistGroup.Text = ((FlightPakMasterService.TripManagerCheckListGroup)RetValue[0]).CheckGroupCD;
                            RadAjaxManager1.FocusControl(tbTripManagerStatus.ClientID);
                            // tbTripManagerStatus.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        #endregion

        #region Tab Prefilght - Look Up Default Tripsheet RW Group
        protected void cvDefaultTripsheetRWGroup_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultTripsheetRWGroup.Text))
                        {
                            CheckAlreadyDefaultTripsheetRWGroup(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        private bool CheckAlreadyDefaultTripsheetRWGroup(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultTripsheetRWGroup.Text != string.Empty) && (tbDefaultTripsheetRWGroup.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetTripSheetReportHeaderList().EntityList.Where(x => x.ReportID != null && x.ReportID.Trim().ToUpper().Equals(tbDefaultTripsheetRWGroup.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultTripsheetRWGroup.ClientID);
                            //tbDefaultTripsheetRWGroup.Focus();
                            returnValue = true;
                        }
                        else
                        {
                            hdnDefaultTripsheetRWGroup.Value = ((FlightPakMasterService.GetTripSheetReportHeader)RetValue[0]).TripSheetReportHeaderID.ToString().Trim();
                            tbDefaultTripsheetRWGroup.Text = ((FlightPakMasterService.GetTripSheetReportHeader)RetValue[0]).ReportID;
                            RadAjaxManager1.FocusControl(tbGADeskId.ClientID);
                            //tbGADeskId.Focus();
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            }
        }
        #endregion

        #region Tab Preflight - Look Up Default Blocked Seat Flt Purpose
        protected void cvDefaultBlockedSeat_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultBlockedSeat.Text))
                        {
                            CheckAlreadyDefaultBlockedSeatExist(sender, e);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        private bool CheckAlreadyDefaultBlockedSeatExist(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultBlockedSeat.Text != string.Empty) && (tbDefaultBlockedSeat.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeCD.Trim().ToUpper().Equals(tbDefaultBlockedSeat.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultBlockedSeat.ClientID);
                            // tbDefaultBlockedSeat.Focus();
                            return true;
                        }
                        else
                        {
                            hdnDefaultBlockedSeat.Value = ((FlightPakMasterService.FlightPurpose)RetValue[0]).FlightPurposeID.ToString().Trim();
                            tbDefaultBlockedSeat.Text = ((FlightPakMasterService.FlightPurpose)RetValue[0]).FlightPurposeCD;
                            RadAjaxManager1.FocusControl(tbSIFLLayoversHrs.ClientID);
                            // tbSIFLLayoversHrs.Focus();
                            return false;
                        }
                    }
                }
                return false;
            }
        }
        #endregion

        #region Tab Charter - Look Up Charter Department

        protected void cvDepartmentCode_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbCharterDepartment.Text))
            {
                CheckAlreadyDepartmentsExist(sender, e);
            }
            else
            {
                hdnDepartmentID.Value = string.Empty;
                lbCharterDeptInfo.Text = string.Empty;
            }
        }
        private bool CheckAlreadyDepartmentsExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbCharterDepartment.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var DepartmentValue = DepartmentService.GetAllDeptAuthorizationList().EntityList.Where(x => x.DepartmentCD.Trim().ToUpper().Equals(tbCharterDepartment.Text.ToString().ToUpper().Trim()));
                        if (DepartmentValue.Count() == 0 || DepartmentValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCharterDepartment);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.GetAllDeptAuth cm in DepartmentValue)
                            {
                                hdnDepartmentID.Value = cm.DepartmentID.ToString();
                                tbCharterDepartment.Text = cm.DepartmentCD;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        #endregion

        #region Tab Charter - Look Up Default Crew Duty Rule
        protected void cvCqefaultCrewDutyRules_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbCQDefaultCrewDutyRules.Text))
            {
                CheckAlreadyCrewDutyRulesExist(sender, e);
            }
        }
        private bool CheckAlreadyCrewDutyRulesExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbCQDefaultCrewDutyRules.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient CrewRulesService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CrewDutyRulesValue = CrewRulesService.GetCrewDutyRuleList().EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper().Equals(tbCQDefaultCrewDutyRules.Text.ToString().ToUpper().Trim()));
                        if (CrewDutyRulesValue.Count() == 0 || CrewDutyRulesValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDefaultCrewDutyRules);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.CrewDutyRules cm in CrewDutyRulesValue)
                            {
                                hdnCrewDutyRules.Value = cm.CrewDutyRulesID.ToString();
                                tbCQDefaultCrewDutyRules.Text = cm.CrewDutyRuleCD;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        #endregion
        #region Tab Charter - Look Up PAX
        protected void cvtbPax_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbPax.Text))
            {
                CheckAlreadyFlightCategoryPaxExist(sender, e);
            }
        }
        private bool CheckAlreadyFlightCategoryPaxExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbPax.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient FltCatService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var FltCatValue = FltCatService.GetFlightCategoryList().EntityList.Where(x => x.FlightCatagoryCD.Trim().ToUpper().Equals(tbPax.Text.ToString().ToUpper().Trim()));
                        if (FltCatValue.Count() == 0 || FltCatValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbPax);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.FlightCatagory cm in FltCatValue)
                            {
                                hdnPax.Value = cm.FlightCategoryID.ToString();
                                lbPaxInfo.Text = System.Web.HttpUtility.HtmlEncode(cm.FlightCatagoryDescription);
                                tbPax.Text = cm.FlightCatagoryCD;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        #endregion

        #region Tab Charter - Look Up No Pax
        protected void cvtbNoPax_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbNoPax.Text))
            {
                CheckAlreadyFlightCategoryNoPaxExist(sender, e);
            }
        }
        private bool CheckAlreadyFlightCategoryNoPaxExist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbNoPax.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient FltCatService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var FltCatValue = FltCatService.GetFlightCategoryList().EntityList.Where(x => x.FlightCatagoryCD.Trim().ToUpper().Equals(tbNoPax.Text.ToString().ToUpper().Trim()));
                        if (FltCatValue.Count() == 0 || FltCatValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbNoPax);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.FlightCatagory cm in FltCatValue)
                            {
                                hdnNoPax.Value = cm.FlightCategoryID.ToString();
                                lbNoPaxInfo.Text =Microsoft.Security.Application.Encoder.HtmlEncode(cm.FlightCatagoryDescription);
                                tbNoPax.Text = cm.FlightCatagoryCD;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        #endregion

        #region Tab Charter - Look Up Fed/VAT
        protected void cvFedAccNum1_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbFedAccNum1.Text))
            {
                CheckAlreadyFederalTax1Exist(sender, e);
            }
        }
        private bool CheckAlreadyFederalTax1Exist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbFedAccNum1.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient AccountsService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var AccountsValue = AccountsService.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbFedAccNum1.Text.ToString().ToUpper().Trim()));
                        if (AccountsValue.Count() == 0 || AccountsValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbFedAccNum1);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.Account cm in AccountsValue)
                            {
                                hdnFedAccNum1.Value = cm.AccountID.ToString();
                                tbFedAccNum1.Text = cm.AccountNum;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        #endregion
        #region Tab Charter - Look Up Rural
        protected void cvFedAccNum2_OnServerValidate(object sender, ServerValidateEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbFedAccNum2.Text))
            {
                CheckAlreadyFederalTax2Exist(sender, e);
            }
        }
        private bool CheckAlreadyFederalTax2Exist(object sender, ServerValidateEventArgs e)
        {
            bool returnValue = false;
            if (!string.IsNullOrEmpty(tbFedAccNum2.Text))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient AccountsService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var AccountsValue = AccountsService.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().Equals(tbFedAccNum2.Text.ToString().ToUpper().Trim()));
                        if (AccountsValue.Count() == 0 || AccountsValue == null)
                        {
                            e.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbFedAccNum2);
                            returnValue = true;
                        }
                        else
                        {
                            foreach (FlightPakMasterService.Account cm in AccountsValue)
                            {
                                hdnFedAccNum2.Value = cm.AccountID.ToString();
                                tbFedAccNum2.Text = cm.AccountNum;
                                returnValue = false;
                            }
                        }
                    }
                }
            }
            return returnValue;
        }
        #endregion
        protected void TabCompanyProfileCatalog_OnTabClick(object sender, RadTabStripEventArgs e)
        {
            Page.Validate("Save");
            if (Page.IsValid)
            {
                e.Tab.Selected = true;
                RadMultiPage1.SelectedIndex = e.Tab.Index;
                if (e.Tab.Index == 4)
                {
                    //LoadCQImage();
                    bool imgFound = false;
                    imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ObjRetImg = ObjImgService.GetFileWarehouseList("CQCompany", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlCQFileName.Text.Trim());
                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                        {
                            imgFound = true;
                            byte[] picture = fwh.UWAFilePath;
                            //start of modification for image issue
                            if (hdnBrowserName.Value == "Chrome")
                            {
                                imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                            }
                            else
                            {
                                Session["CQBase64"] = picture;
                                imgCQ.ImageUrl = "../ImageHandler.ashx?CQBase64=CQBase64&cd=" + Guid.NewGuid();
                            }
                            //end of modification for image issue
                        }
                        tbCQimgName.Text = ddlCQFileName.Text.Trim();
                        if (!imgFound)
                        {
                            //Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CQDicCqImg"];
                            Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                            if (Session["CQDicImg"] == null)
                                Session["CQDicImg"] = new Dictionary<string, byte[]>();
                            dicImg = (Dictionary<string, byte[]>)Session["CQDicImg"];
                            int count = dicImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));
                            if (count > 0)
                            {
                                //start of modification for image issue
                                if (hdnBrowserName.Value == "Chrome")
                                {
                                    imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlCQFileName.Text.Trim()]);
                                }
                                else
                                {

                                    Session["CQBase64"] = dicImg[ddlCQFileName.Text.Trim()];
                                    Session["CQBase641"] = dicImg[ddlCQFileName.Text.Trim()];
                                    imgCQ.ImageUrl = "../ImageHandler.ashx?CQBase64=CQBase64&cd=" + Guid.NewGuid();
                                }
                                //end of modification for image issue
                            }
                        }
                    }
                    
                }
                if (e.Tab.Index == 2)
                {
                    //LoadImage();
                    bool imgFound = false;
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ObjRetImg = ObjImgService.GetFileWarehouseList("Company", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());
                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                        {
                            imgFound = true;
                            byte[] picture = fwh.UWAFilePath;
                            //start of modification for image issue
                            if (hdnBrowserName.Value == "Chrome")
                            {
                                imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                            }
                            else
                            {
                                Session["Base64"] = picture;
                                imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                            }
                            //end of modification for image issue
                        }
                        tbImgName.Text = ddlImg.Text.Trim();
                        if (!imgFound)
                        {
                            Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                            int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                            if (count > 0)
                            {
                                //start of modification for image issue
                                if (hdnBrowserName.Value == "Chrome")
                                {
                                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                }
                                else
                                {
                                    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                    Session["Base641"] = dicImg[ddlImg.Text.Trim()];
                                    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                }
                                //end of modification for image issue
                            }
                        }
                    }
                }
            }

        }
        #endregion

        protected void dgCompanyProfileCatalog_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCompanyProfileCatalog, Page.Session);
        }
         protected void chkAutoPaxcode_CheckedChanged(object sender, EventArgs e) 	
         {
            EnableDisableAutoPaxCode(chkAutoPaxcode.Checked);
         }

        protected void chkAutoCrewcode_CheckedChanged(object sender, EventArgs e)
        {
            EnableDisableAutoCrewCode(chkAutoCrewcode.Checked);
        }
        private void EnableDisableAutoPaxCode(bool enable)
        {
            radlstPaxNameElements.Enabled = enable;
            tbFirstPaxCharacter.Enabled = enable;
            tbInitialPaxCharacter.Enabled = enable;
            tbLastPaxCharacter.Enabled = enable;
        }
        private void EnableDisableAutoCrewCode(bool enable)
        {
            radlstCrewNameElement.Enabled = enable;
            tbFirstCrewCharacter.Enabled = enable;
            tbMiddleCrewCharacter.Enabled = enable;
            tbLastCrewCharacter.Enabled = enable;
        }

        protected void chkDeactivateAutoUpdatingRates_CheckedChanged(object sender, EventArgs e)
        {
            chkTailNumHomebase.Enabled = chkTailNumHomebase.Checked = !chkDeactivateAutoUpdatingRates.Checked;
        }

        private string ConvertTimeToDecimal(string time)
        {
            DateTime dt1 = DateTime.Parse(string.IsNullOrEmpty(time) ? "0:00" : time);
            DateTime dt2 = DateTime.Parse("0:00");
            double span = (dt1 - dt2).TotalHours;
            return span.ToString("#0.00");
        }
    }
}
