﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomPilotLog.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.CustomPilotLog" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Custom Pilot Log</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgCustomPilot.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function ResetRecord(sender, args) {
                var ReturnValue = false;
                //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
                var Msg = "Do you want to Reset All or only the Custom Descriptions of Crew Log Custom Labels 3 and 4 <br><br> Ok --> Reset All <br> Cancel ----> Custom Labels only";
                var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                    if (shouldSubmit) {
                        document.getElementById('<%= hdnSave.ClientID %>').value = "ResetYes";
                        ReturnValue = false;
                    }
                    else {
                        document.getElementById('<%= hdnSave.ClientID %>').value = "ResetNo";
                        ReturnValue = false;
                    }
                    this.click();
                });
                radconfirm(Msg, callBackFunction, 400, 100, null, "Custom Pilot Log");
                args.set_cancel(true);
                return ReturnValue;
            }
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCustomPilot.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "HomeBase");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "BaseDescription")
                }

                if (selectedRows.length > 0) {
                    oArg.HomeBase = cell1.innerHTML;
                    oArg.BaseDescription = cell2.innerHTML;
                }
                else {
                    oArg.HomeBase = "";
                    oArg.BaseDescription = "";
                }


                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            function Close() {
                GetRadWindow().Close();
            }
            function ProcessUpdate() {
                var masterTable = $find('<%= dgCustomPilot.ClientID %>').get_masterTableView();
                if (masterTable.get_selectedItems().length < 0) {
                    alert('Please select a record from the above table.');
                    return false;
                }
            }
            function ProcessDelete(customMsg) {
                //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler

                var masterTable = $find('<%= dgCustomPilot.ClientID %>').get_masterTableView();
                if (masterTable.get_selectedItems().length > 0) {
                    if (confirm('Are you sure you want to delete this record?')) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    alert('Please select a record from the above table.');
                    return false;
                }
            }
            function EnableForm(Enable, control) {
                var show
                if (Enable == true) {
                    show = "inline";
                }
                else {
                    show = "none";
                }
                document.getElementById('<%= btnSaveChanges.ClientID %>').style.display = show;
                document.getElementById('<%= btnCancel.ClientID %>').style.display = show;
                document.getElementById('<%= btnReset.ClientID %>').style.display = "inline";
                //EnableGrid(Enable);
                if (control == 'btnSaveChanges') {
                    document.getElementById('<%= btnSaveChanges1.ClientID %>').click();
                }
                else if (control == 'btnCancel') {
                    document.getElementById('<%= btnCancel1.ClientID %>').click();
                }
                return true;
            }
            function EnableGrid(Enable) {
                Enable = !(Enable);
                var grid = $find("<%=dgCustomPilot.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                for (var i = 0; i < length; i++) {
                    //if ((MasterTable.get_dataItems()[i].getDataKeyValue('TSFlightLogID') == null) || (MasterTable.get_dataItems()[i].getDataKeyValue('TSFlightLogID') == "") || (MasterTable.get_dataItems()[i].getDataKeyValue('TSFlightLogID') == "0")) {
                    MasterTable.get_dataItems()[i].findElement("tbCustDesc").disabled = false;
                    MasterTable.get_dataItems()[i].findElement("tbOrder").disabled = false;
                    MasterTable.get_dataItems()[i].findElement("chkIsPrint").disabled = false;
                    //}
                    //                    else {
                    //                        MasterTable.get_dataItems()[i].findElement("tbCustDesc").disabled = true;
                    //                        MasterTable.get_dataItems()[i].findElement("tbOrder").disabled = true;
                    //                        MasterTable.get_dataItems()[i].findElement("chkIsPrint").disabled = true;
                    //                    }
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgCustomPilot">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomPilot" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomPilot" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomPilot" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomPilot" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnReset">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomPilot" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSaveChanges1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomPilot" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomPilot" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnReset1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomPilot" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table width="100%" class="box1">
            <tr>
                <td>
                    <asp:TextBox ID="tbMainBase" runat="server" CssClass="text80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbBaseDescription" runat="server" CssClass="text180"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgCustomPilot" runat="server" EnableAJAX="True" AllowMultiRowSelection="false"
                        OnItemDataBound="CustomPilot_ItemDataBound" OnNeedDataSource="dgCustomPilot_BindData" OnPageIndexChanged="dgCountry_PageIndexChanged"
                        OnSelectedIndexChanged="dgCustomPilot_OnSelectedIndexChanged" Height="341px" AllowSorting="true" OnSortCommand="dgCustomPilot_SortCommand"
                        Width="600px">
                        <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                            DataKeyNames="TSFlightLogID,OriginalDescription,CustomDescription,SequenceOrder,IsPrint"
                            ClientDataKeyNames="TSFlightLogID" CommandItemDisplay="None" ShowFooter="false"
                            AllowFilteringByColumn="false" AllowPaging="false" AllowMultiColumnSorting="true">
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Description" CurrentFilterFunction="Contains"
                                    DataField="TSFlightLogID" Display="false" ShowFilterIcon="false" UniqueName="desc"
                                    AllowFiltering="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Description" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" UniqueName="desc" AllowFiltering="false"
                                    SortExpression="OriginalDescription" DataField="OriginalDescription">
                                    <ItemTemplate>
                                        <asp:Label ID="lbDesc" runat="server" CssClass="text140" Text='<%# Eval("OriginalDescription") %>'
                                            MaxLength="40"></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Custom Description" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" UniqueName="custdesc" AllowFiltering="false"
                                    SortExpression="CustomDescription" DataField="CustomDescription">
                                    <ItemTemplate>
                                        <asp:TextBox ID="tbCustDesc" runat="server" CssClass="text140" Text='<%# Eval("CustomDescription") %>'
                                            MaxLength="40"></asp:TextBox>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Order" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    UniqueName="Order" AllowFiltering="false" Display="false">
                                    <ItemTemplate>
                                        <asp:TextBox ID="tbOrder" runat="server" CssClass="text100" Text='<%# Eval("SequenceOrder") %>'
                                            MaxLength="4"></asp:TextBox>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Print" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    UniqueName="IsPrint" AllowFiltering="false"
                                    SortExpression="IsPrint" DataField="IsPrint">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsPrint" runat="server" Checked='<%# Eval("IsPrint") != null ? Eval("IsPrint") : false %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; float: left; clear: both;">
                                    <asp:LinkButton ID="lbtnInitInsert" runat="server" OnClientClick="javascript:EnableForm(true);"
                                        OnClick="lbtnInitInsert_Click" ToolTip="Add" CommandName="InitInsert" Visible="false"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                    <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClick="lbtnInitEdit_Click" ToolTip="Edit"
                                        OnClientClick="javascript:return ProcessUpdate(),EnableForm(true);" CommandName="Edit"
                                        Visible='<%# IsAuthorized(Permission.Database.EditCompanyProfileCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                    <asp:LinkButton ID="lbtnDelete" OnClick="lbtnDelete_Click" OnClientClick="javascript:return ProcessDelete();"
                                        runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible="false"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                </div>
                                <div style="padding: 5px 5px; float: right;">
                                    <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                                </div>
                            </CommandItemTemplate>
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        </MasterTableView>
                        <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                    <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                        <tr>
                            <td align="left" class="tdLabel160">
                                <div id="tdSuccessMessage" class="success_msg">
                                    Record saved successfully.</div>
                            </td>
                            <td>
                                <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClientClick="javascript:return EnableForm(false,'btnSaveChanges');"
                                    OnClick="SaveChanges_Click" CssClass="button" Style="display: none;" />
                                <asp:Button ID="btnSaveChanges1" Text="Save" runat="server" OnClick="SaveChanges_Click"
                                    CssClass="button" Style="display: none;" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClientClick="javascript:return EnableForm(false,'btnCancel');"
                                    OnClick="Cancel_Click" Style="display: none;" />
                                <asp:Button ID="btnCancel1" Text="Cancel" runat="server" CssClass="button" OnClick="Cancel_Click"
                                    Style="display: none;" />
                            </td>
                            <td class="custom_radbutton removeBarFromPopup">
                                <telerik:RadButton ID="btnReset" runat="server" Text="Reset" OnClientClicking="ResetRecord" CssClass="button"
                                    OnClick="Reset_Click" />
                            </td>
                            <td>
                                <asp:HiddenField ID="hdnSave" runat="server" />
                                <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
