﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TSABatchProcess.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.TSABatchProcess" ClientIDMode="AutoID"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TSA Batch Process</title>
    <link type="text/css" rel="stylesheet" href="../../../Scripts/jquery.alerts.css" />
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../../../Scripts/jquery.alerts.js"></script>
    <script type="text/javascript">
        function TSANoFlyFile_onchange() {
            document.getElementById("hdnTSANoFlyFile").value = document.getElementById("TSANoFlyFile").value;
//            if (document.getElementById("hdnTSANoFlyFile").value.indexOf("No_Fly") == -1) {
//                document.getElementById("TSANoFlyFile").value = "";
//                alert("Please upload No Fly File");
//            }
            return false;
        }
        function TSASelecteeFile_onchange() {
            document.getElementById("hdnTSASelecteeFile").value = document.getElementById("TSASelecteeFile").value;
//            if (document.getElementById("hdnTSANoFlyFile").value.indexOf("Selectee") == -1) {
//                document.getElementById("TSASelecteeFile").value = "";
//                alert("Please upload Selectee File");
//            }
            return false;
        }
        function TSARadClearFile_onchange() {
            document.getElementById("hdnTSARadClearFile").value = document.getElementById("TSARadClearFile").value;
//            if (document.getElementById("hdnTSANoFlyFile").value.indexOf("Cleared") == -1) {
//                document.getElementById("TSARadClearFile").value = "";
//                alert("Please upload Clear File");
//            }
            return false;
        }
        function CloseWindow() {
            window.close();
            return false;
        }
        function btnSaveChanges_OnClientClick(Disabled) {
            if (Disabled == true) {
                if ((document.getElementById("TSANoFlyFile").value == "") && (document.getElementById("TSASelecteeFile").value == "") && (document.getElementById("TSARadClearFile").value == "")) {
                    alert("Please select files to upload.", "TSA Batch Process");
                    return false;
                }
            }
            document.getElementById("btnSaveChanges").disabled = Disabled;
            document.getElementById("btnCancel").disabled = Disabled;
            document.getElementById("btnClose").disabled = Disabled;
            if (Disabled == true) {
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    document.getElementById("lbUploadStatus").textContent = "File is uploading. Please wait...";
                } else {
                    document.getElementById("lbUploadStatus").innerText = "File is uploading. Please wait...";
                }
                __doPostBack('btnSaveChanges', 'btnSaveChanges_OnClick');
            }
            else {
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    document.getElementById("lbUploadStatus").textContent = "File uploaded successfully";
                } else {
                    document.getElementById("lbUploadStatus").innerText = "File uploaded successfully.";
                }
            }
            return false;
        }
        function browserName() {
            var agt = navigator.userAgent.toLowerCase();
            if (agt.indexOf("msie") != -1) return 'Internet Explorer';
            if (agt.indexOf("chrome") != -1) return 'Chrome';
            if (agt.indexOf("opera") != -1) return 'Opera';
            if (agt.indexOf("firefox") != -1) return 'Firefox';
            if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
            if (agt.indexOf("netscape") != -1) return 'Netscape';
            if (agt.indexOf("safari") != -1) return 'Safari';
            if (agt.indexOf("staroffice") != -1) return 'Star Office';
            if (agt.indexOf("webtv") != -1) return 'WebTV';
            if (agt.indexOf("beonex") != -1) return 'Beonex';
            if (agt.indexOf("chimera") != -1) return 'Chimera';
            if (agt.indexOf("netpositive") != -1) return 'NetPositive';
            if (agt.indexOf("phoenix") != -1) return 'Phoenix';
            if (agt.indexOf("skipstone") != -1) return 'SkipStone';
            if (agt.indexOf('\/') != -1) {
                if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                    return navigator.userAgent.substr(0, agt.indexOf('\/'));
                }
                else return 'Netscape';
            } else if (agt.indexOf(' ') != -1)
                return navigator.userAgent.substr(0, agt.indexOf(' '));
            else return navigator.userAgent;
        }
    </script>
</head>
<body>
    <form id="form2" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    
    <%--<telerik:radajaxmanager id="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divTSAFileUpload" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divTSAFileUpload" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:radajaxmanager>--%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="divTSAFileUpload" runat="server" class="ExternalForm" style="width: 650px;">
        <asp:Panel ID="pnlTSAFileUpload" runat="server" Visible="true">
            <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                <ContentTemplate>
                    <table class="box1">
                        <tr>
                            <td colspan="3" align="left" class="tdLabel160">
                                TSA BATCH PROCESS
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="tblspace_5">
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel140" valign="top">
                                TSA - No Fly
                            </td>
                            <td>
                                <asp:FileUpload ID="TSANoFlyFile" runat="server" CssClass="text330" onchange="javascript:return TSANoFlyFile_onchange();" />
                                <asp:HiddenField ID="hdnTSANoFlyFile" runat="server" />
                            </td>
                            <td>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TSANoFlyFile"
                                    ValidationGroup="Save" ForeColor="Red" ErrorMessage="Upload Only .csv file."
                                    ValidationExpression="(.*\.([Cc][Ss][Vv])$)">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel140" valign="top">
                                TSA - Selectee
                            </td>
                            <td>
                                <asp:FileUpload ID="TSASelecteeFile" runat="server" CssClass="text330" onchange="javascript:return TSASelecteeFile_onchange();" />
                                <asp:HiddenField ID="hdnTSASelecteeFile" runat="server" />
                            </td>
                            <td>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TSASelecteeFile"
                                    ValidationGroup="Save" ForeColor="Red" ErrorMessage="Upload Only .csv file."
                                    ValidationExpression="(.*\.([Cc][Ss][Vv])$)">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel140" valign="top">
                                TSA - Cleared
                            </td>
                            <td>
                                <asp:FileUpload ID="TSARadClearFile" runat="server" CssClass="text330" onchange="javascript:return TSARadClearFile_onchange();" />
                                <asp:HiddenField ID="hdnTSARadClearFile" runat="server" />
                            </td>
                            <td>
                                <asp:RegularExpressionValidator ID="rexp" runat="server" ControlToValidate="TSARadClearFile"
                                    ValidationGroup="Save" ForeColor="Red" ErrorMessage="Upload Only .csv file."
                                    ValidationExpression="(.*\.([Cc][Ss][Vv])$)">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbUploadStatus" runat="server" Text="" Style="color: Red;"></asp:Label>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnSaveChanges" CssClass="button" runat="server" Text="Upload" OnClick="btnSaveChanges_OnClick"
                                    ValidationGroup="Save" OnClientClick="javascript:return btnSaveChanges_OnClientClick(true);" />
                                <asp:Button ID="btnCancel" Text="Reset" CssClass="button" CausesValidation="false"
                                    OnClick="btnCancel_OnClick" runat="server" />
                                <asp:Button ID="btnClose" Text="Close" CssClass="button" CausesValidation="false"
                                    OnClientClick="javascript:return CloseWindow();" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSaveChanges" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
