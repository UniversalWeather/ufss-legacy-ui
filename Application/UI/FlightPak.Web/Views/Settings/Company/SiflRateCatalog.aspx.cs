﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class SiflRateCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private List<string> FareLevelCodes = new List<string>();
        private bool SiflRatePageNavigated = false;
        private ExceptionManager exManager;
        private string strFareLevelCD = "";
        private string strFareLevelID = "";
        string DateFormat = string.Empty;
        DateTime DateStart = new DateTime();
        DateTime DateEnd = new DateTime();
        private bool _selectLastModified = false;

        #region "Comments"

        //Defect : 2164(OntimeID) New Requirement : In SiflRate Catalog,System should validate the Duplicate date and Overlapping date
        //Fixed By: Karthikeyan.

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgSiflRateCatalog, dgSiflRateCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgSiflRateCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                        // Set Logged-in User Name
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }
                        ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewSiflRateCatalog);
                            chkSearchActiveOnly.Checked = true;
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }

        /// <summary>
        /// Default selection on first grid row select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (BindDataSwitch)
                        {
                            dgSiflRateCatalog.Rebind();
                        }
                        if (dgSiflRateCatalog.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedFareLevelID"] = null;
                            //}
                            if (Session["SelectedFareLevelID"] == null)
                            {
                                dgSiflRateCatalog.SelectedIndexes.Add(0);
                                Session["SelectedFareLevelID"] = dgSiflRateCatalog.Items[0].GetDataKeyValue("FareLevelID").ToString();
                            }

                            if (dgSiflRateCatalog.SelectedIndexes.Count == 0)
                                dgSiflRateCatalog.SelectedIndexes.Add(0);

                            ReadOnlyForm();
                            GridEnable(true, true, true);
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }




        /// <summary>
        /// for setting format for date in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSiflRateCatalog_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem DataItem = e.Item as GridDataItem;
                            GridColumn Column = dgSiflRateCatalog.MasterTableView.GetColumn("StartDT");
                            string UwaValue = DataItem["StartDT"].Text.Trim();
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["StartDT"];
                            TableCell cell1 = (TableCell)Item["EndDT"];
                            if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                            {
                                cell.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(cell.Text)));
                            }
                            if ((!string.IsNullOrEmpty(cell1.Text)) && (cell1.Text != "&nbsp;"))
                            {
                                cell1.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(cell1.Text)));
                            }

                            if (Item["Rate1"] != null)
                            {
                                Item["Rate1"].Text = Convert.ToString(System.Math.Round(Convert.ToDecimal(Item["Rate1"].Text.Trim()), 4));
                            }


                            if (Item["Rate2"] != null)
                            {
                                Item["Rate2"].Text = Convert.ToString(System.Math.Round(Convert.ToDecimal(Item["Rate2"].Text.Trim()), 4));
                            }

                            if (Item["Rate3"] != null)
                            {
                                Item["Rate3"].Text = Convert.ToString(System.Math.Round(Convert.ToDecimal(Item["Rate3"].Text.Trim()), 4));
                            }

                            if (Item["TerminalCHG"] != null)
                            {
                                Item["TerminalCHG"].Text = Convert.ToString(System.Math.Round(Convert.ToDecimal(Item["TerminalCHG"].Text.Trim()), 4));
                            }


                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {

                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }

        /// <summary>
        /// Highlight the selected item in the grid on edit
        /// </summary>
        private void SelectItem()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedFareLevelID"] != null)
                        {
                            string ID = Session["SelectedFareLevelID"].ToString();
                            foreach (GridDataItem Item in dgSiflRateCatalog.MasterTableView.Items)
                            {
                                if (Item["FareLevelID"].Text.Trim() == ID.Trim())
                                {
                                    Item.Selected = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            DefaultSelection(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }

        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgSiflRateCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSiflRateCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the delete button in the grid
                            LinkButton lbtnDeleteButton = (e.Item as GridCommandItem).FindControl("lbtnDelete") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnDeleteButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }


        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }

        }
        /// <summary>
        /// Bind siflrate catalog data into grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSiflRateCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient SiflRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {

                            var SiflRate = SiflRateService.GetFareLevel();
                            List<FlightPakMasterService.FareLevel> lstFareLevel = new List<FlightPakMasterService.FareLevel>();
                            if (SiflRate.ReturnFlag == true)
                            {
                                lstFareLevel = SiflRate.EntityList;
                            }
                            dgSiflRateCatalog.DataSource = lstFareLevel;
                            Session["SiflRateList"] = lstFareLevel;
                            //Session.Remove("FareLevelCD");
                            //List<string> SiflRateCodesList = new List<string>();
                            //foreach (FareLevel SiflRateEntity in SiflRate.EntityList)
                            //{
                            //    SiflRateCodesList.Add(SiflRateEntity.FareLevelCD.ToString().Trim());
                            //}
                            //Session["FareLevelCD"] = SiflRateCodesList;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                    }
                }
            }

        }

        /// <summary>
        /// Item Command for siflrate catalog grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSiflRateCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                //Lock will happen from UI
                                if (Session["SelectedFareLevelID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.FareLevel, Convert.ToInt64(Session["SelectedFareLevelID"].ToString().Trim()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FareLevel);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        SelectItem();
                                        DisableLinks();
                                    }
                                }
                                tbFare1.Focus();
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                dgSiflRateCatalog.SelectedIndexes.Clear();
                                tbFare1.Focus();
                                DisableLinks();
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }
        /// <summary>
        /// Update Command for siflrate catalog grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgSiflRateCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedFareLevelID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = Service.UpdateFareLevel(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    //Unlock should happen from Service Layer?
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.FareLevel, Convert.ToInt64(Session["SelectedFareLevelID"].ToString().Trim()));
                                        GridEnable(true, true, true);
                                        SelectItem();
                                        ReadOnlyForm();
                                    }
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FareLevel);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }

        /// <summary>
        /// Update Command for siflrate catalog grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgSiflRateCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                        {
                            var ReturnValue = Service.AddFareLevel(GetItems());
                            if (ReturnValue.ReturnFlag == true)
                            {
                                dgSiflRateCatalog.Rebind();
                                DefaultSelection(false);

                                //using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                //{
                                List<FareLevel> FareLevelList = new List<FareLevel>();

                                var Sifl = Service.GetFareLevel();

                                if (Sifl.ReturnFlag == true)
                                {
                                    FareLevelList = Sifl.EntityList.ToList();
                                }
                                System.Nullable<Int32> LatestCode =
                                 (from code in FareLevelList
                                  select code.FareLevelCD)
                                 .Max();
                                int CurrentPageIndex = dgSiflRateCatalog.CurrentPageIndex;

                                // Traverse to all pages in the RadGrid until the newly inserted item is found
                                for (int Page = 0; Page < dgSiflRateCatalog.PageCount; Page++)
                                {
                                    foreach (GridDataItem Item in dgSiflRateCatalog.Items)
                                    {
                                        if (Item["FareLevelCD"].Text.Trim() == Convert.ToString(LatestCode))
                                        {
                                            Item.Selected = true;
                                            Session["SelectedFareLevelID"] = Item["FareLevelID"].Text;
                                            CurrentPageIndex = -1; // flag exit
                                            break;
                                        }
                                    }
                                    // If item is found then exit dgSiflRateCatalog page loop
                                    if (CurrentPageIndex.Equals(-1))
                                    {
                                        break;
                                    }
                                    // Go to next dgSiflRateCatalog page
                                    CurrentPageIndex++;
                                    if (CurrentPageIndex >= dgSiflRateCatalog.PageCount)
                                    {
                                        CurrentPageIndex = 0;
                                    }
                                    dgSiflRateCatalog.CurrentPageIndex = CurrentPageIndex;
                                    dgSiflRateCatalog.Rebind();
                                }
                                EnableForm(false);
                                ReadOnlyForm();
                                //}
                                GridEnable(true, true, true);
                                ShowSuccessMessage();
                                EnableLinks();
                                _selectLastModified = true;
                            }
                            else
                            {
                                //For Data Anotation
                                ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.FareLevel);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }

        /// <summary>
        /// Command for updating value as delete in the database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgSiflRateCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["SelectedFareLevelID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.FareLevel FareLevel = new FlightPakMasterService.FareLevel();
                                    string Code = Session["SelectedFareLevelID"].ToString();
                                    strFareLevelCD = "";
                                    strFareLevelID = "";
                                    foreach (GridDataItem Item in dgSiflRateCatalog.MasterTableView.Items)
                                    {
                                        if (Item["FareLevelID"].Text.Trim() == Code.Trim())
                                        {
                                            strFareLevelCD = Item["FareLevelCD"].Text.Trim();
                                            strFareLevelID = Item["FareLevelID"].Text.Trim();
                                            FareLevel.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                            break;
                                        }
                                    }

                                    FareLevel.FareLevelCD = Convert.ToInt32(strFareLevelCD);
                                    FareLevel.FareLevelID = Convert.ToInt64(strFareLevelID);
                                    FareLevel.IsDeleted = true;
                                    //Lock will happen from UI

                                    var returnValue = CommonService.Lock(EntitySet.Database.FareLevel, Convert.ToInt64(strFareLevelID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FareLevel);
                                        return;
                                    }
                                    Service.DeleteFareLevel(FareLevel);

                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgSiflRateCatalog.Rebind();
                                    DefaultSelection(true);
                                    GridEnable(true, true, true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                    }
                    finally
                    {
                        // For Positive and negative cases need to unlock the record
                        // The same thing should be done for Edit
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FareLevel, Convert.ToInt64(strFareLevelID));
                    }
                }
            }

        }

        /// <summary>
        /// Bind selected item in the session for update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSiflRateCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem Item = dgSiflRateCatalog.SelectedItems[0] as GridDataItem;
                                Session["SelectedFareLevelID"] = Item["FareLevelID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                    }
                }
            }
        }


        protected void SiflRateCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgSiflRateCatalog.Rebind();
            dgSiflRateCatalog.ClientSettings.Scrolling.ScrollTop = "0";
            GridEnable(true, true, true);
            SiflRatePageNavigated = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SiflRateCatalog_PreRender(object sender, EventArgs e)
        {
            //GridEnable(true, true, true);
            if (hdnSave.Value != "Save")
            {
                SelectItem();
            }
            else if (SiflRatePageNavigated)
            {
                SelectItem();
            }
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgSiflRateCatalog, Page.Session);
        }

        /// <summary>
        /// Display insert form, when click on add button
        /// </summary>
        protected void DisplayInsertForm()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {


                        hdnSave.Value = "Save";
                        ClearForm();
                        EnableForm(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }

        }

        /// <summary>
        /// Command event trigger for save or update siflrate catalog data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if ((!string.IsNullOrEmpty(((TextBox)ucFromDate.FindControl("tbDate")).Text)) && (!string.IsNullOrEmpty(((TextBox)ucToDate.FindControl("tbDate")).Text)))
                            {

                                ValidateSiflDateFormat();
                                bool IsDateValid = CheckAlreadyExistsDate(); //Defect : 2164(OntimeID)
                                if (IsDateValid)
                                {
                                    if (DateStart > DateEnd)
                                    {
                                        
                                        string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                            + @" oManager.radalert('Start Date should be lesser than or equal to End Date', 360, 50, 'SIFL Rate Catalog');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                    }
                                    else
                                    {
                                        if (hdnSave.Value == "Update")
                                        {
                                            (dgSiflRateCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                        }
                                        else
                                        {
                                            (dgSiflRateCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                                        }
                                    }
                                }
                                else
                                {
                                    
                                    string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                        + @" oManager.radalert('Duplicate Dates or Overlapping Dates are not allowed.', 360, 50, 'SIFL Rate Catalog');";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                }
                            }
                            else
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + " oManager.radalert('Start Date and End Date should not be Empty', 360, 50, 'SIFL Rate Catalog');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }

        /// <summary>
        /// Cancel siflrate catalog form informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedFareLevelID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.FareLevel, Convert.ToInt64(Session["SelectedFareLevelID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        /// <summary>
        /// Method to store the values in the FareLevel table
        /// </summary>
        /// <param name="FareLevelCode"></param>
        /// <returns></returns>
        private FareLevel GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                FlightPakMasterService.FareLevel SiflRate = null;
                try
                {
                    SiflRate = new FlightPakMasterService.FareLevel();
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedFareLevelID"] != null)
                            {
                                SiflRate.FareLevelID = Convert.ToInt64(Session["SelectedFareLevelID"].ToString().Trim());
                            }
                        }

                        if (!string.IsNullOrEmpty(hdnCode.Value))
                        {
                            SiflRate.FareLevelCD = Convert.ToInt32(hdnCode.Value);
                        }

                        if (!string.IsNullOrEmpty(((TextBox)ucFromDate.FindControl("tbDate")).Text))
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                SiflRate.StartDT = Convert.ToDateTime(FormatDate(((TextBox)ucFromDate.FindControl("tbDate")).Text, DateFormat));
                            }
                            else
                            {
                                SiflRate.StartDT = Convert.ToDateTime(((TextBox)ucFromDate.FindControl("tbDate")).Text);
                            }
                        }

                        if (!string.IsNullOrEmpty(((TextBox)ucToDate.FindControl("tbDate")).Text))
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                SiflRate.EndDT = Convert.ToDateTime(FormatDate(((TextBox)ucToDate.FindControl("tbDate")).Text, DateFormat));
                            }
                            else
                            {
                                SiflRate.EndDT = Convert.ToDateTime(((TextBox)ucToDate.FindControl("tbDate")).Text);
                            }
                        }


                        //if (!string.IsNullOrEmpty(((TextBox)ucFromDate.FindControl("tbDate")).Text))
                        //{
                        //    SiflRate.StartDT = DateTime.ParseExact(((TextBox)ucFromDate.FindControl("tbDate")).Text.Trim(), DateFormat, CultureInfo.InvariantCulture);
                        //}

                        //if (!string.IsNullOrEmpty(((TextBox)ucToDate.FindControl("tbDate")).Text))
                        //{
                        //    SiflRate.EndDT = DateTime.ParseExact(((TextBox)ucToDate.FindControl("tbDate")).Text.Trim(), DateFormat, CultureInfo.InvariantCulture);
                        //}
                        if (!string.IsNullOrEmpty(tbFare1.Text))
                        {
                            SiflRate.Rate1 = Convert.ToDecimal(tbFare1.Text, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            SiflRate.Rate1 = Convert.ToDecimal("0.0000", CultureInfo.CurrentCulture);
                        }
                        if (!string.IsNullOrEmpty(tbFare2.Text))
                        {
                            SiflRate.Rate2 = Convert.ToDecimal(tbFare2.Text, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            SiflRate.Rate2 = Convert.ToDecimal("0.0000", CultureInfo.CurrentCulture);
                        }
                        if (!string.IsNullOrEmpty(tbFare3.Text))
                        {
                            SiflRate.Rate3 = Convert.ToDecimal(tbFare3.Text, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            SiflRate.Rate3 = Convert.ToDecimal("0.0000", CultureInfo.CurrentCulture);
                        }
                        if (!string.IsNullOrEmpty(tbTerminalCharge.Text))
                        {
                            SiflRate.TerminalCHG = Convert.ToDecimal(tbTerminalCharge.Text, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            SiflRate.TerminalCHG = Convert.ToDecimal("0.0000", CultureInfo.CurrentCulture);
                        }
                        SiflRate.IsInActive = chkInactive.Checked;
                        SiflRate.IsDeleted = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
                return SiflRate;
            }
        }

        /// <summary>
        /// Method to display edit form
        /// </summary>
        protected void DisplayEditForm()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LoadControlData();
                        pnlExternalForm.Visible = true;
                        hdnSave.Value = "Update";
                        hdnRedirect.Value = "";
                        EnableForm(true);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }

        }

        /// <summary>
        /// Method to display form in view mode
        /// </summary>
        protected void ReadOnlyForm()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (dgSiflRateCatalog.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgSiflRateCatalog.SelectedItems[0] as GridDataItem;
                            Label lbLastUpdatedUser = (Label)dgSiflRateCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (Item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                            }
                            else
                            {
                                lbLastUpdatedUser.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                //To retrieve back the UTC date in Homebase format
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));
                            }
                            LoadControlData();
                            EnableForm(false);
                        }
                        else
                        {
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFareLevelID"] != null)
                    {
                        strFareLevelCD = "";
                        strFareLevelCD = "";

                        foreach (GridDataItem Item in dgSiflRateCatalog.MasterTableView.Items)
                        {
                            if (Item["FareLevelID"].Text.Trim() == Session["SelectedFareLevelID"].ToString().Trim())
                            {
                                strFareLevelCD = Item["FareLevelCD"].Text.Trim();
                                strFareLevelCD = Item["FareLevelID"].Text.Trim();

                                hdnCode.Value = Item.GetDataKeyValue("FareLevelCD").ToString().Trim();


                                if (Item.GetDataKeyValue("StartDT") != null)
                                {
                                    if (DateFormat != null)
                                    {
                                        ((TextBox)ucFromDate.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("StartDT").ToString()));
                                    }
                                    else
                                    {
                                        ((TextBox)ucFromDate.FindControl("tbDate")).Text = Item.GetDataKeyValue("StartDT").ToString();
                                    }
                                }
                                else
                                {
                                    ((TextBox)ucFromDate.FindControl("tbDate")).Text = string.Empty;
                                }

                                //if (Item.GetDataKeyValue("StartDT") != null)
                                //{
                                //    string Startdt = Item.GetDataKeyValue("StartDT").ToString().Trim();
                                //    ((TextBox)ucFromDate.FindControl("tbDate")).Text = Convert.ToDateTime(Startdt).ToString(DateFormat, CultureInfo.InvariantCulture);
                                //}


                                if (Item.GetDataKeyValue("EndDT") != null)
                                {
                                    if (DateFormat != null)
                                    {
                                        ((TextBox)ucToDate.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("EndDT").ToString()));
                                    }
                                    else
                                    {
                                        ((TextBox)ucToDate.FindControl("tbDate")).Text = Item.GetDataKeyValue("EndDT").ToString();
                                    }
                                }
                                else
                                {
                                    ((TextBox)ucToDate.FindControl("tbDate")).Text = string.Empty;
                                }

                                //if (Item.GetDataKeyValue("EndDT") != null)
                                //{
                                //    string Enddt = Item.GetDataKeyValue("EndDT").ToString();
                                //    ((TextBox)ucToDate.FindControl("tbDate")).Text = Convert.ToString(Convert.ToDateTime(Enddt).ToString(DateFormat, CultureInfo.InvariantCulture));
                                //}
                                //else
                                //{
                                //    ((TextBox)ucToDate.FindControl("tbDate")).Text = string.Empty;
                                //}
                                if (Item.GetDataKeyValue("Rate1") != null)
                                {
                                    tbFare1.Text = Item.GetDataKeyValue("Rate1").ToString().Trim();
                                }
                                else
                                {
                                    tbFare1.Text = "0.0000";
                                }
                                if (Item.GetDataKeyValue("Rate2") != null)
                                {
                                    tbFare2.Text = Item.GetDataKeyValue("Rate2").ToString().Trim();
                                }
                                else
                                {
                                    tbFare2.Text = "0.0000";
                                }
                                if (Item.GetDataKeyValue("Rate3") != null)
                                {
                                    tbFare3.Text = Item.GetDataKeyValue("Rate3").ToString().Trim();
                                }
                                else
                                {
                                    tbFare3.Text = "0.0000";
                                }
                                if (Item.GetDataKeyValue("TerminalCHG") != null)
                                {
                                    tbTerminalCharge.Text = Item.GetDataKeyValue("TerminalCHG").ToString().Trim();
                                }
                                else
                                {
                                    tbTerminalCharge.Text = "0.0000";
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Start Date";
                                lbColumnName2.Text = "End Date";
                                lbColumnValue1.Text = Item["StartDT"].Text;
                                lbColumnValue2.Text = Item["EndDT"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to clear the form
        /// </summary>
        protected void ClearForm()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ((TextBox)ucFromDate.FindControl("tbDate")).Text = string.Empty;
                        ((TextBox)ucToDate.FindControl("tbDate")).Text = string.Empty;
                        tbTerminalCharge.Text = "0.0000";
                        tbFare1.Text = "0.0000";
                        tbFare2.Text = "0.0000";
                        tbFare3.Text = "0.0000";
                        chkInactive.Checked = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }

        }

        /// <summary>
        /// Method to enable the form
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ((TextBox)ucFromDate.FindControl("tbDate")).Enabled = Enable;
                        ((TextBox)ucToDate.FindControl("tbDate")).Enabled = Enable;
                        tbTerminalCharge.Enabled = Enable;
                        tbFare1.Enabled = Enable;
                        tbFare2.Enabled = Enable;
                        tbFare3.Enabled = Enable;
                        btnSaveChanges.Visible = Enable;
                        btnCancel.Visible = Enable;
                        chkInactive.Enabled = Enable;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }

        }

        /// <summary>
        /// Method to enable the grid 
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgSiflRateCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgSiflRateCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgSiflRateCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        if (IsAuthorized(Permission.Database.AddSiflRateCatalog))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                            {
                                lbtnInsertCtl.Enabled = true;
                            }
                            else
                            {
                                lbtnInsertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.DeleteSiflRateCatalog))
                        {
                            lbtnDelCtl.Visible = false;
                            if (Delete)
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                            }
                            else
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.EditSiflRateCatalog))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                            {
                                lbtnEditCtl.Enabled = true;
                                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnEditCtl.Enabled = false;
                                lbtnEditCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FareLevel);
                }
            }
        }

        private void ValidateSiflDateFormat()
        {
            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
            string StartDate = ((TextBox)ucFromDate.FindControl("tbDate")).Text;
            string EndDate = ((TextBox)ucToDate.FindControl("tbDate")).Text;
            if (!string.IsNullOrEmpty(StartDate))
            {
                if (DateFormat != null)
                {
                    DateStart = Convert.ToDateTime(FormatDate(StartDate, DateFormat));
                }
                else
                {
                    DateStart = Convert.ToDateTime(StartDate);
                }
            }
            if (!string.IsNullOrEmpty(EndDate))
            {
                if (DateFormat != null)
                {
                    DateEnd = Convert.ToDateTime(FormatDate(EndDate, DateFormat));
                }
                else
                {
                    DateEnd = Convert.ToDateTime(EndDate);
                }
            }
        }


        private bool CheckAlreadyExistsDate()
        {
            bool IsValid = true;
            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
            string StartDate = ((TextBox)ucFromDate.FindControl("tbDate")).Text;
            string EndDate = ((TextBox)ucToDate.FindControl("tbDate")).Text;
            if (!string.IsNullOrEmpty(StartDate))
            {
                if (DateFormat != null)
                {
                    DateStart = Convert.ToDateTime(FormatDate(StartDate, DateFormat));
                }
                else
                {
                    DateStart = Convert.ToDateTime(StartDate);
                }
            }
            if (!string.IsNullOrEmpty(EndDate))
            {
                if (DateFormat != null)
                {
                    DateEnd = Convert.ToDateTime(FormatDate(EndDate, DateFormat));
                }
                else
                {
                    DateEnd = Convert.ToDateTime(EndDate);
                }
            }

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.FareLevel> lstSiflRate = new List<FlightPakMasterService.FareLevel>();
                if (Session["SiflRateList"] != null)
                {
                    lstSiflRate = (List<FlightPakMasterService.FareLevel>)Session["SiflRateList"];
                }
                if (lstSiflRate != null && lstSiflRate.Count != 0)
                {
                    lstSiflRate = lstSiflRate.Where(x => x.StartDT == DateStart || x.EndDT == DateEnd || (x.EndDT == DateEnd && x.EndDT == DateEnd)).ToList();
                }
            }
            using (FlightPakMasterService.MasterCatalogServiceClient SiflRateService = new FlightPakMasterService.MasterCatalogServiceClient())
            {

                var SiflRate = SiflRateService.CheckSiflDate(DateStart,DateEnd);
                List<FlightPakMasterService.FareLevel> lstFareLevel = new List<FlightPakMasterService.FareLevel>();
                if (SiflRate.ReturnFlag == true)
                {
                    if (hdnSave.Value == "Update")
                    {
                        if (SiflRate != null && SiflRate.EntityList != null && SiflRate.EntityList.Count > 1)
                        {
                            IsValid = false;
                        }
                    }
                    else
                    {
                        if (SiflRate != null && SiflRate.EntityList != null && SiflRate.EntityList.Count > 0)
                        {
                            IsValid = false;
                        }
                    }
                    if (IsValid == false)
                    {
                        List<string> msgs = new List<string>();
                        foreach (var sifl in SiflRate.EntityList)
                        {
                            string activeOrInactive = "";
                               
                            if (sifl.IsInActive == false)
                            {
                                activeOrInactive = "another active SIFL entry ";
                            }
                            else
                            {
                                activeOrInactive = "an inactive SIFL entry ";                               
                            }

                            if (sifl.StartDT == DateStart)
                            {
                                msgs.Add(string.Format("{0} is already duplicated by {1}. Please amend the start date.", StartDate,activeOrInactive));
                            }
                            else if (sifl.EndDT == DateEnd)
                            {
                                msgs.Add(string.Format("{0} is already duplicated by {1}. Please amend the start date.", EndDate,activeOrInactive));
                            }
                            else if(sifl.StartDT.HasValue && sifl.EndDT.HasValue)
                            {
                                msgs.Add(string.Format("{0} overlaps with {1} {2} - {3}. Please amend the entry.", StartDate + " - "+ EndDate, activeOrInactive, sifl.StartDT.Value.ToString(DateFormat), sifl.EndDT.Value.ToString(DateFormat)));
                            }
                            if (msgs.Count > 0)
                            {
                                break;
                            }
                        }
                        string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                             + @" oManager.radalert('"+string.Join(". ",msgs.ToArray<string>())+"', 360, 50, 'SIFL Rate Catalog');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                    }
                }
            }
            return IsValid;
        }



        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.FareLevel> lstSiflRate = new List<FlightPakMasterService.FareLevel>();
                if (Session["SiflRateList"] != null)
                {
                    lstSiflRate = (List<FlightPakMasterService.FareLevel>)Session["SiflRateList"];
                }
                if (lstSiflRate.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstSiflRate = lstSiflRate.Where(x => x.IsInActive != true || x.IsInActive==null).ToList<FlightPakMasterService.FareLevel>(); }
                    dgSiflRateCatalog.DataSource = lstSiflRate;
                    if (IsDataBind)
                    {
                        dgSiflRateCatalog.DataBind();
                        dgSiflRateCatalog.SelectedIndexes.Add(0);
                        GridDataItem item = dgSiflRateCatalog.SelectedItems[0] as GridDataItem;
                        Session["SelectedFareLevelID"] = item.GetDataKeyValue("FareLevelID").ToString().Trim();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgSiflRateCatalog.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    chkInactive.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var SiflValue = FPKMstService.GetFareLevel();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, SiflValue);
            List<FlightPakMasterService.FareLevel> filteredList = GetFilteredList(SiflValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FareLevelID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgSiflRateCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgSiflRateCatalog.CurrentPageIndex = PageNumber;
            dgSiflRateCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfFareLevel SiflValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedFareLevelID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = SiflValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FareLevelID;
                Session["SelectedFareLevelID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.FareLevel> GetFilteredList(ReturnValueOfFareLevel SiflValue)
        {
            List<FlightPakMasterService.FareLevel> filteredList = new List<FlightPakMasterService.FareLevel>();

            if (SiflValue.ReturnFlag == true)
            {
                filteredList = SiflValue.EntityList;
            }

            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<FlightPakMasterService.FareLevel>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgSiflRateCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgSiflRateCatalog.Rebind();
                    SelectItem();
                }
            }
        }
    }
}