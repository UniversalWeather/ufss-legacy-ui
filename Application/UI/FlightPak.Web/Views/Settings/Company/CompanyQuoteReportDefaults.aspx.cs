﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common;
using FlightPak.Web.FlightPakMasterService;
using System.IO;
using System.Text.RegularExpressions;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class CompanyQuoteReportDefaults : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public Int64 HomeBaseID;
        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["CQQuoteDicImg"]; }
            set { Session["CQQuoteDicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicCQQuoteImgDelete"]; }
            set { Session["DicCQQuoteImgDelete"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["HomeBaseID"]))
            {
                HomeBaseID = Convert.ToInt64(Request.QueryString["HomeBaseID"].Trim());
            }
            if (HomeBaseID != null)
            {
                ltlHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Quote Report Default");
                using (MasterCatalogServiceClient objMaster = new MasterCatalogServiceClient())
                {
                    var objCompany = objMaster.GetListInfoByHomeBaseId(HomeBaseID).EntityList;
                    if (objCompany != null && objCompany.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(objCompany[0].HomebaseCD))
                        {
                            ltlHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Quote Report Default " + "- " + objCompany[0].HomebaseCD + " - " + objCompany[0].BaseDescription);
                        }
                        else
                        {
                            ltlHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Quote Report Default");
                        }
                    }
                    else
                    {
                        ltlHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Quote Report Default");
                    }
                }
            }

            if (!IsPostBack)
            {
                ClearFields();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                ddlImg.Enabled = true;
                fileUL.Enabled = true;
                btndeleteImage.Enabled = true;
                LoadImages();
                if (Session["CompanyInsert"].ToString() != "Insert" && Session["CQQuoteFooterList"] == null)
                {
                    LoadFooter();
                }
                else
                {
                    ddlFooterInfo.Items.Add(new ListItem("None", "0"));
                    ddlFooterInfo.DataBind();
                }

                if (Session["CQQuoteFooterList"] != null)
                {
                    List<FlightPakMasterService.CQMessage> FooterListQuote = new List<FlightPakMasterService.CQMessage>();
                    FooterListQuote = (List<FlightPakMasterService.CQMessage>)Session["CQQuoteFooterList"];
                    ddlFooterInfo.DataSource = FooterListQuote;
                    ddlFooterInfo.DataTextField = "MessageDescription";
                    ddlFooterInfo.DataValueField = "MessageLine";
                    ddlFooterInfo.DataBind();
                    if (Session["CQQuoteFooter"] != null)
                    {
                        string Item = Session["CQQuoteFooter"].ToString();
                        if (ddlFooterInfo.Items.FindByText(Item) != null)
                        {
                            ddlFooterInfo.Items.FindByText(Item).Selected = true;
                            tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text;
                        }
                    }
                }
                if (HomeBaseID != null && HomeBaseID != 0)
                {
                    if ((Session["CqCompanyStringQuoteReport"] == null) && (Session["CqCompanyCheckQuoteReport"] == null) && (Session["CqCompanyRadQuoteReport"] == null))
                        LoadQuoteReportDefaults();
                    else
                        AssignQuoteReport();

                }
                else
                {
                    if ((Session["CqCompanyStringQuoteReport"] == null) && (Session["CqCompanyCheckQuoteReport"] == null) && (Session["CqCompanyRadQuoteReport"] == null))
                        LoadSummaryDescription();
                    else
                        AssignQuoteReport();

                }
            }
            else
            {
                string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                if (eventArgument == "LoadImage")
                {
                    LoadImage();
                }
                else if (eventArgument == "DeleteImage_Click")
                {
                    DeleteImage_Click(sender, e);
                }
            }
        }

        private void LoadSummaryDescription()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbDailyDesc.Text = "DAILY USAGE ADJUSTMENT:";
                tbAdditionalDesc.Text = "ADDITIONAL FEES:";
                tbLandingDesc.Text = "LANDING FEES:";
                tbSegmentDesc.Text = "SEGMENT FEES:";
                tbRonCrewDesc.Text = "RON CREW CHARGE:";
                tbCrewAddCrewDesc.Text = "ADDITIONAL CREW CHARGE:";
                tbAddCrewRonDesc.Text = "ADDITIONAL CREW RON CHARGE:";
                tbWaitDesc.Text = "WAIT CHARGE:";
                tbFlightDesc.Text = "FLIGHT CHARGE:";
                tbSubtotalDesc.Text = "SUBTOTAL:";
                tbDiscountDesc.Text = "DISCOUNT AMOUNT:";
                tbTaxesDesc.Text = "TAXES";
                tbTotalQuoteDesc.Text = "TOTAL QUOTE";
                tbPrepaidDesc.Text = "PREPAID";
                tbRemainingAmtDesc.Text = "REMAINING AMOUNT DUE:";
            }
        }

        private void LoadFooter()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var RetValue = objMasterService.GetCQMessage("Q", HomeBaseID).EntityList.ToList();
                    if (RetValue != null && RetValue.Count() > 0)
                    {
                        ddlFooterInfo.Items.Add(new ListItem("None", "0"));
                        ddlFooterInfo.DataSource = RetValue;
                        ddlFooterInfo.DataTextField = "MessageDescription";
                        ddlFooterInfo.DataValueField = "MessageLine";
                        ddlFooterInfo.DataBind();
                        if (Session["CQQuoteFooter"] != null)
                        {
                            string Item = Session["CQQuoteFooter"].ToString();
                            if (ddlFooterInfo.Items.FindByText(Item) != null)
                            {
                                // ddlFooterInfo.SelectedIndex = 0;
                                ddlFooterInfo.Items.FindByText(Item).Selected = true;
                                tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text;
                            }
                        }
                    }
                    else
                    {
                        ddlFooterInfo.Items.Add(new ListItem("None", "0"));
                        ddlFooterInfo.DataBind();
                    }
                }
            }
        }

        private void LoadImages()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                #region Get Image from Database


                if (Session["CqQuoteDicImg"] == null && Session["CompanyInsert"].ToString() != "Insert")
                {
                    CreateDictionayForImgUpload();
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    // ImgPopup.ImageUrl = null;

                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ObjRetImg = ObjImgService.GetFileWarehouseList("CqCompanyQuote", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList;
                        ddlImg.Items.Clear();
                        ddlImg.Text = "";                       
                        Session["CqQuoteReportBase64"] = null;   //added for image issue
                        int i = 0;
                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                        {
                            Dictionary<string, byte[]> CqQuoteDicImg = (Dictionary<string, byte[]>)Session["CqQuoteDicImg"];
                            string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.UWAFileName);
                            ddlImg.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                            CqQuoteDicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                            if (i == 0)
                            {
                                byte[] picture = fwh.UWAFilePath;

                                //Ramesh: Set the URL for image file or link based on the file type
                                SetURL(fwh.UWAFileName, picture);

                                ////start of modification for image issue
                                //if (hdnBrowserName.Value == "Chrome")
                                //{
                                //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                //}
                                //else
                                //{
                                //    Session["CqQuoteReportBase64"] = picture;
                                   
                                //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid());
                                //}
                                ////end of modification for image issue
                            }
                            i = i + 1;
                        }
                        if (ddlImg.Items.Count <= 0)
                        {
                            ddlImg.SelectedIndex = 0;
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            ImgQuotePopup.ImageUrl = null;
                        }

                    }
                }
                else
                {
                    Dictionary<string, byte[]> dicImgs = new Dictionary<string, byte[]>();
                    if (Session["CQQuoteDicImg"] == null)
                        Session["CQQuoteDicImg"] = new Dictionary<string, byte[]>();
                    dicImgs = (Dictionary<string, byte[]>)Session["CQQuoteDicImg"];
                    string FileName = string.Empty;
                    Byte[] Input = new byte[0];
                    foreach (var DicItem in dicImgs)
                    {
                        FileName = DicItem.Key;
                        Input = DicItem.Value;
                    }
                    ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(FileName, FileName));
                    //start of modification for image issue
                    if (hdnBrowserName.Value == "Chrome")
                    {
                        imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                    }
                    else
                    {
                        Session["CqQuoteReportBase64"] = Input;                       
                        imgFile.ImageUrl = "../ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid();
                    }
                    if (ddlImg.Items.Count > 0)
                    {
                        ddlImg.SelectedIndex = 0;
                        btndeleteImage.Enabled = true;
                    }
                    if (dicImgs.Count() <= 0)
                    {
                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                        ImgQuotePopup.ImageUrl = null;
                    }

                }
                #endregion
            }
        }

        private void ClearFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCharterCompInfo.Text = string.Empty;
                tbTopOffset.Text = string.Empty;
                chkCityDetail.Checked = false;
                chkAirportName.Checked = false;
                chkIcaoId.Checked = false;
                tbCustomHeaderInfo.Text = string.Empty;
                tbCustomFooterInfo.Text = string.Empty;
                fileUL.Enabled = true;
                ddlImg.Enabled = true;
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                //ImgPopup.ImageUrl = null;
                ddlImg.Items.Clear();
                ddlImg.Text = "";
                tbImgName.Text = string.Empty;
                radlstLogoPosition.SelectedIndex = 0;
                RadioImagePosition.SelectedIndex = 0;
                //radSalesLogoPosition.SelectedIndex = 0;
                chkPrintFirstPage.Checked = false;
                radAllLegs.Checked = false;
                radTwoimages.Checked = false;
                radthreeimages.Checked = false;
                radfourimages.Checked = false;
                chkDailyPrint.Checked = false;
                tbDailyDesc.Text = string.Empty;
                chkDailyUnits.Checked = false;
                chkAdditionalPrint.Checked = false;
                tbAdditionalDesc.Text = string.Empty;
                chkLandingPrint.Checked = false;
                tbLandingDesc.Text = string.Empty;
                chkSegmentPrint.Checked = false;
                tbSegmentDesc.Text = string.Empty;
                chkRonCrewPrint.Checked = false;
                tbRonCrewDesc.Text = string.Empty;
                chkRonCrewUnits.Checked = false;
                chkAddCrewPrint.Checked = false;
                tbCrewAddCrewDesc.Text = string.Empty;
                chkAddCrewUnits.Checked = false;
                chkAddCrewRonPrint.Checked = false;
                tbAddCrewRonDesc.Text = string.Empty;
                chkAddCrewRonUnits.Checked = false;
                chkWaitPrint.Checked = false;
                tbWaitDesc.Text = string.Empty;
                chkWaitUnits.Checked = false;
                chkFlightPrint.Checked = false;
                tbFlightDesc.Text = string.Empty;
                chkFlightUnits.Checked = false;
                chkSubtotalPrint.Checked = false;
                tbSubtotalDesc.Text = string.Empty;
                chkDiscountPrint.Checked = false;
                tbDiscountDesc.Text = string.Empty;
                chkDiscountUnits.Checked = false;
                chkTaxesAmt.Checked = false;
                tbTaxesDesc.Text = string.Empty;
                chkTotalQuotePrint.Checked = false;
                tbTotalQuoteDesc.Text = string.Empty;
                chkPrepaidPrint.Checked = false;
                tbPrepaidDesc.Text = string.Empty;
                chkRemainingAmtPrint.Checked = false;
                tbRemainingAmtDesc.Text = string.Empty;
                chkDetail.Checked = false;
                chkSubTotal.Checked = false;
                chkAdditionalFees.Checked = false;
                chkQuoteSummary.Checked = false;
                chkLegNum.Checked = false;
                chkDepartDate.Checked = false;
                chkFromDescription.Checked = false;
                chkToDescription.Checked = false;
                chkQuotedAmount.Checked = false;
                chkFlightHours.Checked = false;
                chkMiles.Checked = false;
                chkPassengerCount.Checked = false;
                radNone.Checked = false;
                radFileQNumber.Checked = false;
                chkAllowEdit.Checked = false;
                chkQuoteFeeDescription.Checked = false;
                chkQuotedAmt.Checked = false;
                chkChargeUnit.Checked = false;
                chkQuantity.Checked = false;
                chkRate.Checked = false;
                chkCharterCompanyInfo.Checked = false;
                chkCustomerInfo.Checked = false;
                chkDescription.Checked = false;
                chkSalesPerson.Checked = false;
                chkTailNum.Checked = false;
                chkAircraftType.Checked = false;
                tbReportTitle.Text = string.Empty;
                tbCustomFooterInfo.Text = string.Empty;
                tbCustomFooterInfo.Enabled = false;
                chkIsRptQuoteDetailArrivalDate.Checked = false;
                chkIsRptQuoteDetailQuoteDate.Checked = false;
            }
        }

        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                trAddMessage.Visible = true;
                lbtxt.Text = "Add Message";
                tbAddMessage.Text = string.Empty;
                hdnEdit.Value = string.Empty;
                btnSaveChanges.Visible = false;
                btnEditChanges.Visible = false;
                btnDelete.Visible = false;
            }
        }

        protected void btnEditChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (ddlFooterInfo.Items.Count > 0)
                    {
                        if (ddlFooterInfo.SelectedIndex == 0)
                        {
                            RadWindowManager1.RadAlert("The default message cannot be Edited", 400, 100, ModuleNameConstants.Database.Company, "", null);
                        }
                        else
                        {
                            trAddMessage.Visible = true;
                            lbtxt.Text = "Edit Message";
                            btnSaveChanges.Visible = false;
                            btnEditChanges.Visible = false;
                            btnDelete.Visible = false;
                            hdnEdit.Value = "true";
                            if (ddlFooterInfo.Items.Count > 0)
                            {
                                tbAddMessage.Text = ddlFooterInfo.SelectedItem.Text.ToString();
                                tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text.ToString();
                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                var items = ddlFooterInfo.Items;
                List<FlightPakMasterService.CQMessage> FooterList = new List<FlightPakMasterService.CQMessage>();
                if (items != null)
                {
                    //foreach (var item in items.Cast<ListItem>().Where(x => x.Value != "-1"))

                    Int64 i = 0;
                    foreach (var item in items.Cast<ListItem>())
                    {
                        i = i + 1;

                        FlightPakMasterService.CQMessage objMessage = new FlightPakMasterService.CQMessage();
                        objMessage.HomebaseID = HomeBaseID;
                        objMessage.MessageTYPE = "Q";
                        objMessage.MessageCD = i;
                        objMessage.MessageDescription = item.Text;
                        objMessage.MessageLine = item.Text;
                        objMessage.IsDeleted = false;

                        FooterList.Add(objMessage);
                    }
                }
                Session["CQQuoteFooterList"] = FooterList;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (ddlFooterInfo.Items.Count > 0)
                {
                    if (ddlFooterInfo.SelectedIndex == 0)
                    {
                        RadWindowManager1.RadAlert("The default message cannot be deleted", 400, 100, ModuleNameConstants.Database.Company, "", null);
                    }
                    else
                    {
                        ddlFooterInfo.Items.Remove(ddlFooterInfo.SelectedItem);
                    }
                }
            }

        }


        protected void btnSaveMsg_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                trAddMessage.Visible = false;
                btnSaveChanges.Visible = true;
                btnEditChanges.Visible = true;
                btnDelete.Visible = true;
                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var RetValue = objMasterService.GetCQMessage("Q", HomeBaseID).EntityList.ToList().Where(x => x.MessageDescription != null && x.MessageDescription.ToUpper().Equals(tbAddMessage.Text.ToUpper()));
                    if (RetValue != null && RetValue.Count() > 0)
                    {
                        RadWindowManager1.RadAlert("Custom Footer Information must be Unique", 500, 50, ModuleNameConstants.Database.Company, "");

                    }
                    else
                    {

                        if (hdnEdit.Value == "true")
                        {
                            ddlFooterInfo.Items.Remove(ddlFooterInfo.SelectedItem);
                        }
                        ddlFooterInfo.Items.Add(tbAddMessage.Text);
                        ddlFooterInfo.DataBind();
                        ddlFooterInfo.SelectedValue = tbAddMessage.Text;
                        tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text.ToString();
                    }
                }


            }
            //if (hdnEdit.Value == "true")
            //{
            //    ddlFooterInfo.Items.Remove(ddlFooterInfo.SelectedItem);
            //}
            //ddlFooterInfo.Items.Add(tbAddMessage.Text);
            //ddlFooterInfo.DataBind();
            //ddlFooterInfo.SelectedValue = tbAddMessage.Text;
            //ddlFooterInfo.SelectedItem.Selected = true;
            //tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.ToString();

        }

        protected void btnCancelMsg_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                trAddMessage.Visible = false;
                btnSaveChanges.Visible = true;
                btnEditChanges.Visible = true;
                btnDelete.Visible = true;
            }
        }

        protected void ddlFooterInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                tbCustomFooterInfo.Text = ddlFooterInfo.SelectedItem.Text.ToString();

            }
        }

        private void LoadQuoteReportDefaults()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(HomeBaseID).EntityList.ToList();
                if (CompanyList.Count > 0)
                {
                    GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                    if (CompanyCatalogEntity.ChtQuoteCompany != null)
                    {
                        tbCharterCompInfo.Text = CompanyCatalogEntity.ChtQuoteCompany;
                    }
                    if (CompanyCatalogEntity.QuotePageOff != null)
                    {
                        tbTopOffset.Text = CompanyCatalogEntity.QuotePageOff.ToString();
                    }
                    if (CompanyCatalogEntity.IsChangeQueueCity != null)
                    {
                        chkCityDetail.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsChangeQueueCity);
                    }
                    if (CompanyCatalogEntity.IsChangeQueueAirport != null)
                    {
                        chkAirportName.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsChangeQueueAirport);
                    }
                    if (CompanyCatalogEntity.IsQuoteChangeQueueICAO != null)
                    {
                        chkIcaoId.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteChangeQueueICAO);
                    }
                    if (CompanyCatalogEntity.ChtQouteHeaderDetailRpt != null)
                    {
                        tbCustomHeaderInfo.Text = CompanyCatalogEntity.ChtQouteHeaderDetailRpt;
                    }
                    if (CompanyCatalogEntity.ChtQouteFooterDetailRpt != null)
                    {
                        tbCustomFooterInfo.Text = CompanyCatalogEntity.ChtQouteFooterDetailRpt;
                    }
                    //if (!string.IsNullOrEmpty(tbCustomFooterInfo.Text))
                    //{
                    //    ddlFooterInfo.DataBind();
                    //    ddlFooterInfo.Items.FindByValue(tbCustomFooterInfo.Text).Selected = true;
                    //}
                    if (CompanyCatalogEntity.QuoteLogoPosition != null)
                    {
                        radlstLogoPosition.SelectedIndex = Convert.ToInt32(CompanyCatalogEntity.QuoteLogoPosition);
                    }
                    if (CompanyCatalogEntity != null)
                    {
                        //radSalesLogoPosition.SelectedIndex = Convert.ToInt32(CompanyCatalogEntity.QuoteSalesLogoPosition);
                    }
                    if (CompanyCatalogEntity.ImagePosition != null)
                    {
                        RadioImagePosition.SelectedIndex = Convert.ToInt32(CompanyCatalogEntity.ImagePosition);
                    }
                    if (CompanyCatalogEntity.ImageCnt != null)
                    {
                        if (CompanyCatalogEntity.ImageCnt == 1)
                            radAllLegs.Checked = true;
                        else if (CompanyCatalogEntity.ImageCnt == 2)
                            radTwoimages.Checked = true;
                        else if (CompanyCatalogEntity.ImageCnt == 3)
                            radthreeimages.Checked = true;
                        else if (CompanyCatalogEntity.ImageCnt == 4)
                            radfourimages.Checked = true;
                    }
                    if (CompanyCatalogEntity.IsQuoteFirstPage != null)
                    {
                        chkPrintFirstPage.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFirstPage);
                    }
                    #region Information tab2
                    if (CompanyCatalogEntity.IsChangeQueueDetail != null)
                    {
                        chkDetail.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsChangeQueueDetail);
                    }
                    if (CompanyCatalogEntity.IsQuoteChangeQueueSubtotal != null)
                    {
                        chkSubTotal.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteChangeQueueSubtotal);
                    }
                    if (CompanyCatalogEntity.IsQuoteChangeQueueFees != null)
                    {
                        chkAdditionalFees.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteChangeQueueFees);
                    }
                    if (CompanyCatalogEntity.IsQuoteChangeQueueSum != null)
                    {
                        chkQuoteSummary.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteChangeQueueSum);
                    }

                    if (CompanyCatalogEntity.IsQuotePrepaidMinimumUsage2FeeAdj != null)
                    {
                        chkDailyPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidMinimumUsage2FeeAdj);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteMinimumUse2FeeDepositAdj))
                    {
                        tbDailyDesc.Text = CompanyCatalogEntity.QuoteMinimumUse2FeeDepositAdj.ToString();
                    }
                    else
                    {
                        tbDailyDesc.Text = "DAILY USAGE ADJUSTMENT:";
                    }
                    if (CompanyCatalogEntity.IsQuotePrepaidMin2UsageFeeAdj != null)
                    {
                        chkDailyUnits.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidMin2UsageFeeAdj);
                    }
                    if (CompanyCatalogEntity.IsQuoteAdditional2FeePrint != null)
                    {
                        chkAdditionalPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditional2FeePrint);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.Additional2FeeDefault))
                    {
                        tbAdditionalDesc.Text = CompanyCatalogEntity.Additional2FeeDefault;
                    }
                    else
                    {
                        tbAdditionalDesc.Text = "ADDITIONAL FEES:";
                    }
                    if (CompanyCatalogEntity.IsQuoteFlight2LandingFeePrint != null)
                    {
                        chkLandingPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFlight2LandingFeePrint);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.uote2LandingFeeDefault))
                    {
                        tbLandingDesc.Text = CompanyCatalogEntity.uote2LandingFeeDefault;
                    }
                    else
                    {
                        tbLandingDesc.Text = "LANDING FEES:";
                    }
                    if (CompanyCatalogEntity.IsQuotePrepaidSegement2Fee != null)
                    {
                        chkSegmentPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuotePrepaidSegement2Fee);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteSegment2FeeDeposit))
                    {
                        tbSegmentDesc.Text = CompanyCatalogEntity.QuoteSegment2FeeDeposit.ToString();
                    }
                    else
                    {
                        tbSegmentDesc.Text = "SEGMENT FEES:";
                    }
                    if (CompanyCatalogEntity.IsQuoteStdCrew2RONPrepaid != null)
                    {
                        chkRonCrewPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteStdCrew2RONPrepaid);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.QouteStdCrew2RONDeposit))
                    {
                        tbRonCrewDesc.Text = CompanyCatalogEntity.QouteStdCrew2RONDeposit;
                    }
                    else
                    {
                        tbRonCrewDesc.Text = "RON CREW CHARGE:";
                    }
                    if (CompanyCatalogEntity.IsQuoteUnpaidStd2CrewRON != null)
                    {
                        chkRonCrewUnits.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUnpaidStd2CrewRON);
                    }

                    if (CompanyCatalogEntity.IsQuoteAdditional2CrewPrepaid != null)
                    {
                        chkAddCrewPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditional2CrewPrepaid);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAddition2CrewDeposit))
                    {
                        tbCrewAddCrewDesc.Text = CompanyCatalogEntity.QuoteAddition2CrewDeposit;
                    }
                    else
                    {
                        tbCrewAddCrewDesc.Text = "ADDITIONAL CREW CHARGE:";
                    }
                    if (CompanyCatalogEntity.IsQuoteUnpaidAdditional2Crew != null)
                    {
                        chkAddCrewUnits.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUnpaidAdditional2Crew);
                    }

                    if (CompanyCatalogEntity.IsQuoteAdditional2CrewRON != null)
                    {
                        chkAddCrewRonPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteAdditional2CrewRON);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteAddition2CrewRON))
                    {
                        tbAddCrewRonDesc.Text = CompanyCatalogEntity.QuoteAddition2CrewRON;
                    }
                    else
                    {
                        tbAddCrewRonDesc.Text = "ADDITIONAL CREW RON CHARGE:";
                    }
                    if (CompanyCatalogEntity.IsQuoteUpaidAdditionl2CrewRON != null)
                    {
                        chkAddCrewRonUnits.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUpaidAdditionl2CrewRON);
                    }

                    if (CompanyCatalogEntity.IsQuoteWaiting2ChargePrepaid != null)
                    {
                        chkWaitPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteWaiting2ChargePrepaid);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteWaiting2Charge))
                    {
                        tbWaitDesc.Text = CompanyCatalogEntity.QuoteWaiting2Charge;
                    }
                    else
                    {
                        tbWaitDesc.Text = "WAIT CHARGE:";
                    }
                    if (CompanyCatalogEntity.IsQuoteUpaid2WaitingChg != null)
                    {
                        chkWaitUnits.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUpaid2WaitingChg);
                    }
                    if (CompanyCatalogEntity.IsQuoteFlight2ChargePrepaid != null)
                    {
                        chkFlightPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteFlight2ChargePrepaid);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.Deposit2FlightCharge))
                    {
                        tbFlightDesc.Text = CompanyCatalogEntity.Deposit2FlightCharge;
                    }
                    else
                    {
                        tbFlightDesc.Text = "FLIGHT CHARGE:";
                    }
                    if (CompanyCatalogEntity.IsQuoteUnpaidFlight2Chg != null)
                    {
                        chkFlightUnits.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUnpaidFlight2Chg);
                    }

                    if (CompanyCatalogEntity.IsQuoteSubtotal2Print != null)
                    {
                        chkSubtotalPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteSubtotal2Print);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.Quote2Subtotal))
                    {
                        tbSubtotalDesc.Text = CompanyCatalogEntity.Quote2Subtotal;
                    }
                    else
                    {
                        tbSubtotalDesc.Text = "SUBTOTAL:";
                    }
                    if (CompanyCatalogEntity.IsQuoteDiscount2AmountPrepaid != null)
                    {
                        chkDiscountPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteDiscount2AmountPrepaid);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.Discount2AmountDeposit))
                    {
                        tbDiscountDesc.Text = CompanyCatalogEntity.Discount2AmountDeposit;
                    }
                    else
                    {
                        tbDiscountDesc.Text = "DISCOUNT AMOUNT:";
                    }
                    if (CompanyCatalogEntity.IsQuoteUnpaid2DiscountAmt != null)
                    {
                        chkDiscountUnits.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteUnpaid2DiscountAmt);
                    }

                    if (CompanyCatalogEntity.IsQuoteTaxes2Print != null)
                    {
                        chkTaxesAmt.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteTaxes2Print);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.Quote2TaxesDefault))
                    {
                        tbTaxesDesc.Text = CompanyCatalogEntity.Quote2TaxesDefault;
                    }
                    else
                    {
                        tbTaxesDesc.Text = "TAXES";
                    }

                    if (CompanyCatalogEntity.IsQuoteInvoice2Print != null)
                    {
                        chkTotalQuotePrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteInvoice2Print);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteInvoice2Default))
                    {
                        tbTotalQuoteDesc.Text = CompanyCatalogEntity.QuoteInvoice2Default;
                    }
                    else
                    {
                        tbTotalQuoteDesc.Text = "TOTAL QUOTE";
                    }

                    if (CompanyCatalogEntity.IsQuote2PrepayPrint != null)
                    {
                        chkPrepaidPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuote2PrepayPrint);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuotePrepay2Default))
                    {
                        tbPrepaidDesc.Text = CompanyCatalogEntity.QuotePrepay2Default;
                    }
                    else
                    {
                        tbPrepaidDesc.Text = "PREPAID";
                    }

                    if (CompanyCatalogEntity.IsQuoteRemaining2AmtPrint != null)
                    {
                        chkRemainingAmtPrint.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteRemaining2AmtPrint);
                    }
                    if (!string.IsNullOrEmpty(CompanyCatalogEntity.QuoteRemaining2AmountDefault))
                    {
                        tbRemainingAmtDesc.Text = CompanyCatalogEntity.QuoteRemaining2AmountDefault;
                    }
                    else
                    {
                        tbRemainingAmtDesc.Text = "REMAINING AMOUNT DUE:";
                    }
                    #endregion
                    #region QuoteDetailColumnsToPrint
                    if (CompanyCatalogEntity.IsRptQuoteDetailLegNum != null)
                    {
                        chkLegNum.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailLegNum);
                    }
                    if (CompanyCatalogEntity.IsRptQuoteDetailDepartureDT != null)
                    {
                        chkDepartDate.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailDepartureDT);
                    }
                    if (CompanyCatalogEntity.IsRptQuoteDetailFromDescription != null)
                    {
                        chkFromDescription.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailFromDescription);
                    }
                    if (CompanyCatalogEntity.IsRptQuoteDetailToDescription != null)
                    {
                        chkToDescription.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailToDescription);
                    }
                    if (CompanyCatalogEntity.IsRptQuoteDetailQuotedAmt != null)
                    {
                        chkQuotedAmount.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailQuotedAmt);
                    }
                    if (CompanyCatalogEntity.IsRptQuoteDetailFlightHours != null)
                    {
                        chkFlightHours.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailFlightHours);
                    }
                    if (CompanyCatalogEntity.IsRptQuoteDetailMiles != null)
                    {
                        chkMiles.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailMiles);
                    }
                    if (CompanyCatalogEntity.IsRptQuoteDetailPassengerCnt != null)
                    {
                        chkPassengerCount.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailPassengerCnt);
                    }
                    #endregion
                    #region QuoteFeeColumn
                    if (CompanyCatalogEntity.IsRptQuoteFeeDescription != null)
                    {
                        chkQuoteFeeDescription.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteFeeDescription);
                    }
                    if (CompanyCatalogEntity.IsRptQuoteFeeQuoteAmt != null)
                    {
                        chkQuotedAmt.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteFeeQuoteAmt);
                    }
                    if (CompanyCatalogEntity.IsCharterQuoteFee3 != null)
                    {
                        chkChargeUnit.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCharterQuoteFee3);
                    }
                    if (CompanyCatalogEntity.IsCharterQuoteFee4 != null)
                    {
                        chkQuantity.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCharterQuoteFee4);
                    }
                    if (CompanyCatalogEntity.IsCharterQuote5 != null)
                    {
                        chkRate.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCharterQuote5);
                    }

                    #endregion
                    #region QuoteInformationToPrint
                    if (CompanyCatalogEntity.IsQuoteInformation2 != null)
                    {
                        chkCharterCompanyInfo.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteInformation2);
                    }
                    if (CompanyCatalogEntity.IsCompanyName2 != null)
                    {
                        chkCustomerInfo.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsCompanyName2);
                    }
                    if (CompanyCatalogEntity.QuoteDescription != null)
                    {
                        chkDescription.Checked = Convert.ToBoolean(CompanyCatalogEntity.QuoteDescription);
                    }
                    if (CompanyCatalogEntity.IsQuoteSales != null)
                    {
                        chkSalesPerson.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteSales);
                    }
                    if (CompanyCatalogEntity.IsQuoteTailNum != null)
                    {
                        chkTailNum.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteTailNum);
                    }
                    if (CompanyCatalogEntity.IsQuoteTypeCD != null)
                    {
                        chkAircraftType.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteTypeCD);
                    }
                    #endregion
                    if (CompanyCatalogEntity.QuoteIDTYPE != null)
                    {
                        if (CompanyCatalogEntity.QuoteIDTYPE == 1)
                        {
                            radNone.Checked = true;
                        }
                        else if (CompanyCatalogEntity.QuoteIDTYPE == 2)
                        {
                            radFileQNumber.Checked = true;
                        }
                    }
                    if (CompanyCatalogEntity.IsQuoteEdit != null)
                    {
                        chkAllowEdit.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsQuoteEdit);
                    }
                    if (CompanyCatalogEntity.QuoteTitle != null)
                    {
                        tbReportTitle.Text = CompanyCatalogEntity.QuoteTitle;
                    }

                    if (CompanyCatalogEntity.IsRptQuoteDetailArrivalDate != null)
                    {
                        chkIsRptQuoteDetailArrivalDate.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailArrivalDate);
                    }
                    else
                    {
                        chkIsRptQuoteDetailArrivalDate.Checked = false;
                    }
                    if (CompanyCatalogEntity.IsRptQuoteDetailQuoteDate != null)
                    {
                        chkIsRptQuoteDetailQuoteDate.Checked = Convert.ToBoolean(CompanyCatalogEntity.IsRptQuoteDetailQuoteDate);
                    }
                    else
                    {
                        chkIsRptQuoteDetailQuoteDate.Checked = false;
                    }

                    #region Get Image from Database
                    CreateDictionayForImgUpload();
                    imgFile.ImageUrl = "";
                    ImgQuotePopup.ImageUrl = null;
                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ObjRetImg = ObjImgService.GetFileWarehouseList("CqCompanyQuote", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList;
                        ddlImg.Items.Clear();
                        ddlImg.Text = "";
                        int i = 0;
                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                        {
                            Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CQQuoteDicImg"];
                            string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.UWAFileName);
                            ddlImg.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                            dicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                            if (i == 0)
                            {
                                byte[] picture = fwh.UWAFilePath;

                                //Ramesh: Set the URL for image file or link based on the file type
                                SetURL(fwh.UWAFileName, picture);

                                ////start of modification for image issue
                                //if (hdnBrowserName.Value == "Chrome")
                                //{
                                //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                //}
                                //else
                                //{
                                //    Session["CqQuoteReportBase64"] = picture;                                    
                                //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid());
                                //    //imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid();
                                //}
                                ////end of modification for image issue
                            }
                            i = i + 1;
                        }
                        if (ddlImg.Items.Count > 0)
                        {
                            ddlImg.SelectedIndex = 0;
                        }
                        else
                        {
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                        }
                    }
                    #endregion


                    //
                    if (CompanyCatalogEntity.QuoteSalesLogoPosition != null)
                    {
                        //radSalesLogoPosition.SelectedIndex = Convert.ToInt32(CompanyCatalogEntity.QuoteSalesLogoPosition);
                    }


                }

            }
        }

        protected void btnMainOk_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GetItems();
                Dictionary<string, string> dictSetQuoteReportDefaultsString = new Dictionary<string, string>();
                dictSetQuoteReportDefaultsString.Add("ChtQuoteCompany", tbCharterCompInfo.Text);
                dictSetQuoteReportDefaultsString.Add("QuotePageOff", tbTopOffset.Text);
                dictSetQuoteReportDefaultsString.Add("ChtQouteHeaderDetailRpt", tbCustomHeaderInfo.Text);
                dictSetQuoteReportDefaultsString.Add("ChtQouteFooterDetailRpt", tbCustomFooterInfo.Text);
                dictSetQuoteReportDefaultsString.Add("QuoteMinimumUse2FeeDepositAdj", tbDailyDesc.Text);
                dictSetQuoteReportDefaultsString.Add("Additional2FeeDefault", tbAdditionalDesc.Text);
                dictSetQuoteReportDefaultsString.Add("uote2LandingFeeDefault", tbLandingDesc.Text);
                dictSetQuoteReportDefaultsString.Add("QuoteSegment2FeeDeposit", tbSegmentDesc.Text);
                dictSetQuoteReportDefaultsString.Add("QouteStdCrew2RONDeposit", tbRonCrewDesc.Text);
                dictSetQuoteReportDefaultsString.Add("QuoteAddition2CrewDeposit", tbCrewAddCrewDesc.Text);
                dictSetQuoteReportDefaultsString.Add("QuoteAddition2CrewRON", tbAddCrewRonDesc.Text);
                dictSetQuoteReportDefaultsString.Add("QuoteWaiting2Charge", tbWaitDesc.Text);
                dictSetQuoteReportDefaultsString.Add("Deposit2FlightCharge", tbFlightDesc.Text);
                dictSetQuoteReportDefaultsString.Add("Quote2Subtotal", tbSubtotalDesc.Text);
                dictSetQuoteReportDefaultsString.Add("Discount2AmountDeposit", tbDiscountDesc.Text);
                dictSetQuoteReportDefaultsString.Add("Quote2TaxesDefault", tbTaxesDesc.Text);
                dictSetQuoteReportDefaultsString.Add("QuoteInvoice2Default", tbTotalQuoteDesc.Text);
                dictSetQuoteReportDefaultsString.Add("QuotePrepay2Default", tbPrepaidDesc.Text);
                dictSetQuoteReportDefaultsString.Add("QuoteRemaining2AmountDefault", tbRemainingAmtDesc.Text);
                // tbRemainingAmtDesc.Text = CompanyCatalogEntity.QuoteRemaining2AmountDefault;
                dictSetQuoteReportDefaultsString.Add("QuoteTitle", tbReportTitle.Text);
                Session.Add("CqCompanyStringQuoteReport", dictSetQuoteReportDefaultsString);

                Dictionary<string, bool> dictSetQuoteReportDefaultsCheck = new Dictionary<string, bool>();
                dictSetQuoteReportDefaultsCheck.Add("IsChangeQueueCity", chkCityDetail.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsChangeQueueAirport", chkAirportName.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueICAO", chkIcaoId.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteFirstPage", chkPrintFirstPage.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsChangeQueueDetail", chkDetail.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueSubtotal", chkSubTotal.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueFees", chkAdditionalFees.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteChangeQueueSum", chkQuoteSummary.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuotePrepaidMinimumUsage2FeeAdj", chkDailyPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteAdditional2FeePrint", chkAdditionalPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuotePrepaidMin2UsageFeeAdj", chkDailyUnits.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteFlight2LandingFeePrint", chkLandingPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuotePrepaidSegement2Fee", chkSegmentPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteStdCrew2RONPrepaid", chkRonCrewPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaidStd2CrewRON", chkRonCrewUnits.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteAdditional2CrewPrepaid", chkAddCrewPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaidAdditional2Crew", chkAddCrewUnits.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteAdditional2CrewRON", chkAddCrewRonPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUpaidAdditionl2CrewRON", chkAddCrewRonUnits.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteWaiting2ChargePrepaid", chkWaitPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUpaid2WaitingChg", chkWaitUnits.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteFlight2ChargePrepaid", chkFlightPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaidFlight2Chg", chkFlightUnits.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteSubtotal2Print", chkSubtotalPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteTaxes2Print", chkTaxesAmt.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteDiscount2AmountPrepaid", chkDiscountPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteUnpaid2DiscountAmt", chkDiscountUnits.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteInvoice2Print", chkTotalQuotePrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuote2PrepayPrint", chkPrepaidPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteRemaining2AmtPrint", chkRemainingAmtPrint.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailLegNum", chkLegNum.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailDepartureDT", chkDepartDate.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailFromDescription", chkFromDescription.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailToDescription", chkToDescription.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailQuotedAmt", chkQuotedAmount.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailFlightHours", chkFlightHours.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailMiles", chkMiles.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailPassengerCnt", chkPassengerCount.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteFeeDescription", chkQuoteFeeDescription.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteFeeQuoteAmt", chkQuotedAmt.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsCharterQuoteFee3", chkChargeUnit.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsCharterQuoteFee4", chkQuantity.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsCharterQuote5", chkRate.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteInformation2", chkCharterCompanyInfo.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsCompanyName2", chkCustomerInfo.Checked);
                dictSetQuoteReportDefaultsCheck.Add("QuoteDescription", chkDescription.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteSales", chkSalesPerson.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteTailNum", chkTailNum.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteTypeCD", chkAircraftType.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsQuoteEdit", chkAllowEdit.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailArrivalDate", chkIsRptQuoteDetailArrivalDate.Checked);
                dictSetQuoteReportDefaultsCheck.Add("IsRptQuoteDetailQuoteDate", chkIsRptQuoteDetailQuoteDate.Checked);
                Session.Add("CqCompanyCheckQuoteReport", dictSetQuoteReportDefaultsCheck);

                Dictionary<string, int> dictSetQuoteReportDefaultsRad = new Dictionary<string, int>();
                dictSetQuoteReportDefaultsRad.Add("QuoteLogoPosition", radlstLogoPosition.SelectedIndex);
                //dictSetQuoteReportDefaultsRad.Add("QuoteSalesLogoPosition", radSalesLogoPosition.SelectedIndex);
                dictSetQuoteReportDefaultsRad.Add("ImagePosition", RadioImagePosition.SelectedIndex);
                Session.Add("CqCompanyRadQuoteReport", dictSetQuoteReportDefaultsRad);
                if (ddlFooterInfo.SelectedItem != null)
                    Session["CQQuoteFooter"] = ddlFooterInfo.SelectedItem.Text;
                if (radAllLegs.Checked == true)
                    dictSetQuoteReportDefaultsRad.Add("ImageCnt", 1);
                else
                {
                    if (radTwoimages.Checked == true)
                        dictSetQuoteReportDefaultsRad.Add("ImageCnt", 2);
                    else if (radthreeimages.Checked == true)
                        dictSetQuoteReportDefaultsRad.Add("ImageCnt", 3);
                    else if (radfourimages.Checked == true)
                        dictSetQuoteReportDefaultsRad.Add("ImageCnt", 4);
                    else
                        dictSetQuoteReportDefaultsRad.Add("ImageCnt", 0);
                }
                if (radNone.Checked == true)
                {
                    dictSetQuoteReportDefaultsRad.Add("QuoteIDTYPE", 1);

                }
                else
                {
                    if (radFileQNumber.Checked == true)
                        dictSetQuoteReportDefaultsRad.Add("QuoteIDTYPE", 2);
                    else

                        dictSetQuoteReportDefaultsRad.Add("QuoteIDTYPE", 0);

                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "Close();", true);
            }
        }

        protected void btnMainCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
            }
        }


        private void AssignQuoteReport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CqCompanyStringQuoteReport"] != null)
                        {
                            Dictionary<string, string> dictSetQuoteReportDefaultsString = (Dictionary<string, string>)Session["CqCompanyStringQuoteReport"];
                            tbCharterCompInfo.Text = dictSetQuoteReportDefaultsString["ChtQuoteCompany"];
                            tbTopOffset.Text = (dictSetQuoteReportDefaultsString["QuotePageOff"]);
                            tbCustomHeaderInfo.Text = dictSetQuoteReportDefaultsString["ChtQouteHeaderDetailRpt"];
                            tbCustomFooterInfo.Text = dictSetQuoteReportDefaultsString["ChtQouteFooterDetailRpt"];
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QuoteMinimumUse2FeeDepositAdj"]))
                            {
                                tbDailyDesc.Text = dictSetQuoteReportDefaultsString["QuoteMinimumUse2FeeDepositAdj"];
                            }
                            else
                            {
                                tbDailyDesc.Text = "DAILY USAGE ADJUSTMENT:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["Additional2FeeDefault"]))
                            {
                                tbAdditionalDesc.Text = dictSetQuoteReportDefaultsString["Additional2FeeDefault"];
                            }
                            else
                            {
                                tbAdditionalDesc.Text = "ADDITIONAL FEES:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["uote2LandingFeeDefault"]))
                            {
                                tbLandingDesc.Text = dictSetQuoteReportDefaultsString["uote2LandingFeeDefault"];
                            }
                            else
                            {
                                tbLandingDesc.Text = "LANDING FEES:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QuoteSegment2FeeDeposit"]))
                            {
                                tbSegmentDesc.Text = dictSetQuoteReportDefaultsString["QuoteSegment2FeeDeposit"];
                            }
                            else
                            {
                                tbSegmentDesc.Text = "SEGMENT FEES:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QouteStdCrew2RONDeposit"]))
                            {
                                tbRonCrewDesc.Text = dictSetQuoteReportDefaultsString["QouteStdCrew2RONDeposit"];
                            }
                            else
                            {
                                tbRonCrewDesc.Text = "RON CREW CHARGE:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QuoteAddition2CrewDeposit"]))
                            {
                                tbCrewAddCrewDesc.Text = dictSetQuoteReportDefaultsString["QuoteAddition2CrewDeposit"];
                            }
                            else
                            {
                                tbCrewAddCrewDesc.Text = "ADDITIONAL CREW CHARGE:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QuoteAddition2CrewRON"]))
                            {
                                tbAddCrewRonDesc.Text = dictSetQuoteReportDefaultsString["QuoteAddition2CrewRON"];
                            }
                            else
                            {
                                tbAddCrewRonDesc.Text = "ADDITIONAL CREW RON CHARGE:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QuoteWaiting2Charge"]))
                            {
                                tbWaitDesc.Text = dictSetQuoteReportDefaultsString["QuoteWaiting2Charge"];
                            }
                            else
                            {
                                tbWaitDesc.Text = "WAIT CHARGE:";
                            }

                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["Deposit2FlightCharge"]))
                            {
                                tbFlightDesc.Text = dictSetQuoteReportDefaultsString["Deposit2FlightCharge"];
                            }
                            else
                            {
                                tbFlightDesc.Text = "FLIGHT CHARGE:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["Quote2Subtotal"]))
                            {
                                tbSubtotalDesc.Text = dictSetQuoteReportDefaultsString["Quote2Subtotal"];
                            }
                            else
                            {
                                tbSubtotalDesc.Text = "SUBTOTAL:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["Discount2AmountDeposit"]))
                            {
                                tbDiscountDesc.Text = dictSetQuoteReportDefaultsString["Discount2AmountDeposit"];
                            }
                            else
                            {
                                tbDiscountDesc.Text = "DISCOUNT AMOUNT:";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["Quote2TaxesDefault"]))
                            {
                                tbTaxesDesc.Text = dictSetQuoteReportDefaultsString["Quote2TaxesDefault"];
                            }
                            else
                            {
                                tbTaxesDesc.Text = "TAXES";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QuoteInvoice2Default"]))
                            {
                                tbTotalQuoteDesc.Text = dictSetQuoteReportDefaultsString["QuoteInvoice2Default"];
                            }
                            else
                            {
                                tbTotalQuoteDesc.Text = "TOTAL QUOTE";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QuotePrepay2Default"]))
                            {
                                tbPrepaidDesc.Text = dictSetQuoteReportDefaultsString["QuotePrepay2Default"];
                            }
                            else
                            {
                                tbPrepaidDesc.Text = "PREPAID";
                            }
                            if (!string.IsNullOrEmpty(dictSetQuoteReportDefaultsString["QuoteRemaining2AmountDefault"]))
                            {
                                tbRemainingAmtDesc.Text = dictSetQuoteReportDefaultsString["QuoteRemaining2AmountDefault"];
                            }
                            else
                            {
                                tbRemainingAmtDesc.Text = "REMAINING AMOUNT DUE:";
                            }
                            tbReportTitle.Text = dictSetQuoteReportDefaultsString["QuoteTitle"];
                        }
                        if (Session["CqCompanyCheckQuoteReport"] != null)
                        {
                            Dictionary<string, bool> dictSetQuoteReportDefaultsCheck = (Dictionary<string, bool>)Session["CqCompanyCheckQuoteReport"];
                            chkCityDetail.Checked = dictSetQuoteReportDefaultsCheck["IsChangeQueueCity"];
                            chkAirportName.Checked = dictSetQuoteReportDefaultsCheck["IsChangeQueueAirport"];
                            chkIcaoId.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteChangeQueueICAO"];
                            chkPrintFirstPage.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteFirstPage"];
                            chkDetail.Checked = dictSetQuoteReportDefaultsCheck["IsChangeQueueDetail"];
                            chkSubTotal.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteChangeQueueSubtotal"];
                            chkAdditionalFees.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteChangeQueueFees"];
                            chkQuoteSummary.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteChangeQueueSum"];
                            chkDailyPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuotePrepaidMinimumUsage2FeeAdj"];
                            chkDailyUnits.Checked = dictSetQuoteReportDefaultsCheck["IsQuotePrepaidMin2UsageFeeAdj"];
                            chkAdditionalPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteAdditional2FeePrint"];
                            chkLandingPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteFlight2LandingFeePrint"];
                            chkSegmentPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuotePrepaidSegement2Fee"];
                            chkRonCrewPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteStdCrew2RONPrepaid"];
                            chkRonCrewUnits.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteUnpaidStd2CrewRON"];
                            chkAddCrewPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteAdditional2CrewPrepaid"];
                            chkAddCrewUnits.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteUnpaidAdditional2Crew"];
                            chkAddCrewRonPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteAdditional2CrewRON"];
                            chkAddCrewRonUnits.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteUpaidAdditionl2CrewRON"];
                            chkWaitPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteWaiting2ChargePrepaid"];
                            chkWaitUnits.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteUpaid2WaitingChg"];
                            chkFlightPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteFlight2ChargePrepaid"];
                            chkFlightUnits.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteUnpaidFlight2Chg"];
                            chkSubtotalPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteSubtotal2Print"];
                            chkDiscountPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteDiscount2AmountPrepaid"];
                            chkDiscountUnits.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteUnpaid2DiscountAmt"];

                            chkTaxesAmt.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteTaxes2Print"];
                            chkTotalQuotePrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteInvoice2Print"];
                            chkPrepaidPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuote2PrepayPrint"];
                            chkRemainingAmtPrint.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteRemaining2AmtPrint"];
                            chkLegNum.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailLegNum"];
                            chkDepartDate.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailDepartureDT"];
                            chkFromDescription.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailFromDescription"];
                            chkToDescription.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailToDescription"];
                            chkQuotedAmount.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailQuotedAmt"];
                            chkFlightHours.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailFlightHours"];
                            chkMiles.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailMiles"];
                            chkPassengerCount.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailPassengerCnt"];
                            chkQuoteFeeDescription.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteFeeDescription"];
                            chkQuotedAmt.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteFeeQuoteAmt"];
                            chkChargeUnit.Checked = dictSetQuoteReportDefaultsCheck["IsCharterQuoteFee3"];
                            chkQuantity.Checked = dictSetQuoteReportDefaultsCheck["IsCharterQuoteFee4"];
                            chkRate.Checked = dictSetQuoteReportDefaultsCheck["IsCharterQuote5"];
                            chkCharterCompanyInfo.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteInformation2"];
                            chkCustomerInfo.Checked = dictSetQuoteReportDefaultsCheck["IsCompanyName2"];
                            chkDescription.Checked = dictSetQuoteReportDefaultsCheck["QuoteDescription"];
                            chkSalesPerson.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteSales"];
                            chkTailNum.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteTailNum"];
                            chkAircraftType.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteTypeCD"];
                            chkIsRptQuoteDetailArrivalDate.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailArrivalDate"];
                            chkIsRptQuoteDetailQuoteDate.Checked = dictSetQuoteReportDefaultsCheck["IsRptQuoteDetailQuoteDate"];

                            chkAllowEdit.Checked = dictSetQuoteReportDefaultsCheck["IsQuoteEdit"];
                            if (Session["CqCompanyRadQuoteReport"] != null)
                            {
                                Dictionary<string, int> dictSetQuoteReportDefaultsRad = (Dictionary<string, int>)Session["CqCompanyRadQuoteReport"];
                                radlstLogoPosition.SelectedIndex = dictSetQuoteReportDefaultsRad["QuoteLogoPosition"];
                                //radSalesLogoPosition.SelectedIndex = dictSetQuoteReportDefaultsRad["QuoteSalesLogoPosition"];
                                RadioImagePosition.SelectedIndex = dictSetQuoteReportDefaultsRad["ImagePosition"];
                                if (dictSetQuoteReportDefaultsRad["ImageCnt"] != null)
                                {
                                    if (Convert.ToInt32(dictSetQuoteReportDefaultsRad["ImageCnt"]) == 1)
                                    {
                                        radAllLegs.Checked = true;
                                    }
                                    else if (Convert.ToInt32(dictSetQuoteReportDefaultsRad["ImageCnt"]) == 2)
                                        radTwoimages.Checked = true;
                                    else if (Convert.ToInt32(dictSetQuoteReportDefaultsRad["ImageCnt"]) == 3)
                                        radthreeimages.Checked = true;
                                    else if (Convert.ToInt32(dictSetQuoteReportDefaultsRad["ImageCnt"]) == 4)
                                        radfourimages.Checked = true;
                                }
                                if (dictSetQuoteReportDefaultsRad["QuoteIDTYPE"] != null)
                                {
                                    if (Convert.ToInt32(dictSetQuoteReportDefaultsRad["QuoteIDTYPE"]) == 1)
                                    {
                                        radNone.Checked = true;
                                    }
                                    else if (Convert.ToInt32(dictSetQuoteReportDefaultsRad["QuoteIDTYPE"]) == 2)
                                    {
                                        radFileQNumber.Checked = true;
                                    }
                                }

                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        //#region Image
        ///// <summary>
        ///// ImageToBinary
        ///// </summary>
        ///// <param name="imagePath"></param>
        ///// <returns></returns>
        //public static byte[] ImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int)fileStream.Length);
        //        fileStream.Close();
        //        return buffer;
        //    }
        //}
        ///// <summary>
        ///// To Load Image
        ///// </summary>
        //private void LoadImage()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        System.IO.Stream myStream;
        //        Int32 fileLen;
        //        if (fileUL.PostedFile != null)
        //        {
        //            if (fileUL.PostedFile.ContentLength > 0)
        //            {
        //                if (ddlImg.Items.Count >= 0)
        //                {
        //                    string FileName = fileUL.FileName;
        //                    fileLen = fileUL.PostedFile.ContentLength;
        //                    Byte[] Input = new Byte[fileLen];
        //                    myStream = fileUL.FileContent;
        //                    myStream.Read(Input, 0, fileLen);
        //                    //start of modification for image issue
        //                    if (hdnBrowserName.Value == "Chrome")
        //                    {
        //                        imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
        //                    }
        //                    else
        //                    {
        //                        Session["CqQuoteReportBase64"] = Input;
        //                        imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid() + "&SessionName=CqQuoteReportBase64";
        //                    }
        //                    //end of modification for image issue
        //                    Dictionary<string, byte[]> CqQuoteDicImg = (Dictionary<string, byte[]>)Session["CqQuoteDicImg"];
        //                    //Commented for removing document name
        //                    //if (tbImgName.Text.Trim() != "")
        //                    //    FileName = tbImgName.Text.Trim();
        //                    int count = CqQuoteDicImg.Count(D => D.Key.Equals(FileName));
        //                    if (count > 0)
        //                    {
        //                        CqQuoteDicImg[FileName] = Input;
        //                    }
        //                    else
        //                    {
        //                        DeleteOtherImage();
        //                        CqQuoteDicImg.Add(FileName, Input);
        //                        ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(FileName, FileName));
        //                        //start of modification for image issue
        //                        if (hdnBrowserName.Value == "Chrome")
        //                        {
        //                            imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
        //                        }
        //                        else
        //                        {
        //                            Session["CqQuoteReportBase64"] = Input;
        //                            imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid() + "&SessionName=CqQuoteReportBase64";
        //                            //imgFile.ImageUrl = "../../ImageHandler.ashx?cd=" + Guid.NewGuid();
        //                        }
        //                        //end of modification for image issue
        //                    }
        //                    tbImgName.Text = "";
        //                    btndeleteImage.Enabled = true;
        //                }
        //            }
        //        }
        //    }
        //}
        ///// <summary>
        ///// To Delete Other Image
        ///// </summary>
        //private void DeleteOtherImage()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        Dictionary<string, byte[]> CqQuoteDicImg = (Dictionary<string, byte[]>)Session["CqQuoteDicImg"];
        //        int count = CqQuoteDicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
        //        if (count > 0)
        //        {
        //            CqQuoteDicImg.Remove(ddlImg.Text.Trim());
        //            Session["CqQuoteDicImg"] = CqQuoteDicImg;
        //            Dictionary<string, string> CqQuoteDicImgDelete = (Dictionary<string, string>)Session["CqQuoteDicImgDelete"];
        //            CqQuoteDicImgDelete.Add(ddlImg.Text.Trim(), "");
        //            Session["CqQuoteDicImgDelete"] = CqQuoteDicImgDelete;
        //            ddlImg.Items.Clear();
        //            ddlImg.Text = "";
        //            tbImgName.Text = "";
        //            int i = 0;
        //            foreach (var DicItem in CqQuoteDicImg)
        //            {
        //                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
        //                i = i + 1;
        //            }
        //            imgFile.ImageUrl = "";
        //            ImgPopup.ImageUrl = null;
        //        }
        //    }
        //}
        ///// <summary>
        ///// Dropdown for image
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                if (ddlImg.Text.Trim() == "")
        //                {
        //                    imgFile.ImageUrl = "";
        //                }
        //                if (ddlImg.SelectedItem != null)
        //                {
        //                    bool imgFound = false;
        //                    imgFile.ImageUrl = "";
        //                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
        //                    {
        //                        var ObjRetImg = ObjImgService.GetFileWarehouseList("CqCompanyQuote", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());
        //                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
        //                        {
        //                            imgFound = true;
        //                            byte[] picture = fwh.UWAFilePath;
        //                            //start of modification for image issue
        //                            if (hdnBrowserName.Value == "Chrome")
        //                            {
        //                                imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
        //                            }
        //                            else
        //                            {
        //                                Session["CqQuoteReportBase64"] = picture;
        //                                imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid() + "&SessionName=CqQuoteReportBase64";
        //                            }
        //                            //end of modification for image issue
        //                        }
        //                        tbImgName.Text = ddlImg.Text.Trim();
        //                        if (!imgFound)
        //                        {
        //                            Dictionary<string, byte[]> CqQuoteDicImg = (Dictionary<string, byte[]>)Session["CqQuoteDicImg"];
        //                            int count = CqQuoteDicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
        //                            if (count > 0)
        //                            {
        //                                //start of modification for image issue
        //                                if (hdnBrowserName.Value == "Chrome")
        //                                {
        //                                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(CqQuoteDicImg[ddlImg.Text.Trim()]);
        //                                }
        //                                else
        //                                {
        //                                    Session["CqQuoteReportBase64"] = CqQuoteDicImg[ddlImg.Text.Trim()];
        //                                    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid() + "&SessionName=CqQuoteReportBase64";
        //                                }
        //                                //end of modification for image issue
        //                            }
        //                        }
        //                    }
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            //ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
        //        }
        //    }
        //}
        ///// <summary>
        ///// To delete image
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void DeleteImage_Click(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                Dictionary<string, byte[]> CqQuoteDicImg = (Dictionary<string, byte[]>)Session["CqQuoteDicImg"];
        //                int count = CqQuoteDicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
        //                if (count > 0)
        //                {
        //                    CqQuoteDicImg.Remove(ddlImg.Text.Trim());
        //                    Session["CqQuoteDicImg"] = CqQuoteDicImg;
        //                    Dictionary<string, string> CqQuoteDicImgDelete = (Dictionary<string, string>)Session["CqQuoteDicImgDelete"];
        //                    if (CqQuoteDicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
        //                    {
        //                        CqQuoteDicImgDelete.Add(ddlImg.Text.Trim(), "");
        //                    }
        //                    Session["CqQuoteDicImgDelete"] = CqQuoteDicImgDelete;
        //                    ddlImg.Items.Clear();
        //                    ddlImg.Text = "";
        //                    tbImgName.Text = "";
        //                    int i = 0;
        //                    foreach (var DicItem in CqQuoteDicImg)
        //                    {
        //                        ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
        //                        i = i + 1;
        //                    }
        //                    imgFile.ImageUrl = "../../../App_Themes/Default/images/noimage.jpg";
        //                    ImgPopup.ImageUrl = null;
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            //ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
        //        }
        //    }
        //}
        ///// <summary>
        ///// To Create dictioanry for image upload
        ///// </summary>
        //private void CreateDictionayForImgUpload()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        Dictionary<string, byte[]> CqQuoteDicImg = new Dictionary<string, byte[]>();
        //        Session["CqQuoteDicImg"] = CqQuoteDicImg;
        //        Dictionary<string, string> CqQuoteDicImgDelete = new Dictionary<string, string>();
        //        Session["CqQuoteDicImgDelete"] = CqQuoteDicImgDelete;
        //    }
        //}
        //#endregion

        #region Image
        //Veracode fix for 1633
        ///// <summary>
        ///// ImageToBinary
        ///// </summary>
        ///// <param name="imagePath"></param>
        ///// <returns></returns>
        //public static byte[] ImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int)fileStream.Length);
        //        fileStream.Close();
        //        return buffer;
        //    }
        //}
        /// <summary>
        /// To Load Image
        /// </summary>
        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream myStream;
                Int32 fileLen;               
                Session["CqQuoteReportBase64"] = null;   //added for image issue
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        if (ddlImg.Items.Count >= 0)
                        {
                            string FileName = fileUL.FileName;
                            fileLen = fileUL.PostedFile.ContentLength;
                            Byte[] Input = new Byte[fileLen];
                            myStream = fileUL.FileContent;
                            myStream.Read(Input, 0, fileLen);

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);

                            ////start of modification for image issue
                            //if (hdnBrowserName.Value == "Chrome")
                            //{

                            //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                            //}
                            //else
                            //{
                            //    Session["CqQuoteReportBase64"] = Input;                                
                            //    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid();
                            //}
                            ////end of modification for image issue
                            Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                            
                            if (Session["CQQuoteDicImg"] == null)
                                Session["CQQuoteDicImg"] = new Dictionary<string, byte[]>();
                            dicImg = (Dictionary<string, byte[]>)Session["CQQuoteDicImg"];
                            //Commented for removing document name
                            //if (tbImgName.Text.Trim() != "")
                            //    FileName = tbImgName.Text.Trim();
                            int count = dicImg.Count(D => D.Key.Equals(FileName));
                            if (count > 0)
                            {
                                dicImg[FileName] = Input;
                            }
                            else
                            {
                                DeleteOtherImage();
                                dicImg.Add(FileName, Input);
                                ddlImg.Items.Clear();
                                string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FileName);
                                ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(encodedFileName, encodedFileName));
                                
                                //Ramesh: Set the URL for image file or link based on the file type
                                SetURL(FileName, Input);

                                ////start of modification for image issue
                                //if (hdnBrowserName.Value == "Chrome")
                                //{
                                //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                                //}
                                //else
                                //{
                                //    Session["CqQuoteReportBase64"] = Input;                                   
                                //    imgFile.ImageUrl = "../ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid();
                                //}
                                ////end of modification for image issue
                            }
                            tbImgName.Text = "";
                            btndeleteImage.Enabled = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// To Delete Other Image
        /// </summary>
        private void DeleteOtherImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();

                if (Session["CQQuoteDicImg"] == null)
                    Session["CQQuoteDicImg"] = new Dictionary<string, byte[]>();
                dicImg = (Dictionary<string, byte[]>)Session["CQQuoteDicImg"];
                int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                if (count > 0)
                {
                    dicImg.Remove(ddlImg.Text.Trim());
                    Session["CQQuoteDicImg"] = dicImg;
                    Dictionary<string, string> dicImgDelete;
                    if (Session["DicCQQuoteImgDelete"] == null)
                        Session["DicCQQuoteImgDelete"] = new Dictionary<string, string>();
                    dicImgDelete = (Dictionary<string, string>)Session["DicCQQuoteImgDelete"];

                    dicImgDelete.Add(ddlImg.Text.Trim(), "");
                    Session["DicCQQuoteImgDelete"] = dicImgDelete;
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbImgName.Text = "";
                    int i = 0;
                    foreach (var DicItem in dicImg)
                    {
                        ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                        i = i + 1;
                    }
                    imgFile.ImageUrl = "";
                    ImgQuotePopup.ImageUrl = null;
                }
            }
        }
        /// <summary>
        /// Dropdown for image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool imgFound = false;
                            imgFile.ImageUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ObjRetImg = ObjImgService.GetFileWarehouseList("CqCompanyQuote", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());
                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                {
                                    imgFound = true;
                                    byte[] picture = fwh.UWAFilePath;

                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(fwh.UWAFileName, picture);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                    //}
                                    //else
                                    //{
                                    //    Session["CqQuoteReportBase64"] = picture;                                       
                                    //    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                                tbImgName.Text = ddlImg.Text.Trim();
                                if (!imgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CQQuoteDicImg"];
                                    int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (count > 0)
                                    {
                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        //}
                                        //else
                                        //{
                                        //    Session["CqQuoteReportBase64"] = dicImg[ddlImg.Text.Trim()];                                           
                                        //    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To delete image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                        if (Session["CQQuoteDicImg"] != null)
                            Session["CQQuoteDicImg"] = new Dictionary<string, byte[]>();
                        dicImg = (Dictionary<string, byte[]>)Session["CQQuoteDicImg"];
                        int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            dicImg.Remove(ddlImg.Text.Trim());
                            Session["DicCQQuoteImgDelete"] = dicImg;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicCQQuoteImgDelete"];
                            if (Session["DicCQQuoteImgDelete"] != null)
                                if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                                {
                                    dicImgDelete.Add(ddlImg.Text.Trim(), "");
                                }
                            Session["DicCQQuoteImgDelete"] = dicImgDelete;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in dicImg)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            ImgQuotePopup.ImageUrl = null;
                        }
                        else
                        {                           
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicCQQuoteImgDelete"];
                            if (Session["DicCQQuoteImgDelete"] != null)
                                if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                                {
                                    dicImgDelete.Add(ddlImg.Text.Trim(), "");
                                }
                            Session["DicCQQuoteImgDelete"] = dicImgDelete;
                            
                            dicImg.Remove(ddlImg.Text.Trim());
                            Session["CQQuoteDicImg"] = dicImg;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                            ImgQuotePopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    // ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Create dictioanry for image upload
        /// </summary>
        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                Session["CQQuoteDicImg"] = dicImg;
                Dictionary<string, string> dicImgDelete = new Dictionary<string, string>();
                Session["DicCQQuoteImgDelete"] = dicImgDelete;
            }
        }

        // //<summary>
        // //Dropdown for image
        // //</summary>
        // //<param name="sender"></param>
        // //<param name="e"></param>
        //protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                if (ddlImg.Text.Trim() == "")
        //                {
        //                    imgFile.ImageUrl = "";
        //                }
        //                if (ddlImg.SelectedItem != null)
        //                {
        //                    bool imgFound = false;
        //                    imgFile.ImageUrl = "";
        //                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
        //                    {
        //                        var ObjRetImg = ObjImgService.GetFileWarehouseList("Company", Convert.ToInt64(Session["HomeBaseID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());
        //                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
        //                        {
        //                            imgFound = true;
        //                            byte[] picture = fwh.UWAFilePath;
        //                            //start of modification for image issue
        //                            if (hdnBrowserName.Value == "Chrome")
        //                            {
        //                                imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
        //                            }
        //                            else
        //                            {
        //                                Session["CqQuoteReportBase64"] = picture;
        //                                imgFile.ImageUrl = "../ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid();
        //                            }
        //                            //end of modification for image issue
        //                        }
        //                        tbImgName.Text = ddlImg.Text.Trim();
        //                        if (!imgFound)
        //                        {
        //                            Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicCqImg"];
        //                            int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
        //                            if (count > 0)
        //                            {
        //                                //start of modification for image issue
        //                                if (hdnBrowserName.Value == "Chrome")
        //                                {
        //                                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
        //                                }
        //                                else
        //                                {
        //                                    Session["CqQuoteReportBase64"] = dicImg[ddlImg.Text.Trim()];
        //                                    imgFile.ImageUrl = "../ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid();
        //                                }
        //                                //end of modification for image issue
        //                            }
        //                        }
        //                    }
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            //ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
        //        }
        //    }
        //}

        ///// <summary>
        ///// To delete image For Cq
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btncqdeleteImage_Click(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                Dictionary<string, byte[]> dicCqImg = (Dictionary<string, byte[]>)Session["DicCqImg"];
        //                int count = dicCqImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
        //                if (count > 0)
        //                {
        //                    dicCqImg.Remove(ddlImg.Text.Trim());
        //                    Session["DicCqImg"] = dicCqImg;
        //                    Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicCqImgDelete"];
        //                    if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
        //                    {
        //                        dicImgDelete.Add(ddlImg.Text.Trim(), "");
        //                    }
        //                    Session["DicCqImgDelete"] = dicImgDelete;
        //                    ddlImg.Items.Clear();
        //                    ddlImg.Text = "";
        //                    tbImgName.Text = "";
        //                    int i = 0;
        //                    foreach (var DicItem in dicCqImg)
        //                    {
        //                        ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
        //                        i = i + 1;
        //                    }
        //                    imgFile.ImageUrl = "../../../App_Themes/Default/images/noimage.jpg";
        //                    imgFile.ImageUrl = null;
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            //ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
        //        }
        //    }
        //}
        ///// <summary>
        ///// To Create dictioanry for image upload for CQ
        ///// </summary>
        //private void CreateCqDictionayForImgUpload()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        Dictionary<string, byte[]> dicCqImg = new Dictionary<string, byte[]>();
        //        Session["DicCqImg"] = dicCqImg;
        //        Dictionary<string, string> dicCqImgDelete = new Dictionary<string, string>();
        //        Session["DicCqImgDelete"] = dicCqImgDelete;
        //    }
        //}
        /// <summary>
        /// Rad Ajax Seeting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        e.Updated = DivExternalForm;

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        #endregion

        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            //Ramesh: Code to allow any file type for upload control.
            int iIndex = filename.Trim().IndexOf('.');
           // string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);  
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            if (regMatch.Success)
            {
                if (Request.UserAgent.Contains("Chrome"))
                {
                    hdnBrowserName.Value = "Chrome";
                }
                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    //lnkFileName.NavigateUrl = "";
                    //lnkFileName.Visible = false;
                }
                else
                {
                    Session["CqQuoteReportBase64"] = filedata;
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CqQuoteReportBase64=CqQuoteReportBase64&cd=" + Guid.NewGuid();
                    //lnkFileName.NavigateUrl = "";
                    //lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgFile.Visible = false;
                //lnkFileName.Visible = true;
                Session["CqQuoteReportBase64"] = filedata;
                //lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();
            }
        }
    }
}
