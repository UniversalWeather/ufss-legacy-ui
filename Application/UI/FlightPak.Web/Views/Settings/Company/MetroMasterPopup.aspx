﻿﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Metro</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var selectedRowData = null;
        var jqgridTableId = '#gridMetros';
        var selectedRowMultipleMetro = "";
        var isopenlookup = false;
        var ismultiSelect = false;
        var metors = [];
        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            var selectedRowData = getQuerystring("MetroCD", "");
            selectedRowMultipleMetro = $.trim(decodeURI(getQuerystring("MetroCD", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultipleMetro);
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            if (ismultiSelect) {
                if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                    selectedRowData = jQuery("#hdnselectedfccd").value;
                }
                metors = selectedRowData.split(',');
            }

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultipleMetro.length > 0)) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData, 0);
                }
                else {
                    showMessageBox('Please select a metro.', popupTitle);
                }
                return false;
            });

            $("#chkInActive").change(function () {
                $(jqgridTableId).trigger('reloadGrid');
            });

            jQuery(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'metros';
                    postData.metroid = 0;
                    postData.metroCD = function () { if (ismultiSelect && selectedRowData.length > 0) { return metors[0]; } else { return selectedRowData; } };
                    postData.showInactive = $("#chkInActive").prop('checked');
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 320,
                width: 500,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                colNames: ['MetroID', 'Code', 'Description'],
                colModel: [
                    { name: 'MetroID', index: 'MetroID', key: true, hidden: true },
                    { name: 'MetroCD', index: 'MetroCD' },
                    { name: 'MetroName', index: 'MetroName' }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData, 1);
                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    selectedRowMultipleMetro = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultipleMetro, rowData.MetroCD);
                },
                onSelectAll: function (id, status) {
                    selectedRowMultipleMetro = JqgridSelectAll(selectedRowMultipleMetro, jqgridTableId, id, status, 'MetroCD');
                    jQuery("#hdnselectedfccd").val(selectedRowMultipleAirport);
                },
                afterInsertRow: function (rowid, rowObject) {
                    JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultipleMetro, rowid, rowObject.MetroCD, jqgridTableId);
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                }
            });
            $("#pagesizebox").insertBefore('.ui-paging-info');
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

        });

        function returnToParent(rowData, one) {
            selectedRowMultipleMetro = jQuery.trim(selectedRowMultipleMetro);
            if (selectedRowMultipleMetro.lastIndexOf(",") == selectedRowMultipleMetro.length - 1)
                selectedRowMultipleMetro = selectedRowMultipleMetro.substr(0, selectedRowMultipleMetro.length - 1);

            var oArg = new Object();
            if (one == 0) {
                oArg.MetroCD = selectedRowMultipleMetro;
            }
            else {
                oArg.MetroCD = rowData["MetroCD"];
            }

            oArg.MetroName = rowData["MetroName"];
            oArg.MetroID = rowData["MetroID"];
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
            else {
                oWnd.close();
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="divGridPanel">
            <div class="jqgrid">
                <input type="hidden" id="hdnselectedfccd" value="" />
                <div>
                    <table class="box1">
                        <tr>
                            <td align="left">
                                <div>
                                    <input type="checkbox" name="chkInActive" id="chkInActive" />
                                    Display Inactive 
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="gridMetros" style="width: 500px !important;" class="table table-striped table-hover table-bordered"></table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="grid_icon">
                                    <div role="group" id="pg_gridPager"></div>
                                    <div id="pagesizebox">
                                        <span>Page Size:</span>
                                        <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                        <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                    </div>
                                </div>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
