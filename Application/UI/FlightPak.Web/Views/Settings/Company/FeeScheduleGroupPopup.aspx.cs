﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class FeeScheduleGroupPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private string Code;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Code"]))
            {
                Code = Request.QueryString["Code"].Trim().ToUpper();
            }

        }

        protected void dgFeeScheduleGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objMastersvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                FeeGroup feeGroup = new FeeGroup();
                List<FeeGroup> FeeGroupList = new List<FeeGroup>();
                var objFeeGroup = objMastersvc.GetFeeGroupList();
                if (objFeeGroup.EntityList != null && objFeeGroup.EntityList.Count > 0)
                {
                    FeeGroupList = objFeeGroup.EntityList;
                }
                dgFeeScheduleGroup.DataSource = FeeGroupList;               
            }            
        }

        protected void dgFeeScheduleGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    string resolvedurl = string.Empty;
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                //e.Item.Selected = true;

                                GridDataItem item = dgFeeScheduleGroup.SelectedItems[0] as GridDataItem;
                                Session["SelectedFeeGroupID"] = item["FeeGroupID"].Text;
                                TryResolveUrl("/Views/Settings/Fleet/FeeScheduleGroupCatalog.aspx?IsPopup=", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radFeeGroupCRUDPopup');", true);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/Fleet/FeeScheduleGroupCatalog.aspx?IsPopup=Add", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radFeeGroupCRUDPopup');", true);
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void dgFeeScheduleGroup_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string SelectedCrewRosterID = string.Empty;
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            GridDataItem item = dgFeeScheduleGroup.SelectedItems[0] as GridDataItem;
                            SelectedCrewRosterID = item["FeeGroupID"].Text;
                            FeeGroup Crew = new FeeGroup();
                            string CrewID = SelectedCrewRosterID;
                            string CrewCD = string.Empty;
                            Crew.FeeGroupID = Convert.ToInt64(CrewID);
                            Crew.FeeGroupCD = item["FeeGroupCD"].Text;  
                            Crew.IsDeleted = true;
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.FeeGroup, Convert.ToInt64(SelectedCrewRosterID));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FeeGroup);
                                    return;
                                }
                            }
                            CrewRosterService.DeleteFeeGroup(Crew);
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
                finally
                {
                    if (Session["SelectedFeeGroupID"] != null)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.Crew, Convert.ToInt64(SelectedCrewRosterID));
                        }
                    }
                }

            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            e.Updated = dgFeeScheduleGroup;
            SelectItem();
        }
        /// <summary>
        /// To Select Item
        /// </summary>
        private void SelectItem()
        {

            if (!string.IsNullOrEmpty(Code))
            {

                foreach (GridDataItem item in dgFeeScheduleGroup.MasterTableView.Items)
                {
                    if (item["FeeGroupCD"].Text.Trim().ToUpper() == Code)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (dgFeeScheduleGroup.MasterTableView.Items.Count > 0)
                {
                    dgFeeScheduleGroup.SelectedIndexes.Add(0);

                }
            }
        }

        protected void dgFeeScheduleGroup_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFeeScheduleGroup, Page.Session);
        }
    }
}