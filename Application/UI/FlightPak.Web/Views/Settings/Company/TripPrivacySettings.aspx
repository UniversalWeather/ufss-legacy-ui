﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TripPrivacySettings.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.TripPrivacySettings" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Trip Privacy Settings</title>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function returnToParent() {
                //create the argument that will be returned to the parent page

                oArg = new Object();
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function Close() {
                GetRadWindow().Close();
            }


        </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgTripPrivacy">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgTripPrivacy" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgTripPrivacy" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgTripPrivacy" runat="server" AllowMultiRowSelection="true"
            AllowSorting="true" OnNeedDataSource="dgTripPrivacy_BindData" OnItemDataBound="dgTripPrivacy_ItemDataBound"
            AutoGenerateColumns="false" Height="380px" AllowPaging="false" Width="400px"
            PagerStyle-AlwaysVisible="false">
            <MasterTableView DataKeyNames="Description" CommandItemDisplay="Bottom" AllowPaging="false">
                <Columns>
                    <telerik:GridTemplateColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        HeaderText="Description" HeaderStyle-HorizontalAlign="Center" AllowFiltering="false"
                        HeaderStyle-Width="300px" AutoPostBackOnFilter="true">
                        <ItemTemplate>
                            <asp:Label ID="lbDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        HeaderText="Display" HeaderStyle-HorizontalAlign="Center" AllowFiltering="false"
                        HeaderStyle-Width="100px" AutoPostBackOnFilter="true">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDisplay" runat="server"></asp:CheckBox>
                            <asp:Label ID="lbDisplay" Text='<%# Eval("Display") %>' Visible="false" runat="server"></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                <PagerStyle AlwaysVisible="false" Mode="NumericPages" Wrap="false" />
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false"></GroupingSettings>
        </telerik:RadGrid>
        <table cellspacing="0" cellpadding="0" class="tblButtonArea">
            <tr>
                <td>
                    <asp:Button ID="btnOk" Text="Ok" runat="server" OnClick="Ok_Click" CssClass="button" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClick="Cancel_Click" />
                    <asp:Label ID="lbScript" runat="server" Text="" Visible="false"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
