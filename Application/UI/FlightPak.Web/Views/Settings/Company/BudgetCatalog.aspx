﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="BudgetCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Company.BudgetCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script runat="server">
        void lnkAccounts_Click(object sender, EventArgs e)
        {
            string URL = "../Company/AccountsCatalog.aspx?Screen=Accounts&AccountID=" + hdnAccountNum.Value; Response.Redirect(URL);
        }
      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgBudgetCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgBudgetCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgBudgetCatalog">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgBudgetCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbAcctNmbr">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbAcctNmbr" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbYear" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cvAcctNmbr" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbDescription" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbTailNmbr">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbTailNmbr" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbMonth1Budget" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cvTailNmbr" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkHomeBaseOnly">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgBudgetCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openWin(radWin) {
                var url = '';
                if (radWin == "radAccountMasterPopup") {
                    url = '../Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbAcctNmbr.ClientID%>').value;
                }
                else if (radWin == "radFleetProfilePopup") {
                    url = '../Fleet/FleetProfilePopup.aspx?TailNumber=' + document.getElementById('<%=tbTailNmbr.ClientID%>').value;
                }
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); // GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }

            function validateEmptyRadRateTextbox(sender, eventArgs) {
                if (sender.get_textBoxValue().length < 1) {
                    sender.set_value('0.0');
                }
            }

            function OnClientCloseAccountMasterPopup(oWnd, args) {
                var combo = $find("<%= tbAcctNmbr.ClientID %>");

                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAcctNmbr.ClientID%>").value = arg.AccountNum;
                        document.getElementById("<%=tbDescription.ClientID%>").value = arg.AccountDescription;
                        document.getElementById("<%=cvAcctNmbr.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnAccountNum.ClientID%>").innerHTML = arg.AccountID;
                    }
                    else {
                        document.getElementById("<%=tbAcctNmbr.ClientID%>").value = "";
                        document.getElementById("<%=tbDescription.ClientID%>").value = "";
                        document.getElementById("<%=hdnAccountNum.ClientID%>").innerHTML = "";
                        document.getElementById("<%=cvAcctNmbr.ClientID%>").innerHTML = "";
                        combo.clearSelection();

                    }
                }
            }
            function OnClientCloseFleetMasterPopup(oWnd, args) {
                var combo = $find("<%= tbTailNmbr.ClientID %>");

                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbTailNmbr.ClientID%>").value = arg.TailNum;
                        document.getElementById("<%=cvTailNmbr.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnFleetID.ClientID%>").value = arg.FleetId;
                    }
                    else {
                        document.getElementById("<%=tbTailNmbr.ClientID%>").value = "";
                        document.getElementById("<%=hdnFleetID.ClientID%>").value = "";
                        combo.clearSelection();

                    }
                }
            }

            function ValidateEmptyTextbox(ctrlID, e) {

                if (ctrlID.value == "") {

                    ctrlID.value = "0.00";
                }

            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");

                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {
                        
                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAccountMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFleetMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div runat="server" id="divNavigation" style="display: none;">
                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left">
                                <div id="Div1" class="tab-nav-top" runat="server">
                                    <span class="head-title">
                                        <asp:LinkButton ID="lnkbtnAccounts" Text="Accounts" runat="server" OnClick="lnkAccounts_Click"></asp:LinkButton>&nbsp;>&nbsp;Budget</span>
                                    <span class="tab-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=BudgetHelp"
                                        class="help-icon" target="_blank" title="Help"></a></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0" runat="server" id="tableBudget">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Budget</span> <span class="tab-nav-icons">
                        <!--<a href="#" class="search-icon">
                        </a><a href="#" class="save-icon"></a><a href="#" class="print-icon"></a>-->
                        <a href="../../Help/ViewHelp.aspx?Screen=BudgetHelp" class="help-icon" target="_blank"
                            title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <%-- <table style="width: 100%;" cellpadding="0" cellspacing="0" id="tableBudget" runat="server">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Budget</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:return Reports_Click();"
                            title="Preview Report" class="search-icon"></asp:LinkButton><asp:LinkButton ID="lbtnSaveReports"
                                runat="server" title="Export Report" OnClientClick="javascript:openReport();return false;"
                                class="save-icon"></asp:LinkButton><a href="#" title="Help" class="help-icon"></a>
                    </span>
                </div>
            </td>
        </tr>
    </table>--%>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlBudgetCatalogForm" runat="server" Visible="true" Style="z-index: -1">
            <table width="100%" cellpadding="0" cellspacing="0" class="sticky" id="tblAccount"
                runat="server">
                <tr>
                    <td class="tdLabel100">
                        Account No.:
                    </td>
                    <td>
                        <asp:TextBox ID="tbSelectedAcctNumber" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkHomeBaseOnly" runat="server" Text="Home Base Only" OnCheckedChanged="chkHomeBaseOnly_CheckedChanged"
                                    AutoPostBack="true" />
                            </span><span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" />
                            </span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgBudgetCatalog" runat="server" AllowSorting="true" OnItemCreated="dgBudgetCatalog_ItemCreated"
                OnNeedDataSource="dgBudgetCatalog_BindData" OnItemCommand="dgBudgetCatalog_ItemCommand"
                OnPreRender="Budget_PreRender" OnUpdateCommand="dgBudgetCatalog_UpdateCommand"
                OnInsertCommand="dgBudgetCatalog_InsertCommand" OnPageIndexChanged="Budget_PageIndexChanged"
                OnDeleteCommand="dgBudgetCatalog_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
                AllowPaging="true" OnSelectedIndexChanged="dgBudgetCatalog_SelectedIndexChanged"
                Height="341px" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
                <MasterTableView DataKeyNames="CustomerID,BudgetID,AccountID,FleetID,AccountNum,FiscalYear,AccountDescription,TailNumber,FleetID,LastUpdUID,LastUpdTS,HomebaseCD,HomeBaseID,Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,Month8Budget,Month9Budget,Month10Budget,Month11Budget,Month12Budget,IsInActive"
                    CommandItemDisplay="Bottom" Width="100%">
                    <Columns>
                        <telerik:GridBoundColumn DataField="AccountNum" HeaderText="Account No." AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterControlWidth="210px"
                            HeaderStyle-Width="230px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountDescription" HeaderText="Account Description"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterControlWidth="200px" HeaderStyle-Width="220px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FiscalYear" HeaderText="Year" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterControlWidth="30px"
                            HeaderStyle-Width="50px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TailNumber" HeaderText="Tail No." AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterControlWidth="60px"
                            HeaderStyle-Width="80px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterControlWidth="80px"
                            HeaderStyle-Width="100px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BudgetID" HeaderText="Home Base" AutoPostBackOnFilter="false"
                            Display="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            HeaderStyle-Width="80px" ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div class="grid_icon">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                CssClass="add-icon-grid" Visible='<%# IsAuthorized(Permission.Database.AddBudgetCatalog)%>'></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                CssClass="edit-icon-grid" Visible='<%# IsAuthorized(Permission.Database.EditBudgetCatalog)%>'></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                                CssClass="delete-icon-grid" Visible='<%# IsAuthorized(Permission.Database.DeleteBudgetCatalog)%>'></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                    <span class="mnd_text">Account No.</span>
                                </td>
                                <td class="tdLabel220">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbAcctNmbr" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                    MaxLength="32" CssClass="text120" ValidationGroup="Save" OnTextChanged="Acctnum_TextChanged"
                                                    AutoPostBack="true" onBlur="return RemoveSpecialChars(this);"></asp:TextBox>
                                                <asp:Button ID="btnAcctNumber" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radAccountMasterPopup');return false;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvAcctNmbr" runat="server" ControlToValidate="tbAcctNmbr"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"
                                                    ErrorMessage="Account No. is Required."></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvAcctNmbr" runat="server" ControlToValidate="tbAcctNmbr"
                                                    ErrorMessage="Invalid Account No." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <span class="mnd_text">Year</span>
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbYear" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                    MaxLength="4" CssClass="text120"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvYear" runat="server" ControlToValidate="tbYear"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"
                                                    ErrorMessage="Year is Required."></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="rvYear" runat="server" ControlToValidate="tbYear" Type="Double"
                                                    MinimumValue="1900" MaximumValue="2100" ValidationGroup="Save" CssClass="alert-text"
                                                    ErrorMessage="Year should be 1900 to 2100" Display="Dynamic"></asp:RangeValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td class="tdLabel220" valign="top">
                                    <asp:TextBox ID="tbDescription" runat="server" CssClass="text120" ReadOnly="true"></asp:TextBox>&nbsp;
                                </td>
                                <td class="tdLabel150" valign="top">
                                    Tail No.
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbTailNmbr" runat="server" CssClass="text120" MaxLength="9" AutoPostBack="true"
                                                    OnTextChanged="tbTailNmbr_TextChanged"></asp:TextBox>
                                                <asp:Button ID="btnTailNo" CssClass="browse-button" OnClientClick="javascript:openWin('radFleetProfilePopup');return false;"
                                                    runat="server"></asp:Button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvTailNmbr" runat="server" ControlToValidate="tbTailNmbr"
                                                    ErrorMessage="Invalid Aircraft Tail No." Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="left">
                    <td>
                        <table width="50%" cellpadding="0" cellspacing="0" class="padding_top_5">
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend>Budget Months</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    1:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth1Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        Text="0.00"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth1Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    2:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth2Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        Text="0.00"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth2Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    3:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth3Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        Text="0.00"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth3Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    4:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth4Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" Text="0.00" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth4Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revOne" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ControlToValidate="tbMonth1Budget" CssClass="alert-text" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revTwo" runat="server" Display="Dynamic" ControlToValidate="tbMonth2Budget"
                                                        CssClass="alert-text" ErrorMessage="000000000000.00" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revThree" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ControlToValidate="tbMonth3Budget" CssClass="alert-text" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revFour" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ControlToValidate="tbMonth4Budget" CssClass="alert-text" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    5:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth5Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        Text="0.00"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth5Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    6:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth6Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        Text="0.00"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth6Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    7:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%-- <asp:TextBox ID="tbMonth7Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        Text="0.00"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth7Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    8:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth8Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" Text="0.00" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth8Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revFive" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ControlToValidate="tbMonth5Budget" CssClass="alert-text" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revSix" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$" ControlToValidate="tbMonth6Budget"
                                                        CssClass="alert-text" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revSeven" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ControlToValidate="tbMonth7Budget" CssClass="alert-text" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revEight" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ControlToValidate="tbMonth8Budget" CssClass="alert-text" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    9:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%-- <asp:TextBox ID="tbMonth9Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        Text="0.00"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth9Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    10:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth10Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        Text="0.00"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth10Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    11:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth11Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                        Text="0.00"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth11Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    12:
                                                </td>
                                                <td class="pr_radtextbox_117">
                                                    <%--<asp:TextBox ID="tbMonth12Budget" runat="server" CssClass="text120" MaxLength="15"
                                                        onBlur="return ValidateEmptyTextbox(this, event)" Text="0.00" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>--%>
                                                    <telerik:RadNumericTextBox ID="tbMonth12Budget" runat="server" Type="Currency" Culture="en-US"
                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="12" Value="0.00"
                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revNine" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ControlToValidate="tbMonth9Budget" CssClass="alert-text" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revTen" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$" ControlToValidate="tbMonth10Budget"
                                                        CssClass="alert-text" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revEleven" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ControlToValidate="tbMonth11Budget" CssClass="alert-text" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revTwelve" runat="server" Display="Dynamic" ErrorMessage="000000000000.00"
                                                        ControlToValidate="tbMonth12Budget" CssClass="alert-text" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"
                                                        ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" CssClass="button" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            ValidationGroup="Save" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" CssClass="button" Text="Cancel" runat="server" OnClick="Cancel_Click"
                            CausesValidation="false" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnAccountNum" runat="server" />
                        <asp:HiddenField ID="hdnFleetID" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
