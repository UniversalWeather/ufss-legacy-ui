﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head runat="server">
    <title>Client Code</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
<link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
   <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
   <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript">
        var selectedRowData = null;
        var jqgridTableId = '#gridClientCode';
        var clientcodeId = null;
        var clientcd = null;
        var clientSelectedCd = "";
        var isopenlookup = false;
        $(document).ready(function () {

            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });
            clientSelectedCd = $.trim(unescape(getQuerystring("ClientCD", "")));
            if (clientSelectedCd != "") {
                isopenlookup = true;
            }
                
            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                
                if (rowData["ClientCD"] == undefined) {
                    showMessageBox('Please select a record.', popupTitle);
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();               
                popupwindow("/Views/Settings/Company/ClientCodeCatalog.aspx?IsPopup=Add", popupTitle, 1100, 798, jqgridTableId);
                return false;
            });


            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                clientcodeId = rowData['ClientId'];
                clientcd = rowData['ClientCD'];
                var widthDoc = $(document).width();
                if (clientcodeId != undefined && clientcodeId != null && clientcodeId != '' && clientcodeId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a record.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        dataType: 'json',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=ClientCode&clientcodeId=' + clientcodeId,
                        contentType: 'application/json',
                        success: function (data) {
                            verifyReturnedResultForJqgrid(data);
                            $(jqgridTableId).trigger('reloadGrid');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (!IsNullOrEmptyOrUndefined(content)) {
                                if (content.indexOf('404'))
                                    showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                            }
                        }
                    });

                }
            }                     

            $("#recordEdit").click(function () {
                             

                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                clientcodeId = rowData['ClientId'];
                var widthDoc = $(document).width();
                if (clientcodeId != undefined && clientcodeId != null && clientcodeId != '' && clientcodeId != 0) {
                    popupwindow("/Views/Settings/Company/ClientCodeCatalog.aspx?IsPopup=&ClientCodeId=" + clientcodeId, popupTitle, 1100, 798, jqgridTableId);
                }
                else {
                    showMessageBox('Please select a client code.', popupTitle);
                }
                return false;
            });

            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");                   
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'clientcode';
                    postData.showInactive = $('#chkSearchActiveOnly').is(':checked') ? false : true;
                    postData.clientCd = clientSelectedCd;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 230,
                width: 500,                
                viewrecords: true,
                rowNum: $("#rowNum").val(),              
                multiselect: false,
                pager: "#pg_gridPager",
                colNames: ['ClientId', 'Code', 'Description'],
                colModel: [
                    { name: 'ClientId', index: 'ClientID', key: true, hidden: true },
                    { name: 'ClientCD', index: 'ClientCD', width: 100, fixed: true },
                    { name: 'ClientDescription', index: 'ClientDescription', width: 357, fixed: true }

                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);

                },
                onSelectRow: function (id) {
                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['ClientId'];//replace name with any column
                    clientSelectedCd = $.trim(rowData['ClientCD']);

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $(jqgridTableId).jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                afterInsertRow: function (rowid, rowObject) {
                    var rowData = $(jqgridTableId).jqGrid("getRowData", rowid);
                    var lastSel = rowData['ClientCD'];//replace name with any column                    
                    
                    if ($.trim(clientSelectedCd) == $.trim(lastSel)) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        $(jqgridTableId).jqGrid('setSelection', rowid);
                    }
                }
            });
            
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
                              
            $("#pagesizebox").insertBefore('.ui-paging-info');
            
        });
       

        function reloadClientCode() {            
                $(jqgridTableId).jqGrid().trigger('reloadGrid');              
        }

        function reloadPageSize() {
            var myGrid = $(jqgridTableId);
            var currentValue = $("#rowNum").val();
            myGrid.setGridParam({ rowNum: currentValue });
            myGrid.trigger('reloadGrid');
        }


        function returnToParent(rowData) {
            var oArg = new Object();
            oArg.ClientDescription = rowData["ClientDescription"];
            oArg.ClientCD = rowData["ClientCD"];
            oArg.ClientID = rowData["ClientId"];

            oArg.Arg1 = oArg.ClientCD;
            oArg.CallingButton = "ClientCD";

            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }

    </script>
    <style type="text/css">
        body {
            background: none !important;
        }
    </style>
</head>
<body>
    <form id="form1">
       <div class="client_code_popup">
        <div class="jqgrid row">

                <table class="box1">

              <tr>
                <td>
                    <table class="client_headingtop">
                        <tbody><tr>
                            <td>
                                <div class="status-list">                                    
                                    <span>
                                        <input type="checkbox" name="chkSearchActiveOnly" id="chkSearchActiveOnly"/><label for="chkSearchActiveOnly">Active Only</label></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <input type="submit" class="button" id="btnSearch" value="Search" name="btnSearch" onclick="reloadClientCode();return false;">
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>

                    <tr>
                        <td>
                            <table id="gridClientCode" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>                              
                               <div id="pagesizebox">
                               <span>Size:</span>
                                <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                </div> 
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                    <div id="jqgridOverflow" style="color: red; display: none;">Loading...</div>
                                    <div class="grid_icon">
                                        <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                        <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                        <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                    </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                            </div>
                                </div>
                        </td>
                    </tr>
                </table>

        </div>
        </div>
    </form>
</body>
</html>
