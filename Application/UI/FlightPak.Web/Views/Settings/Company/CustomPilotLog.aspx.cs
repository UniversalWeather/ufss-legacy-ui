﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.Company
{
    public partial class CustomPilotLog : BaseSecuredPage
    {
        int x;
        string HomeBase;
        Int64 HomeBaseID;
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        HomeBase = (Request.QueryString["HomeBase"]);
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewCompanyProfileCatalog);
                            tbBaseDescription.Enabled = false;
                            tbMainBase.Enabled = false;
                            if (!string.IsNullOrEmpty(HomeBase))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var CompanyData = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD == HomeBase);
                                    foreach (FlightPakMasterService.GetAllCompanyMaster companyEntity in CompanyData)
                                    {
                                        if (companyEntity.HomebaseID != null)
                                        {
                                            hdnHomeBaseID.Value = companyEntity.HomebaseID.ToString();
                                        }
                                        if (companyEntity.HomebaseCD != null)
                                        {
                                            tbMainBase.Text = companyEntity.HomebaseCD;
                                        }
                                        if (companyEntity.BaseDescription != null)
                                        {
                                            tbBaseDescription.Text = companyEntity.BaseDescription;
                                        }
                                    }
                                }
                            }
                            SetDefaultSortOrder();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomPilotLog);
                }
            }
        }

        private void SetDefaultSortOrder()
        {
            GridSortExpression sortExpr = new GridSortExpression();
            sortExpr.FieldName = "OriginalDescription";
            sortExpr.SortOrder = GridSortOrder.Ascending;
            dgCustomPilot.MasterTableView.AllowNaturalSort = false;
            dgCustomPilot.MasterTableView.SortExpressions.AddSortExpression(sortExpr);
            dgCustomPilot.Rebind();
        }

        protected void dgCustomPilot_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSFlightLog oTSFlightLog = new TSFlightLog();
                        oTSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetAllPilotLogList(oTSFlightLog);
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgCustomPilot.DataSource = ObjRetVal.EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomPilotLog);
                }
            }
        }
        protected void CustomPilot_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem Item = (GridDataItem)e.Item;
                if (hdnSave.Value == "Save")
                {
                    if ((Item.GetDataKeyValue("TSFlightLogID") != null) && (Convert.ToInt64(Item.GetDataKeyValue("TSFlightLogID")) != 0))
                    {
                        ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = false;                        
                        ((TextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = false;
                        ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = false;
                    }
                    else
                    {
                        ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = true;
                        ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Focus();
                        ((TextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = true;
                        ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = true;
                    }
                }
                else if (hdnSave.Value == "Update")
                {
                    ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = true;
                    ((TextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = true;
                    ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = true;
                }
                else if (hdnSave.Value == "Delete")
                {
                    ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = false;
                    ((TextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = false;
                    ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = false;
                }
                else if (string.IsNullOrEmpty(hdnSave.Value))
                {
                    ((TextBox)(Item["custdesc"].FindControl("tbCustDesc"))).Enabled = false;
                    ((TextBox)(Item["Order"].FindControl("tbOrder"))).Enabled = false;
                    ((CheckBox)(Item["IsPrint"].FindControl("chkIsPrint"))).Enabled = false;
                }
                
            }
        }

        protected void dgCountry_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCustomPilot.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCustomPilot;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomPilotLog);
                }
            }
        }
        protected void lbtnInitInsert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = "Save";
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<GetAllPilotLog> lstTSFlightLog = new List<GetAllPilotLog>();
                            GetAllFlightLog TSFlightLogdef = new GetAllFlightLog();
                            TSFlightLogdef.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                            TSFlightLog oTSFlightLog = new TSFlightLog();
                            oTSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                            var objFlightLog = Service.GetAllPilotLogList(oTSFlightLog);
                            if (objFlightLog.ReturnFlag == true)
                            {
                                lstTSFlightLog = objFlightLog.EntityList.Where(x => x.OriginalDescription.Contains("Crew Log Custom Label ")).ToList();
                            }
                            Int64 CCLCount = 0;
                            if (lstTSFlightLog.Count != 0)
                            {
                                CCLCount = Convert.ToInt64(lstTSFlightLog[lstTSFlightLog.Count - 1].OriginalDescription.Replace("Crew Log Custom Label ", ""));
                            }
                            List<TSFlightLog> FinalList = new List<TSFlightLog>();
                            List<TSFlightLog> RetainList = new List<TSFlightLog>();
                            List<TSFlightLog> NewList = new List<TSFlightLog>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainCrewVisaGrid(dgCustomPilot);
                            FinalList.AddRange(RetainList);
                            // Bind Selected New Item and add into Final List
                            TSFlightLog TSFlightLog = new TSFlightLog();
                            if (CCLCount != 4)
                            {
                                TSFlightLog.OriginalDescription = "Crew Log Custom Label " + (CCLCount + 1);
                                TSFlightLog.CustomDescription = "Crew Log Custom Label " + (CCLCount + 1);
                            }
                            else if (CCLCount == 4)
                            {
                                TSFlightLog.OriginalDescription = "Crew Log Custom Label " + (CCLCount + 3);
                                TSFlightLog.CustomDescription = "Crew Log Custom Label " + (CCLCount + 3);
                            }
                            TSFlightLog.SequenceOrder = 0;
                            TSFlightLog.IsPrint = true;
                            TSFlightLog.IsDeleted = false;
                            NewList.Add(TSFlightLog);
                            FinalList.AddRange(NewList);
                            // Bind final list into Session
                            Session["CrewVisa"] = FinalList;
                            dgCustomPilot.DataSource = FinalList;
                            dgCustomPilot.Rebind();
                            LinkButton insertCtl, editCtl, delCtl;
                            insertCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                            delCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                            editCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                            insertCtl.Enabled = false;
                            delCtl.Enabled = false;
                            editCtl.Enabled = false;
                            //dgCustomPilot.Enabled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomPilotLog);
                }
            }
        }
        protected void lbtnInitEdit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = "Update";
                        List<TSFlightLog> RetainList = new List<TSFlightLog>();
                        RetainList = RetainCrewVisaGrid(dgCustomPilot);
                        dgCustomPilot.DataSource = RetainList;
                        dgCustomPilot.Rebind();
                        if (Session["OriginalDescription"] != null)
                        {
                            foreach (GridDataItem item in dgCustomPilot.MasterTableView.Items)
                            {
                                if (item["desc"].Text.Trim() == Session["OriginalDescription"].ToString().Trim())
                                {
                                    item.Selected = true;
                                    break;
                                }
                            }
                        }
                        //dgCustomPilot.Enabled = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomPilotLog);
                }
            }
        }
        protected void lbtnDelete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = "Delete";
                        GridDataItem Item = (GridDataItem)dgCustomPilot.SelectedItems[0];
                        TSDefinitionLog TSFlightLog = new TSDefinitionLog();
                        TSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                        TSFlightLog.IsDeleted = true;
                        TSFlightLog.TSDefinitionLogID = Convert.ToInt64(Item.GetDataKeyValue("TSFlightLogID"));
                        using (MasterCatalogServiceClient TsFlightService = new MasterCatalogServiceClient())
                        {
                            TsFlightService.DeleteTSDefinitionLog(TSFlightLog);
                        }
                        dgCustomPilot.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomPilotLog);
                }
            }
        }
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = string.Empty;
                        foreach (GridDataItem Item in dgCustomPilot.MasterTableView.Items)
                        {
                            TSDefinitionLog TSFlightLog = new TSDefinitionLog();
                            TSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                            TSFlightLog.PrimeKey = x;// Hardcoded until DB is ready
                            if (!string.IsNullOrEmpty(((Label)Item["desc"].FindControl("lbDesc")).Text))
                            {
                                TSFlightLog.OriginalDescription = ((Label)Item["desc"].FindControl("lbDesc")).Text;
                            }
                            if (!string.IsNullOrEmpty(((TextBox)Item["custdesc"].FindControl("tbCustDesc")).Text))
                            {
                                TSFlightLog.CustomDescription = ((TextBox)Item["custdesc"].FindControl("tbCustDesc")).Text;
                            }
                            if (!string.IsNullOrEmpty(((TextBox)Item["Order"].FindControl("tbOrder")).Text))
                            {
                                TSFlightLog.SequenceOrder = Convert.ToInt32(((TextBox)Item["Order"].FindControl("tbOrder")).Text);
                            }
                            if ((((CheckBox)Item["IsPrint"].FindControl("chkIsPrint")).Checked))
                            {
                                TSFlightLog.IsPrint = true;
                            }
                            else
                            {
                                TSFlightLog.IsPrint = false;
                            }
                            if (Item.GetDataKeyValue("TSFlightLogID") != null)
                            {
                                TSFlightLog.TSDefinitionLogID = Convert.ToInt64(Item.GetDataKeyValue("TSFlightLogID"));
                            }
                            else
                            {
                                TSFlightLog.TSDefinitionLogID = 0;
                            }
                            TSFlightLog.IsDeleted = false;
                            TSFlightLog.Category = "Pilot Log";
                            using (MasterCatalogServiceClient TsFlightService = new MasterCatalogServiceClient())
                            {
                                var Result = TsFlightService.AddTSDefinitionLog(TSFlightLog);
                                if (Result.ReturnFlag == true)
                                {
                                    //ShowSuccessMessage();
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowSuccessMessage", "ShowSuccessMessage();", true);
                                }
                            }
                        }
                        List<TSFlightLog> FinalList = new List<TSFlightLog>();
                        List<TSFlightLog> RetainList = new List<TSFlightLog>();
                        List<TSFlightLog> NewList = new List<TSFlightLog>();
                        // Retain Grid Items and bind into List and add into Final List
                        //RetainList = RetainCrewVisaGrid(dgCustomPilot);
                        //FinalList.AddRange(RetainList);
                        //dgCustomPilot.DataSource = FinalList;
                        //dgCustomPilot.Rebind();
                        TSFlightLog oTSFlightLog = new TSFlightLog();
                        oTSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetAllPilotLogList(oTSFlightLog);
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgCustomPilot.DataSource = ObjRetVal.EntityList;
                            }
                            dgCustomPilot.Rebind();
                            LinkButton insertCtl, editCtl, delCtl;
                            insertCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                            delCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                            editCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                            insertCtl.Enabled = true;
                            delCtl.Enabled = true;
                            editCtl.Enabled = true;
                            //dgCustomPilot.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomPilotLog);
                }
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = string.Empty;
                        LinkButton insertCtl, editCtl, delCtl;
                        insertCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        delCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        editCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        insertCtl.Enabled = true;
                        delCtl.Enabled = true;
                        editCtl.Enabled = true;
                        dgCustomPilot.Rebind();
                        //dgCustomPilot.Enabled = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomPilotLog);
                }
            }
        }
        protected void Reset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<TSFlightLog> lstTSFlightLog = new List<TSFlightLog>();
                            TSFlightLog oTSFlightLog = new TSFlightLog();
                            oTSFlightLog.TSFlightLogID = 0;
                            oTSFlightLog.CustomerID = 0;
                            oTSFlightLog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                            oTSFlightLog.Category = "Pilot Log";
                            oTSFlightLog.IsDeleted = false;
                            if (hdnSave.Value == "ResetYes")
                            {
                                oTSFlightLog.IsPrint = true;
                            }
                            else if (hdnSave.Value == "ResetNo")
                            {
                                oTSFlightLog.IsPrint = false;
                            }
                            var ObjRetVal = ObjService.ResetFlightLog(oTSFlightLog);
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                dgCustomPilot.DataSource = ObjRetVal.EntityList;
                            }
                            hdnSave.Value = string.Empty;
                            dgCustomPilot.Rebind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomPilotLog);
                }
            }
        }
        private List<TSFlightLog> RetainCrewVisaGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<TSFlightLog> TSFlightLogList = new List<TSFlightLog>();
                foreach (GridDataItem Item in grid.MasterTableView.Items)
                {
                    TSFlightLog TSFlightLog = new TSFlightLog();
                    if (Item.GetDataKeyValue("TSFlightLogID") != null)
                    {
                        TSFlightLog.TSFlightLogID = Convert.ToInt64(Item.GetDataKeyValue("TSFlightLogID").ToString());
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["desc"].FindControl("lbDesc")).Text))
                    {
                        TSFlightLog.OriginalDescription = ((Label)Item["desc"].FindControl("lbDesc")).Text;
                    }
                    if (!string.IsNullOrEmpty(((TextBox)Item["custdesc"].FindControl("tbCustDesc")).Text))
                    {
                        TSFlightLog.CustomDescription = ((TextBox)Item["custdesc"].FindControl("tbCustDesc")).Text;
                    }
                    if (!string.IsNullOrEmpty(((TextBox)Item["Order"].FindControl("tbOrder")).Text))
                    {
                        TSFlightLog.SequenceOrder = Convert.ToInt32(((TextBox)Item["Order"].FindControl("tbOrder")).Text);
                    }
                    if ((((CheckBox)Item["IsPrint"].FindControl("chkIsPrint")).Checked))
                    {
                        TSFlightLog.IsPrint = true;
                    }
                    else
                    {
                        TSFlightLog.IsPrint = false;
                    }
                    TSFlightLog.IsDeleted = false;
                    TSFlightLogList.Add(TSFlightLog);
                }
                return TSFlightLogList;
            }
        }

        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, editCtl, delCtl;
                insertCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                delCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                editCtl = (LinkButton)dgCustomPilot.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                //if (IsAuthorized(Permission.Database.AddCustomFlightLog))
                //{
                //    insertCtl.Visible = true;
                //    if (add)
                //        insertCtl.Enabled = true;
                //    else
                //        insertCtl.Enabled = false;
                //}
                //else
                //{
                    insertCtl.Visible = false;
                //}
                //if (IsAuthorized(Permission.Database.DeleteCustomFlightLog))
                //{
                //    delCtl.Visible = true;
                //    if (delete)
                //    {
                //        delCtl.Enabled = true;
                //        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                //    }
                //    else
                //    {
                //        delCtl.Enabled = false;
                //        delCtl.OnClientClick = string.Empty;
                //    }
                //}
                //else
                //{
                    delCtl.Visible = false;
                //}
                    if (IsAuthorized(Permission.Database.EditCompanyProfileCatalog))
                {
                    editCtl.Visible = true;
                    if (edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
            }
        }
        private void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //btnSaveChanges.Enabled = Enable;
                //btnCancel.Enabled = Enable;
                //btnReset.Enabled = Enable;
            }
        }
        protected void dgCustomPilot_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem Item = dgCustomPilot.SelectedItems[0] as GridDataItem;
                        Session["OriginalDescription"] = Item["desc"].Text;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CustomFlightLog);
                }
            }
        }

        protected void dgCustomPilot_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            //Apply custom sorting

            GridSortExpression sortExpr = new GridSortExpression();
            switch (e.OldSortOrder)
            {
                case GridSortOrder.None:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Ascending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Descending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Descending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;

            }
            e.Canceled = true;
            dgCustomPilot.Rebind();

        }
    }
}