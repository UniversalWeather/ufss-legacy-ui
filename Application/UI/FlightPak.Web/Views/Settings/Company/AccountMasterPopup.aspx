﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <title>Account</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>    
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="../../../App_Themes/Default/style.css" rel="stylesheet" />
    <script type="text/javascript">
        var accountId = null;
        var accountNum = null;
        var selectedRowData = null;
        var jqgridTableId = '#gridAccount';

        var isMultiSelect = false;
        var isopenlookup = false;
        isMultiSelect = getQuerystring("IsMultiselect", "") == "" ? false : true;

        $(document).ready(function () {
            var accountNum = getQuerystring("AccountNum", "");
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            accountNum = $.trim(unescape(getQuerystring("AccountNum", "")));
            if (accountNum != "") {
                isopenlookup = true;
            }

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                returnToParent(rowData);
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/Views/Settings/Company/AccountsCatalog.aspx?IsPopup=Add", popupTitle, widthDoc + 200, 650, jqgridTableId, 'radAccountMasterPopupAdd', true);
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                accountId = rowData['AccountID'];
                accountNum = rowData['AccountNum'];
                var widthDoc = $(document).width();

                if (accountId != undefined && accountId != null && accountId != '' && accountId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select an account.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        async: true,
                        type: 'Get',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Account&accountId=' + accountId + '&accountNum=' + accountNum,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;                            
                            if (content.indexOf('404'))
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                        }
                    });
                }
            }

            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                accountId = rowData['AccountID'];
                var widthDoc = $(document).width();
                if (accountId != undefined && accountId != null && accountId != '' && accountId != 0) {
                    popupwindow("/Views/Settings/Company/AccountsCatalog.aspx?IsPopup=&AccountID=" + accountId, popupTitle, widthDoc + 200, 650, jqgridTableId, 'radAccountMasterPopupAdd', true);
                }
                else {
                    showMessageBox('Please select an account.', popupTitle);
                }
                return false;
            });

            jQuery(jqgridTableId).jqGrid({

                url: '/Views/Utilities/ApiCallerwithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }

                    postData.sendIcaoId = $("#chkHomebaseOnly").is(':checked') ? true : false;
                    postData.apiType = 'fss';
                    postData.method = 'accounts';
                    postData.accountNum = accountNum;
                    postData.accountId = 0;
                    postData.IsInActive = $("#chkActiveOnly").is(':checked') ? true : false;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 300,
                width: 600,
                autowidth: false,
                shrinkToFit: false,
                viewrecords: true,
				multiselect: false,
                pager: "#pg_gridPager",
                colNames: ['IsBilling','Account No.', 'Description', 'Home Base', 'AccountID','Active'],
                colModel: [
                    { name: 'IsBilling', index: 'IsBilling', hidden: true },
                    { name: 'AccountNum', index: 'AccountNum', width: 100 },
                    { name: 'AccountDescription', index: 'AccountDescription', width: 300 },
                    { name: 'IcaoID', index: 'IcaoID', width: 100 },
                    { name: 'AccountID', index: 'AccountID', key: true, hidden: true },
                    { name: 'IsInactive', index: 'IsInactive', width: 40, search: false, align: 'center',fixed: true , width: 40, formatter: function (cellvalue, options, rowObject) {
                        var check = cellvalue == false ? "checked='checked'" : "";
                        return "<input type='checkbox' style='width:100%' disabled='disabled' " + check + " readonly/>";
                    }},
                    
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);
                },
                onSelectRow: function (id) {

                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['AccountDescription'];//replace name with any column

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $(jqgridTableId).jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                gridComplete: function () {
                    var rowIds = $(this).getDataIDs();
                    for (var i = 0; i <= rowIds.length; i++) {
                        var rowData = $(this).getRowData(rowIds[i]);
                        if (rowData['AccountNum'] == accountNum) {
                            $(this).jqGrid('setSelection', rowIds[i]);
                        }
                    }

                }
            });
            $("#pagesizebox").insertBefore('.ui-paging-info');
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
        });
        function reloadAccountGrid() {
            $(jqgridTableId).trigger('reloadGrid');
        }

    </script>
    <script type="text/javascript">

        function returnToParent(rowData) {
            var oArg = new Object();
            oArg.AccountNum = rowData["AccountNum"];
            oArg.AccountDescription = rowData["AccountDescription"];
            oArg.IcaoID = rowData["IcaoID"];
            oArg.AccountId = rowData["AccountID"];
            oArg.Arg1 = oArg.AccountNum;
            
            if (isMultiSelect) {
                oArg.CallingButton = "MultipleAccountNum";
            }
            else {
                oArg.CallingButton = "AccountNum";
            }

            var oWnd = GetRadWindow();
            if (oArg.AccountNum) {
                oWnd.close(oArg);
            }
            else {
                oWnd.close();
            }
        }

        function reloadPageSize() {
            var myGrid = $(jqgridTableId);
            var currentValue = $("#rowNum").val();
            myGrid.setGridParam({ rowNum: currentValue });
            myGrid.trigger('reloadGrid');
        }
    </script>

    <style type="text/css">       
        body {
            background: none !important;
            height:100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="client_code_popup account_codepopup">
        <div class="jqgrid row">

            <div class="col-md-12">
                <table class="box1">
                    <tr>
                        <td colspan="2" align="left">
                            <div class="account_headingtop">
                            <input type="checkbox" name="Homebase" value="HomeBase Only" id="chkHomebaseOnly" />
                                Home Base Only
                            <input type="checkbox" name="Active" value="Active Only" id="chkActiveOnly" checked="checked" />
                                Active Only
                            <input id="btnSearch" class="button" value="Search" type="submit" name="btnSearch" onclick="reloadPageSize(); return false;" />
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table id="gridAccount" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <span class="Span">Page Size:</span>
                                <input class="PageSize" id="rowNum" type="text" value="20" maxlength="5"/>
                                <input id="btnChange" class="btnChange" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                            </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
