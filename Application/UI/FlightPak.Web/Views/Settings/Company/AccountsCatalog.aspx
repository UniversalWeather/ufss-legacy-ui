﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="AccountsCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Company.AccountsCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function CheckTxtBox(control) {
            var tbAccountNumber = document.getElementById("<%=tbAccountNumber.ClientID%>").value;
            var tbCorpAcct = document.getElementById("<%=tbCorpAcct.ClientID%>").value;
            var AccountFormat;
            if (control == 'AccNum') {
                AccountFormat = document.getElementById('<%=hdnAccNumFormat.ClientID%>').value;
            }
            if (control == 'CorpAcct') {
                AccountFormat = document.getElementById('<%=hdnCorpAccNumFormat.ClientID%>').value;
            }
            if (control == 'AccNum') {
                if (tbAccountNumber == "") {
                    ValidatorEnable(document.getElementById('<%=rfvAccountNumber.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbAccountNumber.ClientID%>').focus()", 0);
                    return false;
                }
                else {
                    if (ValidateAccountFormat(document.getElementById("<%=tbAccountNumber.ClientID%>"), control) == false) {
                        alert("Account No. didn't match the Account format pattern " + AccountFormat);
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            }
            if (control == 'CorpAcct') {
                if (tbCorpAcct != "") {
                    if (ValidateAccountFormat(document.getElementById("<%=tbCorpAcct.ClientID%>"), control) == false) {
                        alert("Corporate Account number didn't match the Account format pattern " + AccountFormat);
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            }
        }
        
    </script>
    <script type="text/javascript">
        function fnAllowedChars(myfield, e) {
            var key;
            var keychar;
            var allowedString = "9.!";

            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;
            keychar = String.fromCharCode(key);
            if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                return true;
            else if ((allowedString).indexOf(keychar) > -1)
                return true;
            else
                return false;
        }
        function GenerateExpression(AccountFormat) {
            var splitdot = AccountFormat.split('');
            var regx = "^";
            for (i = 0; i < splitdot.length; i++) {
                if (splitdot[i] == '9') {
                    regx = regx + "[0-9]";
                }
                if (splitdot[i] == '.') {
                    regx = regx + "[\\. ]";
                }
                if (splitdot[i] == '!') {
                    regx = regx + "[\\! ]";
                }
            }
            regx = regx + "$";
            return regx;
        }
        function ValidateAccountFormat(Control, FormatControl) {
            var AccountFormat;
            if (FormatControl == 'AccNum') {
                AccountFormat = document.getElementById('<%=hdnAccNumFormat.ClientID%>').value;
            }
            if (FormatControl == 'CorpAcct') {
                AccountFormat = document.getElementById('<%=hdnCorpAccNumFormat.ClientID%>').value;
            }
            if (AccountFormat == "") {
                return true;
            }
            var Expression = GenerateExpression(AccountFormat);
            var Pattern = new RegExp(Expression);
            if (Pattern.exec(Control.value)) {
                return true;
            } else {
                return false;
            }
        }
        function CheckAccountFormat() {
            var ReturnValue = true;
            if (document.getElementById('<%=hdnSave.ClientID%>').value == "Save") {
                if (CheckTxtBox('AccNum') == false) {
                    ReturnValue = false;
                }
                else {
                    ReturnValue = true;
                }
            }
            if (ReturnValue == true) {
                if (document.getElementById('<%=tbCorpAcct.ClientID%>').value != "") {
                    if (CheckTxtBox('CorpAcct') == false) {
                        ReturnValue = false;
                    }
                    else {
                        ReturnValue = true;
                    }
                }
            }

            if (ReturnValue == true) {
                document.getElementById('<%=btnSaveChanges1.ClientID%>').click();
            }
            return false;
        }

        function RemoveSpecialCharsNew(elementRef) {
            var checkValue = new String(elementRef.value);
            var newValue = '';
            var isExists = false;

            for (var i = 0; i < checkValue.length; i++) {
                var currentChar = checkValue.charAt(i);

                if ((currentChar == '0') || (currentChar == '1') || (currentChar == '2') || (currentChar == '3') || (currentChar == '4') || (currentChar == '5') ||
                (currentChar == '6') || (currentChar == '7') || (currentChar == '8') || (currentChar == '9') || (currentChar == '!') || (currentChar == '.')) {
                    newValue += currentChar;
                }

                if (checkValue.indexOf(currentChar) > -1) {
                    isExists = true;
                }
            }

            elementRef.value = newValue;

        }
        function ShowReports(radWin, ReportFormat, ReportName) {
            document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
            document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
            if (ReportFormat == "EXPORT") {
                url = "../../Reports/ExportReportInformation.aspx";
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); 
                var oWnd = oManager.open(url, 'RadExportData'); 
                return true;
            }
            else {
                return true;
            }
        }
        function OnClientCloseExportReport(oWnd, args) {
            document.getElementById("<%=btnShowReports.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgAccounts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgAccounts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAccounts" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgAccounts">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAccounts" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnclrFilters_Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnclrFilters_Click" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkSearchHomebaseOnly">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAccounts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkSearchActiveOnly">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAccounts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbHomeBase">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            $(document).ready(function () {
                $('#art-contentLayout_popup').css("width", "600px");
                $('#art-sheet').css("width", "620px");
                $('#art-sheet-body').css("width", "620px");
                $('#art-sheet-body').css("position", "fixed");
                $('#art-sheet-body').css("padding", "0px 0px 0px 5px");
            });

            function openWin() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=tbHomeBase.ClientID%>").value, "radCompanyMasterPopup");
            }

            function openReport() {

                url = "../../Reports/ExportReportInformation.aspx?Report=RptDBAccountsExport&UserCD=UC";

                //url = "../../Reports/ExportReportInformation.aspx?Report=RptDBAccounts";
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); 
                var oWnd = oManager.open(url, "RadExportData"); 
            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); // GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName); 
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.location.reload();
            }

            function OnClientClose(oWnd, args) {

                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.HomebaseID;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";

                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";

                    }
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;
                        
                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
        <script runat="server">
            void lnkBudgetButton_Click(object sender, EventArgs e)
            {
                string URL = "../Company/BudgetCatalog.aspx?Screen=Budget&AccountID=" + hdnAcctID.Value + "&AcctNO=" + tbAccountNumber.Text;
                Session["SelectedBudgetID"] = null;
                Response.Redirect(URL);
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Move,Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExportReport" Title="Export Report Information" KeepInScreenBounds="true"
                AutoSize="false" Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Accounts</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBAccounts');"
                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBAccountsExport');return false;"
                            OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                        <asp:Button ID="btnShowReports" runat="server" CssClass="button-disable" Style="display: none;"
                            OnClick="btnShowReports_OnClick" />
                        <a href="../../Help/ViewHelp.aspx?Screen=AccountsHelp" class="help-icon" target="_blank"
                            title="Help"></a>
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                    </span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlAccountCatalogForm" runat="server" Visible="true" Style="z-index: -1">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="fleet_select">
                            <asp:LinkButton ID="lnkBudget" CssClass="fleet_link" runat="server" OnClick="lnkBudgetButton_Click"
                                Text="Budget"></asp:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchHomebaseOnly" runat="server" Text="Home Base Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span> <span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                            AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgAccounts" runat="server" AllowSorting="true" OnNeedDataSource="Accounts_BindData"
                OnItemCommand="Accounts_ItemCommand" OnItemCreated="Accounts_ItemCreated" OnUpdateCommand="Accounts_UpdateCommand"
                OnInsertCommand="Accounts_InsertCommand" OnDeleteCommand="Accounts_DeleteCommand"
                AutoGenerateColumns="true" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="Accounts_SelectedIndexChanged"
                OnPreRender="Accounts_PreRender" OnPageIndexChanged="Accounts_PageIndexChanged"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="341px">
                <MasterTableView ClientDataKeyNames="" DataKeyNames="AccountID,AccountNum,CustomerID,AccountDescription,CorpAccountNum,HomebaseID,IsBilling,LastUpdUID,LastUpdTS,Cost,IsInActive,IsDeleted,HomebaseCD,BaseDescription"
                    CommandItemDisplay="Bottom" Width="100%">
                    <Columns>
                        <telerik:GridBoundColumn DataField="AccountNum" HeaderText="Account No." CurrentFilterFunction="StartsWith"
                            FilterControlWidth="200px" HeaderStyle-Width="220px" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountDescription" HeaderText="Account Description"
                            FilterControlWidth="340px" HeaderStyle-Width="360px" CurrentFilterFunction="StartsWith"
                            ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                            FilterControlWidth="80px" HeaderStyle-Width="100px" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            HeaderStyle-Width="80px" ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div class="grid_icon">
                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                CommandName="InitInsert" Visible='<%# IsAuthorized(Permission.Database.AddAccounts)%>'></asp:LinkButton>
                            <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                ToolTip="Edit" CssClass="edit-icon-grid" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditAccounts)%>'></asp:LinkButton>
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDelete();"
                                CommandName="DeleteSelected" CssClass="delete-icon-grid" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeleteAccounts)%>'></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" class="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel136">
                                    <asp:CheckBox runat="server" ID="chkInclinBilling" Text="Include in Billing" />
                                </td>
                                <td class="tdLabel80">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    <span class="mnd_text">Account No.</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <%--<asp:TextBox ID="tbAccountNumber" runat="server" MaxLength="32" onKeyPress="return fnAllowNumericAndChar(this, event,'.!')"
                                                    onBlur="return RemoveSpecialCharsNew(this)" CssClass="text180" OnTextChanged="Acctnum_TextChanged"
                                                    AutoPostBack="true"> </asp:TextBox>--%>
                                                     <asp:TextBox ID="tbAccountNumber" runat="server" MaxLength="32" onKeyPress="return fnAllowNumericAndChar(this, event,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')"
                                                     CssClass="text180" OnTextChanged="Acctnum_TextChanged"
                                                    AutoPostBack="true"> </asp:TextBox>

                                                <asp:HiddenField ID="hdnAcctID" runat="server" />
                                                <asp:HiddenField ID="hdnCorpAccNumFormat" runat="server" />
                                                <asp:HiddenField ID="hdnAccNumFormat" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ValidationGroup="Save" ID="rfvAccountNumber" runat="server"
                                                    ControlToValidate="tbAccountNumber" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                    ErrorMessage="Account No. is Required."></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvAcctNmbr" runat="server" ControlToValidate="tbAccountNumber"
                                                    ErrorMessage="Unique Account No. is Required." Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" MaxLength="40" runat="server" CssClass="text180"> </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" ValidationGroup="Save" runat="server"
                                                    ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                    ErrorMessage="Description is Required."></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120">
                                    Corporate Account
                                </td>
                                <td>
                                    <asp:TextBox ID="tbCorpAcct" onKeyPress="return fnAllowNumericAndChar(this, event,'.!')"
                                        onBlur="return RemoveSpecialCharsNew(this)" MaxLength="64" runat="server" CssClass="text180"> </asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr valign="top">
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    Home Base
                                </td>
                                <td class="tdLabel130">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbHomeBase" MaxLength="4" runat="server" CssClass="text40" onBlur="return RemoveSpecialChars(this)"
                                                    AutoPostBack="true" OnTextChanged="tbHomeBase_TextChanged"></asp:TextBox>
                                                <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin();return false;" />
                                                <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                    ErrorMessage="Invalid Home Base Code." Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel80" valign="top">
                                    Fixed Cost
                                </td>
                                <td class="pr_radtextbox_180" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <%--<asp:TextBox ID="tbFixedCost" runat="server" MaxLength="17" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                    ValidationGroup="Save" CssClass="text60"> </asp:TextBox>--%>
                                                <telerik:RadNumericTextBox ID="tbFixedCost1" runat="server" Type="Currency" Culture="en-US" ValidationGroup="Save"
                                                    MaxLength="15" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                </telerik:RadNumericTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revCost" runat="server" Display="Dynamic" ErrorMessage="000000000000000.00"
                                                    SetFocusOnError="true" ControlToValidate="tbFixedCost1" ValidationGroup="Save"
                                                    CssClass="alert-text" ValidationExpression="^[0-9]{0,15}(\.[0-9]{0,2})?$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" CssClass="button" Text="Save" runat="server" OnClientClick="return CheckAccountFormat();"
                            OnClick="SaveChanges_Click" ValidationGroup="Save" />
                        <asp:Button ID="btnSaveChanges1" CssClass="button" Text="Save" runat="server" Style="display: none;"
                            OnClick="SaveChanges_Click" ValidationGroup="Save" />
                        <asp:Label ID="lbInjectScript" Text="" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" runat="server" OnClick="Cancel_Click"
                            CausesValidation="false" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
