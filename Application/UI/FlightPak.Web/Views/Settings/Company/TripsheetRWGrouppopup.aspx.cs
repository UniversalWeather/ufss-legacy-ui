﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class TripsheetRWGrouppopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private string RWGroupCD;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["RWGroupCD"]))
                        {
                            RWGroupCD = Request.QueryString["RWGroupCD"].ToUpper().Trim();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// Bind RW Group  Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgRWGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetTripSheetReportHeader> RwGroupList = new List<GetTripSheetReportHeader>();
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                           
                            var ObjRetVal = ObjService.GetTripSheetReportHeaderList();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                RwGroupList = ObjRetVal.EntityList.ToList<GetTripSheetReportHeader>();
                                dgRWGroup.DataSource = RwGroupList;
                                Session["RwGroupList"] = RwGroupList;
                            }
                            if ((chkHomeBaseOnly.Checked == true) ||(!string.IsNullOrEmpty(tbClientCodeFilter.Text)) || (!string.IsNullOrEmpty(tbUserGroup.Text)))
                            {
                                FilterAndSearch(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgRWGroup;
                        //SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Select Item
        /// </summary>
        protected void SelectItem()
        {
            if (!string.IsNullOrEmpty(RWGroupCD))
            {
                foreach (GridDataItem item in dgRWGroup.MasterTableView.Items)
                {
                    if (item.GetDataKeyValue("ReportID") != null)
                    {
                        if (item["ReportID"].Text.ToUpper().Trim() == RWGroupCD)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                if (dgRWGroup.Items.Count > 0)
                {
                    dgRWGroup.SelectedIndexes.Add(0);
                }
            }
        }

        #region "Search and Filter"
        /// <summary>
        /// To search based on HomeBase
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FilterAndSearch(true);
                        return false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        /// <summary>
        /// To search based on Clientcode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FilterByClient_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckClientCodeExist(tbClientCodeFilter, cvClientCodeFilter))
                        {
                            string msgToDisplay = "Invalid ClientCode";
                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "TripSheet Rw Group!", "");
                        }
                        else
                        {
                            FilterAndSearch(true);
                           if(dgRWGroup.Items.Count>0)
                            dgRWGroup.SelectedIndexes.Add(0);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        /// <summary>
        /// To Search based on UserGroup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FilterByUserGroup_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckUserGroupExist(tbUserGroup, cvUserGroup))
                        {
                            string msgToDisplay = "Invalid UserGroup";
                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "TripSheet Rw Group!", "");
                        }
                        else
                        {
                            FilterAndSearch(true);
                            if (dgRWGroup.Items.Count > 0)
                                dgRWGroup.SelectedIndexes.Add(0);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        /// <summary>
        /// To Search the result
        /// </summary>
        /// <param name="IsDataBind"></param>
        /// <returns></returns>
        private bool FilterAndSearch(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsDataBind))
            {
                List<GetTripSheetReportHeader> RwGroupList = new List<GetTripSheetReportHeader>();
                if (Session["RwGroupList"] != null)
                {
                    RwGroupList = (List<FlightPakMasterService.GetTripSheetReportHeader>)Session["RwGroupList"];
                }
                if (RwGroupList.Count != 0)
                {
                    if (chkHomeBaseOnly.Checked == true) { RwGroupList = RwGroupList.Where(x => x.HomebaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetTripSheetReportHeader>(); }
                    if (!string.IsNullOrEmpty(tbClientCodeFilter.Text))
                    {
                        RwGroupList = RwGroupList.Where(x => x.ClientCD != null && x.ClientCD.Trim().ToUpper() == tbClientCodeFilter.Text.Trim().ToUpper()).ToList<GetTripSheetReportHeader>();
                    }
                    if (!string.IsNullOrEmpty(tbUserGroup.Text))
                    {
                        RwGroupList = RwGroupList.Where(x => x.UserGroupCD != null && x.UserGroupCD.Trim().ToUpper() == tbUserGroup.Text.Trim().ToUpper()).ToList<GetTripSheetReportHeader>();
                    }
                    dgRWGroup.DataSource = RwGroupList;
                    if (IsDataBind)
                    {
                        dgRWGroup.DataBind();
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// To Check Unique Code
        /// </summary>
        /// <param name="txtbx"></param>
        /// <param name="cval"></param>
        /// <returns></returns>
        private bool CheckClientCodeExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objClientsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objClientsvc.GetClientByClientCD(txtbx.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.ClientByClientCDResult> ClientList = new List<FlightPakMasterService.ClientByClientCDResult>();
                                ClientList = (List<FlightPakMasterService.ClientByClientCDResult>)objRetVal.ToList();                               
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Check unique UserGroup
        /// </summary>
        /// <param name="txtbx"></param>
        /// <param name="cval"></param>
        /// <returns></returns>
        private bool CheckUserGroupExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {                   
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (AdminService.AdminServiceClient objUsersvc = new AdminService.AdminServiceClient())
                        {
                            var ReturnValue = objUsersvc.GetUserGroupByGroupCode(txtbx.Text.Trim()).EntityInfo;
                            if (ReturnValue != null && ReturnValue.UserGroupCD.Length > 0 && ReturnValue.UserGroupCD.ToUpper().Trim().Equals(txtbx.Text.Trim().ToUpper()))
                            {

                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Focus();
                                returnVal = true;
                            }

                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion

        protected void dgRWGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgRWGroup_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgRWGroup, Page.Session);
        }
    }
}
