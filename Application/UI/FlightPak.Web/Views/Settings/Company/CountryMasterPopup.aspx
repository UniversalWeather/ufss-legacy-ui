﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <title>Country</title>    
    <script type="text/javascript" src="/Scripts/Common.js"></script>
<link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
   <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
   <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>

    <script type="text/javascript">
        var selectedRowData = null;
        var jqgridTableId = '#gridCountry';
        var clientcodeId = null;
        var clientcd = null;
        var selectedRowMultipleCountry = "";
        var isopenlookup = false;
        var ismultiSelect = false;
        var codes = [];
        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            var selectedRowData = getQuerystring("CountryCD", "");
            selectedRowMultipleCountry = $.trim(decodeURI(getQuerystring("CountryCD", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultipleCountry);
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            if (ismultiSelect) {
                if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                    selectedRowData = jQuery("#hdnselectedfccd").value;
                }
                codes = selectedRowData.split(',');
            }

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultipleAirport.length > 0)) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData, 0);
                }
                else {
                    showMessageBox('Please select a country.', popupTitle);
                }
                return false;
            });

            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'countries';
                    postData.countrycd = function () { if (ismultiSelect && selectedRowData.length > 0) { return codes[0]; } else { return selectedRowData; } };
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 230,
                width: 600,
                viewrecords: true,
                rowNum: 500,
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                pgbuttons: false,
                pgtext: null,
                colNames: ['Country ID', 'Country Code', 'Description', 'ISO Code'],
                colModel: [
                    { name: 'CountryID', index: 'CountryID', key: true, hidden: true },
                    { name: 'CountryCD', index: 'CountryCD', width: 200 },
                    { name: 'CountryName', index: 'CountryName', width: 480 },
                { name: 'ISOCD', index: 'ISOCD', width: 200 }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData, 1);

                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    selectedRowMultipleCountry = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultipleCountry, rowData.CountryCD);
                },
                onSelectAll: function (id, status) {
                    selectedRowMultipleCountry = JqgridSelectAll(selectedRowMultipleCountry, jqgridTableId, id, status, 'CountryCD');
                    jQuery("#hdnselectedfccd").val(selectedRowMultipleCountry);
                },
                afterInsertRow: function (rowid, rowObject) {
                    JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultipleCountry, rowid, rowObject.CountryCD, jqgridTableId);
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                }
            });

            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

            $("#pagesizebox").insertBefore('.ui-paging-info');

        });


        function returnToParent(rowData, one) {
            selectedRowMultipleCountry = jQuery.trim(selectedRowMultipleCountry);
            if (selectedRowMultipleCountry.lastIndexOf(",") == selectedRowMultipleCountry.length - 1)
                selectedRowMultipleCountry = selectedRowMultipleCountry.substr(0, selectedRowMultipleCountry.length - 1);

            var oArg = new Object();
            if (one == 0) {
                oArg.CountryCD = selectedRowMultipleCountry;
            }
            else {
                oArg.CountryCD = rowData["CountryCD"];
            }

            oArg.CountryName = rowData["CountryName"];
            oArg.ISOCD = rowData["ISOCD"];
            oArg.CountryID = rowData["CountryID"];
            oArg.Arg1 = oArg.CountryCD;
            oArg.CallingButton = "CountryCD";

            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }

    </script>

</head>
<body>

    <form id="form1">
       
        <div class="jqgrid">
            <input type="hidden" id="hdnselectedfccd" value="" />
            <div>
                <table class="box1">
             
                    <tr>
                        <td>
                            <table id="gridCountry" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>                              
                               <div id="pagesizebox">
                                                      
                                </div> 
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </form>
    
</body>
</html>
