﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class TripManagerCheckListGroupCatalog : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private bool IsEmptyCheck = true;
        private bool ChecklistGroupPageNavigated = false;
        private string strChecklistGroupCD = "";
        private string strChecklistGroupID = "";
        private List<string> lstCheckGroupCodes = new List<string>();
        private bool _selectLastModified = false;

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        {
                            // Grid Control could be ajaxified when the page is initially loaded.
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgTripManagerCheckListGroup, dgTripManagerCheckListGroup, RadAjaxLoadingPanel1);
                            // Store the clientID of the grid to reference it later on the client
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgTripManagerCheckListGroup.ClientID));
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                            if (!IsPostBack)
                            {
                                CheckAutorization(Permission.Database.ViewTripManagerCheckListGroupCatalog);
                                // Method to display first record in read only format   
                                if (Session["SearchItemPrimaryKeyValue"] != null)
                                {
                                    dgTripManagerCheckListGroup.Rebind();
                                    ReadOnlyForm();
                                    DisableLinks();
                                    GridEnable(true, true, true);
                                    Session.Remove("SearchItemPrimaryKeyValue");
                                }
                                else
                                {
                                    chkSearchActiveOnly.Checked = true;
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }

        }

        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgTripManagerCheckListGroup.Rebind();
                    }
                    if (dgTripManagerCheckListGroup.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedChecklistGroupID"] = null;
                        //}
                        if (Session["SelectedChecklistGroupID"] == null)
                        {
                            dgTripManagerCheckListGroup.SelectedIndexes.Add(0);
                            Session["SelectedChecklistGroupID"] = dgTripManagerCheckListGroup.Items[0].GetDataKeyValue("CheckGroupID").ToString();
                        }

                        if (dgTripManagerCheckListGroup.SelectedIndexes.Count == 0)
                            dgTripManagerCheckListGroup.SelectedIndexes.Add(0);

                        ReadOnlyForm();                        
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedChecklistGroupID"] != null)
                    {
                        string ID = Session["SelectedChecklistGroupID"].ToString();
                        foreach (GridDataItem Item in dgTripManagerCheckListGroup.MasterTableView.Items)
                        {
                            if (Item["CheckGroupID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        //if (dgTripManagerCheckListGroup.SelectedItems.Count == 0)
                        //{
                        //    DefaultSelection(false);
                        //}
                    }
                    else
                    {
                        DefaultSelection(false);
                    }                    
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1))
                        {
                            e.Updated = dgTripManagerCheckListGroup;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }
        }

        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckListGroup_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton deleteButton = (e.Item as GridCommandItem).FindControl("lbtnDelete") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(deleteButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }

        }

        /// <summary>
        /// Bind Crew Check List Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckListGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            var CheckGroup = Service.GetTripManagerChecklistGroupList();
                            List<FlightPakMasterService.TripManagerCheckListGroup> lstTripManagerCheckListGroup = new List<FlightPakMasterService.TripManagerCheckListGroup>();
                            if (CheckGroup.ReturnFlag == true)
                            {
                               lstTripManagerCheckListGroup = CheckGroup.EntityList;
                            }
                            dgTripManagerCheckListGroup.DataSource = lstTripManagerCheckListGroup;
                            Session["TripManagerCheckListGroup"] = lstTripManagerCheckListGroup;

                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }

                            Session.Remove("CheckGroupCD");
                            lstCheckGroupCodes = new List<string>();
                            foreach (TripManagerCheckListGroup TripManagerCheckListGroupEntity in CheckGroup.EntityList)
                            {
                                lstCheckGroupCodes.Add(TripManagerCheckListGroupEntity.CheckGroupCD.ToLower().Trim());
                            }
                            Session["CheckGroupCD"] = lstCheckGroupCodes;
                            
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                    }
                }
            }
        }

        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = "Save";
                        ClearForm();
                        EnableForm(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }
        }

        /// <summary>
        /// Item Command for Crew Check List Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckListGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                if (Session["SelectedChecklistGroupID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.TripManagerCheckListGroup, Convert.ToInt64(Session["SelectedChecklistGroupID"].ToString().Trim()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.TripManagerCheckListGroup);
                                            SelectItem();
                                            return;
                                        }
                                        SelectItem();
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                dgTripManagerCheckListGroup.SelectedIndexes.Clear();
                                tbCode.Focus();
                                DisableLinks();
                                break;

                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }
        }

        /// <summary>
        /// Update Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckListGroup_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Session["SelectedChecklistGroupID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient TripChecklistService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal=TripChecklistService.UpdateTripManagerChecklistGroup(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.TripManagerCheckListGroup, Convert.ToInt64(Session["SelectedChecklistGroupID"].ToString().Trim()));

                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        SelectItem();
                                        ReadOnlyForm();
                                    }
                                   
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.TripManagerCheckListGroup);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }
        }

        /// <summary>
        /// To check whether the Already the Passenger exists
        /// </summary>
        /// <returns></returns>
        private bool checkAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objTripsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objTripsvc.GetTripManagerChecklistGroupList().EntityList.Where(x => x.CheckGroupCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                            if (objRetVal.Count() > 0 && objRetVal != null)
                            {
                                cvCode.IsValid = false;
                                tbCode.Focus();
                                returnVal = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
                return returnVal;
            }
        }

        /// <summary>
        /// Update Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckListGroup_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (checkAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objService.AddTripManagerChecklistGroup(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    dgTripManagerCheckListGroup.Rebind();
                                    GridEnable(true, true, true);
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.TripManagerCheckListGroup);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }

        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckListGroup_DeleteCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["SelectedChecklistGroupID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient TripChecklistService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.TripManagerCheckListGroup TripChecklist = new FlightPakMasterService.TripManagerCheckListGroup();
                                    string Code = Session["SelectedChecklistGroupID"].ToString();
                                    strChecklistGroupCD = "";
                                    strChecklistGroupID = "";
                                    foreach (GridDataItem Item in dgTripManagerCheckListGroup.MasterTableView.Items)
                                    {
                                        if (Item["CheckGroupID"].Text.Trim() == Code.Trim())
                                        {
                                            strChecklistGroupCD = Item["CheckGroupCD"].Text.Trim();
                                            strChecklistGroupID = Item["CheckGroupID"].Text.Trim();
                                            TripChecklist.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                            break;
                                        }

                                    }
                                    TripChecklist.CheckGroupCD = strChecklistGroupCD;
                                    TripChecklist.CheckGroupID = Convert.ToInt64(strChecklistGroupID);
                                    TripChecklist.IsDeleted = true;
                                    var returnValue = CommonService.Lock(EntitySet.Database.TripManagerCheckListGroup, Convert.ToInt64(strChecklistGroupID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.TripManagerCheckListGroup);
                                        return;
                                    }
                                    TripChecklistService.DeleteTripManagerChecklistGroup(TripChecklist);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgTripManagerCheckListGroup.Rebind();
                                    DefaultSelection(false);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                    }
                    finally
                    {
                        // For Positive and negative cases need to unlock the record
                        // The same thing should be done for Edit
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.TripManagerCheckListGroup, Convert.ToInt64(strChecklistGroupID));
                    }
                }
            }

        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckListGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgTripManagerCheckListGroup.SelectedItems[0] as GridDataItem;
                                Session["SelectedChecklistGroupID"] = item["CheckGroupID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception.
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                    }
                }
            }
        }

        protected void TripManagerCheckListGroup_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgTripManagerCheckListGroup.ClientSettings.Scrolling.ScrollTop = "0";
            ChecklistGroupPageNavigated = true;
        }

        protected void TripManagerCheckListGroup_PreRender(object sender, EventArgs e)
        {
            //GridEnable(true, true, true);
            if (hdnSave.Value != "Save")
            {
                SelectItem();
            }
            else if (ChecklistGroupPageNavigated)
            {
                SelectItem();
            }
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgTripManagerCheckListGroup, Page.Session);
        }

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgTripManagerCheckListGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                SelectItem();
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                            else
                            {
                                (dgTripManagerCheckListGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                                GridEnable(true, true, true);
                            }                            
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedChecklistGroupID"] != null)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.TripManagerCheckListGroup, Convert.ToInt64(Session["SelectedChecklistGroupID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        private TripManagerCheckListGroup GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.TripManagerCheckListGroup TripManagerCheckListGroupType = null;

                try
                {
                    TripManagerCheckListGroupType = new FlightPakMasterService.TripManagerCheckListGroup();
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                            if (Session["SelectedChecklistGroupID"] != null)
                            {
                                TripManagerCheckListGroupType.CheckGroupID = Convert.ToInt64(Session["SelectedChecklistGroupID"].ToString().Trim());
                            }
                        }
                        else
                        {
                            TripManagerCheckListGroupType.CheckGroupID = 0;
                        }
                        TripManagerCheckListGroupType.CheckGroupCD = tbCode.Text.Trim();
                        TripManagerCheckListGroupType.CheckGroupDescription = tbDescription.Text.Trim();
                        TripManagerCheckListGroupType.IsDeleted = false;
                        TripManagerCheckListGroupType.IsInActive = chkInactive.Checked;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
                return TripManagerCheckListGroupType;
            }
        }

        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LoadControlData();
                        
                        hdnSave.Value = "Update";
                        hdnRedirect.Value = "";
                        EnableForm(true);
                        tbCode.Enabled = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.TripManagerCheckListGroup, Convert.ToInt64(strChecklistGroupID));
                    }
                }
            }
        }

        /// <summary>
        /// Grid enable event
        /// </summary>
        /// <param name="add"></param>
        /// <param name="edit"></param>
        /// <param name="delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgTripManagerCheckListGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgTripManagerCheckListGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgTripManagerCheckListGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        if (IsAuthorized(Permission.Database.AddTripManagerCheckListGroupCatalog))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                            {
                                lbtnInsertCtl.Enabled = true;
                            }
                            else
                            {
                                lbtnInsertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.DeleteTripManagerCheckListGroupCatalog))
                        {
                            lbtnDelCtl.Visible = true;
                            if (Delete)
                            {
                                lbtnDelCtl.Enabled = true;
                                lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                            }
                            else
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.EditTripManagerCheckListGroupCatalog))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                            {
                                lbtnEditCtl.Enabled = true;
                                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnEditCtl.Enabled = false;
                                lbtnEditCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }

        }

        /// <summary>
        /// To check unique Code  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbCode.Text))
                        {
                            if (checkAllReadyExist())
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }
        }

        /// <summary>
        /// Read only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTripManagerCheckListGroup.SelectedItems.Count == 0)
                        {
                            DefaultSelection(false);
                        }
                        if (dgTripManagerCheckListGroup.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgTripManagerCheckListGroup.SelectedItems[0] as GridDataItem;
                            Label lbLastUpdatedUser = (Label)dgTripManagerCheckListGroup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (Item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                            }
                            else
                            {
                                lbLastUpdatedUser.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                            }
                            LoadControlData();
                            EnableForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }
        }

        private void LoadControlData()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedChecklistGroupID"] != null)
                    {
                        strChecklistGroupCD = "";
                        strChecklistGroupID = "";
                        string strCheckGroupName = "";
                        foreach (GridDataItem Item in dgTripManagerCheckListGroup.MasterTableView.Items)
                        {
                            if (Item["CheckGroupID"].Text.Trim() == Session["SelectedChecklistGroupID"].ToString().Trim())
                            {
                                strChecklistGroupCD = Item["CheckGroupCD"].Text.Trim();
                                strChecklistGroupID = Item["CheckGroupID"].Text.Trim();
                                strCheckGroupName = Item.GetDataKeyValue("CheckGroupDescription").ToString().Trim();

                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }

                                lbColumnName1.Text = "Checklist Group Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["CheckGroupCD"].Text;
                                lbColumnValue2.Text = Item["CheckGroupDescription"].Text;
                                break;
                            }
                        }
                        tbCode.Text = strChecklistGroupCD;
                        if (strCheckGroupName != null)
                        {
                            tbDescription.Text = strCheckGroupName;
                        }
                        else
                        {
                            tbDescription.Text = string.Empty;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }

        /// <summary>
        /// Clear form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Enable form
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                       
                        tbCode.Enabled = enable;
                        tbDescription.Enabled = enable;
                        btnCancel.Visible = enable;
                        btnSaveChanges.Visible = enable;                        
                        chkInactive.Enabled = enable; 
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckListGroup);
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.TripManagerCheckListGroup> lstTripManagerCheckListGroup = new List<FlightPakMasterService.TripManagerCheckListGroup>();
                if (Session["TripManagerCheckListGroup"] != null)
                {
                    lstTripManagerCheckListGroup = (List<FlightPakMasterService.TripManagerCheckListGroup>)Session["TripManagerCheckListGroup"];
                }
                if (lstTripManagerCheckListGroup.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstTripManagerCheckListGroup = lstTripManagerCheckListGroup.Where(x => x.IsInActive == false).ToList<TripManagerCheckListGroup>(); }
                    dgTripManagerCheckListGroup.DataSource = lstTripManagerCheckListGroup;
                    if (IsDataBind)
                    {
                        dgTripManagerCheckListGroup.DataBind();
                        GridEnable(true, true, true);
                    }
                }
                //SelectItem();
                //ReadOnlyForm();                
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgTripManagerCheckListGroup.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var ChecklistGroupValue = FPKMstService.GetTripManagerChecklistGroupList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, ChecklistGroupValue);
            List<FlightPakMasterService.TripManagerCheckListGroup> filteredList = GetFilteredList(ChecklistGroupValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CheckGroupID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgTripManagerCheckListGroup.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgTripManagerCheckListGroup.CurrentPageIndex = PageNumber;
            dgTripManagerCheckListGroup.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfTripManagerCheckListGroup ChecklistGroupValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedChecklistGroupID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = ChecklistGroupValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().CheckGroupID;
                Session["SelectedChecklistGroupID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.TripManagerCheckListGroup> GetFilteredList(ReturnValueOfTripManagerCheckListGroup ChecklistGroupValue)
        {
            List<FlightPakMasterService.TripManagerCheckListGroup> filteredList = new List<FlightPakMasterService.TripManagerCheckListGroup>();

            if (ChecklistGroupValue.ReturnFlag)
            {
                filteredList = ChecklistGroupValue.EntityList.Where(x => x.IsDeleted == false).ToList();
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<TripManagerCheckListGroup>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgTripManagerCheckListGroup.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgTripManagerCheckListGroup.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}


