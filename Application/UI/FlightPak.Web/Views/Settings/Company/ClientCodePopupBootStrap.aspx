﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><%@ Import Namespace="System.Web.Optimization" %>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head runat="server">
    <title>Client Code</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
   <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
     <script type="text/javascript" src="/Scripts/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript">
        var selectedRowData = null;
        var jqgridTableId = '#gridClientCode';
        var clientcodeId = null;
        var clientcd = null;
        var clientSelectedCd = "";
        var isopenlookup = false;
        $(document).ready(function () {

            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });
            clientSelectedCd = $.trim(unescape(getQuerystring("ClientCD", "")));
            if (clientSelectedCd != "") {
                isopenlookup = true;
            }
                
            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                if (rowData["ClientCD"] == undefined) {
                    BootboxAlert("Please select a record.");
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();               
                popupwindow("/Views/Settings/Company/ClientCodeCatalog.aspx?IsPopup=Add", popupTitle, 1100, 798, jqgridTableId);
                return false;
            });


            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                clientcodeId = rowData['ClientId'];
                clientcd = rowData['ClientCD'];
                var widthDoc = $(document).width();
                if (clientcodeId != undefined && clientcodeId != null && clientcodeId != '' && clientcodeId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a record.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        dataType: 'json',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=ClientCode&clientcodeId=' + clientcodeId,
                        contentType: 'application/json',
                        success: function (data) {
                            verifyReturnedResultForJqgrid(data);
                            $(jqgridTableId).trigger('reloadGrid');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            var msg = "This ClientCode (" + clientcd + ")";
                            msg = msg.concat(" does not exist.");
                            if (content.indexOf('404'))
                                showMessageBox(msg, popupTitle);
                        }
                    });

                }
            }
            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                clientcodeId = rowData['ClientId'];
                var widthDoc = $(document).width();
                if (clientcodeId != undefined && clientcodeId != null && clientcodeId != '' && clientcodeId != 0) {
                    popupwindow("/Views/Settings/Company/ClientCodeCatalog.aspx?IsPopup=&ClientCodeId=" + clientcodeId, popupTitle, 1100, 798, jqgridTableId);
                }
                else {
                    showMessageBox('Please select a client code.', popupTitle);
                }
                return false;
            });

            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");                   
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'clientcode';
                    postData.showInactive = $('#chkSearchActiveOnly').is(':checked') ? false : true;
                    postData.clientCd = clientSelectedCd;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 230,
                width: 600,                
                viewrecords: true,
                rowNum: $("#rowNum").val(),              
                multiselect: false,
                pager: "#pg_gridPager_clientCode",
                colNames: ['ClientId', 'Code', 'Description'],
                colModel: [
                    { name: 'ClientId', index: 'ClientID', key: true, hidden: true },
                    { name: 'ClientCD', index: 'ClientCD', width: 50 },
                    { name: 'ClientDescription', index: 'ClientDescription', width: 300 }

                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);

                },
                onSelectRow: function (id) {
                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['ClientId'];//replace name with any column
                    clientSelectedCd = $.trim(rowData['ClientCD']);

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $(jqgridTableId).jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                loadComplete: function () {
                    setScrollClientCode();
                },
                afterInsertRow: function (rowid, rowObject) {
                    var rowData = $(jqgridTableId).jqGrid("getRowData", rowid);
                    var lastSel = rowData['ClientCD'];//replace name with any column                    
                    
                    if ($.trim(clientSelectedCd) == $.trim(lastSel)) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        $(jqgridTableId).jqGrid('setSelection', rowid);
                    }
                }
            });
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
            $("#pg_gridPager_clientCode_left").html($("#pagesizebox"));
        });
        $(window).resize(function () {
            setScrollClientCode();
        });
        function reloadClientCode() {
            //Reset the scroll
            $("#gbox_gridClientCode").find(".jspPane").css("top", 0);
            $(jqgridTableId).jqGrid().trigger('reloadGrid');
        }
        function reloadPageSize() {
            var myGrid = $(jqgridTableId);
            var currentValue = $("#rowNum").val();
            myGrid.setGridParam({ rowNum: currentValue });
            myGrid.trigger('reloadGrid');
        }
        function returnToParent(rowData) {
            var parentPageName = getQuerystring("ParentPage", "");
            self.parent.SetHtmlControlValue(parentPageName, rowData["ClientCD"]);
            self.parent.ManuallyCloseBootstrapPopup("#ModalClientCode");
        }
        function setScrollClientCode() {
            var table_header = $('#gbox_gridClientCode').find('.ui-jqgrid-hbox').css("position", "relative");
            $('#gbox_gridClientCode').find('.ui-jqgrid-bdiv').bind('jsp-scroll-x', function (event, scrollPositionX, isAtLeft, isAtRight) {
                table_header.css('right', scrollPositionX);
            }).jScrollPane({
                scrollbarWidth: 15,
                scrollbarMargin: 0
            });
        }
    </script>
    <style type="text/css">
        body {
            background: none !important;
        }
    </style>
</head>
<body>
    <form id="form1">
        <%=Scripts.Render("~/bundles/BootStrapScriptBundle") %>
        <%=Styles.Render("~/bundles/BootStrapPopupCssBundle") %>
       <div class="bootstrap-common clientcode_popup">
        <div class="jqgrid row">
            <div class="col-md-12">
                <table class="box1">
                      <tr>
                        <td>
                            <div class="header_inn_wrapper">
                                <div class="header_wrapper_left">
                                    <ul>
                                        <li>
                                            <input type="checkbox" name="chkSearchActiveOnly" id="chkSearchActiveOnly" onchange="reloadClientCode()"/>
                                            <label class="left_label" for="chkSearchActiveOnly">Active Only</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridClientCode" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager_clientCode" class="footer_type2"></div>                              
                               <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                </div> 
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        </div>
    </form>
</body>
</html>
