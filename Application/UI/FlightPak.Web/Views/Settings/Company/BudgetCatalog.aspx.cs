﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class BudgetCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        private string AcctNum = string.Empty;
        private string FiscalYear = string.Empty;
        private bool BudgetPageNavigated = false;
        private string BudgetID = string.Empty;
        private bool IsAcctnum = false;
        private string HomeBase = string.Empty;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgBudgetCatalog, dgBudgetCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgBudgetCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewBudgetCatalog);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {                                
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                                {
                                    chkHomeBaseOnly.Checked = true;
                                }
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                            if (Request.QueryString["Record"] == "true") { ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowSuccessMessage", "setTimeout(function(){  ShowSuccessMessage(); }, 500);", true); }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// To display the first record as default
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Request.QueryString["AccountID"] != null)
                    {
                        Session["AccountNumber"] = Request.QueryString["AccountID"];
                        divNavigation.Style.Add("display", "block");
                        tableBudget.Visible = false;
                        if (Session["AccountNumber"] != null)
                        {
                            tbSelectedAcctNumber.Text = Request.QueryString["AcctNO"].ToString();
                            tbSelectedAcctNumber.Enabled = false;
                            dgBudgetCatalog.Rebind();
                            SelectedItem();
                            if (IsAcctnum)
                            {
                                ReadOnlyForm();
                            }
                            EnableForm(false);
                            GridEnable(true, true, true);
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                            tbYear.Text = System.DateTime.Now.Year.ToString();
                        }
                    }
                    else
                    {
                        Session["AccountNumber"] = string.Empty;
                        tblAccount.Visible = false;

                        if (BindDataSwitch)
                        {
                            dgBudgetCatalog.Rebind();
                        }
                        if (dgBudgetCatalog.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedBudgetID"] = null;
                            //}
                            if (Session["SelectedBudgetID"] == null)
                            {
                                dgBudgetCatalog.SelectedIndexes.Add(0);
                                Session["SelectedBudgetID"] = dgBudgetCatalog.Items[0].GetDataKeyValue("BudgetID").ToString();
                            }

                            if (dgBudgetCatalog.SelectedIndexes.Count == 0)
                                dgBudgetCatalog.SelectedIndexes.Add(0);

                            ReadOnlyForm();
                            GridEnable(true, true, true);
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                            tbYear.Text = System.DateTime.Now.Year.ToString();
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the selected item which they selected during navigating to transport,hotel,catering
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedBudgetID"] != null)
                        {
                            string ID = Session["SelectedBudgetID"].ToString();
                            foreach (GridDataItem item in dgBudgetCatalog.MasterTableView.Items)
                            {
                                if (item.GetDataKeyValue("BudgetID").ToString().Trim() == ID)
                                {
                                    item.Selected = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            DefaultSelection(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// To select the item based on selected budget id
        /// </summary>
        private void SelectedItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["AccountNumber"] != null)
                        {
                            string ID = Session["AccountNumber"].ToString().Trim();
                            string BudgetId = Session["SelectedBudgetID"] == null ? "" : Session["SelectedBudgetID"].ToString().Trim();
                            foreach (GridDataItem item in dgBudgetCatalog.MasterTableView.Items)
                            {
                                if (item.GetDataKeyValue("AccountID").ToString().Trim() == ID && (BudgetId == "" || item.GetDataKeyValue("BudgetID").ToString().Trim() == BudgetId))
                                {
                                    item.Selected = true;
                                    Session["SelectedBudgetID"] = item.GetDataKeyValue("BudgetID").ToString();
                                    IsAcctnum = true;
                                    break;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// Bind Budget Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgBudgetCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objService.GetBudgetListInfo();
                                List<FlightPakMasterService.GetAllBudget> lstBudget = new List<FlightPakMasterService.GetAllBudget>();
                                if (objRetVal.ReturnFlag == true)
                                {
                                    lstBudget = objRetVal.EntityList.ToList();
                                }
                                dgBudgetCatalog.DataSource = lstBudget;
                                if (!string.IsNullOrEmpty(Session["AccountNumber"].ToString()))
                                {
                                    dgBudgetCatalog.DataSource = lstBudget.Where(x => x.AccountID.ToString().ToUpper().Trim().Equals(Session["AccountNumber"].ToString().ToUpper().Trim()));
                                }
                                Session["BudgetVal"] = lstBudget;
                                if ((chkSearchActiveOnly.Checked == true) || (chkHomeBaseOnly.Checked == true))
                                {
                                    SearchAndFilter(false);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                    }
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgBudgetCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// To check the unique code of Account numer and year
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private bool CheckAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string ID = Session["SelectedBudgetID"].ToString();
                        List<GetAllBudget> lstBudget = (List<GetAllBudget>)Session["BudgetVal"];
                        FlightPakMasterService.Budget budgetObject = new FlightPakMasterService.Budget();
                        var results = (from a in lstBudget
                                       where a.AccountNum.ToString().ToUpper().Trim().Equals(tbAcctNmbr.Text.ToString().ToUpper().Trim()) && (a.FiscalYear.ToString().ToUpper().Equals(tbYear.Text.ToString().ToUpper()))
                                       select a);
                        if (results.Count() > 0 && results != null)
                            RetVal = true;
                        else
                            RetVal = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
                return RetVal;
            }
        }
        /// <summary>
        /// Item Command for Budget Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgBudgetCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                if (Session["SelectedBudgetID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Budget, Convert.ToInt64(Session["SelectedBudgetID"].ToString().Trim()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Budget);
                                            SelectItem();
                                            return;
                                        }
                                        SelectItem();
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNmbr);
                                        //tbTailNmbr.Focus(); 
                                       
                                    }
                                }
                                DisableLinks();
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                tbAcctNmbr.ReadOnly = false;
                                tbAcctNmbr.BackColor = System.Drawing.Color.White;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                dgBudgetCatalog.SelectedIndexes.Clear();
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbAcctNmbr);
                                //tbAcctNmbr.Focus();
                                DisableLinks();
                                break;
                            case "UpdateEdited":
                                dgBudgetCatalog_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkHomeBaseOnly.Enabled = false;
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkHomeBaseOnly.Enabled = true;
            chkSearchActiveOnly.Enabled = true;
        }
        /// <summary>
        /// Update Command for Budget Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgBudgetCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAccountNumber())
                        {
                            cvAcctNmbr.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAcctNmbr);
                            //tbAcctNmbr.Focus();
                            IsValidate = false;
                        }
                        if (CheckFleet())
                        {
                            cvTailNmbr.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNmbr);
                            //tbTailNmbr.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            if (Session["SelectedBudgetID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient BudgetService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = BudgetService.UpdateBudget(GetItems());
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.Budget, Convert.ToInt64(Session["SelectedBudgetID"].ToString().Trim()));
                                                                                     
                                            GridEnable(true, true, true);                                           
                                            SelectItem();
                                            ReadOnlyForm();
                                        }                                        
                                        if (objRetVal.ReturnFlag == true)
                                        {
                                            ShowSuccessMessage();
                                            dgBudgetCatalog.Rebind();
                                            var Budget = GetItems();
                                            var accountId = Budget.AccountID;
                                            var accountNum = Budget.AccountNum;
                                            Response.Redirect("../Company/BudgetCatalog.aspx?Screen=Budget&AccountID=" + accountId + "&AcctNO=" + accountNum + "&Record=true");
                                        }
                                        EnableLinks();                                        
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.Budget);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// Update Command for budget Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgBudgetCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAllReadyExist())
                        {
                            //tbYear.Focus();
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbYear);
                            IsValidate = false;
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Account and year must be unique', 360, 50, 'Budget Catalog');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        if (CheckAccountNumber())
                        {
                            cvAcctNmbr.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAcctNmbr);
                            //tbAcctNmbr.Focus();
                            IsValidate = false;
                        }
                        if (CheckFleet())
                        {
                            cvTailNmbr.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNmbr);
                            //tbTailNmbr.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient BudgetService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = BudgetService.AddBudget(GetItems());
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    dgBudgetCatalog.Rebind();
                                    GridEnable(true, true, true);
                                    DefaultSelection(false);
                                    EnableLinks();
                                    _selectLastModified = true;
                                    var Budget = GetItems();
                                    var accountId = Budget.AccountID;
                                    var accountNum=Budget.AccountNum;
                                    Response.Redirect("../Company/BudgetCatalog.aspx?Screen=Budget&AccountID=" + accountId + "&AcctNO=" + accountNum+"&Record=true");
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.Budget);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgBudgetCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["SelectedBudgetID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient BudgetService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.Budget Budget = new FlightPakMasterService.Budget();
                                    string Code = Session["SelectedBudgetID"].ToString();
                                    AcctNum = "";
                                    FiscalYear = "";
                                    foreach (GridDataItem Item in dgBudgetCatalog.MasterTableView.Items)
                                    {
                                        if (Item.GetDataKeyValue("BudgetID").ToString() == Code.Trim())
                                        {
                                            AcctNum = Item.GetDataKeyValue("AccountNum").ToString();
                                            BudgetID = Item.GetDataKeyValue("BudgetID").ToString();
                                            break;
                                        }
                                    }
                                    Budget.AccountNum = AcctNum;
                                    Budget.BudgetID = Convert.ToInt64(BudgetID);
                                    Budget.IsDeleted = true;
                                    var returnValue = CommonService.Lock(EntitySet.Database.Budget, Convert.ToInt64(BudgetID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Budget);
                                        return;
                                    }
                                    BudgetService.DeleteBudget(Budget);
                                    var returnValue1 = CommonService.UnLock(EntitySet.Database.Budget, Convert.ToInt64(BudgetID));
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgBudgetCatalog.Rebind();
                                    DefaultSelection(false);
                                    GridEnable(true, true, true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                    }
                    finally
                    {
                        // For Positive and negative cases need to unlock the record
                        // The same thing should be done for Edit
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.PaymentType, Convert.ToInt64(BudgetID));
                    }
                }
            }
        }
        /// <summary>
        /// Select and display the Item based on the index 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgBudgetCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgBudgetCatalog.SelectedItems[0] as GridDataItem;
                                Session["SelectedBudgetID"] = item.GetDataKeyValue("BudgetID").ToString();
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                    }
                }
            }
        }
        /// <summary>
        /// Page index change
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void Budget_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgBudgetCatalog.ClientSettings.Scrolling.ScrollTop = "0";
            BudgetPageNavigated = true;
        }
        /// <summary>
        /// PreRender method to select item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Budget_PreRender(object sender, EventArgs e)
        {
            //GridEnable(true, true, true);
            if (hdnSave.Value != "Save")
            {
                SelectItem();
            }
            else if (BudgetPageNavigated)
            {
                SelectItem();
            }
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgBudgetCatalog, Page.Session);
        }
        /// <summary>
        /// To check unique Account number on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Acctnum_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAccountNumber();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// To check unique Account number
        /// </summary>
        /// <returns></returns>
        private bool CheckAccountNumber()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool Retval = false;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAcctNmbr.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objBudgetsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objBudgetsvc.GetAllAccountList().EntityList.Where(x => x.AccountNum.ToString().ToUpper().Trim().Equals(tbAcctNmbr.Text.ToString().ToUpper().Trim())).ToList();
                                if (objRetVal.Count() == 0 && objRetVal != null)
                                {
                                    cvAcctNmbr.IsValid = false;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNmbr);
                                    //tbTailNmbr.Focus();
                                    Retval = true;
                                }
                                else
                                {
                                    List<FlightPakMasterService.GetAccounts> AccountList = new List<FlightPakMasterService.GetAccounts>();
                                    AccountList = (List<FlightPakMasterService.GetAccounts>)objRetVal.ToList();
                                    hdnAccountNum.Value = AccountList[0].AccountID.ToString();
                                    if (!string.IsNullOrEmpty(AccountList[0].AccountDescription))
                                        tbDescription.Text = AccountList[0].AccountDescription.Trim();
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbYear);
                                    //tbYear.Focus();
                                    Retval = false;
                                }
                            }
                        }
                        else
                        {
                            Retval = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
                return Retval;
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = "Save";
                        ClearForm();
                        EnableForm(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Budget Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgBudgetCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgBudgetCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    
                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// Cancel Budget Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedBudgetID"] != null)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Budget, Convert.ToInt64(Session["SelectedBudgetID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
            
            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgBudgetCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private Budget GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Budget Budget = null;
                try
                {
                    Budget = new FlightPakMasterService.Budget();
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAcctNmbr.Text.Trim()))
                        {
                            Budget.AccountID = Convert.ToInt64(hdnAccountNum.Value);
                            Budget.AccountNum = tbAcctNmbr.Text;
                        }
                        if (hdnSave.Value == "Update")
                        {
                            Budget.BudgetID = Convert.ToInt64(Session["SelectedBudgetID"].ToString().Trim());
                        }
                        else
                        {
                            Budget.BudgetID = 0;
                        }
                        if (!string.IsNullOrEmpty(hdnFleetID.Value))
                        {
                            Budget.FleetID = Convert.ToInt64(hdnFleetID.Value);
                        }
                        if (!string.IsNullOrEmpty(tbYear.Text.Trim()))
                        {
                            Budget.FiscalYear = tbYear.Text;
                        }
                        if (tbMonth1Budget.Text != string.Empty)
                        {
                            Budget.Month1Budget = Convert.ToDecimal(tbMonth1Budget.Text);
                        }
                        if (tbMonth2Budget.Text != string.Empty)
                        {
                            Budget.Month2Budget = Convert.ToDecimal(tbMonth2Budget.Text);
                        }
                        if (tbMonth3Budget.Text != string.Empty)
                        {
                            Budget.Month3Budget = Convert.ToDecimal(tbMonth3Budget.Text);
                        }
                        if (tbMonth4Budget.Text != string.Empty)
                        {
                            Budget.Month4Budget = Convert.ToDecimal(tbMonth4Budget.Text);
                        }
                        if (tbMonth5Budget.Text != string.Empty)
                        {
                            Budget.Month5Budget = Convert.ToDecimal(tbMonth5Budget.Text);
                        }
                        if (tbMonth6Budget.Text != string.Empty)
                        {
                            Budget.Month6Budget = Convert.ToDecimal(tbMonth6Budget.Text);
                        }
                        if (tbMonth7Budget.Text != string.Empty)
                        {
                            Budget.Month7Budget = Convert.ToDecimal(tbMonth7Budget.Text);
                        }
                        if (tbMonth8Budget.Text != string.Empty)
                        {
                            Budget.Month8Budget = Convert.ToDecimal(tbMonth8Budget.Text);
                        }
                        if (tbMonth9Budget.Text != string.Empty)
                        {
                            Budget.Month9Budget = Convert.ToDecimal(tbMonth9Budget.Text);
                        }
                        if (tbMonth10Budget.Text != string.Empty)
                        {
                            Budget.Month10Budget = Convert.ToDecimal(tbMonth10Budget.Text);
                        }
                        if (tbMonth11Budget.Text != string.Empty)
                        {
                            Budget.Month11Budget = Convert.ToDecimal(tbMonth11Budget.Text);
                        }
                        if (tbMonth12Budget.Text != string.Empty)
                        {
                            Budget.Month12Budget = Convert.ToDecimal(tbMonth12Budget.Text);
                        }
                        Budget.IsInActive = chkInactive.Checked;
                        Budget.IsDeleted = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
                return Budget;
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LoadControlData();
                        hdnSave.Value = "Update";
                        hdnRedirect.Value = "";
                        EnableForm(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// To assign the data to controls 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedBudgetID"] != null)
                        {
                            AcctNum = "";
                            FiscalYear = "";
                            foreach (GridDataItem Item in dgBudgetCatalog.MasterTableView.Items)
                            {
                                if (Item["BudgetID"].Text.Trim() == Session["SelectedBudgetID"].ToString().Trim())
                                {
                                    if (Item.GetDataKeyValue("AccountNum") != null)
                                    {
                                        AcctNum = Item.GetDataKeyValue("AccountNum").ToString();
                                        tbAcctNmbr.Text = AcctNum;
                                    }
                                    if (Item.GetDataKeyValue("BudgetID") != null)
                                        BudgetID = Item.GetDataKeyValue("BudgetID").ToString();
                                    if (Item.GetDataKeyValue("FiscalYear") != null)
                                    {
                                        tbYear.Text = Item.GetDataKeyValue("FiscalYear").ToString();
                                    }
                                    else
                                    {
                                        tbYear.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("AccountDescription") != null)
                                    {
                                        tbDescription.Text = Item.GetDataKeyValue("AccountDescription").ToString();
                                    }
                                    else
                                    {
                                        tbDescription.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("TailNumber") != null)
                                    {
                                        tbTailNmbr.Text = Item.GetDataKeyValue("TailNumber").ToString();
                                    }
                                    else
                                    {
                                        tbTailNmbr.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("AccountID") != null)
                                    {
                                        hdnAccountNum.Value = Item.GetDataKeyValue("AccountID").ToString();
                                    }
                                    else
                                    {
                                        hdnAccountNum.Value = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month1Budget") != null)
                                    {
                                        tbMonth1Budget.Text = Item.GetDataKeyValue("Month1Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth1Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month2Budget") != null)
                                    {
                                        tbMonth2Budget.Text = Item.GetDataKeyValue("Month2Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth2Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month3Budget") != null)
                                    {
                                        tbMonth3Budget.Text = Item.GetDataKeyValue("Month3Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth3Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month4Budget") != null)
                                    {
                                        tbMonth4Budget.Text = Item.GetDataKeyValue("Month4Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth4Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month5Budget") != null)
                                    {
                                        tbMonth5Budget.Text = Item.GetDataKeyValue("Month5Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth5Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month6Budget") != null)
                                    {
                                        tbMonth6Budget.Text = Item.GetDataKeyValue("Month6Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth6Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month7Budget") != null)
                                    {
                                        tbMonth7Budget.Text = Item.GetDataKeyValue("Month7Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth7Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month8Budget") != null)
                                    {
                                        tbMonth8Budget.Text = Item.GetDataKeyValue("Month8Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth8Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month9Budget") != null)
                                    {
                                        tbMonth9Budget.Text = Item.GetDataKeyValue("Month9Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth9Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month10Budget") != null)
                                    {
                                        tbMonth10Budget.Text = Item.GetDataKeyValue("Month10Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth10Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month11Budget") != null)
                                    {
                                        tbMonth11Budget.Text = Item.GetDataKeyValue("Month11Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth11Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Month12Budget") != null)
                                    {
                                        tbMonth12Budget.Text = Item.GetDataKeyValue("Month12Budget").ToString();
                                    }
                                    else
                                    {
                                        tbMonth12Budget.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("IsInActive") != null)
                                    {
                                        chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    else
                                    {
                                        chkInactive.Checked = false;
                                    }
                                    lbColumnName1.Text = "Account No.";
                                    lbColumnName2.Text = "Description";
                                    lbColumnValue1.Text = Item["AccountNum"].Text;
                                    lbColumnValue2.Text = Item["AccountDescription"].Text;
                                    break;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgBudgetCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgBudgetCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgBudgetCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        if (IsAuthorized(Permission.Database.AddBudgetCatalog))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                            {
                                lbtnInsertCtl.Enabled = true;
                            }
                            else
                            {
                                lbtnInsertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.DeleteBudgetCatalog))
                        {
                            lbtnDelCtl.Visible = true;
                            if (Delete)
                            {
                                lbtnDelCtl.Enabled = true;
                                lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                            }
                            else
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.EditBudgetCatalog))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                            {
                                lbtnEditCtl.Enabled = true;
                                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnEditCtl.Enabled = false;
                                lbtnEditCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        ///To make the controls read only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgBudgetCatalog.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgBudgetCatalog.SelectedItems[0] as GridDataItem;
                            Label lbLastUpdatedUser = (Label)dgBudgetCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (Item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                lbLastUpdatedUser.Text = HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                            }
                            else
                            {
                                lbLastUpdatedUser.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                lbLastUpdatedUser.Text = HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                            }
                            LoadControlData();
                            EnableForm(false);
                        }
                        else
                        {
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        ///To clear the conrols
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbAcctNmbr.Text = string.Empty;
                        tbDescription.Text = string.Empty;
                        tbYear.Text = DateTime.Now.Year.ToString();
                        tbTailNmbr.Text = string.Empty;
                        tbMonth1Budget.Text = "0.00";
                        tbMonth2Budget.Text = "0.00";
                        tbMonth3Budget.Text = "0.00";
                        tbMonth4Budget.Text = "0.00";
                        tbMonth5Budget.Text = "0.00";
                        tbMonth6Budget.Text = "0.00";
                        tbMonth7Budget.Text = "0.00";
                        tbMonth8Budget.Text = "0.00";
                        tbMonth9Budget.Text = "0.00";
                        tbMonth10Budget.Text = "0.00";
                        tbMonth11Budget.Text = "0.00";
                        tbMonth12Budget.Text = "0.00";
                        hdnFleetID.Value = string.Empty;
                        hdnAccountNum.Value = string.Empty;
                        Session["SelectedBudgetID"] = string.Empty;
                        chkInactive.Checked = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        
                        if (hdnSave.Value == "Update")
                        {
                            tbAcctNmbr.Enabled = enable; 
                            tbYear.Enabled = false;
                            btnAcctNumber.Enabled = enable;
                        }
                        else
                        {
                            tbAcctNmbr.Enabled = enable;
                            tbYear.Enabled = enable;
                            btnAcctNumber.Enabled = enable;
                        }
                        tbDescription.Enabled = false;
                        tbTailNmbr.Enabled = enable;
                        tbMonth1Budget.Enabled = enable;
                        tbMonth2Budget.Enabled = enable;
                        tbMonth3Budget.Enabled = enable;
                        tbMonth4Budget.Enabled = enable;
                        tbMonth5Budget.Enabled = enable;
                        tbMonth6Budget.Enabled = enable;
                        tbMonth7Budget.Enabled = enable;
                        tbMonth8Budget.Enabled = enable;
                        tbMonth9Budget.Enabled = enable;
                        tbMonth10Budget.Enabled = enable;
                        tbMonth11Budget.Enabled = enable;
                        tbMonth12Budget.Enabled = enable;
                        btnSaveChanges.Visible = enable;
                        btnCancel.Visible = enable;
                        btnTailNo.Enabled = enable;
                        chkInactive.Enabled = enable;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// To check unique Tail number on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbTailNmbr_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckFleet();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
            }
        }
        /// <summary>
        /// To check Unique Fleet Number
        /// </summary>
        /// <returns></returns>
        private bool CheckFleet()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool Retval = false;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbTailNmbr.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objFleetsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objFleetsvc.GetFleetProfileList().EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNmbr.Text.ToString().ToUpper().Trim())).ToList();
                                if (objRetVal.Count() == 0 || objRetVal == null)
                                {
                                    cvTailNmbr.IsValid = false;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNmbr);
                                    //tbTailNmbr.Focus();
                                    Retval = true;
                                }
                                else
                                {
                                    List<FlightPakMasterService.Fleet> fleetlist = new List<FlightPakMasterService.Fleet>();
                                    fleetlist = (List<FlightPakMasterService.Fleet>)objRetVal.ToList();
                                    hdnFleetID.Value = fleetlist[0].FleetID.ToString();
                                    tbTailNmbr.Text = fleetlist[0].TailNum;
                                    Retval = false;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbMonth1Budget);
                                    //tbMonth1Budget.Focus();
                                }
                            }
                        }
                        else
                        {
                            Retval = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Budget);
                }
                return Retval;
            }
        }
        /// <summary>
        /// To dispaly active and inactive records based on checkbox change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkHomeBaseOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        /// <summary>
        /// To filter based on home Base and Account Number
        /// </summary>
        /// <param name="IsDataBind"></param>
        /// <returns></returns>
        private void SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllBudget> lstBudget = new List<FlightPakMasterService.GetAllBudget>();
                if (Session["BudgetVal"] != null)
                {
                    lstBudget = (List<FlightPakMasterService.GetAllBudget>)Session["BudgetVal"];
                }
                if ((lstBudget.Count != 0) && (string.IsNullOrEmpty(Session["AccountNumber"].ToString())))
                {
                    if (chkSearchActiveOnly.Checked == true) { lstBudget = lstBudget.Where(x => x.IsInActive == false).ToList<GetAllBudget>(); }
                    if (chkHomeBaseOnly.Checked == true) { lstBudget = lstBudget.Where(x => x.HomebaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllBudget>(); }
                    dgBudgetCatalog.DataSource = lstBudget;
                    Session["SelectedBudgetID"] = lstBudget.Count() > 0 ? Convert.ToString(lstBudget.FirstOrDefault().BudgetID) : "";
                    if (IsDataBind)
                    {
                        dgBudgetCatalog.DataBind();
                    }
                }
                else if ((lstBudget.Count != 0) && (!string.IsNullOrEmpty(Session["AccountNumber"].ToString())))
                {
                    if (chkSearchActiveOnly.Checked == true) { lstBudget = lstBudget.Where(x => x.IsInActive == false).ToList<GetAllBudget>(); }
                    dgBudgetCatalog.DataSource = lstBudget.Where(x => x.AccountID.ToString().ToUpper().Trim().Equals(Session["AccountNumber"].ToString().ToUpper().Trim()));
                }
                //return false;
            }
        }
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgBudgetCatalog.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    chkInactive.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
                    int iIndex = 0;
                    int FullItemIndex = 0;

            var BudgetValue = FPKMstService.GetBudgetListInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, BudgetValue);
            List<FlightPakMasterService.GetAllBudget> filteredList = GetFilteredList(BudgetValue);

            foreach (var Item in filteredList)
                    {
                        if (PrimaryKeyValue == Item.BudgetID)
                        {
                            FullItemIndex = iIndex;
                            break;
                        }

                        iIndex++;
                    }

                    int PageSize = dgBudgetCatalog.PageSize;
                    int PageNumber = FullItemIndex / PageSize;
                    int ItemIndex = FullItemIndex % PageSize;

                    dgBudgetCatalog.CurrentPageIndex = PageNumber;
            dgBudgetCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllBudget BudgetValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedBudgetID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = BudgetValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().BudgetID;
                Session["SelectedBudgetID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllBudget> GetFilteredList(ReturnValueOfGetAllBudget BudgetValue)
        {
            List<FlightPakMasterService.GetAllBudget> filteredList = new List<FlightPakMasterService.GetAllBudget>();

            if (BudgetValue.ReturnFlag == true)
            {
                filteredList = BudgetValue.EntityList.Where(x => x.IsDeleted == false).ToList();
            }

            if ((chkSearchActiveOnly.Checked == true) || (chkHomeBaseOnly.Checked == true))
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetAllBudget>(); }
                    if (chkHomeBaseOnly.Checked == true) { filteredList = filteredList.Where(x => x.HomebaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllBudget>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgBudgetCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgBudgetCatalog.Rebind();
                    SelectItem();
                }
            }
        }
    }
}
