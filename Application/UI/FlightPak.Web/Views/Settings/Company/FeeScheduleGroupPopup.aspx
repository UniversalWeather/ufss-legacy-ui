﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeeScheduleGroupPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.FeeScheduleGroupPopup" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Fee Schedule Group</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
                var oArg = new Object();
                var grid = $find("<%= dgFeeScheduleGroup.ClientID %>");

                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }

                function returnToParent() {
                    //create the argument that will be returned to the parent page
                    oArg = new Object();
                    grid = $find("<%= dgFeeScheduleGroup.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var selectedRows = MasterTable.get_selectedItems();
                    var SelectedMain = false;
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "FeeGroupID");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "FeeGroupCD");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "FeeGroupDescription");
                    }

                    if (selectedRows.length > 0) {
                        oArg.FeeGroupID = cell1.innerHTML;
                        oArg.FeeGroupCD = cell2.innerHTML;
                        oArg.FeeGroupDescription = cell3.innerHTML;

                    }
                    else {
                        oArg.FeeGroupID = "";
                        oArg.FeeGroupCD = "";

                        oArg.FeeGroupDescription = "";
                    }


                    var oWnd = GetRadWindow();
                    if (oArg) {
                        oWnd.close(oArg);
                    }
                }

                function Close() {
                    GetRadWindow().Close();
                }

                function rebindgrid() {
                    var masterTable = $find("<%= dgFeeScheduleGroup.ClientID %>").get_masterTableView();
                    masterTable.rebind();
                    masterTable.clearSelectedItems();
                    masterTable.selectItem(masterTable.get_dataItems()[0].get_element());
                }
                function GetGridId() {
                    return $find("<%= dgFeeScheduleGroup.ClientID %>");
                }
                function prepareSearchInput(input) { input.value = input.value; }
            </script>
        </telerik:RadCodeBlock>
        <div class="divGridPanel">
            <asp:ScriptManager ID="scr1" runat="server">
            </asp:ScriptManager>
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="dgFeeScheduleGroup">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="dgFeeScheduleGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                    <telerik:AjaxSetting AjaxControlID="clrFilters">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="dgFeeScheduleGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                            <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
            <telerik:RadGrid ID="dgFeeScheduleGroup" runat="server" AllowMultiRowSelection="true"
                AllowSorting="true" AutoGenerateColumns="false" Height="341px" PageSize="10"
                OnItemCommand="dgFeeScheduleGroup_ItemCommand" OnDeleteCommand="dgFeeScheduleGroup_DeleteCommand"
                AllowPaging="true" Width="700px" AllowFilteringByColumn="true" OnNeedDataSource="dgFeeScheduleGroup_BindData"
                OnPreRender="dgFeeScheduleGroup_PreRender">
                <MasterTableView CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="FeeGroupCD" HeaderText="Code" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="100px"
                            FilterControlWidth="80px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FeeGroupDescription" HeaderText="Description"
                            CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                            HeaderStyle-Width="600px" FilterControlWidth="580px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FeeGroupID" HeaderText="Code" CurrentFilterFunction="EqualTo"
                            Display="false" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div class="grid_icon">
                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                CommandName="PerformInsert" Visible='<%# IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.AddFeeGroup)%>'></asp:LinkButton>
                            <%-- --%>
                            <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                CommandName="Edit" Visible='<%# IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.EditFeeGroup)%>'
                                OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                            <%-- --%>
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgFeeScheduleGroup', 'radFeeGroupPopup');"
                                CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.DeleteFeeGroup)%>'></asp:LinkButton>
                            <%-- CommandName="DeleteSelected" --%>
                        </div>
                        <div class="grd_ok">
                            <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                Ok</button>
                            <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                        </div>
                        <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                            visible="false">
                            Use CTRL key to multi select</div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings>
                    <ClientEvents OnRowDblClick="returnToParent" />
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
        </div>
    </div>
    </form>
</body>
</html>
