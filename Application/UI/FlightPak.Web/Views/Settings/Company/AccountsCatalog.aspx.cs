﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;
using System.Linq.Expressions;

namespace FlightPak.Web.Views.Settings.Company
{
    public partial class AccountsCatalog : BaseSecuredPage //System.Web.UI.Page
    {
        private ExceptionManager exManager;
        #region Page Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAccounts, dgAccounts, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgAccounts.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewAccountReport);

                        //To check the page level access.
                        CheckAutorization(Permission.Database.ViewAccounts);

                        if (!IsPostBack)
                        {
                            SetPageControls();
                            SetSelectionID();
                            AccountNumberFormat();
                            GetList(true);
                            dgAccounts.Rebind();
                            if (IsPopUp)
                                SetPopUpControls();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        #endregion Page Events

        private void SetPageControls()
        {
            if (Session["SearchItemPrimaryKeyValue"] != null)
            {
                chkSearchActiveOnly.Checked = false;
                //chkInactive.Checked = false;
            }
            else
            {
                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                {
                    chkSearchHomebaseOnly.Checked = true;
                }
                chkSearchActiveOnly.Checked = true;
            }

        }
        private void SetPopUpControls()
        {
            //Hide Controls
            dgAccounts.Visible = false;
            //dgAccounts.AllowPaging = false;
            chkSearchHomebaseOnly.Visible = false;
            chkSearchActiveOnly.Visible = false;
            lnkBudget.Visible = false;
            lbtnReports.Visible = false;
            lbtnSaveReports.Visible = false;
            btnShowReports.Visible = false;
            // Show Insert Form
            if (IsAdd)
            {
                (dgAccounts.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
            }
            else
            {
                (dgAccounts.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
            }
        }
        private void SetSelectionID()
        {
            if (Session["SearchItemPrimaryKeyValue"] != null)
            {
                Session["AccountID"] = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                Session.Remove("SearchItemPrimaryKeyValue");
            }
            //if (_selectLastModified)
            //{
            //    Session["AccountID"] = ((List<FlightPakMasterService.GetAccounts>)dgAccounts.DataSource).OrderByDescending(x => x.LastUpdTS).FirstOrDefault().AccountID;
            //}
            if (!string.IsNullOrWhiteSpace(Request.QueryString["AccountID"]))
            {
                Session["AccountID"] = Convert.ToInt64(Request.QueryString["AccountID"]);
            }
        }
        private List<FlightPakMasterService.GetAccounts> GetSortedList()
        {
            List<FlightPakMasterService.GetAccounts> accounts = GetList();

            if (dgAccounts.MasterTableView.SortExpressions.Count > 0)
            {

                var param = Expression.Parameter(typeof(FlightPakMasterService.GetAccounts), "Account");
                var mySortExpression = Expression.Lambda<Func<FlightPakMasterService.GetAccounts, object>>(Expression.Property(param, dgAccounts.MasterTableView.SortExpressions[0].FieldName), param);

                if (dgAccounts.MasterTableView.SortExpressions[0].SortOrder == GridSortOrder.Ascending)
                    accounts = accounts.AsQueryable().OrderBy(mySortExpression).ToList();
                else
                    accounts = accounts.AsQueryable().OrderByDescending(mySortExpression).ToList();
 
            }

            var filterExpression = dgAccounts.MasterTableView.FilterExpression;

            if (!string.IsNullOrEmpty(filterExpression))
            {
                accounts = null;
            }

            return accounts;    
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods through exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["AccountID"] != null && Session["AccountID"] != string.Empty)
                    {
                        string ID = Session["AccountID"].ToString();
                        int iIndex = -1;

                        List<FlightPakMasterService.GetAccounts> accounts = GetSortedList();

                        if (accounts != null)
                        {
                            FlightPakMasterService.GetAccounts account = accounts.Where(x => x.AccountID == long.Parse(ID)).FirstOrDefault<FlightPakMasterService.GetAccounts>();
                            iIndex = accounts.IndexOf(account);
                        }

                        if (iIndex == -1)
                            iIndex = 0;

                        int PageNumber = iIndex / dgAccounts.PageSize;
                        int ItemIndex = iIndex % dgAccounts.PageSize;

                        if (dgAccounts.CurrentPageIndex != PageNumber)
                        {
                            dgAccounts.CurrentPageIndex = PageNumber;
                            dgAccounts.Rebind();
                        }
                        if (ItemIndex!=0)
                            dgAccounts.SelectedIndexes.Add(ItemIndex);
                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        if (dgAccounts.MasterTableView.Items.Count > 0)
                        {
                            dgAccounts.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                                Session["AccountID"] = dgAccounts.Items[0].GetDataKeyValue("AccountID").ToString();

                            ReadOnlyForm();
                            GridEnable(true, true, true);
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, editCtl, delCtl;
                    insertCtl = (LinkButton)dgAccounts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    delCtl = (LinkButton)dgAccounts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    editCtl = (LinkButton)dgAccounts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddAccounts))
                    {
                        insertCtl.Visible = true;
                        if (add)
                        {
                            insertCtl.Enabled = true;
                        }
                        else
                        {
                            insertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteAccounts))
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditAccounts))
                    {
                        editCtl.Visible = true;
                        if (edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //tbAccountNumber.ReadOnly = false;
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Edit Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods through exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //tbAccountNumber.ReadOnly = true;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    if (IsPopUp)
                    {
                        dgAccounts.AllowPaging = false;
                        dgAccounts.Rebind();
                    }
                    LoadControlData();
                    EnableForm(true);
                    tbAccountNumber.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Function used to enable of disable form fields
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbAccountNumber.Enabled = enable;
                    tbDescription.Enabled = enable;
                    tbCorpAcct.Enabled = enable;
                    tbHomeBase.Enabled = enable;
                    chkInclinBilling.Enabled = enable;
                    //tbFixedCost.Enabled = enable;
                    tbFixedCost1.Enabled = enable;
                    chkInactive.Enabled = enable;
                    btnHomeBase.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;
                    // rcbHomeBase.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Function used to clear form fields
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbAccountNumber.Text = string.Empty;
                    hdnAcctID.Value = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbCorpAcct.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    hdnHomeBaseID.Value = string.Empty;
                    chkInclinBilling.Checked = false;
                    //tbFixedCost.Text = string.Empty;
                    tbFixedCost1.Text = string.Empty;
                    chkInactive.Checked = false;
                    // rcbHomeBase.Text = string.Empty;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void LoadControlData()
        {
            if (Session["AccountID"] != null)           
            {
                foreach (GridDataItem Item in dgAccounts.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("AccountID").ToString().Trim() == Session["AccountID"].ToString().Trim())
                    {
                        hdnAcctID.Value = Item.GetDataKeyValue("AccountID").ToString();
                        tbAccountNumber.Text = Item.GetDataKeyValue("AccountNum").ToString();
                        if (Item.GetDataKeyValue("AccountDescription") != null)
                        {
                            tbDescription.Text = Item.GetDataKeyValue("AccountDescription").ToString();
                        }
                        else
                        {
                            tbDescription.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("CorpAccountNum") != null)
                        {
                            tbCorpAcct.Text = Item.GetDataKeyValue("CorpAccountNum").ToString();
                        }
                        else
                        {
                            tbCorpAcct.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("HomebaseCD") != null)
                        {
                            hdnHomeBaseID.Value = Item.GetDataKeyValue("HomebaseID").ToString();
                            tbHomeBase.Text = Item.GetDataKeyValue("HomebaseCD").ToString();
                        }
                        else
                        {
                            hdnHomeBaseID.Value = string.Empty;
                            tbHomeBase.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("IsBilling") != null)
                        {
                            chkInclinBilling.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsBilling").ToString(),
                            CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            chkInclinBilling.Checked = false;
                        }
                        if (Item.GetDataKeyValue("Cost") != null)
                        {
                            //tbFixedCost.Text = Item.GetDataKeyValue("Cost").ToString();
                            tbFixedCost1.Text = Item.GetDataKeyValue("Cost").ToString();
                        }
                        else
                        {
                            //tbFixedCost.Text = string.Empty;
                            tbFixedCost1.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("IsInActive") != null)
                        {
                            chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(),
                            CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            chkInactive.Checked = false;
                        }
                        Label lbLastUpdatedUser;
                        lbLastUpdatedUser =
                            (Label)
                                dgAccounts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl(
                                    "lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text =
                            HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text =
                                HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " +
                                                   GetDateWithoutSeconds(
                                                       Convert.ToDateTime(
                                                           Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                        }
                        Item.Selected = true;
                        lbColumnName1.Text = "Account No.";
                        lbColumnName2.Text = "Description";
                        lbColumnValue1.Text = Item["AccountNum"].Text;
                        lbColumnValue2.Text = Item["AccountDescription"].Text;
                        break;
                    }
                }
            }
        }
        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            lnkBudget.Enabled = false;
            lnkBudget.CssClass = "fleet_link_disable";
            chkSearchActiveOnly.Enabled = false;
            chkSearchHomebaseOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            lnkBudget.Enabled = true;
            lnkBudget.CssClass = "fleet_link";
            chkSearchActiveOnly.Enabled = true;
            chkSearchHomebaseOnly.Enabled = true;
        }
        /// <summary>
        /// Function to getItems in the form and bind it of Entity
        /// </summary>
        /// <returns></returns>
        private FlightPakMasterService.Account GetEntityValuesFromScreen()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Account account = new FlightPakMasterService.Account();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                                        
                    if (hdnSave.Value == "Update")
                    {
                        account.AccountID = Convert.ToInt64(hdnAcctID.Value);
                    }
                    account.AccountNum = tbAccountNumber.Text.Trim();
                    account.AccountDescription = tbDescription.Text.Trim();
                    account.CorpAccountNum = tbCorpAcct.Text.Trim();
                    if (!String.IsNullOrEmpty(hdnHomeBaseID.Value))
                    {
                        account.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                    }
                    account.IsBilling = chkInclinBilling.Checked;
                    decimal fixedCost = 0;
                    //if (tbFixedCost.Text.Length > 0 && Decimal.TryParse(tbFixedCost.Text.ToString(), out fixedCost))
                    if (tbFixedCost1.Text.Length > 0 && Decimal.TryParse(tbFixedCost1.Text.ToString(), out fixedCost))
                    {
                        account.Cost = fixedCost;
                    }
                    account.IsInActive = chkInactive.Checked;
                    account.IsDeleted = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return account;
            }
        }
        
        #region "Grid Events"
        protected void Accounts_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        protected void Accounts_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                // Lock the record
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Account, Convert.ToInt64(Session["AccountID"]));
                                    Session["IsEditLockAccounts"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Account);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Account);
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                    DisableLinks();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgAccounts.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                RadAjaxManager1.FocusControl(tbAccountNumber.ClientID);
                                DisableLinks();
                                break;
                            case RadGrid.FilterCommandName:
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        protected void Accounts_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                            
                            dgAccounts.DataSource = GetList();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        protected void Accounts_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //e.Canceled = true;
                        if (Session["AccountID"] != null)
                        {
                            e.Canceled = true;
                            bool IsValidate = true;
                            if (CheckAlreadyHomeBaseExist())
                            {
                                cvHomeBase.IsValid = false;
                                RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                                IsValidate = false;
                            }
                            if (IsValidate)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.Account objAccount = GetEntityValuesFromScreen();

                                    var objRetVal = Service.UpdateAccount(objAccount);
                                    //For Data Anotation
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.Account, Convert.ToInt64(Session["AccountID"]));
                                        }
                                        Session["IsEditLockAccounts"] = "False";
                                        //e.Item.OwnerTableView.Rebind();
                                        if (objRetVal.ReturnFlag == true)
                                        {
                                            GetList(true);
                                            dgAccounts.Rebind();
                                            
                                            //To Select Last Modified Record
                                            if (((List<FlightPakMasterService.GetAccounts>)dgAccounts.DataSource).Count > 0)
                                                Session["AccountID"] = ((List<FlightPakMasterService.GetAccounts>)dgAccounts.DataSource).OrderByDescending(x => x.LastUpdTS).FirstOrDefault().AccountID;
                                          

                                            
                                            ShowSuccessMessage();
                                            EnableLinks();
                                        }
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.Account);
                                    }
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            //Session.Remove("AccountID");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        protected void Accounts_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAlreadyExist())
                        {
                            cvAcctNmbr.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAccountNumber.ClientID); //tbAccountNumber.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyHomeBaseExist())
                        {
                            cvHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Account objAccount = GetEntityValuesFromScreen();

                                //For Data Anotation
                                var objRetVal = Service.AddAccount(objAccount);
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    GetList(true);
                                    dgAccounts.Rebind();

                                    //To Select Last Modified Record
                                    if (dgAccounts.DataSource != null && ((List<FlightPakMasterService.GetAccounts>)dgAccounts.DataSource).Count > 0)
                                        Session["AccountID"] = ((List<FlightPakMasterService.GetAccounts>)dgAccounts.DataSource).OrderByDescending(x => x.LastUpdTS).FirstOrDefault().AccountID;

                                    ShowSuccessMessage();
                                    //GridEnable(true, true, true);
                                    //DefaultSelection(false);
                                    EnableLinks();
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.Account);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        protected void Accounts_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["AccountID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.Account Accounts = new FlightPakMasterService.Account();
                                    GridDataItem item = dgAccounts.SelectedItems[0] as GridDataItem;
                                    //Lock the record
                                    var returnValue = CommonService.Lock(EntitySet.Database.Account, Convert.ToInt64(Session["AccountID"]));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        //e.Item.Selected = true;
                                        //DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Account);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Account);
                                        return;
                                    }
                                    Accounts.AccountNum = item.GetDataKeyValue("AccountNum").ToString();
                                    Accounts.AccountID = Convert.ToInt64(Session["AccountID"].ToString());
                                    Accounts.IsDeleted = true;
                                    Service.DeleteAccount(Accounts);

                                    Session.Remove("AccountID");
                                    GetList(true);
                                    dgAccounts.Rebind();
                                    //e.Item.OwnerTableView.Rebind();
                                    //e.Item.Selected = true;
                                    //DefaultSelection(false);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Account, Convert.ToInt64(Session["AccountID"]));

                    }
                }
            }
        }
        protected void Accounts_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                //SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgAccounts.SelectedItems[0] as GridDataItem;
                                Session["AccountID"] = item.GetDataKeyValue("AccountID").ToString();
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                    }
                }
            }
        }
        protected void Accounts_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            Session.Remove("AccountID");
            dgAccounts.CurrentPageIndex = e.NewPageIndex;
            dgAccounts.ClientSettings.Scrolling.ScrollTop = "0";
            dgAccounts.Rebind();    
        }
        protected void Accounts_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save" && hdnSave.Value != "Update")
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgAccounts, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        #endregion
        
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgAccounts;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Delay Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgAccounts.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgAccounts.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                        //dgAccounts.Rebind();
                        //GridEnable(true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";

                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        /// <summary>
        /// Cancel Delay Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                       
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.Account, Convert.ToInt64(Session["AccountID"]));
                            }
                      

                        //Session.Remove("AccountID");
                        //DefaultSelection(false);
                        EnableLinks();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        protected void Acctnum_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbAccountNumber.Text != null)
                        {
                            CheckAlreadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        private bool CheckAlreadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //List<FlightPakMasterService.GetAccounts> lstAccount = new List<FlightPakMasterService.GetAccounts>();
                    List<FlightPakMasterService.GetAccounts> lstAccount = GetList().Where(x => x.AccountNum.Trim().ToUpper().ToString() == tbAccountNumber.Text.Trim().ToUpper().ToString()).ToList();
                    if (lstAccount.Count != 0)
                    {
                        cvAcctNmbr.IsValid = false;
                        RadAjaxManager1.FocusControl(tbAccountNumber.ClientID); //tbAccountNumber.Focus();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbAccountNumber.ClientID + "');", true);
                        returnVal = true;
                    }
                    else
                    {
                        RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                        returnVal = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }
        ///// <summary>
        ///// Check for whether the records already exists
        ///// </summary>
        ///// <returns></returns>
        //private bool CheckAllReadyExist()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        bool returnVal = false;
        //        //Handle methods throguh exception manager with return flag
        //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //        exManager.Process(() =>
        //        {
        //            lstaccountNumber = (List<string>)Session["AccountNum"];
        //            if (lstaccountNumber != null && lstaccountNumber.Contains(tbAccountNumber.Text.ToString().Trim()))
        //                returnVal = true;
        //            else
        //                returnVal = false;
        //        }, FlightPak.Common.Constants.Policy.UILayer);
        //        return returnVal;
        //    }
        //}
        /// <summary>
        /// To check unique Home Base on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((tbHomeBase.Text != null) && (tbHomeBase.Text != string.Empty))
                        {
                            CheckAlreadyHomeBaseExist();
                        }
                        else
                        {
                            hdnHomeBaseID.Value = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        private bool CheckAlreadyHomeBaseExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbHomeBase.Text != string.Empty) && (tbHomeBase.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.Trim().ToUpper().Equals(tbHomeBase.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvHomeBase.IsValid = false;
                                hdnHomeBaseID.Value = string.Empty;
                                RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                                ReturnValue = true;
                            }
                            else
                            {
                                List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                CompanyList = (List<FlightPakMasterService.GetAllCompanyMaster>)RetValue.ToList();
                                hdnHomeBaseID.Value = CompanyList[0].HomebaseID.ToString();
                                tbHomeBase.Text = CompanyList[0].HomebaseCD;
                                ReturnValue = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// generate regular expression for Account number and Corp Account number validations
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        private string GenerateExpression(string strInput)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(strInput))
            {
                string strOutput = String.Empty;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    for (int i = 0; i <= strInput.Length - 1; i++)
                    {
                        if (strInput.Substring(i, 1) == ".")
                            strOutput = strOutput + "[.]";
                        else if (strInput.Substring(i, 1) == "!")
                        {
                            strOutput = strOutput + "[!]";
                        }
                        else
                        {
                            int strtoInt;
                            if (int.TryParse(strInput.Substring(i, 1), out strtoInt))
                            {
                                strOutput = strOutput + "[0-9]";
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return strOutput;
            }
        }
        /// <summary>
        /// Validate Home base field based on Strformat.
        /// </summary>
        /// <param name="strFormat"></param>
        private void ValidateHomeBase(string strFormat)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(strFormat))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    strFormat = "999.999.99";
                    RegularExpressionValidator revCorpAcct = new RegularExpressionValidator();
                    if (!tbCorpAcct.Controls.Contains(revCorpAcct))
                    {
                        revCorpAcct.ControlToValidate = tbCorpAcct.ID;
                        revCorpAcct.Display = ValidatorDisplay.Dynamic;
                        tbCorpAcct.Controls.Add(revCorpAcct);
                        revCorpAcct.ForeColor = System.Drawing.Color.Red;
                        revCorpAcct.Visible = true;
                        revCorpAcct.ValidationGroup = "Save";
                        revCorpAcct.ErrorMessage = strFormat.ToString();
                    }
                    revCorpAcct.ValidationExpression = GenerateExpression(strFormat);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //SearchAndFilter(true);
                        dgAccounts.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }

        #endregion

        #region "Account Number Format"
        private void AccountNumberFormat()
        {
            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            //{
            //    using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
            //    {
            //        var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == UserPrincipal.Identity._customerID && x.HomebaseID == UserPrincipal.Identity._homeBaseId).ToList();
            //        if (objCompany.Count != 0)
            //        {
            hdnAccNumFormat.Value = UserPrincipal.Identity._fpSettings._AccountFormat;
            hdnCorpAccNumFormat.Value = UserPrincipal.Identity._fpSettings._CorpAccountFormat;
            if (hdnAccNumFormat.Value != string.Empty)
            {
                tbAccountNumber.Attributes.Add("onkeypress", "javascript:return fnAllowNumericAndChar(this, event,'.!');");
                tbAccountNumber.Attributes.Add("onBlur", "javascript:return RemoveSpecialCharsNew(this);");
            }
                       
            //        }
            //    }
            //}
        }
        #endregion

        #region "Reports
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        #endregion

        #region "Service Calls"
        private List<FlightPakMasterService.GetAccounts> GetList(bool ExpireSession = false)
        {
            List<FlightPakMasterService.GetAccounts> Accounts;// = new List<FlightPakMasterService.GetAccounts>();

            if (!ExpireSession && Session["AccountNum"] != null)
            {
                Accounts = (List<FlightPakMasterService.GetAccounts>)Session["AccountNum"];
            }
            else
            {
                using (FlightPakMasterService.MasterCatalogServiceClient masterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var retVal = masterService.GetAllAccountList();
                    if (retVal.ReturnFlag == true)
                        Accounts = retVal.EntityList.Where(x => x.IsDeleted == false).ToList();
                    else
                        Accounts = null;
                    
                    Session["AccountNum"] = Accounts;
                }
            }
            if (((chkSearchActiveOnly.Checked == true) || (chkSearchHomebaseOnly.Checked == true)) && !IsPopUp)
            {
                if (Accounts.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { Accounts = Accounts.Where(x => x.IsInActive == false).ToList<GetAccounts>(); }
                    if (chkSearchHomebaseOnly.Checked == true) { Accounts = Accounts.Where(x => x.HomebaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAccounts>(); }
                    // after the above linqs executed accounts count may become 0 if there are no active accounts or 
                    //if there are no any homebase accounts. 
                    //if so need to clear the form. But this clearing should not happen when adding a new record.
                    if (Accounts.Count < 1 && hdnSave.Value != "Save") ClearForm();
                }
                
               
            }

            return Accounts;
        }
        #endregion

    }
}
