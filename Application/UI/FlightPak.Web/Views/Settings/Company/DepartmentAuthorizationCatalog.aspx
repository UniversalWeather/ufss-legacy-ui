﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="DepartmentAuthorizationCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Company.DepartmentAuthorizationCatalog"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        /*
        * <summary>
        * Function to Ask for Confirmation, if you select any row to delete.
        * </summary>
        **/
        function ProcessDeptDelete() {
            //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
            var grid = $find('<%= dgDepartment.ClientID %>');
            var msg = 'Are you sure you want to delete this record?';
            var oManager = $find("<%= RadWindowManager1.ClientID %>"); 

            if (grid.get_masterTableView().get_selectedItems().length > 0) {

                oManager.radconfirm(msg, callBackFn, 330, 100, '', 'Delete'); 
                return false;
            }
            else {
                alert('Please select a record from the above table.');
                return false;
            }
            function callBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find('<%= dgDepartment.ClientID %>');
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }
        }

        // this function is used to display the format when the relevent textbox is empty.
        function ValidateEmptyTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "00.00";
            }
        }
            

        function openReport() {
            
            var url = '../../Reports/ExportReportInformation.aspx?Report=RptDBDeptAuthorizationExport';
           
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open(url, "RadExportData");
        }
        
        /*
        * <summary>
        * To check whether the row is selected in grid before update
        * </summary>
        **/
        function ProcessDeptUpdate() {
            var grid = $find('<%= dgDepartment.ClientID %>');
            if (grid.get_masterTableView().get_selectedItems().length == 0) {

                alert('Please select a record from the above table.');
                return false;
            }
        }
        /*
        * <summary>
        * Function to Ask for Confirmation, if you select any row to delete.
        * </summary>
        **/
        function ProcessAuthDelete() {
            //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
            var grid = $find('<%= dgAuthorization.ClientID %>');
            var msg = 'Are you sure you want to delete this record?';
            var oManager = $find("<%= RadWindowManager1.ClientID %>"); 


            if (grid.get_masterTableView().get_selectedItems().length > 0) {

                oManager.radconfirm(msg, callBackFn, 330, 100, '', 'Delete'); 
                return false;
            }
            else {
                alert('Please select a record from the above table');
                return false;
            }
            function callBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find('<%= dgAuthorization.ClientID %>');
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }
        }

        /*
        * <summary>
        * To check whether the row is selected in grid before update
        * </summary>
        **/
        function ProcessAuthUpdate() {
            var grid = $find('<%= dgAuthorization.ClientID %>');
            if (grid.get_masterTableView().get_selectedItems().length == 0) {

                alert('Please select a record from the above table');
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openWin() {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); 
                var oWnd = oManager.open("../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCode.ClientID%>").value, "RadWindow1"); 

            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }


            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = htmlDecode(arg.ClientCD);
                        document.getElementById("<%=hdnclientID.ClientID%>").value = arg.ClientID;
                        document.getElementById("<%=cvClientCode.ClientID%>").innerHTML = "";

                    }
                    else {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                    }
                }

            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                //alert(new String("Width:" + bounds.width + " " + "Height: " + bounds.height));
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function ShowReports(radWin, ReportFormat, ReportName) {
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                if (ReportFormat == "EXPORT") {
                    
                    url = "../../Reports/ExportReportInformation.aspx?Report=" + ReportName ;
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    var oWnd = oManager.open(url, 'RadExportData');
                    return true;
                }
                else {
                    return true;
                }
            }
            function OnClientCloseExportReport(oWnd, args) {
                document.getElementById("<%=btnShowReports.ClientID%>").click();
            }

            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update"
                        && document.getElementById('<%=hdnDept.ClientID%>').value === "Dept") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });


             
            });
            $(document).ready(function () {
                $("<style type='text/css'> .art-Sheet-body-Authorization{ min-height:inherit;} </style>").appendTo("head");
                $(".art-Sheet-body").addClass('art-Sheet-body-Authorization');
            });
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="rpbAuth">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgAuthorization">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAuthorization" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgDepartment" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="rpbAuth" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgDepartment">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDepartment" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <%--<telerik:AjaxUpdatedControl ControlID="dgAuthorization" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgDepartment" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbClientCode">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbClientCode" />
                        <telerik:AjaxUpdatedControl ControlID="cvClientCode" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbAuthCode">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbAuthCode" />
                        <telerik:AjaxUpdatedControl ControlID="cvAuthRequired" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbDeptCode">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbDeptCode" />
                        <telerik:AjaxUpdatedControl ControlID="tbDeptDescription" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkSearchActiveOnly">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDepartment" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkInactiveAuth">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAuthorization" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgDepartment" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="rpbAuth" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" NavigateUrl="CustomerCatalog.aspx" VisibleStatusbar="false">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseExportReport" Title="Export Report Information" KeepInScreenBounds="true"
                    AutoSize="false" Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <br />
        
        <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                        <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
        </telerik:RadWindowManager>
        <table style="width: 100%;" cellpadding="0" cellspacing="0" runat="server" id="table1">
            <tr>
                <td align="left">
                    <div class="tab-nav-top">
                        <span class="head-title">Department/Authorization</span> <span class="tab-nav-icons">
                            <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBDeptAuthorization');"
                                OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                            <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBDeptAuthorization');return false;"
                                OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                            <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                                Style="display: none;" />
                            <a href="../../Help/ViewHelp.aspx?Screen=DeptAuthorizationHelp" class="help-icon"
                                target="_blank" title="Help"></a>
                            <asp:HiddenField ID="hdnReportName" runat="server" />
                            <asp:HiddenField ID="hdnReportFormat" runat="server" />
                            <asp:HiddenField ID="hdnReportParameters" runat="server" />
                        </span>
                    </div>
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" runat="server" id="table2" class="head-sub-menu">
            <tr>
                <td>
                    <div class="tags_select">
                        Department Maintenance</div>
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" runat="server" id="table3" class="head-sub-menu">
            <tr>
                <td>
                    <div class="status-list">
                        <span>
                            <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Display Inactive" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                AutoPostBack="true" /></span>
                    </div>
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="dgDepartment" runat="server" AllowSorting="true" OnItemCreated="dgDepartment_ItemCreated"
            Visible="true" OnNeedDataSource="dgDepartment_BindData" OnItemCommand="dgDepartment_ItemCommand"
            OnUpdateCommand="dgDepartment_UpdateCommand" OnInsertCommand="dgDepartment_InsertCommand"
            OnDeleteCommand="dgDepartment_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
            OnPageIndexChanged="dgDepartment_PageIndexChanged" OnPreRender="dgDepartment_PreRender"
            AllowPaging="true" OnSelectedIndexChanged="dgDepartment_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="341px">
            <MasterTableView DataKeyNames="CustomerID,DepartmentID,DepartmentCD,DepartmentName,IsInActive,DepartPercentage,LastUpdUID,LastUpdTS,ClientID,ClientCD"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="Department Code" AutoPostBackOnFilter="false"
                        HeaderStyle-Width="100px" FilterControlWidth="80px" FilterDelay="500"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DepartmentName" HeaderText="Description" AutoPostBackOnFilter="false"
                        FilterControlWidth="450px" HeaderStyle-Width="470px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" AutoPostBackOnFilter="true"
                        HeaderStyle-Width="80px" AllowFiltering="false" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridBoundColumn DataField="ClientCD" HeaderText="Client" AutoPostBackOnFilter="false"
                        FilterControlWidth="80px" HeaderStyle-Width="100px" FilterDelay="500"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lbtnDepInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            Visible='<%# IsAuthorized(Permission.Database.AddDepartmentAuthorizationCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnDepEdit" runat="server" OnClientClick="javascript:return ProcessDeptUpdate();"
                            ToolTip="Edit" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditDepartmentAuthorizationCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnDepDelete" OnClientClick="javascript:return ProcessDeptDelete();"
                            runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeleteDepartmentAuthorizationCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                    <div>
                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="true" BorderColor="Black"
                CssClass="PanelHeaderStyle">
                <table cellpadding="0" cellspacing="0" width="100%" id="table6" runat="server">
                    <tr>
                        <td align="left" class="tdLabel160">
                            <div id="tdSuccessMessage" class="success_msg">
                                Record saved successfully.</div>
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%" id="table7" runat="server">
                    <tr>
                        <td align="left">
                            Authorization Maintenance
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="tblspace_5">
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="border-box" id="tbdepartform" runat="server">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkDeptInactive" runat="server" Text="Inactive" Font-Size="12px"
                                            ForeColor="Black" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel120" valign="top">
                                        <asp:Label ID="lbDeptCode" runat="server" Text="Department Code" CssClass="mnd_text"></asp:Label>
                                    </td>
                                    <td class="text150" valign="top">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbDeptCode" runat="server" MaxLength="8" CssClass="text50" ValidationGroup="Save"
                                                        OnTextChanged="tbDeptCode_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvDeptCode" runat="server" ErrorMessage="Department Code is Required."
                                                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbDeptCode" CssClass="alert-text"
                                                        ValidationGroup="Save"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvDeptCode" runat="server" ControlToValidate="tbDeptCode"
                                                        ErrorMessage="Unique Department Code is Required." Display="Dynamic" CssClass="alert-text"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel120">
                                        <asp:Label ID="lbDeptDescription" runat="server" Text="Description" CssClass="mnd_text"></asp:Label>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbDeptDescription" runat="server" MaxLength="25" CssClass="text250"
                                                        ValidationGroup="Save"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvDeptDescription" runat="server" ErrorMessage="Description is Required"
                                                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbDeptDescription"
                                                        CssClass="alert-text" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel120" valign="top">
                                        Client Code
                                    </td>
                                    <td valign="top">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="tdLabel120">
                                                    <asp:TextBox ID="tbClientCode" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                        CssClass="text50" ValidationGroup="Save" AutoPostBack="true" onBlur="return RemoveSpecialChars(this)"
                                                        OnTextChanged="tbClientCode_TextChanged"></asp:TextBox>
                                                    <asp:Button ID="btnHomeBase" runat="server" OnClientClick="javascript:openWin();return false;"
                                                        CssClass="browse-button" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="cvClientCode" runat="server" ControlToValidate="tbClientCode"
                                                        ErrorMessage="Invalid Client Code." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel120" valign="top">
                                        Margin Percentage
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbMarginPercentage" runat="server" Text="0.0" MaxLength="10" ValidationGroup="Save"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return ValidateEmptyTextbox(this, event)"
                                            CssClass="text80"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="revtbMarginPercentage" runat="server" Display="Dynamic"
                                            ErrorMessage="Invalid Format (NN.N)" ControlToValidate="tbMarginPercentage" CssClass="alert-text"
                                            ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div id="toolbar" class="scr_esp_down_db">
                    <div class="floating_main_db">
                        <div class="floating_bar_db">
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                            <span class="padright_20">
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                            <span class="padright_10">
                                <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                            <span>
                                <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
                <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
            </asp:Panel>
            <div id="divAuthorization" runat="server">
                <table cellpadding="0" cellspacing="0" class="head-sub-menu" id="table4" runat="server">
                    <tr>
                        <td>
                            <div class="status-list">
                                <span>
                                    <asp:CheckBox ID="chkInactiveAuth" runat="server" Text="Display Inactive" OnCheckedChanged="FilterBychkInactiveAuth_OnCheckedChanged"
                                        AutoPostBack="true" /></span>
                            </div>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="dgAuthorization" runat="server" AllowSorting="true" OnItemCreated="dgAuthorization_ItemCreated"
                    Visible="true" OnNeedDataSource="dgAuthorization_BindData" OnItemCommand="dgAuthorization_ItemCommand"
                    OnUpdateCommand="dgAuthorization_UpdateCommand" OnInsertCommand="dgAuthorization_InsertCommand"
                    OnDeleteCommand="dgAuthorization_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="true" OnSelectedIndexChanged="dgAuthorization_SelectedIndexChanged"
                    OnPageIndexChanged="dgAuthorization_PageIndexChanged" AllowFilteringByColumn="true"
                    PagerStyle-AlwaysVisible="true" Height="341px" OnPreRender="dgAuthorization_PreRender1">
                    <MasterTableView DataKeyNames="CustomerID,AuthorizationCD,DepartmentID,AuthorizationID,DeptAuthDescription,LastUpdUID,LastUpdTS,ClientID,IsInActive,AuthorizerPhoneNum"
                        CommandItemDisplay="Bottom">
                        <Columns>
                            <telerik:GridBoundColumn DataField="AuthorizationCD" HeaderText="Authorization Code"
                               HeaderStyle-Width="100px" FilterControlWidth="80px" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                                CurrentFilterFunction="StartsWith" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DeptAuthDescription" HeaderText="Description"
                                FilterControlWidth="380px" HeaderStyle-Width="400px" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                                CurrentFilterFunction="StartsWith" FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AuthorizerPhoneNum" HeaderText="Auth Phone" AutoPostBackOnFilter="false"
                                HeaderStyle-Width="180px" FilterControlWidth="160px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                FilterDelay="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" AutoPostBackOnFilter="true"
                                AllowFiltering="false" HeaderStyle-Width="80px" ShowFilterIcon="false" CurrentFilterFunction="EqualTo">
                            </telerik:GridCheckBoxColumn>
                        </Columns>
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px; float: left; clear: both;">
                                <asp:LinkButton ID="lbtnInitAuthorizationInsert" runat="server" CommandName="InitInsert"
                                 ToolTip="Add"    Visible='<%# IsAuthorized(Permission.Database.AddAuthorization)%>'><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtnInitAuthorizationEdit" runat="server" OnClientClick="javascript:return ProcessAuthUpdate();"
                                 ToolTip="Edit"   CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditAuthorization)%>'><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtnAuthorizationDelete" OnClientClick="javascript:return ProcessAuthDelete();"
                                  ToolTip="Delete"  runat="server" CommandName="DeleteSelected" Visible='<%# IsAuthorized(Permission.Database.DeleteAuthorization)%>'><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                            </div>
                            <div>
                                <asp:Label ID="lbLastUpdatedAuthorizationUser" runat="server" CssClass="last-updated-text"></asp:Label>
                            </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings CaseSensitive="false" />
                </telerik:RadGrid>
                <table width="100%" cellpadding="0" cellspacing="0" runat="server" id="table5">
                    <tr style="display: none;">
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="tdLabel100" valign="top">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        Authorization Maintainence
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tblspace_5">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div id="rpbAuth" runat="server">
                    <table class="border-box" width="100%">
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel100" valign="top">
                                            <asp:CheckBox ID="chkAuthInactive" runat="server" Text="Inactive" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="tblspace_10">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel120" valign="top">
                                            <asp:Label ID="lbCode" runat="server" Text="Authorization Code" CssClass="mnd_text"></asp:Label>
                                        </td>
                                        <td class="text180">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbAuthCode" runat="server" MaxLength="8" CssClass="text50" OnTextChanged="tbAuthCode_TextChanged"
                                                            AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CustomValidator ID="cvAuthRequired" runat="server" ErrorMessage="Authorization Code is Required."
                                                            ValidationGroup="Save" Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbAuthCode"
                                                            CssClass="alert-text"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel120">
                                            <asp:Label ID="lbAuthorizationDescription" runat="server" Text="Description" CssClass="mnd_text"></asp:Label>
                                        </td>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbAuthDescription" runat="server" MaxLength="25" CssClass="text250"
                                                            ValidationGroup="Save"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CustomValidator ID="cvAuthDescription" runat="server" ErrorMessage="Description is Required"
                                                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbAuthDescription"
                                                            CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel120">
                                            Phone
                                        </td>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbAuthPhone" runat="server" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                            CssClass="text120" ValidationGroup="Save"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" CssClass="button" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            ValidationGroup="Save" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" CssClass="button" Text="Cancel" runat="server" CausesValidation="false"
                            OnClick="Cancel_Click" />
                        <asp:HiddenField ID="hdnDept" runat="server" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnAuth" runat="server" />
                        <asp:HiddenField ID="hdnDeptCode" runat="server" />
                        <asp:HiddenField ID="hdnclientID" runat="server" />
                        <asp:HiddenField ID="hdnClientCode" runat="server" />
                        <asp:HiddenField ID="hdnDeptId" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
