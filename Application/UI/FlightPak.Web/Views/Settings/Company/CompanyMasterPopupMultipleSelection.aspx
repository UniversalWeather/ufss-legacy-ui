﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <title>Company Profile</title>
      <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    
    <script type="text/javascript">
        var selectedRowData = "";
        var jqgridTableId = '#gridHomebase';

        $(document).ready(function () {
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            selectedRowData = decodeURI(getQuerystring("HomeBaseID", ""));
            var splitedArray=new Array();
            splitedArray = selectedRowData.split(",");

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                returnToParent(rowData);
                return false;
            });

            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'homebase';
                    postData.showInactive = true;
                    return postData;
                },
                height: 230,
                width: 650,
                viewrecords: true,
                multiselect: true,
                pager: "#pg_gridPager",
                colNames: ['HomebaseID', 'Main Base', 'Base Description'],
                colModel: [
                    { name: 'HomebaseID', index: 'HomebaseID', key: true, hidden: true },
                    { name: 'HomebaseCD', index: 'ICAOID', width: 50 },
                    { name: 'BaseDescription', index: 'BaseDescription', width: 150 }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);
                },
                onSelectRow: function (id) {
                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['HomebaseID'];//replace name with any column

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $('#results_table').jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                afterInsertRow: function (rowid, rowObject) {
                    for (var i = 0; i < splitedArray.length; i++) {
                        if (rowObject.HomebaseID == splitedArray[i]) {
                            $(jqgridTableId).setSelection(rowid, true);
                        }
                    }
                }
            });
            RemovePaging(jqgridTableId);
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

        });
        
        function returnToParent(rowData) {
            var HomebaseID = "";
            var HomebaseCD = "";

            var oArg = new Object();
            var selectedrows = $(jqgridTableId).jqGrid('getGridParam', 'selarrrow');
            var len = selectedrows.length;
            if (selectedrows.length) {
                for (var i = 0; i < selectedrows.length; i++) {
                    var row = selectedrows[i];
                    var rowsDataValues= $(jqgridTableId).jqGrid('getRowData', selectedrows[i]);
                    var cell1 = rowsDataValues.HomebaseID;
                    var cell2 = rowsDataValues.HomebaseCD;
                    if (i == (selectedrows.length - 1)) {
                        HomebaseID = HomebaseID + cell1;
                        HomebaseCD =cell2;
                    }
                    else {
                        HomebaseID = HomebaseID + cell1 + ",";
                        HomebaseCD =cell2;
                    }
                }
                oArg.HomebaseID = HomebaseID;
                oArg.HomebaseCD = HomebaseCD;
                oArg.Arg1 = oArg.HomebaseCD;
                oArg.CallingButton = "HomeBaseCD";
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
        }


    </script>

</head>
<body>
    <form id="form1">

        <div class="jqgrid">

            <div>
                <table class="box1">
                    <tr>
                        <td>
                            <table id="gridHomebase" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                            </div>
                        </td>
                    </tr>
                </table>

            </div>
        </div>

    </form>
</body>
</html>
