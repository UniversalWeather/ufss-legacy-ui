﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExchangeRateMasterPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.ExchangeRateMasterpopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="FlightPak.Common.Constants" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Exchange Rate</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgExchangeRate.ClientID %>");
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgExchangeRate.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "ExchRateCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "ExchangeRateID");

                }

                if (selectedRows.length > 0) {

                    oArg.ExchangeRateCD = cell1.innerHTML;
                    oArg.ExchangeRateID = cell2.innerHTML;
                }
                else {
                    oArg.ExchangeRateCD = "";
                    oArg.ExchangeRateID = "";

                }


                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
            function rebindgrid() {
                var masterTable = $find("<%= dgExchangeRate.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            function EditButtonProcess() {
                var grid = $find("<%= dgExchangeRate.ClientID %>");
                if (grid.get_masterTableView().get_selectedItems().length > 0) {
                }
                else {
                    radalert('Please select a record from the above table', 330, 100, "Delete", "");
                    return false;
                }

            }
            function ProcessDeleteExchangeRate(customMsg) {

                var grid = $find("<%= dgExchangeRate.ClientID %>");
                var msg = 'Are you sure you want to delete this record?';

                if (customMsg != null) {
                    msg = customMsg;
                }

                if (grid.get_masterTableView().get_selectedItems().length > 0) {

                    radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
                    return false;

                }
                else {
                    radalert('Please select a record from the above table', 330, 100, "Delete", "");
                    return false;
                }
            }
            function callBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find("<%= dgExchangeRate.ClientID %>");
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgExchangeRate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgExchangeRate" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgExchangeRate" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" /></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                    OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgExchangeRate" runat="server" AllowMultiRowSelection="true"
                        OnItemDataBound="dgExchangeRate_ItemDataBound" AllowSorting="true" OnNeedDataSource="dgExchangeRate_BindData"
                        AutoGenerateColumns="false" Height="341px" PageSize="10" AllowPaging="true" Width="650px"
                        OnItemCommand="dgExchangeRate_ItemCommand" OnPreRender="dgExchangeRate_PreRender" OnDeleteCommand="dgExchangeRate_DeleteCommand">
                        <MasterTableView DataKeyNames="ExchRateCD,ExchangeRateID" CommandItemDisplay="Bottom">
                            <Columns>
                                <telerik:GridBoundColumn DataField="ExchRateCD" HeaderText="Exchange Rate Code" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="false" HeaderStyle-Width="100px" FilterControlWidth="80px"
                                    CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ExchangeRateID" HeaderText="ExchangeRateID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ExchRateDescription" HeaderText="Description"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="240px"
                                    FilterControlWidth="220px" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FromDT" HeaderText="From Date" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="false" HeaderStyle-Width="100px" FilterControlWidth="80px"
                                    CurrentFilterFunction="EqualTo" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ToDT" HeaderText="To Date" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="false" HeaderStyle-Width="100px" FilterControlWidth="80px"
                                    CurrentFilterFunction="EqualTo" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ExchRate" HeaderText="Exchange Rate" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="false" HeaderStyle-Width="100px" FilterControlWidth="80px"
                                    CurrentFilterFunction="EqualTo" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <%--<telerik:GridBoundColumn DataField="CurrencyID" HeaderText="Currency ID" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="true" HeaderStyle-Width="100px" FilterControlWidth="80px">
                                </telerik:GridBoundColumn>--%>
                                <%-- <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="LastUpdUID" ShowFilterIcon="false" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ExchangeRateID" HeaderText="Exchange Rate ID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ExchDescription" HeaderText="Description" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FromDT" HeaderText="From Date" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ToDT" HeaderText="To Date" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ExchRate" HeaderText="Exchange Rate" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CurrencyID" HeaderText="Currency ID" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="LastUpdUID" ShowFilterIcon="false" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="LastUpdTS" ShowFilterIcon="false" >
                    </telerik:GridBoundColumn>--%>
                            </Columns>
                            <CommandItemTemplate>
                                  <div style="padding: 5px 5px; float: left;">
                                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                            Visible='<%# IsAuthorized(Permission.Database.AddExchangeRatesCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                            Visible='<%# IsAuthorized(Permission.Database.EditExchangeRatesCatalog)%>' OnClientClick="javascript:return EditButtonProcess();"><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDeleteExchangeRate();"
                                            runat="server" CommandName="DeleteSelected" ToolTip="Delete" Visible='<%# IsAuthorized(Permission.Database.DeleteExchangeRatesCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>

                                    </div>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button okButton">
                                        OK</button>
                                    <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
