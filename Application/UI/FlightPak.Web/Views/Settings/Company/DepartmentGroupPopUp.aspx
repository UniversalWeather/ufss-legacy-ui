﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DepartmentGroupPopUp.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.DepartmentGroupPopUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Department Group</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgDepartmentGroup.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgDepartmentGroup.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "DepartmentGroupCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "DepartmentGroupDescription");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "HomeBaseCD")

                        if (i == 0)
                            oArg.DepartmentGroupCD = cell1.innerHTML + ",";
                        else

                            oArg.DepartmentGroupCD += cell1.innerHTML + ",";
                        oArg.DepartmentGroupDescription += cell2.innerHTML + ",";
                        oArg.HomeBaseCD += cell3.innerHTML + ",";

                    }
                }
                else {
                    oArg.DepartmentGroupCD = "";
                    oArg.DepartmentGroupDescription = "";
                    oArg.HomeBaseCD = "";
                }

                if (oArg.DepartmentGroupCD != "")
                    oArg.DepartmentGroupCD = oArg.DepartmentGroupCD.substring(0, oArg.DepartmentGroupCD.length - 1)
                else
                    oArg.DepartmentGroupCD = "";

                oArg.Arg1 = oArg.DepartmentGroupCD;
                oArg.CallingButton = "DepartmentGroupID";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgDepartmentGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDepartmentGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDepartmentGroup" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" /></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                    OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgDepartmentGroup" runat="server" AllowMultiRowSelection="true"  OnItemCreated="dgDepartmentGroup_ItemCreated"
                        AllowSorting="true" OnNeedDataSource="dgDepartmentGroup_BindData" AutoGenerateColumns="false"
                         AllowPaging="false" Width="500px" OnPreRender="dgDepartmentGroup_PreRender" OnItemCommand="dgDepartmentGroup_ItemCommand" >
                        <MasterTableView DataKeyNames="DepartmentGroupCD,DepartmentGroupDescription,HomeBaseCD,LastUpdUID,LastUpdTS,IsDeleted"
                            CommandItemDisplay="None" Width="100%" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn DataField="DepartmentGroupCD" HeaderText="Code" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DepartmentGroupDescription" HeaderText="Description"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="340px" FilterControlWidth="320px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grd_ok">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                        OK</button>
                                </div>
                                <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                    visible="false">
                                    Use CTRL key to multi select</div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent" />
                            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
