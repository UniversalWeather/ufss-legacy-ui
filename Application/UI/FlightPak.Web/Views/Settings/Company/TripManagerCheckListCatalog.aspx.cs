﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels;
using FlightPak.Web.Views.Transactions;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class TripManagerCheckListCatalog : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private bool IsEmptyCheck = true;
        private bool TripManagerChecklistPageNavigated = false;
        private string strTripManagerChecklistCD = "";
        private string strTripManagerChecklistID = "";
        private List<string> lstCheckGroupCodes = new List<string>();
        private bool _selectLastModified = false;

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        {
                            // Grid Control could be ajaxified when the page is initially loaded.
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgTripManagerCheckList, dgTripManagerCheckList, RadAjaxLoadingPanel1);
                            // Store the clientID of the grid to reference it later on the client
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgTripManagerCheckList.ClientID));
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                            
                            lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewTripManagerChecklistReports);
                            if (!IsPostBack)
                            {
                                CheckAutorization(Permission.Database.ViewTripManagerCheckListCatalog);
                                // Method to display first record in read only format   
                                if (Session["SearchItemPrimaryKeyValue"] != null)
                                {
                                    dgTripManagerCheckList.Rebind();
                                    ReadOnlyForm();
                                    DisableLinks();
                                    GridEnable(true, true, true);
                                    Session.Remove("SearchItemPrimaryKeyValue");
                                }
                                else
                                {
                                    chkSearchActiveOnly.Checked = true;
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }
        }

        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgTripManagerCheckList.Rebind();
                    }
                    if (dgTripManagerCheckList.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedChecklistID"] = null;
                        //}
                        if (Session["SelectedChecklistID"] == null)
                        {
                            dgTripManagerCheckList.SelectedIndexes.Add(0);
                            Session["SelectedChecklistID"] = dgTripManagerCheckList.Items[0].GetDataKeyValue("CheckListID").ToString();
                        }

                        if (dgTripManagerCheckList.SelectedIndexes.Count == 0)
                            dgTripManagerCheckList.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void SelectItem()
        {
            bool itemFlag = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedChecklistID"] != null)
                    {
                        string ID = Session["SelectedChecklistID"].ToString();
                        foreach (GridDataItem Item in dgTripManagerCheckList.MasterTableView.Items)
                        {
                            if (Item["CheckListID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                itemFlag = true;
                                break;
                            }
                        }
                        if (!itemFlag)
                        {
                            if (dgTripManagerCheckList.Items.Count > 0)
                                Session["SelectedChecklistID"] = dgTripManagerCheckList.Items[0].GetDataKeyValue("CheckListID").ToString();
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1))
                        {
                            e.Updated = dgTripManagerCheckList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckList_ItemCreated(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        /// <summary>
        /// Bind Crew Check List Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckList_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            //FlightPakMasterService.MasterCatalogServiceClient objTripChecklistService = new FlightPakMasterService.MasterCatalogServiceClient();
                            //var objTripCheckListVal = objTripChecklistService.GetTripManagerChecklistListInfo();
                            //if (objTripCheckListVal.ReturnFlag == true)
                            //{
                            //    dgTripManagerCheckList.DataSource = objTripCheckListVal.EntityList;
                            //}
                            //Session.Remove("TripChecklistCodes");
                            //lstTripManagerCode = new List<string>();
                            //foreach (GetAllTripManagerCheckList TripChecklistEntity in objTripCheckListVal.EntityList)
                            //{
                            //    lstTripManagerCode.Add(TripChecklistEntity.CheckListCD.ToLower().Trim());
                            //}
                            //Session["TripChecklistCodes"] = lstTripManagerCode;

                            var TripCheckListVal = Service.GetTripManagerChecklistListInfo();
                            List<FlightPakMasterService.GetAllTripManagerCheckList> TripChecklistlst = new List<FlightPakMasterService.GetAllTripManagerCheckList>();
                            if (TripCheckListVal.ReturnFlag == true)
                            {
                                TripChecklistlst = TripCheckListVal.EntityList;
                            }
                            dgTripManagerCheckList.DataSource = TripChecklistlst;
                            Session["ChecklistCD"] = TripChecklistlst;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }

                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                    }
                }
            }
        }

        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSave.Value = "Save";
                        ClearForm();
                        EnableForm(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }
        }

        /// <summary>
        /// Item Command for Crew Check List Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckList_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnIsEdit.Value = "true";
                                if (Session["SelectedChecklistID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.TripManagerCheckList, Convert.ToInt64(Session["SelectedChecklistID"].ToString().Trim()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.TripManagerCheckList);
                                            SelectItem();
                                            return;
                                        }
                                        SelectItem();
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                dgTripManagerCheckList.SelectedIndexes.Clear();
                                tbCode.Focus();
                                hdnIsEdit.Value = "false";
                                DisableLinks();

                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        /// <summary>
        /// Update Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckList_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        hdnIsEdit.Value = "true";
                        if (CheckAlreadyGroupCodeExist())
                        {
                            cvGroup.IsValid = false;
                            tbGroup.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            if (Session["SelectedChecklistID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient TripChecklistService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var RetVal= TripChecklistService.UpdateTripManagerChecklist(GetItems());
                                    if (RetVal.ReturnFlag == true)
                                    {
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.TripManagerCheckList, Convert.ToInt64(Session["SelectedChecklistID"].ToString().Trim()));
                                            e.Item.OwnerTableView.Rebind();
                                            e.Item.Selected = true;                                            
                                            //dgTripManagerCheckList.Rebind();
                                            GridEnable(true, true, true);
                                            SelectItem();
                                            ReadOnlyForm();
                                        }
                                        ShowSuccessMessage();
                                        EnableLinks();
                                        _selectLastModified = true;
                                        ResetChecklistSession();
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(RetVal.ErrorMessage, ModuleNameConstants.Database.TripManagerCheckList);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }
        }

        protected void ResetChecklistSession()
        {
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                PreflightTripViewModel tripObj = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                foreach (PreflightLegViewModel preflightLegObj in tripObj.PreflightLegs)
                {
                    List<PreflightChecklistViewModel> preflightCheckList = preflightLegObj.PreflightChecklist;
                    UserPrincipalViewModel upViewModel = TripManagerBase.getUserPrincipal();
                    long checkGroupId = preflightCheckList == null || preflightCheckList.Count == 0 ? (long)upViewModel._DefaultCheckListGroupID : (long)preflightCheckList[0].CheckGroupID;
                    List<PreflightChecklistViewModel> preflightCheckListNew = new List<PreflightChecklistViewModel>();
                    using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ObjRetVal = ObjService.GetTripManagerChecklistListInfo();
                        List<FlightPakMasterService.GetAllTripManagerCheckList> TripChecklist = new List<FlightPakMasterService.GetAllTripManagerCheckList>();
                        TripChecklist = ObjRetVal.EntityList.Where(x => x.CheckGroupID == checkGroupId && x.IsDeleted == false && x.IsInactive == false).ToList();
                        foreach (GetAllTripManagerCheckList tripManagerCheckListObj in TripChecklist)
                        {
                             PreflightChecklistViewModel matchedPreflightCheckList;
                             if (preflightCheckList != null && preflightCheckList.Count > 0)
                                 matchedPreflightCheckList = preflightCheckList.Find(x => x.CheckListID == tripManagerCheckListObj.CheckListID);
                             else
                                 matchedPreflightCheckList = null;
                             if (matchedPreflightCheckList != null)
                            {
                                matchedPreflightCheckList.CheckGroupCD = tripManagerCheckListObj.CheckGroupCD;
                                matchedPreflightCheckList.CheckGroupID = tripManagerCheckListObj.CheckGroupID;
                                matchedPreflightCheckList.CheckListDescription = tripManagerCheckListObj.CheckListDescription;
                                matchedPreflightCheckList.CheckListID = tripManagerCheckListObj.CheckListID;
                                matchedPreflightCheckList.CustomerID = tripManagerCheckListObj.CustomerID;
                                matchedPreflightCheckList.IsInactive = tripManagerCheckListObj.IsInactive;
                                matchedPreflightCheckList.LastUpdUID = tripManagerCheckListObj.LastUpdUID;
                                preflightCheckListNew.Add(matchedPreflightCheckList);
                            }
                            else
                            {
                                PreflightChecklistViewModel newPreflightCheckListItem = new PreflightChecklistViewModel();
                                newPreflightCheckListItem.CheckGroupCD = tripManagerCheckListObj.CheckGroupCD;
                                newPreflightCheckListItem.CheckGroupDescription = preflightLegObj.CheckGroup;
                                newPreflightCheckListItem.CheckGroupID = tripManagerCheckListObj.CheckGroupID;
                                newPreflightCheckListItem.CheckListDescription = tripManagerCheckListObj.CheckListDescription;
                                newPreflightCheckListItem.CheckListID = tripManagerCheckListObj.CheckListID;
                                newPreflightCheckListItem.ComponentCD = tripManagerCheckListObj.CheckListCD;
                                newPreflightCheckListItem.CustomerID = tripManagerCheckListObj.CustomerID;
                                newPreflightCheckListItem.IsCompleted = false;
                                newPreflightCheckListItem.IsDeleted = false;
                                newPreflightCheckListItem.IsInactive = tripManagerCheckListObj.IsInactive;
                                newPreflightCheckListItem.LastUpdUID = tripManagerCheckListObj.LastUpdUID;
                                newPreflightCheckListItem.LegID = preflightLegObj.LegID;
                                newPreflightCheckListItem.PreflightCheckListID = 0;
                                newPreflightCheckListItem.State = preflightLegObj.State;
                                preflightCheckListNew.Add(newPreflightCheckListItem);
                            }
                        }
                    }
                    preflightLegObj.PreflightChecklist = preflightCheckListNew;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = tripObj;
                }
            }
        }

        /// <summary>
        /// To check whether the Already the Passenger exists
        /// </summary>
        /// <returns></returns>
        //private bool checkAllReadyExist()
        //{

        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        bool returnVal = false;
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {

        //                using (FlightPakMasterService.MasterCatalogServiceClient objTripsvc = new FlightPakMasterService.MasterCatalogServiceClient())
        //                {
        //                    var objRetVal = objTripsvc.GetTripManagerChecklistListInfo().EntityList.Where(x => x.CheckListCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
        //                    if (objRetVal.Count() > 0 && objRetVal != null)
        //                    {
        //                        cvCode.IsValid = false;
        //                        tbCode.Focus();
        //                        returnVal = true;
        //                    }
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);

        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
        //        }
        //        return returnVal;
        //    }

        //}

        /// <summary>
        /// Update Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckList_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        //if (checkAllReadyExist())
                        //{
                        //    cvCode.IsValid = false;
                        //    tbCode.Focus();
                        //    IsValidate = false;
                        //}
                        if (CheckAlreadyGroupCodeExist())
                        {
                            cvGroup.IsValid = false;
                            tbGroup.Focus();
                            IsValidate = false;

                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objService.AddTripManagerChecklist(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    dgTripManagerCheckList.Rebind();
                                    GridEnable(true, true, true);
                                    DefaultSelection(false);
                                    
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                    ResetChecklistSession();
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.TripManagerCheckList);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckList_DeleteCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["SelectedChecklistID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient TripChecklistService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.TripManagerCheckList TripChecklist = new FlightPakMasterService.TripManagerCheckList();
                                    string Code = Session["SelectedChecklistID"].ToString();
                                    strTripManagerChecklistCD = "";
                                    strTripManagerChecklistID = "";
                                    foreach (GridDataItem Item in dgTripManagerCheckList.MasterTableView.Items)
                                    {
                                        if (Item["CheckListID"].Text.Trim() == Code.Trim())
                                        {
                                            strTripManagerChecklistCD = Item["CheckListCD"].Text.Trim();
                                            strTripManagerChecklistID = Item["CheckListID"].Text.Trim();
                                            TripChecklist.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                            break;
                                        }

                                    }
                                    TripChecklist.CheckListCD = strTripManagerChecklistCD;
                                    TripChecklist.CheckListID = Convert.ToInt64(strTripManagerChecklistID);
                                    TripChecklist.IsDeleted = true;
                                    
                                    var returnValue = CommonService.Lock(EntitySet.Database.TripManagerCheckList, Convert.ToInt64(strTripManagerChecklistID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.TripManagerCheckList);

                                        return;
                                    }
                                    TripChecklistService.DeleteTripManagerChecklist(TripChecklist);
                                    //var returnValue1 = CommonService.UnLock(EntitySet.Database.TripManagerCheckList, Convert.ToInt64(strTripManagerChecklistID));
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgTripManagerCheckList.Rebind();
                                    DefaultSelection(false);
                                    _selectLastModified = true;
                                    // GridEnable(true, true, true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                    }
                    finally
                    {
                        // For Positive and negative cases need to unlock the record
                        // The same thing should be done for Edit
                        if (Session["SelectedChecklistID"] != null)
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.Crew, Convert.ToInt64(Session["SelectedChecklistID"].ToString().Trim()));
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TripManagerCheckList_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgTripManagerCheckList.SelectedItems[0] as GridDataItem;
                                Session["SelectedChecklistID"] = item["CheckListID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                    }
                }
            }
        }

        protected void TripManagerCheckList_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgTripManagerCheckList.ClientSettings.Scrolling.ScrollTop = "0";
            TripManagerChecklistPageNavigated = true;
        }

        protected void TripManagerCheckList_PreRender(object sender, EventArgs e)
        {
            //GridEnable(true, true, true);
            if (hdnSave.Value != "Save")
            {
                SelectItem();
            }
            else if (TripManagerChecklistPageNavigated)
            {
                SelectItem();
            }
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgTripManagerCheckList, Page.Session);
        }
        protected void CheckAlreadyGroupCodeExist_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAlreadyGroupCodeExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }
        protected void SaveChanges_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgTripManagerCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgTripManagerCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedChecklistID"] != null)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.TripManagerCheckList, Convert.ToInt64(Session["SelectedChecklistID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        private TripManagerCheckList GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.TripManagerCheckList TripChecklist = null;
                try
                {
                    TripChecklist = new FlightPakMasterService.TripManagerCheckList();
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedChecklistID"] != null)
                            {
                                TripChecklist.CheckListID = Convert.ToInt64(Session["SelectedChecklistID"].ToString().Trim());
                            }
                        }
                        else
                        {
                            TripChecklist.CheckListID = 0;
                        }
                        TripChecklist.CheckListCD = tbCode.Text;
                        TripChecklist.CheckListDescription = tbDescription.Text;
                        if (!string.IsNullOrEmpty(tbGroup.Text.Trim()))
                        {
                            TripChecklist.CheckGroupID = Convert.ToInt64(hdnChecklistGroupCode.Value);
                        }
                        TripChecklist.IsDeleted = false;
                        TripChecklist.IsInActive = chkInactive.Checked;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
                return TripChecklist;
            }
        }

        protected void DisplayEditForm()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LoadControlData();

                        hdnSave.Value = "Update";
                        hdnRedirect.Value = "";
                        EnableForm(true);
                        tbCode.Enabled = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        /// <summary>
        /// Grid enable event
        /// </summary>
        /// <param name="add"></param>
        /// <param name="edit"></param>
        /// <param name="delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgTripManagerCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgTripManagerCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgTripManagerCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        if (IsAuthorized(Permission.Database.AddTripManagerCheckListCatalog))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                            {
                                lbtnInsertCtl.Enabled = true;
                            }
                            else
                            {
                                lbtnInsertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.DeleteTripManagerCheckListCatalog))
                        {
                            lbtnDelCtl.Visible = true;
                            if (Delete)
                            {
                                lbtnDelCtl.Enabled = true;
                                lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete('Deleting check list code will remove code from all the trip sheet check lists');";
                            }
                            else
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.EditTripManagerCheckListCatalog))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                            {
                                lbtnEditCtl.Enabled = true;
                                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnEditCtl.Enabled = false;
                                lbtnEditCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        ///// <summary>
        ///// To check unique Code  on Tab Out
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void Code_TextChanged(object sender, EventArgs e)
        //{

        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
        //                {
        //                    checkAllReadyExist();
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);

        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
        //        }
        //    }

        //}

        /// <summary>
        /// Read only form
        /// </summary>
        protected void ReadOnlyForm()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTripManagerCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgTripManagerCheckList.SelectedItems[0] as GridDataItem;

                            Label lbLastUpdatedUser = (Label)dgTripManagerCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");

                            if (Item.GetDataKeyValue("LastUpdUID") != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                            }
                            else
                            {
                                lbLastUpdatedUser.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("LastUpdTS") != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                            }
                            LoadControlData();
                            EnableForm(false);
                        }
                        else
                        {
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedChecklistID"] != null)
                    {
                        strTripManagerChecklistCD = "";
                        strTripManagerChecklistID = "";
                        string strCheckListName = "";
                        string strGroupName = "";
                        foreach (GridDataItem Item in dgTripManagerCheckList.MasterTableView.Items)
                        {
                            if (Item["CheckListID"].Text.Trim() == Session["SelectedChecklistID"].ToString().Trim())
                            {
                                strTripManagerChecklistCD = Item["CheckListCD"].Text.Trim();
                                strTripManagerChecklistID = Item["CheckListID"].Text.Trim();
                                if (Item.GetDataKeyValue("CheckListDescription") != null)
                                {
                                    strCheckListName = Item["CheckListDescription"].Text.Trim();
                                }
                                else
                                {
                                    strCheckListName = null;
                                }
                                if (Item.GetDataKeyValue("CheckGroupCD") != null)
                                {
                                    strGroupName = Item["CheckGroupCD"].Text.Trim();
                                }
                                else
                                {
                                    strGroupName = null;
                                }
                                if (Item.GetDataKeyValue("CheckGroupID") != null)
                                {
                                    hdnGroupId.Value = Item.GetDataKeyValue("CheckGroupID").ToString();
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Checklist Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["CheckListCD"].Text;
                                lbColumnValue2.Text = Item["CheckListDescription"].Text;
                                break;
                            }
                        }
                        tbCode.Text = strTripManagerChecklistCD;
                        if (strCheckListName != null)
                        {
                            tbDescription.Text = strCheckListName;
                        }
                        else
                        {
                            tbDescription.Text = string.Empty;
                        }
                        if (strGroupName != null)
                        {
                            tbGroup.Text = strGroupName;
                        }
                        else
                        {
                            tbGroup.Text = string.Empty;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Clear form
        /// </summary>
        protected void ClearForm()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbCode.Text = string.Empty;
                        tbDescription.Text = string.Empty;
                        tbGroup.Text = string.Empty;
                        hdnGroup.Value = string.Empty;
                        hdnChecklistGroupCode.Value = string.Empty;
                        hdnIsEdit.Value = string.Empty;
                        chkInactive.Checked = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        /// <summary>
        /// Enable form
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbCode.Enabled = enable;
                        tbDescription.Enabled = enable;
                        tbGroup.Enabled = enable;
                        btnCancel.Visible = enable;
                        btnSaveChanges.Visible = enable;
                        btnGroup.Enabled = enable;
                        chkInactive.Enabled = enable; 
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }

        }

        protected void Group_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbGroup.Text != null)
                        {
                            CheckAlreadyGroupCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }
        }

        private bool CheckAlreadyGroupCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;

                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (tbGroup.Text != null && tbGroup.Text.Trim() != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetValue = ObjService.GetTripManagerChecklistGroupList().EntityList.Where(x => x.CheckGroupCD.Trim().ToUpper().Equals(tbGroup.Text.Trim().ToUpper())).ToList();
                                if (objRetValue.Count() == 0 || objRetValue == null)
                                {
                                    cvGroup.IsValid = false;
                                    cvGroup.ErrorMessage = "Invalid Group Code.";
                                    tbGroup.Focus();
                                    RetVal = true;
                                }
                                else
                                {
                                    hdnChecklistGroupCode.Value = ((FlightPak.Web.FlightPakMasterService.TripManagerCheckListGroup)objRetValue[0]).CheckGroupID.ToString();
                                    tbGroup.Text = ((FlightPak.Web.FlightPakMasterService.TripManagerCheckListGroup)objRetValue[0]).CheckGroupCD;
                                }
                                if (RetVal == false && hdnIsEdit.Value == "false")
                                {

                                    var objRetVal = ObjService.GetTripManagerChecklistListInfo().EntityList.Where(x => x.CheckGroupCD.Trim().ToUpper().Equals(tbGroup.Text.Trim().ToUpper()) && (x.CheckListCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()))).ToList();
                                    if (objRetVal.Count() > 0 && hdnIsEdit.Value == "false")
                                    {
                                        cvGroup.IsValid = false;
                                        cvGroup.ErrorMessage = "Checklist Group and Code combination must be unique.";
                                        tbGroup.Focus();
                                        RetVal = true;
                                    }
                                    else
                                    {
                                        RetVal = false;
                                        hdnChecklistGroupCode.Value = ((FlightPak.Web.FlightPakMasterService.TripManagerCheckListGroup)objRetValue[0]).CheckGroupID.ToString();
                                    }
                                }
                                else
                                {
                                    if (RetVal == false && hdnIsEdit.Value == "true")
                                    {
                                        var objRetVals = ObjService.GetTripManagerChecklistListInfo().EntityList.Where(x => x.CheckGroupID!= Convert.ToInt64(hdnGroupId.Value)).ToList();
                                        var objRetVal = objRetVals.Where(x => x.CheckGroupCD.Trim().ToUpper().Equals(tbGroup.Text.Trim().ToUpper()) && (x.CheckListCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()))).ToList();


                                        if (objRetVal.Count() > 0 && hdnIsEdit.Value == "true")
                                        {
                                            cvGroup.IsValid = false;
                                            cvGroup.ErrorMessage = "Checklist Group and Code combination must be unique.";
                                            tbGroup.Focus();
                                            RetVal = true;
                                        }
                                        else
                                        {
                                            RetVal = false;
                                            hdnChecklistGroupCode.Value = ((FlightPak.Web.FlightPakMasterService.TripManagerCheckListGroup)objRetValue[0]).CheckGroupID.ToString();
                                        }

                                    }
                                }

                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
                return RetVal;
            }

        }

        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TripManagerCheckList);
                }
            }
        }
        #endregion

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                        GridEnable(true, true, true);
                        _selectLastModified = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllTripManagerCheckList> lstTripManagerCheckList = new List<FlightPakMasterService.GetAllTripManagerCheckList>();
                if (Session["ChecklistCD"] != null)
                {
                    lstTripManagerCheckList = (List<FlightPakMasterService.GetAllTripManagerCheckList>)Session["ChecklistCD"];
                }
                if (lstTripManagerCheckList.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstTripManagerCheckList = lstTripManagerCheckList.Where(x => x.IsInactive == false).ToList<GetAllTripManagerCheckList>(); }
                    dgTripManagerCheckList.DataSource = lstTripManagerCheckList;
                    if (IsDataBind)
                    {
                        dgTripManagerCheckList.DataBind();                        
                    }
                }
                LoadControlData();                
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgTripManagerCheckList.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var ChecklistValue = FPKMstService.GetTripManagerChecklistListInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, ChecklistValue);
            List<FlightPakMasterService.GetAllTripManagerCheckList> filteredList = GetFilteredList(ChecklistValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CheckListID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgTripManagerCheckList.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgTripManagerCheckList.CurrentPageIndex = PageNumber;
            dgTripManagerCheckList.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllTripManagerCheckList ChecklistValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedChecklistID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = ChecklistValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().CheckListID;
                Session["SelectedChecklistID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllTripManagerCheckList> GetFilteredList(ReturnValueOfGetAllTripManagerCheckList ChecklistValue)
        {
            List<FlightPakMasterService.GetAllTripManagerCheckList> filteredList = new List<FlightPakMasterService.GetAllTripManagerCheckList>();

            if (ChecklistValue.ReturnFlag)
            {
                filteredList = ChecklistValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInactive == false).ToList<GetAllTripManagerCheckList>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgTripManagerCheckList.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgTripManagerCheckList.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

    }
}