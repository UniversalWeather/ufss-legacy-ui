﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TripsheetRWGrouppopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Company.TripsheetRWGrouppopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <title>Select Tripsheet RW Group</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openWin(radWin) {
                var url = '';
                if (radWin == "RadFilterClientPopup") {
                    url = '../Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbClientCodeFilter.ClientID%>').value;
                }
                else
                    if (radWin == "RadFilterUserPopup") {
                        url = '../Company/UserGrouppopup.aspx?UserGroupCD=' + document.getElementById('<%=tbUserGroup.ClientID%>').value;
                    }
                var oWnd = radopen(url, radWin);
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            var oArg = new Object();
            var grid = $find("<%= dgRWGroup.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgRWGroup.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "ReportID");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "TripSheetReportHeaderID")
                }

                if (selectedRows.length > 0) {
                    oArg.ReportID = cell1.innerHTML;
                    oArg.TripSheetReportHeaderID = cell2.innerHTML;
                }
                else {
                    oArg.ReportID = "";
                    oArg.TripSheetReportHeaderID = "";
                }


                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function OnClientCloseClientPopup(oWnd, args) {
                var combo = $find("<%= tbClientCodeFilter.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=cvClientCodeFilter.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = "";
                        document.getElementById("<%=cvClientCodeFilter.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseUserPopup(oWnd, args) {
                var combo = $find("<%= tbUserGroup.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbUserGroup.ClientID%>").value = arg.UserGroupCD;
                        document.getElementById("<%=cvUserGroup.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbUserGroup.ClientID%>").value = "";
                        document.getElementById("<%=cvUserGroup.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel" id="DivExternalForm" runat="server">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgRWGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgRWGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkHomeBaseOnly">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="chkHomeBaseOnly" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbClientCodeFilter">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbClientCodeFilter" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="lbClientCodeFilter" LoadingPanelID="RadAjaxLoadingPanel1" />
                         <telerik:AjaxUpdatedControl ControlID="cvClientCodeFilter" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbUserGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbUserGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                         <telerik:AjaxUpdatedControl ControlID="cvUserGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnFilter">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgRWGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadFilterClientPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseClientPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadFilterUserPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseUserPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/UserGrouppopup.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td class="tdLabel130">
                    <asp:CheckBox ID="chkHomeBaseOnly" runat="server" Checked="false" Text="Home Base Only"
                        OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" AutoPostBack="true" />
                </td>
                <td>
                    <table id="Table1" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="tdLabel80">
                                <asp:Label runat="server" ID="lbClientCode">Client Code :</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tbClientCodeFilter" runat="server" MaxLength="5" onkeypress="return fnAllowAlphaNumeric(this, event)"
                                    onblur="return RemoveSpecialChars(this)" OnTextChanged="FilterByClient_OnTextChanged"
                                    AutoPostBack="true" CssClass="text50"></asp:TextBox>
                            </td>
                            <td class="tdLabel50">
                                <asp:Button ID="btnClientCodeFilter" OnClientClick="javascript:openWin('RadFilterClientPopup');return false;"
                                    CssClass="browse-button" runat="server" />
                            </td>
                            <td class="tdLabel80">
                                <asp:Label runat="server" ID="Label2">User Group :</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tbUserGroup" runat="server" MaxLength="5" onkeypress="return fnAllowAlphaNumeric(this, event)"
                                    onblur="return RemoveSpecialChars(this)" OnTextChanged="FilterByUserGroup_OnTextChanged"
                                    AutoPostBack="true" CssClass="text50"></asp:TextBox>
                            </td>
                            <td class="tdLabel80">
                                <asp:Button ID="btnUserGroup" OnClientClick="javascript:openWin('RadFilterUserPopup');return false;"
                                    CssClass="browse-button" runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="btnFilter" Text="Search" runat="server" OnClick="FilterByCheckbox_OnCheckedChanged"
                                    CssClass="button" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Label runat="server" ID="lbClientCodeFilter" Text="Invalid ClientCode" Visible="false"
                                    CssClass="alert-text"></asp:Label>
                                <asp:CustomValidator ID="cvClientCodeFilter" runat="server" ControlToValidate="tbClientCodeFilter"
                                    ErrorMessage="Invalid ClientCode" Display="Dynamic" CssClass="alert-text"></asp:CustomValidator>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label runat="server" ID="Label1" Text="Invalid UserGroup" Visible="false" CssClass="alert-text"></asp:Label>
                                <asp:CustomValidator ID="cvUserGroup" runat="server" ControlToValidate="tbUserGroup"
                                    ErrorMessage="Invalid UserGroup" Display="Dynamic" CssClass="alert-text"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="tblspace_10">
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <telerik:RadGrid ID="dgRWGroup" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
                        OnNeedDataSource="dgRWGroup_BindData" AutoGenerateColumns="false" Height="400px" 
                        PageSize="10" AllowPaging="true" Width="800px" PagerStyle-AlwaysVisible="true"
                        OnItemCommand="dgRWGroup_ItemCommand" OnPreRender="dgRWGroup_PreRender">
                        <MasterTableView DataKeyNames="TripSheetReportHeaderID,ReportID,ReportDescription,HomebaseID,UserGroupID,ClientID,HomebaseCD,ClientCD,UserGroupCD,IsDeleted"
                            CommandItemDisplay="Bottom">
                            <Columns>
                                <telerik:GridBoundColumn DataField="ReportID" HeaderText="Report ID" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px"
                                    FilterControlWidth="100px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ReportDescription" HeaderText="Description" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="380px"
                                    FilterControlWidth="360px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="UserGroupCD" HeaderText="User Group" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ClientCD" HeaderText="Client" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TripSheetReportHeaderID" HeaderText="TripSheetReportHeaderID"
                                    Display="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                        Ok</button>
                                    <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
