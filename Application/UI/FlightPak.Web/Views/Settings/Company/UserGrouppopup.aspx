﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserGrouppopup.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Company.UserGrouppopup" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Group</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgUserGroup.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgUserGroup.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "UserGroupCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "UserGroupDescription")
                }

                if (selectedRows.length > 0) {
                    oArg.UserGroupCD = cell1.innerHTML;
                    oArg.UserGroupDescription = cell2.innerHTML;
                }
                else {
                    oArg.UserGroupCD = "";
                    oArg.UserGroupDescription = "";
                }


                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgUserGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgUserGroup" LoadingPanelID="RadAjaxLoadingPanel1" />                      
                    </UpdatedControls>
                </telerik:AjaxSetting>               
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgUserGroup" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgUserGroup" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
            OnNeedDataSource="dgUserGroup_BindData" AutoGenerateColumns="false" Height="300px"
            PageSize="10" AllowPaging="true" Width="600px" PagerStyle-AlwaysVisible="true"
            OnPreRender="dgUserGroup_PreRender" OnItemCommand="dgUserGroup_ItemCommand">
            <MasterTableView DataKeyNames="UserGroupCD,UserGroupDescription" CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="UserGroupCD" HeaderText="Group Code" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                        FilterControlWidth="80px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="UserGroupDescription" HeaderText="Group Description"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="380px" FilterControlWidth="360px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;" >
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button okButton">
                            OK</button>
                        <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick = "returnToParent" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
             <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>

