﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Threading;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class ClientCodeCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private List<string> lstClientCodeCodes = new List<string>();
        private ExceptionManager exManager;
        private bool ClientCodePageNavigated = false;
        private bool _selectLastModified = false;

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgClientCode, dgClientCode, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgClientCode.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewClientCode);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgClientCode.Rebind();
                                ReadOnlyForm();                                
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                        if (IsPopUp)
                        {
                            dgClientCode.AllowPaging = false;
                            chkSearchActiveOnly.Visible = false;
                            Session["ClientID"] = Request.QueryString["ClientCodeId"];
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (BindDataSwitch)
                {
                    dgClientCode.Rebind();
                }
                if (dgClientCode.MasterTableView.Items.Count > 0)
                {
                    //if (!IsPostBack)
                    //{
                    //    Session["ClientID"] = null;
                    //}
                    if (Session["ClientID"] == null)
                    {
                        dgClientCode.SelectedIndexes.Add(0);
                        if (!IsPopUp)
                            Session["ClientID"] = dgClientCode.Items[0].GetDataKeyValue("ClientID").ToString();
                    }

                    if (dgClientCode.SelectedIndexes.Count == 0)
                        dgClientCode.SelectedIndexes.Add(0);

                    ReadOnlyForm();
                    GridEnable(true, true, true);
                }
                else
                {
                    ClearForm();
                    EnableForm(false);
                }
            }
        }

        protected void Budget_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            dgClientCode.ClientSettings.Scrolling.ScrollTop = "0";
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["ClientID"] != null)
                {
                    string ID = Session["ClientID"].ToString();
                    foreach (GridDataItem Item in dgClientCode.MasterTableView.Items)
                    {
                        if (Item["ClientID"].Text.Trim() == ID.Trim())
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    DefaultSelection(false);
                }
            }
        }
        /// <summary>
        /// Grid enable event
        /// </summary>
        /// <param name="add"></param>
        /// <param name="edit"></param>
        /// <param name="delete"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, delCtl, editCtl;
                insertCtl = (LinkButton)dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                delCtl = (LinkButton)dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                editCtl = (LinkButton)dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                if (IsAuthorized(Permission.Database.AddClientCode))
                {
                    insertCtl.Visible = true;
                    if (add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteClientCode))
                {
                    delCtl.Visible = true;
                    if (delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = "";
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditClientCode))
                {
                    editCtl.Visible = true;
                    if (edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = "";
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //pnlExternalForm.Visible = true;
                hdnSave.Value = "Save";
                tbCode.Text = string.Empty;
                hdnClientID.Value = string.Empty;
                tbDescription.Text = string.Empty;
                tbCode.BackColor = System.Drawing.Color.White;
                tbCode.ReadOnly = false;
                ClearForm();
                EnableForm(true);
                btnCancelTop.Visible = true;
                btnSaveChangesTop.Visible = true;
                btnCancel.Visible = true;
                btnSaveChanges.Visible = true;
                LoadAllListBox(0);
            }
        }
        /// <summary>
        /// Client Code Catalog Edit form
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCode.ReadOnly = true;
                tbCode.Enabled = false;
                tbCode.BackColor = System.Drawing.Color.LightGray;
                hdnSave.Value = "Update";
                hdnRedirect.Value = "";
                LoadControlData();
                EnableForm(true);
            }
        }
        /// <summary>
        /// Enable form
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                tbCode.Enabled = enable;
                tbDescription.Enabled = enable;
                lstUserAvailable.Enabled = enable;
                lstUserSelected.Enabled = enable;
                lstFleetAvailable.Enabled = enable;
                lstFleetSelected.Enabled = enable;
                lstCrewAvailable.Enabled = enable;
                lstCrewSelected.Enabled = enable;
                lstPaxAvailable.Enabled = enable;
                lstPaxSelected.Enabled = enable;
                lstDeptAvailable.Enabled = enable;
                lstDeptSelected.Enabled = enable;
                btnCrewAdd.Enabled = enable;
                btnCrewAddAll.Enabled = enable;
                btnCrewRemove.Enabled = enable;
                btnCrewRemoveAll.Enabled = enable;
                btnDeptAdd.Enabled = enable;
                btnDeptAddAll.Enabled = enable;
                btnDeptRemove.Enabled = enable;
                btnDeptRemoveAll.Enabled = enable;
                btnFleetAdd.Enabled = enable;
                btnFleetAddAll.Enabled = enable;
                btnFleetRemove.Enabled = enable;
                btnFleetRemoveAll.Enabled = enable;
                btnPaxAdd.Enabled = enable;
                btnPaxAddAll.Enabled = enable;
                btnPaxRemove.Enabled = enable;
                btnPaxRemoveAll.Enabled = enable;
                btnUserAdd.Enabled = enable;
                btnUserAddAll.Enabled = enable;
                btnUserRemove.Enabled = enable;
                btnUserRemoveAll.Enabled = enable;

                btnFlightCategoryAdd.Enabled = enable;
                btnFlightCategoryAddAll.Enabled = enable;
                btnFlightCategoryRemove.Enabled = enable;
                btnFlightCategoryRemoveAll.Enabled = enable;

                btnFlightPurposeAdd.Enabled = enable;
                btnFlightPurposeAddAll.Enabled = enable;
                btnFlightPurposeRemove.Enabled = enable;
                btnFlightPurposeRemoveAll.Enabled = enable;

                btnFuelLocatorAdd.Enabled = enable;
                btnFuelLocatorAddAll.Enabled = enable;
                btnFuelLocatorRemove.Enabled = enable;
                btnFuelLocatorRemoveAll.Enabled = enable;

                btnCancelTop.Visible = enable;
                btnSaveChangesTop.Visible = enable;
                btnCancel.Visible = enable;
                btnSaveChanges.Visible = enable;

                chkInactive.Enabled = enable; 
            }
        }
        /// <summary>
        /// Clear form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCode.Text = string.Empty;
                hdnClientID.Value = string.Empty;
                tbDescription.Text = string.Empty;
                chkInactive.Checked = false;
            }
        }
        /// <summary>
        /// Read only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LoadControlData();
                EnableForm(false);
            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["ClientID"] != null)
                {
                   
                    FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient();
                    var ClientCodeValue = FPKMstService.GetClientCodeList(); //getting client all data

                    foreach (var Item in ClientCodeValue.EntityList)
                    {
                      
                        if (Item.ClientID.ToString()== Session["ClientID"].ToString().Trim())
                        {
                            hdnClientID.Value = Item.ClientID.ToString();
                            tbCode.Text = HttpUtility.HtmlDecode(Item.ClientCD.ToString());
                            if (Item.ClientDescription!= null)
                            {
                                tbDescription.Text = Item.ClientDescription.ToString();
                            }
                            else
                            {
                                tbDescription.Text = string.Empty;
                            }
                            // For loading selected client code details - AJSS                            
                            LoadAllListBox(Convert.ToInt64(Item.ClientID.ToString()));

                            if (dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem).Count() > 0)
                            {
                                Label lbLastUpdatedUser = lbLastUpdatedUser = (Label)dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.LastUpdUID != null)
                                {
                                    lbLastUpdatedUser.Text = HttpUtility.HtmlEncode("Last Updated User: " + Item.LastUpdUID.ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.LastUpdTS != null)
                                {
                                    lbLastUpdatedUser.Text = HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.LastUpdTS.ToString())));//item.GetDataKeyValue("LastUpdTS").ToString();
                                }
                            }
                            if (Item.IsInActive!= null)
                            {
                                chkInactive.Checked = Convert.ToBoolean(Item.IsInActive.ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                chkInactive.Checked = false;
                            }
                            lbColumnName1.Text = "Client Code";
                            lbColumnName2.Text = "Description";
                            lbColumnValue1.Text = Microsoft.Security.Application.Encoder.HtmlEncode(Item.ClientCD);
                            lbColumnValue2.Text = Microsoft.Security.Application.Encoder.HtmlEncode(Item.ClientDescription);
                            break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Client Code Catalog Get items method
        /// </summary>
        /// <param name="PassengerAdditionalInfoCode"></param>
        /// <returns></returns>
        private FlightPakMasterService.Client GetItems(FlightPakMasterService.Client objClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (hdnSave.Value == "Update")
                {
                    objClientCode.ClientID = Convert.ToInt64(hdnClientID.Value); //Convert.ToInt64(item.GetDataKeyValue("ClientID").ToString());
                }
                if (!string.IsNullOrEmpty(tbCode.Text))
                {
                    objClientCode.ClientCD = tbCode.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbDescription.Text))
                {
                    objClientCode.ClientDescription = tbDescription.Text;
                }
                objClientCode.IsInActive = chkInactive.Checked;
                objClientCode.IsDeleted = false;
                return objClientCode;
            }
        }

        #region "Grid events"
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void dgClientCode_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        /// <summary>
        /// Bind Client Code Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgClientCode_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objClientCodeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPakMasterService.Client> ClientList = new List<Client>();
                            var objClientCodeVal = objClientCodeService.GetClientCodeList();
                            if (objClientCodeVal.ReturnFlag == true)
                            {
                                ClientList = objClientCodeVal.EntityList;
                            }
                            dgClientCode.DataSource = ClientList;
                            Session["ClientCodeCodes"] = ClientList;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        /// <summary>
        /// Item Command for Client Code Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgClientCode_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Client, Convert.ToInt64(Session["ClientID"].ToString().Trim()));
                                    Session["IsClientCodeEditLock"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Client);
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    SelectItem();
                                    DisableLinks();
                                    RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgClientCode.SelectedIndexes.Clear();
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                tbCode.ReadOnly = false;
                                tbCode.BackColor = System.Drawing.Color.White;
                                DisplayInsertForm();                                
                                GridEnable(true, false, false);
                                DisableLinks();
                                //clearing label at top black bar
                                lbColumnValue1.Text = string.Empty;
                                lbColumnValue2.Text = string.Empty;
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    ////column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        /// <summary>
        /// Update Command for Client Code Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgClientCode_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["ClientID"] != null)
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Client objClientCode = new FlightPakMasterService.Client();
                                objClientCode = GetItems(objClientCode);
                                var Result = objService.UpdateClientCode(objClientCode);
                                if (Result.ReturnFlag == true)
                                {
                                    string UserMaster = string.Empty, Fleet = string.Empty, Crew = string.Empty, Passenger = string.Empty, Department = string.Empty;
                                    string FlightCategory = string.Empty, FlightPurpose = string.Empty, FuelLocator = string.Empty;
                                    UserMaster = UpdateClientCodeInUserMaster(lstUserSelected);
                                    Fleet = UpdateClientCodeInFleet(lstFleetSelected);
                                    Crew = UpdateClientCodeInCrew(lstCrewSelected);
                                    Passenger = UpdateClientCodeInPassenger(lstPaxSelected);
                                    Department = UpdateClientCodeInDepartment(lstDeptSelected);
                                    FlightCategory = UpdateClientCodeInFlightCategory(lstFlightCategorySelected);
                                    FlightPurpose = UpdateClientCodeInFlightPurpose(lstFlightPurposeSelected);
                                    FuelLocator = UpdateClientCodeInFuelLocator(lstFuelLocatorSelected);
                                    objService.UpdateClientCodeAll(objClientCode.ClientID, UserMaster, Fleet, Crew, Passenger, Department, FlightCategory, FlightPurpose, FuelLocator);

                                    UserMaster = UpdateClientCodeInUserMaster(lstUserAvailable);
                                    Fleet = UpdateClientCodeInFleet(lstFleetAvailable);
                                    Crew = UpdateClientCodeInCrew(lstCrewAvailable);
                                    Passenger = UpdateClientCodeInPassenger(lstPaxAvailable);
                                    Department = UpdateClientCodeInDepartment(lstDeptAvailable);
                                    FlightCategory = UpdateClientCodeInFlightCategory(lstFlightCategoryAvailable);
                                    FlightPurpose = UpdateClientCodeInFlightPurpose(lstFlightPurposeAvailable);
                                    FuelLocator = UpdateClientCodeInFuelLocator(lstFuelLocatorAvailable);
                                    objService.UpdateClientCodeAll(-1, UserMaster, Fleet, Crew, Passenger, Department, FlightCategory, FlightPurpose, FuelLocator);


                                    ShowSuccessMessage();
                                    // Update Session when there is a change in UserPrincipal
                                    RefreshSession();
                                    EnableLinks();

                                    // Unlock the Record
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.Client, Convert.ToInt64(Session["ClientID"].ToString().Trim()));
                                    }
                                    Session["IsClientCodeEditLock"] = "False";
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    GridEnable(true, true, true);
                                    dgClientCode.Rebind();
                                    SelectItem();
                                    ReadOnlyForm();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.Client);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }               
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        /// <summary>
        /// Insert Command for Client Code Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgClientCode_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAlreadyExist())
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Client objClientCode = new FlightPakMasterService.Client();

                                objClientCode = GetItems(objClientCode);
                                var Result = objService.AddClientCode(objClientCode);

                                // this value will be used to show newly added value in same page
                                //ViewState["addedClientID"] = Convert.ToString(Result.EntityInfo.ClientID);
                             
                                

                                if (Result.ReturnFlag == true)
                                {
                                    //List<FlightPakMasterService.Client> ClientList = new List<Client>();
                                    //ClientList = objService.GetClientCodeList().EntityList.Where(x => x.ClientCD.Trim().ToUpper().ToString() == objClientCode.ClientCD.Trim().ToUpper().ToString() &&
                                    //                                                            x.ClientDescription.Trim().ToUpper().ToString() == objClientCode.ClientDescription.Trim().ToUpper().ToString()).ToList();
                                    //if (ClientList.Count != 0)
                                    //{
                                    //    objClientCode.ClientID = ClientList[0].ClientID;
                                    //}
                                    objClientCode.ClientID = Result.EntityInfo.ClientID;

                                    string UserMaster = string.Empty, Fleet = string.Empty, Crew = string.Empty, Passenger = string.Empty, Department = string.Empty;
                                    string FlightCategory = string.Empty, FlightPurpose = string.Empty, FuelLocator = string.Empty;
                                    UserMaster = UpdateClientCodeInUserMaster(lstUserSelected);
                                    Fleet = UpdateClientCodeInFleet(lstFleetSelected);
                                    Crew = UpdateClientCodeInCrew(lstCrewSelected);
                                    Passenger = UpdateClientCodeInPassenger(lstPaxSelected);
                                    Department = UpdateClientCodeInDepartment(lstDeptSelected);
                                    FlightCategory = UpdateClientCodeInFlightCategory(lstFlightCategorySelected);
                                    FlightPurpose = UpdateClientCodeInFlightPurpose(lstFlightPurposeSelected);
                                    FuelLocator = UpdateClientCodeInFuelLocator(lstFuelLocatorSelected);
                                    objService.UpdateClientCodeAll(objClientCode.ClientID, UserMaster, Fleet, Crew, Passenger, Department, FlightCategory, FlightPurpose, FuelLocator);

                                    UserMaster = UpdateClientCodeInUserMaster(lstUserAvailable);
                                    Fleet = UpdateClientCodeInFleet(lstFleetAvailable);
                                    Crew = UpdateClientCodeInCrew(lstCrewAvailable);
                                    Passenger = UpdateClientCodeInPassenger(lstPaxAvailable);
                                    Department = UpdateClientCodeInDepartment(lstDeptAvailable);
                                    FlightCategory = UpdateClientCodeInFlightCategory(lstFlightCategoryAvailable);
                                    FlightPurpose = UpdateClientCodeInFlightPurpose(lstFlightPurposeAvailable);
                                    FuelLocator = UpdateClientCodeInFuelLocator(lstFuelLocatorAvailable);
                                    objService.UpdateClientCodeAll(-1, UserMaster, Fleet, Crew, Passenger, Department, FlightCategory, FlightPurpose, FuelLocator);
                                    
                                    ShowSuccessMessage();

                                    GridEnable(true, true, true);
                                    DefaultSelection(true);
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.Client);
                                }                                
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }                
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgClientCode_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["ClientID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objClientCodeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Client objClientCode = new FlightPakMasterService.Client();
                                GridDataItem item = dgClientCode.SelectedItems[0] as GridDataItem;
                                objClientCode.ClientID = Convert.ToInt64((item).GetDataKeyValue("ClientID").ToString());
                                objClientCode.ClientCD = (item).GetDataKeyValue("ClientCD").ToString();
                                objClientCode.ClientDescription = (item).GetDataKeyValue("ClientDescription").ToString();
                                objClientCode.IsDeleted = true;

                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Client, Convert.ToInt64(Session["ClientID"].ToString().Trim()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Client);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Client);
                                        return;
                                    }
                                }
                                objClientCodeService.DeleteClientCode(objClientCode);

                                string UserMaster = string.Empty, Fleet = string.Empty, Crew = string.Empty, Passenger = string.Empty, Department = string.Empty;
                                string FlightCategory = string.Empty, FlightPurpose = string.Empty, FuelLocator = string.Empty;
                                UserMaster = UpdateClientCodeInUserMaster(lstUserSelected);
                                Fleet = UpdateClientCodeInFleet(lstFleetSelected);
                                Crew = UpdateClientCodeInCrew(lstCrewSelected);
                                Passenger = UpdateClientCodeInPassenger(lstPaxSelected);
                                Department = UpdateClientCodeInDepartment(lstDeptSelected);
                                FlightCategory = UpdateClientCodeInFlightCategory(lstFlightCategorySelected);
                                FlightPurpose = UpdateClientCodeInFlightPurpose(lstFlightPurposeSelected);
                                FuelLocator = UpdateClientCodeInFuelLocator(lstFuelLocatorSelected);
                                objClientCodeService.UpdateClientCodeAll(-1, UserMaster, Fleet, Crew, Passenger, Department, FlightCategory, FlightPurpose, FuelLocator);

                                // Update Session when there is a change in UserPrincipal
                                RefreshSession();

                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Client, Convert.ToInt64(Session["ClientID"].ToString().Trim()));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgClientCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            //if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgClientCode.SelectedItems[0] as GridDataItem;
                                Session["ClientID"] = item["ClientID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                    }
                }
                // For loading selected client code details - AJSE
            }
            // For loading selected client code details - AJSE
        }
        protected void dgClientCode_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            ClientCodePageNavigated = true;
        }
        protected void dgClientCode_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (ClientCodePageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgClientCode, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.EmergencyContact);
                }
            }
        }
        #endregion

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgClientCode;
                        }
                        else if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgClientCode;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }

        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Client Code Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            //dgClientCode.Rebind();
                            GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                    if (IsPopUp)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('RadClientCodePopup');var contentWin = contentWin.rebindgrid();", true);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
            //GridEnable(true, true, true);
            //btnSaveChanges.Visible = false;
            //btnCancel.Visible = false;
            //ClearForm();
            //EnableForm(false);
            //tbCode.ReadOnly = true;
        }

        /// <summary>
        /// Cancel Client Code Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.Client, Convert.ToInt64(Session["ClientID"]));
                        }
                        //Session.Remove("ClientID");
                        DefaultSelection(false);
                        EnableLinks();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('RadClientCodePopup');var contentWin = contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        /// <summary>
        /// To check unique Code  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            CheckAlreadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }

        private bool CheckAlreadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    List<FlightPakMasterService.Client> ClientList = new List<Client>();
                    if (Session["ClientCodeCodes"] != null)
                    {
                        ClientList = ((List<FlightPakMasterService.Client>)Session["ClientCodeCodes"]).Where(x => x.ClientCD.Trim().ToUpper().ToString() == tbCode.Text.Trim().ToUpper().ToString()).ToList(); ;
                        if (ClientList.Count != 0)
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            returnVal = true;
                        }
                        else
                        {
                            cvCode.IsValid = true;
                            RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                            returnVal = false;
                        }
                    }
                    return returnVal;
                }
            }
        }

        /// <summary>
        /// Clear filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnclrFilters_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //pnlExternalForm.Visible = false;
                        foreach (GridColumn column in dgClientCode.MasterTableView.Columns)
                        {
                            //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                            ////column.CurrentFilterValue = string.Empty;
                        }
                        dgClientCode.MasterTableView.FilterExpression = string.Empty;
                        dgClientCode.MasterTableView.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }

        #region "Bind Data to Listbox"
        /// <summary>
        /// Method to call all load method for listbox from db
        /// </summary>
        private void LoadAllListBox(Int64 ClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCode))
            {
                LoadUserMasterList(ClientCode);
                LoadFleetList(ClientCode);
                LoadCrewCheckList(ClientCode);
                LoadPassengerList(ClientCode);
                LoadDeptList(ClientCode);
                LoadFlightCategoryList(ClientCode);
                LoadFlightPurposeList(ClientCode);
                LoadFuelLocatorList(ClientCode);
            }
        }
        /// <summary>
        /// Method for fetching User Master from DB
        /// </summary>
        /// <param name="ClientCode"></param>
        private void LoadUserMasterList(Int64 ClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCode))
            {
                //FlightPakMasterService.MasterCatalogServiceClient UserMasterService = new FlightPakMasterService.MasterCatalogServiceClient();
                using (AdminService.AdminServiceClient UserMasterService = new AdminService.AdminServiceClient())
                {
                    var UserMaster = UserMasterService.GetUserMasterClient();
                    if (UserMaster.ReturnFlag == true)
                    {
                        List<AdminService.GetUserMasterClient> UserMasterList = new List<AdminService.GetUserMasterClient>();
                        UserMasterList = UserMaster.EntityList.Where(x => (x.ClientID == 0 || x.ClientID == null)).ToList();
                        UserMasterList = UserMasterList.OrderBy(x => x.UserName).ToList();
                        lstUserAvailable.DataSource = UserMasterList;
                        lstUserAvailable.DataTextField = "UserName";
                        lstUserAvailable.DataValueField = "UserName";
                        lstUserAvailable.DataBind();
                        UserMasterList = UserMaster.EntityList.Where(x => x.ClientID == ClientCode).ToList();
                        UserMasterList = UserMasterList.OrderBy(x => x.UserName).ToList();
                        lstUserSelected.DataSource = UserMasterList;
                        lstUserSelected.DataTextField = "UserName";
                        lstUserSelected.DataValueField = "UserName";
                        lstUserSelected.DataBind();
                        Session["UserMaster"] = UserMaster.EntityList.ToList<AdminService.GetUserMasterClient>();
                    }
                }
            }
        }
        /// <summary>
        /// Method for fetching Fleet from DB
        /// </summary>
        /// <param name="ClientCode"></param>
        private void LoadFleetList(Int64 ClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var FleetProfile = FleetService.GetFleetForClient();
                    if (FleetProfile.ReturnFlag == true)
                    {
                        List<FlightPakMasterService.GetFleetForClient> FleetProfileList = new List<FlightPakMasterService.GetFleetForClient>();
                        FleetProfileList = FleetProfile.EntityList.Where(x => (x.ClientID == 0 || x.ClientID == null) && x.IsDeleted == false).ToList();
                        FleetProfileList = FleetProfileList.OrderBy(x => x.TailNum).ToList();
                        lstFleetAvailable.DataSource = FleetProfileList;
                        lstFleetAvailable.DataTextField = "TailNum";
                        lstFleetAvailable.DataValueField = "FleetID";
                        lstFleetAvailable.DataBind();
                        FleetProfileList = FleetProfile.EntityList.Where(x => x.ClientID == ClientCode && x.IsDeleted == false).ToList();
                        FleetProfileList = FleetProfileList.OrderBy(x => x.TailNum).ToList();
                        lstFleetSelected.DataSource = FleetProfileList;
                        lstFleetSelected.DataTextField = "TailNum";
                        lstFleetSelected.DataValueField = "FleetID";
                        lstFleetSelected.DataBind();
                        Session["Fleet"] = FleetProfile.EntityList.ToList<FlightPakMasterService.GetFleetForClient>();
                    }
                }
            }
        }
        /// <summary>
        /// Method for fetching Crewfrom DB
        /// </summary>
        /// <param name="ClientCode"></param>
        private void LoadCrewCheckList(Int64 ClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CrewService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var Crew = CrewService.GetCrewForClient();
                    if (Crew.ReturnFlag == true)
                    {
                        List<FlightPakMasterService.GetCrewForClient> CrewList = new List<FlightPakMasterService.GetCrewForClient>();
                        CrewList = Crew.EntityList.Where(x => (x.ClientID == 0 || x.ClientID == null) && x.IsDeleted == false).ToList();
                        CrewList = CrewList.OrderBy(x => x.CrewCD).ToList();
                        lstCrewAvailable.DataSource = CrewList;
                        lstCrewAvailable.DataTextField = "CrewCD";
                        lstCrewAvailable.DataValueField = "CrewID";
                        lstCrewAvailable.DataBind();
                        CrewList = Crew.EntityList.Where(x => x.ClientID == ClientCode && x.IsDeleted == false).ToList();
                        CrewList = CrewList.OrderBy(x => x.CrewCD).ToList();
                        lstCrewSelected.DataSource = CrewList;
                        lstCrewSelected.DataTextField = "CrewCD";
                        lstCrewSelected.DataValueField = "CrewID";
                        lstCrewSelected.DataBind();
                        Session["Crew"] = Crew.EntityList.ToList<FlightPakMasterService.GetCrewForClient>();
                    }
                }
            }
        }
        /// <summary>
        /// Method for fetching Passenger from DB
        /// </summary>
        /// <param name="ClientCode"></param>
        private void LoadPassengerList(Int64 ClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient PassengerService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var Passenger = PassengerService.GetPassengerForClient();
                    if (Passenger.ReturnFlag == true)
                    {
                        List<FlightPakMasterService.GetPassengerForClient> PassengerList = new List<FlightPakMasterService.GetPassengerForClient>();
                        PassengerList = Passenger.EntityList.Where(x => (x.ClientID == 0 || x.ClientID == null) && x.IsDeleted == false).ToList();
                        PassengerList = PassengerList.OrderBy(x => x.PassengerName).ToList();
                        lstPaxAvailable.DataSource = PassengerList;
                        lstPaxAvailable.DataTextField = "PassengerName";
                        lstPaxAvailable.DataValueField = "PassengerRequestorID";
                        lstPaxAvailable.DataBind();
                        PassengerList = Passenger.EntityList.Where(x => x.ClientID == ClientCode && x.IsDeleted == false).ToList();
                        PassengerList = PassengerList.OrderBy(x => x.PassengerName).ToList();
                        lstPaxSelected.DataSource = PassengerList;
                        lstPaxSelected.DataTextField = "PassengerName";
                        lstPaxSelected.DataValueField = "PassengerRequestorID";
                        lstPaxSelected.DataBind();
                        Session["Passenger"] = Passenger.EntityList.ToList<FlightPakMasterService.GetPassengerForClient>();
                    }
                }
            }
        }
        /// <summary>
        /// Method for fetching Department from DB
        /// </summary>
        /// <param name="ClientCode"></param>
        private void LoadDeptList(Int64 ClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient DeptService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var Dept = DeptService.GetDepartmentForClient();
                    if (Dept.ReturnFlag == true)
                    {
                        List<FlightPakMasterService.GetDepartmentForClient> DeptList = new List<FlightPakMasterService.GetDepartmentForClient>();
                        DeptList = Dept.EntityList.Where(x => (x.ClientID == 0 || x.ClientID == null) && x.IsDeleted == false).ToList();

                        DeptList = DeptList.OrderBy(x => x.DepartmentName).ToList();


                        lstDeptAvailable.DataSource = DeptList;
                        lstDeptAvailable.DataTextField = "DepartmentName";
                        lstDeptAvailable.DataValueField = "DepartmentID";
                        lstDeptAvailable.DataBind();
                        DeptList = Dept.EntityList.Where(x => x.ClientID == ClientCode && x.IsDeleted == false).ToList();
                        DeptList = DeptList.OrderBy(x => x.DepartmentName).ToList();
                        if (hdnSave.Value == "Save") { DeptList = new List<GetDepartmentForClient>(); }
                        lstDeptSelected.DataSource = DeptList;
                        lstDeptSelected.DataTextField = "DepartmentName";
                        lstDeptSelected.DataValueField = "DepartmentID";
                        lstDeptSelected.DataBind();
                        Session["Department"] = Dept.EntityList.ToList<FlightPakMasterService.GetDepartmentForClient>();
                    }
                }
            }
        }
        /// <summary>
        /// Method for fetching Department from DB
        /// </summary>
        /// <param name="ClientCode"></param>
        private void LoadFlightCategoryList(Int64 ClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var FlightCategory = FlightCategoryService.GetFlightCategoryForClient();
                    if (FlightCategory.ReturnFlag == true)
                    {
                        List<FlightPakMasterService.GetFlightCategoryForClient> FlightCatagoryList = new List<FlightPakMasterService.GetFlightCategoryForClient>();
                        FlightCatagoryList = FlightCategory.EntityList.Where(x => (x.ClientID == 0 || x.ClientID == null) && x.IsDeleted == false).ToList();
                        FlightCatagoryList = FlightCatagoryList.OrderBy(x => x.FlightCatagoryDescription).ToList();
                        lstFlightCategoryAvailable.DataSource = FlightCatagoryList;
                        lstFlightCategoryAvailable.DataTextField = "FlightCatagoryDescription";
                        lstFlightCategoryAvailable.DataValueField = "FlightCategoryID";
                        lstFlightCategoryAvailable.DataBind();
                        FlightCatagoryList = FlightCategory.EntityList.Where(x => x.ClientID == ClientCode && x.IsDeleted == false).ToList();
                        FlightCatagoryList = FlightCatagoryList.OrderBy(x => x.FlightCatagoryDescription).ToList();
                        lstFlightCategorySelected.DataSource = FlightCatagoryList;
                        lstFlightCategorySelected.DataTextField = "FlightCatagoryDescription";
                        lstFlightCategorySelected.DataValueField = "FlightCategoryID";
                        lstFlightCategorySelected.DataBind();
                        Session["FlightCatagory"] = FlightCategory.EntityList.ToList<FlightPakMasterService.GetFlightCategoryForClient>();
                    }
                }
            }
        }
        /// <summary>
        /// Method for fetching Department from DB
        /// </summary>
        /// <param name="ClientCode"></param>
        private void LoadFlightPurposeList(Int64 ClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FlightPurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var FlightPurpose = FlightPurposeService.GetFlightPurposeForClient();
                    if (FlightPurpose.ReturnFlag == true)
                    {
                        List<FlightPakMasterService.GetFlightPurposeForClient> FlightPurposeList = new List<FlightPakMasterService.GetFlightPurposeForClient>();
                        FlightPurposeList = FlightPurpose.EntityList.Where(x => (x.ClientID == 0 || x.ClientID == null) && x.IsDeleted == false).ToList();
                        FlightPurposeList = FlightPurposeList.OrderBy(x => x.FlightPurposeDescription).ToList();
                        lstFlightPurposeAvailable.DataSource = FlightPurposeList;
                        lstFlightPurposeAvailable.DataTextField = "FlightPurposeDescription";
                        lstFlightPurposeAvailable.DataValueField = "FlightPurposeID";
                        lstFlightPurposeAvailable.DataBind();
                        FlightPurposeList = FlightPurpose.EntityList.Where(x => x.ClientID == ClientCode && x.IsDeleted == false).ToList();
                        FlightPurposeList = FlightPurposeList.OrderBy(x => x.FlightPurposeDescription).ToList();
                        lstFlightPurposeSelected.DataSource = FlightPurposeList;
                        lstFlightPurposeSelected.DataTextField = "FlightPurposeDescription";
                        lstFlightPurposeSelected.DataValueField = "FlightPurposeID";
                        lstFlightPurposeSelected.DataBind();
                        Session["FlightPurpose"] = FlightPurpose.EntityList.ToList<FlightPakMasterService.GetFlightPurposeForClient>();
                    }
                }
            }
        }
        /// <summary>
        /// Method for fetching Department from DB
        /// </summary>
        /// <param name="ClientCode"></param>
        private void LoadFuelLocatorList(Int64 ClientCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCode))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FuelLocatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var FuelLocator = FuelLocatorService.GetFuelLocatorForClient();
                    if (FuelLocator.ReturnFlag == true)
                    {
                        List<FlightPakMasterService.GetFuelLocatorForClient> FuelLocatorList = new List<FlightPakMasterService.GetFuelLocatorForClient>();
                        FuelLocatorList = FuelLocator.EntityList.Where(x => (x.ClientID == 0 || x.ClientID == null) && x.IsDeleted == false).ToList();
                        FuelLocatorList = FuelLocatorList.OrderBy(x => x.FuelLocatorDescription).ToList();
                        lstFuelLocatorAvailable.DataSource = FuelLocatorList;
                        lstFuelLocatorAvailable.DataTextField = "FuelLocatorDescription";
                        lstFuelLocatorAvailable.DataValueField = "FuelLocatorID";
                        lstFuelLocatorAvailable.DataBind();
                        FuelLocatorList = FuelLocator.EntityList.Where(x => x.ClientID == ClientCode && x.IsDeleted == false).ToList();
                        FuelLocatorList = FuelLocatorList.OrderBy(x => x.FuelLocatorDescription).ToList();
                        lstFuelLocatorSelected.DataSource = FuelLocatorList;
                        lstFuelLocatorSelected.DataTextField = "FuelLocatorDescription";
                        lstFuelLocatorSelected.DataValueField = "FuelLocatorID";
                        lstFuelLocatorSelected.DataBind();
                        Session["FuelLocator"] = FuelLocator.EntityList.ToList<FlightPakMasterService.GetFuelLocatorForClient>();
                    }
                }
            }
        }
        #endregion

        #region "Update ClientCode"
        /// <summary>
        /// Method for Update client code in User Master
        /// </summary>
        private string UpdateClientCodeInUserMaster(ListBox lstBox)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lstBox))
            {
                List<AdminService.GetUserMasterClient> UserMasterList = new List<AdminService.GetUserMasterClient>();
                List<AdminService.GetUserMasterClient> UserMasterEditList = new List<AdminService.GetUserMasterClient>();
                if (Session["UserMaster"] != null)
                {
                    UserMasterList = (List<AdminService.GetUserMasterClient>)Session["UserMaster"];
                }
                string strUserMaster = string.Empty;
                string FirstName = string.Empty;
                string UserName = string.Empty;
                foreach (ListItem lstItem in lstBox.Items)
                {
                    FirstName = lstItem.Text;
                    UserName = lstItem.Value;
                    UserMasterEditList = (from UserMasterLst in UserMasterList
                                          where UserMasterLst.UserName == UserName  //UserMasterLst.UserName == FirstName && 
                                          select UserMasterLst).ToList<AdminService.GetUserMasterClient>();
                    foreach (AdminService.GetUserMasterClient UserMasterAdmin in UserMasterEditList)
                    {
                        strUserMaster = strUserMaster + UserMasterAdmin.UserName + ",";
                    }
                }
                if (strUserMaster.Length != 0)
                {
                    strUserMaster = strUserMaster.Remove(strUserMaster.LastIndexOf(','));
                }
                return strUserMaster.Trim();
            }
        }
        /// <summary>
        /// Method for Update client code in Fleet
        /// </summary>
        private string UpdateClientCodeInFleet(ListBox lstBox)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lstBox))
            {
                List<FlightPakMasterService.GetFleetForClient> FleetList = new List<FlightPakMasterService.GetFleetForClient>();
                List<FlightPakMasterService.GetFleetForClient> FleetEditList = new List<FlightPakMasterService.GetFleetForClient>();
                if (Session["Fleet"] != null)
                {
                    FleetList = (List<FlightPakMasterService.GetFleetForClient>)Session["Fleet"];
                }
                string Fleet = string.Empty;
                string TailNum = string.Empty;
                Int64 FleetID;
                foreach (ListItem lstItem in lstBox.Items)
                {
                    TailNum = lstItem.Text;
                    FleetID = Convert.ToInt64(lstItem.Value);
                    FleetEditList = (from FleetFnlLst in FleetList
                                     where FleetFnlLst.FleetID == FleetID  //FleetFnlLst.TailNum == TailNum &&
                                     select FleetFnlLst).ToList<FlightPakMasterService.GetFleetForClient>();
                    foreach (FlightPakMasterService.GetFleetForClient oFleet in FleetEditList)
                    {
                        Fleet = Fleet + oFleet.FleetID + ",";
                    }
                }
                if (Fleet.Length != 0)
                {
                    Fleet = Fleet.Remove(Fleet.LastIndexOf(','));
                }
                return Fleet.Trim();
            }
        }
        /// <summary>
        /// Method for Update client code in Crew
        /// </summary>
        private string UpdateClientCodeInCrew(ListBox lstBox)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lstBox))
            {
                List<FlightPakMasterService.GetCrewForClient> CrewList = new List<FlightPakMasterService.GetCrewForClient>();
                List<FlightPakMasterService.GetCrewForClient> CrewEditList = new List<FlightPakMasterService.GetCrewForClient>();
                if (Session["Crew"] != null)
                {
                    CrewList = (List<FlightPakMasterService.GetCrewForClient>)Session["Crew"];
                }
                string Crew = string.Empty;
                string CrewCD = string.Empty;
                Int64 CrewID;
                foreach (ListItem lstItem in lstBox.Items)
                {
                    CrewCD = lstItem.Text;
                    CrewID = Convert.ToInt64(lstItem.Value);
                    CrewEditList = (from CrewFnlLst in CrewList
                                    where CrewFnlLst.CrewID == CrewID  //CrewFnlLst.CrewCD == CrewCD &&
                                    select CrewFnlLst).ToList<FlightPakMasterService.GetCrewForClient>();
                    foreach (FlightPakMasterService.GetCrewForClient oCrew in CrewEditList)
                    {
                        Crew = Crew + oCrew.CrewID + ",";
                    }
                }
                if (Crew.Length != 0)
                {
                    Crew = Crew.Remove(Crew.LastIndexOf(','));
                }
                return Crew.Trim();
            }
        }
        /// <summary>
        /// Method for Update client code in Passenger
        /// </summary>
        private string UpdateClientCodeInPassenger(ListBox lstBox)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lstBox))
            {
                List<FlightPakMasterService.GetPassengerForClient> PassengerList = new List<FlightPakMasterService.GetPassengerForClient>();
                List<FlightPakMasterService.GetPassengerForClient> PassengerEditList = new List<FlightPakMasterService.GetPassengerForClient>();
                if (Session["Passenger"] != null)
                {
                    PassengerList = (List<FlightPakMasterService.GetPassengerForClient>)Session["Passenger"];
                }
                string Passenger = string.Empty;
                Int64 PassengerRequestorID;
                string PassengerName = string.Empty;
                foreach (ListItem lstItem in lstBox.Items)
                {
                    PassengerName = lstItem.Text;
                    PassengerRequestorID = Convert.ToInt64(lstItem.Value);
                    PassengerEditList = (from PassengerFnlLst in PassengerList
                                         where PassengerFnlLst.PassengerRequestorID == PassengerRequestorID //PassengerFnlLst.PassengerName == PassengerName && 
                                         select PassengerFnlLst).ToList<FlightPakMasterService.GetPassengerForClient>();
                    foreach (FlightPakMasterService.GetPassengerForClient oPassenger in PassengerEditList)
                    {
                        Passenger = Passenger + oPassenger.PassengerRequestorID + ",";
                    }
                }
                if (Passenger.Length != 0)
                {
                    Passenger = Passenger.Remove(Passenger.LastIndexOf(','));
                }
                return Passenger.Trim();
            }
        }
        /// <summary>
        /// Method for Update client code in Department
        /// </summary>
        private string UpdateClientCodeInDepartment(ListBox lstBox)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lstBox))
            {
                List<FlightPakMasterService.GetDepartmentForClient> DepartmentList = new List<FlightPakMasterService.GetDepartmentForClient>();
                List<FlightPakMasterService.GetDepartmentForClient> DepartmentEditList = new List<FlightPakMasterService.GetDepartmentForClient>();
                if (Session["Department"] != null)
                {
                    DepartmentList = (List<FlightPakMasterService.GetDepartmentForClient>)Session["Department"];
                }
                string Department = string.Empty;
                string DepartmentName = string.Empty;
                Int64 DepartmentID;
                foreach (ListItem lstItem in lstBox.Items)
                {
                    DepartmentName = lstItem.Text;
                    DepartmentID = Convert.ToInt64(lstItem.Value);
                    DepartmentEditList = (from DepartmentFnlLst in DepartmentList
                                          where DepartmentFnlLst.DepartmentID == DepartmentID //DepartmentFnlLst.DepartmentName == DepartmentName && 
                                          select DepartmentFnlLst).ToList<FlightPakMasterService.GetDepartmentForClient>();
                    foreach (FlightPakMasterService.GetDepartmentForClient oDepartment in DepartmentEditList)
                    {
                        Department = Department + oDepartment.DepartmentID + ",";
                    }
                }
                if (Department.Length != 0)
                {
                    Department = Department.Remove(Department.LastIndexOf(','));
                }
                return Department.Trim();
            }
        }
        /// <summary>
        /// Method for Update client code in FlightCatagory
        /// </summary>
        private string UpdateClientCodeInFlightCategory(ListBox lstBox)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lstBox))
            {
                List<FlightPakMasterService.GetFlightCategoryForClient> FlightCatagoryList = new List<FlightPakMasterService.GetFlightCategoryForClient>();
                List<FlightPakMasterService.GetFlightCategoryForClient> FlightCatagoryEditList = new List<FlightPakMasterService.GetFlightCategoryForClient>();
                if (Session["FlightCatagory"] != null)
                {
                    FlightCatagoryList = (List<FlightPakMasterService.GetFlightCategoryForClient>)Session["FlightCatagory"];
                }
                string FlightCatagory = string.Empty;
                string FlightCatagoryDescription = string.Empty;
                Int64 FlightCategoryID;
                foreach (ListItem lstItem in lstBox.Items)
                {
                    FlightCatagoryDescription = lstItem.Text;
                    FlightCategoryID = Convert.ToInt64(lstItem.Value);
                    FlightCatagoryEditList = (from FlightCatagoryFnlLst in FlightCatagoryList
                                              where FlightCatagoryFnlLst.FlightCategoryID == FlightCategoryID //FlightCatagoryFnlLst.FlightCatagoryDescription == FlightCatagoryDescription && 
                                              select FlightCatagoryFnlLst).ToList<FlightPakMasterService.GetFlightCategoryForClient>();
                    foreach (FlightPakMasterService.GetFlightCategoryForClient oFlightCatagory in FlightCatagoryEditList)
                    {
                        FlightCatagory = FlightCatagory + oFlightCatagory.FlightCategoryID + ",";
                    }
                }
                if (FlightCatagory.Length != 0)
                {
                    FlightCatagory = FlightCatagory.Remove(FlightCatagory.LastIndexOf(','));
                }
                return FlightCatagory.Trim();
            }
        }
        /// <summary>
        /// Method for Update client code in FlightPurpose
        /// </summary>
        private string UpdateClientCodeInFlightPurpose(ListBox lstBox)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lstBox))
            {
                List<FlightPakMasterService.GetFlightPurposeForClient> FlightPurposeList = new List<FlightPakMasterService.GetFlightPurposeForClient>();
                List<FlightPakMasterService.GetFlightPurposeForClient> FlightPurposeEditList = new List<FlightPakMasterService.GetFlightPurposeForClient>();
                if (Session["FlightPurpose"] != null)
                {
                    FlightPurposeList = (List<FlightPakMasterService.GetFlightPurposeForClient>)Session["FlightPurpose"];
                }
                string FlightPurpose = string.Empty;
                string FlightPurposeDescription = string.Empty;
                Int64 FlightPurposeID;
                foreach (ListItem lstItem in lstBox.Items)
                {
                    FlightPurposeDescription = lstItem.Text;
                    FlightPurposeID = Convert.ToInt64(lstItem.Value);
                    FlightPurposeEditList = (from FlightPurposeFnlLst in FlightPurposeList
                                             where FlightPurposeFnlLst.FlightPurposeID == FlightPurposeID //FlightPurposeFnlLst.FlightPurposeDescription == FlightPurposeDescription && 
                                             select FlightPurposeFnlLst).ToList<FlightPakMasterService.GetFlightPurposeForClient>();
                    foreach (FlightPakMasterService.GetFlightPurposeForClient oFlightPurpose in FlightPurposeEditList)
                    {
                        FlightPurpose = FlightPurpose + oFlightPurpose.FlightPurposeID + ",";
                    }
                }
                if (FlightPurpose.Length != 0)
                {
                    FlightPurpose = FlightPurpose.Remove(FlightPurpose.LastIndexOf(','));
                }
                return FlightPurpose.Trim();
            }
        }
        /// <summary>
        /// Method for Update client code in Department
        /// </summary>
        private string UpdateClientCodeInFuelLocator(ListBox lstBox)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lstBox))
            {
                List<FlightPakMasterService.GetFuelLocatorForClient> FuelLocatorList = new List<FlightPakMasterService.GetFuelLocatorForClient>();
                List<FlightPakMasterService.GetFuelLocatorForClient> FuelLocatorEditList = new List<FlightPakMasterService.GetFuelLocatorForClient>();
                if (Session["FuelLocator"] != null)
                {
                    FuelLocatorList = (List<FlightPakMasterService.GetFuelLocatorForClient>)Session["FuelLocator"];
                }
                string FuelLocator = string.Empty;
                string FuelLocatorDescription = string.Empty;
                Int64 FuelLocatorID;
                foreach (ListItem lstItem in lstBox.Items)
                {
                    FuelLocatorDescription = lstItem.Text;
                    FuelLocatorID = Convert.ToInt64(lstItem.Value);
                    FuelLocatorEditList = (from FuelLocatorFnlLst in FuelLocatorList
                                           where FuelLocatorFnlLst.FuelLocatorID == FuelLocatorID // FuelLocatorFnlLst.FuelLocatorDescription == FuelLocatorDescription && 
                                           select FuelLocatorFnlLst).ToList<FlightPakMasterService.GetFuelLocatorForClient>();
                    foreach (FlightPakMasterService.GetFuelLocatorForClient oFuelLocator in FuelLocatorEditList)
                    {
                        FuelLocator = FuelLocator + oFuelLocator.FuelLocatorID + ",";
                    }
                }
                if (FuelLocator.Length != 0)
                {
                    FuelLocator = FuelLocator.Remove(FuelLocator.LastIndexOf(','));
                }
                return FuelLocator.Trim();
            }
        }
        #endregion

        #region "Move Listbox items from one to another"
        private void MoveOneItem(ListBox ListBoxFrom, ListBox ListBoxTo)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ListBoxFrom, ListBoxTo))
            {
                for (int index = ListBoxFrom.Items.Count - 1; index >= 0; index--)
                {
                    if (ListBoxFrom.Items[index].Selected == true)
                    {
                        ListBoxTo.Items.Add(ListBoxFrom.Items[index]);
                        ListBoxFrom.Items.Remove(ListBoxFrom.Items[index]);
                    }
                }
            }
        }
        private void MoveAllItem(ListBox ListBoxFrom, ListBox ListBoxTo)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ListBoxTo, ListBoxFrom))
            {
                for (int index = ListBoxFrom.Items.Count - 1; index >= 0; index--)
                {
                    ListBoxTo.Items.Add(ListBoxFrom.Items[index]);
                    ListBoxFrom.Items.Remove(ListBoxFrom.Items[index]);
                }
            }
        }
        protected void btnUserAdd_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstUserAvailable, lstUserSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnUserAddAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstUserAvailable, lstUserSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnUserRemove_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstUserSelected, lstUserAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnUserRemoveAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstUserSelected, lstUserAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFleetAdd_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstFleetAvailable, lstFleetSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFleetAddAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstFleetAvailable, lstFleetSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFleetRemove_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstFleetSelected, lstFleetAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFleetRemoveAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstFleetSelected, lstFleetAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnCrewAdd_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstCrewAvailable, lstCrewSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnCrewAddAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstCrewAvailable, lstCrewSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnCrewRemove_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstCrewSelected, lstCrewAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnCrewRemoveAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstCrewSelected, lstCrewAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnPaxAdd_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstPaxAvailable, lstPaxSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnPaxAddAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstPaxAvailable, lstPaxSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnPaxRemove_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstPaxSelected, lstPaxAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnPaxRemoveAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstPaxSelected, lstPaxAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnDeptAdd_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstDeptAvailable, lstDeptSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnDeptAddAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstDeptAvailable, lstDeptSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnDeptRemove_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstDeptSelected, lstDeptAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnDeptRemoveAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstDeptSelected, lstDeptAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFlightCategoryAdd_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstFlightCategoryAvailable, lstFlightCategorySelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFlightCategoryAddAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstFlightCategoryAvailable, lstFlightCategorySelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFlightCategoryRemove_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstFlightCategorySelected, lstFlightCategoryAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFlightCategoryRemoveAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstFlightCategorySelected, lstFlightCategoryAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFlightPurposeAdd_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstFlightPurposeAvailable, lstFlightPurposeSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFlightPurposeAddAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstFlightPurposeAvailable, lstFlightPurposeSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFlightPurposeRemove_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstFlightPurposeSelected, lstFlightPurposeAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFlightPurposeRemoveAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstFlightPurposeSelected, lstFlightPurposeAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFuelLocatorAdd_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstFuelLocatorAvailable, lstFuelLocatorSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFuelLocatorAddAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstFuelLocatorAvailable, lstFuelLocatorSelected);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFuelLocatorRemove_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveOneItem(lstFuelLocatorSelected, lstFuelLocatorAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        protected void btnFuelLocatorRemoveAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        MoveAllItem(lstFuelLocatorSelected, lstFuelLocatorAvailable);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Client);
                }
            }
        }
        #endregion

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.Client> lstClientCode = new List<FlightPakMasterService.Client>();
                if (Session["ClientCodeCodes"] != null)
                {
                    lstClientCode = (List<FlightPakMasterService.Client>)Session["ClientCodeCodes"];
                }
                if (lstClientCode.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstClientCode = lstClientCode.Where(x => x.IsInActive == false).ToList<Client>(); }
                    dgClientCode.DataSource = lstClientCode;
                    if (IsDataBind)
                    {
                        dgClientCode.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgClientCode.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var ClientCodeValue = FPKMstService.GetClientCodeList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, ClientCodeValue);
            List<FlightPakMasterService.Client> filteredList = GetFilteredList(ClientCodeValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.ClientID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgClientCode.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgClientCode.CurrentPageIndex = PageNumber;
            dgClientCode.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfClient ClientCodeValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["ClientID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = ClientCodeValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().ClientID;
                Session["ClientID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.Client> GetFilteredList(ReturnValueOfClient ClientCodeValue)
        {
            List<FlightPakMasterService.Client> filteredList = new List<FlightPakMasterService.Client>();

            if (ClientCodeValue.ReturnFlag)
            {
                filteredList = ClientCodeValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<Client>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            if (!IsPostBack && IsPopUp)
            {
                //Hide Controls
                dgClientCode.Visible = false;
                chkSearchActiveOnly.Visible = false;
                if (IsAdd)
                {
                    (dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                }
                else
                {
                    (dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                }
            }
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgClientCode.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgClientCode.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
    }
}
