﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="TripManagerCheckListCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.TripManagerCheckListCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function CheckTxtBox(control) {
            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);
                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);
                } return false;
            }
        }
        function openReport() {

            url = "../../Reports/ExportReportInformation.aspx?Report=RptDBTripsheetChecklistExport&UserCD=UC";
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open(url, "RadExportData");
        }
        
    </script>
    <%--<script type="text/javascript">
        function TxtBox(state) {

            ValidatorEnable(document.getElementById('<%=rfvGroup.ClientID%>'), state);
        }
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openWin() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../People/CrewChecklistGroupPopup.aspx?CheckGroupCD=" + document.getElementById("<%=tbGroup.ClientID%>").value, "RadWindow1");
            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); // GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientClose(oWnd, args) {
                var combo = $find("<%= tbGroup.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbGroup.ClientID%>").value = arg.CheckGroupCD;
                        document.getElementById("<%=hdnChecklistGroupCode.ClientID%>").value = arg.CheckGroupID;
                        document.getElementById("<%=btnCheckAlreadyGroupCodeExist.ClientID%>").click();
                        document.getElementById("<%=cvGroup.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbGroup.ClientID%>").value = "";
                        document.getElementById("<%=hdnChecklistGroupCode.ClientID%>").value = "";

                        combo.clearSelection();
                    }
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function ShowReports(radWin, ReportFormat, ReportName) {
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                if (ReportFormat == "EXPORT") {
                    url = "../../Reports/ExportReportInformation.aspx";
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    var oWnd = oManager.open(url, 'RadExportData');
                    return true;
                }
                else {
                    return true;
                }
            }
            function OnClientCloseExportReport(oWnd, args) {
                document.getElementById("<%=btnShowReports.ClientID%>").click();
            }

            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;

            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                Title="CrewChecklist Group" OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" VisibleStatusbar="false" Behaviors="Close" NavigateUrl="~/Views/Settings/People/CrewChecklistGroupPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExportReport" Title="Export Report Information" KeepInScreenBounds="true"
                AutoSize="false" Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgTripManagerCheckList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgTripManagerCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgTripManagerCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgTripManagerCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgTripManagerCheckList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgTripManagerCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="cvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbGroup">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cvGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="True">
            
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="headerText">
                        <div class="tab-nav-top">
                            <span class="head-title">Trip Checklist</span> <span class="tab-nav-icons">
                                <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBTripsheetChecklist');"
                                    OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                                <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBTripsheetChecklist');return false;"
                                    OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                                <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                                    Style="display: none;" />
                                <a href="../../Help/ViewHelp.aspx?Screen=TripMgrChkLstHelp"
                                    target="_blank" title="Help" class="help-icon"></a>
                                <asp:HiddenField ID="hdnReportName" runat="server" />
                                <asp:HiddenField ID="hdnReportFormat" runat="server" />
                                <asp:HiddenField ID="hdnReportParameters" runat="server" />
                            </span>
                        </div>
                    </td>
                </tr>
            </table>
            <%--<table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>--%>
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgTripManagerCheckList" runat="server" AllowSorting="true" OnItemCreated="TripManagerCheckList_ItemCreated"
                OnPageIndexChanged="TripManagerCheckList_PageIndexChanged" OnNeedDataSource="TripManagerCheckList_BindData"
                OnItemCommand="TripManagerCheckList_ItemCommand" OnPreRender="TripManagerCheckList_PreRender"
                OnUpdateCommand="TripManagerCheckList_UpdateCommand" OnInsertCommand="TripManagerCheckList_InsertCommand"
                OnDeleteCommand="TripManagerCheckList_DeleteCommand" AutoGenerateColumns="false"
                Height="341px" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="TripManagerCheckList_SelectedIndexChanged"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
                <MasterTableView ClientDataKeyNames="CheckListCD,CheckListDescription,CheckGroupCD"
                    DataKeyNames="CheckListID,CheckListCD,CheckListDescription,CustomerID,CheckGroupID,CheckGroupCD,LastUpdUID,LastUpdTS,IsInActive"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="CheckListCD" HeaderText="Checklist Code" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="140px" FilterDelay="500"
                            FilterControlWidth="120px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CheckListDescription" HeaderText="Description" FilterDelay="500"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="420px" FilterControlWidth="400px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CheckGroupCD" HeaderText="Group" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px"
                            FilterControlWidth="100px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CheckListID" HeaderText="CheckListID" AutoPostBackOnFilter="false"
                            Display="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" HeaderStyle-Width="80px">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div class="grid_icon">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                CssClass="add-icon-grid" Visible='<%# IsAuthorized(Permission.Database.AddTripManagerCheckListCatalog)%>'></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                CssClass="edit-icon-grid" Visible='<%# IsAuthorized(Permission.Database.EditTripManagerCheckListCatalog)%>'></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                                CssClass="delete-icon-grid" Visible='<%# IsAuthorized(Permission.Database.DeleteTripManagerCheckListCatalog)%>'></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="tdLabel100">
                        <span class="mnd_text">Checklist Code</span>
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="tdLabel150">
                                                <asp:TextBox ID="tbCode" runat="server" MaxLength="4" CssClass="text40" ValidationGroup="Save"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%--<asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Checklist Code is Required."
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>--%>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="tbCode"
                                                    Text="Checklist Code is Required." ValidationGroup="Save" Display="Dynamic" CssClass="alert-text"
                                                    SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" class="tdLabel50">
                                    <span class="mnd_text">Group</span>
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbGroup" Width="60" MaxLength="4" runat="server" OnTextChanged="Group_TextChanged"
                                                                AutoPostBack="true"></asp:TextBox>
                                                            <asp:HiddenField ID="hdnGroup" runat="server" />
                                                        </td>
                                                        <td valign="top">
                                                            <asp:Button ID="btnGroup" runat="server" OnClientClick="javascript:openWin();return false;"
                                                                CssClass="browse-button" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvGroup" runat="server" ControlToValidate="tbGroup"
                                                    Text="Group Code is Required." ValidationGroup="Save" Display="Dynamic" CssClass="alert-text"
                                                    SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvGroup" runat="server" ControlToValidate="tbGroup" ErrorMessage=""
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <span class="mnd_text">Description</span>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbDescription" runat="server" MaxLength="20" CssClass="text260"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="Save"
                                        ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                        ErrorMessage="Description is Required."></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="tblButtonArea">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            CssClass="button" ValidationGroup="Save" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click" CausesValidation="false"
                            CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnChecklistGroupCode" runat="server" />
                        <asp:HiddenField ID="hdnIsEdit" runat="server" />
                        <asp:HiddenField ID="hdnGroupId" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                        <asp:Button ID="btnCheckAlreadyGroupCodeExist" CssClass="button-disable" runat="server"
                            OnClick="CheckAlreadyGroupCodeExist_Click" Style="display: none" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
