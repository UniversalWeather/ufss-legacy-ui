﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class FuelLocator : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private bool FuelLocatorPageNavigated = false;
        private string strFuelID = "";
        private string strFuelCD = "";
        private List<string> lstFuelLocatorCode = new List<string>();
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFuelLocator, dgFuelLocator, this.Page.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFuelLocator.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewFuelLocator);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgFuelLocator.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                if(IsPopUp && !IsAdd)
                                    Session["SelectedFuelLocatorID"] = Request.QueryString["SelectedFuelLocatorID"];
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                        if (IsPopUp)
                        {
                            dgFuelLocator.AllowPaging = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Created for Fuel Locator Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFuelLocator_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, Page.Master.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, Page.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Bind Fuel Locator Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFuelLocator_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objFuelLocatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objFuelLocatorVal = objFuelLocatorService.GetFuelLocatorInfo();
                            List<FlightPakMasterService.GetAllFuelLocator> FuelLocatorList = new List<FlightPakMasterService.GetAllFuelLocator>();
                            if (objFuelLocatorVal.ReturnFlag == true)
                            {
                                FuelLocatorList = objFuelLocatorVal.EntityList;
                            }
                            dgFuelLocator.DataSource = FuelLocatorList;
                            Session["FuelLocatorCD"] = FuelLocatorList;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        /// <summary>
        /// To highlight the selected item 
        /// </summary>        
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFuelLocatorID"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                        //string code = items.GetDataKeyValue("DelayTypeCD").ToString();
                        string ID = Session["SelectedFuelLocatorID"].ToString();
                        foreach (GridDataItem item in dgFuelLocator.MasterTableView.Items)
                        {
                            if (item["FuelLocatorID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Item Command for Fuel Locator Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFuelLocator_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                hdnRedirect.Value = "";
                                if (Session["SelectedFuelLocatorID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.FuelLocator, Convert.ToInt64(Session["SelectedFuelLocatorID"].ToString().Trim()));

                                        Session["IsFuelLocatorEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FuelLocator);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FuelLocator);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        tbCode.Enabled = false;
                                        SelectItem();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFuelLocator.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                EnableForm(true);
                                tbCode.Focus();
                                DisableLinks();
                                break;
                            case "UpdateEdited":
                                dgFuelLocator_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        /// <summary>
        /// Update Command for Fuel Locator Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFuelLocator_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValid = true;
                        if (!CheckClientCodeExist())
                        {
                            cvClientCode.IsValid = false;
                            tbClientCode.Focus();
                            IsValid = false;
                        }
                        if (IsValid == true)
                        {
                            if (Session["SelectedFuelLocatorID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objFuelService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objFuelService.UpdateFuelLocator(GetItems());
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();
                                        EnableLinks();
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.FuelLocator, Convert.ToInt64(Session["SelectedFuelLocatorID"].ToString().Trim()));
                                        }
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        DefaultSelection(true);
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FuelLocator);
                                    }                                    
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            //Session["SelectedFuelLocatorID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('RadFuelLocatorPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFuelLocator_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFuelLocator.ClientSettings.Scrolling.ScrollTop = "0";
                        FuelLocatorPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFuelLocator_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (FuelLocatorPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFuelLocator, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// Update Command for Fuel Locator Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFuelLocator_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValid = true;
                        if (checkAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                            IsValid = false;
                        }
                        if (!CheckClientCodeExist())
                        {
                            cvClientCode.IsValid = false;
                            tbClientCode.Focus();
                            IsValid = false;
                        }
                        if (IsValid == true)
                        {
                            using (MasterCatalogServiceClient objFuelService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objFuelService.AddFuelLocator(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    dgFuelLocator.Rebind();
                                    DefaultSelection(false);
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.FuelLocator);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('RadFuelLocatorPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        protected void FuelLocatorCD_TextChanged(object sender, EventArgs e)
        {
            bool ReturnFlag = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //List<GetAllFuelLocator> FuelCodeList = new List<GetAllFuelLocator>();
                        //FuelCodeList = ((List<GetAllFuelLocator>)Session["FuelLocatorCD"]).Where(x => x.FuelLocatorCD.ToString().ToUpper().Trim().Contains(tbCode.Text.ToString().ToUpper().Trim())).ToList<GetAllFuelLocator>();
                        //if (FuelCodeList.Count != 0)
                        //{
                        //    cvCode.IsValid = false;
                        //    tbCode.Focus();
                        //    ReturnFlag = true;
                        //}
                        //return ReturnFlag;

                        if (!string.IsNullOrEmpty(tbCode.Text))
                        {
                            if (checkAllReadyExist())
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbClientCode.ClientID); //tbClientCode.Focus();
                            }
                        } 
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFuelLocator_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedFuelLocatorID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objFuelLocatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FuelLocator FuelLocatorType = new FlightPakMasterService.FuelLocator();
                                string Code = Session["SelectedFuelLocatorID"].ToString();
                                strFuelCD = "";
                                strFuelID = "";
                                foreach (GridDataItem Item in dgFuelLocator.MasterTableView.Items)
                                {
                                    if (Item["FuelLocatorID"].Text.Trim() == Code.Trim())
                                    {
                                        if (Item.GetDataKeyValue("FuelLocatorCD") != null)
                                        {
                                            FuelLocatorType.FuelLocatorCD = Item.GetDataKeyValue("FuelLocatorCD").ToString().Trim();
                                        }
                                        if (Item.GetDataKeyValue("FuelLocatorID") != null)
                                        {
                                            strFuelID = Item.GetDataKeyValue("FuelLocatorID").ToString().Trim();
                                        }
                                        break;
                                    }
                                }
                                FuelLocatorType.FuelLocatorID = Convert.ToInt64(strFuelID);
                                FuelLocatorType.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.FuelLocator, Convert.ToInt64(strFuelID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FuelLocator);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FuelLocator);
                                        return;
                                    }
                                    objFuelLocatorService.DeleteFuelLocator(FuelLocatorType);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FuelLocator, Convert.ToInt64(strFuelID));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFuelLocator_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            GridDataItem item = dgFuelLocator.SelectedItems[0] as GridDataItem;
                            if (item.GetDataKeyValue("FuelLocatorID") != null)
                            {
                                Session["SelectedFuelLocatorID"] = item["FuelLocatorID"].Text;
                            }
                            if (btnSaveChanges.Visible == false)
                            {
                                ReadOnlyForm();
                            }
                            GridEnable(true, true, true);
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                    }
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFuelLocator;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            //tbClientCode.Text = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbClientCode.Text = string.Empty;
                    hdnClientCode.Value = string.Empty;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        // Set Logged-in User Name
                        if (UserPrincipal != null && UserPrincipal.Identity._clientId != null && UserPrincipal.Identity._clientId != 0)
                        {
                            tbClientCode.Enabled = false;
                            //need to change
                            tbClientCode.Text = UserPrincipal.Identity._clientCd.ToString().Trim();
                            hdnClientCode.Value = UserPrincipal.Identity._clientId.ToString().Trim();
                            btnClientCode.Enabled = false;
                        }
                        else
                        {
                            tbClientCode.Enabled = enable;
                            btnClientCode.Enabled = enable;
                        }
                        tbCode.Enabled = enable;
                        tbDescription.Enabled = enable;
                        btnCancel.Visible = enable;
                        btnSaveChanges.Visible = enable;
                        chkInactive.Enabled = enable;  
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFuelLocatorID"] != null)
                    {
                        strFuelID = "";
                        foreach (GridDataItem Item in dgFuelLocator.MasterTableView.Items)
                        {
                            if (Item["FuelLocatorID"].Text.Trim() == Session["SelectedFuelLocatorID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("FuelLocatorCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("FuelLocatorCD").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("FuelLocatorID") != null)
                                {
                                    strFuelID = Item.GetDataKeyValue("FuelLocatorID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("FuelLocatorDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("FuelLocatorDescription").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ClientCD") != null)
                                {
                                    tbClientCode.Text = Item.GetDataKeyValue("ClientCD").ToString();
                                    hdnClientCode.Value = Item.GetDataKeyValue("ClientID").ToString();
                                }
                                else
                                {
                                    tbClientCode.Text = string.Empty;
                                    hdnClientCode.Value = string.Empty;
                                }
                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgFuelLocator.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));// item.GetDataKeyValue("LastUpdTS").ToString();
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                break;
                            }
                        }
                        EnableForm(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To display the value in read only format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgFuelLocator.SelectedItems.Count >0)
                    {
                        GridDataItem Item = dgFuelLocator.SelectedItems[0] as GridDataItem;
                        if (Item.GetDataKeyValue("FuelLocatorCD") != null)
                        {
                            tbCode.Text = Item.GetDataKeyValue("FuelLocatorCD").ToString();
                        }
                        else
                        {
                            tbCode.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("FuelLocatorDescription") != null)
                        {
                            tbDescription.Text = Item.GetDataKeyValue("FuelLocatorDescription").ToString();
                        }
                        else
                        {
                            tbDescription.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("ClientCD") != null)
                        {
                            tbClientCode.Text = Item.GetDataKeyValue("ClientCD").ToString();
                        }
                        else
                        {
                            tbClientCode.Text = string.Empty;
                        }
                        Label lbLastUpdatedUser;
                        lbLastUpdatedUser = (Label)dgFuelLocator.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));// item.GetDataKeyValue("LastUpdTS").ToString();
                        }
                        if (Item.GetDataKeyValue("IsInActive") != null)
                        {
                            chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            chkInactive.Checked = false;
                        }
                        lbColumnName1.Text = "Locator Code";
                        lbColumnName2.Text = "Description";
                        lbColumnValue1.Text = Item["FuelLocatorCD"].Text;
                        lbColumnValue2.Text = Item["FuelLocatorDescription"].Text;
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Fuel Locator Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgFuelLocator.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgFuelLocator.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        /// <summary>
        /// Cancel Fuel Locator Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedFuelLocatorID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.FuelLocator, Convert.ToInt64(Session["SelectedFuelLocatorID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        //Session.Remove("SelectedFuelLocatorID");
                        EnableLinks();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('RadFuelLocatorPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = false;
                                chkSearchActiveOnly.Visible = false;
                                //table2.Visible = false;
                                dgFuelLocator.Visible = false;
                                if (IsAdd)
                                {
                                    (dgFuelLocator.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgFuelLocator.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                }
                                
                            }
                        }
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }

            
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFuelLocator.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFuelLocator.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!IsPopUp)
                    {

                        if (BindDataSwitch)
                        {
                            dgFuelLocator.Rebind();
                        }
                        if (dgFuelLocator.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedFuelLocatorID"] = null;
                            //}
                            if (Session["SelectedFuelLocatorID"] == null)
                            {
                                dgFuelLocator.SelectedIndexes.Add(0);
                                if (!IsPopUp)
                                {
                                    Session["SelectedFuelLocatorID"] = dgFuelLocator.Items[0].GetDataKeyValue("FuelLocatorID").ToString();
                                }
                            }

                            if (dgFuelLocator.SelectedIndexes.Count == 0)
                                dgFuelLocator.SelectedIndexes.Add(0);

                            ReadOnlyForm();
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                        }
                        GridEnable(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtninsertCtl, lbtndelCtl, lbtneditCtl;
                    lbtninsertCtl = (LinkButton)dgFuelLocator.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    lbtndelCtl = (LinkButton)dgFuelLocator.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    lbtneditCtl = (LinkButton)dgFuelLocator.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddFuelLocator))
                    {
                        lbtninsertCtl.Visible = true;
                        if (add)
                        {
                            lbtninsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtninsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtninsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteFuelLocator))
                    {
                        lbtndelCtl.Visible = true;
                        if (delete)
                        {
                            lbtndelCtl.Enabled = true;
                            lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtndelCtl.Enabled = false;
                            lbtndelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtndelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditFuelLocator))
                    {
                        lbtneditCtl.Visible = true;
                        if (edit)
                        {
                            lbtneditCtl.Enabled = true;
                            lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtneditCtl.Enabled = false;
                            lbtneditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtneditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private FlightPakMasterService.FuelLocator GetItems()
        {
            FlightPakMasterService.FuelLocator FuelServiceType = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.FuelLocator objFuelLocator = new FlightPakMasterService.FuelLocator();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    FuelServiceType = new FlightPakMasterService.FuelLocator();
                    FuelServiceType.FuelLocatorCD = tbCode.Text;
                    FuelServiceType.FuelLocatorDescription = tbDescription.Text;
                    if (hdnSave.Value == "Update")
                    {
                        //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                        if (Session["SelectedFuelLocatorID"] != null)
                        {
                            FuelServiceType.FuelLocatorID = Convert.ToInt64(Session["SelectedFuelLocatorID"].ToString());
                        }
                    }
                    else
                    {
                        FuelServiceType.FuelLocatorID = 0;
                    }
                    if (!string.IsNullOrEmpty(hdnClientCode.Value))
                    {
                        FuelServiceType.ClientID = Convert.ToInt64(hdnClientCode.Value);
                    }
                    else
                    {
                        FuelServiceType.ClientID = null;
                    }
                    FuelServiceType.IsDeleted = false;
                    FuelServiceType.IsInActive = chkInactive.Checked;
                    return FuelServiceType;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return FuelServiceType;
            }
        }
        private bool checkAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnFlag = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<GetAllFuelLocator> FuelCodeList = new List<GetAllFuelLocator>();
                    FuelCodeList = ((List<GetAllFuelLocator>)Session["FuelLocatorCD"]).Where(x => x.FuelLocatorCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<GetAllFuelLocator>();
                    if (FuelCodeList.Count != 0)
                    {
                        cvCode.IsValid = false;
                        tbCode.Focus();
                        ReturnFlag = true;
                    }
                    return ReturnFlag;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnFlag;
            }
        }
        /// <summary>
        /// To check Valid Client Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if (tbClientCode.Text != null && tbClientCode.Text.Trim() != string.Empty)
                        //{
                        //    FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient();
                        //    var objRetVal = ObjService.GetClientCodeList().EntityList.Where(x => x.ClientCD.Trim().ToUpper() == (tbClientCode.Text.Trim().ToUpper())).ToList();
                        //    if (objRetVal.Count() <= 0)
                        //    {
                        //        cvClientCode.IsValid = false;
                        //        tbClientCode.Focus();
                        //    }
                        //    else
                        //    {
                        //        if (string.IsNullOrEmpty(hdnClientCode.Value))
                        //        {

                        //            hdnClientCode.Value = ((FlightPakMasterService.Client)objRetVal[0]).ClientID.ToString().Trim();
                        //        }
                        //    }
                        //}
                        return CheckClientCodeExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
            }
        }
        private bool CheckClientCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tbClientCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = true;
                    if (tbClientCode.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetClientCodeList().EntityList.Where(x => x.ClientCD.Trim().ToUpper().ToString() == tbClientCode.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
                                ClientList = (List<FlightPakMasterService.Client>)objRetVal.ToList();
                                hdnClientCode.Value = ClientList[0].ClientID.ToString();
                                tbClientCode.Text = ClientList[0].ClientCD;
                                tbDescription.Focus();
                                returnVal = true;
                            }
                            else
                            {
                                cvClientCode.IsValid = false;
                                tbClientCode.Focus();
                                returnVal = false;
                            }
                        }
                    }
                    else
                    {
                        hdnClientCode.Value = string.Empty;
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllFuelLocator> lstFuelLocator = new List<FlightPakMasterService.GetAllFuelLocator>();
                if (Session["FuelLocatorCD"] != null)
                {
                    lstFuelLocator = (List<FlightPakMasterService.GetAllFuelLocator>)Session["FuelLocatorCD"];
                }
                if (lstFuelLocator.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstFuelLocator = lstFuelLocator.Where(x => x.IsInActive == false).ToList<GetAllFuelLocator>(); }
                    dgFuelLocator.DataSource = lstFuelLocator;
                    if (IsDataBind)
                    {
                        dgFuelLocator.DataBind();
                    }
                }

                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFuelLocator.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var FuelLocatorValue = FPKMstService.GetFuelLocatorInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, FuelLocatorValue);
            List<FlightPakMasterService.GetAllFuelLocator> filteredList = GetFilteredList(FuelLocatorValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FuelLocatorID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFuelLocator.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFuelLocator.CurrentPageIndex = PageNumber;
            dgFuelLocator.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllFuelLocator FuelLocatorValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedFuelLocatorID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = FuelLocatorValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FuelLocatorID;
                Session["SelectedFuelLocatorID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllFuelLocator> GetFilteredList(ReturnValueOfGetAllFuelLocator FuelLocatorValue)
        {
            List<FlightPakMasterService.GetAllFuelLocator> filteredList = new List<FlightPakMasterService.GetAllFuelLocator>();

            if (FuelLocatorValue.ReturnFlag)
            {
                filteredList = FuelLocatorValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetAllFuelLocator>(); }
                }
            }

            return filteredList;
        }
    }
}
